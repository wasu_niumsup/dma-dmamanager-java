package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;

public interface StoryBoardTargetTypeAccessor {

	public Map<String,List<Map<String, Object>>> listStoryBoardTargetType(ServiceData input) throws Exception;
	
}
