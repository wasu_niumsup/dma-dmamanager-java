package com.jjpa.dma.manager.processor.binding;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.dma.manager.binding.UiJsonBinding;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;

@SuppressWarnings("unchecked")
public class BindingDynamicListActionProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingDynamicListActionProcessor.class);

	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}

	public void process(AppUiDocument appUiDoc) throws Exception {
		logger.info("###Process ===> BindingDynamicListActionProcessor");
		
		
		String templateType = appUiDoc.getTemplateType();

		if (DmaConstants.AppUI.Value.DYNAMIC_LIST.equals(templateType)) {		
			logger.info("case : templateType=" + templateType);
			
			generateDynamicListAction(appUiDoc);
		}
	}
	
	
	/* 
	 * 05/01/2561 add by Akanit : generate List 
	 * */
	private void generateDynamicListAction(AppUiDocument appUiDoc) throws Exception {

		List<Map<String, Object>> bindingList = (List<Map<String, Object>>) appUiDoc.getUiConfigData().get(DmaConstants.AppUI.Key.BINDING);

		
		/* 
		 * 26/01/2561 comment by Akanit : old code
		 * */
//		/* find row in dataList where property_name=Topic */
//		Map<String, Object> topicRow = findRowBySearchString(bindingList, DmaConstants.AppUI.Key.PROPERTY_NAME, DmaConstants.AppUI.Value.TOPIC);
//			if (topicRow != null) {
//			logger.info("found Topic row then binding List");
//	
//			bindingDynamicListActionTopic(appUiDoc, topicRow);
//		}
//	
//		/* find row in dataList where property_name=Description */
//		Map<String, Object> descriptionRow = findRowBySearchString(bindingList, DmaConstants.AppUI.Key.PROPERTY_NAME, DmaConstants.AppUI.Value.DESCRIPTION);
//		
//		if (descriptionRow != null) {
//			logger.info("found Description row then binding List");
//	
//			bindingDynamicListActionDesc(appUiDoc, descriptionRow);
//		}
		
		
		/* 
		 * 26/01/2561 add by Akanit : new code
		 * */
		
		/* find multiRow in bindingList where component_type=combo */
		List<Map<String, Object>> comboRowList = ManagerArrayList.getRowListBySearchString(bindingList, DmaConstants.AppUI.Key.COMPONENT_TYPE, DmaConstants.AppUI.Value.COMBO);
		logger.info("comboRowList.size="+comboRowList.size());
		
		for(Map<String, Object> comboRow : comboRowList){

			Map<String, Object> componentTypeOptionRow = DataUtils.getMapFromObject(comboRow, DmaConstants.AppUI.Key.COMPONENT_TYPE_OPTION);
			boolean defaultFlag = DataUtils.getBooleanValue(comboRow.get(DmaConstants.AppUI.Key.DEFAULT), true);
			
			if(!defaultFlag && componentTypeOptionRow!=null){
				
				String optionType = DataUtils.getStringValue(componentTypeOptionRow, DmaConstants.AppUI.Key.Component_type_option.OPTION_TYPE, null);
				Boolean checkNull = DataUtils.getBooleanValue(componentTypeOptionRow, DmaConstants.AppUI.Key.Component_type_option.CHECKNULL, true);

				if(DmaConstants.AppUI.Value.OUTPUT.equals(optionType)){
					
					bindingDynamicList(appUiDoc, comboRow,checkNull);
				}
			}
		}
		
		
		/* create value for bindingDynamic */
		String value_apiUrl = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/send";	
		String value_apiData = appUiDoc.getDynamicApi();
		
		bindingDynamicListByName(appUiDoc, DmaConstants.AppUI.Value.API_URL, DmaConstants.AppUI.Key.BINDING, value_apiUrl);			
		bindingDynamicListByName(appUiDoc, DmaConstants.AppUI.Value.API_DATA, DmaConstants.AppUI.Key.BINDING, value_apiData);
	}

	
	/* 
	 * 26/01/2561 add by Akanit : sub method use for generateDynamicListAction
	 * */
	private void bindingDynamicList(AppUiDocument appUiDoc, Map<String, Object> bindingRow , Boolean checkNull) throws Exception {

		/* get parameter */
		String propertyName = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PROPERTY_NAME, null);
		String group = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.GROUP, null);
		String path = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PATH, null);
		String value = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.VALUE, null);
	
		if(value!=null){
			if(value.contains("]")){
				logger.info("found ] then extract value and binding newPath");
				
				/* 
				 * get root and name from value.
				 * 
				 * Example data :
				 * 		value = Value.detail[0].username
				 * 		root = Value.detail[0]
				 * 		name = username 
				 * 
				 *  */
				int lastIndex_closeBucket = value.lastIndexOf("]");
				
				String root = value.substring(0, lastIndex_closeBucket+1);
				String name = value.substring(lastIndex_closeBucket+2);
				
				

				/* create value for bind 'uiJson' */
//				List<Map<String, Object>> bindingList = JasonNetteUtils.generateTopicBinding(name);
				String newName = "{{"+name+" || ''}}";
				if(checkNull){
					newName = "{{"+name+" || ''}}";
				}else{
					newName = name;
				}
											
				/* binding field 'uiJson' in db(app_ui_config) */
				UiJsonBinding.bindingAppUi(appUiDoc, path, newName);
				
				
				
				/* create value for bind 'uiJson' */
				String value_resultList = JasonNetteUtils.generateResultList(root);
				
				/* binding field 'uiJson' in db(app_ui_config) */
				bindingDynamicListByName(appUiDoc, DmaConstants.AppUI.Value.RESULT_LIST, DmaConstants.AppUI.Key.BINDING, value_resultList);
				
				
			}else {
				logger.info("not found ] then throw configuration exception.");
				throw new Exception("Invalid value configuration. Component \"" + group + "\", Property \"" + propertyName + "\"" ); 
			}
			
		}else {
			logger.info("value=null then throw configuration exception.");
			throw new Exception("Invalid value configuration. Component \"" + group + "\", Property \"" + propertyName + "\"" ); 
		}
		
	}

	
	/* 
	 * 09/01/2561 add by Akanit : sub method use for generateDynamicListAction
	 * */
	private void bindingDynamicListByName(AppUiDocument appUiDoc, String name, String fieldName_path, String value) throws Exception {
		
		/* 
		 * binding newPath to field 'dynamicDataBinding' in db(app_ui_config) 
		 * */
		List<Map<String, Object>> dynamicDataBindingList = (List<Map<String, Object>>)appUiDoc.getDynamicDataBinding();
				
		/* find row where by name in dynamicDataBinding */
		Map<String, Object> row = ManagerArrayList.findRowBySearchString(dynamicDataBindingList, DmaConstants.AppUI.Key.NAME, name);
		if(row!=null){
			logger.info("binding 'uiJson' in db(app_ui_config)");
			
			/* create path */
			String path = DataUtils.getStringValue(row, fieldName_path, null);

			UiJsonBinding.bindingAppUi(appUiDoc, path, value);
		}
	}
	
	
//	/* 
//	 * 10/01/2561 add by Akanit : sub method use for generateDynamicListAction
//	 * */
//	private void bindingDynamicListActionTopic(AppUiDocument appUiDoc, Map<String, Object> bindingRow) throws Exception {
//
//		/* get parameter */
//		String propertyName = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PROPERTY_NAME, null);
//		String group = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.GROUP, null);
//		String path = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PATH, null);
//		String value = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.VALUE, null);
//	
//		if(value!=null){
//			if(value.contains("]")){
//				logger.info("found ] then extract value and binding newPath");
//				
//				/* 
//				 * get root and name from value.
//				 * 
//				 * Example data :
//				 * 		value = Value.detail[0].username
//				 * 		root = Value.detail[0]
//				 * 		name = username 
//				 * 
//				 *  */
//				int lastIndex_closeBucket = value.lastIndexOf("]");
//				
//				String root = value.substring(0, lastIndex_closeBucket+1);
//				String name = value.substring(lastIndex_closeBucket+2);
//				
//				
//
//				/* create value for bind 'uiJson' */
////				List<Map<String, Object>> bindingList = JasonNetteUtils.generateTopicBinding(name);
//				String newName = "{{"+name+"}}";
//											
//				/* binding field 'uiJson' in db(app_ui_config) */
//				UiJsonBinding.bindingAppUi(appUiDoc, path, newName);
//				
//				
//				
//				/* create value for bind 'uiJson' */
//				String value_resultList = JasonNetteUtils.generateResultList(root);
//				
//				/* binding field 'uiJson' in db(app_ui_config) */
//				bindingDynamicListByName(appUiDoc, DmaConstants.AppUI.Value.RESULT_LIST, DmaConstants.AppUI.Key.BINDING, value_resultList);
//				
//				
//			}else {
//				logger.info("not found ] then throw configuration exception.");
//				throw new Exception("Invalid value configuration. Component \"" + group + "\", Property \"" + propertyName + "\"" ); 
//			}
//			
//		}else {
//			logger.info("value=null then throw configuration exception.");
//			throw new Exception("Invalid value configuration. Component \"" + group + "\", Property \"" + propertyName + "\"" ); 
//		}
//		
//	}
//	
//	/* 
//	 * 05/01/2561 add by Akanit : sub method use for generateDynamicListAction
//	 * */
//	private void bindingDynamicListActionDesc(AppUiDocument appUiDoc, Map<String, Object> bindingRow) throws Exception {
//
//		/* get parameter */
//		String property_name = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PROPERTY_NAME, null);
//		String path = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.PATH, null);
//		String value = DataUtils.getStringValue(bindingRow, DmaConstants.AppUI.Key.VALUE, null);
//	
//		if(value!=null){
//			if(value.contains("]")){
//				logger.info("found ] then extract value and binding newPath");
//				
//				/* 
//				 * extract root and name from value.
//				 * 
//				 * Example data :
//				 * 		value = Value.detail[0].username
//				 * 		root = Value.detail[0]
//				 * 		name = username 
//				 * 
//				 *  */
//				int lastIndex_closeBucket = value.lastIndexOf("]");
//				
//				String root = value.substring(0, lastIndex_closeBucket+1);
//				String name = value.substring(lastIndex_closeBucket+2);
//				
//				
//								
//				/* create value for bind 'uiJson' */
//				String newName = "{{"+name+"}}";
////				List<Map<String, Object>> bindingList = JasonNetteUtils.generateDescriptionBinding(name);
//				
//				/* binding field 'uiJson' in db(app_ui_config) */
//				UiJsonBinding.bindingAppUi(appUiDoc, path, newName);
//				
//				
//				
//				/* create value for bindingDynamic */
//				String value_resultList = JasonNetteUtils.generateResultList(root);
//				
//				/* binding field 'uiJson' in db(app_ui_config) */
//				bindingDynamicListByName(appUiDoc, DmaConstants.AppUI.Value.RESULT_LIST, DmaConstants.AppUI.Key.BINDING, value_resultList);
//				
//				
//			}else {
//				logger.info("not found ] then throw configuration exception.");
//				throw new Exception("Invalid value configuration. field="+property_name); 
//			}
//			
//		}else {
//			logger.info("value=null then throw configuration exception.");
//			throw new Exception("Invalid value configuration. field="+property_name); 
//		}
//		
//	}
}
