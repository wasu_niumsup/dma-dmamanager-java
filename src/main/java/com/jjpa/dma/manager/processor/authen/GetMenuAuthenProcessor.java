package com.jjpa.dma.manager.processor.authen;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ichat.api.iChatApi;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.UserModel;
import com.jjpa.common.util.PreviewConfig;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import com.jjpa.web.servlet.UserSession;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;

public class GetMenuAuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(GetMenuAuthenProcessor.class);

	private UserSession getUserSession() {
		return SpringApplicationContext.getBean("UserSession");
	}

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> GetMenuAuthenProcessor");
		logger.debug("Process ===> GetMenuAuthenProcessor : {}", input.toJsonString());

		return getMenu(input);

	}

	public Map<String, Object> getMenu(ServiceData input) throws Exception {
		//		String username = DataUtils.getStringValue(input.getValue("username"));
		UserSession session = getUserSession();
		UserModel model = session.getUser();
		String role = "";
		if (model != null) {
			role = model.getRole();
		}
		return generateMenu(model.getUsername(), role);
	}

	private Map<String, Object> generateMenu(String username, String role) throws Exception {
		Map<String, Object> menu = null;
		if("templateAdmin".equals(username) && "system".equals(role) ){
			menu = generateTemplateAdminMenu();
		} else if ("admin".equals(role)) {
			menu = generateAdminMenu();
		} 
		else if ("user".equals(role)) {
			menu = generateUserMenu();
		}
		else if ("system".equals(role)) {
			menu = generateSystemMenu();
		} else {
			throw new Exception("Unauthorize user");
		}

		// Call iConsole get preview flag on/off
		Map<String, Object> previewConfig = new LinkedHashMap<>();
		previewConfig.put("enableMode", PreviewConfig.getPreviewEnableMode());
		menu.put("previewConfig",previewConfig);

		return menu;
	}

	private Map<String, Object> generateUserMenu() {
		return new ManagerPropertiesFile().readJson(PropConstants.MenuUserInfo.NAME); // read menu from file /menu-admin-info.json
	}
	private Map<String, Object> generateAdminMenu() {
		return new ManagerPropertiesFile().readJson(PropConstants.MenuAdminInfo.NAME); // read menu from file /menu-admin-info.json
	}

	private Map<String, Object> generateSystemMenu() {
		return new ManagerPropertiesFile().readJson(PropConstants.MenuSystemInfo.NAME); // read menu from file /menu-system-info.json
	}

	private Map<String, Object> generateTemplateAdminMenu() {
		return new ManagerPropertiesFile().readJson(PropConstants.MenuTemplateAdminInfo.NAME); // read menu from file /menu-templateadmin-info.json
	}
	

}
