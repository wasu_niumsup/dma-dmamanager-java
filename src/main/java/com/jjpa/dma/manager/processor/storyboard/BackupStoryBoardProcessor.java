package com.jjpa.dma.manager.processor.storyboard;

import java.io.File;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.backup.GenerateActionChainFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateActionChainTemplateFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateImageFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateScreenFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateServiceGatewayFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateStoryBoardFolderBackup;
import com.jjpa.dma.manager.processor.backup.GenerateTemplateFolderBackup;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;
import servicegateway.utils.Zip4j;
import servicegateway.utils.ZipFiles;

public class BackupStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(BackupStoryBoardProcessor.class);
	
	protected StoryBoardAccessor storyBoardAccessor;
	
	private String fileBackupPath, fileBackupType, folderNameDMA, zipPass;
	
	public BackupStoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> BackupStoryBoardProcessor");
		logger.debug("Process ===> BackupStoryBoardProcessor : {}", input.toJsonString());

		
		/* get input from UI */
		String storyBoardName;
		try{
			storyBoardName = (String)input.getValue(DmaConstants.StoryBoard.Key.STORY_NAME);
		}catch(Exception e){
			String errorMessage = "Please identify "+ DmaConstants.StoryBoard.Key.STORY_NAME;
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		
		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
		fileBackupPath = ManagerMap.getString(propMap, PropConstants.FileLocal.Backup.Key.FILE_PATH, "");
		fileBackupType = ManagerMap.getString(propMap, PropConstants.FileLocal.FILE_TYPE_DMA, "");
		folderNameDMA = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.DMA, "");
//		folderNameSG = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.SG, "");
//		zipNameSG = ManagerMap.getString(propMap, "ZIP_NAME_SG", "");
//		fileTypeSG = ManagerMap.getString(propMap, "FILE_TYPE_SG", "");
		zipPass = ManagerMap.getString(propMap, PropConstants.FileLocal.ZIP_PASSWORD, "");
		
		if(StringUtil.isBlank(fileBackupPath)){
			String errorMessage = "stop process : In "+ PropConstants.FileLocal.NAME +" : FILE_BACKUP_PATH is empty.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		
		/* create path to write file */
		String folderBackupName = "temp" + new Date().getTime();
		String directoryFileBackup = fileBackupPath + folderBackupName;
		String directoryFileBackupDMA = directoryFileBackup + File.separator + folderNameDMA;
//		String directoryFileBackupSG = directoryFileBackup + File.separator + folderNameSG;
		
		
		
		/* find storyBoardDoc from mongo db */
		StoryBoardDocument storyBoardDoc = storyBoardAccessor.findStoryByName(storyBoardName);
		
		if(storyBoardDoc==null){
			String errorMessage = "storyBoard '"+ storyBoardName +"' not found in DB.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		if(storyBoardDoc.getReference()==null){
			String errorMessage = "Reference of storyBoard \"" + storyBoardDoc.getStoryName() + "\" not found, this storyBoard is old version, Please save this storyBoard again.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		
		
		/* create path zip file backup */
		String fileBackupName = new Date().getTime() + fileBackupType;
		String directoryZipFileBackup_temp = fileBackupPath + fileBackupName + "temp";
		String directoryZipFileBackup_pass = fileBackupPath + fileBackupName;
		
		try{
			
			/* create file backup */
			createFileBackupDMA(storyBoardDoc, directoryFileBackupDMA);
			createFileBackupSG(storyBoardDoc, directoryFileBackup);

			
			/**
			 * Tanawat W.
			 * [2018-12-18] DMA 5.0
			 * 
			 */
			Map<String,Object> informationForExport = new LinkedHashMap<>();
			informationForExport.put("storyName", storyBoardDoc.getStoryName());
			informationForExport.put("screenList", storyBoardDoc.getScreenNameList());
			informationForExport.put("dmaDirectoryPath", directoryFileBackup);
			logger.info(" informationForExport : ",informationForExport);
			callHook("Export", informationForExport);

			
			/* zip */
			ZipFiles zip = new ZipFiles();
			zip.setSource(directoryFileBackup);
			
			logger.info("ziping ...");
			zip.zipIt(directoryZipFileBackup_temp);
			logger.info("ziping success");
			
			
			/* 
			 * 09/04/2561 add by Akanit : zipping again, this round have set password
			 * 
			 * Q : why zipping again?
			 * A : because lib zip4j can set password to zip but this lib can't zip folder have multiple level.
			 * 
			 * So i want to use my lib first (my lib can zip multiple level) and then use zip4j.
			 *  */
			new Zip4j().zip(directoryZipFileBackup_temp, directoryZipFileBackup_pass, zipPass);
			
			
		} catch (Exception e){
			throw e;
		} finally {
			ManagerStore.deleteFile(directoryFileBackup);
			ManagerStore.deleteFile(directoryZipFileBackup_temp);
		}
		
		
		Map<String, Object> output = new LinkedHashMap<>();
		output.put(BackupConstants.FILE_UPLOAD_NAME, fileBackupName);
		if(ManagerMap.getBoolean(input.getData(), "fromPrivate",false) == true){
			output.put("reference",  storyBoardDoc.getReference());
		}
		logger.info("output="+output);
		logger.info("End Procces ===> BackupStoryBoardProcessor");
		return output;
	}
	
	

	private void createFileBackupDMA(StoryBoardDocument storyBoardDoc, String directoryFileBackup) throws Exception {
		
		/* create file backup (DMA) */
		new GenerateStoryBoardFolderBackup().process(storyBoardDoc, directoryFileBackup);
		new GenerateImageFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
		new GenerateScreenFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
		new GenerateTemplateFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
		
		/* 17/08/2561 comment by Akanit : get data by reference from database */
//		new GenerateActionChainTemplateFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
//		new GenerateActionChainFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
		
		/* 17/08/2561 add by Akanit : get data all database */
		new GenerateActionChainTemplateFolderBackup().process(directoryFileBackup);
		new GenerateActionChainFolderBackup().process(directoryFileBackup);
		
		
//		new GenerateServiceGatewayFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);
		
		
		/* zip file backup (DMA) */
//		String fileBackupName = new Date().getTime() + fileBackupType;
//		String directoryZipFileBackup = fileBackupPath + folderBackupName + File.separator + folderNameDMA;
//		
//		ZipFiles zip = new ZipFiles();
//		zip.setSource(directoryFileBackup);
//		zip.zipIt(directoryZipFileBackup);
//		
//		ManagerStore.deleteFile(directoryFileBackup);
		
	}
	
	private void createFileBackupSG(StoryBoardDocument storyBoardDoc, String directoryFileBackup) throws Exception {
		
		
		/* create file backup (ServiceGateway) */
		/* 14/11/2562 comment by Akanit : DMA.5.9.0 : DMA-1739 : fix bug reference invalid when change sgwAPIdropdown in storyboard. */
//		new GenerateServiceGatewayFolderBackup().process(storyBoardDoc.getReference(), directoryFileBackup);

		/* 14/11/2562 add by Akanit : DMA.5.9.0 : DMA-1739 : fix bug reference invalid when change sgwAPIdropdown in storyboard. */
		new GenerateServiceGatewayFolderBackup().process(storyBoardDoc, directoryFileBackup);
		
		
		/*
		 *  zip file backup (ServiceGateway) 
		 *  */
//		String directoryZipFileBackup = directoryFileBackup + File.separator + zipNameSG + fileTypeSG;
		
		/* zip folder */
//		ZipFiles zip = new ZipFiles();
//		zip.setSource(directoryFileBackup);
//		zip.zipIt(directoryZipFileBackup);
//		
//		ManagerStore.deleteFile(directoryFileBackup);
			
		/* zip entryFiles */
//		ManagerZip.zipFiles(zipEntryList, directoryZipFileBackup);
	}

}
