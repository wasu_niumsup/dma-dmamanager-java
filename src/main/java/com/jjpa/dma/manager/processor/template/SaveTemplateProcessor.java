package com.jjpa.dma.manager.processor.template;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class SaveTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SaveTemplateProcessor.class);
		
	protected TemplateAccessor templateAccessor;
	
	public SaveTemplateProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveTemplateProcessor");
		logger.debug("Process ===> SaveTemplateProcessor : {}", input.toJsonString());
		
		return saveTemplate(input);
		
	}
	
	@SuppressWarnings("unchecked")
	private Object saveTemplate(ServiceData input) throws Exception {
		
		String user = (String)input.getValue("user");
		String templateCode = (String)input.getValue("templateCode");
		
		Map<String, Object> templateMap = JsonPath.parse(templateCode).json();
		
		DMATemplate template = new DMATemplate();
		template.setTemplateName((String)templateMap.get("templateName"));
		template.setBinding((List<Map<String,Object>>)templateMap.get("binding"));
		template.setActionList((List<Map<String,Object>>)templateMap.get("actionList"));
		template.setInputList((List<Map<String,Object>>)templateMap.get("inputList"));
		template.setInfoUrl((String)templateMap.get("infoUrl"));
		template.setTemplateType((String)templateMap.get("templateType"));
		template.setDynamicApi((String)templateMap.get("dynamicApi"));
		template.setDynamicDataBinding((List<Map<String,Object>>)templateMap.get("dynamicDataBinding"));
		DocumentContext ctx = JsonPath.parse(templateMap.get("nativeJson"));
		String nativeJson = (String)ctx.jsonString();
		Map<String,String> data = new HashMap<String, String>();
		data.put("data", nativeJson);
		template.setNativeJson(data);
		template.setVersionTemplate((String)templateMap.get("versionTemplate"));
		template.setVersionEngine((String)templateMap.get("versionEngine"));
		template.setVersionEngineAndroidMobile((String)templateMap.get("versionEngineAndroidMobile"));
		template.setVersionEngineIOSMobile((String)templateMap.get("versionEngineIOSMobile"));
		
		/* 16/05/2561 add by Akanit new lastUpdate*/
		template.setLastUpdate(new Date());
		
		return templateAccessor.saveTemplateAndUpdateCurrentVersion(template, user);
	}

}
