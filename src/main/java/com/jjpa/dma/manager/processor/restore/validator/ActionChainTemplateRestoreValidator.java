package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.spring.SpringApplicationContext;

public class ActionChainTemplateRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(ActionChainTemplateRestoreValidator.class);
	
	protected ActionChainTemplateAccessor actionChainTemplateAccessor;
	
	public ActionChainTemplateRestoreValidator() {
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
	}
	
	public Map<String, Object> process(List<Map<String, Object>> actionChainTemplateRequiredList) throws Exception {
		logger.info("Process ===> ActionChainTemplateRestoreValidator");
		
		boolean validationStatus = true;
    	@SuppressWarnings("unused")
		String version_restore, version_old, name;
    	
    	for(Map<String, Object> actionChainTemplateRequired : actionChainTemplateRequiredList){
    		
    		actionChainTemplateRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
    		
    		
//    		version_restore = ManagerMap.getString(actionChainTemplateRequired, ReferenceConstants.DMA.VERSION);
//    		name = ManagerMap.getString(actionChainTemplateRequired, ReferenceConstants.DMA.NAME);
//    		
//    		DMAActionChainTemplate doc = actionChainTemplateAccessor.findActionChainTemplateByType(name);
//    		
//    		
//    		/* validate add status */
//    		if(doc==null){
//    			logger.info("#not found old ActionChainTemplate="+ name);
//    			logger.info("#update action=ADD");
//    			logger.info("#update status=true");
//				
//				actionChainTemplateRequired.put("status", true);
//    			continue;
//    			
//    		}else {
//    			logger.info("#found old ActionChainTemplate="+ name);
//    		}
//    		
//    		
//    		try {
//    			version_old = doc.getVersion();
//			} catch (Exception e) {
//				logger.info("#ActionChainTemplate="+ name + "not have reference.");
//				logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				actionChainTemplateRequired.put("status", true);
//				continue;
//			}
//    		
//    		
//    		
//    		if(version_restore == version_old){
//    			logger.info("#ActionChainTemplate="+ name + " can restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				actionChainTemplateRequired.put("status", true);
//    			
//    		}else{
//    			logger.info("#ActionChainTemplate="+ name + " can not restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//
////    			validationStatus = false;
//				actionChainTemplateRequired.put("status", true);
//    		}
    		
    	}
    	
    	/* mapping data to UI */
    	Map<String, Object> output = new LinkedHashMap<>();
    	output.put("summaryValidation", validationStatus);
    	output.put("data", actionChainTemplateRequiredList);
    	
    	return output;
	}
}
