package com.jjpa.dma.manager.processor.actionchain;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class DeleteActionChainProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteActionChainProcessor.class);
		
	protected ActionChainAccessor actionChainAccessor;
	
	public DeleteActionChainProcessor() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> DeleteActionChainProcessor");
		logger.debug("Process ===> DeleteActionChainProcessor : {}", input.toJsonString());
		
		actionChainAccessor.deleteActionChain(input);
		return actionChainAccessor.listActionChainType();
	}

}
