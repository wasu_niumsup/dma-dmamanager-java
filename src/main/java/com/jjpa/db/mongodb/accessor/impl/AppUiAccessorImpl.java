package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardScreenListAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardStoryBoardListAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerLogger;
import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
public class AppUiAccessorImpl implements AppUiAccessor{
	
	private static final Logger logger = LoggerFactory.getLogger(AppUiAccessorImpl.class);
	
	protected MongoDbAccessor mongoDbAccessor;
	protected TemplateAccessor templateAccessor;
	protected StoryBoardStoryBoardListAccessor storyBoardStoryBoardListAccessor;
	protected StoryBoardScreenListAccessor storyBoardScreenListAccessor;
	
	private String errorTemplate;

	protected AppUiAccessorImpl(MongoDbAccessor mongoDbAccessor, TemplateAccessor templateAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
		this.templateAccessor = templateAccessor;
		this.storyBoardStoryBoardListAccessor = SpringApplicationContext.getBean("StoryBoardStoryBoardListAccessor");
		this.storyBoardScreenListAccessor = SpringApplicationContext.getBean("StoryBoardScreenListAccessor");
	}
	
	public List<AppUiDocument> findAll() throws Exception{
		return mongoDbAccessor.findAll(AppUiDocument.class);
	}
	
	public AppUiDocument findAppUiByName(String uiName) throws Exception{
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.AppUI.Path.UI_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(uiName), Pattern.CASE_INSENSITIVE)));
		AppUiDocument uiDoc = mongoDbAccessor.findOneByCriteria(findByName, AppUiDocument.class);
		return uiDoc;
	}
	
	public Map<String,Object> listAppUi(ServiceData input) throws Exception {
		List<AppUiDocument> uiList = mongoDbAccessor.findAll(AppUiDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.AppUI.Key.APP_UI_LIST, getAppUiList(uiList));
		return result;
	}
	
	public Map<String,Object> listAppUiWithAction(ServiceData input) throws Exception {
		List<AppUiDocument> uiList = mongoDbAccessor.findAll(AppUiDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.AppUI.Key.APP_UI_LIST, getAppUiListWithAction(uiList));
		return result;
	}
	
	public Map<String,Object> listAppUiByCriteria(Query query) throws Exception {
		List<AppUiDocument> uiList = mongoDbAccessor.findByCriteria(query, AppUiDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.AppUI.Key.APP_UI_LIST, getAppUiList(uiList));
		return result;
	}


	public int count(Query query) throws Exception {
		return mongoDbAccessor.findCountByCriteria(query,AppUiDocument.class);
	}
	
	public void deleteAppUi(ServiceData input) throws Exception {
		String user = (String)input.getValue(AppConstants.USER);
		List<Map<String, Object>> uiList = (List<Map<String, Object>>)input.getValue(DmaConstants.AppUI.Key.UI_LIST);
		for(Map<String, Object> uiObj : uiList){
			String uiName = (String)uiObj.get(DmaConstants.AppUI.Key.UI_NAME);
			mongoDbAccessor.delete(findAppUiByName(uiName), user);
		}
	}
	
	public Object saveUi(AppUiDocument document, String user) throws Exception{
		if( StringUtil.isBlank(document.getDmaManagerVersion()) ){
			document.setDmaManagerVersion( ConfigurationUtils.getEngineVersion() );
		}
		
		
		String templateName = (String)document.getUiConfigData().get(DmaConstants.AppUI.Key.TEMPLATE);
		DMATemplate template = templateAccessor.findTemplateByName(templateName);
		

		/**
		 * Tanawat W.
		 * 2018-12-18
		 * 
		 * DMA 5.0
		 * 
		 */
		int version  = Integer.parseInt(  template.getVersionTemplate().split("\\.")[0] );

		if(document.getId()!=null){

			AppUiDocument uiDoc = mongoDbAccessor.findById(document.getId(), AppUiDocument.class);
			if(uiDoc!=null){
								
				if(version >= 5){
					uiDoc.setInputList(document.getInputList());
					uiDoc.setActionList(document.getActionList());
				}else{
					uiDoc.setInputList(template.getInputList());
					uiDoc.setActionList(template.getActionList());
				}
				
				uiDoc.setUiName(document.getUiName());
				uiDoc.setRemark(document.getRemark());
				uiDoc.setUiConfigData(document.getUiConfigData());
				uiDoc.setStatus(document.getStatus());
				uiDoc.setDynamicApi(document.getDynamicApi());
				uiDoc.setTemplateType(document.getTemplateType());
				uiDoc.setDynamicInputList(document.getDynamicInputList());
				uiDoc.setDynamicOutputList(document.getDynamicOutputList());
				uiDoc.setDynamicDataBinding(document.getDynamicDataBinding());
				uiDoc.setReference(document.getReference());
				uiDoc.setUiJson(document.getUiJson());
				uiDoc.setDmaManagerVersion( ConfigurationUtils.getEngineVersion() );
				uiDoc.setLastUpdate(document.getLastUpdate());
				
				/* 25/03/2562 add by Akanit : requirement from siam */
				uiDoc.setUrlScreenShot(document.getUrlScreenShot());
				
				document = uiDoc;
			}
			return mongoDbAccessor.save(document, user);
		}else{
			
			if(version >= 5){
				//			document.setMasterJson(template.getNativeJson());
				document.setInputList(document.getInputList());
				document.setActionList(document.getActionList());
				//			document.setDynamicDataBinding(template.getDynamicDataBinding());
			}else{
				//			document.setMasterJson(template.getNativeJson());
				document.setInputList(template.getInputList());
				document.setActionList(template.getActionList());
				//			document.setDynamicDataBinding(template.getDynamicDataBinding());
			}
			return mongoDbAccessor.save(document, user);
		}
	}
	
	public Object restoreUi(AppUiDocument document, String user) throws Exception{
		if( StringUtil.isBlank(document.getDmaManagerVersion()) ){
			document.setDmaManagerVersion( ConfigurationUtils.getEngineVersion() );
		}
		return mongoDbAccessor.save(document, user);
	}
	
	public Object upgradeUi(AppUiDocument document, String user) throws Exception{
		if(document.getId()!=null){

			logger.info("###################### Process ===> AppUiAccessorImpl upgradeUi document.getId=!null ######################");

			AppUiDocument uiDoc = mongoDbAccessor.findById(document.getId(), AppUiDocument.class);
			uiDoc.setUiName(document.getUiName());
			uiDoc.setRemark(document.getRemark());
			uiDoc.setUiConfigData(document.getUiConfigData());
			uiDoc.setStatus(document.getStatus());
			uiDoc.setDynamicApi(document.getDynamicApi());
			uiDoc.setTemplateType(document.getTemplateType());
			uiDoc.setDynamicInputList(document.getDynamicInputList());
			uiDoc.setDynamicOutputList(document.getDynamicOutputList());
			uiDoc.setDynamicDataBinding(document.getDynamicDataBinding());
			uiDoc.setReference(document.getReference());
			uiDoc.setUiJson(document.getUiJson());

			/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
			uiDoc.setUrlScreenShot(document.getUrlScreenShot());
			
			/* add by warrawan 22/03/2018 */
			String templateName = (String)document.getUiConfigData().get(DmaConstants.AppUI.Key.TEMPLATE);
			DMATemplate template = templateAccessor.findTemplateByName(templateName);
			uiDoc.setInputList(template.getInputList());
			uiDoc.setActionList(template.getActionList());


//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getUiName " + uiDoc.getUiName());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getUiConfigData " + uiDoc.getUiConfigData());
//
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getStatus " + uiDoc.getStatus());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getDynamicApi " + uiDoc.getDynamicApi());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getTemplateType " + uiDoc.getTemplateType());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getDynamicInputList " + uiDoc.getDynamicInputList());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getDynamicOutputList " + uiDoc.getDynamicOutputList());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getDynamicDataBinding " + uiDoc.getDynamicDataBinding());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getReference " + uiDoc.getReference());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getActionList " + uiDoc.getActionList());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId=!null uiDoc.getInputList " + uiDoc.getInputList());

//			logger.info("#####################################################################################################");

			return mongoDbAccessor.save(uiDoc, user);
		}else{

			logger.info("@@@@@@@@@@@@@@@@@@@@@ Process ===> AppUiAccessorImpl upGradeUi document.getId==null @@@@@@@@@@@@@@@@@@@@@");

			String templateName = (String)document.getUiConfigData().get(DmaConstants.AppUI.Key.TEMPLATE);
			DMATemplate template = templateAccessor.findTemplateByName(templateName);
//			document.setMasterJson(template.getNativeJson());
			document.setInputList(template.getInputList());
			document.setActionList(template.getActionList());
//			document.setDynamicDataBinding(template.getDynamicDataBinding());

//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId==null getActionList " +document.getActionList());
//			logger.info("Process ===> AppUiAccessorImpl upGradeUi document.getId==null getInputList " +document.getInputList());

//			logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			
			return mongoDbAccessor.save(document, user);
		}
	}
	
	public Object updateUi(AppUiDocument document, String user) throws Exception{
		if(document.getId()!=null){
			return mongoDbAccessor.save(document, user);
		}
		return null;
	}
	
	private List<Map<String,Object>> getAppUiList(List<AppUiDocument> uiList) throws Exception{
		List<Map<String,Object>> appUiList = new ArrayList<Map<String,Object>>();
		
		
		ManagerLogger manageLog = new ManagerLogger();
		manageLog.logStartTime();
		
		errorTemplate = "";
		for(AppUiDocument document : uiList){
			appUiList.add(getAppUi(document));
		}
		
		manageLog.logTotalTime("loop getAppUi", Thread.currentThread());
		
		
		if(!StringUtil.isBlank(errorTemplate)) {
			throw new Exception(errorTemplate);
		}
		
		return appUiList;
	}
	
	private Map<String,Object> getAppUi(AppUiDocument document) throws Exception{

		Map<String,Object> appUi = new HashMap<String,Object>();
		appUi.put(DmaConstants.AppUI.Key.UI_NAME, document.getUiName());
		appUi.put(DmaConstants.AppUI.Key.REMARK, document.getRemark());
		appUi.put(DmaConstants.AppUI.Key.LAST_UPDATE, document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		appUi.put(DmaConstants.AppUI.Key.LAST_PUBLISH, document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
		appUi.put(DmaConstants.AppUI.Key.STATUS, document.getStatus());
		
		/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
//		appUi.put(DmaConstants.AppUI.Key.URL_SCREEN_SHOT, document.getUrlScreenShot());
		
		/* 29/05/2562 add by akanit : {{$get._dma_param_internal_domain}} */
		String urlScreenShot = new DMADomainProcessor().bindingInternalDomain(document.getUrlScreenShot());
		appUi.put(DmaConstants.AppUI.Key.URL_SCREEN_SHOT, urlScreenShot);
		
		/* 14/03/2562 (140325621133) add by akanit : get version of screen */
		appUi.put(DmaConstants.AppUI.Key.VERSION, new GetReferenceProcessor().getVersion(document.getReference()));
		
		// get inUsedByStoryBoard.
		appUi.put(DmaConstants.AppUI.Key.STORY_BOARD, new GetReferenceProcessor().getInUsedByDmaStroyBoard(document.getReference()));
		
		/* 04/04/2562 (040425621211) add by akanit : DMA.5.2 : get TemplateRequired of screen */
		appUi.put(DmaConstants.AppUI.Key.TEMPLATE, new GetReferenceProcessor().getDmaTemplateRequired(document.getReference()));
		
		
		// get template properties and add to screen
		
		//String templateName = (String)document.getUiConfigData().get("template");
		//DMATemplate templateDoc = templateAccessor.findTemplateByName(templateName);
		List<Map<String, Object>> inputList = document.getInputList()==null?new ArrayList<Map<String, Object>>():document.getInputList();
		List<Map<String, Object>> dynamicInputList = document.getDynamicInputList()==null?new ArrayList<Map<String, Object>>():document.getDynamicInputList();
		List<Map<String, Object>> dynamicOutputList = document.getDynamicOutputList()==null?new ArrayList<Map<String, Object>>():document.getDynamicOutputList();
		List<Map<String, Object>> globalInputList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> globalOutputList = new ArrayList<Map<String, Object>>();

		for(Map<String, Object> inputMap : inputList) {
			Map<String, Object> globalInputMap = new HashMap<String, Object>();
			Map<String, Object> globalOutputMap = new HashMap<String, Object>();
			
			globalInputMap.put(DmaConstants.AppUI.Key.INPUT_NAME, inputMap.get(DmaConstants.AppUI.Key.INPUT_NAME));
			globalInputMap.put(DmaConstants.AppUI.Key.FIELD_POSITION, inputMap.get(DmaConstants.AppUI.Key.FIELD_POSITION));
			globalInputMap.put(DmaConstants.AppUI.Key.FIELD_TYPE, inputMap.get(DmaConstants.AppUI.Key.FIELD_TYPE));
			globalInputMap.put(DmaConstants.AppUI.Key.PATH, inputMap.get(DmaConstants.AppUI.Key.PATH));
			
			globalInputList.add(globalInputMap);
			
			globalOutputMap.put(DmaConstants.AppUI.Key.OUTPUT_NAME, inputMap.get(DmaConstants.AppUI.Key.INPUT_NAME));
			globalOutputMap.put(DmaConstants.AppUI.Key.FIELD_TYPE, inputMap.get(DmaConstants.AppUI.Key.FIELD_TYPE));		
			
			globalOutputList.add(globalOutputMap);
		}
		for(Map<String, Object> inputMap : dynamicInputList) {
			Map<String, Object> globalInputMap = new HashMap<String, Object>();
			Map<String, Object> globalOutputMap = new HashMap<String, Object>();
			
			globalInputMap.put(DmaConstants.AppUI.Key.INPUT_NAME, inputMap.get(DmaConstants.AppUI.Key.NAME));
			globalInputMap.put(DmaConstants.AppUI.Key.FIELD_POSITION, DmaConstants.AppUI.Value.API);
			globalInputMap.put(DmaConstants.AppUI.Key.FIELD_TYPE, inputMap.get(DmaConstants.AppUI.Key.TYPE));
			globalInputMap.put(DmaConstants.AppUI.Key.PATH, inputMap.get(DmaConstants.AppUI.Key.PATH));
			globalInputMap.put(DmaConstants.StoryBoard.Key.GlobalInputList.LOCAL_PATH, inputMap.get(DmaConstants.StoryBoard.Key.GlobalInputList.LOCAL_PATH));
			
			globalInputList.add(globalInputMap);
			
			
			if(!DmaConstants.AppUI.Value.DYNAMIC_LIST.equals(document.getTemplateType())){
							
				globalOutputMap.put(DmaConstants.AppUI.Key.OUTPUT_NAME, inputMap.get(DmaConstants.AppUI.Key.NAME));
				globalOutputMap.put(DmaConstants.AppUI.Key.FIELD_TYPE, inputMap.get(DmaConstants.AppUI.Key.TYPE));	
				globalOutputMap.put(DmaConstants.AppUI.Key.DATA_STRUCTURE, inputMap.get(DmaConstants.AppUI.Key.DATA_STRUCTURE));
				
				globalOutputList.add(globalOutputMap);
			}
		}
		for(Map<String, Object> outputMap : dynamicOutputList) {
			Map<String, Object> globalOutputMap = new HashMap<String, Object>();
			
			globalOutputMap.put(DmaConstants.AppUI.Key.OUTPUT_NAME, outputMap.get(DmaConstants.AppUI.Key.NAME));
			globalOutputMap.put(DmaConstants.AppUI.Key.FIELD_TYPE, outputMap.get(DmaConstants.AppUI.Key.FIELD_TYPE));	
			globalOutputMap.put(DmaConstants.AppUI.Key.DATA_STRUCTURE, outputMap.get(DmaConstants.AppUI.Key.DATA_STRUCTURE));
			globalOutputMap.put(DmaConstants.AppUI.Key.PATH, outputMap.get(DmaConstants.AppUI.Key.PATH));
			
			globalOutputList.add(globalOutputMap);
		}

		
		appUi.put(DmaConstants.AppUI.Key.GLOBAL_INPUT_LIST, globalInputList);
		appUi.put(DmaConstants.AppUI.Key.GLOBAL_OUTPUT_LIST, globalOutputList);
		appUi.put(DmaConstants.AppUI.Key.ACTION_LIST, document.getActionList());

		/* edit by akanit 01/10/2561
		 * move code to method 'needUpgrade'
		 *  */
		appUi.put(DmaConstants.AppUI.Key.NEED_UPGRADE, needUpgrade(document));
		
		return appUi;
	}

	
	/* 14/03/2562 edit by Akanit */
	public boolean needUpgrade(AppUiDocument document) throws Exception {
		
		String screenName = document.getUiName();
		
		List<Map<String, Object>> dmaTemplateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(document.getReference());
		if( dmaTemplateRequiredList==null || dmaTemplateRequiredList.size()==0 ){
			throw new Exception("screen '"+ screenName +"' not found templateRequired, Please save this screen again.");
		}
		
		Map<String, Object> dmaTemplateRequired = dmaTemplateRequiredList.get(0);
		
		String templateName = ManagerMap.getString(dmaTemplateRequired, ReferenceConstants.DMA.NAME);
		String templateVersion = ManagerMap.getString(dmaTemplateRequired, ReferenceConstants.DMA.VERSION, "1.0.0");
		String templateNewVersion = ManagerMap.getString(dmaTemplateRequired, ReferenceConstants.DMA.CURRENT_VERSION);
		
		if(templateNewVersion==null){
			
			templateNewVersion = getVersionTemplateFromDB(screenName, templateName);
			
			
			/* update newVersion of templateRequired */
			dmaTemplateRequired.put(ReferenceConstants.DMA.CURRENT_VERSION, templateNewVersion);
			mongoDbAccessor.save(document, "autoLoadVersionTemplate");
			
			logger.info("update Reference 'currentVersion' in 'dmaTemplateRequired' in Screen '"+ screenName +"' success.");
		}
		
		
		if(compareVersion(templateVersion, templateNewVersion) == -1){
			return true;
		}else {
			return false;
		}
		
	}

	private String getVersionTemplateFromDB(String screenName, String templateName) throws Exception {
		
		DMATemplate dmaTemplate = templateAccessor.findTemplateByName(templateName);
		
		if(dmaTemplate==null){
		    
		    String errorMessage = "screen '" + screenName + "' not found template '" + templateName + "'";
		    logger.error(errorMessage);
		    
		    if(StringUtil.isBlank(errorTemplate)) {
		        errorTemplate += errorMessage;
		    }else {
		        errorTemplate += ", <br>" + errorMessage;
		    }
		    return "";
		    
		}else {
			
			return dmaTemplate.getVersionTemplate();
		}
	}
	
	/**
	 * Compares two version strings. (Code from internet)
	 * 
	 * Use this instead of String.compareTo() for a non-lexicographical 
	 * comparison that works for version strings. e.g. "1.10".compareTo("1.6").
	 * 
	 * @note It does not work if "1.10" is supposed to be equal to "1.10.0".
	 * 
	 * @param str1 a string of ordinal numbers separated by decimal points. 
	 * @param str2 a string of ordinal numbers separated by decimal points.
	 * @return The result is a negative integer if str1 is _numerically_ less than str2. 
	 *         The result is a positive integer if str1 is _numerically_ greater than str2. 
	 *         The result is zero if the strings are _numerically_ equal.
	 */
	public static int compareVersion(String str1, String str2) {
		String[] vals1 = str1.split("\\.");
	    String[] vals2 = str2.split("\\.");
	    int i = 0;
	    // set index to first non-equal ordinal or length of shortest version string
	    while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
	      i++;
	    }
	    // compare first non-equal ordinal number
	    if (i < vals1.length && i < vals2.length) {
	        int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
	        return Integer.signum(diff);
	    }
	    // the strings are equal or one string is a substring of the other
	    // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
	    return Integer.signum(vals1.length - vals2.length);
	}
	
	@Deprecated
	private List<Map<String,Object>> getAppUiListWithAction(List<AppUiDocument> uiList) throws Exception {
		List<Map<String,Object>> appUiList = new ArrayList<Map<String,Object>>();
		errorTemplate = "";
		for(AppUiDocument document : uiList){
			Map<String,Object> appUi = getAppUi(document);
			DocumentContext masterCtx = JsonPath.parse(document.getUiJson());
			List<Map<String, Object>> actionList = new ArrayList<Map<String, Object>>();
			try{
				List<Map<String, Object>> actionsMap = masterCtx.read("$.$jason.head.actions.private_extra.actions");
				
				for ( Map<String, Object> action : actionsMap) {
					if(!"$load".equals(action.get(DmaConstants.AppUI.Key.NAME))){
				    	Map<String, Object> actionName = new HashMap<String, Object>();
						actionName.put(DmaConstants.AppUI.Key.ACTION_NAME, action.get(DmaConstants.AppUI.Key.NAME));
						actionList.add(actionName);
				    }
				}
			}catch(Exception e){
				// This template do not have actions
			}
			
			if(actionList.size()>0){
				appUi.put(DmaConstants.AppUI.Key.ACTION_LIST, actionList);
				appUiList.add(appUi);
			}
		}
		return appUiList;
	}
	
}
