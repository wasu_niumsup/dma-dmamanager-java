package com.jjpa.dma.manager.processor.storyboard;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.binding.BindingDomainProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByNameStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(FindByNameStoryBoardProcessor.class);
		
	protected StoryBoardAccessor storyBoardAccessor;
	
	public FindByNameStoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByNameStoryBoardProcessor");
		logger.debug("Process ===> FindByNameStoryBoardProcessor : {}", input.toJsonString());
		
		String storyName = (String)input.getValue(DmaConstants.StoryBoard.Key.STORY_NAME);
		
		StoryBoardDocument storyBoardDoc = storyBoardAccessor.findStoryByName(storyName);
		
		
		storyBoardAccessor.bindingNeedUpgradeStoryBoard(storyBoardDoc);
		
		
		/* 29/05/2562 add by Akanit : {{$get._dma_param_internal_domain}} */
		for(Map<String, Object> group : storyBoardDoc.getGroups()){
			new BindingDomainProcessor().bindInternalDomainToImageUrlInGroup(group);
		}
		
		/**
		 * Tanawat W.
		 * 2018-12-18
		 * DMA5
		 */
		Map<String,Object> output = storyBoardAccessor.bindingNeedUpgradeScreen(storyBoardDoc);
//		output.remove("refference");
		output.remove("screenList");
		logger.info("[DMA5] Improve performance : remove trash data");

		return output;
	}

}
