package com.jjpa.dma.manager.processor.binding;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;

import blueprint.util.StringUtil;

/**
 * BindingBaseConfiguration
 */
@SuppressWarnings({ "unused", "unchecked" })
public class BindingBaseConfiguration {

    private final Logger logger = LoggerFactory.getLogger(BindingBaseConfiguration.class);

    public Map<String, Object> process(Map<String, Object> uiJson, String entityName, String publishVersion,
            String storyBoardName) throws Exception {

        DocumentContext ctx = JsonPath.parse(uiJson);
        Map<String, Object> firstSet = ctx.read("$.$jason.head.actions.$load");

        if ("$set".equalsIgnoreCase(DataUtils.getStringValue(firstSet, "type"))) {
            Map<String, Object> options = (Map<String, Object>) firstSet.get("options");

            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DOMAIN),
                    ConfigurationUtils.getPublishUrl());

            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.ONLY_DOMAIN),
                    ConfigurationUtils.getPublishUrl().replace("https://", "").replace("http://", ""));

            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.APPLICATION), entityName);

            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.STORYBOARD), storyBoardName);

            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DMAMANAGER_VERSION),
                    ConfigurationUtils.getEngineVersion());
            options.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.PUBLISH_VERSION),
                    StringUtil.isBlank(publishVersion) ? "0.0.0" : publishVersion);
        }

        return ctx.json();
    }

}