package com.jjpa.dma.manager.processor.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

public class ListThemeEntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListThemeEntityProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListThemeEntityProcessor");
		logger.debug("Process ===> ListThemeEntityProcessor : {}", input.toJsonString());
		
		return listTheme();
		
	}
	
	private Map<String, Object> listTheme() throws Exception {
		Map<String, Object> themeResponse = new HashMap<String, Object>();
		List<Map<String, Object>> themeList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.THEME){
			Map<String, Object> theme = new HashMap<String, Object>();
			theme.put("themeName", s);
			themeList.add(theme);
		}
		themeResponse.put("themeList", themeList);
		return themeResponse;
	}

}
