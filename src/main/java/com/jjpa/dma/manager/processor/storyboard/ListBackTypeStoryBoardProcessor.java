package com.jjpa.dma.manager.processor.storyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

public class ListBackTypeStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListBackTypeStoryBoardProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListBackTypeBoardProcessor");
		logger.debug("Process ===> ListBackTypeBoardProcessor : {}", input.toJsonString());
		
		return listBackType();
		
	}

	private Map<String, Object> listBackType() throws Exception {
		Map<String, Object> backTypeResponse = new HashMap<String, Object>();
		List<Map<String, Object>> backTypeList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.BACK_TYPE){
			Map<String, Object> backMap = new HashMap<String, Object>();
			backMap.put("backType", s);
			backTypeList.add(backMap);
		}
		backTypeResponse.put("backTypeList", backTypeList);
		return backTypeResponse;
	}
	
}
