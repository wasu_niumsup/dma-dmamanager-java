package com.jjpa.dma.manager.processor.actionchain;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

@SuppressWarnings("unchecked")
public class SaveActionChainProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SaveActionChainProcessor.class);
		
	protected ActionChainAccessor actionChainAccessor;
	
	public SaveActionChainProcessor() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveActionChainProcessor");
		logger.debug("Process ===> SaveActionChainProcessor : {}", input.toJsonString());
		
		return saveActionChain(input);
		
	}
	
	private Object saveActionChain(ServiceData input) throws Exception {
		
		String user = (String)input.getValue("user");
		String actionChainCode = (String)input.getValue("actionChainCode");
		
		Map<String, Object> templateMap = JsonPath.parse(actionChainCode).json();
		
		logger.info("Process ===> SaveActionChainProcessor templateMap : "+ templateMap);

		DMAActionChainDocument actionChain = new DMAActionChainDocument();
		actionChain.setActionChainType((String)templateMap.get("actionChainType"));
		actionChain.setClassName((String)templateMap.get("className"));
		actionChain.setVersion((String)templateMap.get("version"));
		actionChain.setBinding((List<Map<String,Object>>)templateMap.get("binding"));
		DocumentContext ctx = JsonPath.parse(templateMap.get("nativeJson"));
		String nativeJson = (String)ctx.jsonString();
		Map<String,String> data = new HashMap<String, String>();
		data.put("data", nativeJson);
		actionChain.setNativeJson(data);
		
		/* 16/05/2561 add by Akanit new lastUpdate*/
		actionChain.setLastUpdate(new Date());
		
		return actionChainAccessor.saveActionChain(actionChain, user);
	}

}
