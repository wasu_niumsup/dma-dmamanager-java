package com.jjpa.dma.manager.processor.authen;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.UserModel;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import com.jjpa.web.servlet.UserSession;

public class GetLoginUserAuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(GetLoginUserAuthenProcessor.class);
	
	private UserSession getUserSession(){
		return SpringApplicationContext.getBean("UserSession");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> GetLoginUserAuthenProcessor");
		logger.debug("Process ===> GetLoginUserAuthenProcessor : {}", input.toJsonString());
		
		return getLoginUser(input);
		
	}
	
	public Map<String, Object> getLoginUser(ServiceData input) throws Exception {
		Map<String, Object> output = null;
		UserSession session = getUserSession();
		UserModel model = session.getUser();
		if(model != null){
			output = new HashMap<>();
			output.put("username", model.getUsername());
		}
		return output;
	}
	
}
