package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;

public interface ActionChainTemplateAccessor {
	
	public Object saveActionChainTemplate(DMAActionChainTemplate actionChainTemplate, String user) throws Exception;
	public DMAActionChainTemplate findActionChainTemplateByType(String type) throws Exception;
	public List<DMAActionChainTemplate> listActionChainTemplate() throws Exception;
	public Map<String,Object> listActionChainTemplateType() throws Exception;
	public void deleteActionChainTemplate(ServiceData input) throws Exception;
	
}
