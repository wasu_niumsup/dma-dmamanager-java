package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.db.mongodb.document.dma.manager.DMAComponent;

public interface ComponentAccessor {

	public Object saveComponent(DMAComponent component, String user) throws Exception;
	public DMAComponent findComponent(String componentName) throws Exception;
	public Map<String, Object> listComponentName() throws Exception;
	public List<DMAComponent> listComponents() throws Exception;
	public void deleteComponent(DMAComponent component, String user) throws Exception;
	
}
