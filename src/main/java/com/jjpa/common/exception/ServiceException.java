package com.jjpa.common.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.jjpa.common.ResponseCode;

public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	private final List contextInfo;
	private ResponseCode errorCode;

	public ServiceException() {
		this(ResponseCode.DATA_NOT_FOUND.getDefaultMessage());
	}

	@SuppressWarnings("rawtypes")
	public ServiceException(String s) {
		super(s);
		contextInfo = new ArrayList();		
	}

	@SuppressWarnings("rawtypes")
	public ServiceException(Throwable throwable) {
		super(toString(throwable), cause(throwable));
		contextInfo = new ArrayList();		
	}
	
	@SuppressWarnings("rawtypes")
	public ServiceException(String s, Throwable throwable) {
		super(s, cause(throwable));
		contextInfo = new ArrayList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceException(String s, Throwable throwable, List list) {
		super(s, cause(throwable));
		contextInfo = new ArrayList();
		if (list != null) {
			contextInfo.addAll(list);
		}
	}

	private static String toString(Throwable throwable) {
		if (throwable != null && throwable.getClass() == (ServiceException.class)) {
			if (throwable.getCause() != null) {
				String s = throwable.getCause().getMessage();
				if (s != null)
					return s;
				else
					return throwable.getCause().toString();
			}
			String s1 = throwable.getMessage();
			if (s1 != null)
				return s1;
			else
				return throwable.toString();
		}
		if (throwable != null)
			return throwable.toString();
		else
			return null;
	}

	private static Throwable cause(Throwable throwable) {
		if (throwable != null) {
			if (throwable.getClass() == (ServiceException.class)) {
				if (throwable.getCause() != null) {
					return throwable.getCause();
				} else {
					return throwable;
				}
			}
		}
		return throwable;
	}



	@SuppressWarnings("unchecked")
	public void addServiceContext(String s) {
		contextInfo.add(s);
	}

	public String toString() {
		Throwable throwable = getCause();
		String s = getClass().getName();
		String s1 = getLocalizedMessage();
		if (throwable != null) {
			if (s1 == null)
				return s + ": caused by: " + throwable.toString();
			String s2 = throwable.getLocalizedMessage();
			if (s1.equals(s2))
				return s + ": caused by: " + throwable.toString();
			else
				return s + ": " + s1 + ": caused by: " + throwable.toString();
		} else {
			return s1 == null ? s : s + ": " + s1;
		}
	}

	@SuppressWarnings("rawtypes")
	public void printStackTrace(PrintStream printstream) {
		Throwable throwable = getCause();
		if (throwable != null)
			synchronized (printstream) {
				throwable.printStackTrace(printstream);
				if (!contextInfo.isEmpty()) {
					printstream.println("SCA context: ");
					for (Iterator iterator = contextInfo.iterator(); iterator.hasNext(); printstream.println(iterator.next()))
						;
					printstream.println();
				}
				printstream.println("Wrapped by:");
				super.printStackTrace(printstream);
				printstream.println();
			}
		else
			synchronized (printstream) {
				super.printStackTrace(printstream);
				if (!contextInfo.isEmpty()) {
					printstream.println("SCA context: ");
					for (Iterator iterator1 = contextInfo.iterator(); iterator1.hasNext(); printstream.println(iterator1.next()))
						;
					printstream.println();
				}
				printstream.println();
			}
	}

	@SuppressWarnings("rawtypes")
	public void printStackTrace(PrintWriter printwriter) {
		Throwable throwable = getCause();
		if (throwable != null)
			synchronized (printwriter) {
				throwable.printStackTrace(printwriter);
				if (!contextInfo.isEmpty()) {
					printwriter.println();
					printwriter.println("SCA context: ");
					for (Iterator iterator = contextInfo.iterator(); iterator.hasNext(); printwriter.println(iterator.next()))
						;
					printwriter.println();
				}
				printwriter.println("Wrapped by:");
				super.printStackTrace(printwriter);
				printwriter.println();
			}
		else
			synchronized (printwriter) {
				super.printStackTrace(printwriter);
				if (!contextInfo.isEmpty()) {
					printwriter.println();
					printwriter.println("SCA context: ");
					for (Iterator iterator1 = contextInfo.iterator(); iterator1.hasNext(); printwriter.println(iterator1.next()))
						;
					printwriter.println();
				}
				printwriter.println();
			}
	}

	@Deprecated
	public String getMessageCode() {
		ResponseCode code = getErrorCode();
		return code.getCode();
	}

	@Deprecated
	public void setMessageCode(String messageCode) {
		errorCode = ResponseCode.getResponseCodeByString(messageCode);
	}

	public void setErrorCode(ResponseCode errorCode) {
		this.errorCode = errorCode;
	}

	public ResponseCode getErrorCode() {
		if (errorCode == null) {
			errorCode = ResponseCode.DATA_NOT_FOUND;
		}
		return errorCode;
	}

}
