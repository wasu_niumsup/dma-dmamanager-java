package servicegateway.utils;

import java.util.Map;

public class ManagerMap {

	private static boolean validateMap(Map<String, Object> dataMap, String key) throws Exception {
		if(dataMap==null || dataMap.size()==0){
			return false;
		}
		if(dataMap.get(key)==null){
			return false;
		}
		
		Object value = dataMap.get(key);
		if(value==null){
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T get(Map<String, Object> dataMap, String key, Class<?> cls) throws Exception {
		if(dataMap==null || dataMap.size()==0){
			return null;
		}
		
		Object result = null;
		try{
			result = cls.cast(dataMap.get(key));	
		}catch (Exception e){
			
		}
		return (T) result;
	}

	public static Boolean getBoolean(Map<String, Object> dataMap, String key) throws Exception {
		return getBoolean(dataMap, key, false);
	}

	public static Boolean getBoolean(Map<String, Object> dataMap, String key, Boolean defaultValue) throws Exception {
		try {
			if(validateMap(dataMap, key)){
				return Boolean.parseBoolean(String.valueOf(dataMap.get(key)));
			}else {
				return defaultValue;
			}
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	public static Integer getIntger(Map<String, Object> dataMap, String key) throws Exception {
		return getIntger(dataMap, key, null);
	}

	public static Integer getIntger(Map<String, Object> dataMap, String key, Integer defaultValue) throws Exception {
		try {
			if(validateMap(dataMap, key)){
				return Integer.parseInt(String.valueOf(dataMap.get(key)));
			}else {
				return defaultValue;
			}	
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	public static Long getLong(Map<String, Object> dataMap, String key) throws Exception {
		return getLong(dataMap, key, null);
	}

	public static Long getLong(Map<String, Object> dataMap, String key, Long defaultValue) throws Exception {
		try {
			if(validateMap(dataMap, key)){
				return Long.parseLong(String.valueOf(dataMap.get(key)));
			}else {
				return defaultValue;
			}
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	public static Double getDouble(Map<String, Object> dataMap, String key) throws Exception {
		return getDouble(dataMap, key, null);
	}

	public static Double getDouble(Map<String, Object> dataMap, String key, Double defaultValue) throws Exception {
		try {
			if(validateMap(dataMap, key)){
				return Double.parseDouble(String.valueOf(dataMap.get(key)));
			}else {
				return defaultValue;
			}
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	public static String getString(Map<String, Object> dataMap, String key) throws Exception {
		return getString(dataMap, key, null);
	}
	
	public static String getString(Map<String, Object> dataMap, String key, String defaultValue) throws Exception {
		try {
			if(validateMap(dataMap, key)){	
				return String.valueOf(dataMap.get(key));
			}else {
				return defaultValue;
			}
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	
	public static void typeCastingBooleanToString(Map<String, Object> dataMap) throws Exception {
		
		for(String key : dataMap.keySet()){
			if(dataMap.get(key) instanceof Boolean){
				dataMap.put(key, getString(dataMap, key));
			}
		}
	}
	
}
