package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.jjpa.db.mongodb.document.BaseDocument;

public class BaseDMADocument extends BaseDocument{
	
	private static final long serialVersionUID = 1L;
	
	private Map<String,String> nativeJson;
	private List<Map<String,Object>> binding;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdate;
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Map<String, String> getNativeJson() {
		return nativeJson;
	}
	public void setNativeJson(Map<String, String> nativeJson) {
		this.nativeJson = nativeJson;
	}
	public List<Map<String, Object>> getBinding() {
		return binding;
	}
	public void setBinding(List<Map<String, Object>> binding) {
		this.binding = binding;
	}

}
