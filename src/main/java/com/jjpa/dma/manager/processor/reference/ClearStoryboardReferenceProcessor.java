package com.jjpa.dma.manager.processor.reference;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.spring.SpringApplicationContext;

/* 
 * 12/06/2561 create by Akanit : for DMA5 
 * */
public class ClearStoryboardReferenceProcessor {

	private static final Logger logger = LoggerFactory.getLogger(ClearStoryboardReferenceProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public ClearStoryboardReferenceProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	public void process(StoryBoardDocument storyBoardDoc, List<String> screenList, String user) throws Exception {
		logger.info("process : ClearStoryboardReferenceProcessor.");
		
		AppUiDocument appUiDoc;
		for(String screenName : screenList){
			
			appUiDoc = appUiAccessor.findAppUiByName(screenName);
			
			
			/* Reference */
			
			/* clear inUsed reference to appUI(screen) */
			if(appUiDoc!=null){
				new DeleteReferenceProcessor().clearInUsed(appUiDoc.getReference());
				appUiAccessor.saveUi(appUiDoc, user);
			}
			
			/* clear required screen reference to storyBoard */
			new DeleteReferenceProcessor().clearRequiredDmaScreen(storyBoardDoc.getReference(), screenName);
			
			
			/* clear required template reference to storyBoard */
			String templateName = new GetReferenceProcessor().getDmaTemplateRequired(appUiDoc.getReference());
			new DeleteReferenceProcessor().clearRequiredDmaTemplate(storyBoardDoc.getReference(), templateName);
			
			
			/* clear required serviceGateway reference to storyBoard */
			String serviceGatewayName = new GetReferenceProcessor().getSgServiceRequired(appUiDoc.getReference());
			new DeleteReferenceProcessor().clearRequiredSgService(storyBoardDoc.getReference(), serviceGatewayName);

		}
		
	}
	
}
