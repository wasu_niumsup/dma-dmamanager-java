package com.jjpa.dma.manager.processor.template;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;

/* 
 * 07/06/2561 add by Akanit : validate template inUsed by other screen?
 * 
 * */
public class DeleteTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteTemplateProcessor.class);
	
	protected TemplateAccessor templateAccessor;
	
	public DeleteTemplateProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> DeleteTemplateProcessor");
		logger.debug("Process ===> DeleteTemplateProcessor : {}", input.toJsonString());
		
		Map<String,Object> output = new LinkedHashMap<>();
		
		
		/* get parameter from UI */
		List<Map<String, Object>> templateList = (List<Map<String, Object>>)input.getValue(DmaConstants.Template.Key.TEMPLATE_LIST);
		
		
		/* validate template inUsed by other screen. */
		Map<String, Object> errorDetail = validateTemplateInUsedByScreen(templateList);
		
		
		/* 
		 * mapping output return to UI 
		 * */
		if(errorDetail!=null){
			
			String errorMessage = ManagerMap.getString(errorDetail, DmaConstants.Template.Key.ERROR_MESSAGE);
//			List<Map<String, Object>> errorList = ManagerMap.get(errorDetail, DmaConstants.Template.Key.ERROR_LIST, List.class);
//			
//			output.put(DmaManagerConstant.RESULT_DATA, errorList);
//			output.put(DmaManagerConstant.RESULT_MESSAGE, errorMessage);
//			output.put(DmaManagerConstant.RESULT_SUCCESS, false);
			
			throw new Exception(errorMessage);
			
		}else {
			templateAccessor.deleteTemplate(input);
			
			output = templateAccessor.listTemplateName();
			output.put(DmaManagerConstant.RESULT_SUCCESS, true);
		}
		
		
		logger.info("output= {}" , output);
		return output;
	}

	
	/* 09/10/2561 add by Akanit */
	private Map<String,Object> validateTemplateInUsedByScreen(List<Map<String, Object>> templateList) throws Exception {
		
		String errorMessage = "";
		List<Map<String,Object>> errorList = new ArrayList<>();
		
		
		for(Map<String, Object> templateMap : templateList){
			
			String templateName = ManagerMap.getString(templateMap, DmaConstants.Template.Key.TEMPLATE_NAME);
			
			DMATemplate templateDoc = templateAccessor.findTemplateByName(templateName);
			if(templateDoc==null){
				errorMessage = "template "+ templateName + "not found.";
				errorList.add(new LinkedHashMap<>());
			}
			
			Map<String,Object> errorDetail = validateTemplateInUsedByScreen(templateDoc);
			
			if(errorDetail!=null){
				errorMessage += ManagerMap.getString(errorDetail, DmaConstants.Template.Key.ERROR_MESSAGE) +"<br>";
				errorList.add(ManagerMap.get(errorDetail, DmaConstants.Template.Key.ERROR_MAP, Map.class));
			}
			
		}
		
		Map<String,Object> output;
		
		if(StringUtil.isBlank(errorMessage)){
			output = null;
		}else{
			output = new LinkedHashMap<>();
			output.put(DmaConstants.Template.Key.ERROR_LIST, errorList);
			output.put(DmaConstants.Template.Key.ERROR_MESSAGE, errorMessage);
		}
		
		return output;
	}
	
	
	/* 07/06/2561 add by Akanit */
	private Map<String,Object> validateTemplateInUsedByScreen(DMATemplate templateDoc) throws Exception {
		
		String errorMessage = "";
		Map<String, Object> errorMap = new LinkedHashMap<>();
		
		List<Map<String, Object>> inUsedByScreenList = new GetReferenceProcessor().getInUsedByDmaScreens(templateDoc.getReference());
		
		if(inUsedByScreenList!=null && inUsedByScreenList.size()!=0){
			
			
			/* create errorMap */
			errorMap.put(DmaConstants.Template.Key.TEMPLATE_NAME, templateDoc.getTemplateName());
			errorMap.put(DmaConstants.Template.Key.INUSED_BY_DMA_SCREEN, inUsedByScreenList);
			
			
			/* create errorMessage */
			errorMessage = "Template '"+ templateDoc.getTemplateName() + "' is used by screen '";
			
			String inUsedByScreenName;
			for(Map<String, Object> inUsedByScreen : inUsedByScreenList){
				
				inUsedByScreenName = ManagerMap.getString(inUsedByScreen, ReferenceConstants.DMA.NAME);
				
				errorMessage += inUsedByScreenName +", ";
			}
			errorMessage = errorMessage.substring(0, errorMessage.length()-2);
			errorMessage += "'";
			
		}
		
		
		Map<String,Object> output;
		
		if(StringUtil.isBlank(errorMessage)){
			output = null;
		}else{
			output = new LinkedHashMap<>();
			output.put(DmaConstants.Template.Key.ERROR_MAP, errorMap);
			output.put(DmaConstants.Template.Key.ERROR_MESSAGE, errorMessage);
		}
		
		return output;
	}

}
