package com.jjpa.dma.manager.processor.template;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class GetConfigTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetConfigTemplateProcessor.class);
		
	protected TemplateAccessor templateAccessor;
	
	public GetConfigTemplateProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> GetConfigTemplateProcessor");
		logger.debug("Process ===> GetConfigTemplateProcessor : {}", input.toJsonString());
		
		return getTemplateConfig(input);
		
	}
	
	private Map<String, Object> getTemplateConfig(ServiceData input) throws Exception{
		String templateName = (String)input.getValue("templateName");
		DMATemplate template = templateAccessor.findTemplateByName(templateName);
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("templateName", template.getTemplateName());
		output.put("infoUrl", template.getInfoUrl());
		output.put("binding", JsonPath.parse(template.getBinding()).jsonString());
		output.put("nativeJson", template.getNativeJson().get("data"));
		return output;
	}

}
