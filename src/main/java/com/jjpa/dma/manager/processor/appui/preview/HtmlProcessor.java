package com.jjpa.dma.manager.processor.appui.preview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

/**
 * PreviewProcessor
 */
public class HtmlProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private static final Logger logger = LoggerFactory.getLogger(HtmlProcessor.class);
    protected TemplateAccessor templateAccessor;


    
    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        logger.info("0. HtmlProcessor");
        logger.info("0. body = " + input.getData());
        Map<String,Object> d = input.getData();
        String key = d.get("key").toString(); // ./jboss/dd.html
        key = key.replace("AAA", "/");

        File file = new File(key);
        String html = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        logger.info("1. html : " + html.length());
        return html;
    }

    public static void writeFile(String path, String content) {
        FileOutputStream fop = null;
        File file;
        //content = "This is the text content";

        try {

            file = new File(path);
            fop = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes("UTF-8");

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}