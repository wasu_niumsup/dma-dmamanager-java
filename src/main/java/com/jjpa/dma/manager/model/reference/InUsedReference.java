package com.jjpa.dma.manager.model.reference;

import java.util.List;
import java.util.Map;

public class InUsedReference {

	private List<Map<String, Object>> inUsedByDmaScreen;
	private List<Map<String, Object>> inUsedByDmaStroyBoard;
	
	@SuppressWarnings("unused")
	private List<Map<String, Object>> inUsedByDmaEntiry;// this is old name
	private List<Map<String, Object>> inUsedByDmaEntity;
	
	public List<Map<String, Object>> getInUsedByDmaScreen() {
		return inUsedByDmaScreen;
	}
	public void setInUsedByDmaScreen(List<Map<String, Object>> inUsedByDmaScreen) {
		this.inUsedByDmaScreen = inUsedByDmaScreen;
	}
	public List<Map<String, Object>> getInUsedByDmaStroyBoard() {
		return inUsedByDmaStroyBoard;
	}
	public void setInUsedByDmaStroyBoard(List<Map<String, Object>> inUsedByDmaStroyBoard) {
		this.inUsedByDmaStroyBoard = inUsedByDmaStroyBoard;
	}
	public List<Map<String, Object>> getInUsedByDmaEntity() {
		return inUsedByDmaEntity;
	}
	public void setInUsedByDmaEntity(List<Map<String, Object>> inUsedByDmaEntity) {
		this.inUsedByDmaEntity = inUsedByDmaEntity;
	}
	
	
	/* old field */
	public List<Map<String, Object>> getInUsedByDmaEntiry() {
		return inUsedByDmaEntity; // return new field
	}
	/* old field */
	public void setInUsedByDmaEntiry(List<Map<String, Object>> inUsedByDmaEntiry) {
		this.inUsedByDmaEntity = inUsedByDmaEntiry; // cast field name from inUsedByDmaEntiry(old field) to inUsedByDmaEntity(new field)
	}
	
}
