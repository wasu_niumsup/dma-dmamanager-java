package com.jjpa.dma.manager.processor.reference.required.binding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

/* 
 * 16/03/2561 add by Akanit 
 * 28/02/2562 add by Akanit : this method for DMA5 :
 * */
public class BindingRequiredReferenceToStoryBoardProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingRequiredReferenceToStoryBoardProcessor.class);
	
	/* 28/02/2562 add by Akanit : this method for DMA5 :  */
	public void process(StoryBoardDocument storyBoardDoc, Map<String, Object> storyBoard, AppUiDocument appUiDoc) throws Exception {
		logger.info("process : binding required reference to StoryBoard");
		
		bindingDmaScreenRequiredReference(storyBoardDoc, appUiDoc);
		
		// copy requiredReference of DmaScreen to storyBoard
		bindingDmaTemplateRequiredReference(storyBoardDoc, appUiDoc);
		
		/* 14/11/2562 comment by Akanit : DMA.5.9.0 : DMA-1739 : fix bug reference invalid when change sgwAPIdropdown in storyboard. */
//		bindingSgServiceRequiredReference(storyBoardDoc, storyBoard, appUiDoc);
		
		/* this method have binding ActionChain and ActionChainTemplate */
//		bindingActionChainTemplateRequiredReference(storyBoardDoc, actionChainList);
	}
	
//	public void process(StoryBoardDocument storyBoardDoc, Map<String, Object> storyBoard, AppUiDocument appUiDoc, List<Map<String, Object>> actionChainList) throws Exception {
//		logger.info("process : binding required reference to StoryBoard");
//		
//		bindingDmaScreenRequiredReference(storyBoardDoc, appUiDoc);
//		
//		// copy requiredReference of DmaScreen to storyBoard
//		bindingDmaTemplateRequiredReference(storyBoardDoc, appUiDoc);
//		bindingSgServiceRequiredReference(storyBoardDoc, storyBoard, appUiDoc);
//		
//		/* this method have binding ActionChain and ActionChainTemplate */
//		bindingActionChainTemplateRequiredReference(storyBoardDoc, actionChainList);
//	}

	public void bindingDmaScreenRequiredReference(StoryBoardDocument storyBoardDoc, AppUiDocument appUiDoc) throws Exception {
		
		if(!StringUtil.isBlank(appUiDoc.getUiName())){
			logger.info("set StoryBoard '{}' required Screen '{}'", storyBoardDoc.getStoryName(), appUiDoc.getUiName());
			
			/* get dmaScreenRequiredList of storyBoard */
			List<Map<String, Object>> dmaScreenRequiredList_storyBoard = new GetReferenceProcessor().getDmaScreenRequireds(storyBoardDoc.getReference());
			if(dmaScreenRequiredList_storyBoard==null){
				dmaScreenRequiredList_storyBoard = new ArrayList<>();
				storyBoardDoc.setReference(new InitialReferenceProcessor().process(storyBoardDoc.getReference()));
			}
			storyBoardDoc.getReference().getRequired().setDmaScreenRequired(dmaScreenRequiredList_storyBoard);
			
			
			Map<String, Object> dmaScreenRequired = new LinkedHashMap<>();
			dmaScreenRequired.put(ReferenceConstants.DMA.VERSION, new GetReferenceProcessor().getVersion(appUiDoc.getReference()));
			dmaScreenRequired.put(ReferenceConstants.DMA.NAME, appUiDoc.getUiName());
			dmaScreenRequired.put(ReferenceConstants.DMA.TYPE, appUiDoc.getTemplateType());
			
			dmaScreenRequiredList_storyBoard.add(dmaScreenRequired);
		}
		
	}

	public void bindingDmaTemplateRequiredReference(StoryBoardDocument storyBoardDoc, AppUiDocument appUiDoc) throws Exception {
		
		/* get dmaTemplateRequiredList of appUi */
		List<Map<String, Object>> dmaTemplateRequiredList_appUi = new GetReferenceProcessor().getDmaTemplateRequireds(appUiDoc.getReference());
		if( dmaTemplateRequiredList_appUi==null || dmaTemplateRequiredList_appUi.size()==0 ){
			return;
		}
		
		/* get dmaTemplateRequiredList of storyBoard */
		List<Map<String, Object>> dmaTemplateRequiredList_storyBoard = new GetReferenceProcessor().getDmaTemplateRequireds(storyBoardDoc.getReference());
		if( dmaTemplateRequiredList_storyBoard==null || dmaTemplateRequiredList_storyBoard.size()==0 ){
			dmaTemplateRequiredList_storyBoard = new ArrayList<>();
			storyBoardDoc.setReference(new InitialReferenceProcessor().process(storyBoardDoc.getReference()));
		}
		storyBoardDoc.getReference().getRequired().setDmaTemplateRequired(dmaTemplateRequiredList_storyBoard);
		
		
		/* add dmaTemplateRequired */
		Map<String, Object> dmaTemplateRequired;
		String templateVersion, templateName, templateType;
		
		for(Map<String, Object> dmaTemplateRequired_appUi : dmaTemplateRequiredList_appUi){
			
			/* get parameter */
			templateVersion = ManagerMap.getString(dmaTemplateRequired_appUi, ReferenceConstants.DMA.VERSION);
			templateName = ManagerMap.getString(dmaTemplateRequired_appUi, ReferenceConstants.DMA.NAME);
			templateType = ManagerMap.getString(dmaTemplateRequired_appUi, ReferenceConstants.DMA.TYPE);
			if(StringUtil.isBlank(templateName)){
				continue;
			}
			
			/* check have templateName? */
			Map<String, Object> rowTemplateName = ManagerArrayList.findRowBySearchString(dmaTemplateRequiredList_storyBoard, ReferenceConstants.DMA.NAME, templateName);
			if(rowTemplateName==null){
				logger.info("set StoryBoard '{}' required Template '{}'", storyBoardDoc.getStoryName(), templateName);
				
				dmaTemplateRequired = new LinkedHashMap<>();
				dmaTemplateRequired.put(ReferenceConstants.DMA.VERSION, templateVersion);
				dmaTemplateRequired.put(ReferenceConstants.DMA.NAME, templateName);
				dmaTemplateRequired.put(ReferenceConstants.DMA.TYPE, templateType);
				
				dmaTemplateRequiredList_storyBoard.add(dmaTemplateRequired);
			}
		}
		
	}
	
	public void bindingSgServiceRequiredReference(StoryBoardDocument storyBoardDoc, Map<String, Object> storyBoard, AppUiDocument appUiDoc) throws Exception {
		
		/* get sgServiceRequiredList from storyBoard : use for binding */
		List<Map<String, Object>> sgServiceRequiredList_storyBoard = new GetReferenceProcessor().getSgServiceRequireds(storyBoardDoc.getReference());
		if(sgServiceRequiredList_storyBoard==null){
			sgServiceRequiredList_storyBoard = new ArrayList<>();
			storyBoardDoc.setReference(new InitialReferenceProcessor().process(storyBoardDoc.getReference()));
		}
		storyBoardDoc.getReference().getRequired().setSgServiceRequired(sgServiceRequiredList_storyBoard);
		
		
		/* get sgServiceRequiredList from appUi reference : use for copy to storyBoard reference */
		List<Map<String, Object>> sgServiceRequiredList_appUi = new GetReferenceProcessor().getSgServiceRequireds(appUiDoc.getReference());
		if(sgServiceRequiredList_appUi==null){
			sgServiceRequiredList_appUi = new ArrayList<>();
		}
		
		
		/* get sgServiceRequiredList from actionList : use for copy to storyBoard reference */
		List<Map<String, Object>> sgServiceRequiredList_actionList = new ArrayList<>();
		try{
			List<String> apiNameList = JsonPath.read(storyBoard, "$.actionList[*].apiName");
			
			Map<String, Object> sgServiceRequired_actionList;
			for(String apiName : apiNameList){
				
				if("Do Nothing".equals(apiName)){
					continue;
				}
				
				sgServiceRequired_actionList = new LinkedHashMap<>();
				sgServiceRequired_actionList.put(ReferenceConstants.SG.SERVICE_NAME, apiName);
				
				sgServiceRequiredList_actionList.add(sgServiceRequired_actionList);
			}
			
		}catch(Exception e){}
		

		/* merge */
		List<Map<String, Object>> sgServiceRequiredList = ManagerArrayList.mergeByKey(sgServiceRequiredList_appUi, sgServiceRequiredList_actionList, ReferenceConstants.SG.SERVICE_NAME);
		
		
		/* add sgServiceRequired */
		Map<String, Object> rowServiceName, sgServiceRequired_storyBoard;
		String serviceName;
		
		for(Map<String, Object> sgServiceRequired : sgServiceRequiredList){
			
			/* get parameter */
//			serviceId = ManagerMap.getString(sgServiceRequired_appUi, ReferenceConstants.SG.UUID);
			serviceName = ManagerMap.getString(sgServiceRequired, ReferenceConstants.SG.SERVICE_NAME);
			if(StringUtil.isBlank(serviceName)){
				continue;		
			}
			
			/* check have serviceName? */
			rowServiceName = ManagerArrayList.findRowBySearchString(sgServiceRequiredList_storyBoard, ReferenceConstants.SG.SERVICE_NAME, serviceName);
			if(rowServiceName==null){
				logger.info("set StoryBoard '{}' required serviceGateway '{}'", storyBoardDoc.getStoryName(), serviceName);
				
				sgServiceRequired_storyBoard = new LinkedHashMap<>();
//				sgServiceRequired.put(ReferenceConstants.SG.UUID, serviceId);
				sgServiceRequired_storyBoard.put(ReferenceConstants.SG.SERVICE_NAME, serviceName);
				
				sgServiceRequiredList_storyBoard.add(sgServiceRequired_storyBoard);
			}
		}
		
	}
	
	public void bindingActionChainTemplateRequiredReference(StoryBoardDocument storyBoardDoc, List<Map<String, Object>> actionChainTemplateList) throws Exception {
		
		/* get actionChainRequiredList of storyBoard */
//		List<Map<String, Object>> actionChainRequiredList_storyBoard = new GetReferenceProcessor().getDmaActionChainRequireds(storyBoardDoc.getReference());
//		if(actionChainRequiredList_storyBoard==null){
//			actionChainRequiredList_storyBoard = new ArrayList<>();
//			storyBoardDoc.getReference().getRequired().setDmaActionChainRequired(actionChainRequiredList_storyBoard);
//		}
		
		/* get actionChainTemplateRequiredList of storyBoard */
		List<Map<String, Object>> actionChainTemplateRequiredList_storyBoard = new GetReferenceProcessor().getDmaActionChainTemplateRequireds(storyBoardDoc.getReference());
		if(actionChainTemplateRequiredList_storyBoard==null){
			actionChainTemplateRequiredList_storyBoard = new ArrayList<>();
		}
		storyBoardDoc.getReference().getRequired().setDmaActionChainTemplateRequired(actionChainTemplateRequiredList_storyBoard);
		
		
		String actionChainTemplateVersion, actionChainTemplateName;
		Reference reference_actionChainTemplate;
		Map<String, Object> rowActionChainTemplate, actionChainTemplateRequired;
		
		for(Map<String, Object> actionChainTemplate : actionChainTemplateList){
			
			/* get parameter */
			actionChainTemplateName = ManagerMap.getString(actionChainTemplate, DmaConstants.ActionChain.Reference.Key.NAME);
			actionChainTemplateVersion = ManagerMap.getString(actionChainTemplate, DmaConstants.ActionChain.Reference.Key.VERSION);
			reference_actionChainTemplate = ManagerMap.get(actionChainTemplate, DmaConstants.ActionChain.Reference.Key.REFERENCE, Reference.class);
			if(StringUtil.isBlank(actionChainTemplateName)){
				continue;
			}
			
			/* check have serviceName? */
			rowActionChainTemplate = ManagerArrayList.findRowBySearchString(actionChainTemplateRequiredList_storyBoard, ReferenceConstants.DMA.NAME, actionChainTemplateName);
			if(rowActionChainTemplate==null){
				logger.info("set StoryBoard '{}' required actionChainTemplate '{}'", storyBoardDoc.getStoryName(), actionChainTemplateName);
				
				actionChainTemplateRequired = new LinkedHashMap<>();
				actionChainTemplateRequired.put(ReferenceConstants.DMA.VERSION, actionChainTemplateVersion);
				actionChainTemplateRequired.put(ReferenceConstants.DMA.NAME, actionChainTemplateName);
				actionChainTemplateRequired.put(ReferenceConstants.DMA.TYPE, "");
				
				actionChainTemplateRequiredList_storyBoard.add(actionChainTemplateRequired);
			}

			bindingActionChainRequiredReference(storyBoardDoc, reference_actionChainTemplate);
		}
		
	}
	
	private void bindingActionChainRequiredReference(StoryBoardDocument storyBoardDoc, Reference reference_actionChainTemplate) throws Exception {
		
		/* get actionChainRequiredList of appUi */
		List<Map<String, Object>> actionChainRequiredList_actionChainTemplate = new GetReferenceProcessor().getDmaActionChainRequireds(reference_actionChainTemplate);
		if(actionChainRequiredList_actionChainTemplate==null){
			return;
		}
		
		/* get actionChainRequiredList of storyBoard */
		List<Map<String, Object>> actionChainRequiredList_storyBoard = new GetReferenceProcessor().getDmaActionChainRequireds(storyBoardDoc.getReference());
		if(actionChainRequiredList_storyBoard==null){
			actionChainRequiredList_storyBoard = new ArrayList<>();
		}
		storyBoardDoc.getReference().getRequired().setDmaActionChainRequired(actionChainRequiredList_storyBoard);
		
		
		String actionChainVersion, actionChainName;
		Map<String, Object> rowActionChain, actionChainRequired;
		
		for(Map<String, Object> actionChainRequired_actionChainTemplate : actionChainRequiredList_actionChainTemplate){
			
			/* get parameter */
			actionChainName = ManagerMap.getString(actionChainRequired_actionChainTemplate, ReferenceConstants.DMA.NAME);
			actionChainVersion = ManagerMap.getString(actionChainRequired_actionChainTemplate, ReferenceConstants.DMA.VERSION);
//			actionChainType = ManagerMap.getString(actionChainRequired_actionChainTemplate, ReferenceConstants.DMA.TYPE);
			if(StringUtil.isBlank(actionChainName)){
				continue;
			}
			
			/* check have serviceName? */
			rowActionChain = ManagerArrayList.findRowBySearchString(actionChainRequiredList_storyBoard, ReferenceConstants.DMA.NAME, actionChainName);
			if(rowActionChain==null){
				logger.info("set StoryBoard '{}' required actionChain '{}'", storyBoardDoc.getStoryName(), actionChainName);
				
				actionChainRequired = new LinkedHashMap<>();
				actionChainRequired.put(ReferenceConstants.DMA.VERSION, actionChainVersion);
				actionChainRequired.put(ReferenceConstants.DMA.NAME, actionChainName);
				actionChainRequired.put(ReferenceConstants.DMA.TYPE, "");
				
				actionChainRequiredList_storyBoard.add(actionChainRequired);
			}
		}
		
	}
}
