package servicegateway.config;

import java.util.List;
import java.util.Map;

import com.jjpa.common.APIsConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.SGConstants;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

public class ServiceGatewayConfig extends APIsConstants{
	
	private Boolean isJsonConfig_input, isJsonConfig_output;
	private Map<String, Object> rowJsonType_input, rowDataMap_input, rowDataList_input;
	private Map<String, Object> rowJsonType_output, rowDataMap_output, rowDataList_output;
	private List<Map<String, Object>> inputFieldList, inputFieldList_raw;
	private List<Map<String, Object>> outputFieldList, outputFieldList_raw;
	private List<Map<String, Object>> childDataList_input, childDataList_output;
	
	private Map<String, Object> jsonData;
	private String serviceName;
	private String externalApisType;
	private String wsdlLocation;
	private String serviceEndpoint;
	private String portTypeName;
	private String operationName;
	
	
	/* 
	 * constucter 
	 * */
	public ServiceGatewayConfig(Map<String, Object> response) throws Exception {
		
		if(response.containsKey(DmaManagerConstant.RESPONSE_HEADER)){

			Map<String, Object> responseHeader = ManagerMap.get(response, DmaManagerConstant.RESPONSE_HEADER, Map.class);
			
			if (!SGConstants.CODE_SUCCESS.equals(responseHeader.get(DmaManagerConstant.RESPONSE_CODE))) {
				throw new Exception((String) responseHeader.get(DmaManagerConstant.RESPONSE_DESC));
			} else {
				Map<String, Object> responseData = ManagerMap.get(response, DmaManagerConstant.RESPONSE_DATA, Map.class);
				this.jsonData = ManagerMap.get(responseData, SGConstants.Key.JSON_DATA, Map.class);
			}
			
		}else{
			if(response.containsKey(SGConstants.Key.JSON_DATA)){
				this.jsonData = ManagerMap.get(response, SGConstants.Key.JSON_DATA, Map.class);
			}else{
				this.jsonData = response;
			}
		}
		
	}
	
	
	/* get */
	public Map<String, Object> getJsonData() throws Exception {
		return jsonData;
	}
	public String getServiceName() throws Exception {
		if(serviceName==null){
			serviceName = ManagerMap.getString(jsonData, Key.SERVICE_NAME);
		}
		return serviceName;
	}
	public String getExternalApisType() throws Exception {
		if(externalApisType==null){
			externalApisType = ManagerMap.getString(jsonData, Key.EXTERNAL_APIS_TYPE);
		}
		return externalApisType;
	}
	public String getWsdlLocation() throws Exception {
		if(wsdlLocation==null){
			wsdlLocation = ManagerMap.getString(jsonData, Key.WSDL_LOCATION);
		}
		return wsdlLocation;
	}
	public String getServiceEndpoint() throws Exception {
		if(serviceEndpoint==null){
			serviceEndpoint = ManagerMap.getString(jsonData, Key.SERVICE_END_POINT);
		}
		return serviceEndpoint;
	}
	public String getPortTypeName() throws Exception {
		if(portTypeName==null){
			portTypeName = ManagerMap.getString(jsonData, Key.PORT_TYPE_NAME);
		}
		return portTypeName;
	}
	public String getOperationName() throws Exception {
		if(operationName==null){
			operationName = ManagerMap.getString(jsonData, Key.OPERATION_NAME);
		}
		return operationName;
	}
	
	
	
	public List<Map<String, Object>> getInputFieldList() throws Exception {
		if(inputFieldList==null){
			inputFieldList = ManagerMap.get(jsonData, Key.INPUT_FIELD_LIST, List.class);
		}
		return inputFieldList;
	}
	public List<Map<String, Object>> getOutputFieldList() throws Exception {
		if(outputFieldList==null){
			outputFieldList = ManagerMap.get(jsonData, Key.OUTPUT_FIELD_LIST, List.class);
		}
		return outputFieldList;
	}
	
	
	/* hard */
	public Map<String, Object> getRowJsonType_input() throws Exception {		
		if(rowJsonType_input==null){
			if(isJsonConfig_input==null){
				rowJsonType_input = ManagerArrayList.findRowBySearchString(getInputFieldList(), Key.FIELD_TYPE, Type.JSON);
				if(rowJsonType_input!=null){
					isJsonConfig_input = true;
				}else{
					isJsonConfig_input = false;
				}
			}
		}
		return rowJsonType_input;
	}
	public Map<String, Object> getRowJsonType_output() throws Exception {		
		if(rowJsonType_output==null){
			if(isJsonConfig_output==null){
				rowJsonType_output = ManagerArrayList.findRowBySearchString(getOutputFieldList(), Key.FIELD_TYPE, Type.JSON);
				if(rowJsonType_output!=null){
					isJsonConfig_output = true;
				}else{
					isJsonConfig_output = false;
				}
			}
		}
		return rowJsonType_output;
	}
	
	public List<Map<String, Object>> getInputFieldList_raw() throws Exception {
		if(inputFieldList_raw==null){
			if(isJsonConfig_input==null){
				getRowJsonType_input();
			}				
			/* case json type */
			if(isJsonConfig_input){			
				inputFieldList_raw = ManagerMap.get(rowJsonType_input, Key.JSON_LIST, List.class);
				
			}else {				
				inputFieldList_raw = getInputFieldList();
			}
		}
		return inputFieldList_raw;
	}
	
	public List<Map<String, Object>> getOutputFieldList_raw() throws Exception {
		if(outputFieldList_raw==null){
			if(isJsonConfig_output==null){
				getRowJsonType_output();
			}				
			/* case json type */
			if(isJsonConfig_output){		
				outputFieldList_raw = ManagerMap.get(rowJsonType_output, Key.JSON_LIST, List.class);
				
			}else {			
				outputFieldList_raw = getOutputFieldList();
			}
		}
		return outputFieldList_raw;
	}
	
	public Map<String, Object> getRowDataMap_input() throws Exception {
		if(rowDataMap_input==null){
			rowDataMap_input = ManagerArrayList.findRowBySearchString(getInputFieldList_raw(), Key.FIELD_TYPE, Type.DATA_MAP);
		}
		return rowDataMap_input;
	}
	
	public Map<String, Object> getRowDataList_input() throws Exception {
		if(rowDataList_input==null){
			rowDataList_input = ManagerArrayList.findRowBySearchString(getInputFieldList_raw(), Key.FIELD_TYPE, Type.DATA_LIST);
		}
		return rowDataList_input;
	}
	
	public Map<String, Object> getRowDataMap_output() throws Exception {
		if(rowDataMap_output==null){
			rowDataMap_output = ManagerArrayList.findRowBySearchString(getOutputFieldList_raw(), Key.FIELD_TYPE, Type.DATA_MAP);
		}
		return rowDataMap_output;
	}
	
	public Map<String, Object> getRowDataList_output() throws Exception {
		if(rowDataList_output==null){
			rowDataList_output = ManagerArrayList.findRowBySearchString(getOutputFieldList_raw(), Key.FIELD_TYPE, Type.DATA_LIST);
		}
		return rowDataList_output;
	}
	
	//childDataList_input
	public List<Map<String, Object>> getChildDataList_input() throws Exception {
		if(childDataList_input==null){
			if(isJsonConfig_input==null){
				getRowJsonType_input();
			}				
			/* case json type */
			if(isJsonConfig_input){
				childDataList_input =  getOutputFieldList_raw();
			}else {
				childDataList_input =  ManagerMap.get(getRowDataList_input(), Key.DATASET_LIST, List.class);
			}
		}
		return childDataList_input;
	}
	
	public List<Map<String, Object>> getChildDataList_output() throws Exception {
		if(childDataList_output==null){
			if(isJsonConfig_output==null){
				getRowJsonType_output();
			}				
			/* case json type */
			if(isJsonConfig_output){
				childDataList_output =  getOutputFieldList_raw();
			}else {
				childDataList_output =  ManagerMap.get(getRowDataList_output(), Key.DATASET_LIST, List.class);
			}
		}
		return childDataList_output;
	}
}
