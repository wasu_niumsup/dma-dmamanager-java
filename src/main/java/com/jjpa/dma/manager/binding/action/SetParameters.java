package com.jjpa.dma.manager.binding.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

import servicegateway.utils.ManagerMap;

public class SetParameters implements ActionBinding {

	private final Logger logger = LoggerFactory.getLogger(SetParameters.class);

	
	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) throws Exception {
		logger.info("Binding set parameter in Action");
		
		DocumentContext ctx = JsonPath.parse(document.getNativeJson().get("data"));

		
		/* 
		 * binding parameter from config 
		 * */
		Map<String, Object> config = ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.CONFIG, Map.class);
		if(config!=null){
			logger.info("#found configParameter then binding parameter.");
		
			Map<String, Object> parameter = generateParameter(config, eventMap);
			
			logger.info("binding NativeJson");
			logger.info("path={}", JasonPathConstants.NativeJson.Path.OPTIONS);
			logger.info("value={}", parameter);
			ctx.set(JasonPathConstants.NativeJson.Path.OPTIONS, parameter);
		}
		
		return ctx;
	}
	
	
	
	private Map<String, Object> generateParameter(Map<String, Object> config, Map<String, Object> eventMap) throws Exception {
		logger.info("#generateParameter");
		
		List<Map<String, Object>> parameterList = ManagerMap.get(config, DmaConstants.StoryBoard.Key.Config.PARAMETER, List.class);
		if(parameterList==null){
			String errorMessage = DmaConstants.StoryBoard.Key.Config.PARAMETER + " in config not found.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		
		String key, value, valueFrom;
		Map<String, Object> parameter = new LinkedHashMap<>();
		
		for(Map<String, Object> parameterMap : parameterList){
			
			
			/* get parameter of parameterMap */
			key = ManagerMap.getString(parameterMap, DmaConstants.StoryBoard.Key.Config.Parameter.OPTION);
			value = ManagerMap.getString(parameterMap, DmaConstants.StoryBoard.Key.Config.Parameter.VALUE);
			valueFrom = ManagerMap.getString(parameterMap, DmaConstants.StoryBoard.Key.Config.Parameter.VALUE_FROM);
			
			logger.info("#get value of parameter case : valueFrom = "+ valueFrom);
			
			
			/* check case of get parameter is not manual */
			if(!DmaConstants.StoryBoard.Value.MANUAL.equals(valueFrom)){
				
//				value = JasonNetteUtils.generateGlobalValue(value);
				value = JasonNetteUtils.generateResultValue(parameterMap);
			}
			logger.info("#value = "+ value);
			
			parameter.put(key, value);
		}
		
		return parameter;
	}
	
}
