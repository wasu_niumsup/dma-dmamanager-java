package com.jjpa.dma.manager.processor.patch;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jjpa.common.PatchConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.PublishEntityConfigAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.PublishEntityConfigDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.PatchProcessor;

import servicegateway.utils.ManagerMap;

/* 
 * 03/01/2562 add by Akanit : DMA 4.3 : ReStructureStoryBoard :  
 * */
@SuppressWarnings("unchecked")
public class Patch_4_3_0_0 extends CommonProcessor<ServiceData> implements PatchProcessor {

	private static Logger logger = Logger.getLogger(Patch_4_3_0_0.class);
	
	protected MongoDbAccessor mongoDbAccessor = getMongoDbAccessor();
	protected StoryBoardAccessor storyBoardAccessor = getStoryBoardAccessor();
	protected TempEntityConfigAccessor tempEntityConfigAccessor = getTempEntityConfigAccessor();
	protected PublishEntityConfigAccessor publishEntityConfigAccessor = getPublishEntityConfigAccessor();
	
	private String user = "PatchSystem";
	
	@Override
	public Object process(Map<String, Object> input) throws Exception {
		
		/* can edit (patch setting) */
		final String patchVers = "4.3.0.0";
		final String patchDesc = "DMA 4.3 : ReStructureStoryBoard.";
		
		Map<String, Object> output = new LinkedHashMap<>();
		
		logger.info("start Patch "+ patchVers);
		
		
		/* get parameter */
		List<String> storyList = ManagerMap.get(input, PatchConstants.Input.STORY_LIST, List.class);
		
		
		if(storyList!=null){
			logger.info("Patch Mode : Any storyBoard : "+ storyList);
			
			if(storyList.size()!=0){
				upgradeDataStoryBoardByStoryList(storyList);
			}else {
				logger.info("storyList is empty, then skip patch.");
			}
			
		}else{
			logger.info("Patch Mode : Full patch");
			
			upgradeDataStoryBoard();
			upgradeDataEntityTemp();
			upgradeDataEntityPublish();
		}
		
		
		output.put(PatchConstants.PATCH_VERSION, patchVers);
		output.put(PatchConstants.PATCH_DESCRIPTION, patchDesc);
		output.put(PatchConstants.PATCH_LAST_UPDATE, new Date().getTime());
		
		logger.info("output="+ output);
		logger.info("end Patch "+ patchVers);
		return output;
	}
	
	
	
	/*
	 * Patch Mode : Any patch : Mongo -> 
	 *  */
	private void upgradeDataStoryBoardByStoryList(List<String> storyList) throws Exception {
		
		StoryBoardDocument document;
		for(String storyName : storyList){
			
			document = storyBoardAccessor.findStoryByName(storyName);
			logger.info("upgradeDataStoryBoard storyName="+ storyName);
			
			if(versionCurrentDataLowerThanVersionThisPatch(document)){
				storyBoardAccessor.moveRelateData(document, user);
				mongoDbAccessor.save(document, user);
				
				logger.info("upgrade Success.");
			}else{
				
				logger.info("This storyboard is already new version, then skip upgrade.");
			}
			
		}
		
	}
	
	
	
	/*
	 * Patch Mode : Full patch : Mongo -> temp_entity_config
	 *  */
	private void upgradeDataStoryBoard() throws Exception {
		
		List<StoryBoardDocument> documentList = storyBoardAccessor.findAllNotGetRelateData();
		
		if(documentList != null && documentList.size() != 0){
			for(StoryBoardDocument document : documentList){
				
				logger.info("upgradeDataStoryBoard storyName="+ document.getStoryName());
				
				if(versionCurrentDataLowerThanVersionThisPatch(document)){
					storyBoardAccessor.moveRelateData(document, user);
					mongoDbAccessor.save(document, user);
					
					logger.info("upgrade Success.");
				}else{
					
					logger.info("This storyboard is already new version, then skip upgrade.");
				}
			}
		}
	}
	
	/*
	 * Patch Mode : Full patch : Mongo -> temp_entity_config
	 *  */
	private void upgradeDataEntityTemp() throws Exception {
		
		List<TempEntityConfigDocument> documentList = tempEntityConfigAccessor.findAllNotGetRelateData();
		
		if(documentList != null && documentList.size() != 0){
			for(TempEntityConfigDocument document : documentList){
				
				logger.info("upgradeDataEntityTemp entityName="+ document.getEntityName());
				
				if(versionCurrentDataLowerThanVersionThisPatch(document)){
					tempEntityConfigAccessor.moveRelateData(document, user);
					mongoDbAccessor.save(document, user);
					
					logger.info("upgrade Success.");
				}else {
					
					logger.info("This EntityTemp is already new version, then skip upgrade.");
				}
			}
		}
	}
	
	/*
	 * Patch Mode : Full patch : Mongo -> dma_entity_config
	 *  */
	private void upgradeDataEntityPublish() throws Exception {
		
		List<PublishEntityConfigDocument> documentList = publishEntityConfigAccessor.findAllNotGetRelateData();
		
		if(documentList != null && documentList.size() != 0){
			for(PublishEntityConfigDocument document : documentList){
				
				logger.info("upgradeDataEntityPublish entityName="+ document.getEntityName());
				
				if(versionCurrentDataLowerThanVersionThisPatch(document)){
					publishEntityConfigAccessor.moveRelateData(document, user);
					mongoDbAccessor.save(document, user);
					
					logger.info("upgrade Success.");
				}else {
					
					logger.info("This EntityPublish is already new version, then skip upgrade.");
				}
			}
		}
	}
	
	
	
	
	
	private boolean versionCurrentDataLowerThanVersionThisPatch(StoryBoardDocument document) throws Exception {
		
		List<Map<String, Object>> storyBoardList = (List<Map<String, Object>>)document.getStoryBoard();
		List<Map<String, Object>> screenList = (List<Map<String, Object>>)document.getScreenList();
		
		if( (storyBoardList == null || storyBoardList.size() == 0 ) && ( screenList == null || screenList.size() == 0 ) ){
			// document haven't storyBoardList and screenList, So this data is  !!! newData.
			return false;
		}else {
			// document have storyBoardList and screenList, So this data is  !!! oldData.
			return true;
		}
	}
	private boolean versionCurrentDataLowerThanVersionThisPatch(TempEntityConfigDocument document) throws Exception {
		
		List<Map<String, Object>> screenList = (List<Map<String, Object>>)document.getScreenList();
		
		if( screenList == null || screenList.size() == 0 ){
			// document haven't screenList, So this data is  !!! newData.
			return false;
		}else {
			// document have screenList, So this data is  !!! oldData.
			return true;
		}
	}
	private boolean versionCurrentDataLowerThanVersionThisPatch(PublishEntityConfigDocument document) throws Exception {
		
		List<Map<String, Object>> screenList = (List<Map<String, Object>>)document.getScreenList();
		
		if( screenList == null || screenList.size() == 0 ){
			// document haven't screenList, So this data is  !!! newData.
			return false;
		}else {
			// document have screenList, So this data is  !!! oldData.
			return true;
		}
	}

}
