package com.jjpa.processor;

import java.util.Map;

public interface PatchProcessor {

	public Object process(Map<String, Object> input) throws Exception;
	
}
