package com.jjpa.common;

public class DmaConstants {

	public static final String PROJECT_CODE 			= "300";
	public static final String PROJECT_NAME 			= "DMA";
	
	public static  class PublishedStatus{
		
		public static final String ALL 					= "All";
		public static final String FAILURE 				= "F";
		public static final String WARNING 				= "W";
		public static final String PUBLISHED 			= "P";
		public static final String REVERTED 			= "R";
		public static final String RESTORE 				= "RES";
	}

	public static class Path{
		
		public static final String ID 					= "id";
		public static final String REMARK 				= "remark";
		public static final String LAST_UPDATE 			= "lastUpdate";
		public static final String LAST_PUBLISH 		= "lastPublish";
		public static final String STATUS 				= "status";
	}

	public static class Entity{
		
		public static class Key{
			
			public static final String ID 					= "id";
			public static final String ENTITY_LIST 			= "entityList";
			public static final String ENTITY_NAME 			= "entityName";
			public static final String STORY_NAME 			= "storyName";
			public static final String REMARK 				= "remark";
			public static final String ROLE 				= "role";
			public static final String THEME 				= "theme";
			public static final String STATUS 				= "status";
			public static final String VERSION 				= "version";
			public static final String FORCE_LOGOUT 		= "forceLogout";
			public static final String PUBLISH_STATUS 		= "publishStatus";
			public static final String LAST_UPDATE 			= "lastUpdate";
			public static final String LAST_PUBLISH 		= "lastPublish";
			public static final String CREATE_BY      = "createBy";
			
			public static final String HOME_PAGE 			= "homePage";
			public static final String FIRST_PAGE 			= "firstPage";
			public static final String NOTIFICATION_PAGE 	= "notificationPage";
			public static final String PIN_LIST 			= "pinList";
			public static final String SCREEN_LIST 			= "screenList";

			public static final String APP_KEY 				= "appKey";
			public static final String APP_INFO 			= "appInfo";
			public static final String APK_INFO 			= "apkInfo";
			public static final String IPA_INFO 			= "ipaInfo";
			public static final String APP_CHANGE_LOG_IOS 			= "appChangeLogIOS";
			public static final String APP_CHANGE_LOG_ANDRIOD 			= "appChangeLogAndroid";
			public static final String APP_MODE 			= "appMode";
			public static final String APP_LOGO			= "appLogo";



			
		}
		
		public static class Value{
			
		}
	}

	public static class StoryBoard{
		
		public static class Key{
			
			public static final String ID 					= "id";
			public static final String STORY_LIST			= "storyList";
			public static final String STORY_NAME			= "storyName";
			public static final String REMARK 				= "remark";
			public static final String STATUS 				= "status";
			public static final String TABINFO 				= "tabInfo";
			public static final String APIINFO				= "apiInfo";

			public static final String FIRST_PAGE			= "firstPage";
			public static final String HOME_PAGE			= "homePage";
			public static final String NOTIFICATION_PAGE	= "notificationPage";
			public static final String PIN_PAGE				= "pinPage";
			public static final String STORY_BOARD			= "storyBoard";
			public static final String SCREEN_LIST			= "screenList";
			public static final String PIN_LIST				= "pinList";
			public static final String GROUPS				= "groups";
			public static final String LAST_UPDATE 			= "lastUpdate";
			public static final String LAST_PUBLISH 		= "lastPublish";
			
			public static final String ACTION_LIST 			= "actionList";
			public static final String ACTION_NAME 			= "actionName";
			public static final String INPUT_NAME 			= "inputName";
			public static final String TYPE 				= "type";
			public static final String API_NAME 			= "apiName";
			public static final String TARGET_TYPE 			= "targetType";
			public static final String TARGET 				= "target";

			
			public static final String END_ALERT 		= "endAlert";
			public static final String CONFIRM_DIALOG 		= "confirmDialog";
			public static final String NEED_CONFIRM 		= "needConfirm";
			public static final String CONFIRM_MSG 			= "confirmMsg";
			public static final String CONFIRM_BTN 			= "confirmBtn";
			public static final String UI_JSON 				= "uiJson";
			public static final String EXTRA_PATH 			= "extraPath";
			public static final String DMA_TYPE 			= "dmaType";
			public static final String DMA_SCREEN_VERSION 	= "version";
			public static final String DMA_SCREEN_NEED_UPGRAD 		= "needUpgradeScreen";
			public static final String DMA_STORYBOARD_NEED_UPGRADE 	= "needUpgradeStoryBoard";

			public static final String ACTION_DESCRIPTION 	= "action_description";
			public static final String EDIT_VALUE 	= "editValue";
			public static final String HIDE_ACTION 	= "hideAction";
			
			
			public static final String CONFIG				= "config";
			public static class Config{
				
				public static final String DMA_TYPE 			= "dmaType";
				public static final String PARAMETER 			= "parameter";
				
				public static class Parameter{
					
					public static final String OPTION 			= "option";
					public static final String DESC 			= "desc";
					public static final String DEFAULT_VALUE 	= "defaultValue";
					public static final String VALUE 			= "value";
					public static final String FIELD_TYPE 		= "fieldType";
					public static final String VALUE_FROM 		= "valueFrom";
				}
				
			}

			public static final String TARGET_CONDITIONS	= "targetConditions";
			public static class TargetConditions{
				public static final String USE_CONDITIONS 						= "useConditions";
				public static final String CONDITION_FROM_GLOBAL_INPUT			= "conditionFromGlobalInput";
				public static final String CONDITIONS_LIST 						= "conditionsList";
				public static class ConditionsList{
					public static final String TYPE 				= "type";
					public static final String EQUAL_VALUE 			= "equalValue";
					public static final String TARGET 				= "target";

				}
			}
	
			public static final String GLOBAL_INPUT_LIST 	= "globalInputList";
			public static class GlobalInputList{
				public static final String USE_GLOBAL 			= "useGlobal";
				public static final String PATH 				= "path";
				public static final String LOCAL_PATH 			= "localPath";
				public static final String VALUE 				= "value";
				public static final String VALUE_FROM 			= "valueFrom";
				public static final String SERVICE_NAME			= "serviceName";
				public static final String INPUT_NAME			= "inputName";
				public static final String FIELD_POSITION		= "fieldPosition";
				public static final String FIELD_TYPE		    = "fieldType";
				
			}
			public static class DynamicInputList{
				public static final String LOCAL_PATH 			= "localPath";
				public static final String SERVICE_NAME			= "serviceName";
				public static final String NAME			        = "name";
				public static final String PATH			        = "path";
				public static final String FIELD_NAME			= "fieldName";
				public static final String FIELD_TYPE			= "fieldType";
				public static final String TYPE			 		= "type";
				public static final String FIELD_POSITION 		= "fieldPosition";
			}
			
			public static final String VALUE_PROPERTIES 	= "valueProperties";
			public static final String CLASS_NAME 			= "className";
			public static final String DMA_PARAMETER 		= "dmaParameter";
			
			public static final String GLOBAL_OUTPUT_LIST 	= "globalOutputList";
			public static final String GLOBAL_NAME 			= "globalName";
			public static final String OUTPUT_NAME 			= "outputName";
			public static final String NAME 				= "name";
			public static final String KEEP 				= "keep";
			public static final String SOURCE 				= "source";
			public static final String VERSION 				= "version";
			public static final String CURRENT_VERSION 		= "currentVersion";
			public static final String TEMPLATE_TYPE		= "templateType";
			public static final String BACK_TYPE 			= "backType";

			public static final String VALIDATION 						= "validation";
			public static final String STORY_BOARD_VALIDATION 			= "storyBoardValidation";
			public static final String SCREEN_VALIDATION 				= "screenValidation";
			public static final String IMAGE_VALIDATION 				= "imageValidation";
			public static final String TEMPLATE_VALIDATION 				= "templateValidation";
			public static final String ACTION_CHAIN_TEMPLATE_VALIDATION = "actionChainTemplateValidation";
			public static final String ACTION_CHAIN_VALIDATION 			= "actionChainValidation";
			
			public static final String SG_SERVICE_VALIDATION 			= "serviceGatewayValidation";
			public static final String SG_OAUTH_VALIDATION 				= "sgOauthValidation";
			public static final String SG_WSDL_VALIDATION 				= "sgWsdlValidation";
		}
		
		public static class Value{
			
			public static final String LOGIN 				= "Login";
			public static final String DO_NOTHING 			= "Do Nothing";			
			public static final String SCREEN 				= "Screen";			
			public static final String EXTRA 				= "Extra";
			public static final String SELF 				= "self";
			
			public static final String GLOBAL 				= "Global";
			public static final String NATIVE 				= "Native";
			public static final String MANUAL 				= "Manual";
			public static final String API_OUTPUT			= "API_OUTPUT";
			
			public static class BackType{
				public static final String FIRST_PAGE 			= "firstpage";
				public static final String HOME					= "home";
				public static final String STACK				= "stack";
				public static final String NONE			    	= "none";
				public static final String PIN			    	= "pin";
			}
		}

	}
	
	public static class AppUI{
		
		public static class Key{
			
			public static final String STORY_BOARD 			= "storyboard";
			public static final String VERSION 				= "version";
			public static final String IS_APPUI_NOT_INUSED_BY_DMA_STROYBOARD = "isAppUiNotInUsedByDmaStroyBoard";
					
			public static final String ID 					= "id";
			public static final String UI_NAME 				= "uiName";
			public static final String UI_LIST 				= "uiList";
			public static final String APP_UI_LIST 			= "appUiList";
			public static final String REMARK 				= "remark";
			public static final String STATUS 				= "status";
			public static final String LAST_UPDATE 			= "lastUpdate";
			public static final String LAST_PUBLISH 		= "lastPublish";
			
			public static final String UI_CONFIG_DATA 		= "uiConfigData";
			public static final String TEMPLATE 			= "template";
			
			public static final String DYNAMIC_INPUT_LIST 	= "dynamicInputList";
			public static final String DYNAMIC_OUTPUT_LIST 	= "dynamicOutputList";
			
			public static final String ACTION_LIST 			= "actionList";
			public static final String ACTION_NAME			= "actionName";
			
			public static final String GLOBAL_INPUT_LIST 	= "globalInputList";
			public static final String INPUT_NAME 			= "inputName";
			public static final String FIELD_POSITION 		= "fieldPosition";
			
			public static final String GLOBAL_OUTPUT_LIST 	= "globalOutputList";
			public static final String OUTPUT_NAME 			= "outputName";
			public static final String DATA_STRUCTURE 		= "dataStructure";
			
			public static final String SOURCE 				= "source";
			public static final String TEMPLATE_TYPE 		= "templateType";
			public static final String DYNAMIC_API 			= "dynamicApi";
			public static final String DYNAMIC_API_ID 		= "dynamicApiId";
			public static final String UI_JSON 				= "uiJson";
			
			public static final String INPUT_LIST 			= "inputList";
			public static final String OUTPUT_LIST 			= "outputList";
			public static final String FIELD_TYPE 			= "fieldType";
			public static final String TYPE 				= "type";
			
			public static final String DYNAMIC_DATA_BINDING = "dynamicDataBinding";
			public static class DynamicOutputList{
				public static final String NAME				= "name";
				public static final String FIELD_NAME		= "fieldName";
			}
			
			public static final String BINDING 				= "binding";	
			public static final String PROPERTY_NAME 		= "property_name";
			public static final String COMPONENT_TYPE 		= "component_type";
			
			public static final String COMPONENT_TYPE_OPTION= "component_type_option";
			public static class Component_type_option{
				public static final String OPTION_TYPE			= "option_type";
				public static final String CHECKNULL			= "check_null";

			}
			public static final String DEFAULT				= "default";			
			
			
			public static final String GROUP 				= "group";
			public static final String VALUE 				= "value";
			public static final String PATH 				= "path";
			
			public static final String NAME 				= "name";
			
			public static final String NEED_UPGRADE 		= "needUpgrade";
			public static final String URL_SCREEN_SHOT 		= "urlScreenShot";
		}
		
		public static class Value{
			
			public static final String API 					= "api";
			
			public static final String DYNAMIC_INPUT 		= "dynamic_input";
			public static final String DYNAMIC_LIST 		= "dynamic_list";
			public static final String STATIC				= "static";
			
			public static final String TOPIC 				= "Topic";
			public static final String DESCRIPTION 			= "Description";
			public static final String COMBO 				= "combo";		
			public static final String OUTPUT 				= "output";
			
			public static final String RESULT_LIST 			= "result_list";
			public static final String API_URL 				= "api_url";
			public static final String API_DATA 			= "api_data";
		}
	}
	
	public static class Template{
		
		public static class Key{			
			
			public static final String TEMPLATE_LIST 		= "templateList";
			public static final String TEMPLATE_NAME 		= "templateName";
			public static final String INFO_URL 			= "infoUrl";
			public static final String INUSED_BY_DMA_SCREEN = "inUsedByDmaScreen";
			public static final String ERROR_LIST 			= "errorList";
			public static final String ERROR_MAP			= "errorMap";
			public static final String ERROR_MESSAGE 		= "errorMessage";
			
			public static final String DYNAMIC_DATA_BINDING = "dynamicDataBinding";
			public static final String NAME 				= "name";
			public static final String TYPE 				= "type";
			public static final String BINDING 				= "binding";
		}
		
		public static class Type{
			
			public static final String DYNAMIC_FIELD 		= "dynamic_field";
			public static final String DOMAIN 				= "domain";
		}
		
		public static class Name{
			
			public static final String RESULT_INITIAL 		= "result_initial";
			public static final String MAINFORM 			= "mainform";
		}
	}

	public static class ActionChain{
		
		public static class Reference{			
			
			public static class Key{			
				
				public static final String NAME 			= "_name";
				public static final String VERSION 			= "_version";
				public static final String REFERENCE 		= "_refference";
			}
		}
		
		public static class Key{			
			
			public static final String NAME 				= "name";
			public static final String PATH 				= "path";
			public static final String DATA 				= "data";
		}
		
		public static class Value{
			
			public static final String SUCCESS 				= "success";
			public static final String ERROR 				= "error";
			public static final String OPTIONS 				= "options";
		}
	}
	
	public static class ActionChainTemplate{
		
		public static class Key{
			
			public static final String ACTION_CHAIN_TEMPLATE_LIST 	= "actionChainTemplateList";
			
			public static final String ACTION_TYPE 					= "actionType";
			public static final String CHAIN_TEMPLATE 				= "chainTemplate";
			
			public static class ChainTemplate{
				
				public static final String TYPE 				= "type";
				public static final String DMA_TYPE 			= "dmaType";
				
				/** EOAT is mean : End of actionChainTemplate */
				public static final String DMA_EOAT 			= "dmaEOAT";
				public static final String OPTIONS 				= "options";
				public static final String SUCCESS 				= "success";
				public static final String ERROR 				= "error";
				
				public static class DmaEOF{
					
					public static final String SUCCESS 			= "success";
					public static final String FAILURE 			= "failure";
				}

				public static class Options{
					
					public static final String TITLE 			= "title";
					public static final String DESCRIPTION 		= "description";
					public static final String URL 				= "url";
					public static final String METHOD 			= "method";
					public static final String HEADER 			= "header";
					public static final String DATA 			= "data";
					public static final String RESULT 			= "result";
					public static final String OUTPUT1 			= "output1";
					public static final String LOCK 			= "lock";
					public static final String EXPRESSION 		= "expression";
					public static final String POSITIVE_TEXT 	= "positiveText";
					public static final String NEGETIVE_TEXT 	= "negetiveText";
					public static final String FORM 			= "form";
				}
				
			}
			
		}

	}

	public static class StoryboardExtraTargetType{
		
		public static class Key{			
			public static final String APP_UI_LIST		= "appUiList";
			
			public static final String UI_NAME			= "uiName";
			public static final String CONFIG_PARAMETER	= "configParameter";
		}
	}

	public static class Log{
		
		public static class Key{
			public static final String TOTAL 			= "total";
			public static final String TRANSACTION_LIST = "transactionList";
			public static final String LOG_LIST 		= "logList";
			
			public static final String LOG_ID 			= "logID";
			public static final String CONTENT_TYPE 	= "contentType";
			public static final String USERNAME 		= "username";
			public static final String MOBILE_ID 		= "mobileID";
			public static final String DEVICE_TYPE 		= "deviceType";
			public static final String SCREEN 			= "screen";
			public static final String ACTION 			= "action";
			public static final String REQUEST_TIME 	= "requestTime";
			public static final String TIME 			= "time";
			public static final String RESULT 			= "result";
			public static final String SUCCESS 			= "success";
			public static final String RESULT_MESSAGE 	= "resultMessage";
			public static final String RESULT_CODE 		= "resultCode";
			public static final String RESULT_FROM 		= "resultFrom";
			
			/* 30/08/2561 add by Akanit : add httpStatus */
			public static final String RESULT_HTTP_STATUS = "resultHttpStatus";
			
			public static final String MESSAGE 			= "message";
			public static final String INPUT 			= "input";
			public static final String OUTPUT 			= "output";
			
			public static final String START_TIME 		= "startTime";
			public static final String END_TIME 		= "endTime";
		}
		
		public static class Path{
			public static final String DATA 			= "data";
			
			public static final String LOG_ID 			= "data.dmaSeq";
			public static final String CONTENT_TYPE 	= "data.contentType";
			public static final String USERNAME 		= "data.metadata.user.userId";
			public static final String MOBILE_ID 		= "data.metadata.DMA_CLIENT_KEY";
			public static final String DEVICE_TYPE 		= "data.metadata.DMA_DEVICE_TYPE";
			public static final String MESSAGE 			= "data.message";
			public static final String SCREEN 			= "data._dma_source";
			public static final String ACTION 			= "data._dma_action";
			public static final String REQUEST_TIME 	= "data.dmaRequestTime";
			public static final String TIME 			= "data.dmaProcessingTime";
//			public static final String RESPONSE_CODE 	= "data.dmaResponse.responseHeader.responseCode";
//			public static final String RESULT_MESSAGE 	= "data.dmaResponse.responseHeader.responseDescription";
//			public static final String RESPONSE_CODE 	= "responseHeader.responseCode";
//			public static final String RESPONSE_DESC 	= "responseHeader.responseDescription";
//			public static final String RESULT_SUCCESS 	= "resultSuccess";
//			public static final String RESULT_MESSAGE 	= "resultMessage";
			public static final String INPUT 			= "data.dmaRequest";
			public static final String OUTPUT 			= "data.dmaResponse";
			public static final String RESPONSE_SUCCESS = "data.dmaResponseSuccess";
			public static final String RESPONSE_MESSAGE = "data.dmaResponseMessage";
			public static final String RESPONSE_CODE 	= "data.dmaResponseCode";
			public static final String RESPONSE_FROM 	= "data.dmaResponseFrom";
			
			/* 30/08/2561 add by Akanit : add httpStatus */
			public static final String RESPONSE_HTTP_STATUS = "data.dmaResponseHttpStatus";
		}
	}

}
