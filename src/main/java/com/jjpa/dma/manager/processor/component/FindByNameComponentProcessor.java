package com.jjpa.dma.manager.processor.component;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ComponentAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByNameComponentProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
private static final Logger logger = LoggerFactory.getLogger(FindByNameComponentProcessor.class);
	
	protected ComponentAccessor componentAccessor;
	
	public FindByNameComponentProcessor() {
		this.componentAccessor =  SpringApplicationContext.getBean("ComponentAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByNameComponentProcessor");
		logger.debug("Process ===> FindByNameComponentProcessor : {}", input.toJsonString());
		
		String componentName = (String)input.getValue("componentName");
		return componentAccessor.findComponent(componentName);
		
	}

}
