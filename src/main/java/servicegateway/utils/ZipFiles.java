package servicegateway.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFiles {
	
	List<String> fileList;
	private static String SOURCE_FOLDER;

	public ZipFiles(){
		fileList = new ArrayList<String>();
	}
	
//	public static void main( String[] args ) throws Exception{
//		ZipFiles zip = new ZipFiles();
////		appZip.setSource("C:\\PPG\\New folder");
////		appZip.setSource("C:-PPG-");
////		appZip.zipIt("C:\\PPG\\generatePPG.zip");
//		
//		String pathBackup = "D:/temp/";
//		String source = pathBackup + "test1234";
//		String zipSrc = pathBackup + "backup.backup";
//		
//		zip.setSource(source);
//		zip.zipIt(zipSrc);
//		
//		ManagerStore.deleteFile(source);
//	}
	
	public void setSource(String pathName) throws Exception {	
		String last = pathName.substring(pathName.length()-1,pathName.length());
		if("/".equals(last) || "\\".equals(last)){
			pathName = pathName.substring(0,pathName.length()-1);
		}
		SOURCE_FOLDER = pathName;
	}
	
	public synchronized void zipIt(String pathZip) throws Exception {
		
		generateFileList();
		
	    byte[] buffer = new byte[1024];    	
	    try{    		
	    	FileOutputStream fos = new FileOutputStream(pathZip);
	    	ZipOutputStream zos = new ZipOutputStream(fos);
	    	
	    	for(String file : this.fileList){

	    		ZipEntry ze= new ZipEntry(file);
	    		zos.putNextEntry(ze);
	       
	    		FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);   
				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}     
				in.close();
		    }
			zos.closeEntry();
			zos.close();
			
	    }catch(IOException ex){
	    	ex.printStackTrace();   
		}
	}

	public synchronized void zipSelected(List<String> srcFiles, String pathZip) throws Exception {
		
		ZipOutputStream zos=null;
		
	    byte[] buffer = new byte[1024];    	
	    try{    		
	    	FileOutputStream fos = new FileOutputStream(pathZip);
	    	zos = new ZipOutputStream(fos);
	    	
	    	for(String srcFile : srcFiles){
	    		
	    		File file = new File(srcFile);
	    		
	    		zos.putNextEntry(new ZipEntry(file.getName()));

	    		FileInputStream in = new FileInputStream(file); 
				int length;
				while ((length = in.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}     
				zos.closeEntry();
				in.close();
		    }
			zos.close();
			
	    }catch(IOException e){
	    	try {
	    		if(zos!=null){
	    			zos.closeEntry();
					zos.close();
	    		}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    	e.printStackTrace();   
		}
	}
	
	public synchronized void zipSelectedDynamicName(List<ZipEntryModel> zipEntryList, String pathZip) throws Exception {
		
		ZipOutputStream zos=null;
		
	    byte[] buffer = new byte[1024];    	
	    try{    		
	    	FileOutputStream fos = new FileOutputStream(pathZip);
	    	zos = new ZipOutputStream(fos);
	    	
	    	for(ZipEntryModel zipEntry : zipEntryList){
	    		
	    		File file = new File(zipEntry.getEntryPath());
	    		
	    		String fileName = zipEntry.getEntryName();
	    		if(fileName == null){
	    			fileName = file.getName();
	    		}
	    		
	    		zos.putNextEntry(new ZipEntry(fileName));

	    		FileInputStream in = new FileInputStream(file); 
				int length;
				while ((length = in.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}     
				zos.closeEntry();
				in.close();
		    }
			zos.close();
			
	    }catch(IOException e){
	    	try {
	    		if(zos!=null){
	    			zos.closeEntry();
					zos.close();
	    		}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    	e.printStackTrace();   
		}
	}
	
	private void generateFileList() throws Exception {
		File node = new File(SOURCE_FOLDER);
		if(node.isFile()){
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
		}
		if(node.isDirectory()){
			String[] nodeList = node.list();
			for(String subNode : nodeList){
				generateFileList(new File(node,subNode));
			}
		}
    }
	
	private void generateFileList(File node) throws Exception {
		if(node.isFile()){
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
		}
		if(node.isDirectory()){
			String[] nodeList = node.list();
			for(String subNode : nodeList){
				generateFileList(new File(node,subNode));
			}
		}
    }

	private String generateZipEntry(String file) throws Exception {
		return file.substring(SOURCE_FOLDER.length()+1, file.length());
	}
}
