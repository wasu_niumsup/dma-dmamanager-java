package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardScreenListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardScreenListDocument;

public class StoryBoardScreenListAccessorImpl implements StoryBoardScreenListAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	
	protected StoryBoardScreenListAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	private List<StoryBoardScreenListDocument> findMongoByName(String name) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where(MongoConstants.Simple.NAME).is(name));
		return mongoDbAccessor.findByCriteria(findByName, StoryBoardScreenListDocument.class);
	}
	
	
	
	@Override
	public List<Map<String, Object>> findByName(String name) throws Exception {
		
		/* find data from mongo */
		List<StoryBoardScreenListDocument> resultList = findMongoByName(name);

		
		/* 
		 * convert mongoData to listData 
		 * */
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardScreenListDocument resultMap : resultList){
				list.add(resultMap.getData());
			}
		}
		
		
		return list;
	}


	@Override
	public void deleteByName(String name, String user) throws Exception {

		/* find data from mongo */
		List<StoryBoardScreenListDocument> resultList = findMongoByName(name);
		
		
		/* 
		 * loop delete
		 * */
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardScreenListDocument resultMap : resultList){
				mongoDbAccessor.delete(resultMap, user);
			}
		}
		
	}


	@Override
	public void saveByName(String name, List<Map<String, Object>> dataList, String user) throws Exception {
		
		/* delete */
		deleteByName(name, user);
		
		
		/* 
		 * loop save
		 * */
		StoryBoardScreenListDocument document;
		
		if(dataList != null && dataList.size() != 0){
			
			for(Map<String, Object> dataMap : dataList){
				
				document = new StoryBoardScreenListDocument(); 
				document.setName(name);
				document.setData(dataMap);
				
				mongoDbAccessor.save(document, user);
			}
		}
		
	}

}
