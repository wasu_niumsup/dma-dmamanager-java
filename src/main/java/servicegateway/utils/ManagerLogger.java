package servicegateway.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;

public class ManagerLogger {

//	private Logger logger;
	private static final Logger logger = LoggerFactory.getLogger(ManagerLogger.class);
//	private String keyResult = "result";
	
	private long startTime;
	
	private String msg_length_checkMemory = "30";
	private String msg_length_usedtime = "50";
	private int overload_totalTime = 100;
	
	private boolean isStartCheckMemory = false;
	
	
	/* constructer */
//	public ManagerLogger(Logger logger) {
//		this.logger = logger;
//	}
	
	
	/* get set */
	public void setMsgLengthCheckMemory(String msg_length_checkMemory) throws Exception {
    	this.msg_length_checkMemory = msg_length_checkMemory;
	}
    public void setMsgLengthUsedtime(String msg_length_usedtime) throws Exception {
    	this.msg_length_usedtime = msg_length_usedtime;
	}
    public void setOverloadTotalTime(int overload_totalTime) throws Exception {
    	this.overload_totalTime = overload_totalTime;
	}
	
    
    private String getDetail(Thread thread) throws Exception {
		return "(" + thread.getStackTrace()[3].getMethodName() + ":" + thread.getStackTrace()[3].getLineNumber() + ")";
	}
    
	
    
	public long logStartTime() throws Exception {
		startTime = System.currentTimeMillis();
		return startTime;
	}
	
	public void logTotalTime(String tag, Thread thread) throws Exception {
		long ms = TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - startTime);
		if (ms >= overload_totalTime) {
			logger.warn(getDetail(thread) + "Total time of "+tag+" is [over] : " + ms + " ms ");
		} else {
			logger.info(getDetail(thread) + "Total time of "+tag+" is [in time] : " + ms + " ms ");
		}
	}
	public void logTotalTime(String tag, long startTime, Thread thread) throws Exception {
		long ms = TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - startTime);
		if (ms >= overload_totalTime) {
			logger.warn(getDetail(thread) + "Total time of "+tag+" is [over] : " + ms + " ms ");
		} else {
			logger.info(getDetail(thread) + "Total time of "+tag+" is [in time] : " + ms + " ms ");
		}
	}

	
//	public void info(Object data, Thread thread) throws Exception {
//		try{
//			Map<String, Object> log = new LinkedHashMap<>();
//			log.put(keyResult, BeanUtils.describe(data));
//			logger.info(getDetail(thread) + log);
//		}catch (Exception e){ 
//			logger.info(getDetail(thread) + data);
//		}
//	}
//	public void info(List dataList, Thread thread) throws Exception {
//		try{
//			List<Map<String, String>> list = new ArrayList<>();
//			for(Object data : dataList){
//				list.add(BeanUtils.describe(data));
//			}
//			Map<String, Object> log = new LinkedHashMap<>();
//			log.put(keyResult, list);
//			logger.info(getDetail(thread) + log);
//		}catch (Exception e){ 
//			logger.info(getDetail(thread) + dataList);
//		}
//	}
//	public void debug(Object data, Thread thread) throws Exception {
//		try{
//			Map<String, Object> log = new LinkedHashMap<>();
//			log.put(keyResult, BeanUtils.describe(data));
//			logger.debug(getDetail(thread) + log);
//		}catch (Exception e){
//			logger.debug(getDetail(thread) + data);
//		}
//	}
//	public void debug(List dataList, Thread thread) throws Exception {
//		try{
//			List<Map<String, String>> list = new ArrayList<>();
//			for(Object data : dataList){
//				list.add(BeanUtils.describe(data));
//			}
//			Map<String, Object> log = new LinkedHashMap<>();
//			log.put(keyResult, list);
//			logger.debug(getDetail(thread) + log);
//		}catch (Exception e){
//			logger.debug(getDetail(thread) + dataList);
//		}
//	}
	
	
	
	public void infoJsonString(String label, Object value, Thread thread) throws Exception {
		try{
			logger.info(getDetail(thread) + label+ " = " + JsonPath.parse(value).jsonString());
		}catch (Exception e){
			try{
				logger.info(getDetail(thread) + label+ " = " + value);
			}catch (Exception e2){
				logger.info(label+ " = " + value);
			}
		}
	}
	public void debugJsonString(String label, Object value, Thread thread) throws Exception {
		try{
			logger.debug(getDetail(thread) + label+ " = " + JsonPath.parse(value).jsonString());
		}catch (Exception e){
			try{
				logger.debug(getDetail(thread) + label+ " = " + value);
			}catch (Exception e2){
				logger.debug(label+ " = " + value);
			}
		}
	}
	
	
	
	public List<String> getStackTrace(Exception e) throws Exception {
		List<String> list = new ArrayList<String>();
		list.add(e.toString());
		for(StackTraceElement stackTrace : e.getStackTrace()){
			list.add(stackTrace.toString());
		}
		return list;
	}
	public void printStackTrace(Exception e, Thread thread) throws Exception {
		logger.info(getDetail(thread) + "Exception : "+e);
		for(StackTraceElement stackTrace : e.getStackTrace()){
			logger.info(getDetail(thread) + "Exception_detail : "+stackTrace);
		}
	}
	public void printStackTrace(Exception e, String msgTrack, Thread thread) throws Exception {
		logger.info(getDetail(thread) + msgTrack+" : "+e);
		for(StackTraceElement stackTrace : e.getStackTrace()){
			logger.info(getDetail(thread) + msgTrack+"_detail : "+stackTrace);
		}
	}
	
	
	
	public void displayFinishTime(String message, long startTime, Thread thread) throws Exception {
		double use_time = ((double)(System.nanoTime()-startTime))/1000/1000;
		logger.info(getDetail(thread) + String.format("%-"+msg_length_usedtime+"s", message)+" : (total)use_time "+String.format("%-24s", String.valueOf(use_time)+" ms")+" = "+use_time/1000+" s");
	}
	
	public void startCheckMemory() throws Exception {
		Runtime.getRuntime().gc();
		isStartCheckMemory = true;
	}
	public void checkMemory(String message, Thread thread) throws Exception {
		
//		System.gc ();
//		System.runFinalization ();
		
		if(isStartCheckMemory){
	
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Maxx_memory "+Runtime.getRuntime().maxMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Tota_memory "+Runtime.getRuntime().totalMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Free_memory "+Runtime.getRuntime().freeMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Used_memory "+(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));	
		}else{
			
			startCheckMemory();

			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Maxx_memory "+Runtime.getRuntime().maxMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Tota_memory "+Runtime.getRuntime().totalMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Free_memory "+Runtime.getRuntime().freeMemory());
			logger.info(getDetail(thread) + String.format("%-"+msg_length_checkMemory+"s", message)+" : Used_memory "+(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));		
		}
	}
	
}
