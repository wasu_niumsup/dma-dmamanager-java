package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public class ActionChainAccessorImpl implements ActionChainAccessor {

	protected MongoDbAccessor mongoDbAccessor;

	protected ActionChainAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Object saveActionChain(DMAActionChainDocument actionChain, String user) throws Exception {
		
		try{
			DMAActionChainDocument oldActionChain = findActionChainByType(actionChain.getActionChainType());
			if(oldActionChain != null){
				oldActionChain.setBinding(actionChain.getBinding());
				oldActionChain.setClassName(actionChain.getClassName());
				oldActionChain.setNativeJson(actionChain.getNativeJson());
				oldActionChain.setVersion(actionChain.getVersion());
				oldActionChain.setLastUpdate(actionChain.getLastUpdate());
				actionChain = oldActionChain;
			}
			return mongoDbAccessor.save(actionChain, user);
		}catch(Exception e){
			throw e;
		}
		
	}

	public DMAActionChainDocument findActionChainByType(String actionChainType) throws Exception {
		
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where("actionChainType").regex(Pattern.compile(DataUtils.getMongoMatchRegex(actionChainType), Pattern.CASE_INSENSITIVE)));
		
		try{
			return mongoDbAccessor.findOneByCriteria(findByName, DMAActionChainDocument.class);
		}catch(Exception e){
			throw e;
		}
	}

	public List<DMAActionChainDocument> listActionChain() throws Exception {
		
		try{
			return mongoDbAccessor.findAll(DMAActionChainDocument.class);
		}catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,Object> listActionChainType() throws Exception {
		List<DMAActionChainDocument> actionChainObjList = listActionChain();
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String,Object>> actionChainTypeList = new ArrayList<Map<String,Object>>();
		Map<String,Object> actionChainMap;
		for(DMAActionChainDocument document : actionChainObjList){
			actionChainMap = new HashMap<String,Object>();
			actionChainMap.put("actionChainType", document.getActionChainType());
			actionChainMap.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
			/* add by warrawan 12/4/2018 - add version to listActionChain*/
			actionChainMap.put("version", document.getVersion());
			/*end */
			actionChainTypeList.add(actionChainMap);
		}
		result.put("actionChainTypeList", actionChainTypeList);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public void deleteActionChain(ServiceData input) throws Exception {
		String user = (String)input.getValue("user");
		List<Map<String, Object>> actionChainTypeList = (List<Map<String, Object>>)input.getValue("actionChainTypeList");
		for(Map<String, Object> actionChainObj : actionChainTypeList){
			String actionChainType = (String)actionChainObj.get("actionChainType");
			mongoDbAccessor.delete(findActionChainByType(actionChainType), user);
		}
		
	}
	
}
