package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public interface ActionChainAccessor {
	
	public Object saveActionChain(DMAActionChainDocument actionChain, String user) throws Exception;
	public DMAActionChainDocument findActionChainByType(String actionChainType) throws Exception;
	public List<DMAActionChainDocument> listActionChain() throws Exception;
	public Map<String,Object> listActionChainType() throws Exception;
	public void deleteActionChain(ServiceData input) throws Exception;

}
