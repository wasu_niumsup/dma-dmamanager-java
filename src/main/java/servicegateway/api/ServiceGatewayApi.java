package servicegateway.api;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.SGConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

public class ServiceGatewayApi {

	private static final Logger logger = LoggerFactory.getLogger(ServiceGatewayApi.class);
	
	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}
	
	public Map<String, Object> findServiceInputByName(String apiName, Boolean haveChild) throws Exception {
		if(apiName==null){
			return null;
		}
		
		String message = "List Service input by service name : " + apiName;
		
		String findServiceMsg = "{" + "\"contentType\":301," + "\"source\":2," + "\"message\":\"" + message + "\"," 
				+ "\"_sg_name\" : \"" + apiName + "\"" + "," + "\"_sg_haveChild\" : \"" + haveChild + "\"" + "}";
		
		DocumentContext ctx = JsonPath.parse(findServiceMsg);

		/**
		* Tanawat W.
		* [2018-05-08] Change transaction message.
		*/
		ctx.put("$",DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);

		DocumentContext reqData = JsonPath.parse("{}");
		reqData.put("$", "requestData", ctx.read("$"));
		/* 
		 * call service api
		 * */
		logger.info("call serviceGateway... : serviceName = "+ apiName);
		logger.info("findServiceInputByName request="+reqData.jsonString());
		
		DocumentContext responseCtx = getJsonAccessor().send(DataUtils.getSGURL() + "/serviceGateway/apis/json/local/externalAPIs/findServiceInputByName", reqData);
		Map<String, Object> response = responseCtx.json();
		
		
		return getResponseData(response);
	}
	
	public Map<String, Object> findServiceByName(String apiName) throws Exception {
		if(apiName==null){
			return null;
		}

		String message = "Get Service config by service name : " + apiName;
		
		String findServiceMsg = "{" + "\"contentType\":301," + "\"source\":2," + "\"message\":\"" + message + "\","
				+ "\"_sg_name\" : \"" + apiName + "\"" + "}";
	
		DocumentContext ctx = JsonPath.parse(findServiceMsg);
		
		
		/**
		* Tanawat W.
		* [2018-05-08] Change transaction message.
		*/
		ctx.put("$",DmaManagerConstant.MESSAGE, message);
		ctx.put("$",DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);
		
		DocumentContext reqData = JsonPath.parse("{}");
		reqData.put("$", "requestData", ctx.read("$"));
		
		/* 
		 * call service api
		 * */
		logger.info("call serviceGateway... : serviceName = "+ apiName);
		logger.info("findServiceByName request="+reqData.jsonString());
		
		DocumentContext responseCtx = getJsonAccessor().send( DataUtils.getSGURL() + "/serviceGateway/apis/json/local/externalAPIs/findByName", reqData);
		Map<String, Object> response = responseCtx.json();	
		
		logger.info("findServiceByName response="+response);
		
		
		return getResponseData(response);	
	}
	
	public Map<String, Object> backupData(List<Map<String, Object>> dataList) throws Exception {
		if(dataList==null || dataList.size() == 0){
			return null;
		}
		
		String message = "Backup Service Config";
		
		String findServiceMsg = "{" + "\"contentType\":301," + "\"source\":2," + "\"message\":\"" + message + "\"" + "}";

		DocumentContext ctx = JsonPath.parse(findServiceMsg);
		
		ctx.put("$", "dataList", dataList);
		
		
		/**
		* Tanawat W.
		* [2018-05-08] Change transaction message.
		*/
		ctx.put("$", DmaManagerConstant.MESSAGE, message);
		ctx.put("$", DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);
		
		DocumentContext reqData = JsonPath.parse("{}");
		reqData.put("$", "requestData", ctx.read("$"));
		
		/* 
		 * call service api
		 * */
		logger.info("call serviceGateway...");
		logger.info("backupData request="+reqData.jsonString());
		
		DocumentContext responseCtx = getJsonAccessor().send(DataUtils.getSGURL() + "/serviceGateway/apis/json/local/externalAPIs/backupData", reqData);
		Map<String, Object> response = responseCtx.json();	
		
		logger.info("backupData response="+response);
		
		
		return getResponseData(response);
	}

	public Map<String, Object> restoreValidation(String filePath) throws Exception {
		if(filePath==null){
			return null;
		}

		String message = "Get Restore validation";
		
		String findServiceMsg = "{" + "\"contentType\":301," + "\"source\":2," 
				+ "\"message\":\"" + message +"\"," + "\"_sg_filePath\" : \"" + filePath + "\"" + "}";
		
		DocumentContext ctx = JsonPath.parse(findServiceMsg);
		
		
		/**
		* Tanawat W.
		* [2018-05-08] Change transaction message.
		*/
		ctx.put("$",DmaManagerConstant.MESSAGE, message);
		ctx.put("$",DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);
		
		DocumentContext reqData = JsonPath.parse("{}");
		reqData.put("$", "requestData", ctx.read("$"));
		
		/* 
		 * call service api
		 * */
		logger.info("call serviceGateway...");
		logger.info("restoreValidation request="+reqData.jsonString());
		
		DocumentContext responseCtx = getJsonAccessor().send( DataUtils.getSGURL() + "/serviceGateway/apis/json/local/externalAPIs/restoreValidation", reqData);
		Map<String, Object> response = responseCtx.json();	
		
		logger.info("restoreValidation response="+response);
		
		
		return getResponseData(response);	
	}

	
	
	private Map<String, Object> getResponseData(Map<String, Object> response) throws Exception {
		
		/* 
		 * check response success 
		 * */
		Map<String, Object> responseHeader =ManagerMap.get(response, DmaManagerConstant.RESPONSE_HEADER, Map.class);
		
		if (!SGConstants.CODE_SUCCESS.equals(responseHeader.get(DmaManagerConstant.RESPONSE_CODE))) {
			
			String error = ManagerMap.getString(responseHeader, DmaManagerConstant.RESPONSE_DESC);
			logger.info(error);
			throw new Exception(error);
			
		} else {
			
			Map<String, Object> responseData = ManagerMap.get(response, DmaManagerConstant.RESPONSE_DATA, Map.class);
			return responseData;
		}
		
	}
	
}
