package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;

public class StoryBoardRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(StoryBoardRestoreValidator.class);
	
	protected StoryBoardAccessor storyBoardAccessor;
	
	public StoryBoardRestoreValidator() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	public Map<String, Object> process(List<Map<String, Object>> storyBoardRequiredList) throws Exception {
		logger.info("Process ===> StoryBoardRestoreValidator");
		
		
		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.Validation.BackupRestore.NAME);
		boolean validationVersion = ManagerMap.getBoolean(propMap, PropConstants.Validation.BackupRestore.Key.VALIDATION_VERSION_STORYBOARD, true);

		
		
		boolean validationStatus = true;
//    	String version_restore, version_old;
    	String name , storyBoardFromdmaManagerVersion;
		DataUtils.Version dmaEngineVersions = new DataUtils.Version(ConfigurationUtils.getEngineVersion());
		
    	for(Map<String, Object> storyBoardRequired : storyBoardRequiredList){
    		
    		if(validationVersion){
	    		
    			
    			/* get parametere */
//    			version_restore = ManagerMap.getString(storyBoardRequired, ReferenceConstants.DMA.VERSION);
	    		storyBoardFromdmaManagerVersion = DataUtils.getStringValue(storyBoardRequired, "dmaManagerVersion","0.0.0.00");
	    		
				
				/**
				 * 
				 * Validate version
				 */
				// if(StringUtil.isBlank(dmaManagerVersion)){
				// 	throw new Exception("Invalid not found backup storyboard version.");
				// }			
				DataUtils.Version backupVersion = new DataUtils.Version(storyBoardFromdmaManagerVersion);
				logger.info("backup version : " + backupVersion +  " , running dmaManagerVersion : " + dmaEngineVersions);
				
				if(dmaEngineVersions.major == backupVersion.major 
					&& dmaEngineVersions.minor == backupVersion.minor
				){
					// do nothing
				}else{
					logger.warn("Backup file is not compatible with version of engine.");
					validationStatus = false;
	    			storyBoardRequired.put(BackupConstants.Validator.Key.DETAIL, "Backup file is not compatible with version of engine.");
					storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
					break;
				}
			
    		}
			
    		
    		
    		name = ManagerMap.getString(storyBoardRequired, ReferenceConstants.DMA.NAME);
    		
    		StoryBoardDocument doc = storyBoardAccessor.findStoryByName(name);
    		
    		
    		/* validate add status */
    		if(doc==null){
    			logger.info("#not found old StoryBoard="+ name);
    			logger.info("#update action=ADD");
    			logger.info("#update {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
				
				storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
				continue;
				
    		}else {
    			logger.info("#found old StoryBoard="+ name);
    			logger.info("#update {}={}", BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.DUPLICATE);
				logger.info("#update {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
    			
    			validationStatus = false;
    			storyBoardRequired.put(BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.DUPLICATE);
    			storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
    			
    			
    			/* validate version : now not validate version but In the future want to validate version */
//	        	try {
//	    			version_old = doc.getReference().getVersion();
//				} catch (Exception e) {
//					logger.info("#StoryBoard="+ name + "not have reference.");
//					logger.info("#update action=UPDATE");
//	    			logger.info("#update status=true");
//					
//					storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
//					continue;
//				}
//	    		
//	    		
//	    		
//	    		if(version_restore == version_old){
//	    			logger.info("#StoryBoard="+ name + " can restore.");
//	    			logger.info("#update action=UPDATE");
//	    			logger.info("#update status=true");
//					
//					storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
//	    			
//	    		}else{
//	    			logger.info("#StoryBoard="+ name + " can not restore.");
//	    			logger.info("#update detail=version");
//	    			logger.info("#update status=false");
//					
//	    			validationStatus = false;
//    				storyBoardRequired.put(BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.VERSION_CONFLICT);
//					storyBoardRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
//	    		}
    			
    		}
    		
    	}
    	
    	/* mapping data to UI */
    	Map<String, Object> output = new LinkedHashMap<>();
    	output.put("summaryValidation", validationStatus);
    	output.put("data", storyBoardRequiredList);
    	
    	return output;
	}
}
