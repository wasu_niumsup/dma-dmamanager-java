package com.jjpa.dma.manager.processor.restore.reference;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.dma.manager.model.reference.RequiredReference;

public class GenerateTemplateReferenceRestore {

	private static Logger logger = LoggerFactory.getLogger(GenerateTemplateReferenceRestore.class);
	
	public void process(RequiredReference reqRef, String filePath) throws Exception {
		logger.info("Process ===> GenerateTemplateReferenceRestore");
		
		String name, version;
		Map<String, Object> map;
		List<Map<String, Object>> resultList = new ArrayList<>();		
		
		File file = new File(filePath);
		if(file.exists()){
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
							
					try{
						version = JsonPath.read(fileEntry, "$." + "versionTemplate");
					}catch(Exception e){
						version = null;
					}
					
					try{
						name = JsonPath.read(fileEntry, "$." + "templateName");
					}catch(Exception e){
						name = null;
					}
					
					map = new LinkedHashMap<>();
					map.put(ReferenceConstants.DMA.VERSION, version);
					map.put(ReferenceConstants.DMA.NAME, name);
					
					resultList.add(map);
				}
			}
		}
		
		reqRef.setDmaTemplateRequired(resultList);
	}
}
