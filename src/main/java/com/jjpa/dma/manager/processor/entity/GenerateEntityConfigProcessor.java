package com.jjpa.dma.manager.processor.entity;

import java.util.LinkedHashMap;
import java.util.Map;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;

public class GenerateEntityConfigProcessor{

	public Object process(String entityName, StoryBoardDocument storyBoardDoc) throws Exception {
		
		Map<String, Object> map = new LinkedHashMap<>();
		map.put(DmaConstants.Entity.Key.ENTITY_NAME, entityName);
		map.put(DmaConstants.Entity.Key.HOME_PAGE, storyBoardDoc.getHomePage());
		map.put(DmaConstants.Entity.Key.FIRST_PAGE, storyBoardDoc.getFirstPage());
		map.put(DmaConstants.Entity.Key.NOTIFICATION_PAGE, storyBoardDoc.getNotificationPage());
		map.put(DmaConstants.Entity.Key.SCREEN_LIST, storyBoardDoc.getScreenList());
		map.put(DmaConstants.Entity.Key.STORY_NAME, storyBoardDoc.getStoryName());
		
		ServiceData entityConfig = new ServiceData(map);
		
		return entityConfig;
	}

}
