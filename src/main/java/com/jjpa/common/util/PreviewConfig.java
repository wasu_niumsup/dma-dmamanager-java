package com.jjpa.common.util;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.PropConstants;


public class PreviewConfig extends PropConstants {

	private static Logger logger = Logger.getLogger(SystemConfig.class);
	private static String previewConfigFilePath = null;

	public static DocumentContext getConfig() throws Exception {

		if(previewConfigFilePath == null) {
			logger.info("read previewConfig file !");
			previewConfigFilePath = getSystemConfigFilePath();
		}
		
		logger.info("load previewConfig file : name = "+ previewConfigFilePath);
		Map<String, Object> logConfig = ManagerStore.readJsonMapFromFile(previewConfigFilePath,false);		
		Map<String, Object> systemConfig = new LinkedHashMap<>(logConfig);
		
		DocumentContext ctx = JsonPath.parse(systemConfig);
		
		/* keep config to chche */
		logger.info("value = "+ ctx.jsonString());
		
		return ctx;
	}

	public static String getPreviewURLEndpoint() throws Exception {
		DocumentContext previewData = getConfig();
		String endpoint = "";

		try {
			endpoint = previewData.read(PropConstants.PreviewProperties.SCREEN_PREVIEW_URL_ENDPOINT);
		} catch (Exception e) {
			logger.warn("getPreviewURLEndpoint cannot get url");
		}

		return endpoint;
	}

	public static boolean getPreviewEnableMode() throws Exception {
		DocumentContext previewData = getConfig();
		boolean enableMode = false;

		try {
			enableMode = previewData.read(PropConstants.PreviewProperties.SCREEN_PREVIEW_ENABLE_MODE);
		} catch (Exception e) {
			logger.warn("getPreviewURLEndpoint cannot get url");
		}

		return enableMode;
	}
	
	private static String getSystemConfigFilePath() throws Exception {
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.PreviewProperties.NAME);
		return ManagerMap.getString(propMap, PropConstants.PreviewProperties.FILE_PATH_PREVIEW_CONFIG);
	}
	
	
}
