package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerStore;

public class GenerateTemplateFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateTemplateFolderBackup.class);

	protected TemplateAccessor templateAccessor;
	
	public GenerateTemplateFolderBackup() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	public void process(Reference reference, String directoryFileBackup) throws Exception {
		logger.info("process : generate template backup folder");
		
		List<Map<String, Object>> dmaTemplateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(reference);
		if(dmaTemplateRequiredList==null){
			return;
		}
		
		String templateName;
		for(Map<String, Object> dmaTemplateRequired : dmaTemplateRequiredList){
			
			/* find template in mongo */
			templateName = ManagerMap.getString(dmaTemplateRequired, ReferenceConstants.DMA.NAME);
			DMATemplate templateDoc = templateAccessor.findTemplateByName(templateName);
			if(templateDoc==null){
				String errorMessage = "Template '"+ templateName +"' not found in DB.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/template/{templateName}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.TEMPLATE + File.separator + templateName;
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(templateDoc.toMap(), directoryFile);
		}

	}
}
