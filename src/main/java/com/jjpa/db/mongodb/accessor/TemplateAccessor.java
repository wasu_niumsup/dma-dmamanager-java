package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;

public interface TemplateAccessor {
	
	public Object saveTemplate(DMATemplate template, String user) throws Exception;
	public Object saveTemplateAndUpdateCurrentVersion(DMATemplate template, String user) throws Exception;
	public DMATemplate findTemplateByName(String templateName) throws Exception;
	public List<DMATemplate> listTemplates() throws Exception;
	public Map<String,Object> listTemplateName() throws Exception;
	public void deleteTemplate(ServiceData input) throws Exception;
	public void updateCurrentVersionTemplateRequriedReferenceToScreen(DMATemplate template) throws Exception;
	public void updateCurrentVersionTemplateRequriedReferenceToScreen(List<Map<String, Object>> screenList, DMATemplate template) throws Exception;

}
