package servicegateway.utils;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import com.jayway.jsonpath.JsonPath;

public class ManagerPropertiesFile {
	
	public Map<String, Object> readProperties(String path) throws Exception{
		String key, value;
		Map<String, Object> property = new LinkedHashMap<String, Object>();
		
		Properties prop = new Properties();
		prop.load(getClass().getResource(path).openStream());
		Enumeration<?> propertyNames = prop.propertyNames();
		while (propertyNames.hasMoreElements()) {
	        key = (String) propertyNames.nextElement();
	        value = prop.getProperty(key);   
	        property.put(key.trim(), value.trim());
		}	
		return property;
	}
	
	public Map<String, Object> readJson(String path) {
		
		InputStream inputStream = getClass().getResourceAsStream(path);
		return JsonPath.parse(inputStream).json();
	}
	
}
