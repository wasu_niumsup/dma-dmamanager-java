package com.jjpa.dma.manager.processor.appui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.CriteriaUtil;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;

public class ListAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(ListAppUiProcessor.class);
	
	protected AppUiAccessor appUiAccessor;

	public ListAppUiProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	private Integer maxListAppUIRecord;
	
	@Override
	public Object process(HttpServletRequest request, ServiceData serviceData) throws Exception {
		logger.info("Process ===> ListAppUiProcessor");
		
		Map<String,Object> input = serviceData.getData();
		logger.info("inputBody="+input);
		
		
		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.SystemConfig.NAME);
		maxListAppUIRecord = ManagerMap.getIntger(propMap, PropConstants.SystemConfig.Key.MAX_LIST_APP_UI_RECORD);
		
		
		
		/* 
		 * step 1 : create query from WhereOption and GridPaggingModel
		 * */
		logger.info("step 1 : create query from WhereOption and GridPaggingModel");
		Query query = new Query();
		
		List<WhereOption> whereOptList = createWhereOption(input);
		CriteriaUtil.addCriteriaWhere(query, whereOptList);
		
		GridPagging gridPagging = createGridPaggingModel(input);
		CriteriaUtil.addCriteriaPagging(query, gridPagging);

		logger.info("step 1 : success.");
		logger.info("query="+query);

		
//		logger.info("step 1.1 : start call appUiAccessor.listAppUiByCriteria");
		Map<String, Object> appUi = appUiAccessor.listAppUiByCriteria(query);
//		logger.info("step 1.2 : end call appUiAccessor.listAppUiByCriteria");
		
		
		
		/* comment by Akanit 14/03/2562 : (140325621133) move binding version to methord "appUiAccessor.listAppUiByCriteria(query)"  */
//		List<Map<String, Object>> appUiList = ManagerMap.get(appUi, DmaConstants.AppUI.Key.APP_UI_LIST, List.class);
//		logger.info("step 2.1 : start Create appUiList");
//		for (Map<String, Object> uiMap : appUiList) {
//			//Get Version
//			long startTime = System.currentTimeMillis();
//			String screenName = ManagerMap.getString(uiMap, DmaConstants.AppUI.Key.UI_NAME);
//			String newVersion = appUiAccessor.findAppUiByName(screenName).getReference().getVersion();
//			
//			long ms = TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - startTime);
//			if (ms >= 100) {
//				logger.warn("Total time in getVersion [over] : " + ms + " ms ");
//			} else {
//
//			}
//
//
//			uiMap.put(DmaConstants.StoryBoard.Key.VERSION, newVersion);
//		}
//		logger.info("step 2.2 : end Create appUiList");
		
		
		return appUi;
		
	}
	
	
	
	private List<WhereOption> createWhereOption(Map<String, Object> input) throws Exception {
		
		String uiName = ManagerMap.getString(input, DmaConstants.AppUI.Key.UI_NAME);
		String remark = ManagerMap.getString(input, DmaConstants.AppUI.Key.REMARK);
		String status = ManagerMap.getString(input, DmaConstants.AppUI.Key.STATUS);
		String storyboard = ManagerMap.getString(input, DmaConstants.AppUI.Key.STORY_BOARD);
		
		boolean isAppUiNotUsedByStoryboard = ManagerMap.getBoolean(input, DmaConstants.AppUI.Key.IS_APPUI_NOT_INUSED_BY_DMA_STROYBOARD);


		String username = ManagerMap.getString(input, "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}
				
//		Long startTime = ManagerMap.getLong(input, AppConstants.Log.Key.START_TIME);
//		Long endTime = ManagerMap.getLong(input, AppConstants.Log.Key.END_TIME);
		
		
		// create whereOptList.
		List<WhereOption> whereOptList = new ArrayList<>();
		WhereOption whereOpt;

		
		/* where : search */
		whereOpt = new WhereOption();
		whereOpt.setPath(MongoConstants.AppUI.Path.UI_NAME);
		whereOpt.setValue(uiName);
		whereOpt.setSearch(true);
		whereOptList.add(whereOpt);

		if(!"admin".equals(username)){
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.AppUI.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
			whereOptList.add(whereOpt);
		}
		
		whereOpt = new WhereOption();
		whereOpt.setPath(MongoConstants.Path.REMARK);
		whereOpt.setValue(remark);
		whereOpt.setSearch(true);
		whereOptList.add(whereOpt);
		
		whereOpt = new WhereOption();
		whereOpt.setPath(MongoConstants.Path.STATUS);
		whereOpt.setValue(status);
		whereOpt.setSearch(true);
		whereOptList.add(whereOpt);

		if(isAppUiNotUsedByStoryboard){
			
			WhereOption whereOpt2 = new WhereOption();
			whereOpt2.setPath(MongoConstants.Reference.Path.INUSED_BY_DMA_STROYBOARD_NAME);
			whereOpt2.setValue(storyboard);
			
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.Reference.Path.INUSED_BY_DMA_STROYBOARD_NAME);
			whereOpt.setValue(null);
			whereOpt.setAcceptNull(true);
			whereOpt.setOr(whereOpt2);
			whereOptList.add(whereOpt);
			
		}else {
			
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.Reference.Path.INUSED_BY_DMA_STROYBOARD_NAME);
			whereOpt.setValue(storyboard);
			whereOpt.setSearch(true);
			whereOptList.add(whereOpt);
		}
		
		
		/* where : between */
//		whereOpt = new WhereOption();
//		whereOpt.setPath(MongoConstants.Key.JSON_DATA+"."+AppConstants.Log.Key.REQUEST_TIME);
//		whereOpt.setStart(startTime);
//		whereOpt.setEnd(endTime);
//		whereOpt.setBetween(true);
//		whereOptList.add(whereOpt);
		
		
		return whereOptList;
	}

	private GridPagging createGridPaggingModel(Map<String, Object> input) throws Exception {
		
		// get value from UI.	
		String sort = ManagerMap.getString(input, AppConstants.Pagging.Key.SORT);
		String dir = ManagerMap.getString(input, AppConstants.Pagging.Key.DIR);
		String start = ManagerMap.getString(input, AppConstants.Pagging.Key.START);
		String limit = ManagerMap.getString(input, AppConstants.Pagging.Key.LIMIT);
		
		/* check max limit */
		if(maxListAppUIRecord != null){
			if( StringUtil.isBlank(limit) || Integer.parseInt(limit) > maxListAppUIRecord ){
				limit = maxListAppUIRecord.toString();
			}
		}
		
		
		// create gridPaaging model.
		GridPagging gridPagging = new GridPagging();
		gridPagging.setSort(sort);
		gridPagging.setDir(dir);
		gridPagging.setStart(start);
		gridPagging.setLimit(limit);
		return gridPagging;
		
	}
	
}
