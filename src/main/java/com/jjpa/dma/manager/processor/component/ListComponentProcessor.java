package com.jjpa.dma.manager.processor.component;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ComponentAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListComponentProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
private static final Logger logger = LoggerFactory.getLogger(ListComponentProcessor.class);
	
	protected ComponentAccessor componentAccessor;
	
	public ListComponentProcessor() {
		this.componentAccessor =  SpringApplicationContext.getBean("ComponentAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveComponentProcessor");
		logger.debug("Process ===> SaveComponentProcessor : {}", input.toJsonString());
		
		return componentAccessor.listComponentName();
		
	}

}
