package com.jjpa.dma.manager.processor.reference.inused.binding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;

import servicegateway.utils.ManagerArrayList;

/* 16/03/2561 add by Akanit */
public class BindingInUsedReferenceToTemplateProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingInUsedReferenceToTemplateProcessor.class);
	
	public void process(DMATemplate template, AppUiDocument appUiDoc) throws Exception {
		logger.info("process : binding inUsed reference to DmaTemplate");
		
		List<Map<String, Object>> screenInUsedList = new GetReferenceProcessor().getInUsedByDmaScreens(template.getReference());
		if(screenInUsedList==null){
			screenInUsedList = new ArrayList<>();
			template.setReference(new InitialReferenceProcessor().process(template.getReference()));
			template.getReference().getInUsed().setInUsedByDmaScreen(screenInUsedList);
		}
		
		
		/* check screenName dose not duplicate  */
		Map<String, Object> rowDuplicateScreenName = ManagerArrayList.findRowBySearchString(screenInUsedList, ReferenceConstants.DMA.NAME, appUiDoc.getUiName());
		if(rowDuplicateScreenName==null){
			logger.info("set DmaTemplate '{}' inUsed By Screen '{}'", template.getTemplateName(), appUiDoc.getUiName());
			
			Map<String, Object> screenInUsed = new LinkedHashMap<>();
			screenInUsed.put(ReferenceConstants.DMA.VERSION, new GetReferenceProcessor().getVersion(appUiDoc.getReference()));
			screenInUsed.put(ReferenceConstants.DMA.NAME, appUiDoc.getUiName());
			screenInUsed.put(ReferenceConstants.DMA.TYPE, appUiDoc.getTemplateType());
			
			screenInUsedList.add(screenInUsed);
		}
		
	}

}
