package com.jjpa.dma.manager.processor.reference;

import java.util.List;
import java.util.Map;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.dma.manager.model.reference.Reference;

import servicegateway.utils.ManagerMap;

public class GetReferenceProcessor {

//	private static final Logger logger = LoggerFactory.getLogger(GetReferenceProcessor.class);
	
	private Reference validate(Reference reference){ 
		
//		if(reference==null){
//			logger.info("Reference not found, then create Reference.");
//			reference = new Reference();
//		}
//		
//		InUsedReference inUsed = reference.getInUsed();
//		if(inUsed==null){
//			logger.info("InUsedReference not found, then create InUsedReference.");
//			inUsed = new InUsedReference();
//			reference.setInUsed(inUsed);
//		}
//		
//		RequiredReference required = reference.getRequired();
//		if(required==null){
//			logger.info("RequiredReference not found, then create RequiredReference.");
//			required = new RequiredReference();
//			reference.setRequired(required);
//		}
		
		return reference;
	}
	
	
	
	/* 
	 * Version
	 *  */
	public String getVersion(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getVersion();
		}catch(Exception e){
			return null;
		}
	}
	
	
	
	/* 
	 * InUsed
	 *  */
	public String getInUsedByDmaEntity(Reference reference){ 
		try{
			Map<String, Object> required = getInUsedByDmaEntitys(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getInUsedByDmaEntitys(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getInUsed().getInUsedByDmaEntity();
		}catch(Exception e){
			return null;
		}
	}

	
	public String getInUsedByDmaStroyBoard(Reference reference){ 
		try{
			Map<String, Object> required = getInUsedByDmaStroyBoards(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getInUsedByDmaStroyBoards(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getInUsed().getInUsedByDmaStroyBoard();
		}catch(Exception e){
			return null;
		}
	}
	

	public String getInUsedByDmaScreen(Reference reference){ 
		try{
			Map<String, Object> required = getInUsedByDmaScreens(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getInUsedByDmaScreens(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getInUsed().getInUsedByDmaScreen();
		}catch(Exception e){
			return null;
		}
	}
	
	
	
	/* 
	 * Required
	 *  */
	public String getDmaStroyBoardRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaStroyBoardRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaStroyBoardRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaStroyBoardRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getDmaScreenRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaScreenRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaScreenRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaScreenRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getDmaTemplateRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaTemplateRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaTemplateRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaTemplateRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getDmaActionChainRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaActionChainRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaActionChainRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaActionChainRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getDmaActionChainTemplateRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaActionChainTemplateRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaActionChainTemplateRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaActionChainTemplateRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getDmaImageRequired(Reference reference){ 
		try{
			Map<String, Object> required = getDmaImageRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.DMA.NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getDmaImageRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getDmaImageRequired();
		}catch(Exception e){
			return null;
		}
	}
	
	
	public String getSgServiceRequired(Reference reference){ 
		try{
			Map<String, Object> required = getSgServiceRequireds(reference).get(0);
			return ManagerMap.getString(required, ReferenceConstants.SG.SERVICE_NAME);
		}catch(Exception e){
			return null;
		}
	}
	public List<Map<String, Object>> getSgServiceRequireds(Reference reference){ 
		reference = validate(reference);
		try{
			return reference.getRequired().getSgServiceRequired();
		}catch(Exception e){
			return null;
		}
	}
	
}
