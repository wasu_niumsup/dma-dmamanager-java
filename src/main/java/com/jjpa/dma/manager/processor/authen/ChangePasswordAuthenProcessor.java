package com.jjpa.dma.manager.processor.authen;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.UserAccessor;
import com.jjpa.db.mongodb.document.dma.manager.UserDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ChangePasswordAuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(ChangePasswordAuthenProcessor.class);
	
	private UserAccessor getUserAccessor() {
		return SpringApplicationContext.getBean("UserAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ChangePasswordAuthenProcessor");
		logger.debug("Process ===> ChangePasswordAuthenProcessor : {}", input.toJsonString());
		
		return changePassword(input);
		
	}
	
	public Map<String, Object> changePassword(ServiceData input) throws Exception {

		String username = (String)input.getValue("username");
		String password = (String)input.getValue("password");
		String newPassword = (String)input.getValue("newPassword");

		UserAccessor accessor = getUserAccessor();
		UserDocument user = accessor.changePassword(username, password, newPassword);
		if(user != null){
			Map<String, Object> output = new HashMap<>();
			output.put("username", user.getUsername());
			return output;
		}else{
			throw new Exception("Invalid Username or Password");
		}
	}
	
}
