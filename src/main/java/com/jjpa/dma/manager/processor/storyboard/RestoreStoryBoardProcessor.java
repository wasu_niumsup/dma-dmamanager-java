package com.jjpa.dma.manager.processor.storyboard;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.PatchConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.dma.manager.processor.api.PatchVersion;
import com.jjpa.dma.manager.processor.reference.FixReferenceProcessor;
import com.jjpa.dma.manager.processor.restore.ActionChainRestore;
import com.jjpa.dma.manager.processor.restore.ActionChainTemplateRestore;
import com.jjpa.dma.manager.processor.restore.GetVersionRestore;
import com.jjpa.dma.manager.processor.restore.ScreenRestore;
import com.jjpa.dma.manager.processor.restore.StoryBoardRestore;
import com.jjpa.dma.manager.processor.restore.TemplateRestore;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;

public class RestoreStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(RestoreStoryBoardProcessor.class);

	private String user, fileRestorePath, folderNameVers, folderNameDMA, versionRestore;
	private List<String> storyList;
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> RestoreStoryBoardProcessor");
		logger.debug("Process ===> RestoreStoryBoardProcessor : {}", input.toJsonString());
		
		
		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
		fileRestorePath = ManagerMap.getString(propMap, PropConstants.FileLocal.Restore.Key.FILE_PATH, "");
		folderNameVers = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.VERSION, "");
		folderNameDMA = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.DMA, "");
//		folderNameSG = ManagerMap.getString(propMap, "FOLDER_NAME_SG", "");
		
		
		/* get parameter */
		user = (String)input.getValue("user");
		String folderName = (String)input.getValue(BackupConstants.FILE_UPLOAD_NAME);
		
		if(folderName==null){
			String errorDesc = "Please identify '" + BackupConstants.FILE_UPLOAD_NAME + "'";
			logger.info(errorDesc);
			throw new Exception(errorDesc);
		}
		
		
		/* create path of import file */
		String restoreFilePath = fileRestorePath + folderName + "/";
		
		/* create verifile load template old or story*/
		Boolean impotedValue = (Boolean) input.getValue("imported");
		/* 
		 * step 1 : restore data from file. 
		 * */
		logger.info("#step 1 : restore data from file.");

		Map<String , Object> storyObjTemplate = (Map<String , Object>) input.getValue("templateValidation");
		ArrayList<Map<String , Object>> listTemplate = (ArrayList<Map<String , Object>>) storyObjTemplate.get("data");

		try{
			restoreDataByRestoreFile(restoreFilePath , impotedValue , listTemplate);
		}catch(Exception e){
			throw e;
		}finally{
			ManagerStore.deleteFile(restoreFilePath);
		}
		logger.info("#step 1 : success.");
		
		
		
		/* 
    	 * step 2 : patch version.
    	 * */
    	logger.info("#step 2 : patch version.");
    	try{
    		patchVersion(request, restoreFilePath);
    	}catch(Exception e){
    		// not throws exception
    		logger.error("patch version fail. but not stop process.", e);
    	}
    	logger.info("#step 2 : success.");
    	
    	
    	
    	/* 22/05/2562 add by Akanit : fix Ref */
		new FixReferenceProcessor().fixRefAll();
		
    	
    	
    	Map<String, Object> output = new LinkedHashMap<>();
    	logger.info("output="+output);
		logger.info("End procces : restore data externalAPIs.");
		return output;
	}
	
	
	
	private void restoreDataByRestoreFile(String restoreFilePath , Boolean importedValue , ArrayList<Map<String , Object>> listTemplateStoryboard) throws Exception{
		
		
		/* get version from restoreFile */
		String versionFolder = restoreFilePath + folderNameVers;
		versionRestore = new GetVersionRestore().process(versionFolder, user);
		
		
		/* create path of dmaFolder, sgFolder */
		String dmaPath 				= restoreFilePath + folderNameDMA + File.separator;

		String storyBoardFolder 		 = dmaPath + BackupConstants.FolderName.DMA.STORY_BOARD;
		String screenFolder 		     = dmaPath + BackupConstants.FolderName.DMA.SCREEN;
		String templateFolder 		     = dmaPath + BackupConstants.FolderName.DMA.TEMPLATE;
		String actionChainFolder 		 = dmaPath + BackupConstants.FolderName.DMA.ACTION_CHAIN;
		String actionChainTemplateFolder = dmaPath + BackupConstants.FolderName.DMA.ACTION_CHAIN_TEMPLATE;

		
		/* restore */
		new ActionChainRestore().process(actionChainFolder, user);
		new ActionChainTemplateRestore().process(actionChainTemplateFolder, user);
		new TemplateRestore().process(templateFolder, user);
		new ScreenRestore().process(screenFolder, user);
		storyList = new StoryBoardRestore().process(storyBoardFolder, user);
		
		

		/**
		 * Tanawat W.
		 * [2018-12-18] DMA 5.0
		 * 
		 */

		logger.info("importedValue ==> " + importedValue);
		Map<String,Object> informationForExport = new LinkedHashMap<>();
		informationForExport.put("dmaDirectoryPath", restoreFilePath);
		informationForExport.put("storyName", storyList.get(0));
		informationForExport.put("importedValue" , importedValue);
		informationForExport.put("templateStoryboardList" , listTemplateStoryboard);
		callHook("Import", informationForExport);
	}
	
	
	/* 04/01/2561 add by Akanit */
	private void patchVersion(HttpServletRequest request, String importFilePath) throws Exception {
	
    	Map<String, Object> input_patchVersion = new LinkedHashMap<>();
    	input_patchVersion.put(PatchConstants.Input.START_VERSION, versionRestore);
    	input_patchVersion.put(PatchConstants.Input.STORY_LIST, storyList);
    	
    	new PatchVersion().process(request, input_patchVersion);
	}
	
}
