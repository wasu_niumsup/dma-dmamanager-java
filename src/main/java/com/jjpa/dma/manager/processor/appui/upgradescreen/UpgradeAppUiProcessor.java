package com.jjpa.dma.manager.processor.appui.upgradescreen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.binding.BindingComponentProcessor;
import com.jjpa.dma.manager.processor.binding.BindingDynamicListActionProcessor;
import com.jjpa.dma.manager.processor.reference.DeleteReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToScreenProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

public class UpgradeAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

    private static final Logger logger = LoggerFactory.getLogger(UpgradeAppUiProcessor.class);

    protected AppUiAccessor appUiAccessor;
    protected StoryBoardAccessor storyBoardAccessor;
    protected TemplateAccessor templateAccessor;
    private String user;

    public UpgradeAppUiProcessor() {
        this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
        this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
        this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
    }

    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {

        logger.info("Process ===> UpgradeScreenProcessor");
        logger.debug("Process ===> UpgradeScreenProcessor : {}", input.toJsonString());

        return upgradeAppUi(input);

    }

    /* add by warrawan 21/03/2018 */
    private Object upgradeAppUi(ServiceData input) throws Exception {

        user = (String) input.getValue("user");

        String uiName = (String) input.getValue("uiName");

        logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi uiName " + uiName);

        if (DmaManagerConstant.BACK_TYPE.contains(uiName.toLowerCase().trim())) {
            throw new Exception(
                    "Screen name must not be reserved word. (\"firstpage\", \"home\", \"stack\", \"none\")");
        } else {
            AppUiDocument oldDoc = appUiAccessor.findAppUiByName(uiName);
            AppUiDocument appUiDoc = new AppUiDocument();

            logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi oldDoc " + oldDoc);

            if (oldDoc != null) {

                appUiDoc.setId(oldDoc.getId());
                appUiDoc.setReference(oldDoc.getReference());

                String templateName_old = new GetReferenceProcessor().getDmaTemplateRequired(oldDoc.getReference());
                logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi templateName_old " + templateName_old);
                DMATemplate template = templateAccessor.findTemplateByName(templateName_old);
                logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi template " + template);
                if (template != null) {

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi template INNN ");

                    /* get old binding in app ui */
                    Map<String, Object> uiConfigData = oldDoc.getUiConfigData();
                    List<Map<String, Object>> oldBindings = ManagerMap.get(uiConfigData, "binding", List.class);
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi oldBindings " + oldBindings);

                    /* get new binding from template update */
                    List<Map<String, Object>> newBindings = new ArrayList<>(template.getBinding());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi newBindings " + newBindings);

                    /* save old value binding to new binding */
                    for (Map<String, Object> newBinding : newBindings) {

                        String group = (String) newBinding.get("group");
                        logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi group " + group);
                        List<Map<String, Object>> groupList = ManagerArrayList.getRowListBySearchString(oldBindings,
                                "group", group);
                        if (groupList != null) {
                            String propertyName = (String) newBinding.get("property_name");
                            logger.info(
                                    "Process ===> UpgradeScreenProcessor upgradeAppUi propertyName " + propertyName);
                            List<Map<String, Object>> propertyNameList = ManagerArrayList
                                    .getRowListBySearchString(groupList, "property_name", propertyName);

                            if (propertyNameList != null) {
                                String componentType = (String) newBinding.get("component_type");
                                logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi componentType "
                                        + componentType);
                                Map<String, Object> valueMap = ManagerArrayList.findRowBySearchString(propertyNameList,
                                        "component_type", componentType);
                                logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi valueMap " + valueMap);
                                if (valueMap != null) {
                                    /* add old value to new binding */
                                    String value = (String) valueMap.get("value");
                                    logger.info(
                                            "Process ===> UpgradeScreenProcessor upgradeAppUi value binding " + value);
                                    newBinding.put("value", value);

                                    /* add old default to new binding */
                                    Object defaultObject = valueMap.get("default");
                                    logger.info(
                                            "Process ===> UpgradeScreenProcessor upgradeAppUi defaultObject binding "
                                                    + defaultObject);
                                    newBinding.put("default", defaultObject);
                                }
                            }
                        }
                    }

                    Map<String, Object> newUiConfigData = new HashMap<>(uiConfigData);

                    newUiConfigData.put("template", templateName_old);
                //     for DMA 5 
                logger.warn("skip set binding because DMA 5.0 hava no use.  ");
                //     newUiConfigData.put("binding", newBindings);
                    newUiConfigData.put("version", (String) template.getVersionTemplate());

                    logger.info("Process v3 ===> UpgradeScreenProcessor upgradeAppUi template version "
                            + (String) template.getVersionTemplate());
                    logger.info(
                            "Process v3 ===> UpgradeScreenProcessor upgradeAppUi newUiConfigData " + newUiConfigData);

                    appUiDoc.setUiConfigData((Map<String, Object>) newUiConfigData);

                    appUiDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
                    appUiDoc.setUiName(oldDoc.getUiName());
                    appUiDoc.setRemark(oldDoc.getRemark());
                    appUiDoc.setDynamicApi(oldDoc.getDynamicApi());
                    appUiDoc.setTemplateType(oldDoc.getTemplateType());
                    appUiDoc.setDynamicInputList(oldDoc.getDynamicInputList());
                    appUiDoc.setDynamicOutputList(oldDoc.getDynamicOutputList());
                    appUiDoc.setActionList(template.getActionList());
                    appUiDoc.setInputList(template.getInputList());
                    appUiDoc.setDynamicDataBinding(template.getDynamicDataBinding());
                    
                    /* 13/05/2562 (130525621721) add by Akanit : requirement from siam */
        			try{
        				appUiDoc.setUrlScreenShot((String) input.getValue(DmaConstants.AppUI.Key.URL_SCREEN_SHOT));
        			} catch (Exception e){
        				logger.info("not found '"+ DmaConstants.AppUI.Key.URL_SCREEN_SHOT +"' then setUrlScreenShot = ''");
        				appUiDoc.setUrlScreenShot("");
        			}

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi template.getActionList "
                            + template.getActionList());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi template.setInputList "
                            + template.getInputList());

                    /* get jsonTemplate */
                    Map<String, Object> dataBinding = getJsonTemplate(template);
                    /* set new jsonTemplate to app ui */
                    appUiDoc.setUiJson(generateUiJson(dataBinding, appUiDoc));

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi nativeJason New Template "
                            + generateUiJson(dataBinding, appUiDoc));

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getUiConfigData "
                            + appUiDoc.getUiConfigData());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getUiJson "
                            + appUiDoc.getUiJson());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getUiName "
                            + appUiDoc.getUiName());

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getRemark "
                            + appUiDoc.getRemark());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getDynamicApi "
                            + appUiDoc.getDynamicApi());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getTemplateType "
                            + appUiDoc.getTemplateType());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getDynamicInputList "
                            + appUiDoc.getDynamicInputList());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getDynamicOutputList "
                            + appUiDoc.getDynamicOutputList());

                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.setActionList "
                            + appUiDoc.getActionList());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.setInputList "
                            + appUiDoc.getInputList());
                    logger.info("Process ===> UpgradeScreenProcessor upgradeAppUi appUiDoc.getDynamicDataBinding "
                            + appUiDoc.getDynamicDataBinding());

                    new BindingDynamicListActionProcessor().process(appUiDoc);


                    /* 
                     * 21/05/2562 comment by Akanit : Jira DMA-1437 : Storyboard should not save upgrade 
                     * 
                     * */
//                    /* add by warramy 20/04/2018 - update Reference */
//
//                    String templateName = (String) appUiDoc.getUiConfigData().get("template");
//
//                    /* clear inUsed reference to dmaTemplate old */
//                    clearInUsedReferenceToOldTemplate(templateName_old, templateName, appUiDoc.getUiName());
//
//                    /* create reference.inUsed, reference.required */
//                    Reference reference = new InitialReferenceProcessor().process(appUiDoc.getReference());
//                    new DeleteReferenceProcessor().clearRequired(reference);
//                    appUiDoc.setReference(reference);
//
//                    /* update version reference */
//                    new BindingVersionReferenceProcessor().process(appUiDoc.getReference());
//                    
//                    /* 20/03/2562 add by Akanit : */
//                    storyBoardAccessor.updateVersionScreenToStoryboard(appUiDoc);
//
//                    /* update inUsed reference to dmaTemplate new */
//                    new BindingInUsedReferenceToTemplateProcessor().process(template, appUiDoc);
//
//                    /* update required reference to AppUI(screen) */
//                    new BindingRequiredReferenceToScreenProcessor().process(appUiDoc, template);
                    
                    
                    
                    /* 
                     * 21/05/2562 add by Akanit : Jira DMA-1437 : Storyboard should not save upgrade 
                     * */
                    /* 
                     * Reference 
                     * */
                    Reference reference = new InitialReferenceProcessor().process(appUiDoc.getReference());
                    new DeleteReferenceProcessor().clearRequired(reference);
                    appUiDoc.setReference(reference);
                    
                    /* update required reference to AppUi(screen) */
                    new BindingRequiredReferenceToScreenProcessor().process(appUiDoc, template);
                    
                    
                    
                    /* 
                     * save AppUi(screen)
                     * */
                    Object output = appUiAccessor.upgradeUi(appUiDoc, user);
                    
                    
                    
                    /* 
                     * upgrade screen version to storyboard[{name:'storyName', version:'newV', currentVersion:'newV'}] 
                     * */
                    StoryBoardDocument storyDoc = storyBoardAccessor.findStoryByUiName(appUiDoc);
                    storyBoardAccessor.upgradeVersionScreenToStoryboard(appUiDoc, storyDoc);
                    storyBoardAccessor.updateStoryBoardOneLevel(storyDoc, user);
                    
                    
                    
                    return output;
                } else {
                    throw new Exception("oldTemplate Not found!");
                }

            } else {
                throw new Exception("oldDoc Not found!");
            }
        }

    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> getJsonTemplate(DMATemplate template) throws Exception {

        /* get templateJson */
        DocumentContext ctx = JsonPath.parse(template.getNativeJson().get("data"));

        /* create dataBinding from templateJson (old system : dataBinding is templateJson) */
        Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());

        return dataBinding;
    }

    private String generateUiJson(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception {
        logger.info("#do generateUiJson");

        logger.info("#call BindingComponentProcessor()");
        new BindingComponentProcessor().process(dataBinding, appUiDoc);
        String uiJson = JsonPath.parse(dataBinding).jsonString();
        //		logger.info("#BindingComponentProcessor dataBinding="+uiJson);

        return uiJson;
    }

    /* 16/03/2561 add by Akanit */
//    private void clearInUsedReferenceToOldTemplate(String templateName_old, String templateName_new, String screenName)
//            throws Exception {
//
//        if (templateName_old != null && !templateName_old.equals(templateName_new)) {
//            logger.info("#found old template, then clear InUsed Reference of old template.");
//
//            DMATemplate template_old = templateAccessor.findTemplateByName(templateName_old);
//
//            new DeleteReferenceProcessor().clearInUsedByDmaScreen(template_old.getReference(), screenName);
//
//            templateAccessor.saveTemplate(template_old, user);
//        }
//    }

}
