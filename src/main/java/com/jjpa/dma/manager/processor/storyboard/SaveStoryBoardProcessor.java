package com.jjpa.dma.manager.processor.storyboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.binding.BindingDomainProcessor;
import com.jjpa.dma.manager.processor.reference.BindingStoryboardReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.ClearStoryboardReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

/* 
 * 20/03/2562 edit by Akanit : DMA.5.0.0 : improve speed : finished
 * 02/05/2562 add by Akanit : DMA.5.3.0 : groups : finished
 * */
@SuppressWarnings("unchecked")
public class SaveStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(SaveStoryBoardProcessor.class);

	protected AppUiAccessor appUiAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	private String user;

	public SaveStoryBoardProcessor() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {

		logger.info("Process ===> SaveStoryBoardProcessor");
		logger.debug("Process ===> SaveStoryBoardProcessor : {}", input.toJsonString());

		return saveStory(input);

	}

	private Object saveStory(ServiceData input) throws Exception {
		Map<String,Object> body = input.getData();
		user = (String) input.getValue(AppConstants.USER);
		String storyName = (String) input.getValue(DmaConstants.StoryBoard.Key.STORY_NAME);
		logger.info("step 1.1 start findStoryByName");
		StoryBoardDocument oldDoc = storyBoardAccessor.findStoryByName(storyName);
		logger.info("step 1.2 end findStoryByName");
		StoryBoardDocument storyBoardDoc = new StoryBoardDocument();
		if ((input.getValue(DmaConstants.StoryBoard.Key.ID) != null && !"".equals(((String) input.getValue(DmaConstants.StoryBoard.Key.ID)).trim()))) {
			String id = (String) input.getValue(DmaConstants.StoryBoard.Key.ID);
			if (oldDoc != null && !id.equals(oldDoc.getId())) {
				// check rename case
				throw new Exception("Name duplicated");
			}
			storyBoardDoc.setId(id);
		} else if (oldDoc != null) {
			// check create case
			throw new Exception("Name duplicated");
		}
		
		
		/* 22/05/2562 add by Akanit : fix Ref */
//		new FixReferenceProcessor().fixRefAll();
		
		
		if (oldDoc != null) {
			logger.info("Process ===> Is 'update' StoryBoard, then check needUpgrade");
			
			logger.info("step 2.1 start getScreen in storyBoard");
	        /* 01/10/2561 add by Akanit : check needUpgrade, if needUpgrade then don't save.*/
	        for(Map<String, Object> storyBoardMap : oldDoc.getStoryBoard()){
	            
	            String screenName = ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE);
	            
	            AppUiDocument appUiDoc = appUiAccessor.findAppUiByName(screenName);
	            
	            if(appUiAccessor.needUpgrade(appUiDoc)){
	                throw new Exception("Please upgrade screen in \"Screen\" menu before edit storyboard");
	            }
			}
			logger.info("step 2.1 end getScreen in storyBoard");

			storyBoardDoc.setReference(oldDoc.getReference());
			
		}else{
			logger.info("Process ===> Is 'create' StoryBoard");
		}

		if( body.containsKey(DmaConstants.StoryBoard.Key.TABINFO) ){
			Map<String,Object> tabInfo =(Map<String,Object>) body.get(DmaConstants.StoryBoard.Key.TABINFO);
			storyBoardDoc.setTabInfo(tabInfo);
		}else{
			logger.info("This is old storyboard. It's has no tabInfo");
		}

		if( body.containsKey(DmaConstants.StoryBoard.Key.APIINFO) ){
			Map<String,Object> apiInfo =(Map<String,Object>) body.get(DmaConstants.StoryBoard.Key.APIINFO);
			storyBoardDoc.setApiInfo(apiInfo);
		}else{
			logger.info("This is old storyboard. It's has no apiInfo");
		}
		
		

		storyBoardDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
		storyBoardDoc.setStoryName(storyName);
		storyBoardDoc.setRemark((String) input.getValue(DmaConstants.StoryBoard.Key.REMARK));
		storyBoardDoc.setFirstPage((String) input.getValue(DmaConstants.StoryBoard.Key.FIRST_PAGE));
		storyBoardDoc.setHomePage((String) input.getValue(DmaConstants.StoryBoard.Key.HOME_PAGE));
		storyBoardDoc.setNotificationPage((String) input.getValue(DmaConstants.StoryBoard.Key.NOTIFICATION_PAGE));
		
		
		/* 20/03/2562 add by Akanit : validate reference and auto create reference when is null. */
		Reference reference = new InitialReferenceProcessor().process(storyBoardDoc.getReference());
		storyBoardDoc.setReference(reference);
		
		
		/* 21/11/2561 add by Akanit : DMA.4.1.0 : pinbackType */
		try{
			storyBoardDoc.setPinList((List<Map<String, Object>>) input.getValue(DmaConstants.StoryBoard.Key.PIN_LIST));
		}catch (Exception e){
			storyBoardDoc.setPinList(new ArrayList<>());
		}
		
		
		/* 02/05/2562 add by Akanit : DMA.5.3.0 : groups */
		try{
			storyBoardDoc.setGroups((List<Map<String, Object>>) input.getValue(DmaConstants.StoryBoard.Key.GROUPS));
		}catch (Exception e){
			storyBoardDoc.setGroups(new ArrayList<>());
		}
		

		/* 30/05/2562 add by Akanit : {{$get._dma_param_internal_domain}} */
		for(Map<String, Object> group : storyBoardDoc.getGroups()){
			new BindingDomainProcessor().bindDMADomainToImageUrlInGroup(group);
		}
		
		
		List<Map<String, Object>> storyBoard = (List<Map<String, Object>>) input.getValue(DmaConstants.StoryBoard.Key.STORY_BOARD);
		
		
		logger.info("step 3 start fillAdditionalData");
		/**
		 * Tanawat W.
		 * add additional data
		 */
		fillAdditionalData(storyBoard);
		storyBoardDoc.setStoryBoard(storyBoard);
		logger.info("step 3 end fillAdditionalData");

		
		
		logger.info("step 4 start validateScreenIsAvalableToUsed");
		/**
		 * Tanawat W.
		 * 2018-04-04 
		 * Validate screen not found in storyboard
		 */
		validateScreenIsAvalableToUsed(storyBoardDoc);
		logger.info("step 4 end validateScreenIsAvalableToUsed");

		
		
		logger.info("step 5 start validateScreenIsUsedByOtherStoryBoardAndGetScreenRequiredReference");
		/* 13/03/2561 add by Akanit */
		List<Map<String, Object>> screenRequiredList_new = validateScreenIsUsedByOtherStoryBoardAndGetScreenRequiredReference(storyBoardDoc);
		logger.info("step 5 end validateScreenIsUsedByOtherStoryBoardAndGetScreenRequiredReference");

		
		
		logger.info("step 6 start clearInUsedReferenceToOldScreen");
		/* 19/03/2561 add by Akanit : clear inUsed reference to dmaTemplate old */
		List<Map<String, Object>> screenRequiredList_old = new ArrayList<>();
		if(oldDoc!=null){
			screenRequiredList_old = new GetReferenceProcessor().getDmaScreenRequireds(oldDoc.getReference());
		}
		
		logger.info("screenRequiredList_old = "+ screenRequiredList_old);
		logger.info("screenRequiredList_new = "+ screenRequiredList_new);
		/* 20/03/2562 add by Akanit : DMA.5.0.0 : improve speed */
		List<String> disapearScreenList = ManagerArrayList.getStringListDisapearNodeByKey(ReferenceConstants.DMA.NAME,
				screenRequiredList_old, screenRequiredList_new);
		logger.info("disapearScreenList = "+ disapearScreenList);
		
		/* 19/03/2561 add by Akanit 
		 * 20/03/2562 edit by Akanit : DMA.5.0.0 : improve speed
		 * */
		new ClearStoryboardReferenceProcessor().process(storyBoardDoc, disapearScreenList, user);
		
		logger.info("step 6 end clearInUsedReferenceToOldScreen");
		

		
		logger.info("step 7 start GenerateScreenListAndBindingReferenceProcessor");
		/* 13/03/2561 add by Akanit : DMA.4 */
		//storyBoardDoc.setScreenList(new GenerateScreenListAndBindingReferenceProcessor().process(storyBoardDoc, user));
		
		/* 20/03/2562 add by Akanit : DMA.5.0.0 : improve speed */
		List<String> increaseScreenList = ManagerArrayList.getStringListDisapearNodeByKey(ReferenceConstants.DMA.NAME,
				screenRequiredList_new, screenRequiredList_old);
		logger.info("increaseScreenList = "+ increaseScreenList);
		
		/* 01/03/2562 create by Akanit : for DMA5 
		 * 20/03/2562 edit by Akanit : DMA.5.0.0 : improve speed
		 * */
		new BindingStoryboardReferenceProcessor().process(storyBoardDoc, increaseScreenList, user);

		
		/**
		 * Tanawat W.
		 * DMA 5.0 
		 * 
		 * Cancel create Jsonnette setScreenList
		 * 
		 */
		storyBoardDoc.setScreenList(new ArrayList<>());
		logger.info("step 7.1 Cancel create Jsonnette setScreenList");

		logger.info("step 7 end GenerateScreenListAndBindingReferenceProcessor");

		
		
		/* 17/05/2561 add by Akanit */
		storyBoardDoc.setLastUpdate(new Date());

		
		
		logger.info("step 8 start callHook : DMA.5 ");
		/**
		 * Tanawat W.
		 * 
		 * Test plugin-react
		 */
		Map<String,Object> resultContext = callHook("SaveStoryBoard", input.getData()).json();
		
		if( (boolean)resultContext.get("resultSuccess") == false) {
			throw new Exception((String) resultContext.get("resultMessage"));
		}
		logger.info("step 8 end callHook : DMA.5");
		
		
		
		logger.info("step 9 start saveStoryBoard");
		Object result = storyBoardAccessor.saveStoryBoard(storyBoardDoc, user);
		logger.info("step 9 end saveStoryBoard");
		
		return result;
	}

	private void validateScreenIsAvalableToUsed(StoryBoardDocument storyBoardDoc) throws Exception {
		logger.info("## validateScreenIsAvalableToUsed ##");
		
		Map<String, Object> screenList = new LinkedHashMap<>();
		Map<String, Object> screenInActionList = new LinkedHashMap<>();
		for (Map<String, Object> storyBoard : storyBoardDoc.getStoryBoard()) {
			String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
			screenList.put(source, source);

			/* get parameter from app_ui_config */
			List<Map<String, Object>> actionList = new ArrayList<>(
					(List<Map<String, Object>>) storyBoard.get(DmaConstants.StoryBoard.Key.ACTION_LIST));
			for (Map<String, Object> action : actionList) {
				String targetType = DataUtils.getStringValue(action, DmaConstants.StoryBoard.Key.TARGET_TYPE, "");
				if (DmaConstants.StoryBoard.Value.SCREEN.equalsIgnoreCase(targetType)) {
					String targetScreen = DataUtils.getStringValue(action, DmaConstants.StoryBoard.Key.TARGET, null);
					if (targetScreen != null) {
						screenInActionList.put(targetScreen, targetScreen);
					}

				}

				/**
				 * validate feature condition target screen
				 * 
				 */
				if (action.containsKey(DmaConstants.StoryBoard.Key.TARGET_CONDITIONS)) {
					Map<String, Object> targetConditions = (Map<String, Object>) action
							.get(DmaConstants.StoryBoard.Key.TARGET_CONDITIONS);
					if (DataUtils.getBooleanValue(targetConditions,
							DmaConstants.StoryBoard.Key.TargetConditions.USE_CONDITIONS, false) == true) {
						List<Map<String, Object>> conditionsList = (List<Map<String, Object>>) targetConditions
								.get(DmaConstants.StoryBoard.Key.TargetConditions.CONDITIONS_LIST);

						for (Map<String, Object> caseCondition : conditionsList) {
							String target = DataUtils.getStringValue(caseCondition,
									DmaConstants.StoryBoard.Key.TargetConditions.ConditionsList.TARGET, null);
							if (target != null) {
								screenInActionList.put(target, target);
							}

						}
					}
				}
			}

		}
		logger.info("## screenList ## :" + screenList);
		logger.info("## screenInActionList ## :" + screenInActionList);
		Map<String, Object> map = new LinkedHashMap<>(screenInActionList);
		map.put(storyBoardDoc.getFirstPage(), storyBoardDoc.getFirstPage());
		map.put(storyBoardDoc.getHomePage(), storyBoardDoc.getHomePage());

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (screenList.containsKey(key)) {
				continue;
			} else {
				throw new Exception("Invalid. Screen \"" + value + "\" is not found in storyboard.");
			}
		}

		// Map<String,Object> mergeScreen = new LinkedHashMap<>(screenInActionList);
		// mergeScreen.putAll(screenList);

	}

	/* 13/03/2561 edit by Akanit */
	private List<Map<String, Object>> validateScreenIsUsedByOtherStoryBoardAndGetScreenRequiredReference(
			StoryBoardDocument storyBoardDoc) throws Exception {
		logger.info("#do validateScreenIsUsedByOtherStoryBoard and getScreenRequiredReference");

		List<Map<String, Object>> screenRequiredList = new ArrayList<>();

		String source;
		AppUiDocument appUiDoc;
		Map<String, Object> screenRequired;

		for (Map<String, Object> storyBoard : storyBoardDoc.getStoryBoard()) {

			/* get source : old system 'source' is 'uiName' */
			source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);

			/* find appUi by name : mongoDB */
			appUiDoc = appUiAccessor.findAppUiByName(source);

			/* keep screen required use to return */
			screenRequired = new LinkedHashMap<>();
			screenRequired.put(ReferenceConstants.DMA.NAME, source);
			screenRequiredList.add(screenRequired);

			/* if reference not found : screen is old version, please update or save screen again */
			if (appUiDoc.getReference() == null) {
				String errorMessage = "Reference of Screen \"" + appUiDoc.getUiName()
						+ "\" not found, this screen is old version, Please save this screen again.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}

			/* get storyBoard List : at screen InUsed by storyBoard? */
			List<Map<String, Object>> storyBoardInUsedList_appUi = new GetReferenceProcessor().getInUsedByDmaStroyBoards(appUiDoc.getReference());

			/* check this screen is used by other storyBoard? */
			if (storyBoardInUsedList_appUi == null || storyBoardInUsedList_appUi.size() == 0) {

				logger.info("Screen \"" + appUiDoc.getUiName()
						+ "\" is not used by other StroyBoard. So this screen can be use.");
				continue;

			} else {

				for (Map<String, Object> storyBoardInUsed_appUi : storyBoardInUsedList_appUi) {

					String storyBoardInUsedName_appUi = ManagerMap.getString(storyBoardInUsed_appUi,
							ReferenceConstants.DMA.NAME);
					if (storyBoardDoc.getStoryName().equals(storyBoardInUsedName_appUi)) {
						logger.info("Screen \"" + appUiDoc.getUiName() + "\" is used by this StroyBoard.");
						continue;
					} else {

						String errorMessage = "Screen \"" + appUiDoc.getUiName() + "\" is used by other StoryBoard \""
								+ storyBoardInUsedName_appUi + "\"";
						logger.info(errorMessage);
						throw new Exception(errorMessage);
					}
				}
			}
		}

		return screenRequiredList;
	}

	private void fillAdditionalData(List<Map<String, Object>> storyBoard)throws Exception{
		for (Map<String,Object> screenMap : storyBoard) {
			String source = DataUtils.getStringValue(screenMap, DmaConstants.StoryBoard.Key.SOURCE, null);
			AppUiDocument screenUi = appUiAccessor.findAppUiByName(source);

			screenMap.put(DmaConstants.StoryBoard.Key.TEMPLATE_TYPE, screenUi.getTemplateType());
		}
	}
}
