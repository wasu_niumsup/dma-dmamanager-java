package com.jjpa.db.mongodb.bean;

import java.util.List;

public class Pager<T> {

	public static final int MAX_PAGE_SIZE = 500;
	public static final int DEFAULT_LIMIT = 50; 
	
	private int pageNumber;
	private int pageSize;
	private int totalCount = 0;
	private int pageCount = 0;
	private List<T> list;
	private Class<T> entityClass;

	public Pager(int pageNumber, int pageSize, Class<T> entityClass) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.entityClass = entityClass;
	}
	
	public Class<T> getEntityClass() { 
        return entityClass; 
    }

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		if (pageNumber < 0) {
			pageNumber = 1;
		}
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		if (pageSize < 1) {
			pageSize = 1;
		} else if (pageSize > MAX_PAGE_SIZE) {
			pageSize = MAX_PAGE_SIZE;
		}
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageCount() {
		pageCount = totalCount / pageSize;
		if (totalCount % pageSize > 0) {
			pageCount++;
		}
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
