package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.DMAConfigurationDocument;

public interface ConfigurationAccessor {
	
	public Object saveConfiguration(DMAConfigurationDocument configDoc, String user) throws Exception;
	public List<DMAConfigurationDocument> listConfiguration() throws Exception;
	public Map<String,Object> listConfigurationName() throws Exception;
	public void deleteConfiguration(ServiceData input) throws Exception;
	public DMAConfigurationDocument findConfigurationByName(String configName) throws Exception;
	

}
