package com.jjpa.dma.manager.processor.actionchaintemplate;

import java.io.File;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;

public class UploadActionChainTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(UploadActionChainTemplateProcessor.class);

	public Object process(HttpServletRequest request, CommonsMultipartFile file) throws Exception {
		logger.info("Process ===> UploadActionChainTemplateProcessor");
		
    	Map<String, Object> output = new LinkedHashMap<>();
    	
    	
    	/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
		String filePath = ManagerMap.getString(propMap, PropConstants.FileLocal.ActionChain.Key.FILE_PATH, "");
		
    	
    	/* validate file zip type */
    	validateFileType(file.getOriginalFilename(), ".zip");
    	
    	
    	/* create path of folder is unique name. */
		String uniqueFolder = String.valueOf(new Date().getTime());		
		String extractZipFilePath = filePath + uniqueFolder;
		
    	
		ManagerStore.extractZipFile(file, extractZipFilePath);
    	
    	
    	/* process loop SaveActionChain from file. */
    	try{
    		
	    	File folderExtractZip = new File(extractZipFilePath);
			if(folderExtractZip.exists()){
				
				Map<String, Object> input;
				SaveActionChainTemplateProcessor saveActionChainTemplateProc = new SaveActionChainTemplateProcessor();
				
				for(File fileEntry : folderExtractZip.listFiles()){
					if(!fileEntry.isDirectory()){
						
						input = new LinkedHashMap<>();
						input.put("user", "autoSaveActionChainTemplateFromZipFileUpload");
						input.put("actionChainCode", ManagerStore.readJsonStringFromFile(fileEntry, true));

						saveActionChainTemplateProc.process(null, new ServiceData(input));

					}
				}
			}
			
    	}catch(Exception e){
    		String errorDesc = "File is unable to be restored. Please try again.";
    		logger.error(errorDesc);
			throw new Exception(errorDesc);
    	}finally {
    		ManagerStore.deleteFile(extractZipFilePath);
		}
    	
    	
    	logger.info("output="+ output);
		logger.info("End Procces ===> UploadActionChainTemplateProcessor");
		return output;
	}
	
	
	
	private void validateFileType(String fileName, String fileType) throws Exception{
		if(fileName==null)return;
		if(fileType==null)return;
		
		String[] fileName_sp = fileName.split("\\.");
		String type = "."+fileName_sp[fileName_sp.length-1];
		if(!type.equals(fileType)){
			String errorDesc = "Invalid file type. File type must be " + fileType;
			logger.info(errorDesc);
			throw new Exception(errorDesc);
		}	
	}

}
