package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardExtraTargetTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardExtraTargetTypeDocument;

public class StoryBoardExtraTargetTypeAccessorImpl implements StoryBoardExtraTargetTypeAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected StoryBoardExtraTargetTypeAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Map<String,Object> listStoryBoardExtraTargetType(ServiceData input) throws Exception{
		List<Map<String, Object>> extraTypeList = new ArrayList<Map<String, Object>>();
		List<StoryBoardExtraTargetTypeDocument> storyboardExtraTargetTypeList = mongoDbAccessor.findAll(StoryBoardExtraTargetTypeDocument.class);
		for(StoryBoardExtraTargetTypeDocument type : storyboardExtraTargetTypeList){
			Map<String, Object> typeMap = new HashMap<String, Object>();
			typeMap.put(DmaConstants.StoryboardExtraTargetType.Key.UI_NAME, type.getUiName());
			typeMap.put(DmaConstants.StoryboardExtraTargetType.Key.CONFIG_PARAMETER, type.getConfigParameter());
			
			extraTypeList.add(typeMap);
		}
		
		Map<String, Object> extraTypeMap = new HashMap<String, Object>();
		extraTypeMap.put(DmaConstants.StoryboardExtraTargetType.Key.APP_UI_LIST, extraTypeList);
		
		return extraTypeMap;
	}

	
	public StoryBoardExtraTargetTypeDocument findStoryBoardExtraTargetTypeByName(String uiName) throws Exception{
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.StoryboardExtraTargetType.Path.UI_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(uiName), Pattern.CASE_INSENSITIVE)));
		return mongoDbAccessor.findOneByCriteria(findByName, StoryBoardExtraTargetTypeDocument.class);
	}
	
	public Object saveStoryBoardExtraTargetType(StoryBoardExtraTargetTypeDocument document, String user) throws Exception{
		return mongoDbAccessor.save(document, user);
	}
	
	public void deleteStoryBoardExtraTargetType(String uiName, String user) throws Exception{
		mongoDbAccessor.delete(findStoryBoardExtraTargetTypeByName(uiName), user);
	}
	
}
