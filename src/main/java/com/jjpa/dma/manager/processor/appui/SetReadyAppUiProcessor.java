package com.jjpa.dma.manager.processor.appui;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class SetReadyAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(SetReadyAppUiProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public SetReadyAppUiProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SetReadyAppUiProcessor");
		logger.debug("Process ===> SetReadyAppUiProcessor : {}", input.toJsonString());
		
		return setReady(input);
		
	}
	
	private Map<String,Object> setReady(ServiceData input) throws Exception {
		AppUiDocument document = appUiAccessor.findAppUiByName((String)input.getValue("uiName"));
		document.setStatus(DmaManagerConstant.STATUS_READY);
		String user = (String)input.getValue("user");
		
		/* 25/03/2562 add by Akanit : requirement from siam */
		if(document.getUrlScreenShot()==null){
			logger.info("not found '"+ DmaConstants.AppUI.Key.URL_SCREEN_SHOT +"' then setUrlScreenShot = ''");
			document.setUrlScreenShot("");
		}
		
		appUiAccessor.saveUi(document, user);
		
		Map<String,Object> appUi = new HashMap<String,Object>();
		appUi.put("uiName", document.getUiName());
		appUi.put("remark", document.getRemark());
		appUi.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		appUi.put("lastPublish", document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
		appUi.put("status", document.getStatus());
		
		return appUi;
	}
	
}
