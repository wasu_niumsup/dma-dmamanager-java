package com.jjpa.dma.manager.processor.storyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

@SuppressWarnings("unchecked")
public class ListGlobalValueStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListGlobalValueStoryBoardProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListGlobalValueStoryBoardProcessor");
		logger.debug("Process ===> ListGlobalValueStoryBoardProcessor : {}", input.toJsonString());
		
		Map<String, Object> storyBoardMap = input.getData();
		List<Map<String, Object>> storyBoardList = (List<Map<String, Object>>)storyBoardMap.get("storyBoard");
		
		List<Map<String, Object>> storyBoardGlobalList = new ArrayList<Map<String, Object>>();
		
		for(Map<String, Object> storyBoardScreen : storyBoardList){
			List<Map<String, Object>> screenGlobalList = (List<Map<String, Object>>)storyBoardScreen.get("globalOutputList");
			for(Map<String, Object> screenGlobal : screenGlobalList){

				String valueFrom = DataUtils.getStringValue(
					screenGlobal,DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM,
					DmaConstants.StoryBoard.Value.GLOBAL);


				if(DataUtils.getBooleanValue(screenGlobal.get("keep"))){
					Map<String, Object> globalMap = new HashMap<String, Object>();
					globalMap.put("globalName", DataUtils.getStringValue(screenGlobal,"globalName","") );
					globalMap.put("fieldType", DataUtils.getStringValue(screenGlobal,"fieldType","")  );
					globalMap.put("source", DataUtils.getStringValue(storyBoardScreen,"source","")    );
					globalMap.put("api", "");
					globalMap.put("action", "");
					globalMap.put("dataStructure", DataUtils.getStringValue(screenGlobal,"dataStructure","")  );
					globalMap.put(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM, valueFrom);

					storyBoardGlobalList.add(globalMap);
				}else{
					
				}
			}
			List<Map<String, Object>> screenActionList = (List<Map<String, Object>>)storyBoardScreen.get("actionList");
			for(Map<String, Object> screenAction : screenActionList){
				List<Map<String, Object>> globalOutputList = (List<Map<String, Object>>)screenAction.get("globalOutputList");
				if(globalOutputList!=null){
					for(Map<String, Object> actionOutput : globalOutputList){


						String valueFrom = DataUtils.getStringValue(
							actionOutput,DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM,
							DmaConstants.StoryBoard.Value.GLOBAL);
							

						if(DataUtils.getBooleanValue(actionOutput.get("keep"))){
							Map<String, Object> globalMap = new HashMap<String, Object>();
							globalMap.put("globalName", DataUtils.getStringValue(actionOutput,"globalName",""));
							globalMap.put("fieldType", DataUtils.getStringValue(actionOutput,"fieldType","") );
							globalMap.put("source",DataUtils.getStringValue(storyBoardScreen,"source","") );
							globalMap.put("api",  DataUtils.getStringValue(screenAction,"apiName","")   );
							globalMap.put("action",  DataUtils.getStringValue(screenAction,"actionName","") );
							globalMap.put("dataStructure", DataUtils.getStringValue(actionOutput,"dataStructure","")  );
							globalMap.put(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM, valueFrom);
							storyBoardGlobalList.add(globalMap);
						}
					}
				}else{
					
				}
			}
		}
		
		Map<String, Object> globalListMap = new HashMap<String, Object>();
		globalListMap.put("globalList", storyBoardGlobalList);
		
		logger.debug("Process ===> ListGlobalValueStoryBoardProcessor Output : {}", globalListMap);
		
		return globalListMap;
		
	}

}
