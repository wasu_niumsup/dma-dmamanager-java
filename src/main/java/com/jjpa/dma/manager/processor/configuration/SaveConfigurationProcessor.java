package com.jjpa.dma.manager.processor.configuration;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ConfigurationAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAConfigurationDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SaveConfigurationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SaveConfigurationProcessor.class);
		
	protected ConfigurationAccessor configurationAccessor;
	
	public static void main(String[] arg){
		
		logger.info("ddd");
		Map<String,Object> mp = new LinkedHashMap<>();
		mp.put("$actions", "ddd");
		String js = JsonPath.parse(mp).jsonString();
		logger.info(js);
		

	}

	public SaveConfigurationProcessor() {
		this.configurationAccessor = SpringApplicationContext.getBean("ConfigurationAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveConfigurationProcessor");
		logger.debug("Process ===> SaveConfigurationProcessor : {}", input.toJsonString());
		
		return saveConfiguration(input);
		
	}
	
	private Object saveConfiguration(ServiceData input) throws Exception {

		logger.info("Process ===> SaveConfigurationProcessor saveConfiguration");

		String templateCode = (String)input.getValue("templateCode");
		logger.info("Process ===> SaveConfigurationProcessor saveConfiguration templateCode " + templateCode);
		Map<String, Object> templateMap = JsonPath.parse(templateCode).json();
	
		String user = (String)input.getValue("user");
		String configName = (String)input.getValue("configName");
		DocumentContext ctx = JsonPath.parse(templateMap);
		String nativeJson = (String)ctx.jsonString();
		Map<String,String> data = new HashMap<String, String>();
		data.put("data", nativeJson);
        DMAConfigurationDocument configDoc = new DMAConfigurationDocument();
        configDoc.setConfigName(configName);
        configDoc.setNativeJson(data);
		
		return configurationAccessor.saveConfiguration(configDoc,user);
	}

}
