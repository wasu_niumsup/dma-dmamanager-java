package com.jjpa.dma.manager.binding.action;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

import blueprint.util.StringUtil;

@SuppressWarnings("unchecked")
public class SetGlobalToApi implements ActionBinding {

	private final Logger logger = LoggerFactory.getLogger(SetGlobalToApi.class);

	private boolean skipBinding = false;

	public interface ISetData {
		public DocumentContext setCtx(DMAActionChainDocument document);

		public String getBindingPath(DMAActionChainDocument document);

		public Boolean skipCondition(String parrent_serviceName, String child_serviceName);
	}

	private ISetData interfaceSetData = null;

	public SetGlobalToApi() {
		interfaceSetData = null;
		skipBinding = true;
	}

	public SetGlobalToApi(ISetData inter) {
		interfaceSetData = inter;
	}

	private DocumentContext setCtx(DMAActionChainDocument document) {
		return JsonPath.parse(document.getNativeJson().get("data"));
	}

	private String getBindingPath(DMAActionChainDocument document) {
		String bindingPath = "";
		List<Map<String, Object>> bindingList = document.getBinding();
		for (Map<String, Object> binding : bindingList) {
			String bindingName = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			if ("options".equals(bindingName)) {
				bindingPath = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));
				break;
			}
		}

		return bindingPath;
	}

	private Boolean skipCondition(String parrent_serviceName, String child_serviceName) {
		Boolean result = false;

		Boolean condtion = !StringUtil.isBlank(child_serviceName)
				&& parrent_serviceName.trim().equalsIgnoreCase(child_serviceName.trim());
		if (!condtion) {
			result = true;
		}
		logger.info(" [Private] isSkip : " + result + " , parrent_serviceName : " + parrent_serviceName
				+ " ,child_serviceName : " + child_serviceName);
		return result;
	}

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard,
			Map<String, Object> eventMap) throws Exception {
		logger.info(
				"[Start ] =======================Binding set global to api in Action====================================================================");
		logger.info("Binding set global to api in Action : Is skip binding ? : " + skipBinding);
		logger.info("Using Interface : " + (interfaceSetData != null));
		logger.info("storyBoard : " + storyBoard);
		logger.info("eventMap : " + eventMap);

		String action_name = DataUtils.getStringValue(eventMap.get(DmaConstants.StoryBoard.Key.ACTION_NAME));
		String parrent_serviceName = DataUtils.getStringValue(eventMap.get(DmaConstants.StoryBoard.Key.API_NAME));
		logger.info("action_name : " + action_name);
		logger.info("parrent_serviceName : " + parrent_serviceName);

		DocumentContext ctx = interfaceSetData == null ? this.setCtx(document) : interfaceSetData.setCtx(document);
		logger.info("raw : " + ctx.jsonString());

		List<Map<String, Object>> globalInputList = (List<Map<String, Object>>) eventMap
				.get(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST);

		String bindingPath = interfaceSetData == null ? this.getBindingPath(document)
				: interfaceSetData.getBindingPath(document);
		logger.info("bindingPath : " + bindingPath);

		if (globalInputList != null) {
			logger.info("globalInputList : " + globalInputList.size());
			for (Map<String, Object> globalInput : globalInputList) {
				logger.info("globalInput : " + globalInput);
				boolean useGlobal = DataUtils
						.getBooleanValue(globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.USE_GLOBAL));
				String child_serviceName = DataUtils
						.getStringValue(globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.SERVICE_NAME));

				boolean isSkip = interfaceSetData == null ? this.skipCondition(parrent_serviceName, child_serviceName)
						: interfaceSetData.skipCondition(parrent_serviceName, child_serviceName);

				if (useGlobal && !isSkip) {
					logger.info("###found useGlobal");

					/* get parameter */
					String localName = DataUtils.getStringValue(globalInput.get(DmaConstants.StoryBoard.Key.NAME));
					String resultValue = "";					
					
					resultValue = JasonNetteUtils.generateResultValue(globalInput);

					logger.info("bindingPath={}", bindingPath);
					logger.info("localName={}", localName);
					logger.info("resultValue={}", resultValue);


					if(!skipBinding){
						ctx.put(bindingPath, localName, resultValue);
					}
				}
			}
		}
		logger.info("Binding set global to api in Action OUTPUT : " + ctx.jsonString());
		logger.info(
				"[End ] =======================Binding set global to api in Action====================================================================");
		return ctx;
	}


}
