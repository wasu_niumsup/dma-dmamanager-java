package com.jjpa.dma.manager.processor.binding;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.JasonPathConstants;

public class BindingActionProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingActionProcessor.class);

	public Map<String, Object> process(Map<String, Object> dataBinding, List<Map<String, Object>> actionChainList)
			throws Exception {

		DocumentContext masterTemplate = JsonPath.parse(dataBinding);

		for (Map<String, Object> actionChain : actionChainList) {

			for (String key : actionChain.keySet()) {

				if (actionChain.get(key) != null) {
					masterTemplate.put(JasonPathConstants.UiJson.Root.ACTIONS, key, actionChain.get(key));

					/**
					* Tanawat W.
					* [2018-03-20] Add feature initial screen
					* 
					*/
					if (key.equalsIgnoreCase(GenerateInitialScreenActionProcessor.Key.actionName)) {
						logger.info("found initialScreenAction");
						new BindingTriggerInitialScreenProcessor().process(masterTemplate);
					}

				}

				break;
			}
		}

		logger.info("masterTemplate=" + masterTemplate);
		logger.info("dataBinding=" + dataBinding);

		return dataBinding;
	}

}
