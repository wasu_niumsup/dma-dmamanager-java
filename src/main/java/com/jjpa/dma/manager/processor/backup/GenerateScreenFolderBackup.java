package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerStore;

public class GenerateScreenFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateScreenFolderBackup.class);

	protected AppUiAccessor appUiAccessor;
	
	public GenerateScreenFolderBackup() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	public void process(Reference reference, String directoryFileBackup) throws Exception {
		logger.info("process : generate screen backup folder");
		
		List<Map<String, Object>> dmaScreenRequiredList = new GetReferenceProcessor().getDmaScreenRequireds(reference);
		if(dmaScreenRequiredList==null){
			return;
		}
		
		String screenName;
		for(Map<String, Object> dmaScreenRequired : dmaScreenRequiredList){
			
			/* find screen in mongo */
			screenName = ManagerMap.getString(dmaScreenRequired, ReferenceConstants.DMA.NAME);
			AppUiDocument screenDoc = appUiAccessor.findAppUiByName(screenName);
			if(screenDoc==null){
				String errorMessage = "Screen '"+ screenName +"' not found in DB.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/screen/{screenName}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.SCREEN + File.separator + screenName;
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(screenDoc.toMap(), directoryFile);
		}

	}
}
