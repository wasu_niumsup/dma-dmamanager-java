package com.jjpa.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DmaManagerConstant {
	
	public static Set<String> BACK_TYPE = new HashSet<String>();
	static {
		BACK_TYPE.add("firstpage");
		BACK_TYPE.add("home");
		BACK_TYPE.add("stack");
		BACK_TYPE.add("none");
	}
	
	public static Set<String> THEME = new HashSet<String>();
	static {
		THEME.add("dark");
		THEME.add("light");
		THEME.add("gray");
	}
	
	public static String STATUS_DRAFT = "Draft";
	public static String STATUS_READY = "Ready";
	
	public static String STATUS_ACTIVE = "Active";
	public static String STATUS_INACTIVE = "Inactive";
	
	public static Set<String> ACTIVE_STATUS = new HashSet<String>();
	static {
		ACTIVE_STATUS.add(STATUS_ACTIVE);
		ACTIVE_STATUS.add(STATUS_INACTIVE);
	}

	public static Map<String ,String> DYNAMIC_FORM_INPUT_TYPE = new HashMap<String, String>();
	static {
	// key = fieldType from Service Gateway, value = type in jsonTemplate for Mobile
		DYNAMIC_FORM_INPUT_TYPE.put("textfield", "textfield");
		DYNAMIC_FORM_INPUT_TYPE.put("dynamicdropdown", "dropdown");
	}
	
	public static final String REQUEST_HEADER 	= "requestHeader";
	public static final String RESPONSE_HEADER 	= "responseHeader";
	
	public static final String RESPONSE_CODE 	= "responseCode";
	public static final String RESPONSE_DESC 	= "responseDescription";
	
	public static final String REQUEST_DATA 	= "requestData";
	public static final String RESPONSE_DATA 	= "responseData";
	
	public static final String RESULT_SUCCESS 	= "resultSuccess";
	public static final String RESULT_MESSAGE 	= "resultMessage";
	public static final String RESULT_CODE   	= "resultCode";
	public static final String RESULT_DATA   	= "resultData";
	public static final String RESULT_FROM 		= "resultFrom";
	
	public static final String USERNAME 		= "username";
	public static final String MESSAGE 			= "message";
	public static final String MESSAGE_TYPE		= "messageType";

	public class MESSAGE_VALUE {
		public static final String PAGE = "page";
		public static final String OPEN = "Open : ";
		public static final String CALL_API = "Call : ";
		public static final String GET_CONFIG = "Get Config";
		public static final String GET_PREVIEW = "Get Config Preview";
	}
	public class MESSAGE_TYPE_VALUE {
		public static final String INITIAL_SCREEN = "InitialScreen";
		public static final String LOAD_SCREEN = "LoadScreen";
		public static final String CALL_API = "CallAPI";
		public static final String CALL_SUB_API = "CallSubAPI";
		public static final String GET_CONFIG = "GetConfig";

	}
	
	public static final String DMA_INITIAL_SCREEN = "_dma_initial_screen";
	public static final String DMA_TYPE = "_dma_type";
	public static final String DMA_ENTITY = "_dma_entity";
	public static final String DMA_STORYBOARD = "_dma_storyboard";
	
	public static final String DMA_SOURCE = "_dma_source";
	public static final String DMA_ACTION = "_dma_action";
	public static final String DMA_TARGET_PAGEGNAME = "_dma_target_pagename";
	public static final String DMA_DATA = "_dma_data";
	public static final String DMA_UUID = "_dma_uuid";
	public static final String DMA_API = "_dma_api";
	public static final String DMA_SERVICE = "_dma_service";
	public static final String DMA_EXTRA = "_dma_extra";
	public static final String DMA_FIELD = "_dma_field";
	public static final String DMA_RELATE_FROM = "_dma_relate_from";
	
	public static final String PREFIX_APP_GLOBAL = "_app_global_";
	
	public static final String EXTRA_REQUEST_URL = "requestUrl";
	public static final String EXTRA_DOMAIN = "domain";
	public static final String EXTRA_REQUEST_DATA = "requestData";
	public static final String EXTRA_REQUEST_CHANGEPAGE = "changepage";
	public static final String EXTRA_SERVICE_UUID = "serviceUUID";
	
}
