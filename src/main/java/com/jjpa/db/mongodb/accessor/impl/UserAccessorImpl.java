package com.jjpa.db.mongodb.accessor.impl;

import java.util.Date;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.UserAccessor;
import com.jjpa.db.mongodb.document.dma.manager.UserDocument;

import blueprint.util.Base64;
import blueprint.util.StringUtil;

public class UserAccessorImpl implements UserAccessor {

	protected MongoDbAccessor mongoDbAccessor;

	protected UserAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}

	@Override
	public UserDocument save(UserDocument user, String username) throws Exception {
		UserDocument old = null;
		String password = user.getPassword();
		if(StringUtil.isBlank(password)){
			password = "";
		}
		password = Base64.encode(password);
		user.setPassword(password);
		String id = user.getId();
		Query query = new Query(Criteria.where("username").is(user.getUsername()));
		if (id != null && (old=mongoDbAccessor.findById(id, UserDocument.class)) != null){
			user.setId(old.getId());
		}else if((old=mongoDbAccessor.findOneByCriteria(query, UserDocument.class)) != null){
			user.setId(old.getId());
		}else{
			user.setId(null);
		}
		return mongoDbAccessor.save(user, username);
	}

	@Override
	public UserDocument findOne(String username) throws Exception {
		Query query = new Query(Criteria.where("username").is(username));
		UserDocument user = mongoDbAccessor.findOneByCriteria(query,  UserDocument.class);
		return user;
	}


	@Override
	public UserDocument authen(String username, String password) throws Exception {
		Query query = new Query(Criteria.where("username").is(username));
		if(StringUtil.isBlank(password)){
			password = "";
		}
		password = Base64.encode(password);
		query.addCriteria(Criteria.where("password").is(password));

		Update update = new Update();
		update.set("lastLoggedIn", new Date());
		
		UserDocument user = mongoDbAccessor.findOneAndModify(query, update, UserDocument.class);
		return user;
	}

	@Override
	public UserDocument changePassword(String username, String password, String newPassword) throws Exception {
		Query query = new Query(Criteria.where("username").is(username));
		if(StringUtil.isBlank(password)){
			password = "";
		}
		password = Base64.encode(password);
		query.addCriteria(Criteria.where("password").is(password));
			
		Update update = new Update();
		if(StringUtil.isBlank(newPassword)){
			newPassword = "";
		}
		newPassword = Base64.encode(newPassword);
		update.set("password", newPassword);
		update.set("lastUpdateDate", new Date());

		UserDocument user = mongoDbAccessor.findOneAndModify(query, update, UserDocument.class);
		return user;
	}

}
