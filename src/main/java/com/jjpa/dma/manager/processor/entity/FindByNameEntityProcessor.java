package com.jjpa.dma.manager.processor.entity;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByNameEntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(FindByNameEntityProcessor.class);
		
	protected EntityAccessor entityAccessor;
	
	public FindByNameEntityProcessor() {
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByNameEntityProcessor");
		logger.debug("Process ===> FindByNameEntityProcessor : {}", input.toJsonString());
		
		String entityName = (String)input.getValue(DmaConstants.Entity.Key.ENTITY_NAME);
		return entityAccessor.findEntityByName(entityName);
		
	}

}
