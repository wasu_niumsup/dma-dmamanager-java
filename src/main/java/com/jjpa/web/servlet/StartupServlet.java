package com.jjpa.web.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jjpa.dma.manager.processor.api.PatchVersion;

//import web.fileproperties.SystemConfig;
//import web.processor.app.ResponseMessageProcessor;
//import web.processor.servicegateway.api.common.PatchVersion;
//import web.servicegateway.constants.SystemConstants;

/* 04/01/2561 add by Akanit */
/**
 * Servlet implementation class StartupServlet
 */
public class StartupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StartupServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		try {
//			String keySaveCache = SystemConstants.War.SERVICE_GATEWAY + "_" + SystemConstants.Cache.Key.RESPONSE_MESSAGE;
//			new ResponseMessageProcessor().loadAndSaveCache(keySaveCache);
			
//			new SystemConfig().loadConfig();
			
			new PatchVersion().process(config.getServletContext());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
