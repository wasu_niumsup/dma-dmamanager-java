package com.jjpa.dma.manager.processor.restore;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerStore;

public class ScreenRestore {

	private static final Logger logger = LoggerFactory.getLogger(ScreenRestore.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public ScreenRestore() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	
	public List<String> process(String filePath, String user) throws Exception {
		logger.info("Process ===> ScreenRestore");
		logger.info("filePath="+ filePath);
		
		List<String> screenList = new ArrayList<>();
		
		File file = new File(filePath);
		if(file.exists()){
			
			AppUiDocument document;
			Map<String, Object> jsonMap;
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
					
					screenList.add(fileEntry.getName());
					
					jsonMap = ManagerStore.readJsonMapFromFile(fileEntry, true);
					jsonMap.remove("id");
					
					/* 16/05/2561 new lastUpdate, clear lastPublish */
					jsonMap.put(MongoConstants.Path.LAST_UPDATE, new Date());
					jsonMap.put(MongoConstants.Path.LAST_PUBLISH, "");
					
					document = new AppUiDocument(jsonMap);
					
					appUiAccessor.restoreUi(document, user);
					logger.info("restore ScreenName="+ document.getUiName());
				}
			}
		}
		
		logger.info("End Process ===> ScreenRestore");
		return screenList;
	}
}
