package com.jjpa.common.util;

/**
 * For validate name of variable
 * 
 * @author adirak-pc
 *
 */
public class NameUtils {

	// Prevent new instance
	private NameUtils() {
	}

	/**
	 * delete space in name and lowerCase first string index of name
	 * 
	 * @param stringValue
	 */
	public static String getVariableName(String stringValue) {
		StringBuffer sb = new StringBuffer();
		boolean first = true;
		for (int i = 0; i < stringValue.length(); i++) {
			char c = stringValue.charAt(i);
			if (isAcceptedChar(c)) {
				if (first) {
					String s = Character.toString(Character.toLowerCase(c));
					sb.append(s);
					first = false;
				} else {
					sb.append(c);
				}
			}
		}

		if (sb.length() <= 0 || isNumber(Character.toString(sb.charAt(0)))) {
			return "";
		}

		return sb.toString();
	}

	public static String getGeneralName(String stringValue) {
		String name = getVariableName(stringValue);
		if (name.length() > 0) {
			Character c = name.charAt(0);
			String s = Character.toString(Character.toUpperCase(c)) + name.substring(1);
			return s;
		}
		return "";
	}

	/**
	 * Validate the name of variable in java
	 * 
	 * @param name
	 * @return valid
	 */
	public static boolean isValidName(String name) {
		if (name != null && !name.isEmpty()) {
			char first = name.charAt(0);
			if (isAcceptedChar(first)) {
				if (!isNumber(first + "")) {
					for (int i = 1; i < name.length(); i++) {
						char ch = name.charAt(i);
						if (!isAcceptedChar(ch)) {
							return false;
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * filter the character by remove every char is not a-z and number except
	 * '_'
	 * 
	 * @param c
	 * @return accept
	 */
	public static boolean isAcceptedChar(char c) {
		if (c >= 48 && c <= 57) {
			return true;
		} else if (c >= 65 && c <= 90) {
			return true;
		} else if (c >= 97 && c <= 122) {
			return true;
		} else if (c == 95) {
			// under scroll
			return true;
		}
		return false;
	}

	/**
	 * Check the string whether number
	 * 
	 * @param text
	 * @return
	 */
	public static boolean isNumber(String text) {
		try {
			Double.parseDouble(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Find the number of name
	 * @param name
	 * @return num
	 */
	public static int findNumberOfName(String name) {
		if (name != null && name.length() > 1) {
			StringBuilder sb = new StringBuilder();
			for (int i = name.length() - 1; i > 0; i--) {
				char c = name.charAt(i);
				if (c >= 48 && c <= 57) {
					sb.insert(0, c);
				} else {
					break;
				}
			}
			try {
				return Integer.parseInt(sb.toString());
			} catch (Exception e) {
			}
		}
		return 0;
	}
	
	/**
	 * Cut the lasted index number int name
	 * @param name
	 * @return nameNoNumber
	 */
	public static String cutNumberInName(String name) {
		if (name != null && name.length() > 1) {
			StringBuilder sb = new StringBuilder(name);
			for (int i = name.length() - 1; i > 0; i--) {
				char c = name.charAt(i);
				if (c >= 48 && c <= 57) {
					sb.deleteCharAt(i);
				} else {
					break;
				}
			}
			return sb.toString();
		}
		return name;
	}

}
