package com.jjpa.dma.manager.binding.action;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

@SuppressWarnings("unchecked")
public class SetDetailListGlobal implements ActionBinding{

	private final Logger logger = LoggerFactory.getLogger(SetDetailListGlobal.class);
	
	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) throws Exception {
		logger.info("Binding set detail list global in Action");
		DocumentContext ctx =  JsonPath.parse(document.getNativeJson().get("data"));
			
		/* 
		 * get globalOutputList from screen and api
		 * */
		List<Map<String, Object>> globalOutputList_screen = (List<Map<String, Object>>) storyBoard.get(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST);
		List<Map<String, Object>> globalOutputList_api = (List<Map<String, Object>>) eventMap.get(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST);
		logger.debug("globalOutputList_screen={}", globalOutputList_screen);
		logger.debug("globalOutputList_api={}", globalOutputList_api);
			
		String templateType = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.TEMPLATE_TYPE, DmaConstants.AppUI.Value.STATIC);
		/* 
		 * create bindingPath 
		 * */
		String bindingPath = generateBindingPath(document);
		logger.info("bindingPath={}", bindingPath);
		
		
		/* 
		 * binding global
		 * */
		bindingGlobal_screen(ctx, bindingPath, globalOutputList_screen);
		//bindingGlobal_api(ctx, bindingPath, globalOutputList_api);
		new SetGlobal().bindingGlobal_api(ctx, bindingPath, globalOutputList_api,templateType);
		
		return ctx;
	}


	private String generateBindingPath(DMAActionChainDocument document) {
		String bindingPath = "";	
		List<Map<String, Object>> bindingList = document.getBinding();
		for(Map<String, Object> binding : bindingList){
			String bindingName = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			if("options".equals(bindingName)){
				bindingPath = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));
				break;
			}
		}
		return bindingPath;
	}
	
	private void bindingGlobal_screen(DocumentContext ctx, String bindingPath, List<Map<String, Object>> globalOutputList) {
		
		if(globalOutputList != null){
			for(Map<String, Object> globalOutput : globalOutputList){
				
				/* check keep is true? */
				boolean keep = DataUtils.getBooleanValue(globalOutput.get(DmaConstants.StoryBoard.Key.KEEP));
				if(keep){
					logger.info("###found keep. then binding Global screen.");
					
					// get parameter
					String globalName = DataUtils.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
					String outputName = DataUtils.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.OUTPUT_NAME));
					
					
					// create newPath
					outputName = getNameBeforeLastBucket(outputName);					
//					String newPath = "{{"+ outputName + "}}";
					
					
					// generate key and value
					String globalNameKey = DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;
					String globalPathValue = setJasonBucket(JasonPathConstants.UiJson.Key.SELECTED_DETAILS_ITEM + "." + outputName);
					
					logger.info("globalNameKey={}", globalNameKey);
					logger.info("globalPathValue={}", globalPathValue);
					
					ctx.put(bindingPath, globalNameKey, globalPathValue);
				}
			}
		}
	}

// // 	private void bindingGlobal_api(DocumentContext ctx, String bindingPath, List<Map<String, Object>> globalOutputList) {
		
// // 		if(globalOutputList != null){
// // 			for(Map<String, Object> globalOutput : globalOutputList){
				
// // 				/* check keep is true? */
// // 				boolean keep = DataUtils.getBooleanValue(globalOutput.get(DmaConstants.StoryBoard.Key.KEEP));
// // 				if(keep){
// // 					logger.info("###found keep. then binding Global Api.");
					
// // 					// get parameter
// // 					String globalName = DataUtils.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
// // 					String path = DataUtils.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GlobalInputList.PATH));
					
					
// // 					// create newPath
// // 					path = getNameBeforeLastBucket(path);				
// // //					String newPath = "{{"+ path + "}}";
												
					
// // 					// generate key and value
// // 					String globalNameKey = DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;
// // 					String globalPathValue = setJasonBucket(JasonPathConstants.UiJson.Key.RESULT_RESULT_DATA + "." + path);
					

// // 					logger.info("globalNameKey={}", globalNameKey);
// // 					logger.info("globalPathValue={}", globalPathValue);
					
// // 					ctx.put(bindingPath, globalNameKey, globalPathValue);
// // 				}
// // 			}
// // 		}
// // 	}


	private String getNameBeforeLastBucket(String path){
		
		if(path!=null && path.contains("]")){
			logger.info("found ] then extract path and binding newPath");
			
			int lastIndex_closeBucket = path.lastIndexOf("]");
			
			path = path.substring(lastIndex_closeBucket+2);	
		}
		return path;
	}
	
	private String setJasonBucket(String data){	
		return "{{" + data + "}}";
	}

}
