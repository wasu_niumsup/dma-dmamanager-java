package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.jjpa.db.mongodb.document.BaseDocument;

@Document(collection = "entity_config")
public class EntityDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	@Indexed(unique = true)
	private String entityName;
	private String remark;
	private String storyName;
	private String role;
	private String theme;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdate;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastPublish;
	private String version;
	private String status;
	private String publishStatus;
	private Object appInfo;
	private Object appKey;

	public String getEntityName() {
		return entityName;
	}

	public Object getAppKey() {
		return appKey;
	}

	public void setAppKey(Object appKey) {
		this.appKey = appKey;
	}

	public Object getAppInfo() {
		return appInfo;
	}

	public void setAppInfo(Object appInfo) {
		this.appInfo = appInfo;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getStoryName() {
		return storyName;
	}
	public void setStoryName(String storyName) {
		this.storyName = storyName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Date getLastPublish() {
		return lastPublish;
	}
	public void setLastPublish(Date lastPublish) {
		this.lastPublish = lastPublish;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPublishStatus() {
		return publishStatus;
	}
	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}
	
}
