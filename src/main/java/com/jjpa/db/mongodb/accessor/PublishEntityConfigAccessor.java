package com.jjpa.db.mongodb.accessor;

import java.util.List;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.PublishEntityConfigDocument;

public interface PublishEntityConfigAccessor {
	
	/* 04/01/2562 add by Akanit : data is very large size. */
	public void loadRelateData(PublishEntityConfigDocument document, String storyName) throws Exception;
	public void moveRelateData(PublishEntityConfigDocument document, String user) throws Exception;
	public void deleteRelateData(String storyName, String user) throws Exception;
	public List<PublishEntityConfigDocument> findAllNotGetRelateData() throws Exception;
	
	public PublishEntityConfigDocument findPublishConfigByName(String entityName) throws Exception;
	public void deletePublishConfig(ServiceData input) throws Exception;
	public Object savePublishConfig(PublishEntityConfigDocument document, String user) throws Exception;
}
