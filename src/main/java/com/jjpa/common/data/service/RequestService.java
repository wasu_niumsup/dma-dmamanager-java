package com.jjpa.common.data.service;

import java.io.Serializable;

public class RequestService implements Serializable {
	
	public static class Key{
		public static String service_name="service_name";
		public static String service_class="service_class";
		public static String service_group="service_group";
		public static String except_validate_token="except_validate_token";
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String serviceName;
	String serviceClass;
	String serviceGroup;
	private boolean exceptValidateToken;
	private int apiRole;
	
	public int getApiRole() {
		return apiRole;
	}
	public void setApiRole(int apiRole) {
		this.apiRole = apiRole;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceClass() {
		return serviceClass;
	}
	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}
	public String getServiceGroup() {
		return serviceGroup;
	}
	public void setServiceGroup(String serviceGroup) {
		this.serviceGroup = serviceGroup;
	}
	public boolean isExceptValidateToken() {
		return exceptValidateToken;
	}
	public void setExceptValidateToken(boolean exceptValidateToken) {
		this.exceptValidateToken = exceptValidateToken;
	}

}
