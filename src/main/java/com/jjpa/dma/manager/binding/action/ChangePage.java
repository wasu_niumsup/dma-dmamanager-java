package com.jjpa.dma.manager.binding.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.APIsConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.api.ServiceGatewayApi;
import servicegateway.config.ServiceGatewayConfig;

@SuppressWarnings("unchecked")
public class ChangePage implements ActionBinding {

	private final Logger logger = LoggerFactory.getLogger(ChangePage.class);

	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard,
			Map<String, Object> eventMap) throws Exception {
		logger.info("Binding change page in Action");
		DocumentContext ctx = JsonPath.parse(document.getNativeJson().get("data"));

		String apiUrl = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/loadConfig";

		String actionName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.ACTION_NAME, null);
		String apiName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.API_NAME, null);
		String targetPage = null;
//		String extraPath = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.EXTRA_PATH, null);

		String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);

		List<Map<String, Object>> bindingList = document.getBinding();
		for (Map<String, Object> binding : bindingList) {
			logger.info("binding = " + binding);

			String name = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			String path = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));

			apiUrl = apiUrl + "?entity=" + JasonConfigurationUtil.getEntity() + "&source=" + source 
			+ "&_dma_storyboard=" + JasonConfigurationUtil.getStoryBoardName()
			+ "&action="+ actionName;

			if ("url".equals(name)) {
				logger.info("name=url");
				ctx.set(path, apiUrl);

			} else if ("data".equals(name)) {
				logger.info("name=data");

				
				ctx.put(path, DmaManagerConstant.DMA_ENTITY, JasonConfigurationUtil.getEntity());
				ctx.put(path, DmaManagerConstant.DMA_STORYBOARD, JasonConfigurationUtil.getStoryBoardName());
				ctx.put(path, DmaManagerConstant.DMA_ACTION, actionName);
				ctx.put(path, DmaManagerConstant.DMA_SOURCE, source);

				/**
				 * Tanawat W.
				 * [2018-04-21] add feature condition target screen
				 * 
				 * Ex.
				 * =================================================================================
				 * {
				 *     "actionList": [
				 *         {
				 *             "actionName": "submit_button",
				 *             "apiName": "API_LOGIN",
				 *             "targetType": "Screen",
				 *             "target": "menu",
				 *             "confirmDialog": {
				 *                 "needConfirm": false
				 *             },
				 *             "globalInputList": [],
				 *             "globalOutputList": [],
				 *             "targetConditions" : {
				 *                 "useConditions" : true,
				 *                 "conditionFromGlobalInput" : {},
				 *                 "conditionsList" : [
				 *                     {
				 *                         "type" : "case",
				 *                         "equalValue" : "11",
				 *                         "target" : "adminMenu"
				 *                     },
				 *                     {
				 *                         "type" : "case",
				 *                         "equalValue" : "12",
				 *                         "target" : "managerMenu"
				 *                     },
				 *                     {
				 *                         "type" : "default",
				 *                         "equalValue" : "",
				 *                         "target" : "menu"
				 *                     }
				 *                 ]
				 *             }
				 *         }
				 *     ]
				 * }
				 * ===============================================================================
				 * 
				 */
				targetPage = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TARGET, null);
				if (eventMap.containsKey(DmaConstants.StoryBoard.Key.TARGET_CONDITIONS)) {
					Map<String, Object> targetConditions = (Map<String, Object>) eventMap
							.get(DmaConstants.StoryBoard.Key.TARGET_CONDITIONS);
					if (DataUtils.getBooleanValue(targetConditions,
							DmaConstants.StoryBoard.Key.TargetConditions.USE_CONDITIONS, false) == true) {
						logger.info("using contidion target screen");
						Map<String, Object> conditoinFromGlobalInput = (Map<String, Object>) targetConditions
								.get(DmaConstants.StoryBoard.Key.TargetConditions.CONDITION_FROM_GLOBAL_INPUT);
						String conditionFromValue = JasonNetteUtils.generateResultValue(conditoinFromGlobalInput);
						logger.info("condition from value = " + conditionFromValue);
						conditionFromValue = JasonNetteUtils.cleanScope(conditionFromValue);

						List<Map<String, Object>> conditionsList = (List<Map<String, Object>>) targetConditions
								.get(DmaConstants.StoryBoard.Key.TargetConditions.CONDITIONS_LIST);
						logger.info("condtions list count = " + conditionsList.size());

						String totalExpression = "";
						for (Map<String, Object> caseCondition : conditionsList) {
							String target = DataUtils.getStringValue(caseCondition,
									DmaConstants.StoryBoard.Key.TargetConditions.ConditionsList.TARGET, null);
							String equalValue = DataUtils.getStringValue(caseCondition,
									DmaConstants.StoryBoard.Key.TargetConditions.ConditionsList.EQUAL_VALUE, null);
							String type = DataUtils.getStringValue(caseCondition,
									DmaConstants.StoryBoard.Key.TargetConditions.ConditionsList.TYPE, "case");

							if ("case".equalsIgnoreCase(type)) {
								// conditionFromValue == 1 ? 'pageName1' : conditionFromValue == 2  ? 'pageName2' : 
								String expression = conditionFromValue + " == "
										+ JasonNetteUtils.addScropString(equalValue) + " ? "
										+ JasonNetteUtils.addScropString(target) + " : ";

								totalExpression += expression;
							}
						}

						if (conditionsList.size() == 0) {
							totalExpression = JasonNetteUtils.addScropString(targetPage);
						} else {
							// add default
							totalExpression += JasonNetteUtils.addScropString(targetPage);
						}
						logger.info("Total Expression = " + totalExpression);
						targetPage = "{{ " + totalExpression + " }}";

					}
				} else {
					// do nothing using target
				}
				ctx.put(path, DmaManagerConstant.DMA_TARGET_PAGEGNAME, targetPage);

				/**
				 * Tanawat W.
				 * [2018-05-07] Change transaction message.
				 */
				ctx.put(path, DmaManagerConstant.MESSAGE, DmaManagerConstant.MESSAGE_VALUE.OPEN + targetPage + DmaManagerConstant.MESSAGE_VALUE.PAGE);
				ctx.put(path, DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.LOAD_SCREEN);


				if (!DataUtils.hasKey(ctx, path + "." + "_dma_split_data")) {
					ctx.put(path, "_dma_split_data", new LinkedHashMap<>());
					logger.info("new _dma_split_data");
				}

				/**
				 * Tanawat W.
				 * [2018-07-10] improve performance send data
				 */
				Map<String,Object> networkOptions = ctx.read(path);
				new NetworkRequest().setData(storyBoard, networkOptions);


				/**
				 * Add config
				 * 
				 */
				JasonConfigurationUtil.generateGetLocalValue(ctx, path + "." + "_dma_split_data");
				logger.info("Added base config");

				/* binding extra */
				if (apiName != null && !DmaConstants.StoryBoard.Value.DO_NOTHING.equals(apiName)) {
					logger.info("apiName!=Do Nothing then call serviceGateway for get config and binding extra");

					/* call serviceGateway */
					Map<String, Object> sgResponse = new ServiceGatewayApi().findServiceByName(apiName);

					ServiceGatewayConfig sgConf = new ServiceGatewayConfig(sgResponse);

					/* find row is fieldType=datamap */
					Map<String, Object> rowDataMap = sgConf.getRowDataMap_output();
					if (rowDataMap != null) {

						bindingDmaExtraByDataMap(ctx, path, rowDataMap);
						continue;
					}

					/* find row is fieldType=datalist */
					Map<String, Object> rowDataList = sgConf.getRowDataList_output();
					if (rowDataList != null) {

						/* case : simple data : path=value[0].xxx */

						bindingDmaExtraByDataList(ctx, path);
						continue;
					}

				}

			} else {
				logger.info("name=other, then do nothing.");
				// do nothing.
			}

			logger.info("ctx=" + ctx.jsonString());
		}
		return ctx;
	}

	private void bindingDmaExtraByDataMap(DocumentContext ctx, String path, Map<String, Object> rowDataMap) {

		// String xpath = DataUtils.getStringValue(rowDataMap, APIsConstants.Key.XPATH, "");
		// String jsonpath = xpath.replace("xpath:/", "").replace("[?]", "[0]").replace("[*]", "[0]").replace("/", ".");
		String filedName = DataUtils.getStringValue(rowDataMap, APIsConstants.Key.FIELD_NAME, "");
		/* 07/03/2561 add by Akanit : new code 
		 * method : generateResultData();
		 * return data : {{$get.result.resultData." + value +"}}
		 * */
		String value = JasonNetteUtils.generateResultData(filedName);

		logger.info("binding extra");
		logger.info("path={}", path);
		logger.info("key={}", DmaManagerConstant.DMA_EXTRA);
		logger.info("value={}", value);

		ctx.put(path, DmaManagerConstant.DMA_EXTRA, value);
	}

	private void bindingDmaExtraByDataList(DocumentContext ctx, String path) {

		/* 07/03/2561 comment by Akanit : old code */
		//		String filedName = DataUtils.getStringValue(rowDataMap, APIsConstants.Key.FIELD_NAME, "");

		/* create value */
		/* 07/03/2561 comment by Akanit : old code */
		//		String value = "{{JSON.stringify("+ JasonPathConstants.UiJson.Key.RESULT_RESULT_DATA +"."+ filedName +")}}";

		/* 07/03/2561 add by Akanit : new code 
		 * method : generateResultData();
		 * return data : {{$get.result.resultData." + value +"}}
		 * */
		String value = JasonNetteUtils.generateResultData("value");

		logger.info("binding extra");
		logger.info("path={}", path);
		logger.info("key={}", DmaManagerConstant.DMA_EXTRA);
		logger.info("value={}", value);

		ctx.put(path, DmaManagerConstant.DMA_EXTRA, value);
	}

}
