package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToScreenProcessor;

import servicegateway.utils.ManagerMap;

public class TemplateAccessorImpl implements TemplateAccessor {

	private static final Logger logger = LoggerFactory.getLogger(TemplateAccessorImpl.class);
	
	protected MongoDbAccessor mongoDbAccessor;

	protected TemplateAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Object saveTemplate(DMATemplate template, String user) throws Exception {
		
		try{
			String templateName = template.getTemplateName();
			
//			logger.info("find template '{}'.", templateName);
			DMATemplate oldTemplate = findTemplateByName(templateName);
			
			if(oldTemplate != null){
//				logger.info("found template '{}' then update template.", oldTemplate.getTemplateName());
				
				if(template.getReference()!=null){
					oldTemplate.setReference(template.getReference());
				}
				
//				oldTemplate.setTemplateName(template.getTemplateName());
				oldTemplate.setBinding(template.getBinding());
				oldTemplate.setActionList(template.getActionList());
				oldTemplate.setInputList(template.getInputList());
				oldTemplate.setTemplateType(template.getTemplateType());
				oldTemplate.setDynamicApi(template.getDynamicApi());
				oldTemplate.setDynamicDataBinding(template.getDynamicDataBinding());
				oldTemplate.setInfoUrl(template.getInfoUrl());
				oldTemplate.setNativeJson(template.getNativeJson());
				oldTemplate.setVersionEngine(template.getVersionEngine());
				oldTemplate.setVersionEngineAndroidMobile(template.getVersionEngineAndroidMobile());
				oldTemplate.setVersionEngineIOSMobile(template.getVersionEngineIOSMobile());
				oldTemplate.setVersionTemplate(template.getVersionTemplate());
				oldTemplate.setLastUpdate(template.getLastUpdate());
				template = oldTemplate;
			}else {
				logger.info("not found template '{}' then create template.", templateName);
			}
			
			/* 14/03/2562 add by Akanit : */
//			updateCurrentVersionTemplateRequriedReferenceToScreen(template);

			return mongoDbAccessor.save(template, user);
		}catch(Exception e){
			throw e;
		}
		
	}

	public Object saveTemplateAndUpdateCurrentVersion(DMATemplate template, String user) throws Exception {
		
		try{
			String templateName = template.getTemplateName();
			
//			logger.info("find template '{}'.", templateName);
			DMATemplate oldTemplate = findTemplateByName(templateName);
			
			if(oldTemplate != null){
//				logger.info("found template '{}' then update template.", oldTemplate.getTemplateName());
				
				if(template.getReference()!=null){
					oldTemplate.setReference(template.getReference());
				}
				
//				oldTemplate.setTemplateName(template.getTemplateName());
				oldTemplate.setBinding(template.getBinding());
				oldTemplate.setActionList(template.getActionList());
				oldTemplate.setInputList(template.getInputList());
				oldTemplate.setTemplateType(template.getTemplateType());
				oldTemplate.setDynamicApi(template.getDynamicApi());
				oldTemplate.setDynamicDataBinding(template.getDynamicDataBinding());
				oldTemplate.setInfoUrl(template.getInfoUrl());
				oldTemplate.setNativeJson(template.getNativeJson());
				oldTemplate.setVersionEngine(template.getVersionEngine());
				oldTemplate.setVersionEngineAndroidMobile(template.getVersionEngineAndroidMobile());
				oldTemplate.setVersionEngineIOSMobile(template.getVersionEngineIOSMobile());
				oldTemplate.setVersionTemplate(template.getVersionTemplate());
				oldTemplate.setLastUpdate(template.getLastUpdate());
				template = oldTemplate;
			}else {
				logger.info("not found template '{}' then create template.", templateName);
			}
			
			/* 14/03/2562 add by Akanit : */
			updateCurrentVersionTemplateRequriedReferenceToScreen(template);
			
			return mongoDbAccessor.save(template, user);
		}catch(Exception e){
			throw e;
		}
		
	}	
	
	public DMATemplate findTemplateByName(String templateName) throws Exception {
		
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(DmaConstants.Template.Key.TEMPLATE_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(templateName), Pattern.CASE_INSENSITIVE)));
		
		try{
			return mongoDbAccessor.findOneByCriteria(findByName, DMATemplate.class);
		}catch(Exception e){
			throw e;
		}
	}

	public List<DMATemplate> listTemplates() throws Exception {
		
		try{
			return mongoDbAccessor.findAll(DMATemplate.class);
		}catch(Exception e){
			throw e;
		}
	}

	public List<DMATemplate> listSortTemplates() throws Exception {
		
		try{
			Query query = new Query();
			query.with(new Sort(Sort.Direction.ASC, DmaConstants.Template.Key.TEMPLATE_NAME));
			return mongoDbAccessor.findByCriteria(query, DMATemplate.class);
			//return mongoDbAccessor.findAll(DMATemplate.class);
		}catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,Object> listTemplateName() throws Exception {
		List<DMATemplate> templateObjList = listSortTemplates();
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String,Object>> templateList = new ArrayList<Map<String,Object>>();
		Map<String,Object> template;
		for(DMATemplate document : templateObjList){
			template = new HashMap<String,Object>();
			template.put("name", document.getTemplateName());
			template.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
			/* add by warrawan 12/4/2018 - add version to listTemplate */
			template.put("version", document.getVersionTemplate());
			/*end */

			templateList.add(template);
		}
		result.put(DmaConstants.Template.Key.TEMPLATE_LIST, templateList);
		return result;
	}

//	public void deleteTemplate(DMATemplate template, String user) throws Exception {
//		
//		try{
//			mongoDbAccessor.delete(template, user);
//		}catch(Exception e){
//			throw e;
//		}
//	}
	
	@SuppressWarnings("unchecked")
	public void deleteTemplate(ServiceData input) throws Exception {
		String user = (String)input.getValue("user");
		List<Map<String, Object>> templateList = (List<Map<String, Object>>)input.getValue(DmaConstants.Template.Key.TEMPLATE_LIST);
		for(Map<String, Object> templateObj : templateList){
			String templateName = (String)templateObj.get(DmaConstants.Template.Key.TEMPLATE_NAME);
			mongoDbAccessor.delete(findTemplateByName(templateName), user);
		}
		
	}
	
	
	
	/* 
	 * accessor of appUi 
	 * */
	public AppUiDocument findAppUiByName(String uiName) throws Exception{
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.AppUI.Path.UI_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(uiName), Pattern.CASE_INSENSITIVE)));
		AppUiDocument uiDoc = mongoDbAccessor.findOneByCriteria(findByName, AppUiDocument.class);
		return uiDoc;
	}
	
	public void updateCurrentVersionTemplateRequriedReferenceToScreen(DMATemplate template) throws Exception {
		logger.info("try update Reference 'currentVersion' in 'dmaTemplateRequired' in 'Screen(AppUI)'.");
		
		List<Map<String, Object>> inUsedByDmaScreenList = new GetReferenceProcessor().getInUsedByDmaScreens(template.getReference());
		if(inUsedByDmaScreenList==null){
			logger.info("Template '"+ template.getTemplateName() +"' not found refference.inUsed.inUsedByDmaScreen, then skip this step.");
			return;
		}
		
		updateCurrentVersionTemplateRequriedReferenceToScreen(inUsedByDmaScreenList, template);
		
	}
	
	public void updateCurrentVersionTemplateRequriedReferenceToScreen(List<Map<String, Object>> screenList, DMATemplate template) throws Exception {
		
		String screenName;
		AppUiDocument appUiDoc;
		for(Map<String, Object> inUsedByDmaScreen : screenList){
			
			screenName = ManagerMap.getString(inUsedByDmaScreen, ReferenceConstants.DMA.NAME);
			
			appUiDoc = findAppUiByName(screenName);
			if(appUiDoc==null){
//				logger.info("screen '"+ screenName +"' not found, then remove inUsedByDmaScreen reference and continue next loop.");
				logger.info("screen '"+ screenName +"' not found, then continue next loop.");
				continue;
			}
			
			new BindingRequiredReferenceToScreenProcessor().updateCurrentVersionToDmaTemplateRequiredReference(appUiDoc, template);
			mongoDbAccessor.save(appUiDoc, "autoLoadVersionTemplate");
			
			logger.info("update Reference 'currentVersion' in 'dmaTemplateRequired' in Screen '"+ screenName +"' success.");
		}
	}
	
}
