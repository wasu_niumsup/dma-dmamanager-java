package com.jjpa.common;

public class AppConstants {

	public static final String USER 			= "user";
	
	public class Pagging{
		
		public class Key{
			public static final String SORT 	= "sort";
			public static final String DIR 		= "dir";
			public static final String LIMIT 	= "limit";
			public static final String START 	= "start";
		}
		
		public class Value{
			public static final String ASC 		= "ASC";
			public static final String DESC 	= "DESC";
		}
	}
	
	public class Label{
		public static final String ID 					= "ID";
	}
	
//	public class Request{
//		
//		public class Key{
//			
//			public static final String USER 				= "user";
//			
//			/* api AppUiAccessor */
//			public static final String UI_NAME 				= "uiName";
//			public static final String REMARK 				= "remark";
//			public static final String LAST_UPDATE 			= "lastUpdate";
//			public static final String LAST_PUBLISH 		= "lastPublish";
//			public static final String STATUS 				= "status";
//
//			/* api EntityAccessor */
//			public static final String ENTITY_LIST 			= "entityList";
//			public static final String ENTITY_NAME 			= "entityName";
//			public static final String STORY_NAME 			= "storyName";
//			public static final String ROLE 				= "role";
//			public static final String THEME 				= "theme";
//			public static final String VERSION 				= "version";
//			public static final String PUBLISH_STATUS 		= "publishStatus";
//		}
//		
//		public class Value{
//			
//		}
//	}
//
//	public class Response{
//			
//		public class Label{
//			
//			public static final String ID 					= "ID";
//			
//		}
//		
//		public class Key{
//			
//			/* global */
//			public static final String ID 					= "id";
//			public static final String REMARK 				= "remark";
//			public static final String LAST_UPDATE 			= "lastUpdate";
//			public static final String LAST_PUBLISH 		= "lastPublish";
//			public static final String STATUS 				= "status";
//			
//			
//			/* api AppUiAccessor */
//			public static final String UI_NAME 				= "uiName";
//			public static final String STORY_BOARD 			= "storyboard";
//			public static final String ACTION_LIST 			= "actionList";
//			
//			public static final String GLOBAL_INPUT_LIST 	= "globalInputList";
//			public static final String INPUT_NAME 			= "inputName";
//			public static final String FIELD_POSITION 		= "fieldPosition";
//			
//			public static final String GLOBAL_OUTPUT_LIST 	= "globalOutputList";
//			public static final String OUTPUT_NAME 			= "outputName";
//			public static final String DATA_STRUCTURE 		= "dataStructure";
//			
//			
//			/* api EntityAccessor */
//			public static final String ENTITY_NAME 			= "entityName";
//			public static final String STORY_NAME 			= "storyName";
//			public static final String ROLE 				= "role";
//			public static final String THEME 				= "theme";
//			public static final String VERSION 				= "version";
//			public static final String PUBLISH_STATUS 		= "publishStatus";
//
//			
//			/* public */
//			public static final String NAME 				= "name";
//			public static final String TYPE 				= "type";
//			public static final String PATH 				= "path";
//			public static final String LOCAL_PATH 			= "localPath";
//			public static final String FIELD_TYPE 			= "fieldType";
//			public static final String PROMOTE 				= "promote";
//			public static final String LABEL_NAME 			= "labelName";
//			public static final String DEFAULT_VALUE 		= "defaultValue";
//			
//		}
//		
//		public class Value{
//			
//			public static final String API 					= "api";
//		}
//	}
	
	public class Log{
		public class Key{
			public static final String TOTAL 			= "total";
			public static final String LOG_LIST 		= "logList";
			
			public static final String LOG_ID 			= "logID";
			public static final String USERNAME 		= "username";
			public static final String SERVICE 			= "service";
			public static final String REQUEST_TIME 	= "requestTime";
			public static final String TIME 			= "time";
			public static final String RESULT 			= "result";
			public static final String SUCCESS 			= "success";
			public static final String MESSAGE 			= "message";
			public static final String INPUT 			= "input";
			public static final String OUTPUT 			= "output";
			
			public static final String START_TIME 		= "startDate";
			public static final String END_TIME 		= "endDate";
		}
		public class Value{
			
		}
	}
}
