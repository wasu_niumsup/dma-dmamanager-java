package com.jjpa.dma.manager.processor.storyboard;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ScreenInputTypeAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListScreenInputTypeProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListScreenInputTypeProcessor.class);
		
	protected ScreenInputTypeAccessor screenInputTypeAccessor;

	public ListScreenInputTypeProcessor() {
		this.screenInputTypeAccessor = SpringApplicationContext.getBean("ScreenInputTypeAccessor");
    }
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListScreenInputTypeProcessor");
		logger.debug("Process ===> ListScreenInputTypeProcessor : {}", input.toJsonString());
		
		return screenInputTypeAccessor.listScreenInputType(input);
		
	}

}