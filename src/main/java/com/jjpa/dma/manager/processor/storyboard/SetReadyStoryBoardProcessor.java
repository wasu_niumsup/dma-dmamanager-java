package com.jjpa.dma.manager.processor.storyboard;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class SetReadyStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SetReadyStoryBoardProcessor.class);
		
	protected StoryBoardAccessor storyBoardAccessor;
	
	public SetReadyStoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SetReadyStoryBoardProcessor");
		logger.debug("Process ===> SetReadyStoryBoardProcessor : {}", input.toJsonString());
		
		return setReady(input);
		
	}
	
	private Map<String,Object> setReady(ServiceData input) throws Exception {
		StoryBoardDocument document = storyBoardAccessor.findStoryByName((String)input.getValue("storyName"));
		document.setStatus(DmaManagerConstant.STATUS_READY);
		String user = (String)input.getValue("user");
		
		storyBoardAccessor.saveStoryBoard(document, user);
		
		Map<String,Object> appUi = new HashMap<String,Object>();
		appUi.put("storyName", document.getStoryName());
		appUi.put("remark", document.getRemark());
		appUi.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		appUi.put("lastPublish", document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
		appUi.put("status", document.getStatus());
		
		return appUi;
	}

}
