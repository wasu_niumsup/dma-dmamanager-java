package com.jjpa.dma.manager.processor.binding;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;

@SuppressWarnings({"unused", "unchecked"})
public class BindingLocalProcessor{

	private static final Logger logger = LoggerFactory.getLogger(BindingLocalProcessor.class);
	
	public Map<String, Object> process(Map<String, Object> dataBinding, Map<String, Object> storyBoard, AppUiDocument appUiDoc) throws Exception {
			
		
		/* validate data */
		Object globalInputObj = storyBoard.get(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST);
		if(globalInputObj==null){
			return dataBinding;
		}	
		
		DocumentContext ctx = JsonPath.parse(dataBinding);	


		/**
		 * 
		 * Tanawat W.
		 * [2019-02-14]
		 * DMA 5.0
		 * Skip all binding
		 */
		if(true){
			return dataBinding;
		}
		
		
		/* binding global to local */
		boolean useGlobal;
		String path, value, localPath, inputName ,globalValue;
		List<Map<String, Object>> globalInputList = (List<Map<String, Object>>) globalInputObj;
		String templateType = appUiDoc.getTemplateType();
		
		for(Map<String, Object> globalInput : globalInputList){
				
			/* check useGlobal is true? */
			useGlobal = DataUtils.getBooleanValue(globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.USE_GLOBAL));
			logger.info("useGlobal={}" ,useGlobal);
			if(useGlobal){
				logger.info("###found useGlobal. then binding Local.");
				
				/* get parameter */
				path = DataUtils.getStringValue(globalInput, DmaConstants.StoryBoard.Key.GlobalInputList.PATH, null);	
				value = DataUtils.getStringValue(globalInput, DmaConstants.StoryBoard.Key.GlobalInputList.VALUE, null);

				localPath = DataUtils.getStringValue(globalInput, DmaConstants.StoryBoard.Key.GlobalInputList.LOCAL_PATH, null);


				inputName = DataUtils.getStringValue(globalInput, DmaConstants.StoryBoard.Key.INPUT_NAME, null);
				
				globalValue = JasonNetteUtils.generateGlobalValue(value);
				

				/**
				* Tanawat W.
				* [2018-03-20] Add feature initial screen
				* 
				*/
				if (DmaConstants.AppUI.Value.DYNAMIC_INPUT.equals(templateType)) {
					localPath = JasonConfigurationUtil.SET_LOCALVARIABLE_PATH;
				}


				logger.info("path={}", path);
				logger.info("value={}", value);
				logger.info("localPath={}", localPath);
				logger.info("inputName={}", inputName);
				logger.info("globalValue={}", globalValue);
				

				if(localPath!=null && inputName!=null){
					logger.info("#binding global to api. put(localPath, inputName, globalValue)");
					ctx.put(localPath, inputName, globalValue);

				}else {
					logger.info("#binding global normal case. set(path, globalValue)");
					
					ctx.set(path, globalValue);
				}
				
			}
		}
		
		return dataBinding;
	}

	private boolean hasPath(DocumentContext ctx ,String path){
		try{
			Object test =  ctx.read(path);
			return true;
		}catch (Exception ex){
			return false;
		}
	}


}
