package com.jjpa.dma.manager.processor.domain;

import java.net.URL;

import com.jjpa.common.util.ConfigurationUtils;

import blueprint.util.StringUtil;

public class DMADomainProcessor {

//	private static final Logger logger = LoggerFactory.getLogger(BindingDomainProcessor.class);

	public String bindingDMADomain(String url) throws Exception {
		if(StringUtil.isBlank(url)){
			return url;
		}
		URL u = new URL(url);
		return "{{$get._dma_param_internal_domain}}" + u.getPath() + "?" + u.getQuery();
	}

	public String bindingInternalDomain(String url) throws Exception {
		if(StringUtil.isBlank(url)){
			return url;
		}
		return url.replace("{{$get._dma_param_internal_domain}}", ConfigurationUtils.getPublishUrl().toLowerCase());
	}
	
	public boolean isUrlWithDomain(String url) throws Exception {
		if(StringUtil.isBlank(url)){
			return false;
		}
		if(url.indexOf("{{$get._dma_param_internal_domain}}")!=-1){
			return false;
		}
		return true;
	}
	
	public boolean isUrlAutoGenImage(String url) throws Exception {
		if(StringUtil.isBlank(url)){
			return false;
		}
		if(url.indexOf("https://via.placeholder.com")!=-1){ // this url will auto generate image by param
			return true;
		}
		return false;
	}

}
