



[TOC]



# dmaManager

# Overview

![TerminalUser-SendToBot](.\TerminalUser-SendToBot.png)

**Terminal UI**

![Menu](.\Menu.png)



**Message**

![Message](.\Message.png)







# APIs



## ExtraTarget ???

**Detail** ???

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า extratarget



------

### SaveExtraTarget  

**URL** : /dmaManager/apis/extratarget/saveExtraTarget

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request** 

```javascript
{
	"requestData": {
		"user": "admin",
		"uiName": "dial",
         "configParameter" : [
            {
                "option": "tel",
                "desc": "เบอร์โทรศัพท์",
                "defaultValue": "0123456789",
                "value": ""
            }
         ]
	}
}
```



**Response**

```javascript
{
    "success": true,
    "genaratedAt": 1528709941286,
    "status": "success",
    "responseData": {
        "id": "5b1e4335c2dcf3b6e1bd7a4f",
        "createDate": 1528709941289,
        "lastUpdateDate": 1528709941289,
        "createBy": "admin",
        "lastUpdateBy": "admin",
        "dmaManagerVersion": null,
        "uiName": "dial",
        "configParameter": [
            {
                "option": "tel",
                "desc": "เบอร์โทรศัพท์",
                "defaultValue": "0123456789",
                "value": ""
            }
        ],
        "lastUpdate": 1528709941289
    }
}
```



----------------------------

### DeleteExtraTarget 

**URL** : /dmaManager/apis/extratarget/deleteExtraTarget 

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"user": "admin",
		"uiName": "browser"
    }
}
```



**Response**

```javascript
{
    "success": true,
    "genaratedAt": 1528709911378,
    "status": "success",
    "responseData": {
        "appUiList": [
            {
                "configParameter": null,
                "uiName": "self"
            }
        ]
    }
}
```



------------------------------------





## Preview ???

**Detail** ???

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า preview



------

### PreviewScreenUi ???

**URL** : /dmaManager/apis/preview/previewscreenui

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"???": null
	}
}
```



**Response**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      | user login  |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673930,
	"status": "success",
	"responseData": {
		"???": "???"
	}
}
```



------

### DirectPreview ???

**URL** : /dmaManager/apis/preview/DirectPreview

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"???": "admin"
	}
}
```



**Response**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | ???    |             |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673978,
	"status": "success",
	"responseData": {}
}
```



------

### MobileDirectPreview ???

**URL** : /dmaManager/apis/preview/MobileDirectPreview

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"???": ""
	}
}
```



**Response**

| Path | Type   | Required | Sample  | Description            |
| ---- | ------ | -------- | ------- | ---------------------- |
| ???  | String | true     | x.x.x.x | version ของ dmaManager |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673957,
	"status": "success",
	"responseData": {
		"???": ""
	}
}
```



------

### Property ???

**URL** : /dmaManager/apis/preview/property

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"???": "admin"
	}
}
```



**Response**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| ???  | String | true     | x      |             |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560242987046,
	"status": "success",
	"responseData": {
		"???": "admin"
	}
}
```





## Configuration (Not verifyToken)

**Detail** ???

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า configuration



------

### ListConfiguration

**URL** : /dmaManager/apis/configuration/listConfiguration

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Sample Request**

```javascript
{
	"requestData": {
	}
}
```



**Response**

| Path           | Type          | Required | Sample | Description  |
| -------------- | ------------- | -------- | ------ | ------------ |
| configNameList | Array[Object] | true     | [{}]   | config ทั้งหมด |

**Sample Response**

```javascript
{
    "success": true,
    "genaratedAt": 1560248334736,
    "status": "success",
    "responseData": {
        "configNameList": [
            {
                "configName": "versionInfo"
            },
            {
                "configName": "forceLogout"
            },
            {
                "configName": "previewTemplate"
            }
        ]
    }
}
```



------

### FindByNameConfiguration

**URL** : /dmaManager/apis/configuration/findByNameConfiguration

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path       | Type   | Required | Sample | Description |
| ---------- | ------ | -------- | ------ | ----------- |
| configName | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"configName": "versionInfo"
	}
}
```



**Sample Response**

```javascript
{
    "success": true,
    "genaratedAt": 1560248250196,
    "status": "success",
    "responseData": {
        "id": "5c77a24655cd73fd20e5d74c",
        "createDate": null,
        "lastUpdateDate": null,
        "createBy": null,
        "lastUpdateBy": "systemadmin",
        "dmaManagerVersion": null,
        "nativeJson": {
            "data": "{\"$jason\..."}"
        },
        "binding": null,
        "lastUpdate": null,
        "configName": "versionInfo",
        "className": null,
        "data": {},
        "type": null
    }
}
```



------

### GetConfiguration

**URL** : /dmaManager/apis/configuration/getConfiguration

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path       | Type   | Required | Sample | Description |
| ---------- | ------ | -------- | ------ | ----------- |
| configName | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"configName": "versionInfo"
	}
}
```



**Response**

| Path       | Type   | Required | Sample  | Description            |
| ---------- | ------ | -------- | ------- | ---------------------- |
| configName | String | true     | x.x.x.x | version ของ dmaManager |
| nativeJson | Object | true     | {}      |                        |

**Sample Response**

```javascript
{
    "success": true,
    "genaratedAt": 1560248379414,
    "status": "success",
    "responseData": {
        "configName": "versionInfo",
        "nativeJson": {
            "$jason": {...}
        }
    }
}
```



------

### SaveConfiguration

**URL** : /dmaManager/apis/configuration/saveConfiguration

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path         | Type   | Required | Sample | Description |
| ------------ | ------ | -------- | ------ | ----------- |
| configName   | String | true     | x      |             |
| templateCode | String | true     | x      | ???         |

**Sample Request**

```javascript
{
	"requestData": {
		"configName": "versionInfo",
		"templateCode": "...",
		"user": "admin"
	}
}
```



**Response**

| Path     | Type   | Required | Sample | Description |
| -------- | ------ | -------- | ------ | ----------- |
| username | String | true     | x      | username    |

**Sample Response**

```javascript
{
    "success": true,
    "genaratedAt": 1560248250196,
    "status": "success",
    "responseData": {
        "id": "5c77a24655cd73fd20e5d74c",
        "createDate": null,
        "lastUpdateDate": null,
        "createBy": null,
        "lastUpdateBy": "systemadmin",
        "dmaManagerVersion": null,
        "nativeJson": {
            "data": "{\"$jason\..."}"
        },
        "binding": null,
        "lastUpdate": null,
        "configName": "versionInfo",
        "className": null,
        "data": {},
        "type": null
    }
}
```





## DMA5 (Not verifyToken)

**Detail** เป็น API สำหรับให้ระบบ DMA5 (ภายใน) เรียกใช้  โดยจะ **ไม่** มีการ verify token หรือ header

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า dma5



------

### SaveTemplate

**URL** : /dmaManager/apis/dma5/saveTemplate

**Call API** : /dmaManager/apis/template/saveTemplate



------

### ListAppUi

**URL** : /dmaManager/apis/dma5/listAppUi

**Call API** : /dmaManager/apis/storyboard/listAppUi



------

### UpgradeAppUi

**URL** : /dmaManager/apis/dma5/upgradeAppUi

**Call API** : /dmaManager/apis/ui/upgradeAppUi



------

### FindAppUiByName

**URL** : /dmaManager/apis/dma5/findAppUiByName

**Call API** : /dmaManager/apis/ui/findAppUiByName



------

### FindTemplateByName

**URL** : /dmaManager/apis/dma5/findTemplateByName

**Call API** : /dmaManager/apis/ui/findTemplateByName



------

### SaveAppUi

**URL** : /dmaManager/apis/dma5/saveAppUi

**Call API** : /dmaManager/apis/ui/saveAppUi



------

### Upgrade

**URL** : /dmaManager/apis/dma5/upgrade

**Call API** : /dmaManager/apis/storyboard/upgrade



------





## Authen

**Detail** เป็น API ที่ใช้สำหรับ authentication และ get ข้อมูลที่เกี่ยวข้องกับการ authen ของระบบ dmaManager.war

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า authen



------

### Login

**URL** : /dmaManager/apis/authen/login

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"username": "admin",
		"password": "password",
		"deviceKey": null,
		"tokenKey": null
	}
}
```



**Response**

```javascript
{
    "success": true,
    "responseHeader": {
        "responseCode": "0000I",
        "responseDescription": "Success"
    },
    "responseData": {
        "uidKey": "admin",
        "tokenKey": "adminde2d0c50-951b-4aff-b62c-84ed08d66f65"
    }
}
```



------

### Logout

**URL** : /dmaManager/apis/authen/logout

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"tokenKey": "admindc194d8e-d867-4a1b-af8a-f96416643cbd"
	}
}
```



**Response**

```javascript
{
    "success": true,
    "responseHeader": {
        "responseCode": "0000I",
        "responseDescription": "Success"
    },
    "responseData": {}
}
```



------

### GetLoginUser

**URL** : /dmaManager/apis/authen/getLoginUser

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": null
	}
}
```



**Response**

| Path     | Type   | Required | Sample | Description |
| -------- | ------ | -------- | ------ | ----------- |
| username | String | true     | x      | user login  |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673930,
	"status": "success",
	"responseData": {
		"username": "admin"
	}
}
```



------

### GetMenu

**URL** : /dmaManager/apis/authen/getMenu

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "admin"
	}
}
```



**Response**

| Path                 | Type          | Required | Sample | Description                                                  |
| -------------------- | ------------- | -------- | ------ | ------------------------------------------------------------ |
| menuRole             | String        | true     | admin  | role ของ menu<br />**<u>ประกอบด้วย</u>**<br />**admin** : จะโชว์ menu ที่เกี่ยวกับการ config storyboard เพื่อสร้าง mobile application<br />**system** : จะโชว์ menu ที่เกี่ยวกับการ patch หรือ restore ระบบ เช่น PatchTemplate, PatchActionChain, PatchACT, PatchConfig |
| menuLists            | Array[Object] | true     | [{}]   | menu ทั้งหมดของระบบ                                            |
| menuLists[*].menuSeq | Integer       | true     | 1      | menu จะเรียงจากตัวเลข น้อย ไป มาก โดยเรียงจาก บน ลง ล่าง          |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673978,
	"status": "success",
	"responseData": {
		"menuRole": "admin",
		"menuLists": [{
				"menuSeq": 1,
				"menuId": "idScreen",
				"menuName": "Screen",
				"menuLink": "../Screen",
				"menuImage": "../../resources/images/menu/menu_appUi.png",
				"menuActive": true
			}, {
				"menuSeq": 2,
				"menuId": "idStoryboard",
				"menuName": "Storyboard",
				"menuLink": "../Storyboard",
				"menuImage": "../../resources/images/menu/menu_storyboardConfigure.png",
				"menuActive": true
			}
		]
	}
}
```



------

### GetVersion 

**URL** : /dmaManager/apis/authen/getVersion 

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "admin"
	}
}
```



**Response**

| Path    | Type   | Required | Sample  | Description            |
| ------- | ------ | -------- | ------- | ---------------------- |
| version | String | true     | x.x.x.x | version ของ dmaManager |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673957,
	"status": "success",
	"responseData": {
		"version": "5.7.10.0"
	}
}
```



------

### ChangePassword

**URL** : /dmaManager/apis/authen/changePassword

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path        | Type   | Required | Sample | Description   |
| ----------- | ------ | -------- | ------ | ------------- |
| username    | String | true     | x      | username      |
| password    | String | true     | x      | password      |
| newPassword | String | true     | x      | new  password |

**Sample Request**

```javascript
{
	"requestData": {
		"username": "admin",
		"password": "password1",
		"newPassword": "password2",
		"user": "admin"
	}
}
```



**Response**

| Path     | Type   | Required | Sample | Description |
| -------- | ------ | -------- | ------ | ----------- |
| username | String | true     | x      | username    |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560242987046,
	"status": "success",
	"responseData": {
		"username": "admin"
	}
}
```





## AppUI

**Detail** เป็น API ที่ใช้สำหรับระบบ dmaManager.war โดยเป็น API ของ Menu Screen

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า ui



------

### ApiSpecTemplate

**URL** : /dmaManager/apis/ui/listTemplate

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "admin"
	}
}
```



**Response**

| Path         | Type          | Required | Sample | Description                                   |
| ------------ | ------------- | -------- | ------ | --------------------------------------------- |
| templateList | Array[Object] | true     | [{}]   | template ทั้งหมด ของระบบ<br />field นี้ เป็น [] ได้ |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560234278611,
	"status": "success",
	"responseData": {
	}
}
```



------

### ListAppUi  

**URL** : /dmaManager/apis/ui/listAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path                                        | Type    | Required | Sample      | Description                                                  |
| ------------------------------------------- | ------- | -------- | ----------- | ------------------------------------------------------------ |
| uiName                                      | String  | false    | x           | ค้นหาด้วย **uiName**<br />ถ้าไม่มี field นี้ = ไม่มีเงื่อนไขในการค้นหา   |
| remark                                      | String  | false    | x           | ค้นหาด้วย **remark**<br />ถ้าไม่มี field นี้ = ไม่มีเงื่อนไขในการค้นหา   |
| status                                      | String  | false    | x           | ค้นหาด้วย **status**<br />ถ้าไม่มี field นี้ = ไม่มีเงื่อนไขในการค้นหา   |
| storyboard                                  | String  | false    | x           | ค้นหาด้วย **storyboard**<br />ถ้าไม่มี field นี้ = ไม่มีเงื่อนไขในการค้นหา |
| isAppUiNot<br />InUsedBy<br />DmaStroyBoard | Boolean | false    | true        | ถ้าเป็น true = ค้นหา **uiName** ที่<br />ยังไม่เคยถูกใช้งาน และ ถ้า field<br />**storyboard** มีค่า จะค้นหา **uiName**<br />ที่ถูกเรียกใช้โดย **storyboard** นั้นๆ<br />มาเพิ่มด้วย<br />**<u>Example</u>**<br />{<br />     "storyboard": "qa",<br />     "isAppUiNot...": true<br />}<br /><u>จากตัวอย่าง</u><br />ระบบจะค้นหา **uiName** ที่ยังไม่เคย<br />ถูกใช้งาน และ **uiName** ที่ถูกใช้งาน<br />โดย **storyboard **ชื่อ "qa" มาให้ |
| start                                       | String  | false    | 0           | ค้นหาตั้งแต่แถวที่ **start** เป็นต้นไป                               |
| limit                                       | String  | false    | 50          | ค้นหาตั้งแต่แถวที่ **start** เป็นต้นไป<br />เป็นจำนวน **limit** แถว   |
| sort                                        | String  | false    | serviceName | เรียงข้อมูลตาม field **sort**                                   |
| dir                                         | String  | false    | ASC, DESC   | **ASC** : เรียงข้อมูลจาก น้อย ไป มาก<br />**DESC** : เรียงข้อมูลจาก มาก ไป น้อย |

**Sample Request**

```javascript
{
	"requestData": {
		"uiName": "",
		"remark": "",
         "status": "",
		"storyboard": "qa",
		"isAppUiNotInUsedByDmaStroyBoard": true,
		"user": "admin",
         "start": "",
		"limit": "",
		"sort": "",
		"dir": ""
	}
}
```



**Response**

| Path       | Type          | Required | Sample | Description                                                  |
| ---------- | ------------- | -------- | ------ | ------------------------------------------------------------ |
| appUiList  | Array[Object] | true     | [{}]   | appUi ทั้งหมด ภายใต้ search criteria และ paging criteria โดย limit อยู่ที่ 1000 <br />field นี้ เป็น [] ได้ |
| template   | String        | true     | x      | มีการเรียกใช้ **template** ดังกล่าว                               |
| storyboard | String        | true     | x      | ถูกเรียกใช้โดย **storyboard** ดังกล่าว                            |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1528714713725,
	"status": "success",
	"responseData": {
		"appUiList": [{
            	 "uiName": "qa_Chat_ChatOnline",
            	 "remark": "หน้าจอ Chat ของลูกค้า",
				"template": "CHAT_001_Chatting",
				"storyboard": "qa",
            	 "version": "21",
				"needUpgrade": false,
				"lastUpdate": "2019/06/07 17:48:27",
            	 "lastPublish": "2019/06/10 13:28:46",
				"status": "Draft",
				"urlScreenShot": "https://x.x.x/ims/apis/file/Download?key=x.jpg",
            	 "actionList": [{
						"actionName": "handleButton1Click"
					}, {
						"actionName": "handleButton2Click"
					}
				],
            	 "globalInputList": [{
						"path": "$.$jason.head.actions.$load.options.mockup",
						"fieldPosition": "Screen",
						"fieldType": "String",
						"inputName": "usernameChat"
					}, {
						"path": "$.$jason.head.actions.$load.options.mockup",
						"fieldPosition": "Screen",
						"fieldType": "String",
						"inputName": "displayNameChat"
					}
				],
				"globalOutputList": [{
						"outputName": "usernameChat",
						"fieldType": "String"
					}, {
						"outputName": "displayNameChat",
						"fieldType": "String"
					}
				]
			}
		]
	}
}
```



------

### ListAppUiWithAction (cancel)

**URL** : /dmaManager/apis/ui/listAppUiWithAction

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Status** : Deprecated



------

### ListTemplate

**URL** : /dmaManager/apis/ui/listTemplate

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Sample Request**

```javascript
{
	"requestData": {
		"user": "admin"
	}
}
```



**Response**

| Path         | Type          | Required | Sample | Description                                   |
| ------------ | ------------- | -------- | ------ | --------------------------------------------- |
| templateList | Array[Object] | true     | [{}]   | template ทั้งหมด ของระบบ<br />field นี้ เป็น [] ได้ |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560234278611,
	"status": "success",
	"responseData": {
		"templateList": [{
				"lastUpdate": "2019/06/07 15:54:17",
				"name": "CHAT_001_Chatting",
				"version": "5.9.6"
			}, {
				"lastUpdate": "2019/06/06 10:13:16",
				"name": "CUSTOM_003-DEVES_Claim_Online_003",
				"version": "5.0.8"
			}
		]
	}
}
```



------

###FindAppUiByName 

**URL** : /ui/findAppUiByName

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path   | Type   | Required | Sample | Description                                                |
| ------ | ------ | -------- | ------ | ---------------------------------------------------------- |
| uiName | String | false    | x      | ค้นหาด้วย **uiName**<br />ถ้าไม่มี field นี้ = ไม่มีเงื่อนไขในการค้นหา |

**Sample Request**

```javascript
{
	"requestData": {
		"uiName": "testNet01",
		"user": "admin"
	}
}
```

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560234278617,
	"status": "success",
	"responseData": {
		"id": "5cf78ffd6dde39b5e470bd95",
		"createBy": "admin",
		"lastUpdateBy": "admin",
		"createDate": 1559728125745,
		"lastUpdateDate": 1560155915780,
		"dmaManagerVersion": "5.7.8.0",
		"uiName": "LOGIN_004",
		"remark": "",
		"status": "Draft",
		"uiConfigData": {
			"template": "LOGIN_004-Login_with_Passcode_Style_2",
			"binding": [{
					"property_name": "fontSize",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": 18,
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "combo",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Font Size",
					"visible": true,
					"comment": "สำหรับตั้งค่าขนาดตัวอักษรของ Header",
					"component_type_option": {
						"option_type": "selection",
						"value_type": "number",
						"option": [{
								"name": "Tiny",
								"value": "14"
							}, {
								"name": "Small",
								"value": "16"
							}, {
								"name": "Normal",
								"value": "18"
							}, {
								"name": "Large",
								"value": "20"
							}
						]
					}
				}, {
					"property_name": "hideHeader",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": false,
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "boolean",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Hide Header",
					"visible": true,
					"comment": "ใช้สำหรับ แสดง / ซ่อน Header"
				}
			],
			"version": "5.2.5"
		},
		"templateType": "static",
		"refference": {
			"inUsed": {
				"inUsedByDmaEntiry": null,
				"inUsedByDmaStroyBoard": [{
						"version": "1",
						"name": "test_login004",
						"type": ""
					}
				],
				"inUsedByDmaEntity": null,
				"inUsedByDmaScreen": null
			},
			"version": "5",
			"required": {
				"dmaActionChainTemplateRequired": null,
				"dmaImageRequired": null,
				"dmaScreenRequired": null,
				"sgOauthRequired": null,
				"dmaTemplateRequired": [{
						"version": "5.2.5",
						"currentVersion": "5.2.5",
						"name": "LOGIN_004-Login_with_Passcode_Style_2",
						"type": "static"
					}
				],
				"sgWsdlRequired": null,
				"dmaStroyBoardRequired": null,
				"dmaActionChainRequired": null,
				"sgServiceRequired": null
			}
		},
		"inputList": [{
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "username",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}, {
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "password",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}
		],
		"dynamicInputList": null,
		"dynamicOutputList": null,
		"actionList": [{
				"actionName": "handleButton_Login"
			}, {
				"actionName": "handleButton1_Click"
			}, {
				"actionName": "handleButton2_Click"
			}, {
				"actionName": "handleButton3_Click"
			}, {
				"actionName": "handleButton4_Click"
			}, {
				"actionName": "handleAuto_Login"
			}
		],
		"dynamicApi": null,
		"dynamicDataBinding": [],
		"uiJson": "{\"$jason\":{\"head\":{\"title\":\"DMA 5.0 : LOGIN_004-Login_with_Passcode_Style_2\",\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"mockup\":\"false\"},\"success\":{\"type\":\"$render\",\"success\":{}},\"error\":{}}}}}}",
		"urlScreenShot": "",
		"lastUpdate": 1559798083781,
		"lastPublish": 1560155915752,
		"needUpgrade": false
	}
}
```



------

### FindTemplateByName

**URL** : /dmaManager/apis/ui/findTemplateByName 

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"templateName": "LOGIN_002-Login with Header Logo",
		"user": "admin"
	}
}
```

**Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560234278825,
	"status": "success",
	"responseData": {
		"actionList": [{
				"actionName": "handleButton_Login"
			}, {
				"actionName": "handleButton1_Click"
			}, {
				"actionName": "handleButton2_Click"
			}, {
				"actionName": "handleButton3_Click"
			}, {
				"actionName": "handleButton4_Click"
			}, {
				"actionName": "handleAuto_Login"
			}
		],
		"binding": [{
				"property_name": "fontSize",
				"group": "Header",
				"dataType": "resource",
				"type": "property",
				"value": 18,
				"path": "$.$jason.head.actions.$load.options.mockup",
				"component_type": "combo",
				"default": true,
				"group_description": "Header",
				"property_name_description": "Font Size",
				"visible": true,
				"comment": "สำหรับตั้งค่าขนาดตัวอักษรของ Header",
				"component_type_option": {
					"option_type": "selection",
					"value_type": "number",
					"option": [{
							"name": "Tiny",
							"value": "14"
						}, {
							"name": "Small",
							"value": "16"
						}, {
							"name": "Normal",
							"value": "18"
						}, {
							"name": "Large",
							"value": "20"
						}
					]
				}
			}, {
				"property_name": "hideHeader",
				"group": "Header",
				"dataType": "resource",
				"type": "property",
				"value": false,
				"path": "$.$jason.head.actions.$load.options.mockup",
				"component_type": "boolean",
				"default": true,
				"group_description": "Header",
				"property_name_description": "Hide Header",
				"visible": true,
				"comment": "ใช้สำหรับ แสดง / ซ่อน Header"
			}
		],
		"inputList": [{
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "username",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}, {
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "password",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}
		],
		"dynamicDataBinding": [],
		"templateName": "LOGIN_004-Login_with_Passcode_Style_2",
		"templateType": "static",
		"infoUrl": "https://x.x.x/ims/apis/file/Download?key=V.png"
	}
}
```



------

### FindServiceInputByName

**URL** : /dmaManager/apis/ui/findServiceInputByName

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Detail **: call serviceGateway get input output of service

**Request**

```javascript
{
	"requestData": {
		"apiName": "Net01",
		"user": "admin"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560237019942,
	"status": "success",
	"responseData": {
		"outputList": [],
		"inputList": [{
				"fieldName": "provinceCarReg",
				"defaultValue": "",
				"secure": "false",
				"type": "String",
				"required": "false",
				"path": "provinceCarReg",
				"promote": "true",
				"xpath": "xpath:/provinceCarReg",
				"selectionType": "",
				"rawPath": "provinceCarReg",
				"localPath": "$.$jason.head.actions.initial_form.success.options",
				"labelName": "",
				"serviceId": "province_nk",
				"value": " ",
				"frontFieldName": "",
				"placeHolder": "",
				"regexMsg": "",
				"submitform": "labelname",
				"limitSize": "",
				"needInput": "No",
				"maxlength": "",
				"searchLocation": "",
				"index": "",
				"displayFormat": "",
				"submitFormat": "",
				"codeSuccessType": "",
				"readOnly": "false",
				"guildlineImage": "",
				"serviceName": "Net01",
				"regexType": "none-regex",
				"regex": "",
				"field": "provinceCarReg",
				"successValue": "",
				"dataSetType": "",
				"name": "provinceCarReg",
				"defaultLocation": ""
			}
		]
	}
}
```



------

### SaveAppUi

**URL** : /dmaManager/apis/ui/saveAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"uiName": "Test_haveSGW_Net02",
		"remark": "",
		"id": "5cf0daf51da04dcc2b91c2ec",
		"uiConfigData": {
			"template": "LOGIN_004-Login_with_Passcode_Style_2",
			"binding": [{
					"property_name": "title",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": "Header",
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "text",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Title",
					"visible": true,
					"comment": "Header <br/>Default"
				}, {
					"property_name": "fontSize",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": 18,
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "combo",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Font Size",
					"visible": true,
					"comment": "Header",
					"component_type_option": {
						"option_type": "selection",
						"value_type": "number",
						"option": [{
								"name": "Tiny",
								"value": "14"
							}, {
								"name": "Small",
								"value": "16"
							}, {
								"name": "Normal",
								"value": "18"
							}, {
								"name": "Large",
								"value": "20"
							}
						]
					}
				}
			]
		},
		"user": "admin",
		"templateType": "static",
		"actionList": [{
				"actionName": "handleButton_Login"
			}, {
				"actionName": "handleButton1_Click"
			}, {
				"actionName": "handleButton2_Click"
			}, {
				"actionName": "handleButton3_Click"
			}, {
				"actionName": "handleButton4_Click"
			}, {
				"actionName": "handleAuto_Login"
			}
		],
		"templateInputList": [{
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "username",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}, {
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "password",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}
		],
		"urlScreenShot": ""
	}
}
```



**Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560236961682,
	"status": "success",
	"responseData": {
		"id": "5cf0daf51da04dcc2b91c2ec",
		"createDate": 1559288565120,
		"lastUpdateDate": 1560236961737,
		"createBy": "admin",
		"lastUpdateBy": "admin",
		"dmaManagerVersion": "5.7.10.0",
		"uiName": "Test_haveSGW_Net02",
		"remark": "",
		"lastUpdate": 1560236961708,
		"lastPublish": null,
		"status": "Draft",
		"uiConfigData": {
			"template": "LOGIN_004-Login_with_Passcode_Style_2",
			"binding": [{
					"property_name": "title",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": "Header",
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "text",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Title",
					"visible": true,
					"comment": "สำหรับระบุข้อความบน Header <br/>Default = แสดงได้ 1 บรรทัด"
				}, {
					"property_name": "fontSize",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": 18,
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "combo",
					"default": true,
					"group_description": "Header",
					"property_name_description": "Font Size",
					"visible": true,
					"comment": "สำหรับตั้งค่าขนาดตัวอักษรของ Header",
					"component_type_option": {
						"option_type": "selection",
						"value_type": "number",
						"option": [{
								"name": "Tiny",
								"value": "14"
							}, {
								"name": "Small",
								"value": "16"
							}, {
								"name": "Normal",
								"value": "18"
							}, {
								"name": "Large",
								"value": "20"
							}
						]
					}
				}
			]
		},
		"templateType": "static",
		"inputList": [{
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "username",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}, {
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "password",
				"path": "$.$jason.head.actions.$load.options.mockup"
			}
		],
		"dynamicInputList": null,
		"dynamicOutputList": null,
		"actionList": [{
				"actionName": "handleButton_Login"
			}, {
				"actionName": "handleButton1_Click"
			}, {
				"actionName": "handleButton2_Click"
			}, {
				"actionName": "handleButton3_Click"
			}, {
				"actionName": "handleButton4_Click"
			}, {
				"actionName": "handleAuto_Login"
			}
		],
		"dynamicApi": null,
		"dynamicDataBinding": [],
		"uiJson": "{\"$jason\":{\"head\":{\"title\":\"DMA 5.0 : LOGIN_004-Login_with_Passcode_Style_2\",\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"mockup\":\"\"},\"success\":{\"type\":\"$render\",\"success\":{}},\"error\":{}}}}}}",
		"urlScreenShot": "",
		"reference": {
			"version": "3",
			"required": {
				"dmaTemplateRequired": [{
						"version": "5.2.4",
						"currentVersion": "5.2.4",
						"name": "LOGIN_004-Login_with_Passcode_Style_2",
						"type": "static"
					}
				],
				"dmaScreenRequired": null,
				"dmaStroyBoardRequired": null,
				"dmaActionChainRequired": null,
				"dmaActionChainTemplateRequired": null,
				"dmaImageRequired": null,
				"sgServiceRequired": null,
				"sgOauthRequired": null,
				"sgWsdlRequired": null
			},
			"inUsed": {
				"inUsedByDmaScreen": null,
				"inUsedByDmaStroyBoard": null,
				"inUsedByDmaEntiry": null,
				"inUsedByDmaEntity": null
			}
		}
	}
}
```



------

### SetReady

**URL** : /dmaManager/apis/ui/setReady

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"uiName": "testNet01",
		"user": "admin"
	}
}
```



**Response**

```javascript
{
    "uiName" : "testNet01",
    "remark" : "",
    "lastUpdate" : "",
    "lastPublish" : "",
    "status" : ""
}
```



------

### UpgradeAppUi

**URL** : /dmaManager/apis/ui/upgradeAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"template": "MENU_001-Image_Menu_in_Vertical_Style",
		"needUpgrade": true,
		"actionList": [{
				"actionName": "action_button_1"
			}, {
				"actionName": "action_button_2"
			}
		],
		"remark": "",
		"uiName": "qa_MENU",
		"version": "6",
		"storyboard": "qa",
		"lastUpdate": "2019/06/05 11:00:28",
		"urlScreenShot": "https://x.x.x/ims/apis/file/Download?key=a.jpg",
		"globalOutputList": [{
				"outputName": "displayName",
				"fieldType": "String"
			}
		],
		"lastPublish": "",
		"status": "Draft",
		"globalInputList": [{
				"path": "$.$jason.head.actions.$load.options.mockup",
				"fieldPosition": "Screen",
				"fieldType": "String",
				"inputName": "displayName"
			}
		],
		"id": "Screen.model.GridMainAppUIModel-163",
		"user": "admin"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560238732992,
	"status": "success",
	"responseData": {
		"id": "5cf73e5c1da0c5349a85c46a",
		"createDate": 1559707228767,
		"lastUpdateDate": 1560238733161,
		"createBy": "admin",
		"lastUpdateBy": "admin",
		"dmaManagerVersion": "5.7.7.0",
		"uiName": "qa_MENU",
		"remark": "",
		"lastUpdate": 1559707228767,
		"lastPublish": null,
		"status": "Draft",
		"uiConfigData": {
			"template": "MENU_001-Image_Menu_in_Vertical_Style",
			"binding": [{
					"property_name": "title",
					"group": "Header",
					"dataType": "resource",
					"type": "property",
					"value": "Header",
					"path": "$.$jason.head.actions.$load.options.mockup",
					"component_type": "text",
					"default": false,
					"group_description": "Header",
					"property_name_description": "Title",
					"visible": true,
					"comment": "สำหรับระบุข้อความบน Header <br/>Default = แสดงได้ 1 บรรทัด"
				}
			],
			"version": "5.1.4"
		},
		"templateType": "static",
		"inputList": [],
		"dynamicInputList": null,
		"dynamicOutputList": null,
		"actionList": [],
		"dynamicApi": null,
		"dynamicDataBinding": [],
		"uiJson": "{\"$jason\":{\"head\":{\"title\":\"DMA 5.0}}...",
		"urlScreenShot": "https://x.x.x/ims/apis/file/Download?key=V.jpg",
		"reference": {
			"version": "6",
			"required": {
				"dmaTemplateRequired": [{
						"version": "5.1.4",
						"currentVersion": "5.1.4",
						"name": "MENU_001-Image_Menu_in_Vertical_Style",
						"type": "static"
					}
				],
				"dmaScreenRequired": null,
				"dmaStroyBoardRequired": null,
				"dmaActionChainRequired": null,
				"dmaActionChainTemplateRequired": null,
				"dmaImageRequired": null,
				"sgServiceRequired": null,
				"sgOauthRequired": null,
				"sgWsdlRequired": null
			},
			"inUsed": {
				"inUsedByDmaScreen": null,
				"inUsedByDmaStroyBoard": [{
						"version": "1",
						"name": "qa",
						"type": ""
					}
				],
				"inUsedByDmaEntiry": null,
				"inUsedByDmaEntity": null
			}
		}
	}
}
```



------

### UpgradeAllAppUi

**URL** : /dmaManager/apis/ui/upgradeAllAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

```javascript
{
	"requestData": {
		"user": "admin"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560239883462,
	"status": "success",
	"responseData": {
		"success": true,
		"resultSuccess": true,
		"resultMessage": "Upgrade all screen success.",
		"responseData": {}
	}
}
```



------

### DuplicateAppUi (cancel)

**URL** : /dmaManager/apis/ui/duplicateAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Status** : Deprecated



------

### DeleteAppUi

**URL** : /dmaManager/apis/ui/deleteAppUi

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path   | Type          | Required | Sample | Description          |
| ------ | ------------- | -------- | ------ | -------------------- |
| uiList | Array[Object] | true     | [{}]   | **appUi** ที่จะ delete |
| uiName | String        | true     | x      | ชื่อของ **appUi**      |

**Sample Request**

```javascript
{
	"requestData": {
		"uiList": [{
				"uiName": "qa_MENU_dup"
			}
		],
		"user": "admin"
	}
}
```



**Response** : call APIs ListAppUi

------





------

## StoryBoard XXX



### BackupData

**URL** /json/externalAPIs/backupData

**Method** POST

```mermaid
sequenceDiagram
	AdminWeb->>ServiceGateway : BackupData (backupList)
	ServiceGateway->>ServiceGateway : Validate backupList exists in mongodb
	ServiceGateway->>ServiceGateway : GenerateZipFile
	Note right of ServiceGateway : Loop<br>-createAPIsFolder<br>-createWSDLFolder<br>-createOauthFolder<br>-createRelateDDFolder<br>--Loop<br>---createAPIsFolder<br>---createWSDLFolder<br>---createOauthFolder<br>--End Loop<br>End Loop<br>-zip all folder<br>-zip again with<br> password
	ServiceGateway-->>AdminWeb : return success (filePath)
```

**Request**

```javascript
{
	"requestData":{
		"uidKey": "admin",
		"deviceKey": null,
		"tokenKey": "adminf67adcf8-1d1c-45c6-9927-5ec826837afb",
		"backupList":[
			{
				"id": "07658037-78fe-4526-aab8-759902acf1d1"
			},{
				"id": "6ae28b43-2625-4424-aea6-cd05c6fa76ff"
			}
		]
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"filePath": "1528442287555.dmabk"
	}
}
```





------

### DownloadBackupFile

**URL** /standard/externalAPIs/downloadBackupFile

**Method** POST

**Content-type** multipart/form-data



**Request**

```javascript
Key  		:	Value									 :	Type
------------------------------------------------------------------------
uidKey		:	admin									 :	Text
tokenKey	:	admin6f764f9d-b325-4ece-9a92-df0a927a9134	:  Text
filePath	:	1528442287555.dmabk						  :  Text
```



**Response**

```javascript
file steam
```



------

### UploadRestoreFile

**URL** /multipartformdata/externalAPIs/uploadRestoreFile

**Method** POST

**Content-type** multipart/form-data
```mermaid
sequenceDiagram
	AdminWeb->>ServiceGateway : UploadRestoreFile (file)
	ServiceGateway->>ServiceGateway : Validate file type
	ServiceGateway->>ServiceGateway : Extract ZipRestoreFile with password
	ServiceGateway->>ServiceGateway : Extract Zip again
	ServiceGateway->>Server : save extract file to local server (/tmp)
	Server-->>ServiceGateway : return success
	ServiceGateway->>ServiceGateway : GenerateAPIsValidator
	Note right of ServiceGateway : Loop<br>-read data in file<br>-prepare data with mongodb<br>-set status ADD, UPDATE<br>-createWsdlValidator<br>-createOauthValidator<br>End Loop
	ServiceGateway->>ServiceGateway : Generate total, addCount, updateCount
	ServiceGateway-->>AdminWeb : return success (restoreList)
```
**Request**

```javascript
Key  		:	Value									 :	Type
------------------------------------------------------------------------
uidKey		:	admin									 :	Text
tokenKey	:	admin6f764f9d-b325-4ece-9a92-df0a927a9134	:  Text
filePath	:	<button>Browse File<button>			       :  File
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"filePath": "1528441919477",
		"restoreList": [{
				"uuid": "14bc9747-3718-425d-992d-ded686954bf6",
				"name": "WSDL_Oauth_Input",
				"type": "SERVICE",
				"oldServiceName": "WSDL_Oauth_Input",
				"status": "UPDATE",
				"children": [{
						"uuid": "8043c74a-b3a0-4db7-89a3-1f80c4cf84b7",
						"name": "DVS_Authen",
						"type": "OAUTH",
						"leaf": true,
						"oldAuthenName": "DVS_Authen",
						"status": "UPDATE"
					}
				],
				"expanded": true
			}
		],
		"total": 1,
		"addCount": 0,
		"updateCount": 1
	}
}

```



------

### RestoreData

**URL** /json/externalAPIs/restoreData

**Method** POST
```mermaid
sequenceDiagram
	AdminWeb->>ServiceGateway : RestoreData (restoreList)
	ServiceGateway->>Server : read restoreData from file local
	Server-->>ServiceGateway : return restoreData
	ServiceGateway->>mongoDB : save restoreData to mongo by UUID
	mongoDB-->>ServiceGateway : return success
	Note right of ServiceGateway : Loop<br>-readAndRestoreAPIs<br>--set lastUpdate=now()<br>--set publishStatus=RES<br>-readAndRestoreWsdl<br>--set lastUpdate=now()<br>--set publishStatus=RES<br>-readAndRestoreOauth<br>--set lastUpdate=now()<br>--set publishStatus=RES<br>End Loop
	ServiceGateway->>Server : Delete file local
	Server-->>ServiceGateway : return success
	ServiceGateway-->>AdminWeb : return success
```


**Database Name **bes_adapter_bot

**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
		"tokenKey": "adminf67adcf8-1d1c-45c6-9927-5ec826837afb",
         "filePath": "1528441919477",
		"restoreList": [{
				"uuid": "14bc9747-3718-425d-992d-ded686954bf6",
				"name": "WSDL_Oauth_Input",
				"type": "SERVICE",
				"oldServiceName": "WSDL_Oauth_Input",
				"status": "UPDATE",
				"children": [{
						"uuid": "8043c74a-b3a0-4db7-89a3-1f80c4cf84b7",
						"name": "DVS_Authen",
						"type": "OAUTH",
						"leaf": true,
						"oldAuthenName": "DVS_Authen",
						"status": "UPDATE"
					}
				],
				"expanded": true
			}
		]
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	}
}
```



------

### ClearCache

**URL** /json/externalAPIs/clearCache

**Method** POST

**Description** remove session of wsdlFile

​	request.getSession().removeAttribute(APIsConstants.Key.WSDL_FILENAME);
	request.getSession().removeAttribute(APIsConstants.Key.WSDL_STRING);



**Request**

```javascript
{
	"requestData": {
		"id": "7194255e-509f-46ba-9717-f6bf20d31899",
		"tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b",
		"uidKey": "admin",
		"deviceKey": null
	}
}

```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {}
}
```



------

### ListData

**URL** /json/externalAPIs/listData

**Method** POST



**Request**

```javascript
{
	"requestData": {
         "uidKey": "admin",
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b",
		"deviceKey": null,
		"serviceName": "",
		"remark": "",
		"publishedStatus": "All",
		"activeStatus": "active",
		"page": 1,
         "start": 0,
		"limit": 50,
		"dir": "ASC",
		"sort": "serviceName"
	}
}

```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"searchList" : [{
				"id": "7194255e-509f-46ba-9717-f6bf20d31899",
				"fileUploadWSDL": "",
				"activeStatus": "active",
				"publishedStatus": "P",
				"apisType": "Dynamic",
				"serviceName": "Auth_Authantication",
				"remark": "",
				"authenId": "none-authen",
				"externalApisType": "webserviceapis",
				"wsdlLocation": "https://dl.dropboxusercontent.com/s/8/AuthDVS-1.0.0.wsdl",
				"wsdlFileName": "",
				"serviceEndpoint": "https://ssquat.deves.co.th/deves/ad/authen/1.0.0",
				"portTypeName": "AUTH_AuthenticateContainerPort",
				"operationName": "AUTH_AuthenticateContainer",
				"inputFieldList": [{}],
				"outputFieldList": [{}],
            	 "lastUpdated": 1528187205299,
				"lastPublished": 1528187219436
			},{
				...
			}
		],
		"total": 73
	}
}
```



------

### ListDataByType

**URL** /json/externalAPIs/listServiceDynamicDropdown

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b",
         "fieldType": "jsonservicedropdown"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"searchList" : [{
				"id": "7194255e-509f-46ba-9717-f6bf20d31899",
				"value": "7194255e-509f-46ba-9717-f6bf20d31899",
				"fileUploadWSDL": "",
				"activeStatus": "active",
				"publishedStatus": "P",
				"apisType": "Dynamic",
				"serviceName": "Auth_Authantication",
				"remark": "",
				"authenId": "none-authen",
				"externalApisType": "webserviceapis",
				"wsdlLocation": "https://dl.dropboxusercontent.com/s/8/AuthDVS-1.0.0.wsdl",
				"wsdlFileName": "",
				"serviceEndpoint": "https://ssquat.deves.co.th/deves/ad/authen/1.0.0",
				"portTypeName": "AUTH_AuthenticateContainerPort",
				"operationName": "AUTH_AuthenticateContainer",
				"inputFieldList": [{}],
				"outputFieldList": [{}],
				"lastUpdated": 1528187205299,
				"lastPublished": 1528187219436,
			},{
				...
			}
		],
		"total": 73
	}
}

```



------

### CreateData

**URL** /json/externalAPIs/createData

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b"
         "wsdlFileName": "",
		"apisType": "Dynamic",
		"serviceName": "TestNetCreate001",
		"remark": "test",
		"authenId": "3d219440-b725-47a0-88aa-13f472655814",
		"externalApisType": "staticdropdown",
		"dropdownValueList": [{
				"index": "",
				"labelName": "1",
				"value": "1",
				"id": "MyApp.model.GridStaticFieldModel-1"
			}
		],
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"id": "7ff59b5e-0062-45a0-b75a-63c8528a8ad6",
		"lastUpdated": 1528689729075
	}
}
```



------

### UpdateData

**URL** /json/externalAPIs/updateData

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b"
         "id": "7ff59b5e-0062-45a0-b75a-63c8528a8ad6",
         "wsdlFileName": "",
		"apisType": "Dynamic",
		"serviceName": "TestNetCreate001",
		"remark": "test",
		"authenId": "3d219440-b725-47a0-88aa-13f472655814",
		"externalApisType": "staticdropdown",
		"dropdownValueList": [{
				"index": "",
				"labelName": "1",
				"value": "1"
			}
		],
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"lastUpdated": 1528689729075
	}
}
```



------

### DeleteData (cancel)

**URL** /json/externalAPIs/deleteData

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"tokenKey":"admin8a357a70-e9e6-4db5-99d7-acf72c3a1996",
         "uidKey":"admin",
         "deviceKey":null,
		 "deleteList": [{
				"id": "testBot11"
			}, {
				"id": "testBot13"
			}
		 ]
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
	}
}
```



------

### DuplicateData

**URL** /json/externalAPIs/duplicateData

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"tokenKey": "admin8f318070-b3b4-455d-b241-ae23096abd2c",
		"uidKey": "admin",
		"deviceKey": null,
         "duplicateList": [{
				"id": "0cf70a73-9b39-44ba-85ec-762603aefafe",
				"oldServiceName": "ws_insertcarcheckimageTest",
				"newServiceName": "ws_insertcarcheckimageTest02"
			}
		]
	}
}
```



**Response**

```javascript
case : validate : true
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
        "validation": "true"
	}
}

case : validate : false
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"validation": false,
		"duplicateList": [{
				"id": "0cf70a73-9b39-44ba-85ec-762603aefafe",
				"oldServiceName": "ws_insertcarcheckimageTest",
				"newServiceName": "ws_insertcarcheckimageTest",
				"status": false,
				"desc": "duplicate serviceName"
			}
		]
	}
}
```



------

### UpdateHostName (cancel)

**URL** /json/externalAPIs/updateHostname

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b",
         "id": "7ff59b5e-0062-45a0-b75a-63c8528a8ad6",
         "hostname": "https://xxx.xxx.xxx"
         "updateList": [{
				"id": "0cf70a73-9b39-44ba-85ec-762603aefafe"
			}
		]
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
	}
}

```



------

### UpdateHostNameById (cancel)

**URL** /json/externalAPIs/updateHostnameById

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"uidKey": "admin",
		"deviceKey": null,
         "tokenKey": "admind1655c08-837d-4d97-8377-5569b724510b",
         "id": "7ff59b5e-0062-45a0-b75a-63c8528a8ad6",
         "hostname": "https://xxx.xxx.xxx"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
	}
}
```



------

### FindWSDetailByEndpoint

**URL** /json/externalAPIs/findWSDetailByEndpoint

**Method** POST



**Request**

```javascript
{
	"requestData": {
		"tokenKey": "admin49c4ba3b-2677-4149-a9a0-5191a8bf322f",
		"uidKey": "admin",
		"deviceKey": null,
         "wsdlLocation": "https://xxx.xxx.xx.xx/path/dma_insert?wsdl"
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"portTypeNameList": [{
				"portTypeName": "dma_insertcarcheckimageContainerPort",
				"operationNameList": [{
						"operationName": "dma_insertcarcheckimageContainer"
					}
				]
			}
		]
	}
}
```



------

### FindWSParamByEndpoint (not)

**URL** /json/externalAPIs/findWSParamByEndpoint

**Method** POST



**Request**

```javascript
{
	"requestData": {
         "tokenKey": "admin49c4ba3b-2677-4149-a9a0-5191a8bf322f",
		"uidKey": "admin",
		"deviceKey": null,
		"id": "0cf70a73-9b39-44ba-85ec-762603aefafe",
		"wsdlLocation": "https://xxx.xxx.xx.xx/path/dma_insert?wsdl",
		"portTypeName": "dma_insertcarcheckimageContainerPort",
		"operationName": "dma_insertcarcheckimageContainer",
		"wsdlFileName": ""
	}
}
```



**Response**

```javascript
{
	"success": true,
	"responseHeader": {
		"responseCode": "0000I",
		"responseDescription": "Success"
	},
	"responseData": {
		"serviceEndpoint": "https://192.168.3.190/MobileApp/webservice/dma_insertcarcheckimage",
		"inputFieldList": [{
				"index": 1,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/Username",
				"fieldName": "Username",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-34",
				"onChangeLoad": false
			}, {
				"index": 2,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/usergroup",
				"fieldName": "usergroup",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-35",
				"onChangeLoad": false
			}, {
				"index": 3,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/workstation",
				"fieldName": "workstation",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-36",
				"onChangeLoad": false
			}, {
				"index": 4,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/ewitoken",
				"fieldName": "ewitoken",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-37",
				"onChangeLoad": false
			}, {
				"index": 5,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/password",
				"fieldName": "password",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-38",
				"onChangeLoad": false
			}, {
				"index": 6,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/FrontRight_URL",
				"fieldName": "FrontRight_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-39",
				"onChangeLoad": false,
				"labelName": "ด้านขวาส่วนหน้า",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 7,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/Roof_URL",
				"fieldName": "Roof_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-40",
				"onChangeLoad": false,
				"labelName": "หลังคารถ",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 8,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/FrontLeft_URL",
				"fieldName": "FrontLeft_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-41",
				"onChangeLoad": false,
				"labelName": "ด้านซ้ายส่วนหน้า",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 9,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/SpareTyre_URL",
				"fieldName": "SpareTyre_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-42",
				"onChangeLoad": false,
				"labelName": "ยางอะไหล่",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 10,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/Back_URL",
				"fieldName": "Back_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-43",
				"onChangeLoad": false,
				"labelName": "ด้านท้ายรถ",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 11,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/CopyCarRegis_URL",
				"fieldName": "CopyCarRegis_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-44",
				"onChangeLoad": false,
				"labelName": "สำเนาทะเบียนรถ",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 12,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/Front_URL",
				"fieldName": "Front_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "3",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-45",
				"onChangeLoad": false,
				"labelName": "ด้านหน้ารถ",
				"frontFieldName": "1",
				"defaultValue": "2",
				"guildlineImage": "4",
				"selectionType": "CameraOnly",
				"limitSize": "10"
			}, {
				"index": 13,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/BackLeft_URL",
				"fieldName": "BackLeft_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-46",
				"onChangeLoad": false,
				"labelName": "ด้านซ้ายส่วนหลัง",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 14,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/BackRight_URL",
				"fieldName": "BackRight_URL",
				"fieldType": "image",
				"subFieldOfList": false,
				"promote": true,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-47",
				"onChangeLoad": false,
				"labelName": "ด้านขวาส่วนหลัง",
				"frontFieldName": "",
				"defaultValue": "",
				"guildlineImage": "",
				"selectionType": "CameraOnly",
				"limitSize": ""
			}, {
				"index": 15,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/registerNo",
				"fieldName": "registerNo",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-48",
				"onChangeLoad": false,
				"labelName": "เลขตรวจสภาพ"
			}, {
				"index": 16,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/content/username",
				"fieldName": "username",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-49",
				"onChangeLoad": false
			}, {
				"index": 17,
				"xpath": "xpath:/dma_insertcarcheckimageRequest/Password",
				"fieldName": "Password",
				"fieldType": "textfield",
				"subFieldOfList": false,
				"promote": false,
				"required": false,
				"placeHolder": "",
				"needInput": "No",
				"currentLocation": false,
				"searchLocation": false,
				"readOnly": false,
				"markerLocation": false,
				"keyboardType": "text",
				"id": "MyApp.model.InputDataModel-50",
				"onChangeLoad": false
			}
		],
		"outputFieldList": [{
				"index": 1,
				"xpath": "xpath:/dma_insertcarcheckimageResponse/Returncode",
				"fieldName": "Returncode",
				"fieldType": "codesuccess",
				"subFieldOfList": false,
				"id": "MyApp.model.GridOutputModel-39",
				"labelName": "",
				"codeSuccessType": "string",
				"successValue": "EWI-0000I",
				"promote": false
			}, {
				"index": 2,
				"xpath": "xpath:/dma_insertcarcheckimageResponse/Returnscreen",
				"fieldName": "Returnscreen",
				"fieldType": "string",
				"subFieldOfList": false,
				"id": "MyApp.model.GridOutputModel-40"
			}, {
				"index": 3,
				"xpath": "xpath:/dma_insertcarcheckimageResponse/Runmessage",
				"fieldName": "Runmessage",
				"fieldType": "message",
				"subFieldOfList": false,
				"id": "MyApp.model.GridOutputModel-41",
				"promote": false
			}, {
				"index": 4,
				"xpath": "xpath:/dma_insertcarcheckimageResponse/content",
				"fieldName": "content",
				"fieldType": "string",
				"subFieldOfList": false,
				"id": "MyApp.model.GridOutputModel-42"
			}
		]
	}
}
```



------

### FindDetailById (not)

**URL** /json/externalAPIs/findDetailById

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### ChangeActiveStatus (not)

**URL** /json/externalAPIs/changeActiveStatus

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### PublishData (not)

**URL** /json/externalAPIs/publishData

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### PublishById (not)

**URL** /json/externalAPIs/publishById

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### DownloadTemplateFile (not)

**URL** /standard/externalAPIs/downloadTemplateFile

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### UploadExcelFile (not)

**URL** /multipartformdata/externalAPIs/uploadExcelFile

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### UploadWSDLFile (not)

**URL** /multipartformdata/externalAPIs/uploadWSDLFile

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### TestConfiguration (not)

**URL** /json/externalAPIs/testConfiguration

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------





## Template

**Detail** เป็น API ที่ใช้สำหรับระบบ dmaManager.war โดยเป็น API ของ Menu Patch Template

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า template



------

### ListTemplate

**URL** : /dmaManager/apis/template/listTemplate

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "systemadmin"
	}
}
```



**Response**

| Path         | Type          | Required | Sample | Description    |
| ------------ | ------------- | -------- | ------ | -------------- |
| templateList | Array[Object] | true     | [{}]   | template ทั้งหมด |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560249457523,
	"status": "success",
	"responseData": {
		"templateList": [{
				"lastUpdate": "2019/06/05 13:43:39",
				"name": "CHAT_001_Chatting",
				"version": "5.9.1"
			}, {
				"lastUpdate": "2019/06/05 13:43:39",
				"name": "DETAIL_004-Detail_Image",
				"version": "5.1.1"
			}
		]
	}
}
```



------

### SaveTemplate ???

**URL** : /dmaManager/apis/template/saveTemplate

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path         | Type   | Required | Sample | Description |
| ------------ | ------ | -------- | ------ | ----------- |
| templateCode | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
         "templateCode": "{\"templateName\": \"\", \"binding\": []}",
		"user": "admin"
	}
}
```



**Response**

| Path | Type   | Required | Sample  | Description |
| ---- | ------ | -------- | ------- | ----------- |
| ???  | String | true     | x.x.x.x |             |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673957,
	"status": "success",
	"responseData": {
		"???": ""
	}
}
```



------

### DeleteTemplate

**URL** : /dmaManager/apis/template/deleteTemplate

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path                         | Type          | Required | Sample | Description        |
| ---------------------------- | ------------- | -------- | ------ | ------------------ |
| templateList                 | Array[Object] | true     | x      | template ที่ต้องการลบ |
| templateList[*].templateName | String        | true     | x      | ชื่อ template        |

**Sample Request**

```javascript
{
	"requestData": {
        "templateList": [
            {
                "templateName": "x"
            }
        ]
	}
}
```



**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560242987046,
	"status": "success",
	"responseData": {
		"resultSuccess": true
	}
}
```



------





## Actionchain

**Detail** เป็น API ที่ใช้สำหรับระบบ dmaManager.war โดยเป็น API ของ Menu Patch Action Chain

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า actionchain



------

### ListActionChain

**URL** : /dmaManager/apis/actionchain/listActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "systemadmin"
	}
}
```



**Response**

| Path         | Type          | Required | Sample | Description    |
| ------------ | ------------- | -------- | ------ | -------------- |
| templateList | Array[Object] | true     | [{}]   | template ทั้งหมด |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560250450912,
	"status": "success",
	"responseData": {
		"actionChainTypeList": [{
				"lastUpdate": "2019/06/06 09:50:18",
				"actionChainType": "back",
				"version": "3.0.2"
			}, {
				"lastUpdate": "2019/06/06 09:50:18",
				"actionChainType": "banner",
				"version": "3.0.3"
			}
		]
	}
}
```



------

### SaveActionchain ???

**URL** : /dmaManager/apis/actionchain/saveActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path            | Type   | Required | Sample | Description |
| --------------- | ------ | -------- | ------ | ----------- |
| actionChainCode | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
         "actionChainCode": "{\"actionChainType\": \"\", \"binding\": []}",
		"user": "admin"
	}
}
```



**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673957,
	"status": "success",
	"responseData": {
		"actionChainType": "",
         "version": "",
		"binding": [],
         "nativeJson": {}, 
	}
}
```



------

### DeleteActionchain

**URL** : /dmaManager/apis/actionchain/deleteActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path                                   | Type          | Required | Sample | Description           |
| -------------------------------------- | ------------- | -------- | ------ | --------------------- |
| actionChainTypeList                    | Array[Object] | true     | x      | actionChain ที่ต้องการลบ |
| actionChainTypeList[*].actionChainType | String        | true     | x      | ชื่อ actionChain        |

**Sample Request**

```javascript
{
	"requestData": {
        "actionChainTypeList": [
            {
                "actionChainType": "x"
            }
        ]
	}
}
```



**Response** : call API /dmaManager/apis/actionchain/listActionChain

------





## ActionChainTemplate

**Detail** เป็น API ที่ใช้สำหรับระบบ dmaManager.war โดยเป็น API ของ Menu Patch ACT

**Structure** : URL ของ API ประเภทนี้ หลัง /apis/ จะตามด้วยคำว่า actionchaintemplate



------

### ListActionChain

**URL** : /dmaManager/apis/actionchaintemplate/listActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path | Type   | Required | Sample | Description |
| ---- | ------ | -------- | ------ | ----------- |
| user | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
		"user": "systemadmin"
	}
}
```



**Response**

| Path                    | Type          | Required | Sample | Description               |
| ----------------------- | ------------- | -------- | ------ | ------------------------- |
| actionChainTemplateList | Array[Object] | true     | [{}]   | actionChainTemplate ทั้งหมด |

**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560250845387,
	"status": "success",
	"responseData": {
		"actionChainTemplateList": [{
				"actionType": "detailList_confirm_api_change",
				"lastUpdate": "2019/06/06 09:50:18",
				"version": "3.0.3"
			}, {
				"actionType": "detailList_confirm_api_self",
				"lastUpdate": "2019/06/06 09:50:18",
				"version": "3.0.5"
			}
		]
	}
}
```



------

### SaveActionChain ???

**URL** : /dmaManager/apis/actionchaintemplate/saveActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path            | Type   | Required | Sample | Description |
| --------------- | ------ | -------- | ------ | ----------- |
| actionChainCode | String | true     | x      |             |

**Sample Request**

```javascript
{
	"requestData": {
         "actionChainCode": "{\"chainTemplate\": \"\", \"actionType\": \"\"}",
		"user": "admin"
	}
}
```



**Sample Response**

```javascript
{
	"success": true,
	"genaratedAt": 1560241673957,
	"status": "success",
	"responseData": {
		"actionType": "",
         "version": "",
         "chainTemplate": {
             "type": "$set",
			"dmaType": "prepare_list",
             "options": {},
             "success": {},
         }
	}
}
```



------

### DeleteActionChain

**URL** : /dmaManager/apis/actionchaintemplate/deleteActionChain

**Method** : POST

**Content-type**  : application/json; charset=UTF-8;

**Request**

| Path                                  | Type   | Required | Sample | Description                   |
| ------------------------------------- | ------ | -------- | ------ | ----------------------------- |
| actionChainTemplateList               | String | true     | x      | actionChainTemplate ที่ต้องการลบ |
| actionChainTemplateList[*].actionType | String | true     | x      | ชื่อ actionChainTemplate        |

**Sample Request**

```javascript
{
	"requestData": {
        "actionChainTemplateList": [
            {
                "actionType": "x"
            }
        ]
	}
}
```



**Response** : call API /dmaManager/apis/actionchaintemplate/listActionChain

------





## XXX



### ListData (bot)

**URL** /config/createBot

**Method** POST

```mermaid
sequenceDiagram
	AdminWeb->>BotEngine System : /config/createBot
	BotEngine System->>BotEngine System : Insert Bot Config
	Note right of BotEngine System : Database (Config) <br>aycalBot,Config
	BotEngine System-->>AdminWeb : return success
```

**Database Name **bes_bot

> Note : Timeout ตั้งค่ารอได้สูงสุดไม่เกิน 10 วินาที กรณี mode sync

**Request**

```javascript
{
    "configName": "Aycal Bot",
	"userBot": "aycalBot",
    "adapterBot": null,
    "activeStatus": "true"
    "timeout": "100000",
    "features": {
        "welcome" : {
            "message" : "สวัสดีครับ Simibot ยินดีรับใช้"
        }
    },
	"style": {
        "backgroundColor": "#DFA471"
    },
    "menu": [
        {
            "index": 1,
            "menuName": "1",
            "tag": "Q1",
            "question": "Q1",
            "answer": "A1",
            "externalAPI": null,
            "reuiredParameter": false,
            "style": {
                "backgroundColor": "red",
                "urlImage": "https://ims.com/1.png",
                "altText": "Q1"
            }
        },
        {
            "index": 2,
            "menuName": "2",
            "tag": "Q2",
            "question": "Q2",
            "answer": "B",
            "externalAPI": "API_1_NAME",
            "reuiredParameter": false,
            "style": {
                "backgroundColor": "blue",
                "urlImage": "https://ims.com/2.png",
                "altText": "Q2"
            }
        },
        {
            "index": 3,
            "menuName": "3",
            "tag": "Q3",
            "question": "Q3",
            "answer": "ZZZ",
            "externalAPI": "API_2_NAME",
            "reuiredParameter": true,
            "style": {
                "backgroundColor": "blue",
                "urlImage": "https://ims.com/3.png",
                "altText": "Q2"
            }
        }
    ]
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```





------

### ListAuthenType (bot)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### ListGrantType (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### CreateData (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### UpdateData (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### DeleteData (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------

### TestConfiguration (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```



------





## XXX



### ListLog (not)

**URL** /config/createBot

**Method** POST

```mermaid
sequenceDiagram
	AdminWeb->>BotEngine System : /config/createBot
	BotEngine System->>BotEngine System : Insert Bot Config
	Note right of BotEngine System : Database (Config) <br>aycalBot,Config
	BotEngine System-->>AdminWeb : return success
```

**Database Name **bes_bot

> Note : Timeout ตั้งค่ารอได้สูงสุดไม่เกิน 10 วินาที กรณี mode sync

**Request**

```javascript
{
    "configName": "Aycal Bot",
	"userBot": "aycalBot",
    "adapterBot": null,
    "activeStatus": "true"
    "timeout": "100000",
    "features": {
        "welcome" : {
            "message" : "สวัสดีครับ Simibot ยินดีรับใช้"
        }
    },
	"style": {
        "backgroundColor": "#DFA471"
    },
    "menu": [
        {
            "index": 1,
            "menuName": "1",
            "tag": "Q1",
            "question": "Q1",
            "answer": "A1",
            "externalAPI": null,
            "reuiredParameter": false,
            "style": {
                "backgroundColor": "red",
                "urlImage": "https://ims.com/1.png",
                "altText": "Q1"
            }
        },
        {
            "index": 2,
            "menuName": "2",
            "tag": "Q2",
            "question": "Q2",
            "answer": "B",
            "externalAPI": "API_1_NAME",
            "reuiredParameter": false,
            "style": {
                "backgroundColor": "blue",
                "urlImage": "https://ims.com/2.png",
                "altText": "Q2"
            }
        },
        {
            "index": 3,
            "menuName": "3",
            "tag": "Q3",
            "question": "Q3",
            "answer": "ZZZ",
            "externalAPI": "API_2_NAME",
            "reuiredParameter": true,
            "style": {
                "backgroundColor": "blue",
                "urlImage": "https://ims.com/3.png",
                "altText": "Q2"
            }
        }
    ]
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```





------



## XXX



### UpdataData (not)

**URL** /config/adapterBot

**Method** POST



**Database Name **bes_adapter_bot

**Request**

```javascript
{
    "adapterBotName" : "Chat Bot Adapter",
    "hookingApi" : {
        "bot_initial": {
            "endpoint": "http://192.168.10.244:3001/hook/initial",
            "synchronize": false
        },
        "bot_send": {
            "endpoint": "http://192.168.10.244:3001/hook/send",
            "synchronize": false
        }
    }
}
```



**Response**

```javascript
{
    "resultSuccess" : true
}
```


