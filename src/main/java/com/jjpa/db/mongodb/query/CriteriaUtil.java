package com.jjpa.db.mongodb.query;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;

import blueprint.util.StringUtil;

public class CriteriaUtil {
	
	public static class Direction{
		public static String ASC  = "ASC";
		public static String DESC = "DESC";	
	}
	
	public static void addCriteriaWhere(Query query, List<WhereOption> whereOptList){
		
		String path, value;
		Object start, end;
		boolean isAcceptNull, isAcceptBlank, isNotEqual, isSearch, isBetween, isSensitive;
		for(WhereOption whereOpt : whereOptList){
			
			path = whereOpt.getPath();
			isBetween = whereOpt.isBetween();
			
			if(isBetween){
				
				start = whereOpt.getStart();
				end = whereOpt.getEnd();
				
				if(start!=null && end!=null){
					
					query.addCriteria(Criteria.where(path).gte(start).andOperator(Criteria.where(path).lte(end)));
						
				}else if(start!=null){
					
					query.addCriteria(Criteria.where(path).gte(start));
					
				}else if(end!=null){
					
					query.addCriteria(Criteria.where(path).lte(end));
				}
				
			} else {
				
				value = whereOpt.getValue();
				isSearch = whereOpt.isSearch();
				isSensitive = whereOpt.isSensitive();
				isNotEqual = whereOpt.isNotEqual();
				isAcceptNull = whereOpt.isAcceptNull();
				isAcceptBlank = whereOpt.isAcceptBlank();
				
				if(value==null){
					if(isAcceptNull){
						
						WhereOption and = whereOpt.getAnd();
						WhereOption or = whereOpt.getOr();
						
						if(and!=null){
							
							Criteria criteria = new Criteria();
							criteria.andOperator(Criteria.where(path).exists(false), Criteria.where(and.getPath()).is(and.getValue()));
							
							query.addCriteria(criteria);
							
						}else if(or!=null){
							
							Criteria criteria = new Criteria();
							criteria.orOperator(Criteria.where(path).exists(false), Criteria.where(or.getPath()).is(or.getValue()));
							
							query.addCriteria(criteria);
//							query.addCriteria(Criteria.where(path).exists(false).orOperator(Criteria.where(or.getPath()).is(or.getValue())));
						}else {
							query.addCriteria(Criteria.where(path).exists(false));
						}
						
					}	
				}else if("".equals(value)){
					if(isAcceptBlank){
						query.addCriteria(Criteria.where(path).is(value));
					}
				}else {
					if(isNotEqual){
						query.addCriteria(Criteria.where(path).ne(value));
					} else if(isSearch){
						if(isSensitive){
							query.addCriteria(Criteria.where(path).regex(Pattern.compile(DataUtils.convertMongoRegEx(value))));
						}else{
							query.addCriteria(Criteria.where(path).regex(Pattern.compile(DataUtils.convertMongoRegEx(value), Pattern.CASE_INSENSITIVE)));
						}
					}else{
						if(isSensitive){
							query.addCriteria(Criteria.where(path).is(value));
						}else{
							query.addCriteria(Criteria.where(path).regex(Pattern.compile(DataUtils.getMongoMatchRegex(value), Pattern.CASE_INSENSITIVE)));
						}
					}
				}
				
			}
				
		}
	}

//	public static void addCriteriaSearch(Query query, List<WhereOption> whereOptList){
//		
//		String path, value;
//		boolean sensitive;
//		for(WhereOption whereOpt : whereOptList){
//			
//			path = whereOpt.getPath();
//			value = whereOpt.getValue();
//			sensitive = whereOpt.isSensitive();
//			
//			if(!StringUtil.isBlank(value)){
//				if(sensitive){
//					query.addCriteria(Criteria.where(path).regex(Pattern.compile(value)));
//				}else{
//					query.addCriteria(Criteria.where(path).regex(Pattern.compile(value, Pattern.CASE_INSENSITIVE)));
//				}
//			}
//		}
//	}
	
//	public static void addCriteriaBetween(Query query, WhereOption whereOpt){
//
//		String path = whereOpt.getPath();
//		Object start = whereOpt.getStart();
//		Object end = whereOpt.getEnd();
//		
//		if(start!=null && end!=null){
//			
//			query.addCriteria(Criteria.where(path).gte(start).andOperator(Criteria.where(path).lte(end)));
//				
//		}else if(start!=null){
//			
//			query.addCriteria(Criteria.where(path).gte(start));
//			
//		}else if(end!=null){
//			
//			query.addCriteria(Criteria.where(path).lte(end));
//		}
//	}

	public static void addCriteriaPagging(Query query, GridPagging gridPagging){
		
		String sort = gridPagging.getSort();
		String dir = gridPagging.getDir();
		
		if(!StringUtil.isBlank(sort)){
			if(StringUtil.isBlank(dir) || dir.equalsIgnoreCase(Direction.ASC)){
				query.with(new Sort(Sort.Direction.ASC, sort));
			}else if(dir.equalsIgnoreCase(Direction.DESC)){
				query.with(new Sort(Sort.Direction.DESC, sort));
			}
		}
		
		if(!StringUtil.isBlank(gridPagging.getLimit())){
			query.limit(Integer.parseInt(gridPagging.getLimit()));
		}
		
		if(!StringUtil.isBlank(gridPagging.getStart())){
			query.skip(Integer.parseInt(gridPagging.getStart()));
		}
		
	}
}