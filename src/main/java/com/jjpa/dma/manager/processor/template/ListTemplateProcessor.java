package com.jjpa.dma.manager.processor.template;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListTemplateProcessor.class);
		
	protected TemplateAccessor templateAccessor;
	
	public ListTemplateProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListTemplateProcessor");
		logger.debug("Process ===> ListTemplateProcessor : {}", input.toJsonString());
		
		return templateAccessor.listTemplateName();
		
	}

}
