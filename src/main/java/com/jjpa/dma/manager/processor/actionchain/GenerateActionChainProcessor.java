package com.jjpa.dma.manager.processor.actionchain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import blueprint.util.StringUtil;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;
import com.jjpa.dma.manager.binding.action.ActionBinding;
import com.jjpa.dma.manager.binding.action.wrapper.ActionWrapper;
import com.jjpa.dma.manager.processor.binding.GenerateInitialScreenActionProcessor;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

/* 14/03/2561 add by Akanit : keep chainType to actionChainList : use for create reference action chain : finished
 * 12/06/2561 add by Akanit : check dmaEOAT, configParameter if found then binding dmaType : finished
 * */
@SuppressWarnings("unchecked")
public class GenerateActionChainProcessor {

	private static final Logger logger = LoggerFactory.getLogger(GenerateActionChainProcessor.class);

	protected ActionChainAccessor actionChainAccessor;

	protected ActionChainTemplateAccessor actionChainTemplateAccessor;

	private final String action_packageName = "com.jjpa.dma.manager.binding.action";
	private final String wrapper_packageName = "com.jjpa.dma.manager.binding.action.wrapper";
	private final String defaultClassAction = "Echo";
	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}

	public GenerateActionChainProcessor() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
	}

	
	public List<Map<String, Object>> process(Map<String, Object> dataBinding, String entityName, Map<String, Object> storyBoard, AppUiDocument appUiDoc) throws Exception {
		logger.info("###Process ===> GenerateActionChainProcessor");
		
		/* get parameter from app_ui_config */
		List<Map<String, Object>> actionList =  new ArrayList<>((List<Map<String, Object>>) storyBoard.get(DmaConstants.StoryBoard.Key.ACTION_LIST));
	
		/**
		 * Tanawat W.
		 * [2018-03-20] Add feature initial screen
		 * 
		 */
		Map<String,Object> initialScreenAction = new GenerateInitialScreenActionProcessor().process(appUiDoc, actionList, storyBoard);
		if(initialScreenAction != null){ 
			actionList.add(initialScreenAction); 
			logger.info("add initialScreenAction : " + initialScreenAction);
		}
		
		List<Map<String, Object>> actionChainList = generateActionChain(dataBinding, entityName, storyBoard, actionList);
		
		return actionChainList;
	}
		
	/* 
	 * copy from /dmaManager/src/main/java/com/jjpa/common/util/JsonBindingUtils.java 
	 * method edtion : add actionChainList and for loop
	 * */
	private List<Map<String, Object>> generateActionChain(Map<String, Object> dataBinding, String entity, Map<String, Object> storyBoard, List<Map<String, Object>> actionList) throws Exception {

		/**
		 * 05/01/2561 Add by Tanawat W.
		 * 
		 */
//		InputStream is = GenerateActionChainProcessor.class.getResourceAsStream("/chainConfig.json");
//		DocumentContext chainConfig = JsonPath.parse(is);

		/* generateActionChain */
//		String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);

		DocumentContext masterTemplate = JsonPath.parse(dataBinding);
		
		List<Map<String, Object>> actionChainList = new ArrayList<>();

		String actionName;
		for (Map<String, Object> eventMap : actionList) {
		
			/* get parameter */			
			actionName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.ACTION_NAME, null);
			

			// attach confirmDialog to networkRequest success if require and point to masterAction
			DocumentContext masterAction = JsonPath.parse((new LinkedHashMap<>()));

			Map<String,Object> tmp = new LinkedHashMap<>();
			tmp.put("root", "");
			DocumentContext output = JsonPath.parse(tmp);

			Map<String, Object> chainTemplate = null;
	
			
			/* get chainType */
			String chainType = getChainType(eventMap);
			logger.info("chainType={}", chainType);
			
			
			/* get chainTemplate */
			DMAActionChainTemplate actionChainTemplate = actionChainTemplateAccessor.findActionChainTemplateByType(chainType);
			if(actionChainTemplate==null){
				
				String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
				
				logger.info("Invalid configuration. screen={} action={}", source, actionName);
				throw new Exception("Invalid value configuration. Screen \"" + source + "\", Action \"" + actionName + "\"" );
			}
				
			chainTemplate = actionChainTemplate.getChainTemplate(); //   chainConfig.read(chainType);
		

			logger.info("Start deserializeChainForDMA");
			deserializeChainForDMA("$.root", chainTemplate, 0, "", entity, storyBoard, eventMap, output);
			logger.info("End deserializeChainForDMA  output : " + output.jsonString());

			masterAction = JsonPath.parse( (Map<String,Object>)output.read("root") );


			// Map<String, Object> confirmDialog = DataUtils.getMapFromObject(eventMap,
			// 		DmaConstants.StoryBoard.Key.CONFIRM_DIALOG);

			// if (DataUtils.getBooleanValue(confirmDialog.get(DmaConstants.StoryBoard.Key.NEED_CONFIRM))) {

			// 	if (confirmDialogAction != null) {
			// 		logger.debug("attach confirmDialog to openLoading networkRequest");
			// 		bindElement(confirmDialogAction, getPathByKey(confirmDialogActionDoc, "success"),
			// 				openLoadingAction);
			// 		logger.info("confirmDialogAction=" + confirmDialogAction.jsonString());
			// 	} else {
			// 		confirmDialogAction = openLoadingAction;
			// 	}

			// 	masterAction = confirmDialogAction;
			// } else {
			// 	masterAction = openLoadingAction;
			// }
			
			
			Map<String, Object> actionChain = new LinkedHashMap<>();
			if (masterAction == null) {
				logger.info("masterAction=" + masterAction);
				actionChain.put(actionName, masterAction);
			} else {
				logger.info("masterAction=" + masterAction.jsonString());
				
				
				/**
				 * Native input
				 * 
				 */
				// TODO wait for implement type
				Map<String, Object> nonGlobalParamMap = extractNonGlobalParameter(eventMap);
				logger.info("nonGlobalParamMap=" + nonGlobalParamMap);
				
				if(nonGlobalParamMap.size()>0){
					
					Class<?> clz = null;
					for(String className : nonGlobalParamMap.keySet()){
	
						/* get parameter */
						List<String> dmaParamList = ManagerMap.get(nonGlobalParamMap, className, List.class);
						
						/* get class */
						logger.info("using specific class action : " + className);
						clz = Class.forName(wrapper_packageName + "." + className);
						
						ActionWrapper actionWrapper = (ActionWrapper) clz.newInstance();
						
						
						/* action chain */
						masterAction = actionWrapper.wrapperActionChain(dmaParamList, masterAction);
					}
				}
				
				
				logger.info("masterAction=" + masterAction.jsonString());
				actionChain.put(actionName, masterAction.json());
			}

			/* 14/03/2561 add by Akanit : keep chainType to actionChainList : use for create reference action chain */
			actionChain.put(DmaConstants.ActionChain.Reference.Key.NAME, actionChainTemplate.getActionType());
			actionChain.put(DmaConstants.ActionChain.Reference.Key.VERSION, actionChainTemplate.getVersion());
			actionChain.put(DmaConstants.ActionChain.Reference.Key.REFERENCE, actionChainTemplate.getReference());
			
			actionChainList.add(actionChain);
			
			
			// do extra binding for action
			List<Map<String, Object>> extraPath = ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.EXTRA_PATH, List.class);
			if(extraPath!=null){
				logger.info("binding extra chain extraPath=" + extraPath);
				
				for(Map<String, Object> pathObj : extraPath){
					
					String dmaType = DataUtils.getStringValue(pathObj, DmaConstants.StoryBoard.Key.DMA_TYPE);
					String path = DataUtils.getStringValue(pathObj, DmaConstants.StoryBoard.Key.GlobalInputList.PATH);
					
					// call class specific by type (mongoDB : actionChain)
					DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(dmaType);
					if(actionChainDocument==null){
						
						logger.info("Need patching. {}", dmaType);
						throw new Exception("DMA need patching, Code \"" + dmaType + "\"");
					}
					
					DocumentContext generateActionNode = generateActionNode(entity, actionChainDocument, storyBoard, eventMap);

					logger.info("generateActionNode : " + generateActionNode.jsonString());
					
					logger.info("binding masterTemplate");
					logger.info("path={}", path);
					logger.info("value={}", generateActionNode.jsonString());
					masterTemplate.set(path, generateActionNode.json());
				}
			}
			
		}

		return actionChainList;
	}
	
	private String getChainType(Map<String, Object> eventMap) throws Exception {
		
		String actionType = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TYPE, null);
//		String apiName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.API_NAME, null);
//		String target = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TARGET, null);
		
		String chainType = "normal";
		if(actionType!=null){		
			chainType = actionType;			
		}
		
		/* append chainType */
		chainType += getSuffixConfirmDialog(eventMap);
		chainType += getSuffixCallApi(eventMap);
		chainType += getSuffixChangePage(eventMap);
		
		return chainType;
	}

	private String getSuffixConfirmDialog(Map<String, Object> eventMap) throws Exception {
		
		Map<String, Object> confirmDialog = DataUtils.getMapFromObject(eventMap, DmaConstants.StoryBoard.Key.CONFIRM_DIALOG);
		
		if(DataUtils.getBooleanValue(confirmDialog.get(DmaConstants.StoryBoard.Key.NEED_CONFIRM))){
			return "_confirm";
		}else {
			return "_noconfirm";
		}
	}
	
	private String getSuffixCallApi(Map<String, Object> eventMap) throws Exception {
		
		String apiName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.API_NAME, null);
		
		if(DmaConstants.StoryBoard.Value.DO_NOTHING.equals(apiName)){
			return "_noapi";
		}else {
			return "_api";
		}
	}

	private String getSuffixChangePage(Map<String, Object> eventMap) throws Exception {
		
		String targetType = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TARGET_TYPE, null);
		
		if(DmaConstants.StoryBoard.Value.SCREEN.equals(targetType)){
			return "_change";
		}else if(DmaConstants.StoryBoard.Value.EXTRA.equals(targetType)){
			
			String target;
			
			Map<String, Object> config = ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.CONFIG, Map.class);
			
			if(config!=null){
			
				String apiName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.API_NAME, null);
				if(DmaConstants.StoryBoard.Value.DO_NOTHING.equals(apiName)){
					return "_self";
				}else {
					return "_change";
				}
				
			}else {
				target = "_"+DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TARGET, null);
			}
			
			return target;
		}else {
			logger.info("Invalid value configuration. field="+targetType);
//			throw new Exception("Invalid value configuration. field="+property_name);
			return "";
		}
	}
	
	

	private Map<String, Object> extractNonGlobalParameter(Map<String, Object> eventMap) throws Exception {
		
		Map<String, Object> result = new HashMap<>();
		List<String> dmaParameterList;
		
		List<Map<String, Object>> globalList = new ArrayList<>();

		List<Map<String, Object>> globalInputList = ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, List.class);

		List<Map<String, Object>> globalOutputList= ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, List.class);

		if(globalOutputList != null ){
			for (Map<String, Object> item : globalOutputList) {
				globalList.add(item);
			}
		}

		if(globalInputList != null ){
			for (Map<String, Object> item : globalInputList) {
				globalList.add(item);
			}
		}
		logger.info("### merge native globalInputList -  globalOutputList : " + globalList.size() );

		


		if(globalList!=null){
			
			for(Map<String, Object> globalInput : globalList){


				

				/**
				 * Tanawat W.
				 * [2018-02-26] Edit condition Native
				 * fieldType : String is normal input
				 * fieldType : Other are Location , Call etc.
				 * 
				 * Take action warpper only when  valueFrom == Native  and fieldType != 'string'
				 * 							then TackAction 
				 * 							else DoNothing end
				 */
				String fieldType = DataUtils.getStringValue(globalInput, "fieldType", "string");
				String valueFrom = DataUtils.getStringValue(globalInput, DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM, DmaConstants.StoryBoard.Value.GLOBAL);
				if(DmaConstants.StoryBoard.Value.NATIVE.equals(valueFrom) && !"string".equalsIgnoreCase(fieldType) ){
					logger.info("###found non global type for api input.");
					
					/* get parameter */
					Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalInput, DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);
					
					String className = DataUtils.getStringValue(valueProperties, DmaConstants.StoryBoard.Key.CLASS_NAME);
					String dmaParameter = DataUtils.getStringValue(valueProperties, DmaConstants.StoryBoard.Key.DMA_PARAMETER);
					
					
					
					/* get className from result */
					if(result.containsKey(className) && !StringUtil.isBlank(className)){
						dmaParameterList = ManagerMap.get(result, className, List.class);									
					}else {
						dmaParameterList = new ArrayList<String>();
					}
					
					
					/* if not found dmaParameter in dmaParameterList */
					if(!ManagerArrayList.find(dmaParameterList, dmaParameter)){
						dmaParameterList.add(dmaParameter);
					}
					
					logger.info("className={}", className);
					logger.info("dmaParameterList={}", dmaParameterList);
					
					result.put(className, dmaParameterList);
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * 05/01/2561 Add by Tanawat W.
	 * 
	 */
	private void deserializeChainForDMA(String path, Map<String, Object> chainMap,
			int level, String parrentType, String entity, Map<String, Object> storyBoard, Map<String, Object> eventMap,
			DocumentContext output) throws Exception {
		
		if (chainMap.containsKey(DmaConstants.ActionChainTemplate.Key.ChainTemplate.TYPE)) {
			String type = chainMap.get(DmaConstants.ActionChainTemplate.Key.ChainTemplate.TYPE).toString();
			String dmaType = chainMap.get(DmaConstants.ActionChainTemplate.Key.ChainTemplate.DMA_TYPE).toString();
			String dmaEOAT = ManagerMap.getString(chainMap, DmaConstants.ActionChainTemplate.Key.ChainTemplate.DMA_EOAT);
//			String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
//			String actionName = (String) eventMap.get(DmaConstants.StoryBoard.Key.ACTION_NAME);
//			String apiName = (String) eventMap.get(DmaConstants.StoryBoard.Key.API_NAME);

			logger.info("Level : " + level + ", ParrentType : " + parrentType + ", type : " + type + ", dmaType : " + dmaType + ", path : " + path );

			
			/* 12/06/2561 add by Akanit : check dmaEOAT, configParameter if found then binding dmaType */
			if(dmaEOAT!=null){
				logger.info("This chain is dmaEOAT.");
				
				Map<String, Object> config = ManagerMap.get(eventMap, DmaConstants.StoryBoard.Key.CONFIG, Map.class);
				
				if(config!=null){
					logger.info("#found configParameter then binding dmaType of this chain.");
					
					
					/* 
					 * binding dmaType 
					 * */
					dmaType = ManagerMap.getString(config, DmaConstants.StoryBoard.Key.Config.DMA_TYPE);
					if(dmaType==null){
						throw new Exception(DmaConstants.StoryBoard.Key.Config.DMA_TYPE + " in config not found.");
					}
					
					logger.info("set "+ DmaConstants.ActionChainTemplate.Key.ChainTemplate.DMA_TYPE +" from config to chain(dmaEOAT) ="+ dmaType);
					chainMap.put(DmaConstants.ActionChainTemplate.Key.ChainTemplate.DMA_TYPE, dmaType);
					
				}
			}
			
			
			// call class specific by type 
			DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(dmaType);
			
			
			DocumentContext generateActionNode = generateActionNode(entity, actionChainDocument, storyBoard, eventMap);
			logger.info("generateActionNode : " + generateActionNode.jsonString());
			
			
			// if ("$.root".equals(path)) {
			// 	logger.info("case set : ");
			// 	output.set("$", (Map<String,Object>)generateActionNode.json());
				
			// 	logger.info("case set output : "+output.jsonString());
			// } else {
				logger.info("case bind output : " + output.jsonString());
				// waiting for path
				bindElement(output, path, generateActionNode);
			// }
				

			// next chain
			if (chainMap.containsKey(DmaConstants.ActionChainTemplate.Key.ChainTemplate.SUCCESS)) {
				deserializeChainForDMA(path + "." + DmaConstants.ActionChainTemplate.Key.ChainTemplate.SUCCESS, 
						(Map<String, Object>) chainMap.get(DmaConstants.ActionChainTemplate.Key.ChainTemplate.SUCCESS),
						level + 1, type, entity, storyBoard, eventMap, output);
			}

			if (chainMap.containsKey(DmaConstants.ActionChainTemplate.Key.ChainTemplate.ERROR)) {
				deserializeChainForDMA(path + "." + DmaConstants.ActionChainTemplate.Key.ChainTemplate.ERROR, 
						(Map<String, Object>) chainMap.get(DmaConstants.ActionChainTemplate.Key.ChainTemplate.ERROR),
						level + 1, type, entity, storyBoard, eventMap, output);
			}

		}

	}

	
	/* 
	 * copy from /dmaManager/src/main/java/com/jjpa/common/util/JsonBindingUtils.java 
	 * method edtion : do nothing.
	 * */
	private DocumentContext generateActionNode(String entity, DMAActionChainDocument actionChainDocument,
			Map<String, Object> storyBoard, Map<String, Object> eventMap) throws Exception {
		// TODO Generate Action Node
		String className = actionChainDocument.getClassName();


		/**
		 * Edit by Tanawat W.
		 * 
		 * Add defaultClassAction when not found only case prefix name contains 'echo_'
		 */
		Class<?> clz = null;
		if(className.indexOf("echo_") == 0){
			logger.info("using default class action : " + defaultClassAction);
			clz = Class.forName(action_packageName + "." + defaultClassAction);
		}else{
			logger.info("using specific class action : " + className);
			clz = Class.forName(action_packageName + "." + className);
		}

		ActionBinding actionBinding = (ActionBinding) clz.newInstance();
		return actionBinding.bind(entity, actionChainDocument, storyBoard, eventMap);

	}

	
	/* 
	 * copy from /dmaManager/src/main/java/com/jjpa/common/util/JsonBindingUtils.java 
	 * method edition : change child to child.json().
	 * */
	private void bindElement(DocumentContext parent, String jsonPath, DocumentContext child) throws Exception {
		if (jsonPath == null)
			return;
		if (child == null)
			return;

		//bind child to parent success leaf
		parent.set(jsonPath, child.json());
	}

}
