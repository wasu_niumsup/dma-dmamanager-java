package com.jjpa.dma.manager.processor.publish;


import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.AppConstants;
import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.accessor.PublishEntityConfigAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.PublishEntityConfigDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.dma.manager.processor.entity.FindByNameEntityProcessor;
import com.jjpa.dma.manager.processor.entity.SaveEntityProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.storyboard.BackupStoryBoardProcessor;
import com.jjpa.dma.manager.processor.storyboard.DownloadBackupFileProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;

/**
 * SetPublishStatusProcessor
 */
public class SetPublishStatusProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(SetPublishStatusProcessor.class);

	protected EntityAccessor entityAccessor;
	
    public SetPublishStatusProcessor(){
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");

    }

    @Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        try {
            Map<String,Object> body = input.getData();
            logger.info("body = " + body);
            String appName = DataUtils.getStringValue(body, "applicationName",null);
            String status = DataUtils.getStringValue(body, "status",null);

            if(status == null){
                throw new ValidationException("Status is null. Please set to [PUBLISHED / FAILURE]");
            }

            if(appName == null){
                throw new ValidationException("appName is null.");
            }

            EntityDocument entityDocument = entityAccessor.findEntityByName(appName);

            String setToStatus = "";
            if("PUBLISHED".equalsIgnoreCase(status)){
                setToStatus = DmaConstants.PublishedStatus.PUBLISHED;
            }else if ("FAILURE".equalsIgnoreCase(status)){
                setToStatus = DmaConstants.PublishedStatus.FAILURE;
            }else{
                throw new ValidationException("Wrong status !. Please set to [PUBLISHED / FAILURE]");
            }

            logger.info("set publish status = " + setToStatus);
			entityDocument.setPublishStatus(setToStatus);

			logger.info("set last publish = currentDate");
			Date lastPublish = new Date();
			entityDocument.setLastPublish(lastPublish);
			entityDocument = (EntityDocument) entityAccessor.updateEntity(entityDocument, lastPublish, "admin");
            
            Map<String,Object> output = new LinkedHashMap<>();
            output.put("storyname", entityDocument.getStoryName());
            output.put("appname", entityDocument.getEntityName());

            return  output;
        } catch (Exception e) {
            
            throw e;
        }
    
    }

    
}