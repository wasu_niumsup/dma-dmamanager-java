package com.jjpa.dma.manager.processor.entity;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import servicegateway.utils.ManagerMap;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.CriteriaUtil;
import org.springframework.data.mongodb.core.query.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
public class ListEntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListEntityProcessor.class);
		
	protected EntityAccessor entityAccessor;
	
	public ListEntityProcessor() {
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListEntityProcessor");
		logger.debug("Process ===> ListEntityProcessor : {}", input.toJsonString());
		String username = ManagerMap.getString(input.getData(), "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}

		if("admin".equals(username)){
			return entityAccessor.listEntity(input);
		}else{
			Query query = new Query();
		
			List<WhereOption> whereOptList = createWhereOption(input.getData());
			CriteriaUtil.addCriteriaWhere(query, whereOptList);

			return entityAccessor.listEntityByCriteria(query);
		}
		
		
		
	}




	private List<WhereOption> createWhereOption(Map<String, Object> input) throws Exception {
		

		String username = ManagerMap.getString(input, "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}
				

		
		
		// create whereOptList.
		List<WhereOption> whereOptList = new ArrayList<>();
		WhereOption whereOpt;



		if(!"admin".equals(username)){
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.Entity.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
			whereOptList.add(whereOpt);
		}
		
		return whereOptList;
	}


}
