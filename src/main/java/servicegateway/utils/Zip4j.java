package servicegateway.utils;

import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

public class Zip4j {
	
	private static final Logger logger = LoggerFactory.getLogger(Zip4j.class);
	
	public static void main(String[] args) throws Exception {
		
//		String zipFilePath1 = "C:/temp/testZip/123456.dmabk";
		String zipFilePath2 = "C:/temp/testZip/testZip4j.dmabk";
//		System.out.println(new File(zipFilePath2).getAbsolutePath());
		
		/* zip multi level */
//		ZipFiles appZip = new ZipFiles();
//		appZip.setSource("C:/temp/testZip/zip4j");
//		appZip.zipIt(zipFilePath1);
//		
//		/* zip set password */
//		new Zip4j().zip(zipFilePath1, zipFilePath2, "1234");
//		
//		new File(zipFilePath1).delete();
//		
//		
//		/* un zip */
		new Zip4j().unzip(zipFilePath2, "C:/temp/testZip/unZip4j/", "1234");
	}
	
	public void zip(String sourceFilePath, String zipFilePath) throws ZipException {
		zip(sourceFilePath, zipFilePath, null);
	}
	
	public void zip(CommonsMultipartFile file, String zipFilePath) throws ZipException {
		zip(file, zipFilePath, null);
	}
	
	public void zip(CommonsMultipartFile file, String zipFilePath, String password) throws ZipException {
		
		FileItem fileItem = file.getFileItem();
        DiskFileItem diskFileItem = (DiskFileItem) fileItem;
        File srcFile = diskFileItem.getStoreLocation();
		
		zip(srcFile.getAbsolutePath(), zipFilePath, password);
	}
	
	public synchronized void zip(String sourceFilePath, String zipFilePath, String password) throws ZipException {
    	logger.info("#sourceFilePath="+ sourceFilePath);
    	logger.info("#zipFilePath="+ zipFilePath);
    	 
    	ZipParameters zipParameters = new ZipParameters();
        zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
        zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
        zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
        if(password!=null){
        	zipParameters.setPassword(password);
        }
        
//      String baseFileName = FilenameUtils.getBaseName(filePath);
//      String destinationZipFilePath = baseFileName + "." + zipType;

        logger.info("ziping with password ...");
        new ZipFile(zipFilePath).addFile(new File(sourceFilePath), zipParameters);
        logger.info("ziping with password success");
    }

    public void unzip(String zipFilePath, String extractFilePath) throws ZipException {
    	unzip(zipFilePath, extractFilePath, null);
    }
    
    public void unzip(CommonsMultipartFile file, String extractFilePath) throws ZipException {
    	unzip(file, extractFilePath, null);
	}
	
	public void unzip(CommonsMultipartFile file, String extractFilePath, String password) throws ZipException {
		
		FileItem fileItem = file.getFileItem();
        DiskFileItem diskFileItem = (DiskFileItem) fileItem;
        File srcFile = diskFileItem.getStoreLocation();
		
		unzip(srcFile.getAbsolutePath(), extractFilePath, password);
	}
	
	public synchronized void unzip(String zipFilePath, String extractFilePath, String password) throws ZipException {
    	logger.info("#zipFilePath="+ zipFilePath);
    	logger.info("#extractZipFilePath="+ extractFilePath);
    	
    	ZipFile zipFile = new ZipFile(zipFilePath);
        if (zipFile.isEncrypted()){
        	if(password!=null){
        		zipFile.setPassword(password);
        	}
        }

        logger.info("unziping with password ...");
        zipFile.extractAll(extractFilePath);
        logger.info("unziping with password success");
    }
}
