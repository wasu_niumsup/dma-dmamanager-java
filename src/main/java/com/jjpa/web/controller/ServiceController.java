package com.jjpa.web.controller;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.BackupConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.RequestService;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.ServiceRequest;
import com.jjpa.common.data.service.ServiceResponse;
import com.jjpa.processor.ServiceProcessor;

import blueprint.util.StringUtil;

@Controller
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ServiceController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	private Properties applicationConfig = new Properties();
	private Map<String, Object> serviceInfo = new HashMap<>();

	public ServiceController() throws Exception {
		prepareService();
	}

	private void prepareService() throws Exception {
		logger.info("prepareService() : starting.");

		InputStream is = getClass().getResourceAsStream(PropConstants.Service.NAME);
		DocumentContext reqContext = JsonPath.parse(is);

		serviceInfo = reqContext.read(PropConstants.Service.Info.PATH);

		List<Map<String, Object>> list = reqContext.read(PropConstants.Service.ServiceList.PATH);
		RequestService requestService = null;
		for (Map<String, Object> map : list) {
			requestService = new RequestService();
			requestService.setServiceName(
					map.get(RequestService.Key.service_name) != null ? (String) map.get(RequestService.Key.service_name)
							: "");
			requestService.setServiceClass(map.get(RequestService.Key.service_class) != null
					? (String) map.get(RequestService.Key.service_class)
					: "");
			requestService.setServiceGroup(map.get(RequestService.Key.service_group) != null
					? (String) map.get(RequestService.Key.service_group)
					: "");
			requestService.setExceptValidateToken(map.get(RequestService.Key.except_validate_token) != null
					? (boolean) map.get(RequestService.Key.except_validate_token)
					: false);
			applicationConfig.put(requestService.getServiceName(), requestService);
			//logger.debug("service_name["+map.get("service_name")+"], service_class["+map.get("service_class")+"]");
		}

		logger.info("prepareService() : ended.");
	}

	private void logTotaltime(long startTime) {
		long ms = TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - startTime);
		if (ms >= 100) {
			logger.warn("Total time in service [over] : " + ms + " ms ");
		} else {
			logger.info("Total time in service [in time] : " + ms + " ms ");
		}
	}


	/* 07/01/2562 edit by Akanit : send parameter 'request' to method 'service.process()' */
	@RequestMapping(value = "/**", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object process(HttpServletRequest request, HttpServletResponse response,
			@RequestBody ServiceRequest<Map<String, Object>> serviceRequest) throws Exception {
		
		/* get uri */
		String serviceName = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		logger.info("serviceName="+ serviceName);
		
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			
			/* get serviceConfig from propertiesFile -> 'services.json' path in file -> .service_list */
			RequestService requestService = getRequestService(serviceName);
			
			/* generate serviceData, this is parameter for send to process() */
			ServiceData serviceData = getServiceData(serviceRequest, serviceName);
			
			/* call processor */
			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();
				
				serviceResponse.setResponseData(service.process(request, serviceData));
				serviceResponse.setSuccess(true);
			} else {
				logger.info("request " + request.getPathInfo() + " not found.");
			}
			
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return serviceResponse;
	}

	/* 20/03/2561 add by Akanit : standard (standardSubmit, download, post) */
	@RequestMapping(value="/download/*", method = RequestMethod.POST)
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("process of download");
		
		/* get uri */
		String serviceName = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		logger.info("serviceName="+ serviceName);
		
		/* get serviceConfig from propertiesFile -> 'services.json' path in file -> .service_list */
		RequestService requestService = getRequestService(serviceName);
		
		/* call processor */
		String className = requestService.getServiceClass();
		if (className != null) {
			Class<?> clz = Class.forName(className);
			ServiceProcessor service = (ServiceProcessor) clz.newInstance();

			service.process(request, response);	
		} else {
			logger.info("service " + request.getPathInfo() + " not found.");
		}
		
	}
	
	/* 02/02/2561 add by Akanit : multipart/form-data (formSubmit, upload file) */
	@RequestMapping(value={"/upload/*","/privateUpload/*"}, method=RequestMethod.POST, consumes="multipart/form-data",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody Object process(HttpServletRequest request, @RequestParam(BackupConstants.FILE_UPLOAD_NAME) CommonsMultipartFile file) throws Exception{
		logger.info("process of multipart upload");
		
		/* get uri */
		String serviceName = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		logger.info("serviceName="+ serviceName);
		
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			
			/* get serviceConfig from propertiesFile -> 'services.json' path in file -> .service_list */
			RequestService requestService = getRequestService(serviceName);
			
			/* call processor */
			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();
				
				serviceResponse.setResponseData(service.process(request, file));
				serviceResponse.setSuccess(true);
			} else {
				logger.info("service " + request.getPathInfo() + " not found.");
			}
			
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return serviceResponse;
	}
	
	@RequestMapping(value = "/html/*", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
	public @ResponseBody Object textprocessor(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> requestData) throws Exception {
		Object serviceResponse = new Object();
		long startTime = new Date().getTime();
		try {
			String mvcPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
			String functionName = mvcPath.replaceFirst("/ws", "");
			ServiceData serviceData = new ServiceData(requestData);
			RequestService requestService = (RequestService) applicationConfig.get(functionName);

			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();

				serviceResponse = service.process(request, serviceData);

			} else {
				logger.debug("GET-JSON-->" + functionName);
				logger.debug("{}", requestData);
			}
			return serviceResponse;
		} catch (Exception e) {
			//	logger.error("", e);
			throw new Exception("Service Error Please contract Admin.");
		} finally {
			logTotaltime(startTime);

		}

	}

	@RequestMapping(value = {
			"/authen/getVersion" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getVersion(HttpServletRequest request) {
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			Map<String, Object> data = new HashMap<>();
			String version = "1.0";
			Map dmamanager = (Map) serviceInfo.get("dmamanager");
			if (dmamanager != null) {
				version = (String) dmamanager.get("version");
			}
			data.put("version", version);
			serviceResponse.setResponseData(data);
			serviceResponse.setSuccess(true);
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return serviceResponse;
	}

	@RequestMapping(value = {
			"/authen/login" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object login(HttpServletRequest request,
			@RequestBody ServiceRequest<Map<String, Object>> serviceRequest) {
		String functionName = "/authen/login";
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			Map<String, Object> data = serviceRequest.getRequestData();
			data.put("functionName", functionName);
			data.put("clientIp", request.getRemoteAddr());
			data.put("serverIp", request.getLocalAddr());

			ServiceData serviceData = new ServiceData(data);
			RequestService requestService = (RequestService) applicationConfig.get(functionName);
			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();
				serviceResponse.setResponseData(service.process(request, serviceData));
				serviceResponse.setSuccess(true);
			} else {
				logger.debug("POST-JSON-->" + functionName);
				logger.debug("{}", data);
			}
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return serviceResponse;
	}

	@RequestMapping(value = {
			"/authen/logout" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object logout(HttpServletRequest request) {
		String functionName = "/authen/logout";
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			request.getSession().invalidate();
			Map<String, Object> data = new HashMap<>();
			data.put("functionName", functionName);

			ServiceData serviceData = new ServiceData(data);
			RequestService requestService = (RequestService) applicationConfig.get(functionName);
			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();
				serviceResponse.setResponseData(service.process(request, serviceData));
				serviceResponse.setSuccess(true);
			} else {
				logger.debug("POST-JSON-->" + functionName);
				logger.debug("{}", data);
			}
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return serviceResponse;
	}

	@RequestMapping(value = { "/authen/logout" }, method = RequestMethod.GET)
	public String logout2(HttpServletRequest request) {
		String functionName = "/authen/logout";
		long startTime = System.currentTimeMillis();
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		serviceResponse.setGenaratedAt(startTime);
		try {
			request.getSession().invalidate();
			Map<String, Object> data = new HashMap<>();
			data.put("functionName", functionName);

			ServiceData serviceData = new ServiceData(data);
			RequestService requestService = (RequestService) applicationConfig.get(functionName);
			String className = requestService.getServiceClass();
			if (className != null) {
				Class<?> clz = Class.forName(className);
				ServiceProcessor service = (ServiceProcessor) clz.newInstance();
				serviceResponse.setResponseData(service.process(request, serviceData));
				serviceResponse.setSuccess(true);
			} else {
				logger.debug("POST-JSON-->" + functionName);
				logger.debug("{}", data);
			}
		} catch (Exception e) {
			logger.error("", e);
			if(!StringUtil.isBlank(e.getMessage())){
				serviceResponse.setResponseDesc(e.getMessage());
			}
		} finally {
			logTotaltime(startTime);
		}
		return "redirect:/";
		// return "";
	}

	
	private RequestService getRequestService(String serviceName) throws Exception{
		
		/* get detail of service from applicationConfig */
		RequestService requestService = (RequestService)applicationConfig.get(serviceName);
		
		if(requestService==null){
			throw new Exception("request path : "+ serviceName +" not found.");
		}
		
		return requestService;
	}
	private ServiceData getServiceData(ServiceRequest<Map<String, Object>> serviceRequest, String serviceName) {
		Map<String, Object> requestData = serviceRequest.getRequestData();
		if (requestData == null) {
			requestData = new LinkedHashMap<>();
		}
		requestData.put("functionName", serviceName);
		return new ServiceData(requestData);
	}
	
}
