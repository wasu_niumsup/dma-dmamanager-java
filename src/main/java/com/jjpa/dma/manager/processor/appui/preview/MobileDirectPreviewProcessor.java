package com.jjpa.dma.manager.processor.appui.preview;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;


/**
 * MobileDirectPreviewProcessor
 */
public class MobileDirectPreviewProcessor  extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private static final Logger logger = LoggerFactory.getLogger(DirectPreviewProcessor.class);

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		try {
            logger.info("0. MobileDirectPreviewProcessor");
            logger.info("0. body = " + input.getData());


            Map<String,Object> d = input.getData();
            String key =  DataUtils.getStringValue(d, "previewNumber",null);

            String tempFolderPath = ConfigurationUtils.getTempPreview();
            String fullPath = tempFolderPath + key + ".dma";
            logger.info("1. fullPath :" + fullPath);

            Map<String,Object> output = new LinkedHashMap<>();
            if ( new File(fullPath).exists() ){
                InputStream is  = new FileInputStream(fullPath);
                DocumentContext ctx = JsonPath.parse(is);
    
            
                output = ctx.json();
               
            }else{
                logger.warn("Not found file");
                throw new Exception("Not found file");
                
            }
            logger.info("2. output :" + output);

            return output;
        } catch (Exception e) {
            logger.error("#Error {}", e);
            
            InputStream is = MobileDirectPreviewProcessor.class
				.getResourceAsStream("/page/preview_error.json");
            DocumentContext ctx = JsonPath.parse(is);
            return ctx.json();
        }
	}

    
}