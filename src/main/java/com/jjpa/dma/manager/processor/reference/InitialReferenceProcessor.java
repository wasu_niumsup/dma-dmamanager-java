package com.jjpa.dma.manager.processor.reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.dma.manager.model.reference.InUsedReference;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.model.reference.RequiredReference;

public class InitialReferenceProcessor {

	private static final Logger logger = LoggerFactory.getLogger(InitialReferenceProcessor.class);
	
	public Reference process(Reference reference) throws Exception {
		logger.info("process : initial reference.");
		
		if(reference==null){
			logger.info("Reference not found, then create Reference.");
			reference = new Reference();
		}
		
		InUsedReference inUsed = reference.getInUsed();
		if(inUsed==null){
			logger.info("InUsedReference not found, then create InUsedReference.");
			inUsed = new InUsedReference();
			reference.setInUsed(inUsed);
		}
		
		RequiredReference required = reference.getRequired();
		if(required==null){
			logger.info("RequiredReference not found, then create RequiredReference.");
			required = new RequiredReference();
			reference.setRequired(required);
		}
		
		return reference;
	}
}
