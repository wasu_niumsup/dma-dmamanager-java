package com.jjpa.db.mongodb.accessor;

import java.util.Map;

import com.jjpa.common.data.service.ServiceData;

public interface ResponseStatusTypeAccessor {

	public Map<String, Object> listResponseStatusType(ServiceData input) throws Exception;
	
}
