package com.jjpa.dma.manager.binding.action.wrapper;

import java.util.List;

import com.jayway.jsonpath.DocumentContext;

public interface ActionWrapper {

	public DocumentContext wrapperActionChain(List<String> dmaParamList, DocumentContext masterAction) throws Exception;
}
