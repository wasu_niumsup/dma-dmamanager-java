package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;

public class ActionChainTemplateAccessorImpl implements ActionChainTemplateAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;
	
	protected ActionChainTemplateAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
		
	}
	
	public Object saveActionChainTemplate(DMAActionChainTemplate actionChainTemplate, String user) throws Exception {
		
		try{
			DMAActionChainTemplate oldTemplate = findActionChainTemplateByType(actionChainTemplate.getActionType());
			if(oldTemplate != null){
				//oldTemplate.setBinding(actionChainTemplate.getBinding());
				oldTemplate.setChainTemplate(actionChainTemplate.getChainTemplate());
				oldTemplate.setReference(actionChainTemplate.getReference());
				oldTemplate.setVersion(actionChainTemplate.getVersion());
				oldTemplate.setLastUpdate(actionChainTemplate.getLastUpdate());
				actionChainTemplate = oldTemplate;
			}
			return mongoDbAccessor.save(actionChainTemplate, user);
		}catch(Exception e){
			throw e;
		}
		
	}

	public DMAActionChainTemplate findActionChainTemplateByType(String type) throws Exception {
		
		Query findByType = new Query();
		findByType.addCriteria(Criteria.where("actionType").regex(Pattern.compile(DataUtils.getMongoMatchRegex(type), Pattern.CASE_INSENSITIVE)));
		
		try{
			return mongoDbAccessor.findOneByCriteria(findByType, DMAActionChainTemplate.class);
		}catch(Exception e){
			throw e;
		}
	}

	public List<DMAActionChainTemplate> listActionChainTemplate() throws Exception {
		
		try{
			return mongoDbAccessor.findAll(DMAActionChainTemplate.class);
		}catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,Object> listActionChainTemplateType() throws Exception {
		List<DMAActionChainTemplate> templateObjList = listActionChainTemplate();
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String,Object>> templateList = new ArrayList<Map<String,Object>>();
		for(DMAActionChainTemplate document : templateObjList){
			Map<String,Object> actionChainTemplate = new HashMap<String,Object>();
			actionChainTemplate.put("actionType", document.getActionType());

			/* add by warrawan 12/4/2018 - add version and lastUpdate to listActionChainTemplate*/
			actionChainTemplate.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
			actionChainTemplate.put("version", document.getVersion());
			/*end */

			templateList.add(actionChainTemplate);
		}
		result.put("actionChainTemplateList", templateList);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public void deleteActionChainTemplate(ServiceData input) throws Exception {
		String user = (String)input.getValue("user");
		List<Map<String, Object>> templateList = (List<Map<String, Object>>)input.getValue("actionChainTemplateList");
		for(Map<String, Object> templateObj : templateList){
			String actionType = (String)templateObj.get("actionType");
			mongoDbAccessor.delete(findActionChainTemplateByType(actionType), user);
		}
		
	}
	
}
