package servicegateway.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManagerZip {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerZip.class);
	
	public static void zipFiles(List<ZipEntryModel> zipEntryList, String zipSrc) throws Exception{
		logger.info("do zipFiles()");
		logger.info("zipSrc="+zipSrc);
		
		List<String> fileSrcList = new ArrayList<String>();
		List<String> fileNameList = new ArrayList<String>();
		
		
		/* 
		 * Prepare data
		 * 
		 * get fileSrcList and fileNameList 
		 * */
		String filePath, fileName;
		for(ZipEntryModel zipEntry : zipEntryList){
			
			filePath = zipEntry.getEntryPath();
			fileName = zipEntry.getEntryName();
			
			// set filePath
			fileSrcList.add(filePath);
			
			// set fileName
			if(fileName==null){
				fileName = ManagerStore.getFileNameByPart(filePath);
			}
			fileNameList.add(fileName);
		}	
		logger.info("fileSrcList="+fileSrcList);
		logger.info("fileNameList="+fileNameList);
		
		
		
		/* 
		 * Zip file
		 *  */	
		ManagerStore.intervalCheckFileExists(fileSrcList, 50, 200);//50=loopTimeout(round) , 200=time(milisec) ---- sum 10 sec
		
		ZipFiles zip = new ZipFiles();		
		try{
			logger.info("ziping ...");
			zip.zipSelectedDynamicName(zipEntryList, zipSrc);
			
//			ManagerStore.intervalCheckFileExists(zipSrc, 50, 200);//50=loopTimeout(round) , 200=time(milisec) ---- sum 10 sec
//			
//			ManagerStore.intervalCheckEntryZipFileExists(zipSrc, fileNameList, 50, 200);///50=loopTimeout(round) , 200=time(milisec) ---- sum 10 sec		
			logger.info("zip success.");
		
		} finally {			
			ManagerStore.deleteFiles(fileSrcList);
		}
		
		logger.info("end zipFiles()");
	}
}
