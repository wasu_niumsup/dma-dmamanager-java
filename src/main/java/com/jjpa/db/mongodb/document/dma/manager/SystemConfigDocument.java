package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.jjpa.common.PatchConstants;
import com.jjpa.db.mongodb.document.BaseDocument;

import servicegateway.utils.ManagerMap;

@Document(collection = "system_config")
public class SystemConfigDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;
	
	private String patch_version;
	private String patch_description;
	private Long patch_lastUpdate;
	
	public SystemConfigDocument() {
	}
	
	public SystemConfigDocument(Map<String, Object> dataMap) throws Exception {
		fromMap(dataMap);
	}
	
	public String getPatch_version() {
		return patch_version;
	}
	public void setPatch_version(String patch_version) {
		this.patch_version = patch_version;
	}
	public String getPatch_description() {
		return patch_description;
	}
	public void setPatch_description(String patch_description) {
		this.patch_description = patch_description;
	}
	public Long getPatch_lastUpdate() {
		return patch_lastUpdate;
	}
	public void setPatch_lastUpdate(Long patch_lastUpdate) {
		this.patch_lastUpdate = patch_lastUpdate;
	}
	
	public void fromMap(Map<String, Object> dataMap) throws Exception {
		
		String id = ManagerMap.getString(dataMap, "id");
		if(id!=null){
			this.setId(id);
		}
		
		String createBy = ManagerMap.getString(dataMap, "createBy");
		if(createBy!=null){
			this.setCreateBy(createBy);
		}
		
		String lastUpdateBy = ManagerMap.getString(dataMap, "lastUpdateBy");
		if(lastUpdateBy!=null){
			this.setLastUpdateBy(lastUpdateBy);
		}
		
		/* set create date */
		Object createDate = dataMap.get("createDate");
		if(createDate instanceof Long){
			this.setCreateDate(new Date((Long)createDate));
		}else if(createDate instanceof Date){
			this.setCreateDate((Date)createDate);
		}else{
			this.setCreateDate(new Date());
		}
		
		/* set lastUpdate date */
		Object lastUpdateDate = dataMap.get("lastUpdateDate");
		if(lastUpdateDate instanceof Long){
			this.setCreateDate(new Date((Long)lastUpdateDate));
		}else if(lastUpdateDate instanceof Date){
			this.setCreateDate((Date)lastUpdateDate);
		}else{
			this.setCreateDate(new Date());
		}
		
		String patch_version = ManagerMap.getString(dataMap, PatchConstants.PATCH_VERSION);
		if(patch_version!=null){
			this.setPatch_version(patch_version);
		}

		String patch_description = ManagerMap.getString(dataMap, PatchConstants.PATCH_DESCRIPTION);
		if(patch_description!=null){
			this.setPatch_description(patch_description);
		}

		Long patch_lastUpdate = ManagerMap.getLong(dataMap, PatchConstants.PATCH_LAST_UPDATE);
		if(patch_lastUpdate!=null){
			this.setPatch_lastUpdate(patch_lastUpdate);
		}
		
	}
	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate() != null ? this.getCreateDate().getTime() : new Date().getTime() );
		map.put("lastUpdateDate", this.getLastUpdateDate() != null ? this.getLastUpdateDate().getTime() : new Date().getTime() );
		map.put(PatchConstants.PATCH_VERSION, this.getPatch_version());
		map.put(PatchConstants.PATCH_DESCRIPTION, this.getPatch_description());
		map.put(PatchConstants.PATCH_LAST_UPDATE, this.getPatch_lastUpdate());
		
		return map;
	}
	
}
