package servicegateway.utils;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class ManagerJsonPath {

	public static <T> T read(DocumentContext ctx, String path) throws Exception {
        return read(ctx, path, String.class);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T read(DocumentContext ctx, String path, Class<?> cls) throws Exception {

        Object result = null;
        try{
            result = cls.cast(ctx.read(path));    
        }catch (Exception e){}
        
        return (T) result;
    }
    
	public static <T> T read(Object dataMap, String path) throws Exception {
    	return read(dataMap, path, String.class);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T read(Object dataMap, String path, Class<?> cls) throws Exception {

    	Object result = null;
		try{
			result = cls.cast(JsonPath.read(dataMap, path));	
		}catch (Exception e){}
		
		return (T) result;
    }
    
    public static Map<String, Object> parseMap(Object dataMap) throws Exception {

    	Map<String, Object> result = null;
		try{
			result = JsonPath.parse(dataMap).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static Map<String, Object> parseMap(String data) throws Exception {

    	Map<String, Object> result = null;
		try{
			result = JsonPath.parse(data).json();
		}catch (Exception e){}
		
		return result;
    }
    
    public static Map<String, Object> parseMap(File file) throws Exception {

    	Map<String, Object> result = null;
		try{
			result = JsonPath.parse(file).json();
		}catch (Exception e){}
		
		return result;
    }
    
    public static Map<String, Object> parseMap(InputStream is) throws Exception {

    	Map<String, Object> result = null;
		try{
			result = JsonPath.parse(is).json();
		}catch (Exception e){}
		
		return result;
    }
    
    public static Map<String, Object> parseMap(URL url) throws Exception {

    	Map<String, Object> result = null;
		try{
			result = JsonPath.parse(url).json();
		}catch (Exception e){}
		
		return result;
    }
    
    public static List<Map<String, Object>> parseList(Object dataMap) throws Exception {

    	List<Map<String, Object>> result = null;
		try{
			result = JsonPath.parse(dataMap).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static List<Map<String, Object>> parseList(String data) throws Exception {

    	List<Map<String, Object>> result = null;
		try{
			result = JsonPath.parse(data).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static List<Map<String, Object>> parseList(File file) throws Exception {

    	List<Map<String, Object>> result = null;
		try{
			result = JsonPath.parse(file).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static List<Map<String, Object>> parseList(InputStream is) throws Exception {

    	List<Map<String, Object>> result = null;
		try{
			result = JsonPath.parse(is).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static List<Map<String, Object>> parseList(URL url) throws Exception {

    	List<Map<String, Object>> result = null;
		try{
			result = JsonPath.parse(url).json();	
		}catch (Exception e){}
		
		return result;
    }
    
    public static String parseString(Object dataMap) throws Exception {

    	String result = null;
		try{
			result = JsonPath.parse(dataMap).jsonString();	
		}catch (Exception e){}
		
		return result;
    }
    
}
