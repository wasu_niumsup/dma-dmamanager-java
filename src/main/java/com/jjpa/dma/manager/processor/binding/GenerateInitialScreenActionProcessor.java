package com.jjpa.dma.manager.processor.binding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;

import servicegateway.utils.ManagerMap;

/**
 * GenerateInitialScreenActionProcessor
 */
public class GenerateInitialScreenActionProcessor {
    public static class Key{
        public final  static String actionName = "_dma_initial_screen_action";
    }
    private static final Logger logger = LoggerFactory.getLogger(GenerateInitialScreenActionProcessor.class);

    public Map<String, Object> process(AppUiDocument appUiDoc, List<Map<String, Object>> actionChainList,
            Map<String, Object> storyBoard) throws Exception {
        logger.info("###Process ===> GenerateInitialScreenActionProcessor");
        Map<String, Object> actionResult = new LinkedHashMap<>();

        String templateType = appUiDoc.getTemplateType();

        if (DmaConstants.AppUI.Value.DYNAMIC_INPUT.equals(templateType)) {
            logger.info("case : templateType=" + templateType);

            List<Map<String, Object>> screenInput = ManagerMap.get(storyBoard, DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, List.class);
            logger.info("screenInput : " + screenInput);

            List<Map<String, Object>> globalInputList = new ArrayList<>( screenInput);
            if(globalInputList != null){
                for (int i = 0; i < globalInputList.size(); i++) {
                    Map<String, Object> item  = globalInputList.get(i);
                    String inputName = DataUtils.getStringValue(item, DmaConstants.StoryBoard.Key.GlobalInputList.INPUT_NAME,null);
                    

                    item.put(DmaConstants.StoryBoard.Key.GlobalInputList.SERVICE_NAME, appUiDoc.getDynamicApi());
                    item.put(DmaConstants.StoryBoard.Key.NAME,inputName);
                }
            }


            Map<String, Object> confirmDialog = new LinkedHashMap<>();
            confirmDialog.put("needConfirm", false);
            actionResult.put("actionName", Key.actionName);
            actionResult.put("apiName", appUiDoc.getDynamicApi());
           
            actionResult.put("targetType", "Extra");
            actionResult.put("target", "initialScreen");
            actionResult.put("globalInputList", globalInputList);
            actionResult.put("confirmDialog", confirmDialog);
            

        }else {
            actionResult = null;
        }
        return actionResult;
    }

}