package servicegateway.utils;

public class ZipEntryModel {
	
	private String entryPath;
	private String entryName;
	
	public String getEntryPath() {
		return entryPath;
	}
	public void setEntryPath(String entryPath) {
		this.entryPath = entryPath;
	}
	public String getEntryName() {
		return entryName;
	}
	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}
	
}
