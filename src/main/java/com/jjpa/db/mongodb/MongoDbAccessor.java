package com.jjpa.db.mongodb;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.jjpa.db.mongodb.bean.Pager;
import com.jjpa.db.mongodb.document.BaseDocument;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.SelectOption;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.DBObjectUtil;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;

@SuppressWarnings("rawtypes")
public class MongoDbAccessor {

	private static Logger logger = Logger.getLogger(MongoDbAccessor.class);
	
	protected MongoOperations mongoOperation;

	public static class Dir{
		public static String ASC  = "ASC";
		public static String DESC = "DESC";	
	}
	
	protected MongoDbAccessor(MongoTemplate mongoTemplate) {
		this.mongoOperation = mongoTemplate;
	}

	public <T> T save(T obj, String user) throws Exception {
		if (obj instanceof BaseDocument) {
			Date now = new Date();
			if (StringUtil.isBlank(((BaseDocument) obj).getId())) {
				((BaseDocument) obj).setCreateBy(user);
				((BaseDocument) obj).setCreateDate(now);
			}
			((BaseDocument) obj).setLastUpdateBy(user);
			((BaseDocument) obj).setLastUpdateDate(now);
		}
		mongoOperation.save(obj);
		return obj;
	}

	public int delete(Object obj, String user) throws Exception {
		return mongoOperation.remove(obj).getN();
	}

	public <T> int deleteByQuery(Query query, Class<T> clz, String user) throws Exception {
		return mongoOperation.remove(query, clz).getN();
	}

	public <T> List<T> findAll(Class<T> clz) throws Exception {
		return mongoOperation.findAll(clz);
	}

	public <T> T findById(String id, Class<T> clz) throws Exception {
		Query findById = new Query(Criteria.where("_id").is(new ObjectId(id)));
		return mongoOperation.findOne(findById, clz);
	}

	public <T> List<T> findById(List<String> id, Class<T> clz) throws Exception {
		List<ObjectId> objectIds = new ArrayList<>();
		if (id != null) {
			for (String stid : id) {
				objectIds.add(new ObjectId(stid));
			}
		}

		Query findById = new Query(Criteria.where("_id").in(objectIds));
		return mongoOperation.find(findById, clz);
	}

	public <T> List<T> findById(List<String> id, Class<T> clz, Sort sort) throws Exception {
		List<ObjectId> objectIds = new ArrayList<>();
		if (id != null) {
			for (String stid : id) {
				objectIds.add(new ObjectId(stid));
			}
		}

		Query findById = new Query(Criteria.where("_id").in(objectIds));
		if(sort != null){
			findById.with(sort);
		}
		return mongoOperation.find(findById, clz);
	}

	public <T> List<T> findByCriteria(Query query, Class<T> clz) throws Exception {
		return mongoOperation.find(query, clz);
	}

	public <T> T findOneByCriteria(Query query, Class<T> clz) throws Exception {
		return mongoOperation.findOne(query, clz);
	}

	public <T> T findOneAndModify(Query query, Update update, Class<T> clz) {
		FindAndModifyOptions modifyOpts = new FindAndModifyOptions();
		modifyOpts.returnNew(true);
		return mongoOperation.findAndModify(query, update, modifyOpts, clz);
	}
	
	public int findCountByCriteria(Query query, Class clz) throws Exception {
		return Integer.parseInt(String.valueOf(mongoOperation.count(query, clz)));
	}
	
	public <T> Pager<T> findPage(Pager<T> pager, Query query) {
		Class<T> clz = pager.getEntityClass();
		pager.setTotalCount((int) mongoOperation.count(query, clz));
		int pageNumber = pager.getPageNumber();
		int pageSize = pager.getPageSize();
		query.skip((pageNumber - 1) * pageSize).limit(pageSize);
		pager.setList(mongoOperation.find(query, clz));
		return pager;
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> aggregate(String collectionName, List<SelectOption> selectOptList, List<WhereOption> whereOptList, GridPagging gridPagging) throws Exception {
		if(StringUtil.isBlank(collectionName)){
			return null;
		}
		
		DBCollection dbCollection = mongoOperation.getCollection(collectionName);
		
		
		List<DBObject> aggregateList = new ArrayList<>();
		
		DBObjectUtil.bindingMatch(aggregateList, whereOptList);

		
		/* 
		 * call mongo get count
		 *  */
		int count = aggregateCount(dbCollection, aggregateList);

		
		/* 
		 * call mongo get data
		 *  */
		List<Map<String, Object>> dataList = new ArrayList<>();
		if(count>0){

			DBObjectUtil.bindingGridPagging(aggregateList, gridPagging);
			DBObjectUtil.bindingProject(aggregateList, selectOptList);
			
			
			/* call mongo for get data */
			logger.info("call aggregate mongo collection="+ dbCollection.getName());
			Cursor aggregateOutput = dbCollection.aggregate(aggregateList, AggregationOptions.builder().allowDiskUse(true).build());
			
			while ( aggregateOutput.hasNext() ) {
		        DBObject doc = aggregateOutput.next();
		        dataList.add(doc.toMap());
		    }
		
//			logger.info("dataList="+ dataList);
			logger.info("call success, but ignore show data.");
		}
		
		
		/* mapping result */
		Map<String, Object> result = new LinkedHashMap<>();
		result.put("dataList", dataList);
		result.put("count", count);
		
		return result;
	}

	@SuppressWarnings("unchecked")
	private int aggregateCount(DBCollection dbCollection, List<DBObject> aggregateList) throws Exception {
		
		
		/* binding criteria count */
		DBObject count = new BasicDBObject("$count", "total");
		aggregateList.add(count);
		logger.info(count.toMap());
		
		
		/* call mongo for get count */
		logger.info("call aggregate mongo collection="+ dbCollection.getName());
		Cursor aggregateCount = dbCollection.aggregate(aggregateList, AggregationOptions.builder().allowDiskUse(true).build());
		
		
		/* get total */
		int total = 0; 
		while ( aggregateCount.hasNext() ) {
	        DBObject doc = aggregateCount.next();
	        total = ManagerMap.getIntger(doc.toMap(), "total");
	        break;
	    }
		
		//remove count
		aggregateList.remove(count);
		
		
		logger.info("total="+ total);
		return total;
	}

}
