package com.jjpa.dma.manager.processor.binding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMAComponent;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
public class BindingComponentProcessor{

	private static final Logger logger = LoggerFactory.getLogger(BindingComponentProcessor.class);

	public MongoDbAccessor getMongoDbAccessor() {
		return SpringApplicationContext.getBean("MongoDbAccessor");
	}
	
	
	public Map<String, Object> process(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception {

		bindingDomain(dataBinding, appUiDoc);
		bindingTemplate(dataBinding, appUiDoc);
		
		return dataBinding;
	}
	
	/* akanit 20/02/2018 */
	private void bindingDomain(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception{
		
		/* find rowList where type=domain */
		List<Map<String, Object>> bindingDomainList = ManagerArrayList.getRowListBySearchString(appUiDoc.getDynamicDataBinding(), DmaConstants.AppUI.Key.TYPE, DmaConstants.Template.Type.DOMAIN);
		
		if(bindingDomainList != null && bindingDomainList.size()!=0){
			
			DocumentContext ctx = JsonPath.parse(dataBinding);
			
			String path;
			String domain = JasonConfigurationUtil.getPublishUrl();
			
			logger.info("binding domain.");
			for(Map<String, Object> rowDomain : bindingDomainList){
				
				// get parameter
				path = ManagerMap.getString(rowDomain, DmaConstants.AppUI.Key.BINDING);
				
				logger.info("path {}", path);
				logger.info("value {}", domain);
				ctx.set(path, domain);
			}
		}
	}
	
	
	/* 
	 * copy from /dmaManager/src/main/java/com/jjpa/common/util/JsonBindingUtils.java 
	 * method edition : remove bindingAction
	 * */
	private void bindingTemplate(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception{
		
		/* old code ctx is templateJson */
//		DocumentContext ctx = input.getJsonDocument();
		DocumentContext ctx = JsonPath.parse(dataBinding);
		
		List<Map<String, Object>> bindingList = (List<Map<String, Object>>) appUiDoc.getUiConfigData().get(DmaConstants.AppUI.Key.BINDING);

		if (bindingList != null && bindingList.size() > 0) {
			logger.info("###found binding in appUiConfig. then binding template and component.");
			
			List<Map<String, Object>> nonComponentList = new ArrayList<>();
			List<Map<String, Object>> componentList = new ArrayList<>();
			List<String> ignoreContainThisPath = new ArrayList<>();
			logger.debug("1. split category component or value");
			for (Map<String, Object> binding : bindingList) {
				logger.debug("Binding Data {}", binding);
				if(binding.get("path")!=null){
					String targetJsonPath = binding.get("path").toString();
					//String specialType = binding.containsKey("special") ? binding.get("special").toString() : "";
					String type = binding.get("type").toString();
					
					if ("component".equalsIgnoreCase(type)) {
						ignoreContainThisPath.add(targetJsonPath);
						componentList.add(binding);
					}else{
						nonComponentList.add(binding);
					}
				}
			}
			logger.debug("2. binding property" );
			bindingHelper(ctx, nonComponentList);
			logger.debug("3. binding component");
			bindingHelper(ctx, componentList);
		}
		
		logger.debug("4. result template =" + ctx.jsonString());
		
//		dataBinding = ctx.json();
		
//		return dataBinding;
//		return input;
	}
	
	private void bindingHelper(DocumentContext ctx, List<Map<String, Object>> bindingList) throws Exception {
		for (Map<String, Object> binding : bindingList) {
			boolean isDefault = binding.get("default")!=null?(Boolean)binding.get("default"):true;
			if(isDefault){
				logger.debug("use default for this property");
				continue;
			}else{			
				
				String targetJsonPath = DataUtils.getStringValue(binding.get("path"));
				String value = DataUtils.getStringValue(binding.get("value"), null);
				String type = DataUtils.getStringValue(binding.get("type"));
				
				if(value==null){
					String group = DataUtils.getStringValue(binding.get("group"));
					String propertyName = DataUtils.getStringValue(binding.get("property_name"));
					
					logger.info("Invalid configuration. group={} propertyName={}", group, propertyName);
					throw new Exception("Invalid value configuration. Component \"" + group + "\", Property \"" + propertyName + "\"" );
				}

				logger.info("type : " + type + " ,perpare target path : " + targetJsonPath);
				
				Object target = null;
				try{
					target =ctx.read(targetJsonPath);
							
				}catch (Exception ex){
					logger.info("skip " + targetJsonPath);
					continue;
				}
	
				String specialType = binding.containsKey("special") ? binding.get("special").toString() : "";
	
				if ("changeKey".equalsIgnoreCase(specialType)) {
					logger.info("change key");
					String oldname = DataUtils.getStringValue(binding.get("oldvalue"));
					
					ctx.renameKey(targetJsonPath, oldname, value);
				} 
				else if ("component".equalsIgnoreCase(type)){
					logger.info("case component");
					
					
					List<Map<String, Object>> componentBindingList =(List<Map<String, Object>>) binding.get(DmaConstants.AppUI.Key.BINDING);
					
					//DocumentContext componentContext = JsonPath.parse((InputStream) (new FileInputStream(
					//		baseUrl + "/components/" + componentType + ".json")));
					
					// TODO load Component JSON from mongo by componentType
					
					DocumentContext componentContext = JsonPath.parse(getComponentJson(value));
					
					logger.debug("3.2 recursive binding component");
					bindingHelper(componentContext, componentBindingList);
					logger.debug("data ="+componentContext.jsonString());
					logger.debug("target data =" + targetJsonPath);
					ctx.set(targetJsonPath, componentContext.json());
				}
				else {
					logger.info("case other");
					
					if (target instanceof Map) {
						logger.debug("case object is map");
						ctx.set(targetJsonPath, value);
	
					} else {
						logger.debug("case object is List<Map>");
						ctx.set(targetJsonPath, value);
					}
				}
			}

		}
	}
	
	private String getComponentJson(String componentName) throws Exception {
		
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where("componentName").regex(Pattern.compile(DataUtils.getMongoMatchRegex(componentName), Pattern.CASE_INSENSITIVE)));
		
		try{
			DMAComponent document = getMongoDbAccessor().findOneByCriteria(findByName, DMAComponent.class);
			return document.getNativeJson().get("data");
		}catch(Exception e){
			throw e;
		}
	}

}
