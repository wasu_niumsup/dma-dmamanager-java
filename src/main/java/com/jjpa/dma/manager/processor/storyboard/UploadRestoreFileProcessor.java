package com.jjpa.dma.manager.processor.storyboard;

import java.io.File;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.dma.manager.model.reference.RequiredReference;
import com.jjpa.dma.manager.processor.restore.reference.GenerateActionChainReferenceRestore;
import com.jjpa.dma.manager.processor.restore.reference.GenerateActionChainTemplateReferenceRestore;
import com.jjpa.dma.manager.processor.restore.reference.GenerateImageReferenceRestore;
import com.jjpa.dma.manager.processor.restore.reference.GenerateScreenReferenceRestore;
import com.jjpa.dma.manager.processor.restore.reference.GenerateStoryBoardReferenceRestore;
import com.jjpa.dma.manager.processor.restore.reference.GenerateTemplateReferenceRestore;
import com.jjpa.dma.manager.processor.restore.validator.ActionChainRestoreValidator;
import com.jjpa.dma.manager.processor.restore.validator.ActionChainTemplateRestoreValidator;
import com.jjpa.dma.manager.processor.restore.validator.ImageRestoreValidator;
import com.jjpa.dma.manager.processor.restore.validator.ScreenRestoreValidator;
import com.jjpa.dma.manager.processor.restore.validator.StoryBoardRestoreValidator;
import com.jjpa.dma.manager.processor.restore.validator.TemplateRestoreValidator;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.api.ServiceGatewayApi;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;
import servicegateway.utils.Zip4j;

/* 
 * 17/05/2562 (170525621300) add by Akanit : for DMA.5 : validate restoreFile must be DMA.5 : finished.
 * */
public class UploadRestoreFileProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(UploadRestoreFileProcessor.class);
	
	private String fileRestorePath, fileRestoreType, folderNameDMA, zipPass;

	public Object process(HttpServletRequest request, CommonsMultipartFile file) throws Exception {
		logger.info("Process ===> UploadRestoreFileProcessor");
		
    	Map<String, Object> output = new LinkedHashMap<>();
    	
    	
    	/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
		fileRestorePath = ManagerMap.getString(propMap, PropConstants.FileLocal.Restore.Key.FILE_PATH, "");
		fileRestoreType = ManagerMap.getString(propMap, PropConstants.FileLocal.FILE_TYPE_DMA, "");
		folderNameDMA = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.DMA, "");
//		folderNameSG = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.SG, "");
		zipPass = ManagerMap.getString(propMap, PropConstants.FileLocal.ZIP_PASSWORD, "");
		
    	/* validate file zip type */
    	validateFileType(file.getOriginalFilename(), fileRestoreType);
    	
    	
    	
    	/* create path of folder is unique name. */
		String uniqueFolder = String.valueOf(new Date().getTime());		
		String extractZipFilePath = fileRestorePath + uniqueFolder;
		
		
		
    	/* process uploadRestoreFile and save file to server. */
    	try{
	    	/* 
	    	 * step 1 : extract restoreFile.zip
	    	 * */
	    	logger.info("#step 1 : extract restoreFile.zip");
	    	
	    	/* 09/04/2561 add by Akanit : unziping with password */
	    	new Zip4j().unzip(file, extractZipFilePath, zipPass);
	    	
	    	/* unzip again */
			File folderExtractZip = new File(extractZipFilePath);
			if(folderExtractZip.exists()){
				for(File fileEntry : folderExtractZip.listFiles()){
					if(!fileEntry.isDirectory()){
				    	/* get index = 0 : because entryZip have one file */
				    	ManagerStore.extractZipFile(fileEntry.getAbsolutePath(), extractZipFilePath);
				    	
				    	ManagerStore.deleteFile(fileEntry.getAbsolutePath());
				    	break;
					}
				}
			}
	    	logger.info("#step 1 : success.");
	    	
    	}catch(Exception e){
    		String errorDesc = "File is unable to be restored. Please try again.";
    		logger.info(errorDesc, e);
    		
    		ManagerStore.deleteFile(extractZipFilePath);
			throw new Exception(errorDesc);
    	}    
	    	
	    
    	
    	try{
	    	/* 
	    	 * 17/05/2562 (170525621300) add by Akanit : 
	    	 * step 2 : for DMA.5 : validate restoreFile must be DMA.5
	    	 * */
	    	logger.info("#step 2 : for DMA.5 : validate restoreFile must be DMA.5");
	    	validateFileMustBeDMA5(extractZipFilePath);
	    	
	    	logger.info("#step 2 : success.");
	    	
    	}catch(Exception e){
    		String errorDesc = "Your DMA version 4's exported file is not support with this DMA version.";
    		logger.info(errorDesc, e);
    		
    		ManagerStore.deleteFile(extractZipFilePath);
			throw new Exception(errorDesc);
    	} 	
	    	
    	
	    
    	try{
	    	/* 
	    	 * step 3 : read restoreFile get name, version and convert to RequiredReference.
	    	 * */
	    	logger.info("#step 3 : read restoreFile get name, version and convert to RequiredReference.");
	    	RequiredReference requiredRef = readReferenceFromRestoreFile(extractZipFilePath);
	    	
	    	logger.info("#step 3 : success.");
	    	
	    	
	    	
	    	/* 
	    	 * step 4 : add status, detail, summaryValidation, validation to RequiredReference
	    	 * */
	    	logger.info("#step 4 : add status, detail, summaryValidation, validation to RequiredReference");
	    	output = validateRequiredReference(requiredRef);
	    	
	    	logger.info("#step 4 : success.");
	    	
	    	
	    	
	    	/* 
	    	 * step 5 : binding Validation from serviceGateway
	    	 * */
	    	logger.info("#step 5 : binding Validation from serviceGateway");
	    	bindingServiceGatewayValidation(output, extractZipFilePath);
	    	
	    	logger.info("#step 5 : success.");
	    	
	    	
	    	
    	}catch(Exception e){
    		String errorDesc = "File is unable to be restored. Please try again.";
    		logger.info(errorDesc, e);
    		
    		ManagerStore.deleteFile(extractZipFilePath);
			throw new Exception(errorDesc);
    	}
    	
    	
    	output.put(BackupConstants.FILE_UPLOAD_NAME, uniqueFolder);
    	
    	logger.info("output="+ output);
		logger.info("End Procces ===> UploadRestoreFileProcessor");
		return output;
	}
	
	/* 17/05/2562 (170525621300) add by Akanit : for DMA.5 : validate restoreFile must be DMA.5 : finished. */
	private void validateFileMustBeDMA5(String filePath) throws Exception {
		
		File file = new File(filePath + File.separator + "dma5.zip");
		
		if(!file.exists()){
			String errorDesc = "file dma5.zip not found.";
    		logger.info(errorDesc);
    		throw new Exception(errorDesc);
		}
	}
	
	private RequiredReference readReferenceFromRestoreFile(String filePath) throws Exception{
		
		/* create path of dmaFolder, sgFolder */
		String dmaPath = filePath + File.separator + folderNameDMA + File.separator;

		String storyBoardFolder 		 = dmaPath + BackupConstants.FolderName.DMA.STORY_BOARD;
		String screenFolder 		     = dmaPath + BackupConstants.FolderName.DMA.SCREEN;
		String templateFolder 		     = dmaPath + BackupConstants.FolderName.DMA.TEMPLATE;
		String actionChainFolder 		 = dmaPath + BackupConstants.FolderName.DMA.ACTION_CHAIN;
		String actionChainTemplateFolder = dmaPath + BackupConstants.FolderName.DMA.ACTION_CHAIN_TEMPLATE;
		String imsTemplateFolder		 = dmaPath + BackupConstants.FolderName.DMA.IMS;

		/* generate reference required */
		RequiredReference reqRef = new RequiredReference();
		
		new GenerateImageReferenceRestore().process(reqRef, imsTemplateFolder);
		new GenerateStoryBoardReferenceRestore().process(reqRef, storyBoardFolder);
		new GenerateScreenReferenceRestore().process(reqRef, screenFolder);
		new GenerateTemplateReferenceRestore().process(reqRef, templateFolder);
		new GenerateActionChainReferenceRestore().process(reqRef, actionChainFolder);
		new GenerateActionChainTemplateReferenceRestore().process(reqRef, actionChainTemplateFolder);
		
		return reqRef;
	}
	
	
	private Map<String, Object> validateRequiredReference(RequiredReference requiredRef) throws Exception {
		
		boolean validation = true;

		Map<String, Object> storyBoardValidation = new StoryBoardRestoreValidator().process(requiredRef.getDmaStroyBoardRequired());
		if(ManagerMap.getBoolean(storyBoardValidation, "summaryValidation") == false){
			validation = false;
		}
		Map<String, Object> screenValidation = new ScreenRestoreValidator().process(requiredRef.getDmaScreenRequired());
		if(ManagerMap.getBoolean(screenValidation, "summaryValidation") == false){
			validation = false;
		}
		Map<String, Object> imageValidation = new ImageRestoreValidator().process(requiredRef.getDmaImageRequired());
		if(ManagerMap.getBoolean(imageValidation, "summaryValidation") == false){
			validation = false;
		}
		Map<String, Object> templateValidation = new TemplateRestoreValidator().process(requiredRef.getDmaTemplateRequired());
		if(ManagerMap.getBoolean(templateValidation, "summaryValidation") == false){
			validation = false;
		}
		Map<String, Object> actionChainTemplateValidation = new ActionChainTemplateRestoreValidator().process(requiredRef.getDmaActionChainTemplateRequired());
		if(ManagerMap.getBoolean(actionChainTemplateValidation, "summaryValidation") == false){
			validation = false;
		}
		Map<String, Object> actionChainValidation = new ActionChainRestoreValidator().process(requiredRef.getDmaActionChainRequired());
		if(ManagerMap.getBoolean(actionChainValidation, "summaryValidation") == false){
			validation = false;
		}
		
		Map<String, Object> validate = new LinkedHashMap<>();
		validate.put(DmaConstants.StoryBoard.Key.STORY_BOARD_VALIDATION, storyBoardValidation);
    	validate.put(DmaConstants.StoryBoard.Key.SCREEN_VALIDATION, screenValidation);
		validate.put(DmaConstants.StoryBoard.Key.IMAGE_VALIDATION, imageValidation);
    	validate.put(DmaConstants.StoryBoard.Key.TEMPLATE_VALIDATION, templateValidation);
    	validate.put(DmaConstants.StoryBoard.Key.ACTION_CHAIN_TEMPLATE_VALIDATION, actionChainTemplateValidation);
    	validate.put(DmaConstants.StoryBoard.Key.ACTION_CHAIN_VALIDATION, actionChainValidation);

    	validate.put(DmaConstants.StoryBoard.Key.VALIDATION, validation);
    	
    	return validate;
	}
	
	private void bindingServiceGatewayValidation(Map<String, Object> validate, String filePath) throws Exception {
		
		
		/* call serviceGateway */
		Map<String, Object> sgRestoreValidation = new ServiceGatewayApi().restoreValidation(filePath);
		
		
		List<Map<String, Object>> dataList = ManagerMap.get(sgRestoreValidation, "dataList", List.class);
		
		boolean validationStatus = true;
		for(Map<String, Object> dataMap : dataList){
			validationStatus = convertValidationToDma(dataMap, validationStatus);
		}
		
			
		/* mapping data to UI */
    	Map<String, Object> dmaSgValidation = new LinkedHashMap<>();
    	dmaSgValidation.put("summaryValidation", validationStatus);
    	dmaSgValidation.put("data", dataList);
    	
    	
    	/* binding */
		validate.put(DmaConstants.StoryBoard.Key.SG_SERVICE_VALIDATION, dmaSgValidation);
		
		if(validationStatus == false){
			validate.put(DmaConstants.StoryBoard.Key.VALIDATION, false);
		}
		
	}

	private boolean convertValidationToDma(Map<String, Object> dataMap, boolean validationStatus) throws Exception {
		
		String name = ManagerMap.getString(dataMap, "name");
		String status = ManagerMap.getString(dataMap, "status");
		
		if( "ADD".equals(status) || "CREATE".equals(status) ){
			logger.info("#not found old ServiceGateway="+ name);
			logger.info("#set {}={}", BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.NOT_FOUND);
			logger.info("#set {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
			
			validationStatus = false;
			dataMap.put(BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.NOT_FOUND);
			dataMap.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
			
		}else {
			logger.info("#found old ServiceGateway="+ name);
			logger.info("#set {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
			
			dataMap.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
		}
		
		return validationStatus;
	}
	
	
	
	private void validateFileType(String fileName, String fileType) throws Exception{
		if(fileName==null)return;
		if(fileType==null)return;
		
		String[] fileName_sp = fileName.split("\\.");
		String type = "."+fileName_sp[fileName_sp.length-1];
		if(!type.equals(fileType)){
			String errorDesc = "Invalid file type. File type must be " + fileType;
			logger.info(errorDesc);
			throw new Exception(errorDesc);
		}	
	}

}
