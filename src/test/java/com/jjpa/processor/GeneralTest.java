package com.jjpa.processor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.util.DataUtils;
import com.jjpa.dma.manager.processor.restoreimage.UploadImageAllProcessor;

import blueprint.util.StringUtil;

/**
 * GeneralTest
 */
@SuppressWarnings("unused")
public class GeneralTest {
    public static void main(String[] args) throws Exception {
        testUpload();

    }

    private static void testUpload(){
        UploadImageAllProcessor up = new UploadImageAllProcessor();
      
        System.out.println("xxxxxx");
    }

    private static String getDomainName(String url, boolean includeProtocal) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            String result = domain.startsWith("www.") ? domain.substring(4) : domain;
            String protocal = url.substring(0, url.indexOf("//") + 2);

            return includeProtocal ? protocal + result : result;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }
}