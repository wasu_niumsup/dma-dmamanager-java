package com.jjpa.dma.manager.processor.info;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import org.springframework.data.mongodb.core.query.Query;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.CriteriaUtil;

/**
 * DashBoardProcessor
 */
public class DashBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

    private final Logger logger = LoggerFactory.getLogger(DashBoardProcessor.class);

    protected AppUiAccessor appUiAccessor;
    protected StoryBoardAccessor storyBoardAccessor;

    public DashBoardProcessor(){
        this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
    }
    
    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {

        Map<String,Object> body = input.getData();
        logger.info("body = " + body);
        String username = DataUtils.getStringValue(body, "username",null);

        int countScreen = whereCountScreen(username);
        int countStoryboard  = whereCountStoryBoard(username);
       
        
        Map<String,Object> output = new LinkedHashMap<>();
        output.put("screenCount", countScreen);
        output.put("storyBoardCount", countStoryboard);
        logger.info("outputInfo : " + output);
        return output;
    }

    public int whereCountScreen(String username) throws Exception {
        int result = 0;
        Query query = new Query();
        List<WhereOption> whereOptList = new ArrayList<>();
        WhereOption whereOpt;
        whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.AppUI.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
            whereOptList.add(whereOpt);
            
        CriteriaUtil.addCriteriaWhere(query, whereOptList);
        result = appUiAccessor.count(query);
        return result;
    }

    public int whereCountStoryBoard(String username) throws Exception {
        int result = 0;
        Query query = new Query();
        List<WhereOption> whereOptList = new ArrayList<>();
        WhereOption whereOpt;
        whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.AppUI.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
            whereOptList.add(whereOpt);
            
        CriteriaUtil.addCriteriaWhere(query, whereOptList);
        result = storyBoardAccessor.count(query);
        return result;
    }

    

    

    
}