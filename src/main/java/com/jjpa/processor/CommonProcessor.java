package com.jjpa.processor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.PublishEntityConfigAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;;

public abstract class CommonProcessor<T> {
	
	public MongoDbAccessor getMongoDbAccessor() {
		return SpringApplicationContext.getBean("MongoDbAccessor");
	}
	
	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}
	
	public StoryBoardAccessor getStoryBoardAccessor() {
		return SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	public TempEntityConfigAccessor getTempEntityConfigAccessor() {
		return SpringApplicationContext.getBean("TempEntityConfigAccessor");
	}
	
	public PublishEntityConfigAccessor getPublishEntityConfigAccessor() {
		return SpringApplicationContext.getBean("PublishEntityConfigAccessor");
	}
	
	/* 07/01/2562 comment by Akanit */
	public Object process(T input) throws Exception {
		return process(null, input);
	}
	/* 07/01/2562 add by Akanit */
	public Object process(HttpServletRequest request, T input) throws Exception {
		return process(input);
	}


	public DocumentContext callHook(String name,Map<String,Object> data) throws Exception {
		String baseUrl = DataUtils.getHookURL();
		DocumentContext reqContext = JsonPath.parse(data);

		try {
			if("SaveScreen".equalsIgnoreCase(name)){

				return getJsonAccessor().send(baseUrl + "/SaveScreen", reqContext);
				
			}else if("SaveStoryBoard".equalsIgnoreCase(name)){

				return getJsonAccessor().send(baseUrl+"/SaveStoryBoard", reqContext);
				
			}else if("PublishApp".equalsIgnoreCase(name)){
				
				return getJsonAccessor().send(baseUrl+"/PublishApp", reqContext);
				
			}else if("UpgradeAllScreen".equalsIgnoreCase(name)){
				
				return getJsonAccessor().send(baseUrl+"/UpgradeAllScreen", reqContext);
				
			}else if("UpgradeAllStoryboard".equalsIgnoreCase(name)){
				
				return getJsonAccessor().send(baseUrl+"/UpgradeAllStoryboard", reqContext);
				
			}else {
				return getJsonAccessor().send(baseUrl+"/"+name, reqContext);
			}
			
		} catch (Exception e) {
			throw e;
		}

	}

	public DocumentContext callHookSyncGetPublishQueueList(Map<String, Object> data) throws Exception {
		/* String baseUrl = DataUtils.getHookURL();
		DocumentContext reqContext = JsonPath.parse(data);
		getJsonAccessor().send(baseUrl + "/FindTemplate", reqContext); */
		return  callHook("getPublishQueueList",data);
	}

	public DocumentContext callHookSyncFindtmplate(Map<String, Object> data) throws Exception {
		/* String baseUrl = DataUtils.getHookURL();
		DocumentContext reqContext = JsonPath.parse(data);
		getJsonAccessor().send(baseUrl + "/FindTemplate", reqContext); */
		return  callHook("FindTemplate",data);
	}

	public DocumentContext callHookSyncDeleteStoryBoardAndScreen(Map<String, Object> data) throws Exception {
		/* String baseUrl = DataUtils.getHookURL();
		DocumentContext reqContext = JsonPath.parse(data);
		getJsonAccessor().send(baseUrl + "/DeleteStoryBoardAndScreen", reqContext); */
		return  callHook("DeleteStoryBoardAndScreen", data);
	}
	
	/* download service */
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
	}
	
	/* upload service */
	public Object process(HttpServletRequest request, CommonsMultipartFile file) throws Exception{
		return null;
	}
	
}
