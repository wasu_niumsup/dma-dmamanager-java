package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.index.Indexed;

import com.jjpa.db.mongodb.document.BaseDocument;

public class EntityConfigDocument extends BaseDocument{

	private static final long serialVersionUID = 1L;
	
	@Indexed(unique=true)
	private String entityName;
	private String home;
	private String firstPage;
	private String notificationPage;
	private Object screenList;
	private String storyBoardName;


	
	public String getStoryBoardName() {
		return storyBoardName;
	}
	public void setStoryBoardName(String storyBoardName) {
		this.storyBoardName = storyBoardName;
	}
	
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getFirstPage() {
		return firstPage;
	}
	public void setFirstPage(String firstPage) {
		this.firstPage = firstPage;
	}
	public String getNotificationPage() {
		return notificationPage;
	}
	public void setNotificationPage(String notificationPage) {
		this.notificationPage = notificationPage;
	}
	public Object getScreenList() {
		return screenList;
	}
	public void setScreenList(Object screenList) {
		this.screenList = screenList;
	}
	
}
