package com.jjpa.dma.manager.binding.action.wrapper;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;

public class LocationBlank implements ActionWrapper{

	private final Logger logger = LoggerFactory.getLogger(LocationBlank.class);
	
	protected ActionChainAccessor actionChainAccessor;
	
	private String check_location = "check_location";
	private String set_location = "set_location";
	
	public LocationBlank() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public DocumentContext wrapperActionChain(List<String> dmaParamList, DocumentContext masterAction) throws Exception {
		
		DocumentContext wrapLocation = new LocationCheck().wrapperActionChain(dmaParamList, masterAction);
		
		DocumentContext resultContext = generateCheckLocationActionChain(dmaParamList, wrapLocation, masterAction);
		
		return resultContext;
	}

	private DocumentContext generateCheckLocationActionChain(List<String> dmaParamList, DocumentContext innerContext, DocumentContext masterAction) throws Exception {
		
		/* find action chain type from db */
		DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(check_location);
		if(actionChainDocument==null){
			
			logger.info("Need patching. {}", check_location);
			throw new Exception("DMA need patching, Code \"" + check_location + "\"");
		}
		
		DocumentContext resultContext = JsonPath.parse(actionChainDocument.getNativeJson().get(DmaConstants.ActionChain.Key.DATA));
		
		
		List<Map<String, Object>> bindingList = actionChainDocument.getBinding();
		
		
		/* 
		 * binding successPath
		 *  */	
		/* select row where name=success */
		Map<String, Object> bindingSuccessMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.SUCCESS);
		
		String successPath = DataUtils.getStringValue(bindingSuccessMap.get(DmaConstants.ActionChain.Key.PATH));
		
		logger.info("#binding checkLocationActionChain success. (set)");
		
		logger.info("path={}", successPath);
		logger.info("innerContext={}", innerContext.jsonString());
		
		resultContext.set(successPath, innerContext.json());
		
		
		/* 
		 * binding error
		 *  */
		DocumentContext errorContext = generateSetLocationActionChain(masterAction, dmaParamList);
		
		/* select row where name=error */
		Map<String, Object> bindingErrorMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.ERROR);
		
		String errorPath = DataUtils.getStringValue(bindingErrorMap.get(DmaConstants.ActionChain.Key.PATH));
		
		logger.info("#binding checkLocationActionChain error. (set)");
		
		logger.info("path={}", errorPath);
		logger.info("innerContext={}", errorContext.jsonString());
		
		resultContext.set(errorPath, errorContext.json());
		
		
		return resultContext;
	}
	
	private DocumentContext generateSetLocationActionChain(DocumentContext innerContext, List<String> dmaParamList) throws Exception {
		
		/* find action chain type from db */
		DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(set_location);
		if(actionChainDocument==null){
			
			logger.info("Need patching. {}", set_location);
			throw new Exception("DMA need patching, Code \"" + set_location + "\"");
		}
		
		DocumentContext resultContext = JsonPath.parse(actionChainDocument.getNativeJson().get(DmaConstants.ActionChain.Key.DATA));
		
		
		List<Map<String, Object>> bindingList = actionChainDocument.getBinding();
		
		
		/* 
		 * binding successPath
		 *  */
		
		/* select row where name=success */
		Map<String, Object> bindingSuccessMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.SUCCESS);
		
		String successPath = DataUtils.getStringValue(bindingSuccessMap.get(DmaConstants.ActionChain.Key.PATH));
		
		logger.info("#binding setLocationActionChain success. (set)");
		
		logger.info("path={}", successPath);
		logger.info("innerContext={}", innerContext.jsonString());
		
		resultContext.set(successPath, innerContext.json());
		

		/* 
		 * binding optionsPath
		 *  */
		
		/* select row where name=options */
		Map<String, Object> bindingOptionsMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.OPTIONS);
		
		String optionsPath = DataUtils.getStringValue(bindingOptionsMap.get(DmaConstants.ActionChain.Key.PATH));
		String value = "";
		
		logger.info("#binding setLocationActionChain options. (for(put))");

		for(String dmaParam : dmaParamList){
			logger.info("path={}", optionsPath);
			logger.info("dmaParam={}", dmaParam);
			logger.info("value={}", value);
			
			resultContext.put(optionsPath, dmaParam, value);
		}
		
		return resultContext;
	}
}
