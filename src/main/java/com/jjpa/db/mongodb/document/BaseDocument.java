package com.jjpa.db.mongodb.document;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class BaseDocument implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date createDate;

	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdateDate;

	private String createBy;
	private String lastUpdateBy;

	private String dmaManagerVersion;

	public String getDmaManagerVersion() {
		return dmaManagerVersion;
	}
	public void setDmaManagerVersion(String dmaManagerVersion) {
		this.dmaManagerVersion = dmaManagerVersion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

}
