package com.jjpa.common;

public class MongoConstants {

	public class Path{
		
		public static final String ID 					= "id";
		public static final String REMARK 				= "remark";
		public static final String LAST_UPDATE 			= "lastUpdate";
		public static final String LAST_PUBLISH 		= "lastPublish";
		public static final String STATUS 				= "status";
	}
	
	public class Simple{

		public static final String NAME		= "name";
		public static final String DATA		= "data";
	}

	public class Reference{
		
		public class Path{
			
			public static final String INUSED_BY_DMA_STROYBOARD_NAME 			= "refference.inUsed.inUsedByDmaStroyBoard.name";
			public static final String INUSED_BY_DMA_SCREEN_NAME 				= "refference.inUsed.inUsedByDmaScreen.name";
			
			public static final String DMA_SCREEN_REQUIRED_NAME 				= "refference.required.dmaScreenRequired.name";
			public static final String DMA_TEMPLATE_REQUIRED_NAME 				= "refference.required.dmaTemplateRequired.name";
			public static final String DMA_ACTIONCHAIN_REQUIRED_NAME 			= "refference.required.dmaActionChainRequired.name";
			public static final String DMA_ACTIONCHAIN_TEMPLATE_REQUIRED_NAME 	= "refference.required.dmaActionChainTemplateRequired.name";
			public static final String SG_SERVICE_REQUIRED_NAME 				= "refference.required.sgServiceRequired.serviceName";
		}
	}
	
	public class AppUI{
		
		public class Path{
			public static final String CREATE_BY 			= "createBy";
			public static final String UI_NAME 			= "uiName";
			public static final String UI_JSON 			= "uiJson";
			public static final String TEMPLATE_TYPE 	= "templateType";
			public static final String TEMPLATE_NAME 	= "uiConfigData.template";
		}
		
		public class UiName{
			public static final String HOME 			= "home";
			public static final String FIRST 			= "first";
			public static final String FIRST_PAGE 		= "firstpage";
		}
	}

	public class StoryBoard{

		public class Path{
			public static final String STORY_NAME		= "storyName";
			public static final String FIRST_PAGE		= "firstPage";
			public static final String HOME_PAGE		= "homePage";
		}
	}
	
	public class Entity{

		public class Path{
			public static final String CREATE_BY 	= "createBy";
			public static final String LAST_UPDATE_BY 	= "lastUpdateBy";
			public static final String ENTITY_NAME 		= "entityName";
			public static final String STORY_NAME 		= "storyName";
			public static final String ROLE 			= "role";
			public static final String THEME 			= "theme";
			public static final String VERSION 			= "version";
			public static final String PUBLISH_STATUS 	= "publishStatus";
		}
	}
	
	public class StoryboardExtraTargetType{
		
		public class Path{
			public static final String UI_NAME			= "uiName";
		}
	}
}

