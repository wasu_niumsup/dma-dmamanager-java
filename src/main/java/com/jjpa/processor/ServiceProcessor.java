package com.jjpa.processor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public interface ServiceProcessor<T> {

	/* 07/01/2562 comment by Akanit */
//	public Object process(T input) throws Exception;
	
	/* 07/01/2562 add by Akanit */
	public Object process(HttpServletRequest request, T input) throws Exception;

	/* download service */
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception;
	
	/* upload service */
	public Object process(HttpServletRequest request, CommonsMultipartFile file) throws Exception;
}
