package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.dma.manager.processor.api.PatchVersion;
import com.jjpa.spring.SpringApplicationContext;

public class TemplateRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(TemplateRestoreValidator.class);

	protected TemplateAccessor templateAccessor;

	public TemplateRestoreValidator() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");

	}

	public Map<String, Object> process(List<Map<String, Object>> templateRequiredList) throws Exception {
		logger.info("Process ===> TemplateRestoreValidator");

		boolean validationStatus = true;
		@SuppressWarnings("unused")
		String version_restore, version_old, name;
		Map<String, Object> templatelist = this.templateAccessor.listTemplateName();

		List<Map<String, Object>> databaseLastestTemplate = (List<Map<String, Object>>) templatelist
				.get("templateList");

		logger.info("[TemplateRestoreValidator] testList");

		// for (int i = 0; i < testList.size(); i++) {
		// logger.info("[TemplateRestoreValidator] testList ==== > " +
		// testList.get(i).toString());
		// if(templateRequiredList.get(i).get("name").toString().equals(testList.get(i).get("name").toString())){
		// templateRequiredList.get(i).put("lastestVersion",
		// testList.get(i).get("version").toString());
		// }
		// }
		PatchVersion patchVersion = new PatchVersion();
		for (Map<String, Object> templateRequired : templateRequiredList) {

			templateRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);

			for (Map<String, Object> dbLastest : databaseLastestTemplate) {
				// logger.info();
				String latestName = DataUtils.getStringValue(dbLastest, "name", "");
				String importName = DataUtils.getStringValue(templateRequired, "name", "");

				if (importName.equals("")) {
					throw new ValidationException("Import template name is null.");
				}
				if (latestName.equals(importName)) {
					templateRequired.put("lastestTemplate", dbLastest.get("version").toString());

					String importVersion = DataUtils.getStringValue(templateRequired, "version");
					String oldVersion = DataUtils.getStringValue(dbLastest, "version");
					int verifyVersion = patchVersion.compareVersion(importVersion, oldVersion);
					logger.info("verifyVersion == > " + Integer.toString(verifyVersion));

					if (verifyVersion == -1) {
						logger.info("importversion < oldVersion  " + importVersion + " to " + oldVersion);
						templateRequired.put("status", false);
					}

				}

			}

			// version_restore = ManagerMap.getString(templateRequired,
			// ReferenceConstants.DMA.VERSION);
			// name = ManagerMap.getString(templateRequired, ReferenceConstants.DMA.NAME);
			//
			// DMATemplate doc = templateAccessor.findTemplateByName(name);
			//
			//
			// /* validate add status */
			// if(doc==null){
			// logger.info("#not found old Template="+ name);
			// logger.info("#update action=ADD");
			// logger.info("#update status=true");
			//
			// templateRequired.put(BackupConstants.Validator.Key.STATUS,
			// BackupConstants.Validator.Status.TRUE);
			// continue;
			//
			// }else {
			// logger.info("#found old Template="+ name);
			// }

			// try {
			// version_old = doc.getVersionTemplate();
			// } catch (Exception e) {
			// logger.info("#Template="+ name + "not have reference.");
			// logger.info("#update action=UPDATE");
			// logger.info("#update status=true");
			//
			// templateRequired.put("status", true);
			// continue;
			// }
			//
			//
			//
			// if(version_restore == version_old){
			// logger.info("#Template="+ name + " can restore.");
			// logger.info("#update action=UPDATE");
			// logger.info("#update status=true");
			//
			// templateRequired.put("status", true);
			//
			// }else{
			// logger.info("#Template="+ name + " can not restore.");
			// logger.info("#update action=UPDATE");
			// logger.info("#update status=true");
			//
			//// validationStatus = false;
			// templateRequired.put("status", true);
			// }

		}

		/* mapping data to UI */
		Map<String, Object> output = new LinkedHashMap<>();
		output.put("summaryValidation", validationStatus);
		output.put("data", templateRequiredList);

		return output;
	}

	public Boolean validationVersion(String oldVersion, String newVersion) {

		return true;
	}
}
