package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ComponentAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAComponent;

public class ComponentAccessorImpl implements ComponentAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected ComponentAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Object saveComponent(DMAComponent component, String user) throws Exception {

		try{
			DMAComponent oldComponent = findComponent(component.getComponentName());
			if(oldComponent != null){
				oldComponent.setComponentName(component.getComponentName());
				oldComponent.setBinding(component.getBinding());
				oldComponent.setNativeJson(component.getNativeJson());
				component = oldComponent;
			}
			return mongoDbAccessor.save(component, user);
		}catch(Exception e){
			throw e;
		}
		
	}

	public DMAComponent findComponent(String componentName) throws Exception {
		
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where("componentName").regex(Pattern.compile(DataUtils.getMongoMatchRegex(componentName), Pattern.CASE_INSENSITIVE)));
		
		try{
			return mongoDbAccessor.findOneByCriteria(findByName, DMAComponent.class);
		}catch(Exception e){
			throw e;
		}
	}
	
	public Map<String, Object> listComponentName() throws Exception{
		List<DMAComponent> componentObjList = listComponents();
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String,Object>> componentList = new ArrayList<Map<String,Object>>();
		for(DMAComponent document : componentObjList){
			Map<String,Object> appUi = new HashMap<String,Object>();
			appUi.put("name", document.getComponentName());
			componentList.add(appUi);
		}
		result.put("componentList", componentList);
		return result;
	}

	public List<DMAComponent> listComponents() throws Exception {
		
		try{
			return mongoDbAccessor.findAll(DMAComponent.class);
		}catch(Exception e){
			throw e;
		}
	}

	public void deleteComponent(DMAComponent component, String user) throws Exception {
		
		try{
			mongoDbAccessor.delete(component, user);
		}catch(Exception e){
			throw e;
		}
	}

}
