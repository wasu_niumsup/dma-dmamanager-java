package com.jjpa.common.data.service;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ServiceResponse<T> implements Serializable{

	private static final long serialVersionUID = 1L;
	private boolean success = false;
	private long genaratedAt;
	private String status = "failure";
	private String responseDesc = "Service error. Please contact admin.";
	
	private T responseData;

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
		this.status = "success";
		this.responseDesc = null;
	}
	public long getGenaratedAt() {
		return genaratedAt;
	}
	public void setGenaratedAt(long genaratedAt) {
		this.genaratedAt = genaratedAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
	public T getResponseData() {
		return responseData;
	}
	public void setResponseData(T responseData) {
		this.responseData = responseData;
	}
	
}
