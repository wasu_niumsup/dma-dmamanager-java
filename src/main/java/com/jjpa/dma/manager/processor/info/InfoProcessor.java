package com.jjpa.dma.manager.processor.info;

import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

public class InfoProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
    private final Logger logger = LoggerFactory.getLogger(InfoProcessor.class);

    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        logger.info("Process ===> InfoProcessor");
        logger.info("Process ===> InfoProcessor : {}", input.toJsonString());

        InputStream is = getClass().getResourceAsStream(PropConstants.Service.NAME);
        DocumentContext ctx = JsonPath.parse(is);
        Map<String, Object> outputInfo = ctx.read(PropConstants.Service.Info.Engine.PATH);

        logger.info("outputInfo : " + outputInfo);
        return outputInfo;
    }
}