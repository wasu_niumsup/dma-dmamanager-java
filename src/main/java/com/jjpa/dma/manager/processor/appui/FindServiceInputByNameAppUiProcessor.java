package com.jjpa.dma.manager.processor.appui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.SGConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import blueprint.util.StringUtil;
import servicegateway.api.ServiceGatewayApi;

@SuppressWarnings("unchecked")
public class FindServiceInputByNameAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(FindServiceInputByNameAppUiProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindServiceInputByNameAppUiProcessor");
		logger.debug("Process ===> FindServiceInputByNameAppUiProcessor : {}", input.toJsonString());
		
		return findServiceInputByName(input);
		
	}
	
	private Map<String, Object> findServiceInputByName(ServiceData input) throws Exception {
		
		String apiName = (String)input.getValue("apiName");
		Boolean haveChild = DataUtils.getBooleanValue(input.getValue("haveChild"));
		logger.info("apiName="+ apiName);
		logger.info("haveChild="+ haveChild);
		
		if(StringUtil.isBlank(apiName)){
			
			/* return inputList=[] */
			Map<String, Object> inputMap = new HashMap<String, Object>();
			inputMap.put(SGConstants.Key.INPUT_LIST, new ArrayList<>());
			return inputMap;
			
		}else{
			
			/* call serviceGateway 'findServiceInputByName' */
//			Map<String, Object> resultService = sgFindServiceInputByName(apiName, haveChild);
			Map<String, Object> resultService = new ServiceGatewayApi().findServiceInputByName(apiName, haveChild);
			
			/* 09/01/2561 add by Akanit : */
			/* mapping data from serviceGateway to dmaUI */
			Map<String, Object> output = mappingDataToUi(resultService);
			
			return output;
		}
	}
	
	
	/* 
	 * mapping to UI 
	 * 09/01/2561 add by Akanit : 
	 * */
	private Map<String, Object> mappingDataToUi(Map<String, Object> data) throws Exception {
		
		Map<String, Object> output = new HashMap<String, Object>(); 
		
		output.put(DmaConstants.AppUI.Key.INPUT_LIST, generateInputList(data));
		output.put(DmaConstants.AppUI.Key.OUTPUT_LIST, generateOutputList(data));
		
		return output;
	}
	
	/* 09/01/2561 add by Akanit : */
	private List<Map<String, Object>> generateInputList(Map<String, Object> data) throws Exception {
		
		List<Map<String, Object>> resultList = new ArrayList<>(); 
		
		/* get inputList */
		List<Map<String, Object>> inputList = (List<Map<String, Object>>) data.get(SGConstants.Key.INPUT_LIST);
		if(inputList.size()>0){
			
			/*
			 *  mapping inputList data 
			 *  */
			Map<String, Object> map;
			for(Map<String, Object> inputRow : inputList){
				
				map = new HashMap<String, Object>(inputRow);
				map.put(DmaConstants.StoryBoard.Key.TYPE, inputRow.get(SGConstants.Key.FIELD_TYPE));
				map.remove(SGConstants.Key.FIELD_TYPE);
								
				
				/* 30/01/2561 add by Akanit : put local path -> use to binding local */
				map.put(DmaConstants.StoryBoard.Key.GlobalInputList.LOCAL_PATH, JasonPathConstants.UiJson.Root.SET_LOCAL);
				
				resultList.add(map);
			}		
		}
	
		return resultList;
	}
	
	/* 09/01/2561 add by Akanit : */
	private List<Map<String, Object>> generateOutputList(Map<String, Object> data) throws Exception {
		
		List<Map<String, Object>> resultList = new ArrayList<>(); 
		
		/* get outputList */
		List<Map<String, Object>> outputList = (List<Map<String, Object>>) data.get(SGConstants.Key.OUTPUT_LIST);
		if(outputList.size()>0){
		
			/* 
			 * mapping outputList data 
			 * */
			boolean promote;
			Map<String, Object> map;
			for(Map<String, Object> outputRow : outputList){
				
				/* filter field promote=true then putAll row */
				promote = DataUtils.getBooleanValue(outputRow.get(SGConstants.Key.PROMOTE));
				if(promote){
					map = new HashMap<String, Object>(outputRow);
					/**
					 * Tanawat W.
					 * 2018-06-15
					 * Old system FieldName is parrentName
					 * FieldName duplicate.Just use childName.
					 * 
					 */
					map.put(SGConstants.Key.OutputFieldList.FIELD_NAME,    map.get(SGConstants.Key.OutputFieldList.NAME) );
					resultList.add(map);
				}
			}
		}
		
		return resultList;
	}
}
