package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Map;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dma_configuration")
public class DMAConfigurationDocument extends BaseDMADocument{

	private static final long serialVersionUID = 1L;

	@Indexed(unique=true)
	private String configName;
    private String className;
	private Map<String,Object> data;
	private String type;


	public String getType(){
		return type;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
    }
    
    public Map<String,Object> getData() {
		return data;
	}

	public void setData(Map<String,Object> data) {
		this.data = data;
	}
	
}
