package com.jjpa.common;

public class PropConstants {
	
	public class Service{
		
		public static final String NAME 		= "/services.json";
		
		public class Info{
			
			public static final String PATH 	= "$.info";
			
			public class Engine{
				
				public static final String PATH		= "$.info.dmamanager";
				
				public class Key{
					public static final String NAME 	= "name";
					public static final String VERSION 	= "version";
					public static final String LOG 		= "log";
					public static final String DATE 	= "date";
				}
			}
			
			public class UI{
				
				public static final String PATH		= "$.info.ui";
				
				public class Key{
					public static final String VERSION 		= "version";
					public static final String DESCRIPTION 	= "description";
				}
			}
		}
		
		public class ServiceList{
			
			public static final String PATH 	= "$.service_list";
			
			public class Key{
				public static final String SERVICE_NAME 	= "service_name";
				public static final String SERVICE_CLASS 	= "service_class";
				public static final String SERVICE_GROUP 	= "service_group";
			}
		}

	}

	public class Validation{
		
		public class BackupRestore{
			public static final String NAME 			= "/backup-restore.properties";
			
			public class Key{
				public static final String VALIDATION_VERSION_STORYBOARD = "VALIDATION_VERSION_STORYBOARD";
				public static final String VALIDATION_VERSION_SCREEN = "VALIDATION_VERSION_SCREEN";
				public static final String VALIDATION_VERSION_TEMPLATE = "VALIDATION_VERSION_TEMPLATE";
				public static final String VALIDATION_VERSION_ACTION_CHAIN = "VALIDATION_VERSION_ACTION_CHAIN";
				public static final String VALIDATION_VERSION_ACTION_CHAIN_TEMPLATE = "VALIDATION_VERSION_ACTION_CHAIN_TEMPLATE";
			}
		}
		
	}
	
	public class FileLocal{
		
		public static final String NAME 				= "/file-local.properties";
		public static final String FILE_TYPE_DMA 	  	= "FILE_TYPE_DMA";
		
		public static final String FILE_NAME_SG 	  	= "FILE_NAME_SG";
		public static final String FILE_TYPE_SG 	  	= "FILE_TYPE_SG";
		
		public static final String ZIP_DOWNLOAD_NAME_DMA= "ZIP_DOWNLOAD_NAME_DMA";
		public static final String ZIP_TYPE  		  	= "ZIP_TYPE";
		public static final String ZIP_PASSWORD  		= "ZIP_PASSWORD";
		
		public class FolderName{
			public static final String VERSION  		= "FOLDER_NAME_VERSION";
			public static final String DMA  			= "FOLDER_NAME_DMA";
			public static final String SG 				= "FOLDER_NAME_SG";
//			public static final String WSDL 			= "FOLDER_NAME_WSDL";
//			public static final String OAUTH 			= "FOLDER_NAME_OAUTH";
		}
		
		public class Backup{
			public class Key{
				public static final String FILE_PATH	= "FILE_BACKUP_PATH";
			}	
		}
		public class Restore{
			public class Key{
				public static final String FILE_PATH	= "FILE_RESTORE_PATH";
			}
		}
		
		public class ActionChain{
			public class Key{
				public static final String FILE_PATH	= "FILE_ACTIONCHAIN_PATH";
			}	
		}
		public class ActionChainTemplate{
			public class Key{
				public static final String FILE_PATH	= "FILE_ACTIONCHAINTEMPLATE_PATH";
			}
		}

	}
	
//	public class FileClassPath{
//		
//		public static final String NAME 		= "/file-classpath.properties";
//		
//		public class Template{
//			public static final String EXCEL_DOWNLOAD_CLASSPATH = "TEMPLATE_EXCEL_DOWNLOAD_CLASSPATH";
//			public static final String EXCEL_DOWNLOAD_NAME 	 	= "TEMPLATE_EXCEL_DOWNLOAD_NAME";
//			public static final String EXCEL_DOWNLOAD_TYPE		= "TEMPLATE_EXCEL_DOWNLOAD_TYPE";
//		}
//	}
	
	public class SystemConfig{
		public static final String NAME 		= "/system-config.properties";
		
		public class Key{
			public static final String MAX_IMPORT_RECORD = "MAX_IMPORT_RECORD_PER_TIME";
			public static final String MAX_LIST_APP_UI_RECORD = "MAX_LIST_APP_UI_RECORD_PER_TIME";
			public static final String MAX_LIST_STORY_RECORD = "MAX_LIST_STORY_BOARD_RECORD_PER_TIME";
			public static final String MAX_LIST_LOG_RECORD = "MAX_LIST_LOG_RECORD_PER_TIME";
			public static final String MAX_DAY_LIST_LOG = "MAX_DURATION_DAY_LIST_LOG";
		}
	}
	
	public class MenuUserInfo{
        
        public static final String NAME             = "/menu-user-info.json";
    }
    public class MenuAdminInfo{
        
        public static final String NAME             = "/menu-admin-info.json";
    }
    
    public class MenuSystemInfo{
        
        public static final String NAME             = "/menu-system-info.json";
    }


	public class MenuTemplateAdminInfo{
        
        public static final String NAME             = "/menu-templateadmin-info.json";
    }

	
	public class PreviewProperties {
		public static final String NAME = "/preview.properties";
		public static final String DOMAIN = "DOMAIN";
		public static final String FILE_PATH_PREVIEW_CONFIG = "FILE_PATH_PREVIEW_CONFIG";
		public static final String SCREEN_PREVIEW_URL_ENDPOINT = "$.SCREEN_PREVIEW.URL['Screen Preview Server Endpoint']";
		public static final String SCREEN_PREVIEW_ENABLE_MODE = "$.SCREEN_PREVIEW.ENABLE_MODE['Enable Function']";
		public static final String GET_PAIRING_KEY = "/service/getPairingKey";
		public static final String REFRESH_SCREEN_PREVIEW = "/service/refreshScreenPreview";
		public static final String RESET_SCREENSHOT = "/service/resetScreenshot";
		public static final String GET_PAIRING_KEY_LIST = "/service/getPairingKeyList";
		public static final String GET_URI = "/service/getUri";
		public static final String UPLOAD_SCREEN_SHOT = "/service/uploadScreenShot";
		public static final String GET_SCREENSHOT = "/service/getScreenshot";
	}
}
