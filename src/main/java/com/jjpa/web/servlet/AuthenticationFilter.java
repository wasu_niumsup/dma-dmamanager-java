package com.jjpa.web.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.UserModel;
import com.jjpa.spring.SpringApplicationContext;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
public class AuthenticationFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
	
	@SuppressWarnings({"unchecked"})
	private Map<String, Set<String>> roleActions = Collections.EMPTY_MAP;
	/**
	 * Default constructor.
	 */
	public AuthenticationFilter() {
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		logger.info("init()");
		setRoleActions(readFileAsString());
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		logger.info("destroy()");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		String contextPath = request.getContextPath();
		String path = request.getRequestURI().replaceFirst(contextPath, "");
		Object userRole = null;
		String username = null;
		UserSession userSeesion = SpringApplicationContext.getBean("UserSession");
		UserModel user = userSeesion.getUser();
		if (user != null) {
			userRole = user.getRole();
			username = user.getUsername();
		}
		boolean allow = hasSufficientRole(userRole, path);
		logger.debug(" User '" + username + "' "+request.getMethod()+"-> " + path + " = " + allow);
		if (allow) {
			chain.doFilter(request, response);
		} else if (username != null) {
			logger.error(" User '" + username + "' does not have right to access");
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		/*
		} else if("GET".equalsIgnoreCase(request.getMethod())){
			response.sendRedirect(contextPath+"process/Login/?ts="+System.currentTimeMillis());
		*/
		} else {
			logger.error("[doFilter] : No session or session Expired.");
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
	}

	private String readFileAsString() {
		StringBuffer fileData = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/access.properties")));
			char[] buf = new char[1024];
			int numRead = 0;
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
			}
		} catch (Exception e) {
			logger.error("readFileAsString", e);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				logger.error("readFileAsString", e);
			}
		}
		return fileData.toString();
	}

	public void setRoleActions(String roleActionsParam) {
		StringTokenizer roleActionsParamTokenizer = new StringTokenizer(roleActionsParam, ";");
		roleActions = new HashMap<String, Set<String>>(roleActionsParamTokenizer.countTokens());
		while (roleActionsParamTokenizer.hasMoreTokens()) {
			String roleActionArray[] = roleActionsParamTokenizer.nextToken().trim().split(":");
			if (roleActionArray.length == 2) {
				String role = roleActionArray[0].toLowerCase();
				roleActions.put(role, commaDelimitedStringToSet(roleActionArray[1]));
			}
		}
	}

	private Set<String> commaDelimitedStringToSet(String string) {
		Set<String> myHashSet = new HashSet<>(); // Or a more realistic size
		StringTokenizer st = new StringTokenizer(string, ",");
		while (st.hasMoreTokens())
			myHashSet.add(st.nextToken());
		return myHashSet;
	}

	public boolean hasSufficientRole(Object userRole, String actionName) {
		if (userRole == null){
			userRole = "*";
		}
		if (userRole instanceof String) {
			String roles = (String) userRole;
			if(!roles.startsWith("*")){
				roles = "*,"+roles;
			}
			String[] userRoleArr = roles.split(",");
			for (int i = 0; i < userRoleArr.length; i++) {
				String role = userRoleArr[i].toLowerCase();
				Set<String> roleAction = roleActions.get(role);
				if (hasSufficientRole(actionName, roleAction)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean hasSufficientRole(String actionName, Set<String> actions) {
		if (actions != null) {
			Iterator<String> iter = actions.iterator();
			while (iter.hasNext()) {
				String action = iter.next();
				if (matches(action, actionName)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean matches(String action, String actionName){
		action = action.replaceAll(Pattern.quote("*"), "(.*)");
		return actionName.matches(action);
	}

}
