package com.jjpa.dma.manager.processor.actionchain;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByTypeActionChainProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(FindByTypeActionChainProcessor.class);
		
	protected ActionChainAccessor actionChainAccessor;
	
	public FindByTypeActionChainProcessor() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByTypeActionChainProcessor");
		logger.debug("Process ===> FindByTypeActionChainProcessor : {}", input.toJsonString());
		
		String actionChainType = (String)input.getValue("actionChainType");
		return actionChainAccessor.findActionChainByType(actionChainType);
		
	}

}
