package com.jjpa.dma.manager.processor.storyboard;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;

public class DownloadBackupFileProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(DownloadBackupFileProcessor.class);

	@Override
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("Process ===> DownloadBackupFileProcessor");
    	
		/* get parameter from UI */ 
    	String fileNameInput = (String) request.getParameter(BackupConstants.FILE_UPLOAD_NAME);
    	logger.info(BackupConstants.FILE_UPLOAD_NAME+" = "+fileNameInput);
    	
   	
    	/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties("/file-local.properties");
		String fileBackupPath = ManagerMap.getString(propMap, "FILE_BACKUP_PATH", "");
		String fileBackupType = ManagerMap.getString(propMap, "FILE_TYPE_DMA", "");
		String zipName = ManagerMap.getString(propMap, "ZIP_DOWNLOAD_NAME_DMA", "");
		if("".equals(fileBackupPath) || "".equals(zipName)){
			logger.info("stop process : In file-local.properties : FILE_BACKUP_PATH or ZIP_DOWNLOAD_NAME_DMA is empty.");
		}
		
		String fileSrc =  getAbsolutePathFile(fileNameInput);//fileBackupPath + fileNameInput;
		String fileName = zipName + fileBackupType;
		
		/* downloadTemplate */
		try{							
	    	logger.info("downloadBackup : starting..");	    	
	    	writeToResponse(fileSrc, fileName, response);
	    	
    	}finally{
    		ManagerStore.deleteFile(fileSrc);
    	}
    	
		logger.info("End procces : DownloadBackupFileProcessor");
    }
	
	public String getAbsolutePathFile(String fileNameInput) throws Exception {
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties("/file-local.properties");
		String fileBackupPath = ManagerMap.getString(propMap, "FILE_BACKUP_PATH", "");
		String fileBackupType = ManagerMap.getString(propMap, "FILE_TYPE_DMA", "");
		String zipName = ManagerMap.getString(propMap, "ZIP_DOWNLOAD_NAME_DMA", "");
		if("".equals(fileBackupPath) || "".equals(zipName)){
			logger.info("stop process : In file-local.properties : FILE_BACKUP_PATH or ZIP_DOWNLOAD_NAME_DMA is empty.");
		}
		String fileSrc = fileBackupPath + fileNameInput;
		return fileSrc;
	}
	
	private synchronized void writeToResponse(String pathFile, String fileName, HttpServletResponse response) throws Exception{
//		response.setContentType("application/vnd.ms-excel");
	    response.setHeader("Content-Disposition", "attachment; filename="+fileName);
	
		FileInputStream in = new FileInputStream(new File(pathFile));
		
		ServletOutputStream out = response.getOutputStream();
		byte[] buffer = new byte[4096];
	    int bytesRead = -1;
	    
	    while ((bytesRead = in.read(buffer)) != -1) {
	    	out.write(buffer, 0, bytesRead);
	    }
	    in.close();
		out.close();
	}

}
