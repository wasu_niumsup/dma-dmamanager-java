package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerStore;

/* 
 * 17/08/2561 add by Akanit : get data all database : finished.
 * */
public class GenerateActionChainFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateActionChainFolderBackup.class);

	protected ActionChainAccessor actionChainAccessor;
	
	public GenerateActionChainFolderBackup() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	public void process(Reference reference, String directoryFileBackup) throws Exception {
		logger.info("process : generate actionChain backup folder");
		
		List<Map<String, Object>> actionChainRequiredList = new GetReferenceProcessor().getDmaActionChainRequireds(reference);
		if(actionChainRequiredList==null){
			return;
		}
		
		String actionChainName;
		for(Map<String, Object> actionChainRequired : actionChainRequiredList){
			
			/* find actionChain in mongo */
			actionChainName = ManagerMap.getString(actionChainRequired, ReferenceConstants.DMA.NAME);
			DMAActionChainDocument actionChainDoc = actionChainAccessor.findActionChainByType(actionChainName);
			if(actionChainDoc==null){
				String errorMessage = "ActionChain '"+ actionChainName +"' not found in DB.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/actionChain/{actionChainName}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.ACTION_CHAIN + File.separator + actionChainName;
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(actionChainDoc.toMap(), directoryFile);
		}

	}
	
	/* 17/08/2561 add by Akanit : get data all database */
	public void process(String directoryFileBackup) throws Exception {
		logger.info("process : generate all actionChain backup folder");
		
		List<DMAActionChainDocument> actionChainDocList = actionChainAccessor.listActionChain();
		
		for(DMAActionChainDocument actionChainDoc : actionChainDocList){
			
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/actionChain/{actionChainName}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.ACTION_CHAIN + File.separator + actionChainDoc.getActionChainType();
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(actionChainDoc.toMap(), directoryFile);
		}
		
	}
	
}
