package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ResponseStatusTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.ResponseStatusTypeDocument;

public class ResponseStatusTypeAccessorImpl implements ResponseStatusTypeAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected ResponseStatusTypeAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Map<String, Object> listResponseStatusType(ServiceData input) throws Exception{
		
		List<ResponseStatusTypeDocument> responseStatusTypeDocList = mongoDbAccessor.findAll(ResponseStatusTypeDocument.class);
		
		Map<String, Object> responseStatusType;
		List<Map<String, Object>> responseStatusTypeList = new ArrayList<>();
		for(ResponseStatusTypeDocument document : responseStatusTypeDocList){
			responseStatusType = new HashMap<String, Object>();
			responseStatusType.put("labelName", document.getLabelName());
			responseStatusType.put("value", document.getValue());
			responseStatusTypeList.add(responseStatusType);
		}
		
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("responseStatusType", responseStatusTypeList);
		return output;
	}
	
}
