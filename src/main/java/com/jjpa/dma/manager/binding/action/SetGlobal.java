package com.jjpa.dma.manager.binding.action;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

import blueprint.util.StringUtil;

@SuppressWarnings("unchecked")
public class SetGlobal implements ActionBinding {

	private final Logger logger = LoggerFactory.getLogger(SetGlobal.class);

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard,
			Map<String, Object> eventMap) throws Exception {
		logger.info("Binding set global in Action");
		DocumentContext ctx = JsonPath.parse(document.getNativeJson().get("data"));

		/* 
		 * get globalOutputList from screen and api
		 * */
		List<Map<String, Object>> globalOutputList_screen = (List<Map<String, Object>>) storyBoard
				.get(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST);
		List<Map<String, Object>> globalOutputList_api = (List<Map<String, Object>>) eventMap
				.get(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST);
		logger.debug("globalOutputList_screen={}", globalOutputList_screen);
		logger.debug("globalOutputList_api={}", globalOutputList_api);


		String templateType = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.TEMPLATE_TYPE, DmaConstants.AppUI.Value.STATIC);
		

		/* 
		 * create bindingPath 
		 * */
		String bindingPath = generateBindingPath(document);
		logger.info("bindingPath={}", bindingPath);

		/* 
		 * binding global
		 * */
		bindingGlobal_screen(ctx, bindingPath, globalOutputList_screen);
		bindingGlobal_api(ctx, bindingPath, globalOutputList_api,templateType);
		bindingGlobal_api_publish_version(ctx,bindingPath);
		return ctx;
	}

	private String generateBindingPath(DMAActionChainDocument document) {
		String bindingPath = "";
		List<Map<String, Object>> bindingList = document.getBinding();
		for (Map<String, Object> binding : bindingList) {
			String bindingName = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			if ("options".equals(bindingName)) {
				bindingPath = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));
				break;
			}
		}
		return bindingPath;
	}

	private void bindingGlobal_screen(DocumentContext ctx, String bindingPath,
			List<Map<String, Object>> globalOutputList) {

		if (globalOutputList != null) {
			for (Map<String, Object> globalOutput : globalOutputList) {

				/* check keep is true? */
				boolean keep = DataUtils.getBooleanValue(globalOutput.get(DmaConstants.StoryBoard.Key.KEEP));
				if (keep) {
					logger.info("###found keep. then binding Global screen.");

					// get parameter
					String globalName = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
					String outputName = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.OUTPUT_NAME));

					// generate key and value
					String globalNameKey = DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;

					/* 15/02/2561 edit by Akanit */
					//					String globalPathValue = setJasonBucket(JasonPathConstants.UiJson.Key.GET + "." + outputName);
					String globalPathValue = JasonNetteUtils.generateNativeValue(outputName);

					logger.info("globalNameKey={}", globalNameKey);
					logger.info("globalPathValue={}", globalPathValue);

					ctx.put(bindingPath, globalNameKey, globalPathValue);
				}
			}
		}
	}

	public void bindingGlobal_api(DocumentContext ctx, String bindingPath,
			List<Map<String, Object>> globalOutputList,String templateType ) throws Exception {

		if (globalOutputList != null) {
			for (Map<String, Object> globalOutput : globalOutputList) {

				/* check keep is true? */
				boolean keep = DataUtils.getBooleanValue(globalOutput.get(DmaConstants.StoryBoard.Key.KEEP));

				/**
				* Tanawat W.
				* [2018-03-02] Add Create Output
				* fieldType : String is normal input
				* fieldType : Other are Location , Call etc.
				* 
				*/
				String valueFrom = DataUtils
						.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM), null);
				String fieldType = DataUtils.getStringValue(globalOutput, "fieldType", "string");
				if (!StringUtil.isBlank(valueFrom)) {

					if (DmaConstants.StoryBoard.Value.MANUAL.equals(valueFrom)
							&& "string".equalsIgnoreCase(fieldType)) {
						logger.info("###found Manual. then binding Global.");

						// get parameter
						String globalName = DataUtils
								.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
						String value = DataUtils
								.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE));

						// generate key and value
						String globalNameKey = JasonNetteUtils.setGlobal(globalName); //DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;

						String globalPathValue = JasonNetteUtils.cleanScope(value);

						logger.info("globalNameKey={}", globalNameKey);
						logger.info("globalPathValue={}", globalPathValue);

						ctx.put(bindingPath, globalNameKey, globalPathValue);

						// Skip
						keep = false;
						continue;

					} else if (DmaConstants.StoryBoard.Value.NATIVE.equals(valueFrom)
							&& !"string".equalsIgnoreCase(fieldType)) {

						logger.info("###found Native  binding input from native.");
						/* get parameter */
						Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalOutput,
								DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);
						String dmaParameter = DataUtils.getStringValue(valueProperties,
								DmaConstants.StoryBoard.Key.DMA_PARAMETER);


						// get parameter
						String globalName = DataUtils
						.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
						String globalPathValue = "";

						String globalNameKey = JasonNetteUtils.setGlobal(globalName);

						/* "{{"$get."+ value +" || ''}}"; */
						if (!StringUtil.isBlank(dmaParameter)) {
							globalPathValue = JasonNetteUtils.generateNativeValue(dmaParameter);
						}

						logger.info("globalNameKey={}", globalNameKey);
						logger.info("globalPathValue={}", globalPathValue);

						ctx.put(bindingPath, globalNameKey, globalPathValue);

						// Skip
						keep = false;
						continue;

					}

					else if (DmaConstants.StoryBoard.Value.NATIVE.equals(valueFrom)
							&& "string".equalsIgnoreCase(fieldType)) {
						logger.info("###found Native [Screen]. then binding Global.");
						
						
						if (DmaConstants.AppUI.Value.DYNAMIC_LIST.equalsIgnoreCase(templateType)
						
						)   {

							/**
							 * Case detailList
							 */
							Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalOutput,
							DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);
							String value = DataUtils.getStringValue(valueProperties, "pathValueName");


							// get parameter
							String globalName = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
							String globalPathValue = "";

							String globalNameKey = JasonNetteUtils.setGlobal(globalName);

							/* "{{"$get.selected_details_item."+ value +" || ''}}"; */
							globalPathValue = JasonNetteUtils.generateSelectedItemNativeValue(value);

							logger.info("globalNameKey={}", globalNameKey);
							logger.info("globalPathValue={}", globalPathValue);

							ctx.put(bindingPath, globalNameKey, globalPathValue);


							// Skip
							keep = false;
							continue;
						}
						else if (DmaConstants.AppUI.Value.DYNAMIC_INPUT.equalsIgnoreCase(templateType))   {
							/**
							 * Case input from
							 */
							Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalOutput,
							DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);
							String value = DataUtils.getStringValue(valueProperties, "pathValueName");


							// get parameter
							String globalName = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
							String globalPathValue = "";

							String globalNameKey = JasonNetteUtils.setGlobal(globalName);

							/* "{{"$get."+ value +" || ''}}"; */
							globalPathValue = JasonNetteUtils.generateNativeValue(value);

							logger.info("globalNameKey={}", globalNameKey);
							logger.info("globalPathValue={}", globalPathValue);

							ctx.put(bindingPath, globalNameKey, globalPathValue);


							// Skip
							keep = false;
							continue;
						}
						
						else{
							logger.warn("Not keep from this please keep in screen output");
						}


					}

				}

				if (keep) {
					logger.info("###found keep. then binding Global Api.");

					// get parameter
					String globalName = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GLOBAL_NAME));
					String path = DataUtils
							.getStringValue(globalOutput.get(DmaConstants.StoryBoard.Key.GlobalInputList.PATH));

					// generate key and value
					String globalNameKey = DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;

					/* 15/02/2561 edit by Akanit */
					//					String globalPathValue = setJasonBucket(JasonPathConstants.UiJson.Key.RESULT_RESULT_DATA + "." + path);
					String globalPathValue = JasonNetteUtils.generateResultData(path);

					logger.info("globalNameKey={}", globalNameKey);
					logger.info("globalPathValue={}", globalPathValue);

					ctx.put(bindingPath, globalNameKey, globalPathValue);
				}

			}
		}
	}


	private void bindingGlobal_api_publish_version(DocumentContext ctx, String bindingPath) throws Exception {

				ctx.put(bindingPath, "_dma_param_internal_publish_version", "{{$get._dma_param_internal_publish_version}}");
	}


}
