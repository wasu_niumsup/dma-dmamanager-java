package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityScreenListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

/* 
 * 03/01/2562 add by Akanit : data is very large size : finished.
 * */
public class TempEntityConfigAccessorImpl implements TempEntityConfigAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	protected TempEntityScreenListAccessor tempEntityScreenListAccessor;
	
	protected TempEntityConfigAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
		this.tempEntityScreenListAccessor = SpringApplicationContext.getBean("TempEntityScreenListAccessor");
	}
	
	@Override
	public List<TempEntityConfigDocument> findAllNotGetRelateData() throws Exception {
		return mongoDbAccessor.findAll(TempEntityConfigDocument.class);
	}
	
	@Override
	public TempEntityConfigDocument findTempConfigByName(String entityName) throws Exception {
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(DmaConstants.Entity.Key.ENTITY_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(entityName), Pattern.CASE_INSENSITIVE)));
		TempEntityConfigDocument document = mongoDbAccessor.findOneByCriteria(findByName, TempEntityConfigDocument.class);
		
		/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'entity'. */
		loadRelateData(document, entityName);
		
		return document;
	}

	@Override
	public Object saveTempConfig(TempEntityConfigDocument document, String user) throws Exception {
		
//		if(document.getId()!=null){
//			TempEntityConfigDocument entityDoc = mongoDbAccessor.findById(document.getId(), TempEntityConfigDocument.class);
//			entityDoc.setEntityName(document.getEntityName());
//			entityDoc.setHome(document.getHome());
//			entityDoc.setFirstPage(document.getFirstPage());
//			entityDoc.setScreenList(document.getScreenList());
//			return mongoDbAccessor.save(entityDoc, user);
//		}else{			
		
			/* 03/01/2562 add by Akanit : data is very large size : move 'entity' to new database table */
			moveRelateData(document, user);
		
			return mongoDbAccessor.save(document, user);
//		}
			
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void deleteTempConfig(ServiceData input) throws Exception {
		
		String user = (String)input.getValue("user");
		String entityName;
		
		
		List<Map<String, Object>> entityList = (List<Map<String, Object>>)input.getValue(DmaConstants.Entity.Key.ENTITY_LIST);
		
		for(Map<String, Object> entityMap : entityList){
			
			entityName = ManagerMap.getString(entityMap, DmaConstants.Entity.Key.ENTITY_NAME);
			
			/* 03/01/2562 add by Akanit : data is very large size : delete relate data. */
			deleteRelateData(entityName, user);
			
			mongoDbAccessor.delete(findTempConfigByName(entityName), user);
		}
	}
	
	
	
	/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'entity'. */
	public void loadRelateData(TempEntityConfigDocument document, String entityName) throws Exception {
		if(document!=null){
			document.setScreenList(tempEntityScreenListAccessor.findByName(entityName));
		}
	}
	
	/* 03/01/2562 add by Akanit : data is very large size : move 'entity' to new database table */
	public void moveRelateData(TempEntityConfigDocument document, String user) throws Exception {
		if(document!=null){
			tempEntityScreenListAccessor.saveByName(document.getEntityName(), document.getScreenList(), user);
			document.setScreenList(new ArrayList<>());
		}
	}

	/* 03/01/2562 add by Akanit : data is very large size : delete relate data. */
	public void deleteRelateData(String entityName, String user) throws Exception {
		tempEntityScreenListAccessor.deleteByName(entityName, user);
	}

}
