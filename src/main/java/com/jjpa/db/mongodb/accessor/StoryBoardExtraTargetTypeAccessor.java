package com.jjpa.db.mongodb.accessor;

import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardExtraTargetTypeDocument;

public interface StoryBoardExtraTargetTypeAccessor {

	public Map<String,Object> listStoryBoardExtraTargetType(ServiceData input) throws Exception;
	public StoryBoardExtraTargetTypeDocument findStoryBoardExtraTargetTypeByName(String uiName) throws Exception;
	public Object saveStoryBoardExtraTargetType(StoryBoardExtraTargetTypeDocument document, String user) throws Exception;
	public void deleteStoryBoardExtraTargetType(String uiName, String user) throws Exception;
}
