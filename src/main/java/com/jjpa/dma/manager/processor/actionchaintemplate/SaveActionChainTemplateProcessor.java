package com.jjpa.dma.manager.processor.actionchaintemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.model.reference.RequiredReference;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
public class SaveActionChainTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SaveActionChainTemplateProcessor.class);
		
	protected ActionChainTemplateAccessor actionChainTemplateAccessor;
	protected ActionChainAccessor actionChainAccessor;
	
	public SaveActionChainTemplateProcessor() {
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveActionChainTemplateProcessor");
		logger.debug("Process ===> SaveActionChainTemplateProcessor : {}", input.toJsonString());
		
		return saveActionChainTemplate(input);
		
	}
	
	private Object saveActionChainTemplate(ServiceData input) throws Exception {
		
		String user = (String)input.getValue("user");
		String actionChainCode = (String)input.getValue("actionChainCode");
		
		Map<String, Object> templateMap = JsonPath.parse(actionChainCode).json();
		
		logger.info("Process ===> SaveActionChainTemplateProcessor templateMap : "+ templateMap);

		Map<String,Object> chainTemplate = (Map<String,Object>)templateMap.get("chainTemplate");


		List<Map<String,Object>> actionChainRequiredList = new ArrayList<>();

		logger.info("Start deserializeChainForDMA");
		deserializeChainForDMA("$.root", chainTemplate, 0, "", actionChainRequiredList);
		
		logger.info("End deserializeChainForDMA  output : " + actionChainRequiredList);

		
		/* 15/03/2561 add by Akanit */
		Reference reference = generateReference(templateMap, actionChainRequiredList);
		
		
		DMAActionChainTemplate actionChainTemplate = new DMAActionChainTemplate();
		actionChainTemplate.setActionType((String)templateMap.get("actionType"));
		actionChainTemplate.setVersion((String)templateMap.get("version"));
		actionChainTemplate.setChainTemplate(  chainTemplate );
		actionChainTemplate.setReference(reference);
		
		/* 16/05/2561 add by Akanit new lastUpdate*/
		actionChainTemplate.setLastUpdate(new Date());
		
		return actionChainTemplateAccessor.saveActionChainTemplate(actionChainTemplate,user);
	}

	/* 15/03/2561 add by Akanit */
	private Reference generateReference(Map<String, Object> templateMap, List<Map<String,Object>> actionChainRequiredList) throws Exception {
		
		Reference reference = new Reference();
		
		/* set version to Reference */
		reference.setVersion(ManagerMap.getString(templateMap, "version"));
		
		/* set required to Reference */
		RequiredReference requiredRef = new RequiredReference();
		
		requiredRef.setDmaActionChainRequired(actionChainRequiredList);
		reference.setRequired(requiredRef);

		return reference;
	}
	
	private void deserializeChainForDMA(String path, Map<String, Object> chainMap,
			int level, String parrentType , List<Map<String,Object>> actionChainRequiredList) throws Exception {
		
		if (chainMap.containsKey("type")) {
			String type = chainMap.get("type").toString();
			String dmaType =chainMap.get("dmaType").toString();

			logger.info(
					"Level : " + level + ", ParrentType : " + parrentType + ", type : " + type + ", dmaType : " + dmaType + ", path : " + path );
			
			/**
			 * prepare data
			 * 
			 */
			DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(dmaType);
			if(actionChainDocument==null){
				String errorMessage = "ActionChain \"" + dmaType + "\" not found. Need patching.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}
			
			if(!StringUtil.isBlank(actionChainDocument.getActionChainType())){
				
				Map<String, Object> rowActionChain = ManagerArrayList.findRowBySearchString(actionChainRequiredList, ReferenceConstants.DMA.NAME, actionChainDocument.getActionChainType());
				if(rowActionChain==null){
					
					Map<String,Object> actionChainRequired = new LinkedHashMap<>();
					actionChainRequired.put(ReferenceConstants.DMA.NAME, actionChainDocument.getActionChainType()  );
					actionChainRequired.put(ReferenceConstants.DMA.VERSION,actionChainDocument.getVersion());
		
					actionChainRequiredList.add(actionChainRequired);
				}
			}
			

			// next chain
			if (chainMap.containsKey("success")) {
				deserializeChainForDMA(path + ".success", (Map<String, Object>) chainMap.get("success"),
						level + 1, type, actionChainRequiredList);
			}

			if (chainMap.containsKey("error")) {
				deserializeChainForDMA(path + ".error", (Map<String, Object>) chainMap.get("error"),
						level + 1, type, actionChainRequiredList);
			}

		}

	}

}
