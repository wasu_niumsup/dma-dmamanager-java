package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;

public class EntityAccessorImpl implements EntityAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;
	protected AppUiAccessor appUiAccessor;
	
	protected EntityAccessorImpl(MongoDbAccessor mongoDbAccessor,AppUiAccessor appUiAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
		this.appUiAccessor = appUiAccessor;
		
	}

	public EntityDocument findEntityByName(String entityName) throws Exception{
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.Entity.Path.ENTITY_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(entityName), Pattern.CASE_INSENSITIVE)));
		EntityDocument entityDoc = mongoDbAccessor.findOneByCriteria(findByName, EntityDocument.class);
		return entityDoc;
	}
	
	public Map<String,Object> listEntity(ServiceData input) throws Exception {
		List<EntityDocument> entityList = mongoDbAccessor.findAll(EntityDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.Entity.Key.ENTITY_LIST, getEntityList(entityList));
		return result;
	}
	public Map<String,Object> listEntityByCriteria(Query query) throws Exception{
		List<EntityDocument> entityList = mongoDbAccessor.findByCriteria(query,EntityDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.Entity.Key.ENTITY_LIST, getEntityList(entityList));
		return result;

	}

	@SuppressWarnings("unchecked")
	public void deleteEntity(ServiceData input) throws Exception {
		String user = (String)input.getValue(AppConstants.USER);
		List<Map<String, Object>> entityList = (List<Map<String, Object>>)input.getValue(DmaConstants.Entity.Key.ENTITY_LIST);
		for(Map<String, Object> entityObj : entityList){
			String entityName = (String)entityObj.get(MongoConstants.Entity.Path.ENTITY_NAME);
			mongoDbAccessor.delete(findEntityByName(entityName), user);
		}
	}
	
	public Object saveEntity(EntityDocument document, String user) throws Exception{
		if(document.getId()!=null){
			EntityDocument entityDoc = mongoDbAccessor.findById(document.getId(), EntityDocument.class);
			entityDoc.setEntityName(document.getEntityName());
			entityDoc.setRemark(document.getRemark());
			entityDoc.setStoryName(document.getStoryName());
			entityDoc.setRole(document.getRole());
			entityDoc.setTheme(document.getTheme());
			entityDoc.setStatus(document.getStatus());
			entityDoc.setPublishStatus(document.getPublishStatus());
			entityDoc.setVersion(document.getVersion());
			entityDoc.setLastUpdate(document.getLastUpdate());
			entityDoc.setAppInfo(document.getAppInfo());
			return mongoDbAccessor.save(entityDoc, user);
		}else{
			document.setPublishStatus(DmaConstants.PublishedStatus.WARNING);
			return mongoDbAccessor.save(document, user);
		}
	}
	
	public Object updateEntity(EntityDocument document, Date lastPublish, String user) throws Exception{
		if(document.getId()!=null){
			return mongoDbAccessor.save(document, user);
		}
		return null;
	}
	
//	public EntityDocument setEntityStatus(String entity, Date lastPublish, String user) throws Exception {
//		Query findByName = new Query();
//		findByName.addCriteria(Criteria.where(MongoConstants.Entity.Path.ENTITY_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(entity), Pattern.CASE_INSENSITIVE)));
//		EntityDocument entityDoc = mongoDbAccessor.findOneByCriteria(findByName, EntityDocument.class);
//		entityDoc.setLastPublish(lastPublish);
//		entityDoc.setPublishStatus(DmaConstants.PublishedStatus.PUBLISHED);
//		return (EntityDocument) mongoDbAccessor.save(entityDoc, user);
//	}

//	public EntityDocument setLastPublish(String entity, StoryBoardDocument story, Map<String, Object> storyBoardUiList,
//			String user) throws Exception {
//		Date lastPublish = new Date();
//		// set story board last publish
//		story.setLastPublish(lastPublish);
//		mongoDbAccessor.save(story, user);
//		// set app ui last publish
//		for (String uiName : storyBoardUiList.keySet()) {
//			
//			if (MongoConstants.AppUI.UiName.HOME.equalsIgnoreCase(uiName) 
//			 || MongoConstants.AppUI.UiName.FIRST.equalsIgnoreCase(uiName)
//			 || MongoConstants.AppUI.UiName.FIRST_PAGE.equalsIgnoreCase(uiName)) {
//				// do nothing
//			
//			} else {
//				AppUiDocument appUiDoc = appUiAccessor.findAppUiByName(uiName);
//				if (appUiDoc != null) {
//					appUiDoc.setLastPublish(lastPublish);
//					mongoDbAccessor.save(appUiDoc, user);
//				}else{
//					
//				}
//			}
//		}
//		// set entity last publish
//		return setEntityStatus(entity, lastPublish, user);
//	}
	
	public StoryBoardDocument findStoryBoardByEntity(String entity) throws Exception {

		EntityDocument entityDoc = findEntityByName(entity);
		String storyName = entityDoc.getStoryName();

		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.Entity.Path.STORY_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(storyName), Pattern.CASE_INSENSITIVE)));
		StoryBoardDocument story = mongoDbAccessor.findOneByCriteria(findByName, StoryBoardDocument.class);
		return story;
	}
	
	private List<Map<String,Object>> getEntityList(List<EntityDocument> entityDocList){
		List<Map<String,Object>> entityList = new ArrayList<Map<String,Object>>();
		for(EntityDocument document : entityDocList){
			entityList.add(getEntity(document));
		}
		return entityList;
	}
	
	private Map<String,Object> getEntity(EntityDocument document){
		
		Map<String,Object> entity = new HashMap<String,Object>();
		entity.put(DmaConstants.Entity.Key.ENTITY_NAME, document.getEntityName());
		entity.put(DmaConstants.Entity.Key.STORY_NAME, document.getStoryName());
		entity.put(DmaConstants.Entity.Key.REMARK, document.getRemark());
		entity.put(DmaConstants.Entity.Key.ROLE, document.getRole());
		entity.put(DmaConstants.Entity.Key.THEME, document.getTheme());
		entity.put(DmaConstants.Entity.Key.VERSION, document.getVersion());
		entity.put(DmaConstants.Entity.Key.STATUS, document.getStatus());
		entity.put(DmaConstants.Entity.Key.PUBLISH_STATUS, document.getPublishStatus());
		entity.put(DmaConstants.Entity.Key.LAST_UPDATE, document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		entity.put(DmaConstants.Entity.Key.LAST_PUBLISH, document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
		return entity;
	}


	public List<EntityDocument> listFullEntityByCriteria(Query query) throws Exception{
		List<EntityDocument> entityList = mongoDbAccessor.findByCriteria(query,EntityDocument.class);
		return entityList;

	}
	
	
	
}
