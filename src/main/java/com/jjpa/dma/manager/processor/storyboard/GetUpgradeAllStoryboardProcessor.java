package com.jjpa.dma.manager.processor.storyboard;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import servicegateway.utils.ManagerMap;

public class GetUpgradeAllStoryboardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

  private static final Logger logger = LoggerFactory.getLogger(GetUpgradeAllStoryboardProcessor.class);
  private static final String TAG = "GetUpgradeAllStoryboardProcessor";

  @Override
  public Object process(HttpServletRequest request, ServiceData input) throws Exception {

    logger.info("Process ===> " + TAG);
    logger.debug("Process ===> {}" , input.toJsonString());

    return upgradeAllStoryboard(input);
  }

  private Object upgradeAllStoryboard(ServiceData input) throws Exception {

    Map<String,Object> resultContext = callHook("UpgradeAllStoryboard", input.getData()).json();
    if( ManagerMap.getBoolean(resultContext, "resultSuccess") == false) {
      throw new Exception(ManagerMap.getString(resultContext, "resultMessage"));
    }

    return resultContext.get("responseData");
  }

}