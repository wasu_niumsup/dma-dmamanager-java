package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dma_entity_config")
public class PublishEntityConfigDocument extends EntityConfigDocument{

	private static final long serialVersionUID = 1L;
}
