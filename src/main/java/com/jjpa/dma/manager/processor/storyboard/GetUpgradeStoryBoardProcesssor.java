package com.jjpa.dma.manager.processor.storyboard;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

public class GetUpgradeStoryBoardProcesssor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetUpgradeStoryBoardProcesssor.class);
	
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	
	public GetUpgradeStoryBoardProcesssor() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	public Object process(ServiceData serviceData) throws Exception {
		
		logger.info("Start process : GetUpgradeStoryBoardProcesssor");
//		  Map<String,Object> inputHeader=serviceData.getHeader();
		  Map<String,Object> inputBody= serviceData.getData();
//		  logger.info("inputHeader="+inputHeader);
		  logger.info("inputBody="+inputBody);

		  Map<String,Object> input = new LinkedHashMap<>(inputBody);
		  Map<String, Object> output = new LinkedHashMap<>();
		  Map<String, Object> screenFromResponse = new LinkedHashMap<>();
		
		  String TAG = "[main process]";
		  
		
		String screenName = ManagerMap.getString(input, DmaConstants.StoryBoard.Key.SOURCE);
		
		List<Map<String, Object>> storyBoardList =  ManagerMap.get(input, DmaConstants.StoryBoard.Key.STORY_BOARD, List.class);
		
		if(storyBoardList!=null) {
			screenFromResponse = getMapInListMap(storyBoardList, DmaConstants.StoryBoard.Key.SOURCE, screenName);
			output = new LinkedHashMap<>(screenFromResponse);
		}else {
			throw new Exception("Cannot get StoryBoard");
		}
		logger.info(TAG + "step 1. get selected screen = " + screenName);
		
		logger.info("Process ===> screenName :"+ screenName);
		//Get appUiAccessor
		AppUiDocument screenFromDB = appUiAccessor.findAppUiByName(screenName);
		
		logger.info(TAG + "step 2. get screen from db : templateType " + screenFromDB.getTemplateType() );
		//Check Type templateType
		String templateType = screenFromDB.getTemplateType();
		//logger.info("Process ===> TemplateType :"+ templateType);
		//Start Modify ActionList
		
		logger.info(TAG + "step 3. modifytActionList");
		List<Map<String, Object>> actionListModify = modifytActionList(screenFromResponse, screenFromDB);
		if(actionListModify!=null) {
			//logger.info("Process ===> TemplateType :"+ templateType);
			logger.info(TAG + "step 3.1 put action list");
			output.put(DmaConstants.StoryBoard.Key.ACTION_LIST, actionListModify);
		}
		//End Modify ActionList
		
		//Start Modify GlobalInputList
		List<Map<String, Object>> globalInputList = new ArrayList<>();
		//Check Type Template DYNAMIC_INPUT  fix 12/07/61 By Dee Request  by ice
		if(templateType.equals(DmaConstants.AppUI.Value.DYNAMIC_INPUT)) {
			logger.info(TAG + "step 4 case DYNAMIC_INPUT");
			logger.info("Process ===> TemplateType :"+ templateType);
			//Add DynamicInput
			List<Map<String, Object>> dynamicInputList =  modifyDynamicInputTypeInput(screenFromResponse, screenFromDB);
			if(dynamicInputList!=null) {
				//Merge List
				globalInputList.addAll(dynamicInputList);
			}
			//Add InputList
			List<Map<String, Object>> globalInputListModify = modifyGlobalInputList(screenFromResponse, screenFromDB);
			if(globalInputListModify!=null) {
				//Merge List
				globalInputList.addAll(globalInputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, globalInputList);
			//End Modify GlobalInputList
			
			//Start Modify GlobalOutputList
			List<Map<String, Object>> globalOutputList = new ArrayList<>();
			
			//Add DynamicOutput1
			List<Map<String, Object>> globalOutputListFromDynamicInput = mapDynamicInputToDynamicOutput(screenFromResponse, screenFromDB);
			if(globalOutputListFromDynamicInput!=null) {
			     globalOutputList.addAll(globalOutputListFromDynamicInput);	
			}
			//Add DynamicOutput2
			List<Map<String, Object>> dynamicOutputList =  modifyDynamicInputTypeOutput(screenFromResponse, screenFromDB);
			if(dynamicOutputList!=null) {
				//Merge List
				globalOutputList.addAll(dynamicOutputList);
			}
			//Add InputList
			List<Map<String, Object>> globalOutputListModify = modifyGlobalOutputList(screenFromResponse, screenFromDB);
			if(globalOutputListModify!=null) {
				globalOutputList.addAll(globalOutputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, globalOutputList);
		//Check Type Template DYNAMIC_LIST fix 12/07/61 By Dee Request  by ice
		}else if(templateType.equals(DmaConstants.AppUI.Value.DYNAMIC_LIST)) {
			logger.info(TAG + "step 4 case DYNAMIC_LIST");
			logger.info("Process ===> TemplateType :"+ templateType);
			//Add DynamicInputList
			List<Map<String, Object>> dynamicInputList =  modifyDynamicInputList(screenFromResponse, screenFromDB);
			if(dynamicInputList!=null) {
				//Merge List
				globalInputList.addAll(dynamicInputList);
			}
			//Add InputList
			List<Map<String, Object>> globalInputListModify = modifyGlobalInputList(screenFromResponse, screenFromDB);
			if(globalInputListModify!=null) {
				//Merge List
				globalInputList.addAll(globalInputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, globalInputList);
			//End Modify GlobalInputList
			
			//Start Modify GlobalOutputList
			List<Map<String, Object>> globalOutputList = new ArrayList<>();
			
			//Add DynamicOutput
			List<Map<String, Object>> dynamicOutputList =  modifyDynamicOutputList(screenFromResponse, screenFromDB);
				if(dynamicOutputList!=null) {
					//Merge List
					globalOutputList.addAll(dynamicOutputList);
				}
			//Add InputList
			List<Map<String, Object>> globalOutputListModify = modifyGlobalOutputList(screenFromResponse, screenFromDB);
			if(globalOutputListModify!=null) {
				globalOutputList.addAll(globalOutputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, globalOutputList);
			
		}else if (templateType.equals(DmaConstants.AppUI.Value.STATIC)) {
			logger.info(TAG + "step 4 case STATIC");
			logger.info("Process ===> TemplateType :"+ templateType);
			
			//Add InputList
			List<Map<String, Object>> globalInputListModify = modifyGlobalInputList(screenFromResponse, screenFromDB);
			if(globalInputListModify!=null) {
				//Merge List
				globalInputList.addAll(globalInputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, globalInputList);
			//End Modify GlobalInputList
			//Start Modify GlobalOutputList
			List<Map<String, Object>> globalOutputList = new ArrayList<>();
			
			//Add DynamicOutput
			List<Map<String, Object>> dynamicOutputList =  modifyDynamicOutputList(screenFromResponse, screenFromDB);
				if(dynamicOutputList!=null) {
					//Merge List
					globalOutputList.addAll(dynamicOutputList);
				}
			//Add InputList
			List<Map<String, Object>> globalOutputListModify = modifyGlobalOutputList(screenFromResponse, screenFromDB);
			if(globalOutputListModify!=null) {
				globalOutputList.addAll(globalOutputListModify);
			}
			output.put(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, globalOutputList);
		}
		
		
		//End Modify GlobalOutputList
		
		logger.info(TAG + "step 5 get uiJson from db to put in output");
		//MoveUiJson
		String uiJson = screenFromDB.getUiJson();
		if(uiJson!=null) {
			logger.info("Process ===> Update UiJson");
			output.put(DmaConstants.StoryBoard.Key.UI_JSON, uiJson);
		}
		
		//Get Current Version 
		int versionsScreen = Integer.parseInt(screenFromDB.getReference().getVersion());
		output.put(DmaConstants.StoryBoard.Key.DMA_SCREEN_VERSION, versionsScreen);
		logger.info(TAG + "step 6 put versionsScreen : " + versionsScreen);
		//Reset DMA_SCREEN_NEED_UPGRADE  Disable Upgrade
		output.put(DmaConstants.StoryBoard.Key.DMA_STORYBOARD_NEED_UPGRADE, false);
		logger.info(TAG + "step 7 reset flag  upgrade ");
		logger.info("Process ===> outputBody="+output);
		
		
		/* 21/05/2562 comment by Akanit : Jira DMA-1437 : Storyboard should not save upgrade */
//		storyBoardAccessor.upgradeVersionScreenToStoryboard(screenFromDB);
		
		/* 21/05/2562 add by Akanit : Jira DMA-1437 : Storyboard should not save upgrade */
		storyBoardAccessor.bindingUpgradeVersionScreenToStoryboard(output, screenFromDB);
		
		logger.info(TAG + "step 8 complete ");
		return output;
		
	}
	private List<Map<String,Object>> modifytActionList(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
	
		//Get ActionList
		Map<String, Object> confirmDialogMap = new LinkedHashMap<>();
		Map<String, Object> endAlertMap = new LinkedHashMap<>();

		List<Map<String, Object>> actionList  = screebFromDB.getActionList();
		List<Map<String, Object>> actionListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.ACTION_LIST, List.class);	
		logger.info("Process ===> ActionListFromResponse Size : "+actionListFromResponse.size());
		
		if(actionList!=null) {
			logger.info("Process ===> ActionList Size : "+actionList.size());
			//Loob  Data in ActionList FromDB
			List<Map<String, Object>> actionListModify = new ArrayList<>();
			
			// Start Loop ActionList Form Database
			for (Map<String, Object> action : actionList) {
				String actionName = ManagerMap.getString(action, DmaConstants.StoryBoard.Key.ACTION_NAME);
				 //Find Action in ActionList From Response 
				 Map<String, Object> actionFromResponse = getMapInListMap(actionListFromResponse,DmaConstants.StoryBoard.Key.ACTION_NAME,actionName);
				//Match ActionFromDatabase
				 if(actionFromResponse!=null) {
					 Map<String, Object> actionResponse = new LinkedHashMap<>(actionFromResponse);
					 logger.info("Action Modify Put Name  ===> "+actionName);
					 //add actionlist fields from Database to actionFromResponse
					 if(action!=null&&action.size()!=0) {
						//+++++++  Loop ActionlistFromDatabses for modify data  +++++++++++++++++++++++++++++
						 int i =0;
						 for (Entry<String, Object> entryDB : action.entrySet()) {
							 //Check Type in DB and Copy to Response
							 
							 if(entryDB.getKey().equals(DmaConstants.StoryBoard.Key.TYPE)) {
								 actionResponse.put(entryDB.getKey(), entryDB.getValue());
								 logger.info("Process ===>  Field Type in DB to Response Value : "+entryDB.getValue());
								 continue;
							 }else if(i==action.size()-1) {
								 //Remove Type from ActionList
								 String type = ManagerMap.getString(actionFromResponse, DmaConstants.StoryBoard.Key.TYPE);
									if(type!=null) {
										 logger.info("Process ===> Remove Field Type from Response Value : "+type);
										actionResponse.remove(DmaConstants.StoryBoard.Key.TYPE);
									}	 
							 }
							 i++;
						 }
						//+++++++  End Loop ActionlistFromDatabses for modify data  +++++++++++++++++++++++++++++
					 }
					//add actionlist success modify
					 actionListModify.add(actionResponse);
					
					 
					//Not Match ActionFromDatabase
				}else {
					logger.info("Action New Put Name  ===> "+actionName);
					  Map<String, Object> newAction = new LinkedHashMap<>(action);
					  newAction.put(DmaConstants.StoryBoard.Key.API_NAME, DmaConstants.StoryBoard.Value.DO_NOTHING);
					  newAction.put(DmaConstants.StoryBoard.Key.TARGET, DmaConstants.StoryBoard.Value.SELF);
					  newAction.put(DmaConstants.StoryBoard.Key.TARGET_TYPE, DmaConstants.StoryBoard.Value.EXTRA);
					  confirmDialogMap.put(DmaConstants.StoryBoard.Key.NEED_CONFIRM, false);
					  newAction.put(DmaConstants.StoryBoard.Key.CONFIRM_DIALOG, confirmDialogMap);
					  endAlertMap.put(DmaConstants.StoryBoard.Key.NEED_CONFIRM, false);
					  newAction.put(DmaConstants.StoryBoard.Key.END_ALERT, endAlertMap);
					  newAction.put(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, new ArrayList<>());
					  newAction.put(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, new ArrayList<>());
					  //Pongpat add put new field for support action descrition
					  newAction.put(DmaConstants.StoryBoard.Key.HIDE_ACTION, false);
					  newAction.put(DmaConstants.StoryBoard.Key.EDIT_VALUE, false);
					  newAction.put(DmaConstants.StoryBoard.Key.ACTION_DESCRIPTION, actionName);



					  logger.info("Action New Put  ===> "+newAction);
					actionListModify.add(newAction);
				}
				//Put Action Modify 
				
			}
			return actionListModify;
		}else {
			return null;
		}
		
	}
	private List<Map<String,Object>> modifyGlobalInputList(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalInputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, List.class);	
		List<Map<String, Object>> globalInputListFromDB	=  screebFromDB.getInputList();
		Map<String, Object> globalInputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> globalInputListModify = new ArrayList<>();
		Map<String, Object> globalInputModify = new LinkedHashMap<>();
		//Get ActionList
		if(globalInputListFromDB!=null&&globalInputListFromDB.size()>0) {
			for (Map<String, Object> globalInputDB : globalInputListFromDB) {
				String globalInputName =  ManagerMap.getString(globalInputDB, DmaConstants.StoryBoard.Key.INPUT_NAME);
				logger.info("GlobalInputName  ===> "+globalInputName);
				//Find InputListFromRes
				globalInputFromRes = getMapInListMap(globalInputListFromResponse,DmaConstants.StoryBoard.Key.INPUT_NAME,globalInputName);
				if(globalInputFromRes!=null) {
					globalInputModify = new LinkedHashMap<>(globalInputFromRes);
					for (Entry<String, Object> entry : globalInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.GlobalInputList.FIELD_POSITION)) {
							globalInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.GlobalInputList.FIELD_TYPE)) {
							globalInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.GlobalInputList.INPUT_NAME)){
							globalInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.GlobalInputList.PATH)){
							globalInputModify.put(entry.getKey(), entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add globalInputListModify  ===> "+globalInputModify);
					globalInputListModify.add(globalInputModify);
					
				}else {
					//Add Map to List output from DB
					globalInputModify = new LinkedHashMap<>(globalInputDB);
					 logger.info("Add globalInputListModify  ===> "+globalInputModify);
					globalInputListModify.add(globalInputModify);
				}
				
			}
			return globalInputListModify;
		}else {

			return null;
		}
	}
	private List<Map<String,Object>> modifyGlobalOutputList(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalOutputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, List.class);	
		List<Map<String, Object>> globalInputListFromDB	=  screebFromDB.getInputList();
		Map<String, Object> globalOutputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> globalOutputListModify = new ArrayList<>();
		Map<String, Object> globalOutputModify = new LinkedHashMap<>();
		//Get ActionList
		if(globalInputListFromDB!=null&&globalInputListFromDB.size()>0) {
			for (Map<String, Object> globalOutputDB : globalInputListFromDB) {
				String globalOutputName =  ManagerMap.getString(globalOutputDB,  DmaConstants.StoryBoard.Key.INPUT_NAME);
				logger.info("GlobalOutputName  ===> "+globalOutputName);
				//Find InputListFromDB
				globalOutputFromRes = getMapInListMap(globalOutputListFromResponse,DmaConstants.StoryBoard.Key.OUTPUT_NAME,globalOutputName);
				if(globalOutputFromRes!=null) {
					globalOutputModify = new LinkedHashMap<>(globalOutputFromRes);
					for (Entry<String, Object> entry : globalOutputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.OUTPUT_NAME)) {
							globalOutputModify.put(DmaConstants.StoryBoard.Key.OUTPUT_NAME, entry.getValue());
							//Remove inputName
							globalOutputModify.remove(entry.getKey());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.GlobalInputList.FIELD_TYPE)) {
							globalOutputModify.put(entry.getKey(), entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add globalOutputListModify  ===> "+globalOutputModify);
					globalOutputListModify.add(globalOutputModify);
				}else {
					//Add Map to List output from DB
					globalOutputModify = new LinkedHashMap<>(globalOutputDB);
					String outNameValue =  ManagerMap.getString(globalOutputModify, DmaConstants.StoryBoard.Key.INPUT_NAME);
					globalOutputModify.put(DmaConstants.StoryBoard.Key.OUTPUT_NAME, outNameValue);
					//Remove inputName
					globalOutputModify.remove(DmaConstants.StoryBoard.Key.INPUT_NAME);
					logger.info("Add globalOutputList  ===> "+globalOutputModify);
					globalOutputListModify.add(globalOutputModify);
				}
				
			}

			return globalOutputListModify;
		}else {

			return null;
		}
	}
	
	private List<Map<String,Object>> modifyDynamicInputList(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalInputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, List.class);	
		List<Map<String, Object>> dynamicInputListFromDB	=  screebFromDB.getDynamicInputList();
		Map<String, Object> globalInputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> dynamicInputListModify = new ArrayList<>();
		Map<String, Object> dynamicInputModify = new LinkedHashMap<>();
		//Get ActionList
		if(dynamicInputListFromDB!=null&&dynamicInputListFromDB.size()>0) {
			for (Map<String, Object> dynamicInputDB : dynamicInputListFromDB) {
				String dynamicInputName =  ManagerMap.getString(dynamicInputDB, DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME);
				logger.info("DynamicInputName  ===> "+dynamicInputName);
				//Find InputListFromRes
				globalInputFromRes = getMapInListMap(globalInputListFromResponse,DmaConstants.StoryBoard.Key.INPUT_NAME,dynamicInputName);
				if(globalInputFromRes!=null) {
					dynamicInputModify = new LinkedHashMap<>(globalInputFromRes);
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add DynamicInputModify  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
					
				}else {
					//Add Map to List output from DB and Default Value
					dynamicInputModify = new LinkedHashMap<>();
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.INPUT_NAME, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
				// Fix Data
					dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_POSITION, "api");
					
					
					 logger.info("Add DynamicInput  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
				}
				
			}
			return dynamicInputListModify;
		}else {

			return null;
		}
	}

	private List<Map<String,Object>> modifyDynamicOutputList(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalOutputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, List.class);	
		List<Map<String, Object>> dynamicOutputListFromDB	=  screebFromDB.getDynamicOutputList();
		Map<String, Object> globalOutputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> dynamicOutputListModify = new ArrayList<>();
		Map<String, Object> dynamicOutputModify = new LinkedHashMap<>();
		//Get ActionList
		if(dynamicOutputListFromDB!=null&&dynamicOutputListFromDB.size()>0) {
			for (Map<String, Object> dynamicOutputDB : dynamicOutputListFromDB) {
				String dynamicInputName =  ManagerMap.getString(dynamicOutputDB, DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME);
				logger.info("DynamicInputName  ===> "+dynamicInputName);
				//Find InputListFromRes
				globalOutputFromRes = getMapInListMap(globalOutputListFromResponse,DmaConstants.StoryBoard.Key.OUTPUT_NAME,dynamicInputName);
				if(globalOutputFromRes!=null) {
					dynamicOutputModify = new LinkedHashMap<>(globalOutputFromRes);
					for (Entry<String, Object> entry : dynamicOutputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add DynamicOutputModify  ===> "+dynamicOutputModify);
					 dynamicOutputListModify.add(dynamicOutputModify);
					
				}else {
					//Add Map to List output from DB and Default Value
					dynamicOutputModify = new LinkedHashMap<>();
					for (Entry<String, Object> entry : dynamicOutputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.OUTPUT_NAME, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
				// Fix Data
					dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_POSITION, "api");
					
					
					 logger.info("Add DynamicOutput  ===> "+dynamicOutputModify);
					 dynamicOutputListModify.add(dynamicOutputModify);
				}
				
			}
			return dynamicOutputListModify;
		}else {
	
			return null;
		}	
	
	}

	private List<Map<String,Object>> modifyDynamicInputTypeInput(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalInputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST, List.class);	
		List<Map<String, Object>> dynamicInputListFromDB	=  screebFromDB.getDynamicInputList();
		Map<String, Object> globalInputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> dynamicInputListModify = new ArrayList<>();
		Map<String, Object> dynamicInputModify = new LinkedHashMap<>();
		//Get ActionList
		if(dynamicInputListFromDB!=null&&dynamicInputListFromDB.size()>0) {
			for (Map<String, Object> dynamicInputDB : dynamicInputListFromDB) {
				String dynamicInputName =  ManagerMap.getString(dynamicInputDB, DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME);
				logger.info("DynamicInputName  ===> "+dynamicInputName);
				//Find InputListFromRes
				globalInputFromRes = getMapInListMap(globalInputListFromResponse,DmaConstants.StoryBoard.Key.INPUT_NAME,dynamicInputName);
				if(globalInputFromRes!=null) {
					dynamicInputModify = new LinkedHashMap<>(globalInputFromRes);
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add DynamicInputModify  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
					
				}else {
					//Add Map to List output from DB and Default Value
					dynamicInputModify = new LinkedHashMap<>();
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.INPUT_NAME, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
				// Fix Data
					dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_POSITION, "api");
					
					
					 logger.info("Add DynamicInput  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
				}
				
			}
			return dynamicInputListModify;
		}else {
	
			return null;
		}
	}

	private List<Map<String,Object>> modifyDynamicInputTypeOutput(Map<String, Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		
		List<Map<String, Object>> globalOutputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, List.class);	
		List<Map<String, Object>> dynamicOutputListFromDB	=  screebFromDB.getDynamicOutputList();
		Map<String, Object> globalOutputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> dynamicOutputListModify = new ArrayList<>();
		Map<String, Object> dynamicOutputModify = new LinkedHashMap<>();
		
		
		//Get ActionList
		if(dynamicOutputListFromDB!=null&&dynamicOutputListFromDB.size()>0) {
			for (Map<String, Object> dynamicOutputDB : dynamicOutputListFromDB) {
				String dynamicInputName =  ManagerMap.getString(dynamicOutputDB, DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME);
				logger.info("DynamicInputName  ===> "+dynamicInputName);
				//Find InputListFromRes
				globalOutputFromRes = getMapInListMap(globalOutputListFromResponse,DmaConstants.StoryBoard.Key.OUTPUT_NAME,dynamicInputName);
				if(globalOutputFromRes!=null) {
					dynamicOutputModify = new LinkedHashMap<>(globalOutputFromRes);
					for (Entry<String, Object> entry : dynamicOutputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add DynamicOutputModify  ===> "+dynamicOutputModify);
					 dynamicOutputListModify.add(dynamicOutputModify);
					
				}else {
					//Add Map to List output from DB and Default Value
					dynamicOutputModify = new LinkedHashMap<>();
					for (Entry<String, Object> entry : dynamicOutputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicOutputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.OUTPUT_NAME, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
				// Fix Data
					dynamicOutputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_POSITION, "api");
					
					
					 logger.info("Add DynamicOutput  ===> "+dynamicOutputModify);
					 dynamicOutputListModify.add(dynamicOutputModify);
				}
				
			}
			return dynamicOutputListModify;
		}else {
		
			return null;
		}
	
	}

	private List<Map<String,Object>> mapDynamicInputToDynamicOutput(Map<String,Object> screenFromResponse,AppUiDocument screebFromDB) throws Exception{
		logger.info("+++++++ MapDynamicInputToDynamicOutput  +++++++++");
		List<Map<String, Object>> globalOutputListFromResponse = ManagerMap.get(screenFromResponse, DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST, List.class);	
		List<Map<String, Object>> dynamicInputListFromDB	=  screebFromDB.getDynamicInputList();
		Map<String, Object> globalInputFromRes = new LinkedHashMap<>();
		//Output List
		List<Map<String, Object>> dynamicInputListModify = new ArrayList<>();
		Map<String, Object> dynamicInputModify = new LinkedHashMap<>();
		//Get ActionList
		if(dynamicInputListFromDB!=null&&dynamicInputListFromDB.size()>0) {
			for (Map<String, Object> dynamicInputDB : dynamicInputListFromDB) {
				String dynamicInputName =  ManagerMap.getString(dynamicInputDB, DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME);
				logger.info("DynamicInputName  ===> "+dynamicInputName);
				//Find InputListFromRes
				globalInputFromRes = getMapInListMap(globalOutputListFromResponse,DmaConstants.StoryBoard.Key.OUTPUT_NAME,dynamicInputName);
				if(globalInputFromRes!=null) {
					dynamicInputModify = new LinkedHashMap<>(globalInputFromRes);
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
					//Add Map Modify Properties
					 logger.info("Add DynamicInputModify  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
					
				}else {
					//Add Map to List output from DB and Default Value
					dynamicInputModify = new LinkedHashMap<>();
					for (Entry<String, Object> entry : dynamicInputDB.entrySet()) {
						//Modify Properties
						if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.LOCAL_PATH)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.NAME)) {
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.PATH)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.SERVICE_NAME)){
							dynamicInputModify.put(entry.getKey(), entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_NAME)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.OUTPUT_NAME, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}else if(entry.getKey().equals(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE)) {
							dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_TYPE, entry.getValue());
						}
					}
				// Fix Data
					dynamicInputModify.put(DmaConstants.StoryBoard.Key.DynamicInputList.FIELD_POSITION, "api");
					
					
					 logger.info("Add DynamicInput  ===> "+dynamicInputModify);
					 dynamicInputListModify.add(dynamicInputModify);
					 logger.info("+++++++ End MapDynamicInputToDynamicOutput  +++++++++");
				}
				
			}
			return dynamicInputListModify;
		}else {
	
			return null;
		}
	
	}	

	private  Map<String,Object> getMapInListMap(List<Map<String, Object>> mapList,String key,String name) {
		if(mapList!=null) {
			for (Map<String, Object> map : mapList) {
				String keyMap;
				try {
					keyMap = ManagerMap.getString(map, key);
					if(keyMap.equals(name)) {
						logger.info("Process ===> Get Map Name  :{}", key);
						return map;
					}
				} catch (Exception e) {
					return null;
					
				}
				
			}
		}else {
			return null;
		}
		return null;
	}
	
}
