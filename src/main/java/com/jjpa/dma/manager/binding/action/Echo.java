package com.jjpa.dma.manager.binding.action;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Echo
 */
public class Echo implements ActionBinding {

    private final Logger logger = LoggerFactory.getLogger(Echo.class);

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> screenConfig,
			Map<String, Object> eventMap) throws Exception {
		logger.info("Binding Echo in Action");
		DocumentContext ctx = JsonPath.parse(document.getNativeJson().get("data"));

		logger.warn("Do nothing wait for implement");
		logger.info("entity : " +  entity);
		logger.info("screenConfig : " + screenConfig);
		logger.info("eventMap : " + eventMap);
		logger.info("document.getActionChainType : "+document.getActionChainType());
		logger.info("document.getNativeJson : "+document.getNativeJson());

		logger.info("ctx=" + ctx.jsonString());
		return ctx;
	}
    
}