package com.jjpa.common.data.service;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.ResponseCode;
import com.jjpa.common.data.service.createpath.PathCreator;
import com.jjpa.common.exception.ServiceException;

/**
 * 
 * @author adirak-pc
 *
 */
public class ServiceData implements Serializable {
	
	public static class HeaderKey{
		public static String deviceKey="deviceKey";
		public static String applicationId="applicationId";
		
		public static String remoteAddr="remoteAddr";
		public static String remoteHost="remoteHost";
		public static String remotePort="remotePort";
		public static String remoteUser="remoteUser";
		/**
		 * 
		 * userId is username
		 * 
		 */
		public static String userId="userId";
	}

	private static final long serialVersionUID = 1L;

	public static final String ROOT_NAME = "ROOT";
	public static final String ROOT_PATH = "$";

	/** Data document */
	private DocumentContext jsonDocument;
	private Map<String, Object> header = new LinkedHashMap<>();

	/**
	 * Constructor of this data
	 */
	public ServiceData() {
		this("{}");
	}

	public ServiceData(String jsonString) {
		jsonDocument = JsonPath.parse(jsonString);
	}
	
	public ServiceData(Map<String, Object> data){
		jsonDocument = JsonPath.parse(data);
	}
	
	/**
	 * validate the path and change ROOT to $
	 * 
	 * @param path
	 * @return
	 * @throws ServiceException
	 */
	private String filterPath(String path) throws ServiceException {
		if (path == null) {
			ServiceException ex = new ServiceException("path to query is null");
			ex.setErrorCode(ResponseCode.DATA_REQUIRE);
			throw ex;
		} else {
			boolean hasRoot = path.startsWith(ROOT_NAME);
			if (hasRoot) {
				StringBuilder sbPath = new StringBuilder(path);
				sbPath.delete(0, ROOT_NAME.length());
				sbPath.insert(0, ROOT_PATH);
				return sbPath.toString();
			}
		}
		return path;
	}

	/**
	 * Get the value by path, if it's not exist path, it will throw exception
	 * 
	 * @param path
	 * @return value
	 * @throws ServiceException
	 */
	public Object getValue(String path) throws ServiceException {
		String jPath = filterPath(path);
		Object value = getValue(jPath, false);
		return value;
	}

	/**
	 * Get the value by path, if it's not exist path, it will throw exception
	 * (if throwError = true)
	 * 
	 * @param path
	 * @param throwError
	 * @return value
	 * @throws ServiceException
	 */
	public Object getValue(String path, boolean throwError) throws ServiceException {
		String jPath = filterPath(path);
		Object value = null;
		try {
			value = jsonDocument.read(jPath);
		} catch (Exception e) {
			if (throwError) {
				ServiceException ex = new ServiceException(e);
				ex.setErrorCode(ResponseCode.DATA_INVALID);
				throw ex;
			}
		}
		return value;
	}

	/**
	 * Set value by path, and auto create no exist path
	 * 
	 * @param path
	 * @param value
	 * @throws ServiceException
	 */
	public void setValue(String path, Object value) throws ServiceException {
		setValue(path, value, true);
	}

	/**
	 * Set value to path and check auto create path, if autoPath=true it will
	 * create new path automatic
	 * 
	 * @param path
	 * @param value
	 * @param autoPath
	 * @throws ServiceException
	 */
	public void setValue(String path, Object value, boolean forceReplace) throws ServiceException {
		String jpath = filterPath(path);
		PathCreator pc = new PathCreator(this, jpath, value);
		pc.create(forceReplace);
	}

	/**
	 * Remove path
	 * @param path
	 * @throws ServiceException
	 */
	public void removeValue(String path) throws ServiceException {
		try {
			String jpath = filterPath(path);
			PathCreator pc = new PathCreator(this, jpath);
			pc.remove();
		} catch (Exception e) {
			ServiceException se = new ServiceException(e);
			se.setErrorCode(ResponseCode.DATA_INVALID);
			throw se;
		}
	}

	/**
	 * Remove paths
	 * @param paths
	 * @throws ServiceException
	 */
	public void removeValues(List<String> paths) throws ServiceException {
		for (int i = 0; i < paths.size(); i++) {
			removeValue(paths.get(i));
		}
	}

	/** 
	 * Get string data from document
	 * @return string
	 */
	public String toJsonString() {
		return jsonDocument.jsonString();
	}

	/**
	 * Get Json Document
	 * @return
	 */
	public DocumentContext getJsonDocument() {
		return jsonDocument;
	}
	
	public Object getHeaderValue(String name){
		return header.get(name);
	}
	
	public void setHeaderValue(String name, Object value){
		header.put(name, value);
	}
	
	public Map<String, Object> getHeader(){
		return header;
	}
	
	public <T> T getData(){
		return jsonDocument.read(ROOT_PATH);
	}

	public static void main(String[] args) throws ServiceException {
		ServiceData sd = new ServiceData();
		System.out.println(sd.toJsonString());
		System.out.println("--------------------------");

		// sd.setValue("$.a", "A");
		// sd.setValue("$.a.x", "X");
		sd.setValue("$.b.c.d[2]", "DD");
		sd.setValue("ROOT.a[0]", "A");
		sd.setValue("ROOT.a[1]", "AA");
		sd.setValue("ROOT.a[2]", "AAA");

		// sd.setValue("ROOT.a[0].x", "XXXX");
		//sd.removeValue("ROOT.a[0]");

		System.out.println("--------------------------");
		System.out.println(sd.toJsonString());

	}

}
