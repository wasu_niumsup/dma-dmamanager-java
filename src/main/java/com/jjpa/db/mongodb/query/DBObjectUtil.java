package com.jjpa.db.mongodb.query;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jjpa.db.mongodb.MongoDbAccessor.Dir;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.SelectOption;
import com.jjpa.db.mongodb.model.WhereOption;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import blueprint.util.StringUtil;


public class DBObjectUtil {

	private static Logger logger = Logger.getLogger(DBObjectUtil.class);
	
	public static void bindingProject(List<DBObject> dbObjectList, List<SelectOption> selectOptList) {
		
		/* binding project from selectOptList to dbObjectList */
		if(selectOptList != null){
			
			Map<String, Object> projectMap = new LinkedHashMap<>();
			
			String path;
			int inclusion;
			for(SelectOption selectOpt : selectOptList){
				
				path = selectOpt.getPath();
				inclusion = selectOpt.getInclusion();
				
				projectMap.put(path, inclusion);
			}
			
			if(projectMap.size()!=0){
				DBObject project = new BasicDBObject("$project", new BasicDBObject(projectMap));
				dbObjectList.add(project);
				logger.info(project.toMap());
			}
		}
	}
	
	public static void bindingMatch(List<DBObject> dbObjectList, List<WhereOption> whereOptList) {
		
		/* binding match from whereOptList to dbObjectList */
		if(whereOptList != null){
			
			BasicDBList and = new BasicDBList();
			
			String path, value;
			Object start, end;
			boolean isNotEqual, isBetween, isSearch, isSensitive;
			for(WhereOption whereOpt : whereOptList){
				
				path = whereOpt.getPath();
				value = whereOpt.getValue();
				isBetween = whereOpt.isBetween();
				isNotEqual = whereOpt.isNotEqual();
				
				if(isNotEqual){
					
					and.add(new BasicDBObject(path, new BasicDBObject("$ne", value)));
					
				} else if(isBetween){
					
					start = whereOpt.getStart();
					end = whereOpt.getEnd();
					
					Map<String, Object> between = new LinkedHashMap<>();
					between.put("$gte", Long.parseLong(String.valueOf(start)));
					between.put("$lte", Long.parseLong(String.valueOf(end)));
					
					and.add(new BasicDBObject(path, new BasicDBObject(between)));
					
				}else {
					
					isSearch = whereOpt.isSearch();
					isSensitive = whereOpt.isSensitive();
					
					if(!StringUtil.isBlank(value)){
						if(isSearch){
							if(isSensitive){
								and.add(new BasicDBObject(path, new BasicDBObject("$regex", value)));
							}else{
								and.add(new BasicDBObject(path, new BasicDBObject("$regex", value).append("$options", "i")));
							}
						}else {
							if(isSensitive){
								and.add(new BasicDBObject(path, value));
							}else{
								and.add(new BasicDBObject(path, new BasicDBObject("$regex", "^"+value+"$").append("$options", "i")));
							}
						}
					}
				}
			}
			
			if(and.size()!=0){
				DBObject match = new BasicDBObject("$match", new BasicDBObject("$and", and));
				dbObjectList.add(match);
				logger.info(match.toMap());
			}
		}
	
	}
	
	public static void bindingGridPagging(List<DBObject> dbObjectList, GridPagging gridPagging) {
		
		/* binding sort, skip, limit from gridPagging to dbObjectList */
		if(gridPagging !=null){
			if(!StringUtil.isBlank(gridPagging.getSort())){
				
				int dir;
				if(StringUtil.isBlank(gridPagging.getDir()) || gridPagging.getDir().equalsIgnoreCase(Dir.ASC)){
					dir = 1;
				}else {
					dir = -1;
				}
				DBObject sort = new BasicDBObject("$sort", new BasicDBObject(gridPagging.getSort(), dir));
				dbObjectList.add(sort);
				logger.info(sort.toMap());
			}
			
			if(!StringUtil.isBlank(gridPagging.getStart())){
				DBObject skip = new BasicDBObject("$skip", Integer.valueOf(gridPagging.getStart()));
				dbObjectList.add(skip);
				logger.info(skip.toMap());
			}
			
			if(!StringUtil.isBlank(gridPagging.getLimit())){
				DBObject limit = new BasicDBObject("$limit", Integer.valueOf(gridPagging.getLimit()));
				dbObjectList.add(limit);
				logger.info(limit.toMap());
			}
		}
	}
	
}
