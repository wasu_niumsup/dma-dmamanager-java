package com.jjpa.dma.manager.processor.restore;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerStore;

public class ActionChainTemplateRestore {

	private static final Logger logger = LoggerFactory.getLogger(ActionChainTemplateRestore.class);
	
	protected ActionChainTemplateAccessor actionChainTemplateAccessor;
	
	public ActionChainTemplateRestore() {
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
	}
	
	
	public List<String> process(String filePath, String user) throws Exception {
		logger.info("Process ===> ActionChainTemplateRestore");
		logger.info("filePath="+ filePath);
		
		List<String> actionChainTemplateList = new ArrayList<>();
		
		File file = new File(filePath);
		if(file.exists()){
			
			DMAActionChainTemplate document;
			Map<String, Object> jsonMap;
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
					
					actionChainTemplateList.add(fileEntry.getName());
					
					jsonMap = ManagerStore.readJsonMapFromFile(fileEntry, true);
					
					/* 16/05/2561 new lastUpdate */
					jsonMap.put(MongoConstants.Path.LAST_UPDATE, new Date());
					
					document = new DMAActionChainTemplate(jsonMap);
					
					actionChainTemplateAccessor.saveActionChainTemplate(document, user);
					logger.info("restore ActionChainTemplateType="+ document.getActionType());
				}
			}
		}
		
		logger.info("End Process ===> ActionChainTemplateRestore");
		return actionChainTemplateList;
	}
}
