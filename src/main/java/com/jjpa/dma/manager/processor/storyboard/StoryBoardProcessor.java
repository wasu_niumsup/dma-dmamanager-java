package com.jjpa.dma.manager.processor.storyboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class StoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(StoryBoardProcessor.class);
	
	protected StoryBoardAccessor storyBoardAccessor;
	
	public StoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.debug("Do StoryBoardProcessor");
		
		String functionName = (String)input.getValue("functionName");
		logger.debug("Do function : {}", functionName);
		if("/storyboard/listStory".equals(functionName)){
			return storyBoardAccessor.listStory(input);
		}else if("/storyboard/deleteStory".equals(functionName)){
			storyBoardAccessor.deleteStory(input);
		}else if("/storyboard/saveStory".equals(functionName)){
			return saveStory(input);
		}else if("/storyboard/findStoryByName".equals(functionName)){
			String storyName = (String)input.getValue("storyName");
			return storyBoardAccessor.findStoryByName(storyName);
		}else if("/storyboard/listBackType".equals(functionName)){
			return listBackType();
		}else if("/storyboard/setReady".equals(functionName)){
			return setReady(input);
		}else if("/storyboard/listStoryBoardWithApi".equals(functionName)){
			return storyBoardAccessor.listStoryBoardWithApi(input);
		}
		return input.getData();
	}
	
	@SuppressWarnings("unchecked")
	private Object saveStory(ServiceData input) throws Exception {
		StoryBoardDocument oldDoc = storyBoardAccessor.findStoryByName((String)input.getValue("storyName"));
		StoryBoardDocument document = new StoryBoardDocument();
		if((input.getValue("id")!=null && !"".equals(((String)input.getValue("id")).trim()))){
			String id = (String)input.getValue("id");
			if(oldDoc!=null && !id.equals(oldDoc.getId())){
				throw new Exception("Name duplicated");
			}
			document.setId(id);
		}else if(oldDoc != null){
			throw new Exception("Name duplicated");
		}
		document.setStatus(DmaManagerConstant.STATUS_DRAFT);
		document.setStoryName((String)input.getValue("storyName"));
		document.setRemark((String)input.getValue("remark"));
		document.setFirstPage((String)input.getValue("firstPage"));
		document.setHomePage((String)input.getValue("homePage"));
		document.setStoryBoard((List<Map<String, Object>>)input.getValue("storyBoard"));
		document.setLastUpdate(new Date());
		String user = (String)input.getValue("user");
		return storyBoardAccessor.saveStoryBoard(document, user);
	}
	
	private Map<String, Object> listBackType() throws Exception {
		Map<String, Object> backTypeResponse = new HashMap<String, Object>();
		List<Map<String, Object>> backTypeList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.BACK_TYPE){
			Map<String, Object> backMap = new HashMap<String, Object>();
			backMap.put("backType", s);
			backTypeList.add(backMap);
		}
		backTypeResponse.put("backTypeList", backTypeList);
		return backTypeResponse;
	}
	
	private Map<String,Object> setReady(ServiceData input) throws Exception {
		StoryBoardDocument document = storyBoardAccessor.findStoryByName((String)input.getValue("storyName"));
		document.setStatus(DmaManagerConstant.STATUS_READY);
		String user = (String)input.getValue("user");
		
		storyBoardAccessor.saveStoryBoard(document, user);
		
		Map<String,Object> appUi = new HashMap<String,Object>();
		appUi.put("storyName", document.getStoryName());
		appUi.put("remark", document.getRemark());
		appUi.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		appUi.put("lastPublish", document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
		appUi.put("status", document.getStatus());
		
		return appUi;
	}
	
}
