package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.PublishEntityScreenListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.PublishEntityScreenListDocument;

import servicegateway.utils.ManagerArrayList;

public class PublishEntityScreenListAccessorImpl implements PublishEntityScreenListAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	
	protected PublishEntityScreenListAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	private List<PublishEntityScreenListDocument> findMongoByName(String name) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where(MongoConstants.Simple.NAME).is(name));
		return mongoDbAccessor.findByCriteria(findByName, PublishEntityScreenListDocument.class);
	}
	
	
	
	@Override
	public List<Map<String, Object>> findByName(String name) throws Exception {
		
		/* find data from mongo */
		List<PublishEntityScreenListDocument> resultList = findMongoByName(name);

		
		/* 
		 * convert mongoData to listData 
		 * */
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(resultList != null && resultList.size() != 0){
			for(PublishEntityScreenListDocument resultMap : resultList){
				list.add(resultMap.getData());
			}
		}
		
		
		return list;
	}


	@Override
	public void deleteByName(String name, String user) throws Exception {

		/* find data from mongo */
		List<PublishEntityScreenListDocument> resultList = findMongoByName(name);
		
		
		if(resultList != null && resultList.size() != 0){
			for(PublishEntityScreenListDocument resultMap : resultList){
				mongoDbAccessor.delete(resultMap, user);
			}
		}
		
	}

	
	@Override
	public void saveByName(String name, Object dataListObj, String user) throws Exception {
		
		/* delete */
		deleteByName(name, user);
		
		
		/* 
		 * loop save
		 * */
		PublishEntityScreenListDocument document;
		
		List<Map<String, Object>> dataList = ManagerArrayList.convertObjectToListMap(dataListObj);
		
		if(dataList != null && dataList.size() != 0){
			
			for(Map<String, Object> dataMap : (List<Map<String, Object>>)dataList){
				
				document = new PublishEntityScreenListDocument(); 
				document.setName(name);
				document.setData(dataMap);
				
				mongoDbAccessor.save(document, user);
			}
		}
		
	}

}
