//package com.jjpa.dma.manager.processor.binding;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.jayway.jsonpath.DocumentContext;
//import com.jayway.jsonpath.JsonPath;
//import com.jjpa.common.DmaConstants;
//import com.jjpa.common.util.ConfigurationUtils;
//import com.jjpa.common.util.DataUtils;
//import com.jjpa.db.mongodb.accessor.AppUiAccessor;
//import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
//import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
//import com.jjpa.dma.manager.model.reference.Reference;
//import com.jjpa.dma.manager.processor.actionchain.GenerateActionChainProcessor;
//import com.jjpa.dma.manager.processor.appui.preview.DirectPreviewProcessor;
//import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;
//import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToScreenProcessor;
//import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToStoryBoardProcessor;
//import com.jjpa.dma.manager.processor.reference.version.binding.BindingVersionReferenceProcessor;
//import com.jjpa.spring.SpringApplicationContext;
//
///* 16/03/2561 edit by Akanit */
//@SuppressWarnings("unchecked")
//public class GenerateScreenListAndBindingReferenceProcessor {
//
//	private static final Logger logger = LoggerFactory.getLogger(GenerateScreenListAndBindingReferenceProcessor.class);
//	
//	protected AppUiAccessor appUiAccessor;
//	
//	public GenerateScreenListAndBindingReferenceProcessor() {
//		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
//	}
//	
//	public List<Map<String, Object>> process(StoryBoardDocument storyBoardDoc, String user) throws Exception {
//		logger.info("process : generate ScreenList and binding reference.");
//		
//		
//		/* initial : create reference.inUsed, reference.required */
//		Reference reference = new InitialReferenceProcessor().process(storyBoardDoc.getReference());
//		storyBoardDoc.setReference(reference);
//		
//		
//		/* update version reference */
//		new BindingVersionReferenceProcessor().process(storyBoardDoc.getReference());
//		
//		
//		String entityName = "tempApplication";
//
//		String source, uiJson;
//		AppUiDocument appUiDoc;
//		Map<String, Object> screen, dataBinding;
//		List<Map<String, Object>> actionChainList, screenList = new ArrayList<>();
//		
//		for(Map<String, Object> storyBoard : storyBoardDoc.getStoryBoard()){
//			
//			
//			/* get source, backType : old system 'source' is 'uiName' */
//			source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
//			
//			/* find appUi by name : mongoDB */
//			appUiDoc = appUiAccessor.findAppUiByName(source);
//			
//			
//			/* get templateJson */	
//			dataBinding = getJsonTemplate(appUiDoc);
//			logger.info("dataBinding from appUiDoc="+dataBinding);
//			
//			
//			
//			/* 
//			 * Binding 
//			 * */
//			
//			/* binding local */
//			new BindingLocalProcessor().process(dataBinding, storyBoard, appUiDoc);
//			uiJson = JsonPath.parse(dataBinding).jsonString();
//			logger.info("BindingLocalProcessor dataBinding="+uiJson);
//			
//			/* set uiJson to storyBoard row. */
//			storyBoard.put(DmaConstants.StoryBoard.Key.UI_JSON, uiJson);
//			
//			
//			actionChainList = new GenerateActionChainProcessor().process(dataBinding, entityName, storyBoard, appUiDoc); 
//			logger.info("GenerateActionChainProcessor actionChainList="+actionChainList);
//						
//			actionChainList = new GenerateDynamicInputActionProcessor().process(appUiDoc, actionChainList, storyBoard);
//			logger.info("GenerateDynamicActionProcessor actionChainList="+actionChainList);
//			
//			new BindingActionProcessor().process(dataBinding, actionChainList);
//			logger.info("BindingActionProcessor dataBinding="+dataBinding);
//					
//			new BindingBackActionProcessor().process(dataBinding, entityName, storyBoard, storyBoardDoc);
//			logger.info("BindingBackActionProcessor dataBinding="+dataBinding);
//			
//			new BindingBaseConfiguration().process(dataBinding, entityName, null,"");
//			
//			
//			
//			/* 
//			 * Reference 
//			 * */
//			
//			/* update inUsed reference to appUI(screen) */
//			new BindingInUsedReferenceToScreenProcessor().process(appUiDoc, storyBoardDoc);
//			appUiAccessor.saveUi(appUiDoc, user);
//			
//			/* update required reference to storyBoard */
//			new BindingRequiredReferenceToStoryBoardProcessor().process(storyBoardDoc, storyBoard, appUiDoc, actionChainList);
//			
//			
//			
//			/* 
//			 * create screen 
//			 * */
//			screen = new LinkedHashMap<>();
//			screen.put(DmaConstants.AppUI.Key.SOURCE, source);
//			screen.put(DmaConstants.AppUI.Key.DYNAMIC_API, appUiDoc.getDynamicApi());
////			screen.put(DmaConstants.AppUI.Key.DYNAMIC_API_ID, configServiceGateway.get("uuid"));
//			screen.put(DmaConstants.AppUI.Key.DYNAMIC_DATA_BINDING, appUiDoc.getDynamicDataBinding());
//			screen.put(DmaConstants.AppUI.Key.TEMPLATE_TYPE, appUiDoc.getTemplateType());
//			screen.put(DmaConstants.AppUI.Key.UI_JSON, JsonPath.parse(dataBinding).jsonString());
//			screen.put(DmaConstants.AppUI.Key.ACTION_LIST, storyBoard.get(DmaConstants.StoryBoard.Key.ACTION_LIST));
//			screen.put(DmaConstants.AppUI.Key.GLOBAL_INPUT_LIST, storyBoard.get(DmaConstants.StoryBoard.Key.GLOBAL_INPUT_LIST));
//			screen.put(DmaConstants.AppUI.Key.GLOBAL_OUTPUT_LIST, storyBoard.get(DmaConstants.StoryBoard.Key.GLOBAL_OUTPUT_LIST));
//			
//			screenList.add(screen);
//		}
//		return screenList;
//	}
//	
//	private Map<String, Object> getJsonTemplate(AppUiDocument appUiDoc) throws Exception {
//		
//		logger.info("Start addNoCacheToImageInScreen");
//		appUiDoc = addNoCacheToImageInScreen(appUiDoc);
//		logger.info("End addNoCacheToImageInScreen");
//
//		/* get templateJson */
//		DocumentContext ctx = JsonPath.parse(appUiDoc.getUiJson());
//
//		/* create dataBinding from templateJson (old system : dataBinding is templateJson) */
//		Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());
//		
//		return dataBinding;
//	}
//
//	@SuppressWarnings("unused")
//	private AppUiDocument addNoCacheToImageInScreen(AppUiDocument appUiDoc) throws Exception{
//		List<Map<String, Object>> bindingList = (List<Map<String, Object>>) appUiDoc.getUiConfigData()
//		.get("binding");
//
//		String internalDomain = ConfigurationUtils.getPublishUrl().toLowerCase();
//		String prefix = "&dmaNoCache=";
//		String addNoCache = "&dmaNoCache=" +String.format("%013d",new Date().getTime());
//	
//		if (bindingList != null) {
//			for (Map<String, Object> item : bindingList) {
//				String componentType = DataUtils.getStringValue(item, DmaConstants.AppUI.Key.COMPONENT_TYPE,
//						"string");
//
//				if ("image".equalsIgnoreCase(componentType)) {
//					String value = DataUtils.getStringValue(item, "value", "");
//					String jsonPath = DataUtils.getStringValue(item, "path", null);
//					boolean defaultBinding = DataUtils.getBooleanValue(item, "default", true);
//
//					String valueFromDefault = "";
//
//					String url = defaultBinding ? valueFromDefault : value;
//					
//					logger.info("before change url : "+url);
//					/**
//					 * 
//					 * Ex. "{{$get._dma_param_internal_domain}}/ims/axxx.jpg";
//					 */
//					if (url.trim().indexOf("{{") == 0) {
//						if( url.lastIndexOf(prefix) > 0 ){
//							int startPrefix = url.lastIndexOf(prefix);
//							String oldCache = url.substring(url.lastIndexOf(prefix),  startPrefix + prefix.length()+13); 
//							System.out.println("oldCache="+oldCache);
//							System.out.println("newCache="+addNoCache);
//							url = url.replace(oldCache, addNoCache);
//							
//						}else{
//							url+= addNoCache;
//						}
//						
//					}
//					logger.info("after change url : "+url);
//					if(!defaultBinding){
//						item.put("value", url);
//					}
//					logger.info("item : " + item);
//				}
//
//			}
//		}
//
//
//		Map<String,Object> oldUiConfigData = appUiDoc.getUiConfigData();
//		oldUiConfigData.put("binding", bindingList);
//
//		appUiDoc.setUiConfigData(oldUiConfigData);
//
//		return appUiDoc = new DirectPreviewProcessor().reBindingJson(appUiDoc);
//	}
//}
