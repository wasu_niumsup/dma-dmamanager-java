package com.jjpa.dma.manager.processor.authen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.UserModel;
import com.jjpa.db.mongodb.accessor.UserAccessor;
import com.jjpa.db.mongodb.document.dma.manager.UserDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import com.jjpa.web.servlet.UserSession;

public class AuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private UserAccessor getUserAccessor() {
		return SpringApplicationContext.getBean("UserAccessor");
	}
	
	private UserSession getUserSession(){
		return SpringApplicationContext.getBean("UserSession");
	}

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		String functionName = (String)input.getValue("functionName");
		if("/authen/login".equals(functionName)){
			return login(input);
		}else if("/authen/getMenu".equals(functionName)){
			return getMenu(input);
		}else if("/authen/changePassword".equals(functionName)){
			return changePassword(input);
		}else if("/authen/getLoginUser".equals(functionName)){
			return getLoginUser(input);
		}else if("/authen/logout".equals(functionName)){
			return logout(input);
		}
		return input.getData();
	}

	public Map<String, Object> login(ServiceData input) throws Exception {
		UserModel model = new UserModel();
		model.setClientIp((String)input.getValue("clientIp"));
		model.setServerIp((String)input.getValue("serverIp"));
		model.setUsername((String)input.getValue("username"));
		
		UserAccessor accessor = getUserAccessor(); 
		UserDocument user = accessor.authen(model.getUsername(), (String)input.getValue("password"));
		if(user != null){
			BeanUtils.copyProperties(user, model);

			UserSession session = getUserSession();
			session.setUser(model);
			
			Map<String, Object> output = new HashMap<>();
			output.put("username", user.getUsername());
			return output;
		}else{
			throw new Exception("Invalid Username or Password");
		}
	}
	
	public Map<String, Object> changePassword(ServiceData input) throws Exception {

		String username = (String)input.getValue("username");
		String password = (String)input.getValue("password");
		String newPassword = (String)input.getValue("newPassword");

		UserAccessor accessor = getUserAccessor();
		UserDocument user = accessor.changePassword(username, password, newPassword);
		if(user != null){
			Map<String, Object> output = new HashMap<>();
			output.put("username", user.getUsername());
			return output;
		}else{
			throw new Exception("Invalid Username or Password");
		}
	}

	public Map<String, Object> getLoginUser(ServiceData input) throws Exception {
		Map<String, Object> output = null;
		UserSession session = getUserSession();
		UserModel model = session.getUser();
		if(model != null){
			output = new HashMap<>();
			output.put("username", model.getUsername());
		}
		return output;
	}
	
	public Map<String, Object> getMenu(ServiceData input) throws Exception {
		Map<String, Object> menuResponse = new HashMap<String, Object>();
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> menu1 = new HashMap<String, Object>();
		menu1.put("menuSeq", 1);
		menu1.put("menuLink", "../AppUI");
		menu1.put("menuActive", true);
		menu1.put("menuId", "idAppUI");
		menu1.put("menuName", "APP UI");
		menu1.put("menuImage", "../../resources/images/menu/menu_appUi.png");
		
		//Map<String, Object> menu2 = new HashMap<String, Object>();
		//menu2.put("menuSeq", 2);
		//menu2.put("menuLink", "../ApiConfigure");
		//menu2.put("menuActive", true);
		//menu2.put("menuId", "idApiConfigure");
		//menu2.put("menuName", "API Configure");
		//menu2.put("menuImage", "../../resources/images/menu/menu_apiConfigure.png");
		
		Map<String, Object> menu3 = new HashMap<String, Object>();
		menu3.put("menuSeq", 3);
		menu3.put("menuLink", "../StoryBoardConfigure");
		menu3.put("menuActive", true);
		menu3.put("menuId", "idStoryBoardConfigure");
		menu3.put("menuName", "Storyboard<br>Configure");
		menu3.put("menuImage", "../../resources/images/menu/menu_storyboardConfigure.png");
		
		Map<String, Object> menu4 = new HashMap<String, Object>();
		menu4.put("menuSeq", 4);
		menu4.put("menuLink", "../EntityManagement");
		menu4.put("menuActive", true);
		menu4.put("menuId", "idEntityManagement");
		menu4.put("menuName", "Entity<br>Management");
		menu4.put("menuImage", "../../resources/images/menu/menu_entityManagement.png");
		
		Map<String, Object> menu5 = new HashMap<String, Object>();
		menu5.put("menuSeq", 5);
		menu5.put("menuLink", "../BackupRestore");
		menu5.put("menuActive", true);
		menu5.put("menuId", "idBackupRestore");
		menu5.put("menuName", "Backup/<br>Restore");
		menu5.put("menuImage", "../../resources/images/menu/menu_backupRestore.png");
		
		menuList.add(menu1);
		//menuList.add(menu2);
		menuList.add(menu3);
		menuList.add(menu4);
		menuList.add(menu5);
		
		menuResponse.put("menuLists", menuList);
		
		return menuResponse;
	}

	public Map<String, Object> logout(ServiceData input) throws Exception {
		UserSession session = getUserSession();
		session.setUser(null);
		return null;
	}
	
	public Map<String, Object> save(ServiceData input) throws Exception {
		UserDocument user = new UserDocument();
		String username = (String)input.getValue("username");
		user.setPassword((String)input.getValue("password"));
		user.setUsername(username);
		UserAccessor accessor = getUserAccessor();
		user = accessor.save(user, username);
		Map<String, Object> output = null;
		if(user != null){
			output = new HashMap<>();
			output.put("username", user.getUsername());
		}
		return output;
	}
}
