package com.jjpa.dma.manager.processor.storyboard;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import blueprint.util.StringUtil;

public class FixReferenceProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(FixReferenceProcessor.class);
		
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> FixReferenceProcessor : {}", input.toJsonString());
		
		
		String alertValidateTopic = "Please input {'topic': xx} -> xx is ALL, storyboard, screen, template ";
		
		String topic = (String)input.getValue("topic");
		if(StringUtil.isBlank(topic)){
			throw new Exception(alertValidateTopic);
		}else if(!"ALL".equals(topic) && !"storyboard".equals(topic) && !"screen".equals(topic) && !"template".equals(topic)){
			throw new Exception(alertValidateTopic);
		}
		
		
		if("ALL".equals(topic)){
			new com.jjpa.dma.manager.processor.reference.FixReferenceProcessor().fixRefAll();
		}else if("storyboard".equals(topic)){
			new com.jjpa.dma.manager.processor.reference.FixReferenceProcessor().fixRefStoryboard();
		}else if("screen".equals(topic)){
			new com.jjpa.dma.manager.processor.reference.FixReferenceProcessor().fixRefScreen();
		}else if("template".equals(topic)){
			new com.jjpa.dma.manager.processor.reference.FixReferenceProcessor().fixRefTemplate();
		}
		

		Map<String, Object> output = new LinkedHashMap<>();
		output.put("success", "true");
		logger.info("End Procces ===> FixReferenceProcessor");
		return output;
	}

}
