package com.jjpa.dma.manager.processor.binding;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.util.DataUtils;

/**
 * BindingTriggerInitialScreenProcessor
 */
@SuppressWarnings("rawtypes")
public class BindingTriggerInitialScreenProcessor {
    private static final Logger logger = LoggerFactory.getLogger(BindingTriggerInitialScreenProcessor.class);

	public DocumentContext process(DocumentContext masterTemplate) throws Exception {
        logger.info("###Process ===> BindingTriggerInitialScreenProcessor");
        Map<String, Object> actions = masterTemplate.read("$.$jason.head.actions");
        for (Map.Entry<String, Object> entry : actions.entrySet()) {
            String key = entry.getKey();

            if (key.contains("$load")) {
                logger.info("Prepare clean all action after $render :" + key);
                DocumentContext load = JsonPath.parse((Map) actions.get(key));
                List<Map<String, Object>> allType = load.read("$..[?(@.type)]");

                if (allType != null && allType.size() > 0) {
                    logger.info("Do nothing : allType.size() " + allType.size());

                    for (int i = 0; i < allType.size(); i++) {
                        Map<String, Object> item = allType.get(i);
                        String type = DataUtils.getStringValue(item, "type", "");
                        if ("$render".equalsIgnoreCase(type)) {
                            logger.info("Found $render : " + item);
                            
                            item.remove("type");
                            item.remove("success");
                            item.remove("error");
                            item.put("trigger", GenerateInitialScreenActionProcessor.Key.actionName);

                            logger.info("put trigger : item " + item);
                        }



                    }
                }

            }
        }

        return masterTemplate;

    }
}