package com.jjpa.dma.manager.binding.action;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public class ConfirmDialog implements ActionBinding{

	private final Logger logger = LoggerFactory.getLogger(ConfirmDialog.class);
	
	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) throws Exception {
		logger.info("Binding confirm dialog in Action");		
		DocumentContext ctx =  JsonPath.parse(document.getNativeJson().get("data"));
		
		Map<String, Object> confirmDialog = DataUtils.getMapFromObject(eventMap, DmaConstants.StoryBoard.Key.CONFIRM_DIALOG);
		
		if(DataUtils.getBooleanValue(confirmDialog.get(DmaConstants.StoryBoard.Key.NEED_CONFIRM))){
			
			bindingData(ctx, document, confirmDialog);
		}
		
		return ctx;
	}
	
	private void bindingData(DocumentContext ctx, DMAActionChainDocument document, Map<String, Object> confirmDialog){
		
		List<Map<String, Object>> bindingList = document.getBinding();
		for(Map<String, Object> binding : bindingList){
			
			String key = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			
			if(confirmDialog.containsKey(key)){
				
				ctx.set(DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH)), confirmDialog.get(key));
			}
		}
	}

}
