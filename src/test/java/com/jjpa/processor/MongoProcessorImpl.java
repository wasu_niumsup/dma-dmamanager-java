package com.jjpa.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class MongoProcessorImpl {
	
	@SuppressWarnings("unchecked")
	public static void main(String[] arg){
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> item1 = new HashMap<String, Object>();
		item1.put("type", "book");
		item1.put("value", "Lodos");
		
		Map<String, Object> item2 = new HashMap<String, Object>();
		item2.put("type", "book");
		item2.put("value", "Slayer");
		
		Map<String, Object> item3 = new HashMap<String, Object>();
		item3.put("type", "vdo");
		item3.put("value", "Lords");
		
		list.add(item1);
		list.add(item2);
		list.add(item3);
		
		Map<String, Object> data = new HashMap<String, Object>();

		data.put("data", list);
		
		DocumentContext ctx = JsonPath.parse(data);
		
		System.out.println(ctx.jsonString());
		
		List<String> items = (List<String>)ctx.read("$.data[?(@.type == 'vdo')].value");
		
		if(!items.isEmpty()){
			System.out.println(items.get(0));
		}else{
			System.out.println("No item");
		}
		
	}
	
}
