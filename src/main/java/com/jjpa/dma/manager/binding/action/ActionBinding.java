package com.jjpa.dma.manager.binding.action;

import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public interface ActionBinding {

	/**
	 * 
	 * document.getNativeJson() // Jason Template
	 * [Parrent] story_board_config.storyboard[?] is screen ui config from DMA Manager
	 * [Child]  eventMap is story_board_config.storyboard[?].actionList from  Story board DMA Manager
	 */
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> screenConfig, Map<String, Object> eventMap) throws Exception;
	
}
