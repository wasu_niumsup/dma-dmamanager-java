package com.jjpa.dma.manager.processor.storyboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import blueprint.util.StringUtil;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.ScreenNativeInputTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unchecked")
public class ListScreenNativeInputTypeProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(ListScreenInputTypeProcessor.class);

	protected ScreenNativeInputTypeAccessor screenNativeInputTypeAccessor;
	protected AppUiAccessor appUiAccessor;

	public ListScreenNativeInputTypeProcessor() {
		this.screenNativeInputTypeAccessor = SpringApplicationContext.getBean("ScreenNativeInputTypeAccessor");
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
	}

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {

		logger.info("Process ===> ListScreenNativeInputTypeProcessor NEW2");
		logger.debug("Process ===> ListScreenNativeInputTypeProcessor : {}", input.toJsonString());
		Map<String, Object> storyBoardMap = input.getData();

		/**
		 * 
		 * Get data from database
		 */
		Map<String, List<Map<String, Object>>> nativeInput = screenNativeInputTypeAccessor
				.listScreenNativeInputType(input);

		List<Map<String, Object>> storyBoardGlobalList = nativeInput.get("globalList");

		List<Map<String, Object>> storyBoardList = (List<Map<String, Object>>) storyBoardMap.get("storyBoard");

		String source = DataUtils.getStringValue(storyBoardMap, "source", null);

		Map<String, Object> currentStoryBoardScreen = null;// new LinkedHashMap<>();
		for (Map<String, Object> storyBoardScreen : storyBoardList) {
//			List<Map<String, Object>> screenGlobalList = (List<Map<String, Object>>) storyBoardScreen
//					.get("globalOutputList");
			/**
			 * Tanawat W.
			 * [2018-02-23] Add feature Select Use from Screen
			 * 
			 */
			if (source != null && source.equals(DataUtils.getStringValue(storyBoardScreen, "source"))) {
				logger.info("Found screen : " + source);
				currentStoryBoardScreen = storyBoardScreen;

				break;
			}

		}

		/**
		 * Tanawat W.
		 * [2018-02-23] Add feature Select Use from Screen
		 * 
		 * appUiList[?].globalInputList : {
		 * 	fieldPosition : "Screen",
		 *  fieldType : "String",
		 *  inputName : username",
		 *  path : "$.$jason.head.actions.$load.options.username"
		 * 
		 * }
		 * 
		 * appUiList[?].uiName : "N_Login"
		 * 
		 */
		// currentStoryBoardScreen = null; // comment waiting for  Add feature Select Use from Screen
		logger.info("currentStoryBoardScreen : " + currentStoryBoardScreen);
		if (currentStoryBoardScreen != null) {
			storyBoardGlobalList.addAll(  extractNativeVariable(currentStoryBoardScreen) );
		}

		Map<String, Object> globalListMap = new LinkedHashMap<>();
		globalListMap.put("globalList", storyBoardGlobalList);

		logger.info("Process ===> ListScreenNativeInputTypeProcessor Output : {}", globalListMap);

		return globalListMap;

	}

	public List<Map<String, Object>>  extractNativeVariable(Map<String, Object> currentStoryBoardScreen)
			throws Exception {
		List<Map<String, Object>> storyBoardGlobalList = new ArrayList<>();

		String uiName = DataUtils.getStringValue(currentStoryBoardScreen, "source");
		List<Map<String, Object>> screenInput = (List<Map<String, Object>>) currentStoryBoardScreen
				.get("globalInputList");

		if (screenInput != null) {
			for (Map<String, Object> i : screenInput) {
				logger.info("screenInput  i: " + i);
				if (i.containsKey("path")) {
					String path = DataUtils.getStringValue(i, "path", "");
					String inputName = DataUtils.getStringValue(i, "inputName", "");

					boolean condition = false;

					String pathValueName = "";

					/**
					 * Tanawat W.
					 * [2018-12-18] DMA 5.0
					 */

					if (!StringUtil.isBlank(path) 
						&&  ( path.contains("$.$jason.head.actions.$load.options") 
								||
							  path.contains("$.$jason.head.actions.$load.options.mockup") // Bypass DMA5.0
							)
					
					) {
						pathValueName = path.substring(path.lastIndexOf(".")).replace(".", "");

						logger.info("pathValueName  : " + pathValueName + " , inputName :" + inputName);
						boolean bypassDMA5 = path.contains("$.$jason.head.actions.$load.options.mockup");
						if (!StringUtil.isBlank(inputName) 
							&& (
									inputName.equals(pathValueName)
									|| 
									bypassDMA5
								)
							)
						{
							condition =  true;
						}

					} else {
						logger.warn("Skip because not in condition  inputName:["+inputName+"] not equal last of path:["+path+"]");
						continue;
					}
					logger.info("condition : " + condition);
					if (condition) {
						Map<String, Object> globalMap = createNativeInput(uiName,path,inputName,pathValueName,(String)i.get("fieldType"),(String)i.get("dataStructure"),JasonPathConstants.UiJson.Key.GET  );//(uiName, i, path, inputName, pathValueName,"$get");
														//createNativeInput(String uiName, String path, String inputName,	String pathValueName,String fieldType,   String dataStructure,String jasonGetValueFrom) 

						storyBoardGlobalList.add(globalMap);
					}

				}

			}


			/**
			 * 
			 * Add feature to detail list
			 */

			AppUiDocument screenDoc = appUiAccessor.findAppUiByName(uiName);
			if( DmaConstants.AppUI.Value.DYNAMIC_LIST.equalsIgnoreCase( (screenDoc.getTemplateType() ) )){
				logger.info("case template =" +screenDoc.getTemplateType() );
				if( screenDoc.getDynamicOutputList() != null) {
					for (Map<String,Object> dynamicInput : screenDoc.getDynamicOutputList()) {
						String name = DataUtils.getStringValue(dynamicInput, DmaConstants.AppUI.Key.DynamicOutputList.FIELD_NAME, null);
						if( !StringUtil.isBlank(name) ){
							Map<String, Object> globalMap = createNativeInput(uiName,name,name,name,"string",null,JasonPathConstants.UiJson.Key.SELECTED_DETAILS_ITEM);
							storyBoardGlobalList.add(globalMap);
						}
					}
				}
			}else if (DmaConstants.AppUI.Value.DYNAMIC_INPUT.equalsIgnoreCase( (screenDoc.getTemplateType() ) )){
				logger.info("case template =" +screenDoc.getTemplateType() );
				if( screenDoc.getDynamicInputList() != null) {
					for (Map<String,Object> dynamicInput : screenDoc.getDynamicInputList()) {
						String name = DataUtils.getStringValue(dynamicInput, DmaConstants.AppUI.Key.DynamicOutputList.FIELD_NAME, null);
						if( !StringUtil.isBlank(name) ){
							Map<String, Object> globalMap = createNativeInput(uiName,name,name,name,"string",null,JasonPathConstants.UiJson.Key.GET);
							storyBoardGlobalList.add(globalMap);
						}
					}
				}
			}
			
		}

		return storyBoardGlobalList;
	}

	/**
	 * @param uiName
	 */
	private Map<String, Object> createNativeInput(String uiName, String path, String inputName,
			String pathValueName,String fieldType,   String dataStructure,String jasonGetValueFrom) {
		Map<String, Object> globalMap = new LinkedHashMap<>();
							globalMap.put("globalName", inputName);
							globalMap.put("fieldType",  fieldType);//screenInput.get("fieldType"));
							globalMap.put("source", DmaConstants.StoryBoard.Value.NATIVE);
							globalMap.put("api", "");
							globalMap.put("action", "");
							globalMap.put("dataStructure", dataStructure);//screenInput.get("dataStructure"));
							globalMap.put("getValueFrom", jasonGetValueFrom);
							globalMap.put("screen", uiName);
							globalMap.put("path", path);
							globalMap.put("pathValueName", pathValueName);
							logger.info("put : " + globalMap);
		return globalMap;
	}

}