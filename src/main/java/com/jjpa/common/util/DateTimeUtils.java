package com.jjpa.common.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtils {
	
	public final static String dateTimeFormat = "yyyy/MM/dd HH:mm:ss";

	public static String getCurrentDateTime() {
		return getCurrentDateTime(dateTimeFormat);
	}
	
	public static String getCurrentDateTime(String pattern) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}
	
	public static String getFormattedDate(Date date){
		return new SimpleDateFormat(dateTimeFormat).format(date);
	}
	
    public static String getFormattedDate(Long timestamp, String pattern) {
        Date date = new Date(timestamp);
        SimpleDateFormat fullDateFormat = new SimpleDateFormat(pattern);
        return fullDateFormat.format(date);
    }
}
