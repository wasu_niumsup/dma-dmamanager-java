package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jjpa.dma.manager.model.reference.Reference;

import servicegateway.utils.ManagerMap;

@Document(collection = "dma_template")
public class DMATemplate extends BaseDMADocument{

	private static final long serialVersionUID = 1L;
	
	@Indexed(unique=true)
	private String templateName;
	private String infoUrl;
	
	private List<Map<String,Object>> actionList;
	private List<Map<String,Object>> inputList;
	private String templateType;
	private String dynamicApi;
	private List<Map<String,Object>> dynamicDataBinding;
	
	private Reference refference;
	private String versionTemplate;
	private String versionEngine;
	private String versionEngineAndroidMobile;
	private String versionEngineIOSMobile;
	
	public DMATemplate() {
	}
	
	public DMATemplate(Map<String, Object> dataMap) throws Exception {
		
		
		/* ************************** set extends *********************************** */
		this.setId(ManagerMap.getString(dataMap, "id"));
		this.setCreateBy(ManagerMap.getString(dataMap, "createBy"));
		this.setLastUpdateBy(ManagerMap.getString(dataMap, "lastUpdateBy"));
		this.setDmaManagerVersion(ManagerMap.getString(dataMap, "dmaManagerVersion"));
		this.setNativeJson(ManagerMap.get(dataMap, "nativeJson", Map.class));
		this.setBinding(ManagerMap.get(dataMap, "binding", List.class));
		
		/* set create date */
		Long createDate = ManagerMap.getLong(dataMap, "createDate");
		if(createDate!=null){
			this.setCreateDate(new Date(createDate));
		}
		
		/* set lastUpdate date */
		Long lastUpdateDate = ManagerMap.getLong(dataMap, "lastUpdateDate");
		if(lastUpdateDate!=null){
			this.setLastUpdateDate(new Date(lastUpdateDate));
		}
		
		/* set lastUpdate */
		Long lastUpdate = ManagerMap.getLong(dataMap, "lastUpdate");
		if(lastUpdate!=null){
			this.setLastUpdate(new Date(lastUpdate));
		}else {
			Object lastUpdateObj = dataMap.get("lastUpdate");
			if(lastUpdateObj instanceof Date){
				this.setLastUpdate( (Date)lastUpdateObj );
			}
		}
		/* *************************************************************************** */
		
		
		/* ****************************** set this *********************************** */
		this.setTemplateName(ManagerMap.getString(dataMap, "templateName"));
		this.setInfoUrl(ManagerMap.getString(dataMap, "infoUrl"));
		this.setActionList(ManagerMap.get(dataMap, "actionList", List.class));
		this.setInputList(ManagerMap.get(dataMap, "inputList", List.class));
		this.setTemplateType(ManagerMap.getString(dataMap, "templateType"));
		this.setDynamicApi(ManagerMap.getString(dataMap, "dynamicApi"));
		this.setDynamicDataBinding(ManagerMap.get(dataMap, "dynamicDataBinding", List.class));
		
		Map<String, Object> referenceMap = ManagerMap.get(dataMap, "refference", Map.class);
		Reference reference = new ObjectMapper().convertValue(referenceMap, Reference.class);
		
		this.setReference(reference);
		this.setVersionTemplate(ManagerMap.getString(dataMap, "versionTemplate"));
		this.setVersionEngine(ManagerMap.getString(dataMap, "versionEngine"));
		this.setVersionEngineAndroidMobile(ManagerMap.getString(dataMap, "versionEngineAndroidMobile"));
		this.setVersionEngineIOSMobile(ManagerMap.getString(dataMap, "versionEngineIOSMobile"));
		/* *************************************************************************** */
		
	}
	
	public String getTemplateName() {
		return templateName;
	}
	
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getInfoUrl() {
		return infoUrl;
	}

	public void setInfoUrl(String infoUrl) {
		this.infoUrl = infoUrl;
	}

	public String getVersionTemplate() {
		return versionTemplate;
	}

	public void setVersionTemplate(String versionTemplate) {
		this.versionTemplate = versionTemplate;
	}

	public String getVersionEngine() {
		return versionEngine;
	}

	public void setVersionEngine(String versionEngine) {
		this.versionEngine = versionEngine;
	}

	public String getVersionEngineAndroidMobile() {
		return versionEngineAndroidMobile;
	}

	public void setVersionEngineAndroidMobile(String versionEngineAndroidMobile) {
		this.versionEngineAndroidMobile = versionEngineAndroidMobile;
	}

	public String getVersionEngineIOSMobile() {
		return versionEngineIOSMobile;
	}

	public void setVersionEngineIOSMobile(String versionEngineIOSMobile) {
		this.versionEngineIOSMobile = versionEngineIOSMobile;
	}

	public List<Map<String, Object>> getActionList() {
		return actionList;
	}

	public void setActionList(List<Map<String, Object>> actionList) {
		this.actionList = actionList;
	}

	public List<Map<String, Object>> getInputList() {
		return inputList;
	}

	public void setInputList(List<Map<String, Object>> inputList) {
		this.inputList = inputList;
	}

	public String getDynamicApi() {
		return dynamicApi;
	}

	public void setDynamicApi(String dynamicApi) {
		this.dynamicApi = dynamicApi;
	}

	public List<Map<String, Object>> getDynamicDataBinding() {
		return dynamicDataBinding;
	}

	public void setDynamicDataBinding(List<Map<String, Object>> dynamicDataBinding) {
		this.dynamicDataBinding = dynamicDataBinding;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public Reference getReference() {
		return refference;
	}

	public void setReference(Reference reference) {
		this.refference = reference;
	}
	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate().getTime());
		map.put("lastUpdateDate", this.getLastUpdateDate().getTime());
		map.put("dmaManagerVersion", this.getDmaManagerVersion());
		
		map.put("nativeJson", this.getNativeJson());
		map.put("binding", this.getBinding());
		try{
			map.put("lastUpdate", this.getLastUpdate().getTime());
		}catch(Exception e){
			map.put("lastUpdate", "");
		}
		
		map.put("templateName", this.getTemplateName());
		map.put("infoUrl", this.getInfoUrl());
		map.put("actionList", this.getActionList());
		map.put("inputList", this.getInputList());
		map.put("templateType", this.getTemplateType());
		map.put("dynamicApi", this.getDynamicApi());
		map.put("dynamicDataBinding", this.getDynamicDataBinding());
		map.put("refference", this.getReference());
		map.put("versionTemplate", this.getVersionTemplate());
		map.put("versionEngine", this.getVersionEngine());
		map.put("versionEngineAndroidMobile", this.getVersionEngineAndroidMobile());
		map.put("versionEngineIOSMobile", this.getVersionEngineIOSMobile());
		
		return map;
	}

}
