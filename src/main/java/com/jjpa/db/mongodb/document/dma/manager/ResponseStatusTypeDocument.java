package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.mapping.Document;

import com.jjpa.db.mongodb.document.BaseDocument;

@Document(collection = "response_status_type")
public class ResponseStatusTypeDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	private String labelName;
	private String value;
	
	public String getLabelName() {
		return labelName;
	}
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
