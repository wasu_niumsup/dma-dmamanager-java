package com.jjpa.dma.manager.processor.storyboard;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.dma.manager.processor.reference.DeleteReferenceProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
public class DeleteStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteStoryBoardProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	private String user;
	
	public DeleteStoryBoardProcessor() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> DeleteEntityProcessor");
		logger.debug("Process ===> DeleteEntityProcessor : {}", input.toJsonString());

		user = (String)input.getValue("user");
		
		Map<String,Object> validate =  (Map<String, Object>) new ValidateDeleteStoryBoard().process(request, input);
//		boolean isValid = DataUtils.getBooleanValue(validate, ValidateDeleteStoryBoard.Key.resultSuccess, false);
		List<Map<String,Object>> screenList = (List<Map<String,Object>>) validate.get(ValidateDeleteStoryBoard.Key.resultData);

		/* 20/03/2561 add by Akanit */
		clearInUsedReferenceToScreen(screenList);
		
		storyBoardAccessor.deleteStory(input);
		return storyBoardAccessor.listStory(input);
		
	}
	
	
	/* 20/03/2561 add by Akanit */
	private void clearInUsedReferenceToScreen(List<Map<String,Object>> screenList) throws Exception{
		
		if(screenList != null && screenList.size() != 0){
			String screenName;
			AppUiDocument appUiDoc;
			
			for(Map<String, Object> screen : screenList){
				
				screenName = ManagerMap.getString(screen, "uiName");
				appUiDoc = appUiAccessor.findAppUiByName(screenName);
				
				if(appUiDoc!=null){
					logger.info("#clear InUsed Reference to screen="+ screenName);
					new DeleteReferenceProcessor().clearInUsed(appUiDoc.getReference());
					
					appUiAccessor.saveUi(appUiDoc, user);
				}else {
					logger.info("#screen="+ screenName+" not found. then skip clear InUsed Reference.");
				}
				
			}
		}
	}
}
