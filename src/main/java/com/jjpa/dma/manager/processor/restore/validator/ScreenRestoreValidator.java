package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

public class ScreenRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(ScreenRestoreValidator.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public ScreenRestoreValidator() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	public Map<String, Object> process(List<Map<String, Object>> screenRequiredList) throws Exception {
		logger.info("Process ===> ScreenRestoreValidator");

		boolean validationStatus = true;
    	@SuppressWarnings("unused")
		String version_restore, version_old, name;
    	
    	for(Map<String, Object> screenRequired : screenRequiredList){
    		
    		version_restore = ManagerMap.getString(screenRequired, ReferenceConstants.DMA.VERSION);
    		name = ManagerMap.getString(screenRequired, ReferenceConstants.DMA.NAME);
    		
    		AppUiDocument doc = appUiAccessor.findAppUiByName(name);
    		
    		
    		/* validate add status */
    		if(doc==null){
    			logger.info("#not found old Screen="+ name);
    			logger.info("#update action=ADD");
    			logger.info("#update {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
				
    			screenRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
    			continue;
    			
    		}else {
    			logger.info("#found old Screen="+ name);
    			logger.info("#update {}={}", BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.DUPLICATE);
				logger.info("#update {}={}", BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
				
    			validationStatus = false;
    			screenRequired.put(BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.DUPLICATE);
    			screenRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
    			
				
    			/* validate version : now not validate version but In the future want to validate version */
//    			try {
//        			version_old = doc.getReference().getVersion();
//    			} catch (Exception e) {
//    				logger.info("#Screen="+ name + "not have reference.");
//    				logger.info("#update action=UPDATE");
//        			logger.info("#update status=true");
//    				
//    				screenRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
//    				continue;
//    			}
//        		
//        		
//        		if(version_restore == version_old){
//        			logger.info("#Screen="+ name + " can restore.");
//        			logger.info("#update action=UPDATE");
//        			logger.info("#update status=true");
//    				
//    				screenRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
//        			
//        		}else{
//        			logger.info("#Screen="+ name + " can not restore.");
//        			logger.info("#update detail=version");
//        			logger.info("#update status=false");
//    				
//        			validationStatus = false;    
//				    screenRequired.put(BackupConstants.Validator.Key.DETAIL, BackupConstants.Validator.Detail.VERSION_CONFLICT);
//					screenRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.FALSE);
//        		}
        		
    		}
    		
    	}
    	
    	/* mapping data to UI */
    	Map<String, Object> output = new LinkedHashMap<>();
    	output.put("summaryValidation", validationStatus);
    	output.put("data", screenRequiredList);
    	
    	return output;
	}
}
