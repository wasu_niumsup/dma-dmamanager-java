package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dma_component")
public class DMAComponent extends BaseDMADocument{

	private static final long serialVersionUID = 1L;

	@Indexed(unique=true)
	private String componentName;
	
	public String getComponentName() {
		return componentName;
	}
	
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	
}
