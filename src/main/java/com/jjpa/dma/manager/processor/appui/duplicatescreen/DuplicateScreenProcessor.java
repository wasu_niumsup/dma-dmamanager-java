package com.jjpa.dma.manager.processor.appui.duplicatescreen;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToTemplateProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;


public class DuplicateScreenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

    private static final Logger logger = LoggerFactory.getLogger(DuplicateScreenProcessor.class);

    protected AppUiAccessor appUiAccessor;
    protected TemplateAccessor templateAccessor;
    private String user;

    public DuplicateScreenProcessor() {
        this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
        this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
    }

    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {

        logger.info("Process ===> DuplicateScreenProcessor");
        logger.debug("Process ===> DuplicateScreenProcessor : {}", input.toJsonString());

        return duplicateAppUi(input);

    }

    private Object duplicateAppUi(ServiceData input) throws Exception {

        user = (String) input.getValue("user");

        String uiName = (String) input.getValue("uiName");
        String newUiName = (String) input.getValue("newUiName");
        if (DmaManagerConstant.BACK_TYPE.contains(newUiName.toLowerCase().trim())) {
            throw new Exception(
                    "New Screen name must not be reserved word. (\"firstpage\", \"home\", \"stack\", \"none\")");
        } else {
            AppUiDocument oldDoc = appUiAccessor.findAppUiByName(uiName);

            if (oldDoc != null) {
                //check newUiName in Collection
                AppUiDocument newDoc = appUiAccessor.findAppUiByName(newUiName);

                if (newDoc != null) {
                    throw new Exception("New name Duplicated");
                }

                AppUiDocument appUiDoc = new AppUiDocument();

                appUiDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
                appUiDoc.setUiName((String) input.getValue("newUiName"));
                appUiDoc.setRemark(oldDoc.getRemark());
                appUiDoc.setUiConfigData(oldDoc.getUiConfigData());
                appUiDoc.setDynamicApi(oldDoc.getDynamicApi());
                appUiDoc.setTemplateType(oldDoc.getTemplateType());
                appUiDoc.setDynamicInputList(oldDoc.getDynamicInputList());
                appUiDoc.setDynamicOutputList(oldDoc.getDynamicOutputList());

                /* 25/03/2562 add by Akanit : requirement from siam */
    			try{
    				appUiDoc.setUrlScreenShot((String) input.getValue(DmaConstants.AppUI.Key.URL_SCREEN_SHOT));
    			} catch (Exception e){
    				logger.info("not found '"+ DmaConstants.AppUI.Key.URL_SCREEN_SHOT +"' then setUrlScreenShot = ''");
    				appUiDoc.setUrlScreenShot("");
    			}

    			/* 25/03/2562 () comment by Akanit : Why i comment this log? because data of log is saved to database, you can see data at database mongo. */
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getUiConfigData " + appUiDoc.getUiConfigData());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getUiJson " + appUiDoc.getUiJson());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getUiName " + appUiDoc.getUiName());
//
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getRemark " + appUiDoc.getRemark());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getDynamicApi " + appUiDoc.getDynamicApi());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getTemplateType " + appUiDoc.getTemplateType());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getDynamicInputList " + appUiDoc.getDynamicInputList());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getDynamicOutputList " + appUiDoc.getDynamicOutputList());
//
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.setActionList " + appUiDoc.getActionList());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.setInputList " + appUiDoc.getInputList());
//                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getDynamicDataBinding " + appUiDoc.getDynamicDataBinding());


                Reference newReferenceData = new Reference();

                newReferenceData.setVersion(oldDoc.getReference().getVersion());
                newReferenceData.setRequired(oldDoc.getReference().getRequired());

                appUiDoc.setReference(newReferenceData);

                
                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getReference " + appUiDoc.getReference());

                
                /* 09/10/2561 add by Akanit : binding reference to templateRequired. */
                String templateRequired = new GetReferenceProcessor().getDmaTemplateRequired(appUiDoc.getReference());
                if(templateRequired==null){
                	logger.info("Screen "+ appUiDoc.getUiName() +" not found template "+ templateRequired);
                }
                
                DMATemplate templateDoc = templateAccessor.findTemplateByName(templateRequired);
                
                new BindingInUsedReferenceToTemplateProcessor().process(templateDoc, appUiDoc);
                
                templateAccessor.saveTemplate(templateDoc, user);
                
                
                
                /* 12/03/2561 add by Akanit : get template config */
//                String templateName = (String) appUiDoc.getUiConfigData().get("template");
              //  DMATemplate template = templateAccessor.findTemplateByName(templateName);

                /* 12/03/2561 add by Akanit */
                appUiDoc.setDynamicDataBinding(oldDoc.getDynamicDataBinding());

                
                /* 04/01/2561 add by Akanit : get jsonTemplate */
                appUiDoc.setUiJson(oldDoc.getUiJson());

                
                appUiDoc.setActionList(oldDoc.getActionList());
                
                
                /* 17/05/2561 add by Akanit */
                appUiDoc.setLastUpdate(new Date());
                
                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getDynamicDataBinding " + appUiDoc.getDynamicDataBinding());
                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getUiJson " + appUiDoc.getUiJson());
                logger.info("Process ===> DuplicateScreenProcessor duplicateAppUi appUiDoc.getActionList " + appUiDoc.getActionList());

                return appUiAccessor.saveUi(appUiDoc, user);
            }else{
                throw new Exception("Old screen not found");
            }

        }
    }

//
//    /* 16/03/2561 add by Akanit */
//    private void clearInUsedReferenceToOldTemplate(String templateName_old, String templateName_new, String screenName)
//            throws Exception {
//
//        if (templateName_old != null && !templateName_old.equals(templateName_new)) {
//            logger.info("#found old template, then clear InUsed Reference of old template.");
//
//            DMATemplate template_old = templateAccessor.findTemplateByName(templateName_old);
//
//            new DeleteReferenceProcessor().clearInUsedByDmaScreen(template_old.getReference(), screenName);
//
//            templateAccessor.saveTemplate(template_old, user);
//        }
//    }
//
//    /* 04/01/2561 add by Akanit */
//    /* 12/03/2561 edit by Akanit */
//    private Map<String, Object> getJsonTemplate(DMATemplate template) throws Exception {
//
//        /* get templateJson */
//        DocumentContext ctx = JsonPath.parse(template.getNativeJson().get("data"));
//
//        /* create dataBinding from templateJson (old system : dataBinding is templateJson) */
//        Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());
//
//        return dataBinding;
//    }
//
//    /* 04/01/2561 add by Akanit */
//    private String generateUiJson(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception {
//        logger.info("#do generateUiJson");
//
//        logger.info("#call BindingComponentProcessor()");
//        new BindingComponentProcessor().process(dataBinding, appUiDoc);
//        String uiJson = JsonPath.parse(dataBinding).jsonString();
//        //		logger.info("#BindingComponentProcessor dataBinding="+uiJson);
//
//        return uiJson;
//    }

}
