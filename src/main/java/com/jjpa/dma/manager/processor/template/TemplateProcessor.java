package com.jjpa.dma.manager.processor.template;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class TemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(TemplateProcessor.class);
	
	protected TemplateAccessor templateAccessor;
	
	public TemplateProcessor() {
		this.templateAccessor =  SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.debug("Do TemplateProcessor");
		
		String functionName = (String)input.getValue("functionName");
		logger.debug("Do function : {}", functionName);
		if("/ui/listTemplate".equals(functionName)){
			return templateAccessor.listTemplateName();
		}else if("/ui/findTemplateByName".equals(functionName)){
			String templateName = (String)input.getValue("templateName");
			return templateAccessor.findTemplateByName(templateName);
		}else if("/template/saveTemplate".equals(functionName)){
			return saveTemplate(input);
		}else if("/template/getTemplateConfig".equals(functionName)){
			return getTemplateConfig(input);
		}else{
			throw new Exception("No Api for function : " + functionName);
		}
	}
	
	private Map<String, Object> getTemplateConfig(ServiceData input) throws Exception{
		String templateName = (String)input.getValue("templateName");
		DMATemplate template = templateAccessor.findTemplateByName(templateName);
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("templateName", template.getTemplateName());
		output.put("infoUrl", template.getInfoUrl());
		output.put("binding", JsonPath.parse(template.getBinding()).jsonString());
		output.put("nativeJson", template.getNativeJson().get("data"));
		return output;
	}
	
	@SuppressWarnings("unchecked")
	private Object saveTemplate(ServiceData input) throws Exception {
		String user = (String)input.getValue("user");
		DMATemplate template = new DMATemplate();
		template.setTemplateName((String)input.getValue("templateName"));
		template.setBinding((List<Map<String,Object>>)input.getValue("binding"));
		template.setInfoUrl((String)input.getValue("infoUrl"));
		DocumentContext ctx = JsonPath.parse(input.getValue("nativeJson"));
		String nativeJson = (String)ctx.jsonString();
		Map<String,String> data = new HashMap<String, String>();
		data.put("data", nativeJson);
		template.setNativeJson(data);
		
		/* 16/05/2561 add by Akanit new lastUpdate*/
		template.setLastUpdate(new Date());
		
		return templateAccessor.saveTemplate(template,user);
	}

}
