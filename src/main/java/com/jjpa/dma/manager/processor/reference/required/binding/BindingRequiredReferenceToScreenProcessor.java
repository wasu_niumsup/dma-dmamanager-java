package com.jjpa.dma.manager.processor.reference.required.binding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;

import blueprint.util.StringUtil;

/* 16/03/2561 add by Akanit */
public class BindingRequiredReferenceToScreenProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingRequiredReferenceToScreenProcessor.class);
	
	public void process(AppUiDocument appUiDoc, DMATemplate template) throws Exception {
		logger.info("process : binding required reference to Screen");

		bindingDmaTemplateRequiredReference(appUiDoc, template);
		bindingSgServiceRequiredReference(appUiDoc);
	}
	
	private void bindingDmaTemplateRequiredReference(AppUiDocument appUiDoc, DMATemplate template) throws Exception {
		
		if(!StringUtil.isBlank(template.getTemplateName())){
			logger.info("set Screen '{}' required dmaTemplate '{}'", appUiDoc.getUiName(), template.getTemplateName());
			
			/* get dmaScreenRequiredList of storyBoard */
			List<Map<String, Object>> dmaTemplateRequiredList_appUi = new GetReferenceProcessor().getDmaTemplateRequireds(appUiDoc.getReference());
			if(dmaTemplateRequiredList_appUi==null){
				dmaTemplateRequiredList_appUi = new ArrayList<>();
				appUiDoc.setReference(new InitialReferenceProcessor().process(appUiDoc.getReference()));
			}
			appUiDoc.getReference().getRequired().setDmaTemplateRequired(dmaTemplateRequiredList_appUi);
			
			
			Map<String, Object> dmaTemplateRequired = new LinkedHashMap<>();
			dmaTemplateRequired.put(ReferenceConstants.DMA.VERSION, template.getVersionTemplate());
			dmaTemplateRequired.put(ReferenceConstants.DMA.CURRENT_VERSION, template.getVersionTemplate());
			dmaTemplateRequired.put(ReferenceConstants.DMA.NAME, template.getTemplateName());
			dmaTemplateRequired.put(ReferenceConstants.DMA.TYPE, template.getTemplateType());
			
			dmaTemplateRequiredList_appUi.add(dmaTemplateRequired);
		}

	}
	
	private void bindingSgServiceRequiredReference(AppUiDocument appUiDoc) throws Exception {

		if(!StringUtil.isBlank(appUiDoc.getDynamicApi())){
			logger.info("set Screen '{}' required serviceGateway '{}'", appUiDoc.getUiName(), appUiDoc.getDynamicApi());
			
			/* get dmaScreenRequiredList of storyBoard */
			List<Map<String, Object>> SgServiceRequiredList_appUi = new GetReferenceProcessor().getSgServiceRequireds(appUiDoc.getReference());
			if(SgServiceRequiredList_appUi==null){
				SgServiceRequiredList_appUi = new ArrayList<>();
				appUiDoc.setReference(new InitialReferenceProcessor().process(appUiDoc.getReference()));
			}
			appUiDoc.getReference().getRequired().setSgServiceRequired(SgServiceRequiredList_appUi);
			
			
			Map<String, Object> SgServiceRequired = new LinkedHashMap<>();
	//		SgServiceRequired.put(ReferenceConstants.SG.UUID, uuid);
			SgServiceRequired.put(ReferenceConstants.SG.SERVICE_NAME, appUiDoc.getDynamicApi());
			
			SgServiceRequiredList_appUi.add(SgServiceRequired);
		}
	}
	
	
	
	public void updateCurrentVersionToDmaTemplateRequiredReference(AppUiDocument appUiDoc, DMATemplate template) throws Exception {
		
		/* get dmaScreenRequiredList of storyBoard */
		List<Map<String, Object>> dmaTemplateRequiredList_appUi = new GetReferenceProcessor().getDmaTemplateRequireds(appUiDoc.getReference());
		
		Map<String, Object> dmaTemplateRequired;
		if(dmaTemplateRequiredList_appUi==null){
			
			dmaTemplateRequired = new LinkedHashMap<>();
			dmaTemplateRequired.put(ReferenceConstants.DMA.VERSION, template.getVersionTemplate());
			dmaTemplateRequired.put(ReferenceConstants.DMA.CURRENT_VERSION, template.getVersionTemplate());
			dmaTemplateRequired.put(ReferenceConstants.DMA.NAME, template.getTemplateName());
			dmaTemplateRequired.put(ReferenceConstants.DMA.TYPE, template.getTemplateType());
			
		}else {
			
			dmaTemplateRequired = new LinkedHashMap<>(dmaTemplateRequiredList_appUi.get(0));
			dmaTemplateRequired.put(ReferenceConstants.DMA.CURRENT_VERSION, template.getVersionTemplate());
		}
		
		dmaTemplateRequiredList_appUi = new ArrayList<>();
		dmaTemplateRequiredList_appUi.add(dmaTemplateRequired);
		
		appUiDoc.setReference(new InitialReferenceProcessor().process(appUiDoc.getReference()));
		appUiDoc.getReference().getRequired().setDmaTemplateRequired(dmaTemplateRequiredList_appUi);
	}


}
