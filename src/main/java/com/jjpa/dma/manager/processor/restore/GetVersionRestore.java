package com.jjpa.dma.manager.processor.restore;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.spring.SpringApplicationContext;

public class GetVersionRestore {

	private static final Logger logger = LoggerFactory.getLogger(GetVersionRestore.class);
	
	protected StoryBoardAccessor storyBoardAccessor;
	
	public GetVersionRestore() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	
	public String process(String filePath, String user) throws Exception {
		logger.info("Process ===> GetVersionRestore");
		logger.info("filePath="+ filePath);
		
		String version = "0.0.0.0";
    	
		File file = new File(filePath);
		if(file.exists()){
			
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
					
					version = fileEntry.getName();
					
			    	/* break loop : because entryZip have one file */
			    	break;
				}
			}
		}
		
		logger.info("End Process ===> GetVersionRestore");
		return version;
	}
	
}
