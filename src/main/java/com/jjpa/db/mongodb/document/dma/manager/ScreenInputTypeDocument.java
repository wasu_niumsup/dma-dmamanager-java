package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.jjpa.db.mongodb.document.BaseDocument;

@Document(collection = "screen_input_type")
public class ScreenInputTypeDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	@Indexed
	private String name;
	private String api;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the api
	 */
	public String getApi() {
		return api;
	}

	/**
	 * @param api the api to set
	 */
	public void setApi(String api) {
		this.api = api;
	}

}
