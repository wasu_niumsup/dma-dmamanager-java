package com.jjpa.dma.manager.processor.authen;



import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import blueprint.util.StringUtil;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.UserAccessor;
import com.jjpa.db.mongodb.document.dma.manager.UserDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
/**
 * RegisterUserProcessor
 */
public class RegisterUserProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(RegisterUserProcessor.class);
	UserAccessor userAccessor;
	public RegisterUserProcessor() {
		this.userAccessor = SpringApplicationContext.getBean("UserAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        Map<String,Object> body = input.getData();
        logger.info("step 0 : body =" + body);
        String username = (String)input.getValue("username");
        String password = (String)input.getValue("password");
        
        if(StringUtil.isBlank(username)){
            throw new ValidationException("This username can't be blank.");
        }

        UserDocument user = userAccessor.findOne(username);
        if(user != null){
            throw new ValidationException("This username is already used. Please change username");
        }
        UserDocument saveUser = new UserDocument();
        saveUser.setUsername(username);
        saveUser.setPassword(password);
        saveUser.setRole("user");
        return userAccessor.save(saveUser, "admin");
    }

    
}