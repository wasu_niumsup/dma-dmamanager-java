package com.jjpa.db.mongodb.accessor;

import com.jjpa.db.mongodb.document.dma.manager.UserDocument;

public interface UserAccessor {

	public UserDocument save(UserDocument user, String username) throws Exception;
	public UserDocument authen(String username, String password) throws Exception;
	public UserDocument changePassword(String username, String password, String newPassword) throws Exception;
	public UserDocument findOne(String username) throws Exception;

}