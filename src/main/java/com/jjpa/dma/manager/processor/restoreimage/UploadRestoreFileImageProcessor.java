package com.jjpa.dma.manager.processor.restoreimage;

import java.io.File;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.dma.manager.model.reference.RequiredReference;
import com.jjpa.dma.manager.processor.restore.reference.GenerateImageReferenceRestore;
import com.jjpa.dma.manager.processor.restore.validator.ImageRestoreValidator;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;
import servicegateway.utils.Zip4j;

public class UploadRestoreFileImageProcessor extends CommonProcessor<ServiceData>
		implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(UploadRestoreFileImageProcessor.class);

	private String fileRestorePath, fileRestoreType, folderNameDMA, zipPass;

	public Object process(HttpServletRequest request, CommonsMultipartFile file) throws Exception {
		logger.info("Process ===> UploadRestoreFileImageProcessor");

		Map<String, Object> output = new LinkedHashMap<>();

		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
		fileRestorePath = ManagerMap.getString(propMap, PropConstants.FileLocal.Restore.Key.FILE_PATH, "");
		fileRestoreType = ManagerMap.getString(propMap, PropConstants.FileLocal.FILE_TYPE_DMA, "");
		folderNameDMA = ManagerMap.getString(propMap, PropConstants.FileLocal.FolderName.DMA, "");
		zipPass = ManagerMap.getString(propMap, PropConstants.FileLocal.ZIP_PASSWORD, "");

		/* validate file zip type */
		validateFileType(file.getOriginalFilename(), fileRestoreType);

		/* create path of folder is unique name. */
		String uniqueFolder = String.valueOf(new Date().getTime());
		String extractZipFilePath = fileRestorePath + uniqueFolder;

		/* process uploadRestoreFileImage and save file to server. */
		try {

			/*
			 * step 1 : extract restoreFile.zip
			 */
			logger.info("#step 1 : extract restoreFile.zip");

			/* unziping with password */
			new Zip4j().unzip(file, extractZipFilePath, zipPass);

			/* unzip again */
			File folderExtractZip = new File(extractZipFilePath);
			if (folderExtractZip.exists()) {
				for (File fileEntry : folderExtractZip.listFiles()) {
					if (!fileEntry.isDirectory()) {
						/* get index = 0 : because entryZip have one file */
						ManagerStore.extractZipFile(fileEntry.getAbsolutePath(), extractZipFilePath);

						ManagerStore.deleteFile(fileEntry.getAbsolutePath());
						break;
					}
				}
			}
			logger.info("#step 1 : success.");

			/*
			 * #step 2 : read restoreFile get urlList convert to RequiredReference.
			 */
			logger.info("#step 2 : read restoreFile get urlList convert to RequiredReference.");
			RequiredReference requiredRef = readReferenceFromRestoreFile(extractZipFilePath);

			logger.info("#step 2 : success.");

			/*
			 * step 3 : add status, detail, summaryValidation, validation to
			 * RequiredReference
			 */
			logger.info("#step 3 : add status, detail, summaryValidation, validation to RequiredReference");
			output = validateRequiredReference(requiredRef);

			logger.info("#step 3 : success.");

		} catch (Exception e) {
			String errorDesc = "File is unable to be restored. Please try again.";
			logger.info(errorDesc);

			ManagerStore.deleteFile(extractZipFilePath);
			throw new Exception(errorDesc);
		}

		output.put(BackupConstants.FILE_UPLOAD_NAME, uniqueFolder);

		logger.info("output=" + output);

		/*
		 * Wut remove file extractZip (case harddisk maximum) ;
		 */
		logger.info("delete file extract zip !");
		ManagerStore.deleteFile(extractZipFilePath);

		logger.info("End Procces ===> UploadRestoreFileImageProcessor");

		return output;
	}

	private RequiredReference readReferenceFromRestoreFile(String filePath) throws Exception {
		if (filePath == null || "".equals(filePath)) {
			return null;
		}

		/* create path of dma img Folder */
		String imsTemplateFolder = filePath + File.separator + folderNameDMA + File.separator
				+ BackupConstants.FolderName.DMA.IMS;

		/* generate reference required */
		RequiredReference reqRef = new RequiredReference();

		new GenerateImageReferenceRestore().process(reqRef, imsTemplateFolder);

		return reqRef;
	}

	private Map<String, Object> validateRequiredReference(RequiredReference requiredRef) throws Exception {

		boolean validation = true;

		Map<String, Object> imageValidation = new ImageRestoreValidator().process(requiredRef.getDmaImageRequired());
		if (ManagerMap.getBoolean(imageValidation, "summaryValidation") == false) {
			validation = false;
		}

		/* generate output to UI. */
		Map<String, Object> validate = new LinkedHashMap<>();
		validate.put(DmaConstants.StoryBoard.Key.IMAGE_VALIDATION, imageValidation);
		validate.put(DmaConstants.StoryBoard.Key.VALIDATION, validation);

		return validate;
	}

	private void validateFileType(String fileName, String fileType) throws Exception {
		if (fileName == null)
			return;
		if (fileType == null)
			return;

		String[] fileName_sp = fileName.split("\\.");
		String type = "." + fileName_sp[fileName_sp.length - 1];
		if (!type.equals(fileType)) {
			String errorDesc = "Invalid file type. File type must be " + fileType;
			logger.info(errorDesc);
			throw new Exception(errorDesc);
		}
	}

}