package com.jjpa.dma.manager.processor.authen;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.UserModel;
import com.jjpa.db.mongodb.accessor.UserAccessor;
import com.jjpa.db.mongodb.document.dma.manager.UserDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import com.jjpa.web.servlet.UserSession;

public class LoginAuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(LoginAuthenProcessor.class);
	
	private UserAccessor getUserAccessor() {
		return SpringApplicationContext.getBean("UserAccessor");
	}
	
	private UserSession getUserSession(){
		return SpringApplicationContext.getBean("UserSession");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> LoginAuthenProcessor");
		logger.debug("Process ===> LoginAuthenProcessor : {}", input.toJsonString());
		
		return login(input);
		
	}
	
	public Map<String, Object> login(ServiceData input) throws Exception {
		UserModel model = new UserModel();
		model.setClientIp((String)input.getValue("clientIp"));
		model.setServerIp((String)input.getValue("serverIp"));
		model.setUsername((String)input.getValue("username"));
		
		UserAccessor accessor = getUserAccessor(); 
		UserDocument user = accessor.authen(model.getUsername(), (String)input.getValue("password"));
		if(user != null){
			BeanUtils.copyProperties(user, model);

			UserSession session = getUserSession();
			session.setUser(model);
			
			Map<String, Object> output = new HashMap<>();
			output.put("username", user.getUsername());
			return output;
		}else{
			throw new Exception("Invalid Username or Password");
		}
	}
	
}
