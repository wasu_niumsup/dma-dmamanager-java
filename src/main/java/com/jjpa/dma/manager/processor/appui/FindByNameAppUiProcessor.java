package com.jjpa.dma.manager.processor.appui;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByNameAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
private static final Logger logger = LoggerFactory.getLogger(FindByNameAppUiProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public FindByNameAppUiProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByNameAppUiProcessor");
		logger.debug("Process ===> FindByNameAppUiProcessor : {}", input.toJsonString());
		
		
		String uiName = (String)input.getValue(DmaConstants.AppUI.Key.UI_NAME);
		
		
        AppUiDocument appUiDoc = appUiAccessor.findAppUiByName(uiName);
        if(appUiDoc==null){
        	throw new Exception("Screen '"+ uiName +"' not found in database.");
        }
        
        
        /* 30/05/2562 add by akanit : {{$get._dma_param_internal_domain}} */
		appUiDoc.setUrlScreenShot(new DMADomainProcessor().bindingInternalDomain(appUiDoc.getUrlScreenShot()));
		
        
        /* 08/10/2561 add by Akanit : binding needUpgrade.*/
        Map<String, Object> output = new LinkedHashMap<>(appUiDoc.toMap());
		output.put(DmaConstants.AppUI.Key.NEED_UPGRADE, appUiAccessor.needUpgrade(appUiDoc));
		

		logger.info("FindScreenByName hooking output");
		/**
		 * Tanawat W.
		 * 
		 * DMA 5 Hooking Case Edit FindScreenByName
		 */
		Map<String,Object> hookoutput =  callHook("FindScreenByName",output).json();
        
		return hookoutput;
	}

}
