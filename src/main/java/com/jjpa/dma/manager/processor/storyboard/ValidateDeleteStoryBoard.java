package com.jjpa.dma.manager.processor.storyboard;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * ValidateDeleteStoryBoard
 */
@SuppressWarnings({ "unused", "unchecked" })
public class ValidateDeleteStoryBoard extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

    public static class Key{
        /**
         * 
         * 
         * resultSuccess is boolean
         * 
         * if resultSuccess == false then validation error
         * 
         */
        public static final String resultSuccess = "resultSuccess";

        /**
         * resultData is List<Map<String,Object>> 
         * 
         * key : uiName
         * 
         */
        public static final String resultData="resultData";
    }

    private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteStoryBoard.class);
    protected StoryBoardAccessor storyBoardAccessor;
	
	public ValidateDeleteStoryBoard() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        logger.info("Process ===> ValidateDeleteStoryBoard");
		logger.debug("Process ===> ValidateDeleteStoryBoard : {}", input.toJsonString());
		Map<String,Object> body = input.getData();

		logger.info("1. validation");
		List<Map<String,Object>> screenList = new ArrayList<>();
		
		String user = (String)input.getValue("user");
		List<Map<String, Object>> storyList = (List<Map<String, Object>>)input.getValue("storyList");
		
		/**
		 * New condition
		 * Delete 1 storyboard per time
		 * 
		 */
		Map<String,Object> storyObj = storyList.get(0) ;
		String storyName = (String)storyObj.get("storyName");
		StoryBoardDocument  doc =  storyBoardAccessor.findStoryByName(storyName);
		List<Map<String,Object>> storyBoardScreenList =  doc.getStoryBoard();
		for (int i = 0; i < storyBoardScreenList.size(); i++) {
			Map<String,Object> screen = storyBoardScreenList.get(i);
			String uiName = DataUtils.getStringValue(screen, DmaConstants.StoryBoard.Key.SOURCE,null);
			
			Map<String,Object> row = new LinkedHashMap<>();
			row.put("uiName",uiName);

			screenList.add(row);
			
		}

		
		Map<String,Object> output = new LinkedHashMap<>();
        output.put("resultSuccess", screenList.size() == 0);
        
		if(screenList.size() > 0){
			output.put("resultData", screenList);
		}
		
        return output;
    }
}