package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

public interface StoryBoardStoryBoardListAccessor {

	public List<Map<String, Object>> findByName(String name) throws Exception;
	public void deleteByName(String name, String user) throws Exception;
	public void saveByName(String name, List<Map<String, Object>> dataList, String user) throws Exception;
	public void updateOneByScreenName(String screenName, Map<String, Object> dataMap, String user) throws Exception;
}
