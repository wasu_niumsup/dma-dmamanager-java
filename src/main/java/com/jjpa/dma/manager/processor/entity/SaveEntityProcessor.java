package com.jjpa.dma.manager.processor.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.dma.manager.processor.binding.BindingBaseConfiguration;
import com.jjpa.dma.manager.processor.storyboard.FindByNameStoryBoardProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

@SuppressWarnings("unchecked")
public class SaveEntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(SaveEntityProcessor.class);
		
	protected EntityAccessor entityAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	protected TempEntityConfigAccessor tempEntityConfigAccessor;
	
	public SaveEntityProcessor() {
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
		this.tempEntityConfigAccessor = SpringApplicationContext.getBean("TempEntityConfigAccessor");
	}
	
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveEntityProcessor");
		logger.info("Process ===> SaveEntityProcessor : {}", input.toJsonString());
		
		String user = (String)input.getValue(AppConstants.USER);
		String entityName = (String) input.getValue(DmaConstants.Entity.Key.ENTITY_NAME);
		Map<String,Object> body = input.getData();
		
		/* 13/12/2560 comment by Akanit
		 * old version : this method run saveEntity only. 
		 * */
//		return saveEntity(input);
		
		
		/* 28/03/2562 add by Akanit :  */
		String storyName = (String)input.getValue(DmaConstants.Entity.Key.STORY_NAME);
		
		if(storyBoardAccessor.isNeedUpgradeScreen(storyName)){
			throw new Exception("Please upgrade related screen in storyboard '"+ storyName +"'.");
		}
		
		
		/** 13/12/2560 add by Akanit
		 * new version : this method run 5 step.
		 * 1.saveEntity() : save config before binding, use for UI
		 * 2.FindByNameStoryBoardProcessor()
		 * 3.JsonBindingProcessor()
		 * 4.GenerateEntityConfigProcessor()
		 * 5.saveTempConfig() : save biding config to temp. (findTempConfig, deleteTempConfig, saveTempConfig)
		 *  */		
		
		
		/** 
		 * step 1  
		 * */
		logger.info("step 1 : save raw entity to DB.");
		Object entity = saveEntity(input,user);	
		
		logger.info("step 1 : success.");
//		logger.debug("entity={}", new Gson().toJson(entity));
		
		
		/** 
		 * step 2  
		 * */
		logger.info("step 2 : find story board by name.");
		Object storyBoardObj = new FindByNameStoryBoardProcessor().process(request, input);			
		
		StoryBoardDocument storyBoardDoc;
		try{
			storyBoardDoc = (StoryBoardDocument) storyBoardObj;
		}catch (Exception e){
			Map<String,Object> storyBoardMap = (Map<String,Object>) storyBoardObj;
			storyBoardDoc = new StoryBoardDocument(storyBoardMap);
		}
		
		logger.info("step 2 : success.");
		logger.info("storyBoardName={}", storyBoardDoc.getStoryName());
//		logger.debug("storyBoardDoc={}", new Gson().toJson(storyBoardDoc));
		
		
//		/** 
//		 * step 3 
//		 * */
//		logger.info("step 3 : create screenList : binding config from story board and UI to jsonTemplate.");
//		List<Map<String, Object>> screenList = (List<Map<String, Object>>) new JsonBindingProcessor().process(input, storyBoardDoc);
//		
//		logger.info("step 3 : success.");
//		logger.info("screenLis size={}", screenList.size());
//		logger.debug("screenList={}", screenList);
		
		logger.info("step 3 : replace domain and entity application");
		List<Map<String,Object>> screenList = storyBoardDoc.getScreenList();
		if(screenList != null){
			for (int i = 0; i < screenList.size(); i++) {
				Map<String,Object> screen = screenList.get(i);
				//logger.info("uiJson : " + (String)screen.get(DmaConstants.StoryBoard.Key.UI_JSON));
				DocumentContext  tmpCtx = JsonPath.parse( (String)screen.get(DmaConstants.StoryBoard.Key.UI_JSON)  );
				//logger.info("after parse json : " + tmpCtx.jsonString());
				Map<String,Object> replaceDomainScreen = new BindingBaseConfiguration().process(tmpCtx.json(), entityName, DataUtils.getStringValue(body, DmaConstants.Entity.Key.VERSION),storyBoardDoc.getStoryName());
				screen.put(DmaConstants.StoryBoard.Key.UI_JSON, JsonPath.parse(replaceDomainScreen).jsonString());
				
			}
		}
		logger.info("step 3 : success.");
		


		
		/** 
		 * step 4  
		 * */
		logger.info("step 4 : generate entiry config.");
		ServiceData entityConfig = (ServiceData) new GenerateEntityConfigProcessor().process(entityName, storyBoardDoc);	
		
		logger.info("step 4 : success.");
		logger.debug("entityConfig={}", (Map<String, Object>)entityConfig.getData());
		
		
		/** 
		 * step 5  
		 * */
		logger.info("step 5 : save binding entity to DB.");
		saveTempEntityConfig(entityConfig,user);
		
		logger.info("step 5 : success.");

		callHook("changeStoryBoard", body);
		
		return entity;
	}
	
	private Object saveEntity(ServiceData input,String user) throws Exception {
				
		/* 
		 * check duplicate entiry name. 
		 * */
		EntityDocument oldDoc = entityAccessor.findEntityByName((String)input.getValue(DmaConstants.Entity.Key.ENTITY_NAME));
		EntityDocument document = new EntityDocument();
		if((input.getValue(DmaConstants.Entity.Key.ID)!=null && !"".equals(((String)input.getValue(DmaConstants.Entity.Key.ID)).trim()))){
			String id = (String)input.getValue(DmaConstants.Entity.Key.ID);
			if(oldDoc!=null && !id.equals(oldDoc.getId())){
				// check rename case
				throw new Exception("Name duplicated");
			}
			document.setId(id);
		}else if(oldDoc != null){
			// check create case
			throw new Exception("Name duplicated");
		}
		
		
		/* set entity param. */
		document.setEntityName((String)input.getValue(DmaConstants.Entity.Key.ENTITY_NAME));
		document.setRemark((String)input.getValue(DmaConstants.Entity.Key.REMARK));
		document.setStoryName((String)input.getValue(DmaConstants.Entity.Key.STORY_NAME));
		document.setRole((String)input.getValue(DmaConstants.Entity.Key.ROLE));
		document.setTheme((String)input.getValue(DmaConstants.Entity.Key.THEME));
		document.setStatus((String)input.getValue(DmaConstants.Entity.Key.STATUS));
		document.setVersion((String)input.getValue(DmaConstants.Entity.Key.VERSION));
		document.setPublishStatus(DmaConstants.PublishedStatus.WARNING);
		document.setLastUpdate(new Date());
		document.setAppInfo(input.getValue(DmaConstants.Entity.Key.APP_INFO));
		document.setAppKey(oldDoc.getAppKey());
		/* save entity to DB. */
		return entityAccessor.saveEntity(document, user);
	}

	private Object saveTempEntityConfig(ServiceData input,String user) throws Exception {
				
		/* 
		 * check entiry name is exists. 
		 * */		
		String entityName = (String) input.getValue(DmaConstants.Entity.Key.ENTITY_NAME);
		
		TempEntityConfigDocument document = tempEntityConfigAccessor.findTempConfigByName(entityName);
		if(document==null){
			document = new TempEntityConfigDocument();
		}
		
		/* set tempEntity param. */
		document.setEntityName((String)input.getValue(DmaConstants.Entity.Key.ENTITY_NAME));
		document.setHome((String)input.getValue(DmaConstants.Entity.Key.HOME_PAGE));
		document.setFirstPage((String)input.getValue(DmaConstants.Entity.Key.FIRST_PAGE));
		document.setNotificationPage((String)input.getValue(DmaConstants.Entity.Key.NOTIFICATION_PAGE));
		document.setScreenList(input.getValue(DmaConstants.Entity.Key.SCREEN_LIST));
		document.setStoryBoardName((String)input.getValue(DmaConstants.Entity.Key.STORY_NAME));
		
		
		/* save tempEntity to DB. */
		return tempEntityConfigAccessor.saveTempConfig(document, user);
	}
}
