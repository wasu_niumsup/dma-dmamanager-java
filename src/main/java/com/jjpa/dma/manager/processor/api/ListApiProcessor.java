package com.jjpa.dma.manager.processor.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.SGConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;

public class ListApiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(ListApiProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListApiProcessor");
		logger.debug("Process ===> ListApiProcessor : {}", input.toJsonString());
		
		return listApi(input);
		
	}
	
	@SuppressWarnings("unchecked")
	private Object listApi(ServiceData input) throws Exception{
		
		Map<String, Object> apiResponse = new LinkedHashMap<String, Object>();
		List<Map<String, Object>> apiList = new ArrayList<Map<String, Object>>();
		
		String findServiceMsg = "{"
				+ "\"contentType\":301,"
				+ "\"source\":2,"
				+ "\"message\":\"Get Service List\","
				+ "\"requestData\" : {}"
			 	+ "}";
		
		DocumentContext ctx = JsonPath.parse(findServiceMsg);

		/**
		* Tanawat W.
		* [2018-05-08] Change transaction message.
		*/
		ctx.put("$",DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);

		DocumentContext responseCtx = getJsonAccessor().send(DataUtils.getSGURL() + "/serviceGateway/apis/json/local/externalAPIs/listData", ctx);
		Map<String, Object> response = responseCtx.json();
		Map<String, Object> responseHeader = (Map<String, Object>)response.get("responseHeader");
		if(!"0000I".equals(responseHeader.get("responseCode"))){
			throw new Exception((String)responseHeader.get("responseDescription"));
		}else{
			Map<String, Object> responseData = (Map<String, Object>)response.get(DmaManagerConstant.RESPONSE_DATA);
			List<Map<String, Object>> searchList = (List<Map<String, Object>>)responseData.get("searchList");
		
			Map<String, Object> doNothing = new HashMap<String, Object>();
			doNothing.put("apiName", "Do Nothing");
			apiList.add(doNothing);
			
			Map<String, Object> apiName;
			for(Map<String, Object> service : searchList){
//				Map<String, Object> jsonData = (Map<String, Object>)service.get("jsonData");
//				String serviceName = (String)jsonData.get("serviceName");
				apiName = new HashMap<String, Object>();
				apiName.put("apiName", ManagerMap.getString(service, SGConstants.Key.SERVICE_NAME));
				apiList.add(apiName);
			}
			apiResponse.put("apiList", apiList);
			return apiResponse;
		}
	}
	
}
