package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import servicegateway.utils.ManagerMap;

@Document(collection = "dma_action_chain")
public class DMAActionChainDocument extends BaseDMADocument{

	private static final long serialVersionUID = 1L;

	@Indexed(unique=true)
	private String actionChainType;
	private String className;
	private String version;

	public DMAActionChainDocument() {
	}
	
	public DMAActionChainDocument(Map<String, Object> dataMap) throws Exception {
		
		
		/* ************************** set extends *********************************** */
		this.setId(ManagerMap.getString(dataMap, "id"));
		this.setCreateBy(ManagerMap.getString(dataMap, "createBy"));
		this.setLastUpdateBy(ManagerMap.getString(dataMap, "lastUpdateBy"));
		this.setDmaManagerVersion(ManagerMap.getString(dataMap, "dmaManagerVersion"));
		this.setNativeJson(ManagerMap.get(dataMap, "nativeJson", Map.class));
		this.setBinding(ManagerMap.get(dataMap, "binding", List.class));
		
		/* set create date */
		Long createDate = ManagerMap.getLong(dataMap, "createDate");
		if(createDate!=null){
			this.setCreateDate(new Date(createDate));
		}
		
		/* set lastUpdate date */
		Long lastUpdateDate = ManagerMap.getLong(dataMap, "lastUpdateDate");
		if(lastUpdateDate!=null){
			this.setLastUpdateDate(new Date(lastUpdateDate));
		}

		/* set lastUpdate */
		Long lastUpdate = ManagerMap.getLong(dataMap, "lastUpdate");
		if(lastUpdate!=null){
			this.setLastUpdate(new Date(lastUpdate));
		}else {
			Object lastUpdateObj = dataMap.get("lastUpdate");
			if(lastUpdateObj instanceof Date){
				this.setLastUpdate( (Date)lastUpdateObj );
			}
		}
		/* *************************************************************************** */
		
		
		/* ****************************** set this *********************************** */
		this.setActionChainType(ManagerMap.getString(dataMap, "actionChainType"));
		this.setClassName(ManagerMap.getString(dataMap, "className"));
		this.setVersion(ManagerMap.getString(dataMap, "version"));
		/* *************************************************************************** */
		
	}
	
	public String getActionChainType() {
		return actionChainType;
	}

	public void setActionChainType(String actionChainType) {
		this.actionChainType = actionChainType;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate().getTime());
		map.put("lastUpdateDate", this.getLastUpdateDate().getTime());
		map.put("dmaManagerVersion", this.getDmaManagerVersion());
		
		map.put("nativeJson", this.getNativeJson());
		map.put("binding", this.getBinding());
		try{
			map.put("lastUpdate", this.getLastUpdate().getTime());
		}catch(Exception e){
			map.put("lastUpdate", "");
		}
		
		map.put("actionChainType", this.getActionChainType());
		map.put("className", this.getClassName());
		map.put("version", this.getVersion());
		
		return map;
	}
}
