package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.jjpa.db.mongodb.document.BaseDocument;

@Document(collection = "screen_native_input_type")
public class ScreenNativeInputTypeDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	@Indexed
	private String globalName;
	private String fieldType;
	private String source;
	private String api;
	private String action;
	private String dataStructure;
	private String className;
	private String dmaParameter;

	/**
	 * @return the globalName
	 */
	public String getGlobalName() {
		return globalName;
	}

	/**
	 * @param globalName the globalName to set
	 */
	public void setGlobalName(String globalName) {
		this.globalName = globalName;
	}

	/**
	 * @return the fieldType
	 */
	public String getFieldType() {
		return fieldType;
	}

	/**
	 * @param fieldType the fieldType to set
	 */
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the api
	 */
	public String getApi() {
		return api;
	}
	
	/**
	 * @param api the api to set
	 */
	public void setApi(String api) {
		this.api = api;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the dataStructure
	 */
	public String getDataStructure() {
		return dataStructure;
	}

	/**
	 * @param dataStructure the dataStructure to set
	 */
	public void setDataStructure(String dataStructure) {
		this.dataStructure = dataStructure;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the dmaParameter
	 */
	public String getDmaParameter() {
		return dmaParameter;
	}

	/**
	 * @param dmaParameter the dmaParameter to set
	 */
	public void setDmaParameter(String dmaParameter) {
		this.dmaParameter = dmaParameter;
	}

}
