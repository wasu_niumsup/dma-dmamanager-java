package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToStoryBoardProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import net.lingala.zip4j.exception.ZipException;
import servicegateway.api.ServiceGatewayApi;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;
import servicegateway.utils.ManagerStore;
import servicegateway.utils.Zip4j;

/* 
 * XX/XX/2561 create by Akanit : : finished.
 * 02/05/2561 add by Akanit : if this service have relateDropdown then load child service config : finished. 
 * */
public class GenerateServiceGatewayFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateServiceGatewayFolderBackup.class);

	protected AppUiAccessor appUiAccessor;
	
	public GenerateServiceGatewayFolderBackup() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	public void process(StoryBoardDocument storyBoardDoc, String directoryExtractFileBackup) throws Exception {
		logger.info("process : generate serviceGateway backup folder");
		
		/* 14/11/2562 add by Akanit : DMA.5.9.0 : DMA-1739 : fix bug reference invalid when change sgwAPIdropdown in storyboard. */
		bindingSgServiceRequired(storyBoardDoc);
		
		
		List<Map<String, Object>> serviceGatewayRequiredList = new GetReferenceProcessor().getSgServiceRequireds(storyBoardDoc.getReference());
		if(serviceGatewayRequiredList == null || serviceGatewayRequiredList.size() == 0){
			logger.info("#not found serviceGateway required.");
			return;
		}
		
		/* call serviceGateway */
		Map<String, Object> sgResponse = new ServiceGatewayApi().backupData(serviceGatewayRequiredList);
		
		
		String filePath = ManagerMap.getString(sgResponse, "filePath");
		if(StringUtil.isBlank(filePath)){
			String errorMessage = "#not found parameter filePath from serviceGateway.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
		}
		
		extractBackupFileFromServiceGateway(filePath, directoryExtractFileBackup);
		
	}

	private void bindingSgServiceRequired(StoryBoardDocument storyBoardDoc) throws Exception {
		
		String screenName;
		AppUiDocument appUiDoc;
		
		for(Map<String, Object> storyBoard : storyBoardDoc.getStoryBoard()){
			
			screenName = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
			
			appUiDoc = appUiAccessor.findAppUiByName(screenName);
			
			if(appUiDoc==null){
				throw new Exception("screen '"+ screenName +"' not found.");
			}
			
			new BindingRequiredReferenceToStoryBoardProcessor().bindingSgServiceRequiredReference(storyBoardDoc, storyBoard, appUiDoc);
		}

	}
	
	private void extractBackupFileFromServiceGateway(String filePath, String directoryExtractFileBackup) throws Exception, ZipException {
		
		try{
			
			/* read constants from file.properties */
			Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.FileLocal.NAME);
			String zipPass = ManagerMap.getString(propMap, PropConstants.FileLocal.ZIP_PASSWORD, "");
			
			
	    	/* unziping with password */
	    	new Zip4j().unzip(filePath, directoryExtractFileBackup, zipPass);
	    	
	    	/* unzip again */
			File folderExtractZip = new File(directoryExtractFileBackup);
			if(folderExtractZip.exists()){
				for(File fileEntry : folderExtractZip.listFiles()){
					if(!fileEntry.isDirectory()){
				    	/* get index = 0 : because entryZip have one file */
				    	ManagerStore.extractZipFile(fileEntry.getAbsolutePath(), directoryExtractFileBackup);
				    	
				    	ManagerStore.deleteFile(fileEntry.getAbsolutePath());
				    	break;
					}
				}
			}
			
		}finally{
			
			/* remove backupFile of serviceGateway */
			ManagerStore.deleteFile(filePath);
		}
		
	}

}
