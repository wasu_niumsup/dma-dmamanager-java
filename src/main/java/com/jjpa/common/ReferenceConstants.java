package com.jjpa.common;

public class ReferenceConstants {

	public static class DMA{
		
		public static final String VERSION 			= "version";
		public static final String CURRENT_VERSION 	= "currentVersion";
		public static final String NAME 			= "name";
		public static final String TYPE 			= "type";
	}

	public static class SG{

		public static final String UUID 			= "uuid";
		public static final String SERVICE_NAME 	= "serviceName";
	}

}
