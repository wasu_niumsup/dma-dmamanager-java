
package com.jjpa.common.data.service;

import java.io.InputStream;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.PropConstants;

/**
 * ServicesConfig
 */
public class ServicesConfig {

    

    public Map<String,Object> getConfig(){
        InputStream is = getClass().getResourceAsStream(PropConstants.Service.NAME);
        DocumentContext reqContext = JsonPath.parse(is);

        Map<String,Object> output = reqContext.json();

        return output;
    }


}