package com.jjpa.dma.manager.processor.configuration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ConfigurationAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListConfigurationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListConfigurationProcessor.class);
		
	protected ConfigurationAccessor configurationAccessor;
	
	public ListConfigurationProcessor() {
		this.configurationAccessor = SpringApplicationContext.getBean("ConfigurationAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListConfigurationProcessor");
		logger.debug("Process ===> ListConfigurationProcessor : {}", input.toJsonString());
		
		return configurationAccessor.listConfigurationName();
		
	}

}
