package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.spring.SpringApplicationContext;

public class ActionChainRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(ActionChainRestoreValidator.class);
	
	protected ActionChainAccessor actionChainAccessor;
	
	public ActionChainRestoreValidator() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	public Map<String, Object> process(List<Map<String, Object>> actionChainRequiredList) throws Exception {
		logger.info("Process ===> ActionChainRestoreValidator");
		
		boolean validationStatus = true;
    	@SuppressWarnings("unused")
		String version_restore, version_old, name;

    	for(Map<String, Object> actionChainRequired : actionChainRequiredList){
    		
    		actionChainRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
    		
    		
//    		version_restore = ManagerMap.getString(actionChainRequired, ReferenceConstants.DMA.VERSION);
//    		name = ManagerMap.getString(actionChainRequired, ReferenceConstants.DMA.NAME);
//    		
//    		DMAActionChainDocument doc = actionChainAccessor.findActionChainByType(name);
//    		
//    		
//    		/* validate add status */
//    		if(doc==null){
//    			logger.info("#not found old ActionChain="+ name);
//    			logger.info("#update action=ADD");
//    			logger.info("#update status=true");
//    			
//    			actionChainRequired.put("status", true);
//    			continue;
//    			
//    		}else {
//    			logger.info("#found old ActionChain="+ name);
//    		}
//    		
//    		
//    		try {
//    			version_old = doc.getVersion();
//			} catch (Exception e) {
//				logger.info("#ActionChain="+ name + "not have reference.");
//				logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//
////    			validationStatus = false;
//				actionChainRequired.put("status", true);
//				continue;
//			}
//    		
//    		
//    		
//    		if(version_restore == version_old){
//    			logger.info("#ActionChain="+ name + " can restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				actionChainRequired.put("status", true);
//    			
//    		}else{
//    			logger.info("#ActionChain="+ name + " can not restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				actionChainRequired.put("status", true);
//    		}
    		
    	}
    	
    	/* mapping data to UI */
    	Map<String, Object> output = new LinkedHashMap<>();
    	output.put("summaryValidation", validationStatus);
    	output.put("data", actionChainRequiredList);
    	
    	return output;
	}
}
