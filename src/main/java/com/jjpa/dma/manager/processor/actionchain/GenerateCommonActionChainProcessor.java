package com.jjpa.dma.manager.processor.actionchain;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;

public class GenerateCommonActionChainProcessor {

	private static final Logger logger = LoggerFactory.getLogger(GenerateCommonActionChainProcessor.class);

	protected ActionChainAccessor actionChainAccessor;
	
	public GenerateCommonActionChainProcessor() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	public DocumentContext process(DocumentContext innerContext, String actionChainType) throws Exception {
		logger.info("GenerateCommonActionChain");
		
		/* find action chain type from db */
		DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(actionChainType);
		if(actionChainDocument==null){
			
			logger.info("Need patching. {}", actionChainType);
			throw new Exception("DMA need patching, Code \"" + actionChainType + "\"");
		}
		
		DocumentContext resultContext = JsonPath.parse(actionChainDocument.getNativeJson().get(DmaConstants.ActionChain.Key.DATA));
		
		
		List<Map<String, Object>> bindingList = actionChainDocument.getBinding();
		
		/* find row key=name value=success */
		Map<String, Object> bindingSuccessMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.SUCCESS);
		
		String path = DataUtils.getStringValue(bindingSuccessMap.get(DmaConstants.ActionChain.Key.PATH));
		
		logger.info("#binding commonActionChain {}={}. (set)", DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.SUCCESS);
		
		logger.info("path={}", path);
		logger.info("innerContext={}", innerContext.jsonString());
		
		resultContext.set(path, innerContext.json());
		
		logger.info("resultContext={}", resultContext.jsonString());
		return resultContext;
	}
}
