package com.jjpa.dma.manager.processor.backup;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;

import servicegateway.utils.ManagerStore;

public class GenerateStoryBoardFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateStoryBoardFolderBackup.class);
	
	public void process(StoryBoardDocument storyBoardDoc, String directoryFileBackup) throws Exception {
		logger.info("process : generate storyBoard backup folder");
		
		/* example 
		 * directoryFile =  {directoryFileBackup}/storyBoard/{storyName}
		 * */
		String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.STORY_BOARD + File.separator + storyBoardDoc.getStoryName();
		
		/* save file */
		ManagerStore.createFileByJsonMap(storyBoardDoc.toMap(), directoryFile);
	}
}
