package com.jjpa.dma.manager.processor.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListStatusEntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListStatusEntityProcessor.class);
		
	protected EntityAccessor entityAccessor;
	
	public ListStatusEntityProcessor() {
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListStatusEntityProcessor");
		logger.debug("Process ===> ListStatusEntityProcessor : {}", input.toJsonString());
		
		return listEntityStatus();
		
	}
	
	private Map<String, Object> listEntityStatus() throws Exception {
		Map<String, Object> statusResponse = new HashMap<String, Object>();
		List<Map<String, Object>> statusList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.ACTIVE_STATUS){
			Map<String, Object> status = new HashMap<String, Object>();
			status.put("statusName", s);
			statusList.add(status);
		}
		statusResponse.put("statusList", statusList);
		return statusResponse;
	}

}
