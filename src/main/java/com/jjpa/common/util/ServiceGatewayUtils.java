package com.jjpa.common.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.APIsConstants;

public class ServiceGatewayUtils {

	private static final Logger logger = LoggerFactory.getLogger(ServiceGatewayUtils.class);
	
	public static String getKeyField(String externalApisType, Map<String, Object> configField) {
		
		String key;
		
		/* 21/02/2561 add by Akanit :  */
		if (APIsConstants.ExternalApiType.JSON_SERVICE_APIS.equalsIgnoreCase(externalApisType) ||
			APIsConstants.ExternalApiType.JSON_SERVICE_DROPDOWN.equalsIgnoreCase(externalApisType)) {
			logger.info("case : jsonserviceapis, jsonservicedropdown");
			
			key = DataUtils.getStringValue(configField.get(APIsConstants.Key.XPATH));
		}else {
			logger.info("case : not json");
			
			key = DataUtils.getStringValue(configField.get(APIsConstants.Key.INDEX));
		}
		logger.info("key="+ key);
		return key;
	}
}
