package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainTemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerStore;

/* 
 * 17/08/2561 add by Akanit : get data all database : finished.
 * */
public class GenerateActionChainTemplateFolderBackup {

	private static final Logger logger = LoggerFactory.getLogger(GenerateActionChainTemplateFolderBackup.class);

	protected ActionChainTemplateAccessor actionChainTemplateAccessor;
	
	public GenerateActionChainTemplateFolderBackup() {
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
	}
	
	public void process(Reference reference, String directoryFileBackup) throws Exception {
		logger.info("process : generate actionChainTemplate backup folder");
		
		List<Map<String, Object>> actionChainTemplateRequiredList = new GetReferenceProcessor().getDmaActionChainTemplateRequireds(reference);
		if(actionChainTemplateRequiredList==null){
			return;
		}
		
		String actionChainTemplateName;
		for(Map<String, Object> actionChainTemplateRequired : actionChainTemplateRequiredList){
			
			/* find actionChainTemplate in mongo */
			actionChainTemplateName = ManagerMap.getString(actionChainTemplateRequired, ReferenceConstants.DMA.NAME);
			DMAActionChainTemplate actionChainTemplateDoc = actionChainTemplateAccessor.findActionChainTemplateByType(actionChainTemplateName);
			if(actionChainTemplateDoc==null){
				String errorMessage = "ActionChainTemplate '"+ actionChainTemplateName +"' not found in DB.";
				logger.info(errorMessage);
				throw new Exception(errorMessage);
			}
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/actionChainTemplate/{actionChainTemplateName}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.ACTION_CHAIN_TEMPLATE + File.separator + actionChainTemplateName;
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(actionChainTemplateDoc.toMap(), directoryFile);
		}

	}
	
	/* 17/08/2561 add by Akanit : get data all database */
	public void process(String directoryFileBackup) throws Exception {
		logger.info("process : generate all actionChainTemplate backup folder");
		
		List<DMAActionChainTemplate> actionChainTemplateDocList = actionChainTemplateAccessor.listActionChainTemplate();
		
		for(DMAActionChainTemplate actionChainTemplateDoc : actionChainTemplateDocList){
			
			
			/* example 
			 * directoryFile =  {directoryFileBackup}/actionChainTemplate/{actionType}
			 * */
			String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.ACTION_CHAIN_TEMPLATE + File.separator + actionChainTemplateDoc.getActionType();
			
			
			/* save file */
			ManagerStore.createFileByJsonMap(actionChainTemplateDoc.toMap(), directoryFile);
		}
		
	}
	
}
