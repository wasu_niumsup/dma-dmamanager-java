package com.jjpa.dma.manager.processor.storyboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Query;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerPropertiesFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.CriteriaUtil;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListStoryBoardProcessor.class);
		
	protected StoryBoardAccessor storyBoardAccessor;
	
	public ListStoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	private Integer maxListStoryRecord;
	
	@Override
	public Object process(HttpServletRequest request, ServiceData serviceData) throws Exception {
		
		logger.info("Process ===> ListStoryBoardProcessor");
//		logger.debug("Process ===> ListStoryBoardProcessor : {}", serviceData.toJsonString());

		Map<String,Object> input = serviceData.getData();
		logger.info("inputBody="+input);
		
		
		/* read constants from file.properties */
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(PropConstants.SystemConfig.NAME);
		maxListStoryRecord = ManagerMap.getIntger(propMap, PropConstants.SystemConfig.Key.MAX_LIST_STORY_RECORD);
		
		
		/* 
		 * step 1 : create query from WhereOption and GridPaggingModel
		 * */
		logger.info("create query from WhereOption and GridPaggingModel");
		Query query = new Query();
		
		List<WhereOption> whereOptList = createWhereOption(input);
		CriteriaUtil.addCriteriaWhere(query, whereOptList);
		
		GridPagging gridPagging = createGridPaggingModel(input);
		CriteriaUtil.addCriteriaPagging(query, gridPagging);

		logger.info("step 1 : success.");
		logger.info("query="+query);
		
		
		return storyBoardAccessor.listStoryByCriteria(query);
		
	}


	
	private List<WhereOption> createWhereOption(Map<String, Object> input) throws Exception {
		
		String storyName = ManagerMap.getString(input, DmaConstants.StoryBoard.Key.STORY_NAME);
		String remark = ManagerMap.getString(input, DmaConstants.StoryBoard.Key.REMARK);
		


		String username = ManagerMap.getString(input, "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}
		
//		Long startTime = ManagerMap.getLong(input, AppConstants.Log.Key.START_TIME);
//		Long endTime = ManagerMap.getLong(input, AppConstants.Log.Key.END_TIME);
		
		
		// create whereOptList.
		List<WhereOption> whereOptList = new ArrayList<>();
		WhereOption whereOpt;

		
		/* where : search */
		whereOpt = new WhereOption();
		whereOpt.setPath(MongoConstants.StoryBoard.Path.STORY_NAME);
		whereOpt.setValue(storyName);
		whereOpt.setSearch(true);
		whereOptList.add(whereOpt);


		if(!"admin".equals(username)){
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.AppUI.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
			whereOptList.add(whereOpt);
		}

		whereOpt = new WhereOption();
		whereOpt.setPath(MongoConstants.Path.REMARK);
		whereOpt.setValue(remark);
		whereOpt.setSearch(true);
		whereOptList.add(whereOpt);
		
		
		/* where : between */
//		whereOpt = new WhereOption();
//		whereOpt.setPath(MongoConstants.Key.JSON_DATA+"."+AppConstants.Log.Key.REQUEST_TIME);
//		whereOpt.setStart(startTime);
//		whereOpt.setEnd(endTime);
//		whereOpt.setBetween(true);
//		whereOptList.add(whereOpt);
		
		
		return whereOptList;
	}

	private GridPagging createGridPaggingModel(Map<String, Object> input) throws Exception {
		
		// get value from UI.	
		String sort = ManagerMap.getString(input, AppConstants.Pagging.Key.SORT);
		String dir = ManagerMap.getString(input, AppConstants.Pagging.Key.DIR);
		String start = ManagerMap.getString(input, AppConstants.Pagging.Key.START);
		String limit = ManagerMap.getString(input, AppConstants.Pagging.Key.LIMIT);
		
		/* check max limit */
		if(maxListStoryRecord != null){
			if( StringUtil.isBlank(limit) || Integer.parseInt(limit) > maxListStoryRecord ){
				limit = maxListStoryRecord.toString();
			}
		}
		
		
		// create gridPaaging model.
		GridPagging gridPagging = new GridPagging();
		gridPagging.setSort(sort);
		gridPagging.setDir(dir);
		gridPagging.setStart(start);
		gridPagging.setLimit(limit);
		return gridPagging;
		
	}

}
