package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jjpa.db.mongodb.document.BaseDocument;
import com.jjpa.dma.manager.model.reference.Reference;

import servicegateway.utils.ManagerMap;

@Document(collection = "app_ui_config")
public class AppUiDocument extends BaseDocument{

	private static final long serialVersionUID = 1L;
	
	@Indexed(unique=true)
	private String uiName;
	private String remark;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdate;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastPublish;
	private String status;
	private Map<String, Object> uiConfigData;
//	private Map<String, String> masterJson;
	private String templateType;
	private Reference refference;
	private List<Map<String,Object>> inputList;
	private List<Map<String,Object>> dynamicInputList;
	private List<Map<String,Object>> dynamicOutputList;
	private List<Map<String,Object>> actionList;
	private String dynamicApi;
	private List<Map<String,Object>> dynamicDataBinding;
	private String uiJson;
	
	/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
	private String urlScreenShot;
	
	public AppUiDocument() {
	}
	
	public AppUiDocument(Map<String, Object> dataMap) throws Exception {
		
		
		/* ************************** set extends *********************************** */
		this.setId(ManagerMap.getString(dataMap, "id"));
		this.setCreateBy(ManagerMap.getString(dataMap, "createBy"));
		this.setLastUpdateBy(ManagerMap.getString(dataMap, "lastUpdateBy"));
		this.setDmaManagerVersion(ManagerMap.getString(dataMap, "dmaManagerVersion"));
		
		/* set create date */
		Long createDate = ManagerMap.getLong(dataMap, "createDate");
		if(createDate!=null){
			this.setCreateDate(new Date(createDate));
		}
		
		/* set lastUpdate date */
		Long lastUpdateDate = ManagerMap.getLong(dataMap, "lastUpdateDate");
		if(lastUpdateDate!=null){
			this.setLastUpdateDate(new Date(lastUpdateDate));
		}
		/* *************************************************************************** */
		
		
		/* ****************************** set this *********************************** */
		this.setUiName(ManagerMap.getString(dataMap, "uiName"));
		this.setRemark(ManagerMap.getString(dataMap, "remark"));
		this.setStatus(ManagerMap.getString(dataMap, "status"));
		this.setUiConfigData(ManagerMap.get(dataMap, "uiConfigData", Map.class));
		this.setTemplateType(ManagerMap.getString(dataMap, "templateType"));
		
		Map<String, Object> referenceMap = ManagerMap.get(dataMap, "refference", Map.class);
		Reference reference = new ObjectMapper().convertValue(referenceMap, Reference.class);
		
		this.setReference(reference);
		this.setInputList(ManagerMap.get(dataMap, "inputList", List.class));
		this.setDynamicInputList(ManagerMap.get(dataMap, "dynamicInputList", List.class));
		this.setDynamicOutputList(ManagerMap.get(dataMap, "dynamicOutputList", List.class));
		this.setActionList(ManagerMap.get(dataMap, "actionList", List.class));
		this.setDynamicApi(ManagerMap.getString(dataMap, "dynamicApi"));
		this.setDynamicDataBinding(ManagerMap.get(dataMap, "dynamicDataBinding", List.class));
		this.setUiJson(ManagerMap.getString(dataMap, "uiJson"));
		
		/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
		this.setUrlScreenShot(ManagerMap.getString(dataMap, "urlScreenShot"));
		
		/* set lastUpdate */
		Long lastUpdate = ManagerMap.getLong(dataMap, "lastUpdate");
		if(lastUpdate!=null){
			this.setLastUpdate(new Date(lastUpdate));
		}else {
			Object lastUpdateObj = dataMap.get("lastUpdate");
			if(lastUpdateObj instanceof Date){
				this.setLastUpdate( (Date)lastUpdateObj );
			}
		}
		
		/* set lastPublish */
		Long lastPublish = ManagerMap.getLong(dataMap, "lastPublish");
		if(lastPublish!=null){
			this.setLastPublish(new Date(lastPublish));
		}else {
			Object lastPublishObj = dataMap.get("lastPublish");
			if(lastPublishObj instanceof Date){
				this.setLastPublish( (Date)lastPublishObj );
			}
		}
		
		/* *************************************************************************** */
		
	}
	
	public String getUiJson() {
		return uiJson;
	}
	public void setUiJson(String uiJson) {
		this.uiJson = uiJson;
	}
	public String getUiName() {
		return uiName;
	}
	public void setUiName(String uiName) {
		this.uiName = uiName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Date getLastPublish() {
		return lastPublish;
	}
	public void setLastPublish(Date lastPublish) {
		this.lastPublish = lastPublish;
	}
	public Map<String, Object> getUiConfigData() {
		return uiConfigData;
	}
	public void setUiConfigData(Map<String, Object> uiConfigData) {
		this.uiConfigData = uiConfigData;
	}
//	public Map<String, String> getMasterJson() {
//		return masterJson;
//	}
//	public void setMasterJson(Map<String, String> masterJson) {
//		this.masterJson = masterJson;
//	}
	public List<Map<String, Object>> getDynamicInputList() {
		return dynamicInputList;
	}
	public void setDynamicInputList(List<Map<String, Object>> dynamicInputList) {
		this.dynamicInputList = dynamicInputList;
	}
	public String getDynamicApi() {
		return dynamicApi;
	}
	public void setDynamicApi(String dynamicApi) {
		this.dynamicApi = dynamicApi;
	}
	public List<Map<String, Object>> getInputList() {
		return inputList;
	}
	public void setInputList(List<Map<String, Object>> inputList) {
		this.inputList = inputList;
	}
	public Reference getReference() {
		return refference;
	}
	public void setReference(Reference reference) {
		this.refference = reference;
	}
	public List<Map<String, Object>> getActionList() {
		return actionList;
	}
	public void setActionList(List<Map<String, Object>> actionList) {
		this.actionList = actionList;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public List<Map<String, Object>> getDynamicDataBinding() {
		return dynamicDataBinding;
	}
	public void setDynamicDataBinding(List<Map<String, Object>> dynamicDataBinding) {
		this.dynamicDataBinding = dynamicDataBinding;
	}
	public List<Map<String, Object>> getDynamicOutputList() {
		return dynamicOutputList;
	}
	public void setDynamicOutputList(List<Map<String, Object>> dynamicOutputList) {
		this.dynamicOutputList = dynamicOutputList;
	}
	
	/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
	public String getUrlScreenShot() {
		return urlScreenShot;
	}
	public void setUrlScreenShot(String urlScreenShot) {
		this.urlScreenShot = urlScreenShot;
	}

	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate().getTime());
		map.put("lastUpdateDate", this.getLastUpdateDate().getTime());
		map.put("dmaManagerVersion", this.getDmaManagerVersion());
		
		map.put("uiName", this.getUiName());
		map.put("remark", this.getRemark());
		map.put("status", this.getStatus());
		map.put("uiConfigData", this.getUiConfigData());
		map.put("templateType", this.getTemplateType());
		map.put("refference", this.getReference());
		map.put("inputList", this.getInputList());
		map.put("dynamicInputList", this.getDynamicInputList());
		map.put("dynamicOutputList", this.getDynamicOutputList());
		map.put("actionList", this.getActionList());
		map.put("dynamicApi", this.getDynamicApi());
		map.put("dynamicDataBinding", this.getDynamicDataBinding());
		map.put("uiJson", this.getUiJson());
		
		/* 25/03/2562 (250325621035) add by Akanit : requirement from siam */
		map.put("urlScreenShot", this.getUrlScreenShot());
		
		try{
			map.put("lastUpdate", this.getLastUpdate().getTime());
		}catch(Exception e){
			map.put("lastUpdate", "");
		}
		
		try{
			map.put("lastPublish", this.getLastPublish().getTime());
		}catch(Exception e){
			map.put("lastPublish", "");
		}

		return map;
	}
	
}
