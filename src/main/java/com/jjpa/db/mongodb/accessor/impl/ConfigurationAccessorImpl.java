package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ConfigurationAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAConfigurationDocument;

public class ConfigurationAccessorImpl implements ConfigurationAccessor {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationAccessorImpl.class);


    public static class Key{
        public static String configurationType ="configurationType";
        public static class Data {
            public static String version = "version";
            public static String linkdownload = "linkdownload";
            
        }
    }
	protected MongoDbAccessor mongoDbAccessor;

	protected ConfigurationAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Object saveConfiguration(DMAConfigurationDocument configDoc, String user) throws Exception {
		
		logger.info("Process ===> ConfigurationAccessorImpl saveConfiguration configDoc " + configDoc);

		try{
			String configName = configDoc.getConfigName();

			DMAConfigurationDocument oldConfig = findConfigurationByName(configName);
			if(oldConfig != null){
				oldConfig.setNativeJson(configDoc.getNativeJson());
				configDoc = oldConfig;
			}
			return mongoDbAccessor.save(configDoc, user);
		}catch(Exception e){
			logger.info("Process ===> ConfigurationAccessorImpl saveConfiguration ERROR!!!!! " + e);
			throw e;
		}
		
	}

	public List<DMAConfigurationDocument> listConfiguration() throws Exception {
		
		try{
			return mongoDbAccessor.findAll(DMAConfigurationDocument.class);
		}catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,Object> listConfigurationName() throws Exception {
		List<DMAConfigurationDocument> configurationObjList = listConfiguration();
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String,Object>> configurationNameList = new ArrayList<Map<String,Object>>();
		for(DMAConfigurationDocument document : configurationObjList){
			Map<String,Object> configurationMap = new HashMap<String,Object>();
			configurationMap.put("configName", document.getConfigName());
			configurationNameList.add(configurationMap);
		}
		result.put("configNameList", configurationNameList);
		return result;
	}
	

	public DMAConfigurationDocument findConfigurationByName(String configName) throws Exception{
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where("configName").regex(Pattern.compile(DataUtils.getMongoMatchRegex(configName), Pattern.CASE_INSENSITIVE)));
		DMAConfigurationDocument configDoc = mongoDbAccessor.findOneByCriteria(findByName, DMAConfigurationDocument.class);
		return configDoc;
	}


	@SuppressWarnings("unchecked")
	public void deleteConfiguration(ServiceData input) throws Exception {
		String user = (String)input.getValue("user");
		List<Map<String, Object>> actionChainTypeList = (List<Map<String, Object>>)input.getValue("actionChainTypeList");
		for(Map<String, Object> actionChainObj : actionChainTypeList){
			String actionChainType = (String)actionChainObj.get("actionChainType");
			mongoDbAccessor.delete(findConfigurationByName(actionChainType), user);
		}
		
    }
	
}
