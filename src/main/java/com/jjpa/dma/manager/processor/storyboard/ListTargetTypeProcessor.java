package com.jjpa.dma.manager.processor.storyboard;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardTargetTypeAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListTargetTypeProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListTargetTypeProcessor.class);
		
	protected StoryBoardTargetTypeAccessor storyBoardTargetTypeAccessor;

	public ListTargetTypeProcessor() {
		this.storyBoardTargetTypeAccessor = SpringApplicationContext.getBean("StoryBoardTargetTypeAccessor");
    }
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListTargetTypeProcessor");
		logger.debug("Process ===> ListTargetTypeProcessor : {}", input.toJsonString());
		
		return storyBoardTargetTypeAccessor.listStoryBoardTargetType(input);
		
	}

}