package com.jjpa.dma.manager.processor.info;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

/**
 * PropertyProcessor
 */
public class PropertyProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private final Logger logger = LoggerFactory.getLogger(PropertyProcessor.class);
    
    @Override
    public Object process(ServiceData input) throws Exception {
        logger.info("In PropertyProcessor");
        Map<String, Object> body = input.getData();
        Map<String, Object> header = input.getHeader();

        logger.info("body"+body);
        logger.info("header"+header);

       
   
        Map<String, Object> outputInfo = new LinkedHashMap<>();
       

        logger.info("outputInfo : " + outputInfo);
        return outputInfo;
    }
}