package com.jjpa.db.mongodb.accessor;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import org.springframework.data.mongodb.core.query.Query;

public interface EntityAccessor {
	
	public EntityDocument findEntityByName(String entityName) throws Exception;
	public Map<String,Object> listEntityByCriteria(Query query) throws Exception;
	public Map<String,Object> listEntity(ServiceData input) throws Exception;
	public List<EntityDocument> listFullEntityByCriteria(Query query) throws Exception;
	public void deleteEntity(ServiceData input) throws Exception;
	public Object saveEntity(EntityDocument document, String user) throws Exception;
//	public EntityDocument setEntityStatus(String entity, Date lastPublish, String user) throws Exception;
//	public EntityDocument setLastPublish(String entity, StoryBoardDocument story, Map<String, Object> storyBoardUiList, String user) throws Exception;
	public Object updateEntity(EntityDocument document, Date lastPublish, String user) throws Exception;
	public StoryBoardDocument findStoryBoardByEntity(String entity) throws Exception;
	
}
