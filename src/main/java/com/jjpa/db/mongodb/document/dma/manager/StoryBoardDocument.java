package com.jjpa.db.mongodb.document.dma.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jjpa.common.DmaConstants;
import com.jjpa.db.mongodb.document.BaseDocument;
import com.jjpa.dma.manager.model.reference.Reference;

import servicegateway.utils.ManagerMap;

@Document(collection = "story_board_config")
public class StoryBoardDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	@Indexed(unique = true)
	private String storyName;
	private String remark;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdate;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastPublish;
	private String status;
	private String firstPage;
	private String homePage;
	private String notificationPage;
	private Object tabInfo;
	private Object apiInfo;
	private Reference refference;
	private List<Map<String, Object>> pinList;
	private List<Map<String, Object>> storyBoard;
	private List<Map<String, Object>> screenList;
	private List<Map<String, Object>> groups;

	public StoryBoardDocument() {
	}

	public Object getApiInfo() {
		return apiInfo;
	}

	public void setApiInfo(Object apiInfo) {
		this.apiInfo = apiInfo;
	}

	public Object getTabInfo() {
		return tabInfo;
	}

	public void setTabInfo(Object tabInfo) {
		this.tabInfo = tabInfo;
	}

	public StoryBoardDocument(Map<String, Object> dataMap) throws Exception {
		
		
		/* ************************** set extends *********************************** */
		this.setId(ManagerMap.getString(dataMap, "id"));
		this.setCreateBy(ManagerMap.getString(dataMap, "createBy"));
		this.setLastUpdateBy(ManagerMap.getString(dataMap, "lastUpdateBy"));
		this.setDmaManagerVersion(ManagerMap.getString(dataMap, "dmaManagerVersion"));
		
		/* set create date */
		Long createDate = ManagerMap.getLong(dataMap, "createDate");
		if(createDate!=null){
			this.setCreateDate(new Date(createDate));
		}
		
		/* set lastUpdate date */
		Long lastUpdateDate = ManagerMap.getLong(dataMap, "lastUpdateDate");
		if(lastUpdateDate!=null){
			this.setLastUpdateDate(new Date(lastUpdateDate));
		}
		/* *************************************************************************** */
		
		
		/* ****************************** set this *********************************** */
		this.setStoryName(ManagerMap.getString(dataMap, "storyName"));
		this.setRemark(ManagerMap.getString(dataMap, "remark"));
		this.setStatus(ManagerMap.getString(dataMap, "status"));
		this.setFirstPage(ManagerMap.getString(dataMap, "firstPage"));
		this.setHomePage(ManagerMap.getString(dataMap, "homePage"));
		this.setNotificationPage(ManagerMap.getString(dataMap, "notificationPage"));
		
		Map<String, Object> referenceMap = ManagerMap.get(dataMap, "refference", Map.class);
		Reference reference = new ObjectMapper().convertValue(referenceMap, Reference.class);
		
		this.setReference(reference);
		this.setStoryBoard(ManagerMap.get(dataMap, "storyBoard", List.class));
		this.setScreenList(ManagerMap.get(dataMap, "screenList", List.class));
		this.setPinList(ManagerMap.get(dataMap, "pinList", List.class));
		this.setGroups(ManagerMap.get(dataMap, "groups", List.class));
		
		
		/* set lastUpdate */
		Long lastUpdate = ManagerMap.getLong(dataMap, "lastUpdate");
		if(lastUpdate!=null){
			this.setLastUpdate(new Date(lastUpdate));
		}else {
			Object lastUpdateObj = dataMap.get("lastUpdate");
			if(lastUpdateObj instanceof Date){
				this.setLastUpdate( (Date)lastUpdateObj );
			}
		}

		if(dataMap.containsKey(DmaConstants.StoryBoard.Key.TABINFO)){
			Map<String,Object> tabInfo =(Map<String,Object>) dataMap.get(DmaConstants.StoryBoard.Key.TABINFO);
			this.setTabInfo(tabInfo);
		}

		if(dataMap.containsKey(DmaConstants.StoryBoard.Key.APIINFO)){
			Map<String,Object> apiInfo =(Map<String,Object>) dataMap.get(DmaConstants.StoryBoard.Key.APIINFO);
			this.setApiInfo(apiInfo);
		}
		
		/* set lastPublish */
		Long lastPublish = ManagerMap.getLong(dataMap, "lastPublish");
		if(lastPublish!=null){
			this.setLastPublish(new Date(lastPublish));
		}else {
			Object lastPublishObj = dataMap.get("lastPublish");
			if(lastPublishObj instanceof Date){
				this.setLastPublish( (Date)lastPublishObj );
			}
		}
		/* *************************************************************************** */
		
	}
	
	public String getStoryName() {
		return storyName;
	}
	public void setStoryName(String storyName) {
		this.storyName = storyName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Date getLastPublish() {
		return lastPublish;
	}
	public void setLastPublish(Date lastPublish) {
		this.lastPublish = lastPublish;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFirstPage() {
		return firstPage;
	}
	public void setFirstPage(String firstPage) {
		this.firstPage = firstPage;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	public String getNotificationPage() {
		return notificationPage;
	}
	public void setNotificationPage(String notificationPage) {
		this.notificationPage = notificationPage;
	}
	public Reference getReference() {
		return refference;
	}
	public void setReference(Reference reference) {
		this.refference = reference;
	}
	public List<Map<String, Object>> getStoryBoard() {
		return storyBoard;
	}
	public void setStoryBoard(List<Map<String, Object>> storyBoard) {
		this.storyBoard = storyBoard;
	}
	public List<Map<String, Object>> getScreenList() {
		return screenList;
	}


	public List<Map<String, Object>> getScreenNameList() {
		List<Map<String,Object>> result = new ArrayList<>();
		
		for (int i = 0; i < storyBoard.size(); i++) {
			Map<String,Object> screen =	storyBoard.get(i);
			Map<String,Object> item = new LinkedHashMap<>();
			item.put("screenName", screen.get("source"));
			result.add(item);
		}
		return result;
	}

	public void setScreenList(List<Map<String, Object>> screenList) {
		this.screenList = screenList;
	}
	public List<Map<String, Object>> getPinList() {
		return pinList;
	}
	public void setPinList(List<Map<String, Object>> pinList) {
		this.pinList = pinList;
	}
	public List<Map<String, Object>> getGroups() {
		return groups;
	}
	public void setGroups(List<Map<String, Object>> groups) {
		this.groups = groups;
	}
	
	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate().getTime());
		map.put("lastUpdateDate", this.getLastUpdateDate().getTime());
		map.put("dmaManagerVersion", this.getDmaManagerVersion());
		
		map.put("storyName", this.getStoryName());
		map.put("remark", this.getRemark());
		map.put("status", this.getStatus());
		map.put("firstPage", this.getFirstPage());
		map.put("homePage", this.getHomePage());
		map.put("notificationPage", this.getNotificationPage());
		map.put("refference", this.getReference());
		map.put("storyBoard", this.getStoryBoard());
		map.put("screenList", this.getScreenList());
		map.put("pinList", this.getPinList());
		map.put("groups", this.getGroups());
		map.put(DmaConstants.StoryBoard.Key.TABINFO, this.getTabInfo());
		map.put(DmaConstants.StoryBoard.Key.APIINFO, this.getApiInfo());
		
		try{
			map.put("lastUpdate", this.getLastUpdate().getTime());
		}catch(Exception e){
			map.put("lastUpdate", "");
		}
		
		try{
			map.put("lastPublish", this.getLastPublish().getTime());
		}catch(Exception e){
			map.put("lastPublish", "");
		}
		
		return map;
	}
	
}
