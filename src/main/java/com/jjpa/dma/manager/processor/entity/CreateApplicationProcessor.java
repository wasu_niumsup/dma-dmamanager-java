package com.jjpa.dma.manager.processor.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.dma.manager.processor.binding.BindingBaseConfiguration;
import com.jjpa.dma.manager.processor.storyboard.FindByNameStoryBoardProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

/**
 * CreateApplicationProcessor
 */
public class CreateApplicationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

    private static final Logger logger = LoggerFactory.getLogger(CreateApplicationProcessor.class);

    protected EntityAccessor entityAccessor;

    public CreateApplicationProcessor() {
        this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");

    }

    @Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        logger.info("Process ===> CreateApplicationProcessor");
		logger.info("Process ===> CreateApplicationProcessor : {}", input.toJsonString());
		
		String user = (String)input.getValue(AppConstants.USER);
		String entityName = (String) input.getValue("applicationName");
        Map<String,Object> body = input.getData();
        
        /* 
		 * check duplicate entiry name. 
		 * */
		EntityDocument oldDoc = entityAccessor.findEntityByName( entityName );
		EntityDocument document = new EntityDocument();
		if((input.getValue(DmaConstants.Entity.Key.ID)!=null && !"".equals(((String)input.getValue(DmaConstants.Entity.Key.ID)).trim()))){
			String id = (String)input.getValue(DmaConstants.Entity.Key.ID);
			if(oldDoc!=null && !id.equals(oldDoc.getId())){
				// check rename case
				throw new Exception("Name duplicated");
			}
            document.setId(id);
            throw new Exception("Name duplicated");
		}else if(oldDoc != null){
			// check create case
        }
        
        /* set entity param. */
        document.setEntityName( entityName);
        document.setRemark( DataUtils.getStringValue(body,DmaConstants.Entity.Key.REMARK, "")  );
        document.setStoryName(   DataUtils.getStringValue(body,DmaConstants.Entity.Key.STORY_NAME, ""));
        document.setRole( DataUtils.getStringValue(body,DmaConstants.Entity.Key.ROLE, ""));
        document.setTheme(DataUtils.getStringValue(body,DmaConstants.Entity.Key.THEME, ""));
        document.setStatus( DataUtils.getStringValue(body,DmaConstants.Entity.Key.STATUS, "Active"));
        document.setVersion(  DataUtils.getStringValue(body,DmaConstants.Entity.Key.VERSION, "0.0")  );
        document.setPublishStatus(DmaConstants.PublishedStatus.ALL);
        document.setLastUpdate(new Date());
        document.setAppInfo(input.getValue(DmaConstants.Entity.Key.APP_INFO)   );
        document.setAppKey(input.getValue(DmaConstants.Entity.Key.APP_KEY));
        /* save entity to DB. */
        return entityAccessor.saveEntity(document, user);
    }
}