package com.jjpa.common;

public class JasonPathConstants {
	
	public class UiJson{
		
		public class Root{
			public static final String ACTIONS 					= "$.$jason.head.actions";
			public static final String SHOW 					= "$.$jason.head.actions.$show";
			public static final String SHOW_TYPE 				= "$.$jason.head.actions.$show.type";
			public static final String SET_LOCAL 				= "$.$jason.head.actions.initial_form.success.options";
		}
		
		public class Key{
			public static final String GET 						= "$get";
			public static final String RESULT_RESULT_DATA 		= "$get.result.resultData";
			public static final String RESULT_API_RESULT_DATA 	= "$get.result_api.resultData";
			public static final String SELECTED_DETAILS_ITEM 	= "$get.selected_details_item";
			public static final String RESULT_JASON			 	= "$jason.resultData";
			public static final String GLOBAL 					= "$global";
			
			public static final String OPTIONS 					= "options";
			public static final String ON_BACK 					= "$on.back";
			
			public static final String COORD_JASON				= "$jason.coord";
		}
		
		public class Value{
			public static final String UTIL_BACK 				= "$util.back";
		}
		
	}
	
	public class NativeJson{
		
		public class Path{
			public static final String TYPE 					= "$.type";
			public static final String OPTIONS 					= "$.options";
		}
	}
}
