package com.jjpa.dma.manager.processor.reference.inused.binding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;

import blueprint.util.StringUtil;

/* 16/03/2561 add by Akanit */
public class BindingInUsedReferenceToScreenProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingInUsedReferenceToScreenProcessor.class);

	public void process(AppUiDocument appUiDoc, StoryBoardDocument storyBoardDoc) throws Exception {
		logger.info("process : binding inUsed reference to Screen");
		
		if(!StringUtil.isBlank(storyBoardDoc.getStoryName())){
			
			Map<String, Object> storyBoardInUsed = new LinkedHashMap<>();
			storyBoardInUsed.put(ReferenceConstants.DMA.VERSION, new GetReferenceProcessor().getVersion(storyBoardDoc.getReference()));
			storyBoardInUsed.put(ReferenceConstants.DMA.NAME, storyBoardDoc.getStoryName());
			storyBoardInUsed.put(ReferenceConstants.DMA.TYPE, "");
			
			List<Map<String, Object>> storyBoardInUsedList = new ArrayList<>();
			storyBoardInUsedList.add(storyBoardInUsed);
			
			appUiDoc.setReference(new InitialReferenceProcessor().process(appUiDoc.getReference()));
			appUiDoc.getReference().getInUsed().setInUsedByDmaStroyBoard(storyBoardInUsedList);
			
			logger.info("Screen inUsed by storyBoard="+ storyBoardInUsedList);
		}
	}
}
