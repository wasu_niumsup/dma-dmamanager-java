package com.jjpa.dma.manager.processor.restoreimage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.mongodb.util.Hash;

/**
 * UploadImageAllProcessor
 */
public class UploadImageAllProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

    private static final Logger logger = LoggerFactory.getLogger(UploadImageAllProcessor.class);

    @Override
    public Object process(HttpServletRequest request, ServiceData serviceData) throws Exception {
        logger.info("[UploadImageAllProcessor] on process service !");

        Map<String, Object> input = serviceData.getData();
        // logger.info("[UploadImageAllProcessor] datainput =" + input);

        Map<String, Object> data = (Map<String, Object>) input.get("imageValidation");
        // logger.info("[UploadImageAllProcessor] imageValidation =" + data);

        ArrayList<Map<String, Object>> dataValue = (ArrayList<Map<String, Object>>) data.get("data");
        // logger.info("[UploadImageAllProcessor] data = " + dataValue);

        // if (dataValue == null) {
        // throw new Exception("Data of null please contact admin !");
        // } else {
        // // Donotting
        // }
        ArrayList<Map<String, Object>> warningList = new ArrayList<Map<String, Object>>();
        Map<String, Object> waringMap = new HashMap<String, Object>();
        boolean verifyWarning = true ;
        for (Map<String, Object> entry : dataValue) {
            Boolean default_binding = (Boolean) entry.get("default_binding");
            // logger.info("[UploadImageAllProcessor] default_binding ===> " +
            // default_binding);

            if (default_binding == false) {
                String fromRawUrl = entry.get("sourceRawUrl").toString();
                logger.info("[UploadImageAllProcessor] entry ==== >" + entry.toString());
                if (fromRawUrl.equals("")) {
                    throw new Exception("Data of fromRawUrl please contact admin !");
                }

                String tragetRawUrl = entry.get("targetDomain").toString();


                /**
                 * 25/03/2563
                 * Wut : verify tragetUrl  
                 * รบกวนกรอก url ที่ต้องการจะ call ด้วยนะครับ
                 */
                // String tragetRawUrl = getTragetUrl("url");

                if (tragetRawUrl.equals("")) {
                    throw new Exception("Data of tragetRawUrl please contact admin !");
                }

                // boolean verifykey = entry.containsKey("key");
                URL domain = new URL(fromRawUrl);
                Map<String , String> mapqury = splitQuery(domain);
                String keyImage = mapqury.get("key");
                // if(verifykey == true){
                //      keyImage = entry.get("key").toString();
                // } else {
                //     URL domain = new URL(fromRawUrl);
                //     Map<String , String> mapqury = splitQuery(domain);
                //     keyImage = mapqury.get("key");
                // }
             
                if (keyImage.equals("")) {
                    throw new Exception("Data of keyImage please contact admin !");
                }

                logger.info("[UploadImageAllProcessor] from ==> " + fromRawUrl + "  " + " to ==> " + tragetRawUrl);
                boolean verlifyDownloadurl = (boolean) downloadImg(fromRawUrl);
                if (verlifyDownloadurl == true) {
                    logger.info("verify dowload url = " + verlifyDownloadurl);
                    uploadFileToIMS(tragetRawUrl, keyImage);
                    delateFileImage();
                } else {

                    waringMap.put("uiname", entry.get("uiName"));
                    warningList.add(waringMap);
                    verifyWarning = false ;
                    logger.error("verify dowload url = " + verlifyDownloadurl);
                }
            }
        }
        input.put("verifyWarning", verifyWarning);
        input.put("warningList", warningList);
        return input;
    }

    public boolean downloadImg(String fromUrldownload) throws Exception {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(fromUrldownload);
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("[UploadImageAllProcessor] on pass download");
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                String filePath = "/tmp/avatar.png";
                FileOutputStream fos = new FileOutputStream(new File(filePath));
                // byte[] bytes = IOUtils.toByteArray(is);
                int inByte;
                while ((inByte = is.read()) != -1) {
                    fos.write(inByte);
                }

                is.close();
                fos.close();
                client.close();
                logger.info("[UploadImageAllProcessor] File Download Completed!!!");
                logger.info("[UploadImageAllProcessor] end process downloadImg");
            } else {
                logger.warn("[UploadImageAllProcessor] error call http get status code = "
                        + response.getStatusLine().getStatusCode());
                return false;
            }
        } catch (ClientProtocolException e) {
            logger.error("error downloadImg ", e);
        } catch (UnsupportedOperationException e) {
            logger.error("error downloadImg ", e);
        } catch (IOException e) {
            logger.error("error downloadImg ", e);
        }
        return true;
    }

    public void delateFileImage() {
        try {
            File file = new File("/tmp/avatar.png");
            if (file.delete()) {
                logger.info("[UploadImageAllProcessor] delete file");
                logger.info("[UploadImageAllProcessor] end process delete file");
            } else {
                logger.info("[UploadImageAllProcessor] error delete file");
            }
        } catch (Exception e) {
            // TODO: handle exception
            logger.info("[UploadImageAllProcessor]" + e.getMessage());
        }
    }

    public void uploadFileToIMS(String toDomain, String keyImage) throws Exception {
        try {
            logger.info("[UploadImageAllProcessor] upload ==> on pass uploadFileToIMS");

            logger.info("verlify path");
            File file = new File("/tmp/avatar.png");
            if (file.exists() == false) {
                throw new Exception("can't file download image please contact admin");
            }

            MediaType MEDIA_IMAGE = MediaType.parse("image/jpeg");
            OkHttpClient client = new OkHttpClient().newBuilder().build();
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("files[]", "avatar.png",
                            RequestBody.create(MEDIA_IMAGE, new File("/tmp/avatar.png")))
                    .addFormDataPart("key", keyImage).build();

            logger.info("[UploadImageAllProcessor] upload request body ==>" + body.toString());

            Request request = new Request.Builder().url(toDomain + "/ims/apis/file/UploadPermanent")
                    .method("POST", body).build();
            Response response = client.newCall(request).execute();
            logger.info("[UploadImageAllProcessor] end process uploadFileToIMS");
            logger.info("[UploadImageAllProcessor] end process respones ==>" + response.body().string());
        } catch (Exception e) {
            // TODO: handle exception
            logger.error("[UploadImageAllProcessor] upload Exception ==>" + e.getMessage());
        }
    }

    public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    public static String getTragetUrl(String fromUrl) throws Exception {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
            .url(fromUrl)
            .post(null)
            .build();

            Response response = client.newCall(request).execute();
            if(response.code() == 200){
                DocumentContext documentContext = JsonPath.parse(response.body().string()); 
                Map<String , String > mapResponse = documentContext.json();
                return mapResponse.get("tragetUrl");
            } else {
                throw new Exception("Call service get traurl error please contact admin status code = " + response.code());
            }

        } catch (Exception e) {
            //TODO: handle exception
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }
}
