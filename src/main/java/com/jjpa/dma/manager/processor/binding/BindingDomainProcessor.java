package com.jjpa.dma.manager.processor.binding;

import java.util.List;
import java.util.Map;

import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;

import servicegateway.utils.ManagerMap;

public class BindingDomainProcessor {

//	private static final Logger logger = LoggerFactory.getLogger(BindingDomainProcessor.class);

	public void bindDMADomainToImageUrlInGroup(Map<String, Object> group) throws Exception {
		
		List<Map<String, Object>> screenList = ManagerMap.get(group, "screen", List.class);
		if( screenList==null){
			return;
		}
		
		DMADomainProcessor dmadp = new DMADomainProcessor();
		
		String urlScreenShot;
		for(Map<String, Object> screen : screenList){
			
			urlScreenShot = ManagerMap.getString(screen, "imageUrl"); 
			
			if( dmadp.isUrlWithDomain(urlScreenShot) && !dmadp.isUrlAutoGenImage(urlScreenShot) ){
				screen.put("imageUrl", dmadp.bindingDMADomain(urlScreenShot));
			}
			
		}
		
		group.put("screen", screenList);
	}
	
	public void bindInternalDomainToImageUrlInGroup(Map<String, Object> group) throws Exception {
		
		List<Map<String, Object>> screenList = ManagerMap.get(group, "screen", List.class);
		if( screenList==null){
			return;
		}
		
		for(Map<String, Object> screen : screenList){
			screen.put("imageUrl", new DMADomainProcessor().bindingInternalDomain(ManagerMap.getString(screen, "imageUrl")));
		}
		
		group.put("screen", screenList);
	}

}
