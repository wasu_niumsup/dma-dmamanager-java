package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;

public interface AppUiAccessor {
	
	public List<AppUiDocument> findAll() throws Exception;
	public AppUiDocument findAppUiByName(String uiName) throws Exception;
	public Map<String,Object> listAppUi(ServiceData input) throws Exception;
	public Map<String,Object> listAppUiWithAction(ServiceData input) throws Exception;
	public Map<String,Object> listAppUiByCriteria(Query query) throws Exception;
	public void deleteAppUi(ServiceData input) throws Exception;
	public Object saveUi(AppUiDocument document, String user) throws Exception;
	public Object restoreUi(AppUiDocument document, String user) throws Exception;
	public Object upgradeUi(AppUiDocument document, String user) throws Exception;
	public Object updateUi(AppUiDocument document, String user) throws Exception;
	public boolean needUpgrade(AppUiDocument document) throws Exception;
	public int count(Query query) throws Exception;
}
