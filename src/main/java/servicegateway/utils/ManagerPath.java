package servicegateway.utils;

import java.io.File;

public class ManagerPath {

	public static String toPath(String path) throws Exception {
		if(path==null){
			return null;
		}
		if("".equals(path)){
			return "/";
		}
		if(!"/".equals(path.substring(path.length()-1))){
			path += File.separator;
		}
		return path;
	}
	
}
