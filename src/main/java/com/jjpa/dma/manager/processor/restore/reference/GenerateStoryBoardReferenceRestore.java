package com.jjpa.dma.manager.processor.restore.reference;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.dma.manager.model.reference.RequiredReference;

public class GenerateStoryBoardReferenceRestore {

	private static Logger logger = LoggerFactory.getLogger(GenerateStoryBoardReferenceRestore.class);
	
	public void process(RequiredReference reqRef, String filePath) throws Exception {
		logger.info("Process ===> GenerateStoryBoardReferenceRestore");
		
		String name, version,dmaManagerVersion;
		Map<String, Object> map;
		List<Map<String, Object>> resultList = new ArrayList<>();		
		
		File file = new File(filePath);
		if(file.exists()){
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
							
					try{
						version = JsonPath.read(fileEntry, "$." + "reference.version");
					}catch(Exception e){
						version = null;
					}
					
					try{
						name = JsonPath.read(fileEntry, "$." + "storyName");
					}catch(Exception e){
						name = null;
					}

					
					try{
						dmaManagerVersion = JsonPath.read(fileEntry, "$." + "dmaManagerVersion");
					}catch(Exception e){
						dmaManagerVersion = null;
					}
					
					map = new LinkedHashMap<>();
					map.put(ReferenceConstants.DMA.VERSION, version);
					map.put(ReferenceConstants.DMA.NAME, name);
					map.put("dmaManagerVersion", dmaManagerVersion);
					resultList.add(map);
				}
			}
		}
		
		reqRef.setDmaStroyBoardRequired(resultList);
	}
}
