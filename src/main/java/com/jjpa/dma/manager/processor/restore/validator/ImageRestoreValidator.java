package com.jjpa.dma.manager.processor.restore.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.BackupConstants;

public class ImageRestoreValidator {

	private static Logger logger = LoggerFactory.getLogger(ImageRestoreValidator.class);
	
//	protected TemplateAccessor templateAccessor;
//	
//	public ImageRestoreValidator() {
//		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
//	}
	
	public Map<String, Object> process(List<Map<String, Object>> imageRequiredList) throws Exception {
		logger.info("Process ===> ImageRestoreValidator");
		
		boolean validationStatus = true;
//    	String version_restore, version_old, name;
    	
    	for(Map<String, Object> imageRequired : imageRequiredList){
    		
    		imageRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
    		
    		
//    		version_restore = ManagerMap.getString(templateRequired, ReferenceConstants.DMA.VERSION);
//    		name = ManagerMap.getString(templateRequired, ReferenceConstants.DMA.NAME);
//    		
//    		DMATemplate doc = templateAccessor.findTemplateByName(name);
//    		
//    		
//    		/* validate add status */
//    		if(doc==null){
//    			logger.info("#not found old Template="+ name);
//    			logger.info("#update action=ADD");
//    			logger.info("#update status=true");
//				
//    			templateRequired.put(BackupConstants.Validator.Key.STATUS, BackupConstants.Validator.Status.TRUE);
//    			continue;
//    			
//    		}else {
//    			logger.info("#found old Template="+ name);
//    		}
    		
    		
//    		try {
//    			version_old = doc.getVersionTemplate();
//			} catch (Exception e) {
//				logger.info("#Template="+ name + "not have reference.");
//				logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				templateRequired.put("status", true);
//				continue;
//			}
//    		
//    		
//    		
//    		if(version_restore == version_old){
//    			logger.info("#Template="+ name + " can restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//				
//				templateRequired.put("status", true);
//    			
//    		}else{
//    			logger.info("#Template="+ name + " can not restore.");
//    			logger.info("#update action=UPDATE");
//    			logger.info("#update status=true");
//    			
////    			validationStatus = false;				
//				templateRequired.put("status", true);
//    		}
    		
    	}
    	
    	/* mapping data to UI */
    	Map<String, Object> output = new LinkedHashMap<>();
    	output.put("summaryValidation", validationStatus);
    	output.put("data", imageRequiredList);
    	
    	return output;
	}
}
