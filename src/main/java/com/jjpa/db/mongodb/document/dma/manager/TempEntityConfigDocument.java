package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "temp_entity_config")
public class TempEntityConfigDocument extends EntityConfigDocument{

	private static final long serialVersionUID = 1L;
	
}
