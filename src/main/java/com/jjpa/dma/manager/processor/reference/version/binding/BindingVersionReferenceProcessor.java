package com.jjpa.dma.manager.processor.reference.version.binding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.dma.manager.model.reference.Reference;

/* 16/03/2561 add by Akanit */
public class BindingVersionReferenceProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingVersionReferenceProcessor.class);
	
	public void process(Reference reference) throws Exception {
		logger.info("process : binding version reference.");
		
		String version;
		
		if(reference == null){
			String errorMessage = "Reference is null, Please initial reference.";
			logger.info(errorMessage);
			throw new Exception(errorMessage);
			
		}else if(reference.getVersion() == null){
			version = "1";
			logger.info("#version=null, initial reference version=1");
		}else{
			version = String.valueOf(Integer.parseInt(reference.getVersion()) + 1);
			logger.info("#up reference version to "+ version);
		}
		
		reference.setVersion(version);
	}

}
