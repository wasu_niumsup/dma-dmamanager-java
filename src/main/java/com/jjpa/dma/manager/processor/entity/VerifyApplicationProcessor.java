package com.jjpa.dma.manager.processor.entity;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import blueprint.util.StringUtil;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.dma.manager.processor.binding.BindingBaseConfiguration;
import com.jjpa.dma.manager.processor.storyboard.FindByNameStoryBoardProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
/**
 * VerifyApplicationProcessor
 */
public class VerifyApplicationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    
    private static final Logger logger = LoggerFactory.getLogger(VerifyApplicationProcessor.class);

    protected EntityAccessor entityAccessor;

    public VerifyApplicationProcessor() {
        this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");

    }

    @Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        logger.info("Process ===> VerifyApplicationProcessor");
		logger.info("Process ===> VerifyApplicationProcessor : {}", input.toJsonString());
		
		String user = (String)input.getValue(AppConstants.USER);
		String entityName = (String) input.getValue("applicationName");
        Map<String,Object> body = input.getData();

        if(StringUtil.isBlank(entityName)){
            throw new ValidationException("This applicationName can't be blank.");
        }
        
        /* 
		 * check duplicate entiry name. 
		 * */
        EntityDocument oldDoc = entityAccessor.findEntityByName( entityName );
        Boolean foundApplication = false;
        if(oldDoc == null){
            foundApplication = false;
        }else{
            foundApplication = true;
        }
        
        if(foundApplication){
            throw new ValidationException("ApplicationName is already used. Please select to new application name");
        }
        
        Map<String,Object> output = new LinkedHashMap<>();
        output.put("applicationName", entityName);
     
        return output;
    }
}