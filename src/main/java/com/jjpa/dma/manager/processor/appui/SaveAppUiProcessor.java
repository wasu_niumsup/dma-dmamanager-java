package com.jjpa.dma.manager.processor.appui;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.binding.BindingComponentProcessor;
import com.jjpa.dma.manager.processor.binding.BindingDynamicListActionProcessor;
import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;
import com.jjpa.dma.manager.processor.reference.DeleteReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.InitialReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToTemplateProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToScreenProcessor;
import com.jjpa.dma.manager.processor.reference.version.binding.BindingVersionReferenceProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
public class SaveAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(SaveAppUiProcessor.class);

	protected AppUiAccessor appUiAccessor;
	protected TemplateAccessor templateAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	private String user;

	public SaveAppUiProcessor() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Process ===> SaveAppUiProcessor");
		logger.debug("Process ===> SaveAppUiProcessor : {}", input.toJsonString());
		
		return saveAppUi(input);
	}
	
	private Object saveAppUi(ServiceData input) throws Exception {

		user = (String) input.getValue(AppConstants.USER);
		String uiName = (String) input.getValue(DmaConstants.AppUI.Key.UI_NAME);
		// Pongpat add  ValidateValueFistNumber 
		if(uiName.matches("^[0-9].*")){
			throw new Exception(
					"Screen name cannot initiate with numeric.");
		}
		
		if (DmaManagerConstant.BACK_TYPE.contains(uiName.toLowerCase().trim())) {
			throw new Exception(
					"Screen name must not be reserved word. (\"firstpage\", \"home\", \"stack\", \"none\")");
		} else {
			
			AppUiDocument oldDoc = appUiAccessor.findAppUiByName(uiName);
			AppUiDocument appUiDoc = new AppUiDocument();
			if ((input.getValue(DmaConstants.AppUI.Key.ID) != null && !"".equals(((String) input.getValue(DmaConstants.AppUI.Key.ID)).trim()))) {
				String id = (String) input.getValue(DmaConstants.AppUI.Key.ID);
				if (oldDoc != null && !id.equals(oldDoc.getId())) {
					// check rename case
					throw new Exception("Name duplicated.");
				}
				appUiDoc.setId(id);
			} else if (oldDoc != null) {
				// check create case
				throw new Exception("Name duplicated.");
			}
			
			if (oldDoc != null) {
				logger.info("Process ===> Is 'update' Screen, then check needUpgrade.");
				
				/* 01/10/2561 add by Akanit : check needUpgrade, if needUpgrade then don't save.*/
				if(appUiAccessor.needUpgrade(oldDoc)){
					throw new Exception("Please press \"Upgrade\" before edit screen.");
				}
				
				appUiDoc.setReference(oldDoc.getReference());
				
			}else{
				logger.info("Process ===> Is 'create' Screen");
			}
			
			appUiDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
			appUiDoc.setUiName(uiName);
			appUiDoc.setRemark((String) input.getValue(DmaConstants.AppUI.Key.REMARK));
			appUiDoc.setUiConfigData((Map<String, Object>) input.getValue(DmaConstants.AppUI.Key.UI_CONFIG_DATA));
			appUiDoc.setDynamicApi((String) input.getValue(DmaConstants.AppUI.Key.DYNAMIC_API));
			appUiDoc.setTemplateType((String) input.getValue(DmaConstants.AppUI.Key.TEMPLATE_TYPE));
			appUiDoc.setDynamicInputList((List<Map<String, Object>>) input.getValue(DmaConstants.AppUI.Key.DYNAMIC_INPUT_LIST));
			appUiDoc.setDynamicOutputList((List<Map<String, Object>>) input.getValue(DmaConstants.AppUI.Key.DYNAMIC_OUTPUT_LIST));
//			appUiDoc.setActionList( (List<Map<String, Object>>)input.getValue(DmaConstants.AppUI.Key.ACTION_LIST) );

			/* 25/03/2562 add by Akanit : requirement from siam */
			try{
				appUiDoc.setUrlScreenShot((String) input.getValue(DmaConstants.AppUI.Key.URL_SCREEN_SHOT));
			} catch (Exception e){
				logger.info("not found '"+ DmaConstants.AppUI.Key.URL_SCREEN_SHOT +"' then setUrlScreenShot = ''");
				appUiDoc.setUrlScreenShot("");
			}
			
			
			/* 29/05/2562 add by Akanit : {{$get._dma_param_internal_domain}} */
			DMADomainProcessor dmadp = new DMADomainProcessor();
			
			String urlScreenShot = appUiDoc.getUrlScreenShot();
			
			if( dmadp.isUrlWithDomain(urlScreenShot) && !dmadp.isUrlAutoGenImage(urlScreenShot) ){
				appUiDoc.setUrlScreenShot(dmadp.bindingDMADomain(urlScreenShot));
			}
			

			/* 25/03/2562 () comment by Akanit : Why i comment this log? because data of log is saved to database, you can see data at database mongo. */
//			logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getUiName " + appUiDoc.getUiName());
//			logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getUiConfigData " + appUiDoc.getUiConfigData());
//			logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getDynamicApi " + appUiDoc.getDynamicApi());
//			logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getTemplateType " + appUiDoc.getTemplateType());
//
//			logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getDynamicInputList " + appUiDoc.getDynamicInputList());
//          logger.info("Process ===> SaveAppUiProcessor upgradeAppUi appUiDoc.getDynamicOutputList " + appUiDoc.getDynamicOutputList());

			
			if( !DataUtils.Validate.validateRegex(appUiDoc.getUiName(), DataUtils.Validate.NORMAL_PATTERN)){
				logger.warn("Format Screen name is invalid");
				throw new ValidationException("Format Screen name is invalid.", "VV009");
			}

//			if( appUiDoc.getActionList() == null ){
//				logger.warn("Invalid screen because the action list is null.");
//				throw new ValidationException("Invalid screen because the action list is null.", "VV010");
//			}

			/* 12/03/2561 add by Akanit : get template config */
			String templateName = (String) appUiDoc.getUiConfigData().get(DmaConstants.AppUI.Key.TEMPLATE);
			DMATemplate template = templateAccessor.findTemplateByName(templateName);



			/**
			 * Tanawat W.
			 * 2018-12-18
			 * 
			 * DMA 5.0
			 * 
			 */
			logger.info("template.getVersionTemplate() : " + template.getVersionTemplate());
			int version  = Integer.parseInt(  template.getVersionTemplate().split("\\.")[0] );
			if(version >= 5){
				appUiDoc.setActionList((List<Map<String, Object>>)input.getValue(DmaConstants.AppUI.Key.ACTION_LIST));
				appUiDoc.setInputList( (List<Map<String, Object>>)input.getValue("templateInputList")  );

				logger.info("[DMA 5.0 ]set value to ActionList and InputList " + template.getVersionTemplate());
			}else{
				// do nothing
			}



			/* 12/03/2561 add by Akanit */
			appUiDoc.setDynamicDataBinding(template.getDynamicDataBinding());

			/* 04/01/2561 add by Akanit : get jsonTemplate */
			Map<String, Object> dataBinding = getJsonTemplate(template);
			appUiDoc.setUiJson(generateUiJson(dataBinding, appUiDoc));
			
			/* 17/05/2561 add by Akanit */
			appUiDoc.setLastUpdate(new Date());

			/* 08/01/2561 add by Akanit */
			new BindingDynamicListActionProcessor().process(appUiDoc);

			
			
			/* 
			 * Reference (own)
			 * */
			
			/* 16/03/2561 add by Akanit : create reference.inUsed, reference.required */
			Reference reference = new InitialReferenceProcessor().process(appUiDoc.getReference());
			new DeleteReferenceProcessor().clearRequired(reference);
			appUiDoc.setReference(reference);

			/* 16/03/2561 add by Akanit : update version reference */
			new BindingVersionReferenceProcessor().process(appUiDoc.getReference());

			/* 16/03/2561 add by Akanit : update required reference to AppUI(screen) */
			new BindingRequiredReferenceToScreenProcessor().process(appUiDoc, template);

			
			
			/**
			 * Tanawat W.
			 * 
			 * Test plugin-react
			 */
			Map<String,Object> resultContext = callHook("SaveScreen", input.getData()).json();
			
			if( ManagerMap.getBoolean(resultContext, "resultSuccess") == false) {
				throw new Exception(ManagerMap.getString(resultContext, "resultMessage"));
			}

			
			
			Object output = appUiAccessor.saveUi(appUiDoc, user);
			
			
			
			/* 
			 * Reference (not own)
			 * */

			/* 16/03/2561 add by Akanit : clear inUsed reference to dmaTemplate old */
			String templateName_old = null;
			if(oldDoc!=null){
				templateName_old = new GetReferenceProcessor().getDmaTemplateRequired(oldDoc.getReference());
			}
			clearInUsedReferenceToOldTemplate(templateName_old, templateName, appUiDoc.getUiName());
			
			/* 16/03/2561 add by Akanit : update inUsed reference to dmaTemplate new */
			new BindingInUsedReferenceToTemplateProcessor().process(template, appUiDoc);
			
			templateAccessor.saveTemplate(template, user);
			
			
			
			/* 20/03/2562 add by Akanit : 
			 * 21/05/2562 comment by Akanit : Jira DMA-1437 : Storyboard should not save upgrade
			 * */
//			storyBoardAccessor.updateVersionScreenToStoryboard(appUiDoc, false);
			
			/* 21/05/2562 add by Akanit : Jira DMA-1437 : Storyboard should not save upgrade */
			storyBoardAccessor.updateVersionScreenToStoryboard(appUiDoc);

			
			/* 
			 * Binding Groups DMA.5.3.0
			 * */
			
			/* 08/05/2562 (080525621421) add by Akanit : DMA.5.3.0 : binding groups to storyboard */
			storyBoardAccessor.updateGroups(appUiDoc, user);
			
			
			return output;
		}
	}

	/* 16/03/2561 add by Akanit */
	private void clearInUsedReferenceToOldTemplate(String templateName_old, String templateName_new, String screenName) throws Exception {

		if (templateName_old != null && !templateName_old.equals(templateName_new)) {
			logger.info("#found old template, then clear InUsed Reference of old template.");

			DMATemplate template_old = templateAccessor.findTemplateByName(templateName_old);

			new DeleteReferenceProcessor().clearInUsedByDmaScreen(template_old.getReference(), screenName);

			templateAccessor.saveTemplate(template_old, user);
		}
	}

	/* 04/01/2561 add by Akanit */
	/* 12/03/2561 edit by Akanit */
	private Map<String, Object> getJsonTemplate(DMATemplate template) throws Exception {

		/* get templateJson */
		DocumentContext ctx = JsonPath.parse(template.getNativeJson().get("data"));

		/* create dataBinding from templateJson (old system : dataBinding is templateJson) */
		Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());

		return dataBinding;
	}

	/* 04/01/2561 add by Akanit */
	private String generateUiJson(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception {
		logger.info("#do generateUiJson");

		logger.info("#call BindingComponentProcessor()");
		new BindingComponentProcessor().process(dataBinding, appUiDoc);
		String uiJson = JsonPath.parse(dataBinding).jsonString();
		//		logger.info("#BindingComponentProcessor dataBinding="+uiJson);

		return uiJson;
	}

}
