package com.jjpa.db.mongodb.accessor;

import java.util.List;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;

public interface TempEntityConfigAccessor {
	
	/* 04/01/2562 add by Akanit : data is very large size. */
	public void loadRelateData(TempEntityConfigDocument document, String storyName) throws Exception;
	public void moveRelateData(TempEntityConfigDocument document, String user) throws Exception;
	public void deleteRelateData(String storyName, String user) throws Exception;
	public List<TempEntityConfigDocument> findAllNotGetRelateData() throws Exception;
	
	public TempEntityConfigDocument findTempConfigByName(String entityName) throws Exception;
	public void deleteTempConfig(ServiceData input) throws Exception;
	public Object saveTempConfig(TempEntityConfigDocument document, String user) throws Exception;
}
