package com.jjpa.dma.manager.processor.backup;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;
import servicegateway.utils.ManagerStore;

/**
 * GenerateImageFloderBackup
 */
public class GenerateImageFolderBackup {
	
    private static final Logger logger = LoggerFactory.getLogger(GenerateImageFolderBackup.class);

    public static class Key {
    	
    	public static final String IMAGE_TYPE 		= "imageType";
        public static final String FROM_DOMAIN 		= "fromDomain";
        public static final String DOMAIN 			= "domain";
        public static final String RAW_URL 			= "rawUrl";
        public static final String WITHOUT_URL 		= "withoutUrl";
        public static final String IS_INTERNAL 		= "isInternal";
        public static final String UI_NAME 			= "uiName";
        public static final String DEFAULT_BINDING 	= "default_binding";
        public static final String KEY 				= "key";

        public static final String TARGET_DOMAIN 	= "targetDomain";
        public static final String TARGET_RAWURL 	= "targetRawUrl";
        public static final String SOURCE_RAWURL 	= "sourceRawUrl";

    }
    
    public static class ImageType {
        
    	public static final String IMAGE 			= "image";
    	public static final String PREVIEW_TEMPLATE = "previewTemplate";
    	public static final String SCREEN_SHOT 		= "screenShot";

    }

    protected AppUiAccessor appUiAccessor;
    protected TemplateAccessor templateAccessor;
    

    public GenerateImageFolderBackup() {
        this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
        this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
    }

    
    
	public void process(Reference reference, String directoryFileBackup) throws Exception {
        logger.info("process : generate ims backup folder");
        
        List<Map<String, Object>> screenRequiredList = new GetReferenceProcessor().getDmaScreenRequireds(reference);
		if(screenRequiredList==null){
			return;
		}
		
        /* example 
         * directoryFile =  {directoryFileBackup}/IMS/{screenName}
         * */
        String directoryFile = directoryFileBackup + File.separator + BackupConstants.FolderName.DMA.IMS
                + File.separator + "allScreen";

        /* save file */
        ManagerStore.createFileByJsonMap(generateJsonData(screenRequiredList), directoryFile);

    }

	private Map<String, Object> generateJsonData(List<Map<String, Object>> screenList) throws Exception {
		
		String internalDomain = ConfigurationUtils.getPublishUrl().toLowerCase();
		
		
		String screenName, templateName;
        AppUiDocument screenDoc;
        DMATemplate templateDoc;
        List<Map<String, Object>> urlList = new ArrayList<>();
        for (Map<String, Object> screen : screenList) {

            /* find screen in mongo */
            screenName = ManagerMap.getString(screen, ReferenceConstants.DMA.NAME);
            screenDoc = appUiAccessor.findAppUiByName(screenName);
            
            /* 30/05/2562 add by akanit : {{$get._dma_param_internal_domain}} */
            screenDoc.setUrlScreenShot(new DMADomainProcessor().bindingInternalDomain(screenDoc.getUrlScreenShot()));
            
            
            /* find template of screen in mongo */
            templateName = new GetReferenceProcessor().getDmaTemplateRequired(screenDoc.getReference());
            templateDoc = templateAccessor.findTemplateByName(templateName);

            
            /* bingding 'urlList' -> domain, rawUrl, key, etc. */
            addImage(urlList, screenDoc, internalDomain);
            addPreviewTemplateImage(urlList, screenDoc, templateDoc, internalDomain);
            addScreenShotImage(urlList, screenDoc, internalDomain);

        }
        
        
        Map<String, Object> jsonData = new LinkedHashMap<>();
        jsonData.put("urlList", urlList);
		return jsonData;
	}


	private void addImage(List<Map<String, Object>> urlList, AppUiDocument screenDoc, String internalDomain) throws Exception {
		
		List<Map<String, Object>> bindingList = ManagerMap.get(screenDoc.getUiConfigData(), "binding", List.class);
        if (bindingList != null) {
			
			DocumentContext uiJson = JsonPath.parse(screenDoc.getUiJson());
			
			/* 28/05/2562 add by Akanit : move variable out of for loop. because it takes a lot of memory. */
			boolean defaultBinding, isInternal;
			Object readValueFromDefault;
			Map<String, Object> image;
			String componentType, value, jsonPath, valueFromDefault, backupJsonPath, queryString, url, baseUrl, withoutDomain;
		    for (Map<String, Object> item : bindingList) {
		    	
		        componentType = ManagerMap.getString(item, DmaConstants.AppUI.Key.COMPONENT_TYPE, "string");

		        if ("image".equalsIgnoreCase(componentType)) {
		            value = DataUtils.getStringValue(item, "value", "");
		            jsonPath = DataUtils.getStringValue(item, "path", null);
		            defaultBinding = DataUtils.getBooleanValue(item, "default", true);

		            valueFromDefault = "";
		            readValueFromDefault = uiJson.read(jsonPath);
		            
		            if (readValueFromDefault instanceof String) {
		                valueFromDefault = (String) readValueFromDefault;
		            } else {
		                logger.warn("screen : " + screenDoc.getUiName() + " ,error path : " + jsonPath
		                        + " , beacase image value must be string : " + readValueFromDefault);
		                try {
		                    if (item.containsKey("backup")) {

		                        backupJsonPath = (String) JsonPath.read(item, "$.backup.path");
		                        logger.warn("attemp read backup path : " + backupJsonPath);
		                        valueFromDefault = uiJson.read(backupJsonPath);
		                    }
		                } catch (Exception e) {
		                    logger.warn("screen : " + screenDoc.getUiName() + " , Not found backup path ");
		                }

		            }

		            url = defaultBinding ? valueFromDefault : value;

		            
		            /**
		             * 
		             * Ex. "{{$get._dma_param_internal_domain}}/ims/axxx.jpg";
		             */
		            if (url.trim().indexOf("{{") == 0) {

		                withoutDomain = url.substring(url.indexOf("}}") + 2);
		                url = getDomainName(internalDomain, true) + withoutDomain;
		            }

		            
		            baseUrl = getDomainName(url, true);
		            isInternal = baseUrl.equalsIgnoreCase(getDomainName(internalDomain, true));

		            
		            image = new LinkedHashMap<>();
		            
		            /* 28/05/2562 add by Akanit : add imageType  */
		            image.put(Key.IMAGE_TYPE, ImageType.IMAGE);
		    		
		            image.put(Key.FROM_DOMAIN, getDomainName(internalDomain, true));
		            image.put("_extra", item);

		            image.put(Key.DEFAULT_BINDING, defaultBinding);
		            image.put(Key.DOMAIN, baseUrl);
		            image.put(Key.RAW_URL, url);

		            image.put(Key.WITHOUT_URL, url.replace(baseUrl, ""));

		            image.put(Key.IS_INTERNAL, isInternal);
		            image.put(Key.UI_NAME, screenDoc.getUiName());
		            
		            if (isInternal) {
		                queryString = (String) image.get(Key.WITHOUT_URL);
		                
//		                logger.info("find bug " + screenDoc.getUiName() + " queryString : " + queryString);
		                if(StringUtil.isBlank(queryString)){
		                    logger.info("queryString is blank");
		                }else{
		                    image.put(Key.KEY, queryString.substring(queryString.indexOf("key=") + 4));
		                }

		            }

		            urlList.add(image);
		        }

		    }
		}
		
	}
	
	private void addPreviewTemplateImage(List<Map<String, Object>> urlList, AppUiDocument screenDoc,
			DMATemplate screenUsingTemplate, String internalDomain) {

		String url = screenUsingTemplate.getInfoUrl();
		/* 18/06/2561 add by Tanawat : fix url don't null */
		if(url == null){
			url = "";
		}

		/* 28/05/2562 add by Akanit : This method 'getDomainName' is working? 
		 * You can see detail inside this method 'getDomainName'. */
		url = url.replace("{{$get._dma_param_internal_domain}}", getDomainName(internalDomain, true));
				
		String baseUrl = getDomainName(url, true);
		boolean isInternal = baseUrl.equalsIgnoreCase(getDomainName(internalDomain, true));

		
		Map<String, Object> previewTemplateImage = new LinkedHashMap<>();
		
		/* 28/05/2562 add by Akanit : add imageType  */
		previewTemplateImage.put(Key.IMAGE_TYPE, ImageType.PREVIEW_TEMPLATE);
		
		previewTemplateImage.put(Key.FROM_DOMAIN, getDomainName(internalDomain, true));
		previewTemplateImage.put("_extra", null);
		previewTemplateImage.put(Key.DEFAULT_BINDING, true);
		previewTemplateImage.put(Key.DOMAIN, baseUrl);
		previewTemplateImage.put(Key.RAW_URL, url);

		previewTemplateImage.put(Key.WITHOUT_URL, url.replace(baseUrl, ""));

		previewTemplateImage.put(Key.IS_INTERNAL, isInternal);
		previewTemplateImage.put(Key.UI_NAME, screenDoc.getUiName());
		if (isInternal) {
		    String queryString = (String) previewTemplateImage.get(Key.WITHOUT_URL);
		    previewTemplateImage.put(Key.KEY, queryString.substring(queryString.indexOf("key=") + 4));
		}
		urlList.add(previewTemplateImage);
	}

	private void addScreenShotImage(List<Map<String, Object>> urlList, AppUiDocument screenDoc, String internalDomain) {
		
		String url = screenDoc.getUrlScreenShot();
		if(StringUtil.isBlank(url)){
			return;
		}
		
		/* 28/05/2562 add by Akanit : This method 'getDomainName' is working? 
		 * You can see detail inside this method 'getDomainName'. */
		url = url.replace("{{$get._dma_param_internal_domain}}", getDomainName(internalDomain, true));

		String baseUrl = getDomainName(url, true);
		boolean isInternal = baseUrl.equalsIgnoreCase(getDomainName(internalDomain, true));

		
		Map<String, Object> screenShotImage = new LinkedHashMap<>();
		
		/* 28/05/2562 add by Akanit : add imageType  */
		screenShotImage.put(Key.IMAGE_TYPE, ImageType.SCREEN_SHOT);
		
		screenShotImage.put(Key.FROM_DOMAIN, getDomainName(internalDomain, true));
		screenShotImage.put("_extra", null);
		screenShotImage.put(Key.DEFAULT_BINDING, false);
		screenShotImage.put(Key.DOMAIN, baseUrl);
		screenShotImage.put(Key.RAW_URL, url);

		screenShotImage.put(Key.WITHOUT_URL, url.replace(baseUrl, ""));

		screenShotImage.put(Key.IS_INTERNAL, isInternal);
		screenShotImage.put(Key.UI_NAME, screenDoc.getUiName());
		if (isInternal) {
		    String queryString = (String) screenShotImage.get(Key.WITHOUT_URL);
		    screenShotImage.put(Key.KEY, queryString.substring(queryString.indexOf("key=") + 4));
		}
		urlList.add(screenShotImage);
	}
	
	
	/* 
	 * Question by Akanit : 29/05/2562
	 * - This method is working? 
	 * 
	 * When you call this method, you will get a url without 'www.'
	 * 
	 * Example
	 * 	input 
	 *      url = 'https://www.google.com/test'
	 *      includeProtocal = true
	 *  output
	 * 		'https://google.com'
	 * 
	 * Is correct?
	 * 
	 * */
    private static String getDomainName(String url, boolean includeProtocal) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();

            String result = domain.startsWith("www.") ? domain.substring(4) : domain;
            String protocal = url.substring(0, url.indexOf("//") + 2);

            String resultDomain = includeProtocal ? protocal + result : result;

            return resultDomain.toLowerCase();
        } catch (Exception e) {
            return "";
        }

    }

}