package com.jjpa.dma.manager.processor.publish;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

public class GetPublishQueueProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetPublishQueueProcessor.class);
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> GetPublishQueueProcessor");
		logger.debug("Process ===> GetPublishQueueProcessor : {}", input.toJsonString());
		
		Map<String,Object> output = callHookSyncGetPublishQueueList(input.getData()).json();

    return output;
    
	}

}
