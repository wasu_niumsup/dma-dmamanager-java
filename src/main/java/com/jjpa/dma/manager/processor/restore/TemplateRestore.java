package com.jjpa.dma.manager.processor.restore;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerStore;

public class TemplateRestore {

	private static final Logger logger = LoggerFactory.getLogger(TemplateRestore.class);
	
	protected TemplateAccessor templateAccessor;
	
	public TemplateRestore() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	public List<String> process(String filePath, String user) throws Exception {
		logger.info("Process ===> TemplateRestore");
		logger.info("filePath="+ filePath);
		
		List<String> templateList = new ArrayList<>();
		
		File file = new File(filePath);
		if(file.exists()){
			
			DMATemplate document;
			Map<String, Object> jsonMap;
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
					logger.info("restoring Template. TemplateName="+ fileEntry.getName());
					
					templateList.add(fileEntry.getName());
					
					jsonMap = ManagerStore.readJsonMapFromFile(fileEntry, true);
					
					/* 16/05/2561 new lastUpdate */
					jsonMap.put(MongoConstants.Path.LAST_UPDATE, new Date());
					
					document = new DMATemplate(jsonMap);
					
					
					/* 22/05/2562 add by Akanit : Jira DMA-1447, DMA-1445 : Old storyboard can't use when restore */
					mergeReferenceBetweenOldAndNew(document);
					
					
					templateAccessor.saveTemplate(document, user);
					logger.info("save Template success. TemplateName="+ document.getTemplateName());
					
					templateAccessor.updateCurrentVersionTemplateRequriedReferenceToScreen(document);
					logger.info("restore Template success. TemplateName="+ document.getTemplateName());
				}
			}
		}
		
		logger.info("End Process ===> TemplateRestore");
		return templateList;
	}
	
	/* 22/05/2562 add by Akanit : Jira DMA-1447, DMA-1445 : Old storyboard can't use when restore */
	private DMATemplate mergeReferenceBetweenOldAndNew(DMATemplate newTemplateDoc) throws Exception {
		logger.info("merge TemplateInUsedByScreenReference Between Database And ImportFile.");
		
		DMATemplate oldTemplateDoc = templateAccessor.findTemplateByName(newTemplateDoc.getTemplateName());
		
		/* 31/10/2562 add by Akanit : Jira DMA-1727 : Import Storyboard ใช้งานไม่ได้  */
		if(oldTemplateDoc == null){
			logger.info("template '"+ newTemplateDoc.getTemplateName() +"' not found in DB, then skip merge Ref.");
			return newTemplateDoc;
		}
		
		/* get Old ref */
		List<Map<String, Object>> inUsedByScreenList_oldTemplate = new GetReferenceProcessor().getInUsedByDmaScreens(oldTemplateDoc.getReference());
		
		/* get New ref */
		List<Map<String, Object>> inUsedByScreenList_newTemplate = new GetReferenceProcessor().getInUsedByDmaScreens(newTemplateDoc.getReference());
		newTemplateDoc.getReference().getInUsed().setInUsedByDmaScreen(inUsedByScreenList_newTemplate);
		
		
		/* when same template : then update ref to screen */
		/* 31/10/2562 comment by Akanit : Jira DMA-1727 : Import Storyboard ใช้งานไม่ได้ */
//		List<Map<String, Object>> duplicateScreenList = ManagerArrayList.getListDuplicateNodeByKey("name", inUsedByScreenList_oldTemplate, inUsedByScreenList_newTemplate);
//		templateAccessor.updateCurrentVersionTemplateRequriedReferenceToScreen(duplicateScreenList, newTemplateDoc);
		
		
		/* merge ref */
		List<Map<String, Object>> disapearScreenList = ManagerArrayList.getListDisapearNodeByKey("name", inUsedByScreenList_oldTemplate, inUsedByScreenList_newTemplate);
		
		for(Map<String, Object> disapearScreen : disapearScreenList){
			inUsedByScreenList_newTemplate.add(disapearScreen);
		}
		
		
		return newTemplateDoc;
	}
	
}
