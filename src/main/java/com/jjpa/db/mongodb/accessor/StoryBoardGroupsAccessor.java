package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import com.jjpa.db.mongodb.document.dma.manager.StoryBoardGroupsDocument;

public interface StoryBoardGroupsAccessor {

	public StoryBoardGroupsDocument save(StoryBoardGroupsDocument document, String user) throws Exception;
	public List<StoryBoardGroupsDocument> findAll() throws Exception;
	public List<Map<String, Object>> findByName(String name) throws Exception;
	public void deleteByName(String name, String user) throws Exception;
	public void saveByName(String name, List<Map<String, Object>> dataList, String user) throws Exception;
}
