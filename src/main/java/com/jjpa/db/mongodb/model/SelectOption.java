package com.jjpa.db.mongodb.model;

public class SelectOption {
	
	private String path;
	
	/** 
	 * inclusion is 0 or 1
	 * 1 is inclusion
	 * 0 is exclusion
	 * default is 1
	 * */
	private int inclusion = 1;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getInclusion() {
		return inclusion;
	}

	public void setInclusion(int inclusion) {
		this.inclusion = inclusion;
	}

}
