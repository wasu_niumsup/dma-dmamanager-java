package com.jjpa.dma.manager.processor.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.APIsConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.ServiceGatewayUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;

import blueprint.util.StringUtil;
import servicegateway.config.ServiceGatewayConfig;

public class BindingInputFieldProcessor{

	private final Logger logger = LoggerFactory.getLogger(BindingInputFieldProcessor.class);
	
	public Map<String, Object> process(Map<String, Object> dataBinding, AppUiDocument appUiDoc) throws Exception {

//		List<Map<String, Object>> dynamicDataBinding = (List<Map<String, Object>>)screenConfig.get(DmaConstants.Template.Key.DYNAMIC_DATA_BINDING);
		List<Map<String, Object>> dynamicDataBinding = appUiDoc.getDynamicDataBinding();
		
		/* old */
		Map<String, Object> rowDynamicField = findRowBySearchString(dynamicDataBinding, DmaConstants.Template.Key.TYPE, DmaConstants.Template.Type.DYNAMIC_FIELD);
		
		if(rowDynamicField!=null){
			logger.info("found type dynamicField. then binding dynamicField.");
						
			bindingDynamicInputField(dataBinding, appUiDoc, rowDynamicField);
		}
		
		/* new */
//		String name, path;
//		for(Map<String, Object> dynamicData : dynamicDataBinding){
//			
//			/* get parameter */
//			name = DataUtils.getStringValue(dynamicData.get(DmaConstants.Template.Key.NAME));
//			path = DataUtils.getStringValue(dynamicData.get(DmaConstants.Template.Key.BINDING));
//			
//			
//			if(DmaConstants.Template.Name.RESULT_INITIAL.equals(name)){
//				
//				bindingResultInitial(dataBinding, path, appUiDoc, configServiceGateway);
//				
//			}else if(DmaConstants.Template.Name.MAINFORM.equals(name)){
//				
//				bindingDynamicInputField(dataBinding, path, appUiDoc, configServiceGateway);
//			}			
//		}
				
		return dataBinding;
	}
	
//	private void bindingResultInitial(Map<String, Object> dataBinding, String path, AppUiDocument appUiDoc, Map<String, Object> configServiceGateway) throws Exception{
//		
//		DocumentContext ctx = JsonPath.parse(dataBinding);
//		
//		Map<String, Object> resultInitial = new LinkedHashMap<String, Object>();
//		
//		String apiName = appUiDoc.getDynamicApi();
//		if(apiName!=null){
//
//			if(configServiceGateway==null || !configServiceGateway.containsKey("uuid")){ //jsonData
//				resultInitial.put("resultSuccess", false);
//				resultInitial.put("resultMessage", "Initial Form Failed.");
//			}else{
//				resultInitial.put("resultSuccess", true);
//				resultInitial.put("resultMessage", "Initial Form Successed.");
//			}
//			
//		}else {
//			resultInitial.put("resultSuccess", false);
//			resultInitial.put("resultMessage", "Initial Form Failed.");	
//		}
//		
//		// binding ctx
//		ctx.set(path, resultInitial);
//	}
	
	private void bindingDynamicInputField(Map<String, Object> dataBinding, AppUiDocument appUiDoc, Map<String, Object> rowDynamicField) throws Exception{
		
		/* cast data to DocumentContext */
		DocumentContext masterTemplate = JsonPath.parse(dataBinding);
		
		/* get jasonPath */
		String path = DataUtils.getStringValue(rowDynamicField, DmaConstants.Template.Key.BINDING, null);
		
		/* binding dynamicInputList to jasonPath */
		List<Map<String,Object>> field =  masterTemplate.read(path);
		field.clear();
		field.addAll(appUiDoc.getDynamicInputList());

	}

//	private void bindingDynamicInputField(Map<String, Object> dataBinding, String path, AppUiDocument appUiDoc, Map<String, Object> configServiceGateway) throws Exception{
//		
//		/* cast data to DocumentContext */
//		DocumentContext masterTemplate = JsonPath.parse(dataBinding);
//		
//		List<Map<String, Object>> bindingTriger = getDynamicInput(configServiceGateway);
//		logger.info("bindingTriger="+bindingTriger);
//		
//		masterTemplate.set(path, bindingTriger);
//		
//	}
	
	
	@SuppressWarnings("unused")
	private List<Map<String, Object>> getDynamicInput(Map<String, Object> configServiceGateway) throws Exception {

//		Map<String, Object> responseData = DataUtils.getMapFromObject(response, DmaEngineConstant.RESPONSE_DATA);
//
		Map<String, Object> jsonData = DataUtils.getMapFromObject(configServiceGateway, "jsonData");
		
//		List<Map<String, Object>> serviceFieldsList = (List<Map<String, Object>>) jsonData.get("inputFieldList");
		ServiceGatewayConfig config = new ServiceGatewayConfig(jsonData);
		List<Map<String, Object>> serviceFieldsList = config.getInputFieldList_raw();
		
		String externalApisType = config.getExternalApisType();
		
		
		List<Map<String, Object>> listField = new ArrayList<Map<String, Object>>();

		Map<String, Object> indexFieldActionMap = new HashMap<String, Object>();

		if (serviceFieldsList != null) {
			// first loop for create fieldlist with index
			addIndexFieldListToMap(indexFieldActionMap, serviceFieldsList, externalApisType);

			// second loop for create add relate field
			addRelateFieldToMap(indexFieldActionMap, serviceFieldsList);

			// final loop for generate data and return to front
			generateBindingTrigger(serviceFieldsList, listField, indexFieldActionMap, externalApisType);
		}

		//return generateResponseTemplate(true, "Initial Form Success", listField);

		return listField;
		
	}
	
	private void addIndexFieldListToMap(Map<String, Object> indexFieldActionMap, List<Map<String, Object>> serviceFieldsList, String externalApisType) throws Exception {
		
		boolean promote;
		String key, fieldName;
		Map<String, Object> fieldActionMap;
		
		if(serviceFieldsList!=null){
			for (Map<String, Object> serviceData : serviceFieldsList) {
				promote = DataUtils.getBooleanValue(serviceData.get(APIsConstants.Key.PROMOTE));
	//			if (serviceData.get(APIsConstants.Key.PROMOTE) != null && DataUtils.getBooleanValue(serviceData.get(APIsConstants.Key.PROMOTE))) {
				if(promote){
					fieldActionMap = new HashMap<String, Object>();
	
					/* 21/02/2561 add by Akanit :  */
					if (APIsConstants.ExternalApiType.JSON_SERVICE_APIS.equalsIgnoreCase(externalApisType) ||
						APIsConstants.ExternalApiType.JSON_SERVICE_DROPDOWN.equalsIgnoreCase(externalApisType)) {
						
						key = DataUtils.getStringValue(serviceData.get(APIsConstants.Key.XPATH));
					}else {
						key = DataUtils.getStringValue(serviceData.get(APIsConstants.Key.INDEX));
					}
					
					fieldName = (String) serviceData.get(APIsConstants.Key.FIELD_NAME);
					fieldActionMap.put(APIsConstants.Key.FIELD_NAME, fieldName);
					indexFieldActionMap.put(key, fieldActionMap);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void addRelateFieldToMap(Map<String, Object> indexFieldActionMap, List<Map<String, Object>> serviceFieldsList) throws Exception {
		
		boolean promote;
		
		if(serviceFieldsList!=null){
			for (Map<String, Object> serviceData : serviceFieldsList) {
				promote = DataUtils.getBooleanValue(serviceData.get(APIsConstants.Key.PROMOTE));
	//			if (serviceData.get(APIsConstants.Key.PROMOTE) != null && DataUtils.getBooleanValue(serviceData.get(APIsConstants.Key.PROMOTE))) {
				if(promote){
					String fieldType = (String) serviceData.get(APIsConstants.Key.FIELD_TYPE);
					String fieldName = (String) serviceData.get(APIsConstants.Key.FIELD_NAME);
					if ("dynamicdropdown".equals(fieldType)) {
						if (APIsConstants.Value.YES.equalsIgnoreCase((String) serviceData.get(APIsConstants.Key.NEED_INPUT))) {
							List<Map<String, Object>> relateDataMap = (List<Map<String, Object>>) serviceData
									.get("inputFieldList");
							for (Map<String, Object> relateField : relateDataMap) {
								if (relateField.containsKey(APIsConstants.Key.VALUE_FROM)
										&& !StringUtil.isBlank(DataUtils.getStringValue(relateField.get(APIsConstants.Key.VALUE_FROM)))
										&& relateField.containsKey("onChangeLoad")
										&& DataUtils.getBooleanValue(relateField.get("onChangeLoad"))) {
									String valueFrom = DataUtils.getStringValue(relateField.get(APIsConstants.Key.VALUE_FROM));
									Map<String, Object> fieldActionMap = DataUtils.getMapFromObject(indexFieldActionMap, valueFrom);
									fieldActionMap.put("trigger", "load_" + fieldName);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void generateBindingTrigger(List<Map<String, Object>> serviceFieldsList, List<Map<String, Object>> listField, Map<String, Object> indexFieldActionMap, String externalApisType) throws Exception {
		
		String key;
		
		if(serviceFieldsList!=null){
			for (Map<String, Object> serviceData : serviceFieldsList) {
				logger.debug("Service Data {}", serviceData);
				
				/* 22/02/2561 add by Akanit : */
				key = ServiceGatewayUtils.getKeyField(externalApisType, serviceData);
				
				// generate dynamic field
				Map<String, Object> data = new HashMap<String, Object>();
				String fieldType = DataUtils.getStringValue(serviceData.get(APIsConstants.Key.FIELD_TYPE));
	
				if ("dynamicdropdown".equals(fieldType)) {
	
					String trigger = "donothing";
					Map<String, Object> fieldActionMap = DataUtils.getMapFromObject(indexFieldActionMap, key);
					String needInput = (String) serviceData.get(APIsConstants.Key.NEED_INPUT);
					if (fieldActionMap.containsKey("trigger")) {
						trigger = (String) fieldActionMap.get("trigger");
					}
	
					data.put("triggername", trigger);
					
				} else if ("staticdropdown".equals(fieldType)) {
					String trigger = "donothing";
					Map<String, Object> fieldActionMap = DataUtils.getMapFromObject(indexFieldActionMap, key);
					if (fieldActionMap.containsKey("trigger")) {
						trigger = (String) fieldActionMap.get("trigger");
					}
	
					data.put("triggername", trigger);
				}
	
				listField.add(data);
				
			}
		}
	}
	
	
	public static Map<String, Object> findRowBySearchString(List<Map<String, Object>> dataList, String searchKey, String searchValue){
		if(dataList==null)return null;
		if(searchKey==null)return null;
		if(searchValue==null)return null;
		
		String value;
		for(Map<String, Object> dataMap : dataList){
			
			if(dataMap.get(searchKey)==null){
				continue;
			}
			
			value = String.valueOf(dataMap.get(searchKey));
			if(value.equals(searchValue)){
				return dataMap;
			}
		}
		return null;
	}
}
