package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.AppConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardGroupsAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardScreenListAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardStoryBoardListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerMap;

/* 
 * 03/01/2562 add by Akanit : data is very large size : finished.
 * 08/05/2562 (080525621421) add by Akanit : DMA.5.3.0 : binding groups to storyboard : finished.
 * */
public class StoryBoardAccessorImpl implements StoryBoardAccessor{
	
	private static final Logger logger = LoggerFactory.getLogger(StoryBoardAccessorImpl.class);
	
	protected MongoDbAccessor mongoDbAccessor;
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardStoryBoardListAccessor storyBoardStoryBoardListAccessor;
	protected StoryBoardScreenListAccessor storyBoardScreenListAccessor;
	protected StoryBoardGroupsAccessor storyBoardGroupsAccessor;

	protected StoryBoardAccessorImpl(MongoDbAccessor mongoDbAccessor, AppUiAccessor appUiAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
		this.appUiAccessor = appUiAccessor;
		this.storyBoardStoryBoardListAccessor = SpringApplicationContext.getBean("StoryBoardStoryBoardListAccessor");
		this.storyBoardScreenListAccessor = SpringApplicationContext.getBean("StoryBoardScreenListAccessor");
		this.storyBoardGroupsAccessor = SpringApplicationContext.getBean("StoryBoardGroupsAccessor");
	}
	
	public List<StoryBoardDocument> findAllNotGetRelateData() throws Exception {
		return mongoDbAccessor.findAll(StoryBoardDocument.class);
	}
	
	public StoryBoardDocument findStoryByName(String storyName) throws Exception {
		Query findByName = new Query();
		findByName.addCriteria(Criteria.where(MongoConstants.StoryBoard.Path.STORY_NAME).regex(Pattern.compile(DataUtils.getMongoMatchRegex(storyName), Pattern.CASE_INSENSITIVE)));
		StoryBoardDocument document = mongoDbAccessor.findOneByCriteria(findByName, StoryBoardDocument.class);
		
		/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'storyBoard' and 'screenList'. */
		loadRelateData(document, storyName);
		
		return document;
	}
	

	public int count(Query query) throws Exception {
		return mongoDbAccessor.findCountByCriteria(query,StoryBoardDocument.class);
	}

	public Map<String,List<Map<String, Object>>> listStoryBoardWithApi(ServiceData input) throws Exception{
		Map<String,List<Map<String, Object>>> storyApiList = new HashMap<String,List<Map<String, Object>>>();
		List<Map<String, Object>> storyBoardWithApiList = new ArrayList<Map<String, Object>>();
		String appUiName = (String)input.getValue("appUiName");
		
		List<StoryBoardDocument> storyList = mongoDbAccessor.findAll(StoryBoardDocument.class);
		
		if(storyList!=null){
			for(StoryBoardDocument document : storyList){
				
				String storyName = document.getStoryName();
				
				/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'storyBoard' and 'screenList'. */
				loadRelateData(document, storyName);
				
		
				List<Map<String, Object>> storyBoard = document.getStoryBoard();
				String apiName = "";
				for(Map<String, Object> story : storyBoard){
					// get api from (source = {appUiName}) and (action = "initial_form")
					if(appUiName.equals(story.get("source")) && "initial_form".equals(story.get("action"))){
						apiName = (String)story.get("api");
						break;
					}
				}
				if(!StringUtil.isBlank(apiName)){
					Map<String, Object> storyApiMap = new HashMap<String, Object>();
					storyApiMap.put("storyName", storyName);
					storyApiMap.put("apiName", apiName);
					storyBoardWithApiList.add(storyApiMap);
				}
			}
		}
		storyApiList.put("storyApiList", storyBoardWithApiList);
		return storyApiList;
	}
	
	public Map<String,Object> listStory(ServiceData input) throws Exception {
		List<StoryBoardDocument> storyList = mongoDbAccessor.findAll(StoryBoardDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.StoryBoard.Key.STORY_LIST, getStoryList(storyList));
		return result;
	}

	@Override
	public Map<String, Object> listStoryByCriteria(Query query) throws Exception {
		List<StoryBoardDocument> storyList = mongoDbAccessor.findByCriteria(query, StoryBoardDocument.class);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(DmaConstants.StoryBoard.Key.STORY_LIST, getStoryList(storyList));
		return result;
	}

	
	@SuppressWarnings("unchecked")
	public void deleteStory(ServiceData input) throws Exception {
		
		String user = (String)input.getValue(AppConstants.USER);
		String storyName;
		
		
		List<Map<String, Object>> storyList = (List<Map<String, Object>>)input.getValue(DmaConstants.StoryBoard.Key.STORY_LIST);
		
		for(Map<String, Object> storyMap : storyList){
			
			storyName = ManagerMap.getString(storyMap, DmaConstants.StoryBoard.Key.STORY_NAME);
			
			/* 03/01/2562 add by Akanit : data is very large size : delete relate data. */
			deleteRelateData(storyName, user);
			
			mongoDbAccessor.delete(findStoryByName(storyName), user);
		}
	}
	
	public Object saveStoryBoard(StoryBoardDocument document, String user) throws Exception{
		if( StringUtil.isBlank(document.getDmaManagerVersion()) ){
			document.setDmaManagerVersion( ConfigurationUtils.getEngineVersion() );
		}

		if(document.getId()!=null){
			StoryBoardDocument storyDoc = mongoDbAccessor.findById(document.getId(), StoryBoardDocument.class);
			if(storyDoc!=null){
				storyDoc.setStoryName(document.getStoryName());
				storyDoc.setRemark(document.getRemark());
				storyDoc.setFirstPage(document.getFirstPage());
				storyDoc.setHomePage(document.getHomePage());
				storyDoc.setNotificationPage(document.getNotificationPage());
				storyDoc.setStatus(document.getStatus());
				storyDoc.setReference(document.getReference());
				storyDoc.setStoryBoard(document.getStoryBoard());
				storyDoc.setScreenList(document.getScreenList());
				storyDoc.setPinList(document.getPinList());
				storyDoc.setGroups(document.getGroups());
				storyDoc.setDmaManagerVersion( ConfigurationUtils.getEngineVersion() );
				storyDoc.setLastUpdate(document.getLastUpdate());
				storyDoc.setTabInfo(document.getTabInfo());
				storyDoc.setApiInfo(document.getApiInfo());
				document = storyDoc;
			}
			
			/* 03/01/2562 add by Akanit : data is very large size : move 'storyBoard' and 'screenList' to new database table */
			moveRelateData(document, user);
			
			return mongoDbAccessor.save(document, user);
			
		}else{
			
			/* 03/01/2562 add by Akanit : data is very large size : move 'storyBoard' and 'screenList' to new database table */
			moveRelateData(document, user);
			
			return mongoDbAccessor.save(document, user);
		}
		
	}
	
	public Object updateStoryBoardOneLevel(StoryBoardDocument document, String user) throws Exception{
		if(document==null){
			logger.info("storyDoc is null. then skip update storyBoard.");
			return null;
		}
		
		if(document.getId()!=null){
			
			/* 24/05/2562 add by Akanit :  */
			removeKeyRelateData(document);
			
			return mongoDbAccessor.save(document, user);
		}
		return null;
	}
	
	public Object updateGroupStoryBoard(StoryBoardDocument document, String user) throws Exception{
		if(document==null){
			logger.info("storyDoc is null. then skip update groups.");
			return null;
		}
		
		if(document.getId()!=null){
			
			/* 24/05/2562 add by Akanit :  */
			moveGroupRelateData(document, user);
			
			return mongoDbAccessor.save(document, user);
		}
		return null;
	}
	
	/* 08/05/2562 add by Akanit : (080525621421) */
	public void updateGroups(AppUiDocument appUiDoc, String user) throws Exception{
		updateGroups(findStoryByUiName(appUiDoc), appUiDoc, user);
	}
	
	/* 08/05/2562 add by Akanit : (080525621421) */
	public void updateGroups(StoryBoardDocument storyDoc, AppUiDocument appUiDoc, String user) throws Exception{
		if(storyDoc==null){
			logger.info("storyDoc is null. then skip update groups.");
			return;
		}
		if(appUiDoc==null){
			logger.info("appUiDoc is null. then skip update groups.");
			return;
		}
		
		for(Map<String, Object> group : storyDoc.getGroups()){
			if(updateImageUrlInGroupByUiName(group, appUiDoc.getUiName(), appUiDoc.getUrlScreenShot())){
				updateGroupStoryBoard(storyDoc, user);
				logger.info("update 'groups' in StoryBoard '"+ storyDoc.getStoryName() +"'.");
				break;
			}
		}
		
	}

	/* 08/05/2562 add by Akanit : (080525621421) */
	private boolean updateImageUrlInGroupByUiName(Map<String, Object> group, String uiName, String imageUrl) throws Exception {
		
		List<Map<String, Object>> screenList = ManagerMap.get(group, "screen", List.class);
		
		Map<String, Object> screenRow = ManagerArrayList.findRowBySearchString(screenList, DmaConstants.AppUI.Key.NAME, uiName);
		
		if(screenRow!=null){
			
			String old_value = ManagerMap.getString(screenRow, "imageUrl");
			String new_value = imageUrl;
			
			logger.info("edit StoryBoard.groups[?].screen[?].imageUrl from '"+ old_value +"' to '"+ new_value +"'");
			screenRow.put("imageUrl", new_value);
			
			return true;
		}else {
			return false;
		}
		
	}
	
	
	public void updateVersionScreenToStoryboard(AppUiDocument appUiDoc) throws Exception {
		updateVersionScreenToStoryboard(appUiDoc, findStoryByUiName(appUiDoc));
	}
	public void updateVersionScreenToStoryboard(AppUiDocument appUiDoc, StoryBoardDocument storyDoc) throws Exception {
		if(storyDoc==null){
			logger.info("storyDoc is null. then skip update versionScreen to storyBoard.");
			return;
		}
		
		/* find storyboard in storyboardList : find with condition -> 'source' = {$screenName} */
		Map<String, Object> storyBoardRow = ManagerArrayList.findRowBySearchString(storyDoc.getStoryBoard(), DmaConstants.StoryBoard.Key.SOURCE, appUiDoc.getUiName());
		if(storyBoardRow==null){
			logger.info("StoryBoard '"+ storyDoc.getStoryName() +"' not found config of Screen "+ appUiDoc.getUiName() +", then cancel this step.");
			return;
		}
		
		/* update currentVersion */
		bindingUpdateVersionScreenToStoryboard(storyBoardRow, appUiDoc);
		
		/* save */
//		updateStoryBoardOneLevel(storyDoc, "autoLoadVersionScreen");
		storyBoardStoryBoardListAccessor.updateOneByScreenName(appUiDoc.getUiName(), storyBoardRow, "autoLoadVersionScreen");
		
		logger.info("update 'currentVersion' in 'storyBoardList' in StoryBoard '"+ storyDoc.getStoryName() +"' at source '"+ appUiDoc.getUiName() +"'.");
	}
	
	public void upgradeVersionScreenToStoryboard(AppUiDocument appUiDoc) throws Exception {
		upgradeVersionScreenToStoryboard(appUiDoc, findStoryByUiName(appUiDoc));
	}
	public void upgradeVersionScreenToStoryboard(AppUiDocument appUiDoc, StoryBoardDocument storyDoc) throws Exception {
		if(storyDoc==null){
			logger.info("storyDoc is null. then skip update versionScreen to storyBoard.");
			return;
		}
		
		/* find storyboard in storyboardList : find with condition -> 'source' = {$screenName} */
		Map<String, Object> storyBoardRow = ManagerArrayList.findRowBySearchString(storyDoc.getStoryBoard(), DmaConstants.StoryBoard.Key.SOURCE, appUiDoc.getUiName());
		if(storyBoardRow==null){
			logger.info("StoryBoard '"+ storyDoc.getStoryName() +"' not found config of Screen "+ appUiDoc.getUiName() +", then cancel this step.");
			return;
		}
		
		bindingUpgradeVersionScreenToStoryboard(storyBoardRow, appUiDoc);
		
	}
	
	
	
	/* 08/05/2562 add by Akanit : move code out of method 'updateVersionScreenToStoryboard' */
	public StoryBoardDocument findStoryByUiName(AppUiDocument appUiDoc) throws Exception{
		
		String storyBoardName = new GetReferenceProcessor().getInUsedByDmaStroyBoard(appUiDoc.getReference());
		if(storyBoardName==null){
			logger.info("Screen '"+ appUiDoc.getUiName() +"' not found refference.inUsed.inUsedByDmaStroyBoard, return null.");
			return null;
		}
		
		
		/* accessor find storyboard from database */
		StoryBoardDocument storyDoc = findStoryByName(storyBoardName);
		if(storyDoc==null){
			logger.info("StoryBoard '"+ storyBoardName +"' not found, return null.");
			return null;
		}
		
		return storyDoc;
	}
	
	private List<Map<String, Object>> getStoryList(List<StoryBoardDocument> storyList) throws Exception{
		List<Map<String, Object>> storyBoardList = new ArrayList<Map<String, Object>>();
		for(StoryBoardDocument document : storyList){
			storyBoardList.add(getStory(document));
		}
		return storyBoardList;
	}
	
	private Map<String, Object> getStory(StoryBoardDocument document) throws Exception{
		
		if(document!=null){
			
			String storyName = document.getStoryName();
			logger.info("Process ===> StoryBoard Name = "+ storyName);
			
			Map<String,Object> story = new HashMap<String,Object>();
			story.put(DmaConstants.StoryBoard.Key.STORY_NAME, document.getStoryName());
			story.put(DmaConstants.StoryBoard.Key.REMARK, document.getRemark());
			story.put(DmaConstants.StoryBoard.Key.LAST_UPDATE, document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
			story.put(DmaConstants.StoryBoard.Key.LAST_PUBLISH, document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
			story.put(DmaConstants.StoryBoard.Key.STATUS, document.getStatus());
			story.put(DmaConstants.StoryBoard.Key.TABINFO, document.getTabInfo());
			story.put(DmaConstants.StoryBoard.Key.APIINFO, document.getApiInfo());
			
			
			/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'storyBoard' and 'screenList'. */
			loadRelateData(document, storyName);
			
			/* 01/03/2562 add by Akanit : binding needUpgrade .*/
			story.put(DmaConstants.AppUI.Key.NEED_UPGRADE, isNeedUpgradeScreen(document));
			
			return story;
			
		}else {
			return new LinkedHashMap<>();
		}
		
	}

	public boolean isNeedUpgradeScreen(String storyBoardName) throws Exception {
		return isNeedUpgradeScreen(findStoryByName(storyBoardName));
	}

	public boolean isNeedUpgradeScreen(StoryBoardDocument storyBoardDoc) throws Exception {
		
		if(storyBoardDoc!=null){
			
			List<Map<String, Object>> storyBoardList = storyBoardDoc.getStoryBoard();
		
			if(storyBoardList != null && storyBoardList.size() > 0){
				
				for(Map<String, Object> storyBoardMap : storyBoardList){
					
					if(isNeedUpgradeScreen(storyBoardMap)){
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private boolean isNeedUpgradeScreen(Map<String, Object> storyBoardMap) throws Exception {
		
		String screenName = ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE);

		AppUiDocument appUiDoc = appUiAccessor.findAppUiByName(screenName);
		if(appUiDoc==null){
			throw new Exception("screenName '"+ screenName + "' not found in db.");
		}
		 
		if(appUiAccessor.needUpgrade(appUiDoc)){
			return true;
		}else {
			return false;
		}
	}
	
	public void bindingNeedUpgradeStoryBoard(StoryBoardDocument storyBoardDoc) throws Exception {
		
		if(storyBoardDoc!=null){
			
			List<Map<String, Object>> storyBoardList = storyBoardDoc.getStoryBoard();
			
			if(storyBoardList != null && storyBoardList.size() > 0){
				
				boolean reSaveStoryboard = false;
				
				for (Map<String, Object> storyBoardMap : storyBoardList) {
//					String screenName = ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE);
//					logger.info("Process ===> Screen Name = "+ screenName);
					
					if(reSaveStoryboard==false){
						reSaveStoryboard = isOldVersionStoryboard(storyBoardMap);
					}
					
					if(isNeedUpgradeStoryBoard(storyBoardMap, storyBoardDoc.getStoryName())){
						bindingNeedUpgradeStoryBoard(storyBoardMap, storyBoardDoc.getScreenList(), true);
					}else{
						bindingNeedUpgradeStoryBoard(storyBoardMap, storyBoardDoc.getScreenList(), false);
					}
					
				}
				
				if(reSaveStoryboard){
					logger.info("re save storyboard '"+ storyBoardDoc.getStoryName() +"' because it is old version.");
					saveStoryBoard(storyBoardDoc, "autoLoadVersionScreen");
				}
			}
			
		}
		
	}
	private boolean isOldVersionStoryboard(Map<String, Object> storyBoardMap) throws Exception {
		
		if(ManagerMap.getIntger(storyBoardMap, DmaConstants.StoryBoard.Key.CURRENT_VERSION)==null){
			return true;
		}else{
			return false;
		}
	}
	public boolean isNeedUpgradeStoryBoard(Map<String, Object> storyBoardMap, String storyboardName) throws Exception {
		
		
		/* 
		 * get screen version in storyBoard 
		 * */
		int screenVersion = ManagerMap.getIntger(storyBoardMap, DmaConstants.StoryBoard.Key.VERSION, 0);
//		logger.info("Process ===> Current Version Screen  : "+ oldVersion);
		
		
		/* 
		 * get screen version in database
		 * */
		Integer currentScreenVersion = ManagerMap.getIntger(storyBoardMap, DmaConstants.StoryBoard.Key.CURRENT_VERSION);
		
		if(currentScreenVersion==null){
			
			currentScreenVersion = getVersionScreenFromDB(storyBoardMap);
			
			/* update newVersion of templateRequired */
			storyBoardMap.put(DmaConstants.StoryBoard.Key.CURRENT_VERSION, currentScreenVersion);
			logger.info("add field 'currentVersion' in to field 'storyboard' in storyboardlist '"+ storyboardName + "' in source '" + ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE) +"'.");
		}
//		logger.info("Process ===> New Version Screen  : "+ newVersion);
		
		
		
		/* 
		 * check version : needUpgrade?
		 * */
		if(screenVersion < currentScreenVersion) {
			return true;
		}else {
			return false;
		}
		
	}
	private int getVersionScreenFromDB(Map<String, Object> storyBoardMap) throws Exception {
		
		String screenName = ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE);
		
		AppUiDocument appUi = appUiAccessor.findAppUiByName(screenName);
		if(appUi==null){
			throw new Exception("screenName '"+ screenName + "' not found in db.");
		}
		
		int version = 0;
		try{
			version = Integer.parseInt(appUi.getReference().getVersion());
		} catch (Exception e){}
		
		return version;
	}
	private void bindingNeedUpgradeStoryBoard(Map<String, Object> storyBoardMap, List<Map<String, Object>> screenList, boolean needUpgrade) throws Exception {
		
		String screenName = ManagerMap.getString(storyBoardMap, DmaConstants.StoryBoard.Key.SOURCE);
		
//		logger.info("Process ===> Add needUpgradeStoryBoard = " + needUpgrade + " in storyBoard");
		storyBoardMap.put(DmaConstants.StoryBoard.Key.DMA_STORYBOARD_NEED_UPGRADE, needUpgrade);
		
		//Add needUpgradeStoryBoard to ScreenList 
		if( screenList!=null && screenList.size()>0 ){
			for (Map<String, Object> screenMap : screenList) {
				
				String screenNameInScreenList = (String) screenMap.get(DmaConstants.StoryBoard.Key.SOURCE);
				//Check  match screen
				if(screenNameInScreenList.equals(screenName)) {
//					logger.info("Process ===> Add needUpgradeStoryBoard = " + needUpgrade + " in screenList");
					screenMap.put(DmaConstants.StoryBoard.Key.DMA_STORYBOARD_NEED_UPGRADE, needUpgrade);
					break;
				}
			}
		}
		
	}
	
	
	
	
	public Map<String, Object> bindingNeedUpgradeScreen(StoryBoardDocument storyBoardDoc) throws Exception {
        if(storyBoardDoc==null){
        	return new LinkedHashMap<>();
        }
	    
        Map<String, Object> output = new LinkedHashMap<>(storyBoardDoc.toMap());
        output.put(DmaConstants.StoryBoard.Key.DMA_SCREEN_NEED_UPGRAD, isNeedUpgradeScreen(storyBoardDoc));
        
        return output;
	}
	
	public void bindingUpdateVersionScreenToStoryboard(Map<String, Object> storyBoardRow, AppUiDocument appUiDoc) throws Exception {
		try{
			storyBoardRow.put(DmaConstants.StoryBoard.Key.CURRENT_VERSION, appUiDoc.getReference().getVersion());
		} catch (Exception e){
			logger.info("can not get reference.version of screen '"+ appUiDoc.getUiName() +"' then set currentVersion = null.");
		}
	}

	public void bindingUpgradeVersionScreenToStoryboard(Map<String, Object> storyBoardRow, AppUiDocument appUiDoc) throws Exception {
		try{
			storyBoardRow.put(DmaConstants.StoryBoard.Key.VERSION, appUiDoc.getReference().getVersion());
			storyBoardRow.put(DmaConstants.StoryBoard.Key.CURRENT_VERSION, appUiDoc.getReference().getVersion());
		} catch (Exception e){
			logger.info("can not get reference.version of screen '"+ appUiDoc.getUiName() +"' then set currentVersion = null.");
		}
	}
	
	
	/* 03/01/2562 add by Akanit : data is very large size : get relate data from child table and put to 'storyBoard' and 'screenList'. */
	public void loadRelateData(StoryBoardDocument document, String storyName) throws Exception {
		if(document!=null){
			document.setStoryBoard(storyBoardStoryBoardListAccessor.findByName(storyName));
			document.setScreenList(storyBoardScreenListAccessor.findByName(storyName));
			document.setGroups(storyBoardGroupsAccessor.findByName(storyName));
		}
	}
	
	
	/* 03/01/2562 add by Akanit : data is very large size : move 'storyBoard' and 'screenList' to new database table */
	public void moveRelateData(StoryBoardDocument document, String user) throws Exception {
		if(document!=null){
			storyBoardStoryBoardListAccessor.saveByName(document.getStoryName(), document.getStoryBoard(), user);
			storyBoardScreenListAccessor.saveByName(document.getStoryName(), document.getScreenList(), user);
			storyBoardGroupsAccessor.saveByName(document.getStoryName(), document.getGroups(), user);
			removeKeyRelateData(document);
		}
	}

	/* 03/01/2562 add by Akanit : data is very large size : delete relate data. */
	public void deleteRelateData(String storyName, String user) throws Exception {
		storyBoardStoryBoardListAccessor.deleteByName(storyName, user);
		storyBoardScreenListAccessor.deleteByName(storyName, user);
		storyBoardGroupsAccessor.deleteByName(storyName, user);
	}
	
	/* 24/05/2562 add by Akanit :  */
	public void moveGroupRelateData(StoryBoardDocument document, String user) throws Exception {
		if(document!=null){
			storyBoardGroupsAccessor.saveByName(document.getStoryName(), document.getGroups(), user);
			removeKeyRelateData(document);
		}
	}
	
	/* 24/05/2562 add by Akanit :  */
	private void removeKeyRelateData(StoryBoardDocument document) throws Exception {
		document.setStoryBoard(new ArrayList<>());
		document.setScreenList(new ArrayList<>());
		document.setGroups(new ArrayList<>());
	}
	
}
