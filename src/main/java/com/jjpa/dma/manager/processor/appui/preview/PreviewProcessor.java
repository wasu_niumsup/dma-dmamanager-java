package com.jjpa.dma.manager.processor.appui.preview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.binding.BindingComponentProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import screenbuilder.beebuddy.net.webengine.Core;

/**
 * PreviewProcessor
 */
@SuppressWarnings("unchecked")
public class PreviewProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private static final Logger logger = LoggerFactory.getLogger(PreviewProcessor.class);
    protected TemplateAccessor templateAccessor;

    public PreviewProcessor() {
        this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
    }

    public static void main(String[] args) {
        String test = "$['$jason']['head']['templates']['body']['sections'][1]['items']['{{#each $get.result_list}}']['components'][3][0]['{{#if $get.checkedssss == 'true'}}']";
        List<String> stack = new ArrayList<>();
        int endScopeCount = 0;
        int startScopeCount = 0;

        String temp = "";
        for (int i = test.length() - 1; i >= 0; i--) {

            String c = String.valueOf(test.charAt(i));
            if (c.equalsIgnoreCase("]")) {
                endScopeCount++;
            } else if (c.equalsIgnoreCase("[")) {
                startScopeCount++;
            }

            temp += c;

            if (endScopeCount > 0 && endScopeCount == startScopeCount) {
                stack.add(new StringBuilder(temp).reverse().toString());
                temp = "";
            }

        }

        for (String item : stack) {
            System.out.println(item);
        }

        System.out.println(stack.get(stack.size() - 1 - 1));
    }

	@Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        try {
            logger.info("0. PreviewProcessor");
            logger.info("0. body = " + input.getData());
    
            AppUiDocument appUiDoc = new AppUiDocument();
            appUiDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
            appUiDoc.setUiName((String) input.getValue("uiName"));
            appUiDoc.setRemark((String) input.getValue("remark"));
            appUiDoc.setUiConfigData((Map<String, Object>) input.getValue("uiConfigData"));
            appUiDoc.setDynamicApi((String) input.getValue("dynamicApi"));
            appUiDoc.setTemplateType((String) input.getValue("templateType"));
            appUiDoc.setDynamicInputList((List<Map<String, Object>>) input.getValue("dynamicInputList"));
            appUiDoc.setDynamicOutputList((List<Map<String, Object>>) input.getValue("dynamicOutputList"));
    
            logger.info("#step 1 : get default jasonTemplate");
            Map<String, Object> dataBinding = getJsonTemplate(appUiDoc);
    
            logger.info("#step 2 : binding component to jasonTemplate");
    
            dataBinding = new BindingComponentProcessor().process(dataBinding, appUiDoc);
            String uiJson = JsonPath.parse(dataBinding).jsonString();
            logger.info("uiJson (after) = "+ uiJson);
    
            Map<String, Object> param = new LinkedHashMap<>();
            param.put(Core.KeyParams.DATA_MAP, JsonPath.parse(dataBinding).json());
            param.put(Core.KeyParams.CONTROLLER_URL, "https://100.100.30.81/dmaManager/resources/previewscreenJS/controller.js");
    
            Core core = new Core();
            String result = core.run(param);
    
            logger.info("#step 3 : outputJson : " + uiJson);
    
            Date createdDate = new Date();
            String defaultFormatString = "yyyyMMdd";
            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(defaultFormatString,
                    java.util.Locale.ENGLISH);
            String createDateStr = format.format(createdDate.getTime());
            String uuid = UUID.randomUUID().toString().replace("-", "");
    
            String fileName = createDateStr + uuid + ".html";
    
            logger.info("#step 4 : file name : " + fileName);
            String tempFolderPath = System.getProperty("jboss.server.temp.dir");
    
            logger.info("#step 5 : tempFolderPath : " + tempFolderPath);
    
            String fullPath = tempFolderPath +"/" + fileName;
            writeFile(fullPath, result);
    
            logger.info("Exist file : " +  new File(fullPath).exists() );
    
            logger.info("#step 6 : full path : " + fullPath);
    
            Map<String, Object> output = new LinkedHashMap<>();
    
            output.put("fileName", fullPath.replace("/", "AAA"));
    
            logger.info("#step 7 : output={}", output);
    
            return output; 
        } catch (Exception e) {
            logger.error("#Error {}", e);
            throw new Exception("This template is not available to preview.");
        }
        
    }

    public static void writeFile(String path, String content) {
        FileOutputStream fop = null;
        File file;
        //content = "This is the text content";

        try {

            file = new File(path);
            fop = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes("UTF-8");

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* 04/01/2561 add by Akanit */
    private Map<String, Object> getJsonTemplate(AppUiDocument appUiDoc) throws Exception {

        /* get template config */
        String templateName = (String) appUiDoc.getUiConfigData().get("template");
        DMATemplate template = templateAccessor.findTemplateByName(templateName);

        /* set dynamicDataBinding from template */
        appUiDoc.setDynamicDataBinding(template.getDynamicDataBinding());

        /* get templateJson */
        DocumentContext ctx = JsonPath.parse(template.getNativeJson().get("data"));

        /* create dataBinding from templateJson (old system : dataBinding is templateJson) */
        Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());

        return dataBinding;
    }

}