package com.jjpa.dma.manager.model.reference;

import java.util.List;
import java.util.Map;

public class RequiredReference {
	
	private List<Map<String, Object>> dmaTemplateRequired;
	private List<Map<String, Object>> dmaScreenRequired;
	private List<Map<String, Object>> dmaStroyBoardRequired;
	private List<Map<String, Object>> dmaActionChainRequired;
	private List<Map<String, Object>> dmaActionChainTemplateRequired;
	private List<Map<String,Object>>  dmaImageRequired;
	
	private List<Map<String, Object>> sgServiceRequired;
	private List<Map<String, Object>> sgOauthRequired;
	private List<Map<String, Object>> sgWsdlRequired;
	
	
	public List<Map<String, Object>> getDmaTemplateRequired() {
		return dmaTemplateRequired;
	}
	public void setDmaTemplateRequired(List<Map<String, Object>> dmaTemplateRequired) {
		this.dmaTemplateRequired = dmaTemplateRequired;
	}
	public List<Map<String, Object>> getDmaScreenRequired() {
		return dmaScreenRequired;
	}
	public void setDmaScreenRequired(List<Map<String, Object>> dmaScreenRequired) {
		this.dmaScreenRequired = dmaScreenRequired;
	}
	public List<Map<String, Object>> getDmaStroyBoardRequired() {
		return dmaStroyBoardRequired;
	}
	public void setDmaStroyBoardRequired(List<Map<String, Object>> dmaStroyBoardRequired) {
		this.dmaStroyBoardRequired = dmaStroyBoardRequired;
	}
	public List<Map<String, Object>> getDmaActionChainRequired() {
		return dmaActionChainRequired;
	}
	public void setDmaActionChainRequired(List<Map<String, Object>> dmaActionChainRequired) {
		this.dmaActionChainRequired = dmaActionChainRequired;
	}
	public List<Map<String, Object>> getDmaActionChainTemplateRequired() {
		return dmaActionChainTemplateRequired;
	}
	public void setDmaActionChainTemplateRequired(List<Map<String, Object>> dmaActionChainTemplateRequired) {
		this.dmaActionChainTemplateRequired = dmaActionChainTemplateRequired;
	}
	public List<Map<String, Object>> getDmaImageRequired() {
		return dmaImageRequired;
	}
	public void setDmaImageRequired(List<Map<String, Object>> dmaImageRequired) {
		this.dmaImageRequired = dmaImageRequired;
	}
	
	
	public List<Map<String, Object>> getSgServiceRequired() {
		return sgServiceRequired;
	}
	public void setSgServiceRequired(List<Map<String, Object>> sgServiceRequired) {
		this.sgServiceRequired = sgServiceRequired;
	}
	public List<Map<String, Object>> getSgOauthRequired() {
		return sgOauthRequired;
	}
	public void setSgOauthRequired(List<Map<String, Object>> sgOauthRequired) {
		this.sgOauthRequired = sgOauthRequired;
	}
	public List<Map<String, Object>> getSgWsdlRequired() {
		return sgWsdlRequired;
	}
	public void setSgWsdlRequired(List<Map<String, Object>> sgWsdlRequired) {
		this.sgWsdlRequired = sgWsdlRequired;
	}
	
}
