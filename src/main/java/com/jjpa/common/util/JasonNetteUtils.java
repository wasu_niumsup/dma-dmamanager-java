package com.jjpa.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.JasonPathConstants;

import blueprint.util.StringUtil;

public class JasonNetteUtils {

	public static List<Map<String, Object>> generateIfElseBinding(String condition, String trueValue, String falseValue)
			throws Exception {

		List<Map<String, Object>> result = new ArrayList<>();

		/* 
		 * create jason if 
		 * */
		Map<String, Object> conditionIf = new HashMap<>();
		conditionIf.put("{{#if " + condition + "}}", trueValue);

		result.add(conditionIf);

		/* 
		 * create jason else 
		 * */
		Map<String, Object> conditionElse = new HashMap<>();
		conditionElse.put("{{#else }}", falseValue);

		result.add(conditionElse);

		return result;
	}

	public static List<Map<String, Object>> generateTopicBinding(String name) throws Exception {

		/* Example
		 *  "text": [
		        {
		            "{{#if $get.showTopicLabel == 'true'}}": "{{$get.topicLabel || ''}}: {{ _dma_title || ''}}"
		        },
		        {
		            "{{#else }}": "{{ _dma_title }}"
		        }
		    ]
		 *  */
		String condition = "$get.showTopicLabel == 'true'";
		String trueValue = "{{$get.topicLabel || ''}}: {{ " + name + " || ''}}";
		String falseValue = "{{ " + name + " || ''}}";

		return generateIfElseBinding(condition, trueValue, falseValue);
	}

	public static List<Map<String, Object>> generateDescriptionBinding(String name) throws Exception {

		/* Example
		 *  "text": [
		        {
		            "{{#if $get.showDescLabel == 'true'}}": "{{$get.descLabel || ''}}: {{ _dma_desc || ''}}"
		        },
		        {
		            "{{#else }}": "{{ _dma_desc }}"
		        }
		    ]
		 *  */
		String condition = "$get.showDescLabel == 'true'";
		String trueValue = "{{$get.descLabel || ''}}: {{ " + name + " || ''}}";
		String falseValue = "{{ " + name + " || ''}}";

		return generateIfElseBinding(condition, trueValue, falseValue);
	}

	public static String generateResultList(String path) throws Exception {

		String value = path.replace("[0]", "").replace("[?]", ""); // Example data : path=Value.detail
		String result = "{{" + JasonPathConstants.UiJson.Key.RESULT_JASON + "." + value + "}}"; // Example data : {{$jason.resultData.Value.detail}}
		return result;
	}

	public static String generateGlobalValue(String value) {

		/*  
		 * example : 
		 * {{$global.hasOwnProperty('_app_global_value') ?  $global._app_global_value : '' }}
		 * */
		String globalValue = JasonPathConstants.UiJson.Key.GLOBAL + "." + DmaManagerConstant.PREFIX_APP_GLOBAL + value;
		String property = DmaManagerConstant.PREFIX_APP_GLOBAL + value;

		String result = "{{$global.hasOwnProperty('" + property + "') ? " + globalValue + " : '' }}";

		return result;
	}

	public static String generateNativeValue(String value) {

		String result = "{{" + JasonPathConstants.UiJson.Key.GET + "." + value + " || ''}}";
		return result;
	}

	public static String generateSystemGlobalValue(String value) {
		
		String result = "{{"+ JasonPathConstants.UiJson.Key.GLOBAL +"."+ value +" || ''}}";		
		return result;
	}
	
	public static String generateSelectedItemNativeValue(String value) {

		String result = "{{" + JasonPathConstants.UiJson.Key.SELECTED_DETAILS_ITEM + "." + value + "}}";
		return result;
	}

	public static String generateResultData(String value) {

		String result = "{{" + JasonPathConstants.UiJson.Key.RESULT_RESULT_DATA + "." + value + "}}";
		return result;
	}

	public static String setGlobal(String globalName) {
		String addedprefix = DmaManagerConstant.PREFIX_APP_GLOBAL + globalName;
		return addedprefix;
	}

	public static String cleanScope(String value) {
		String result = value == null ? "" : value;
		return result.replace("{{", "").replace("}}", "");
	}


	public static String addScropString(String value) {
		String result = value == null ? "" : value;
		return  " '" + result +  "' " ;
	}

	/**
	* Tanawat W.
	* [2018-02-26] Edit condition Native
	* fieldType : String is normal input
	* fieldType : Other are Location , Call etc.
	* 
	* Take action warpper only when  valueFrom == Native  and fieldType != 'string'
	* 							then TackAction 
	* 							else DoNothing end
	* @param globalInput {DmaConstants.StoryBoard.Key.GlobalInputList[?]}
	*/
	public static String generateResultValue(Map<String, Object> globalInput) throws Exception {

		String valueFrom = DataUtils.getStringValue(
				globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE_FROM),
				DmaConstants.StoryBoard.Value.GLOBAL);
		String fieldType = DataUtils.getStringValue(globalInput, "fieldType", "string");

		String resultValue = "";
		if (DmaConstants.StoryBoard.Value.NATIVE.equals(valueFrom) && !"string".equalsIgnoreCase(fieldType)) {
			//logger.info("binding api input from native.");

			/* get parameter */
			Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalInput,
					DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);
			String dmaParameter = DataUtils.getStringValue(valueProperties, DmaConstants.StoryBoard.Key.DMA_PARAMETER);

			/* "{{"$get."+ value +" || ''}}"; */
			if (!StringUtil.isBlank(dmaParameter)) {
				resultValue = JasonNetteUtils.generateNativeValue(dmaParameter);
			}

		} else if (DmaConstants.StoryBoard.Value.NATIVE.equals(valueFrom) && "string".equalsIgnoreCase(fieldType)) {

			//logger.info("binding api input from [Screen] native.");

			Map<String, Object> valueProperties = DataUtils.getMapFromObject(globalInput,
					DmaConstants.StoryBoard.Key.VALUE_PROPERTIES);

			/* get parameter */
			String value = DataUtils.getStringValue(valueProperties, "pathValueName");

			if (JasonPathConstants.UiJson.Key.SELECTED_DETAILS_ITEM.equalsIgnoreCase(
					DataUtils.getStringValue(valueProperties, "getValueFrom", JasonPathConstants.UiJson.Key.GET))) {
				/* "{{"$get.selected_details_item."+ value +" || ''}}"; */
				resultValue = JasonNetteUtils.generateSelectedItemNativeValue(value);
			} else {
				/* "{{"$get."+ value +" || ''}}"; */
				resultValue = JasonNetteUtils.generateNativeValue(value);
			}

		} 
		else if(DmaConstants.StoryBoard.Value.MANUAL.equals(valueFrom)){
			String value = DataUtils.getStringValue(globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE));
			resultValue =  cleanScope(value);
		}
		else {
			//logger.info("binding api input from global.");

			/* get parameter */
			String value = DataUtils.getStringValue(globalInput.get(DmaConstants.StoryBoard.Key.GlobalInputList.VALUE));

			/* {{$global.hasOwnProperty('_app_global_value') ?  $global._app_global_value : '' }} */
			resultValue = JasonNetteUtils.generateGlobalValue(value);
		}
		return resultValue;
	}

}
