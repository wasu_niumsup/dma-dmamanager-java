package com.jjpa.dma.manager.processor.restore.reference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.dma.manager.model.reference.RequiredReference;
import com.jjpa.dma.manager.processor.backup.GenerateImageFolderBackup;

/**
 * GenerateImageReferenceRestore
 */
public class GenerateImageReferenceRestore {
    private static final Logger logger = LoggerFactory.getLogger(GenerateImageReferenceRestore.class);
    
	public void process(RequiredReference reqRef, String filePath) throws Exception {
		logger.info("Process ===> GenerateImageReferenceRestore");
		
		List<Map<String, Object>> resultList = new ArrayList<>();		
		
		File file = new File(filePath);
		if(file.exists()){
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
							
//					try{
//						version = JsonPath.read(fileEntry, "$." + "reference.version");
//					}catch(Exception e){
//						version = null;
//					}
//					
//					try{
//						name = JsonPath.read(fileEntry, "$." + "storyName");
//					}catch(Exception e){
//						name = null;
//					}
					
					
	                List<Map<String, Object>> images = JsonPath.read(fileEntry, "$.urlList");
	                resultList = new ArrayList<>(images);	
	                break;
				}
	        }
	        
	
	        String targetDomain = ConfigurationUtils.getPublishUrl().toLowerCase();
	        for (Map<String,Object> item : resultList) {
	        	
	        	@SuppressWarnings("unused")
	            boolean isInternal = DataUtils.getBooleanValue(item, GenerateImageFolderBackup.Key.IS_INTERNAL, false);
	            String targetRawUtl = "";
	
	            targetRawUtl = targetDomain + DataUtils.getStringValue(item, GenerateImageFolderBackup.Key.WITHOUT_URL);
	            item.put(GenerateImageFolderBackup.Key.TARGET_RAWURL,   targetRawUtl  );
	            item.put(GenerateImageFolderBackup.Key.SOURCE_RAWURL,  DataUtils.getStringValue(item, GenerateImageFolderBackup.Key.RAW_URL));
	            item.put(GenerateImageFolderBackup.Key.TARGET_DOMAIN,targetDomain );
	        }
	    }

		reqRef.setDmaImageRequired(resultList);

	}
}