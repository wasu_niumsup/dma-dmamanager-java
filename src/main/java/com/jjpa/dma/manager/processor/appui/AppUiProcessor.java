//package com.jjpa.dma.manager.processor.appui;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.jayway.jsonpath.DocumentContext;
//import com.jayway.jsonpath.JsonPath;
//import com.jjpa.common.DmaManagerConstant;
//import com.jjpa.common.data.service.ServiceData;
//import com.jjpa.common.util.DataUtils;
//import com.jjpa.common.util.DateTimeUtils;
//import com.jjpa.db.mongodb.accessor.AppUiAccessor;
//import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
//import com.jjpa.processor.CommonProcessor;
//import com.jjpa.processor.ServiceProcessor;
//import com.jjpa.spring.SpringApplicationContext;
//
//import blueprint.util.StringUtil;
//
//@SuppressWarnings("unchecked")
//public class AppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
//
//	private static final Logger logger = LoggerFactory.getLogger(AppUiProcessor.class);
//	
//	protected AppUiAccessor appUiAccessor;
//	
//	public AppUiProcessor() {
//		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
//	}
//	
//	@Override
//	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
//		
//		logger.debug("Do AppUiProcessor");
//		
//		String functionName = (String)input.getValue("functionName");
//		logger.debug("Do function : {}", functionName);
//		if("/ui/listAppUi".equals(functionName)){
//			return appUiAccessor.listAppUi(input);
//		}else if("/ui/deleteAppUi".equals(functionName)){
//			appUiAccessor.deleteAppUi(input);
//			return appUiAccessor.listAppUi(input);
//		}else if("/ui/findAppUiByName".equals(functionName)){
//			String uiName = (String)input.getValue("uiName");
//			return appUiAccessor.findAppUiByName(uiName);
//		}else if("/ui/saveAppUi".equals(functionName)){
//			return saveAppUi(input);
//		}else if("/ui/setReady".equals(functionName)){
//			return setReady(input);
//		}else if("/ui/listAppUiWithAction".equals(functionName)){
//			return appUiAccessor.listAppUiWithAction(input);
//		}else if("/ui/findServiceInputByName".equals(functionName)){
//			return findServiceInputByName(input);
//		}
//		return input.getData();
//	}
//	
//	private Map<String, Object> findServiceInputByName(ServiceData input) throws Exception {
//		String apiName = (String)input.getValue("apiName");
//		Boolean haveChild = DataUtils.getBooleanValue(input.getValue("haveChild"));
//		logger.info("apiName="+ apiName);
//		logger.info("haveChild="+ haveChild);
//		
//		Map<String, Object> inputMap = new HashMap<String, Object>();
//		List<Map<String, Object>> inputList = new ArrayList<Map<String, Object>>();
//		inputMap.put("inputList", inputList);
//		if(StringUtil.isBlank(apiName)){
//			return inputMap;
//		}else{
//			return sgFindServiceInputByName(apiName, haveChild);
//		}
//	}
//	
//	private Map<String, Object> sgFindServiceInputByName(String service, Boolean haveChild) throws Exception {
//		logger.info("call serviceGateway ...");
//		
//		String findServiceMsg = "{" + "\"contentType\":301," + "\"source\":2," + "\"message\":\"List Service input by service name : "
//				+ service + " \"," + "\"_sg_name\" : \"" + service + "\"" + "," + "\"_sg_haveChild\" : \"" + haveChild + "\"" + "}";
//		
//		DocumentContext ctx = JsonPath.parse(findServiceMsg);
//
//		/**
//		* Tanawat W.
//		* [2018-05-08] Change transaction message.
//		*/
//		ctx.put("$",DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.GET_CONFIG);
//
//		DocumentContext responseCtx = getJsonAccessor().send("/rest/ws/service/findServiceInputByName", ctx);
//		
//		Map<String, Object> response = responseCtx.json();
//		Map<String, Object> output = (Map<String, Object>)response.get(DmaManagerConstant.RESPONSE_DATA);
//		return output;
//	}
//	
//	private Map<String,Object> setReady(ServiceData input) throws Exception {
//		AppUiDocument document = appUiAccessor.findAppUiByName((String)input.getValue("uiName"));
//		document.setStatus(DmaManagerConstant.STATUS_READY);
//		String user = (String)input.getValue("user");
//		
//		appUiAccessor.saveUi(document, user);
//		
//		Map<String,Object> appUi = new HashMap<String,Object>();
//		appUi.put("uiName", document.getUiName());
//		appUi.put("remark", document.getRemark());
//		appUi.put("lastUpdate", document.getLastUpdate()==null?"":DateTimeUtils.getFormattedDate(document.getLastUpdate()));
//		appUi.put("lastPublish", document.getLastPublish()==null?"":DateTimeUtils.getFormattedDate(document.getLastPublish()));
//		appUi.put("status", document.getStatus());
//		
//		return appUi;
//	}
//	
//	private Object saveAppUi(ServiceData input) throws Exception {
//		AppUiDocument oldDoc = appUiAccessor.findAppUiByName((String)input.getValue("uiName"));
//		AppUiDocument document = new AppUiDocument();
//		if((input.getValue("id")!=null && !"".equals(((String)input.getValue("id")).trim()))){
//			String id = (String)input.getValue("id");
//			if(oldDoc!=null && !id.equals(oldDoc.getId())){
//				throw new Exception("Name duplicated");
//			}
//			document.setId(id);
//		}else if(oldDoc != null){
//			throw new Exception("Name duplicated");
//		}
//		document.setStatus(DmaManagerConstant.STATUS_DRAFT);
//		document.setUiName((String)input.getValue("uiName"));
//		document.setRemark((String)input.getValue("remark"));
//		document.setUiConfigData((Map<String, Object>)input.getValue("uiConfigData"));
//		document.setLastUpdate(new Date());
//		String user = (String)input.getValue("user");
//		return appUiAccessor.saveUi(document, user);
//	}
//	
//}
