package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardGroupsAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardGroupsDocument;

public class StoryBoardGroupsAccessorImpl implements StoryBoardGroupsAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	
	protected StoryBoardGroupsAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	@Override
	public StoryBoardGroupsDocument save(StoryBoardGroupsDocument document, String user) throws Exception {
		return mongoDbAccessor.save(document, user);
	}
	
	private List<StoryBoardGroupsDocument> findMongoByName(String name) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where(MongoConstants.Simple.NAME).is(name));
		return mongoDbAccessor.findByCriteria(findByName, StoryBoardGroupsDocument.class);
	}
	
	@Override
	public List<StoryBoardGroupsDocument> findAll() throws Exception {
		return mongoDbAccessor.findAll(StoryBoardGroupsDocument.class);
	}
	
	@Override
	public List<Map<String, Object>> findByName(String name) throws Exception {
		
		/* find data from mongo */
		List<StoryBoardGroupsDocument> resultList = findMongoByName(name);

		
		/* 
		 * convert mongoData to listData 
		 * */
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardGroupsDocument resultMap : resultList){
				list.add(resultMap.getData());
			}
		}
		
		
		return list;
	}


	@Override
	public void deleteByName(String name, String user) throws Exception {

		/* find data from mongo */
		List<StoryBoardGroupsDocument> resultList = findMongoByName(name);
		
		
		/* 
		 * loop delete
		 * */
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardGroupsDocument resultMap : resultList){
				mongoDbAccessor.delete(resultMap, user);
			}
		}
		
	}


	@Override
	public void saveByName(String name, List<Map<String, Object>> dataList, String user) throws Exception {
		
		/* delete */
		deleteByName(name, user);
		
		
		/* 
		 * loop save
		 * */
		StoryBoardGroupsDocument document;
		
		if(dataList != null && dataList.size() != 0){
			
			for(Map<String, Object> dataMap : dataList){
				
				document = new StoryBoardGroupsDocument(); 
				document.setName(name);
				document.setData(dataMap);
				
				mongoDbAccessor.save(document, user);
			}
		}
		
	}

}
