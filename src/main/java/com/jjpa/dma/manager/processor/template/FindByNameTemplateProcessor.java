package com.jjpa.dma.manager.processor.template;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class FindByNameTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(FindByNameTemplateProcessor.class);
		
	protected TemplateAccessor templateAccessor;
	
	public FindByNameTemplateProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> FindByNameTemplateProcessor");
		logger.debug("Process ===> FindByNameTemplateProcessor : {}", input.toJsonString());
		
		String templateName = (String)input.getValue("templateName");
		
		DMATemplate template = templateAccessor.findTemplateByName(templateName);

		//DataUtils.Version version = new DataUtils.Version(template.getVersionTemplate());
		
		/**
		 * Tanawat W.
		 * 2018-12-18
		 * 
		 * DMA 5.0
		 * 
		 */
		logger.info("template.getVersionTemplate() : " + template.getVersionTemplate());
		int version  = Integer.parseInt(  template.getVersionTemplate().split("\\.")[0] );

		if(version >= 5){
			logger.info("case : version 5.0");
			Map<String,Object> output = callHookSyncFindtmplate(input.getData()).json();
			
			output.put(DmaConstants.Template.Key.INFO_URL, template.getInfoUrl());
			
			return output;
		}else{
			logger.info("case : old version");
			return template;
		}
	}

}
