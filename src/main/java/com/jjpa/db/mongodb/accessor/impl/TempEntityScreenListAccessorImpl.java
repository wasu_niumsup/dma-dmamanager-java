package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityScreenListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityScreenListDocument;

import servicegateway.utils.ManagerArrayList;

public class TempEntityScreenListAccessorImpl implements TempEntityScreenListAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	
	protected TempEntityScreenListAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	private List<TempEntityScreenListDocument> findMongoByName(String name) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where(MongoConstants.Simple.NAME).is(name));
		return mongoDbAccessor.findByCriteria(findByName, TempEntityScreenListDocument.class);
	}
	
	
	
	@Override
	public List<Map<String, Object>> findByName(String name) throws Exception {
		
		/* find data from mongo */
		List<TempEntityScreenListDocument> resultList = findMongoByName(name);

		
		/* 
		 * convert mongoData to listData 
		 * */
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(resultList != null && resultList.size() != 0){
			for(TempEntityScreenListDocument resultMap : resultList){
				list.add(resultMap.getData());
			}
		}
		
		
		return list;
	}


	@Override
	public void deleteByName(String name, String user) throws Exception {

		/* find data from mongo */
		List<TempEntityScreenListDocument> resultList = findMongoByName(name);
		
		
		if(resultList != null && resultList.size() != 0){
			for(TempEntityScreenListDocument resultMap : resultList){
				mongoDbAccessor.delete(resultMap, user);
			}
		}
		
	}


	@Override
	public void saveByName(String name, Object dataListObj, String user) throws Exception {
		
		/* delete */
		deleteByName(name, user);
		
		
		/* 
		 * loop save
		 * */
		TempEntityScreenListDocument document;
		
		List<Map<String, Object>> dataList = ManagerArrayList.convertObjectToListMap(dataListObj);

		if(dataList != null && dataList.size() != 0){
			
			for(Map<String, Object> dataMap : (List<Map<String, Object>>)dataList){
				
				document = new TempEntityScreenListDocument(); 
				document.setName(name);
				document.setData(dataMap);
				
				mongoDbAccessor.save(document, user);
			}
		}
		
	}

}
