package com.jjpa.common.data.service.createpath;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import net.minidev.json.JSONArray;

public class PathValue implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum CreateType {
		Map(LinkedHashMap.class), Array(JSONArray.class), Other(null);
		@SuppressWarnings("rawtypes")
		Class type;

		@SuppressWarnings("rawtypes")
		CreateType(Class type) {
			this.type = type;
		}

		@Override
		public String toString() {
			if (this == Array) {
				return "ARRAY";
			} else if (this == Map) {
				return "MAP";
			} else {
				return "OBJECT";
			}
		}
	}

	// private Object value = null;
	private CreateType type = CreateType.Map;
	private String name = null;
	private int index = 0;

	public PathValue(CreateType type, String name) {
		this.type = type;
		this.name = name;
	}

	public PathValue(CreateType type, int index) {
		this.type = type;
		this.index = index;
	}

	public CreateType getType() {
		return type;
	}

	public void setType(CreateType type) {
		this.type = type;
	}

	public String getName() {
		if (name == null) {
			name = "";
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getSubPath() {
		if (name == null) {
			name = "";
		}
		if (type == CreateType.Array) {
			return name + "[" + index + "]";
		}
		return name;
	}

	/**
	 * compare type, if null it will ignore
	 * 
	 * @param obj
	 * @return
	 */
	public boolean isNotSameType(Object obj) {
		if (obj != null) {
			CreateType cType = getTypeByObject(obj);
			if (cType == this.type) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public boolean canAddDotToSubPath() {
		if (type == CreateType.Map) {
			return true;
		} else if (type == CreateType.Array) {
			if (name != null && !name.trim().isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public CreateType getTypeByObject(Object obj) {
		if (obj != null) {
			CreateType cType = CreateType.Other;
			if (obj.getClass().isAssignableFrom(Map.class) || (obj instanceof LinkedHashMap)) {
				cType = CreateType.Map;
			} else if (obj.getClass().isAssignableFrom(JSONArray.class) || (obj instanceof JSONArray)) {
				cType = CreateType.Array;
			}
			return cType;
		}
		return null;
	}

	public JSONArray createArray() {
		return createArray(null);
	}

	public JSONArray createArray(Object dObj) {

		JSONArray arrayInstance = new JSONArray();

		while (index >= arrayInstance.size()) {
			arrayInstance.add(null);
		}
		arrayInstance.set(index, dObj);
		return arrayInstance;
	}

	@SuppressWarnings("rawtypes")
	public LinkedHashMap createMap() {
		return createMap(null);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public LinkedHashMap createMap(Object obj) {
		LinkedHashMap mapInstance = new LinkedHashMap<>();
		if (obj != null) {
			mapInstance.put(name, obj);
		}
		return mapInstance;
	}

	public Object createObject(Object dObj, boolean canDefault) {
		if (getType() == CreateType.Array) {
			return createArray(dObj);			
		} else if (getType() == CreateType.Map) {			
			if(canDefault) {
				return createMap(dObj);
			}else {
				return createMap();
			}
		}
		return dObj;
	}

	@Override
	public String toString() {
		if (type == CreateType.Array) {
			return "[Array,(" + index + ")]";
		} else if (type == CreateType.Map) {
			return "[Map,(" + name + ")]";
		} else {
			return "[Object]";
		}
	}

}
