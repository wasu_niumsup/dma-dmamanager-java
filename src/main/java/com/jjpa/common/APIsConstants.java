package com.jjpa.common;

public class APIsConstants {

	public class ExternalApiType{
		public static final String DYNAMIC_DROPDOWN 		= "dynamicdropdown";
		public static final String WEB_SERVICE_APIS 		= "webserviceapis";
		public static final String WEB_SERVICE_DROPDOWN 	= "webservicedropdown";
		public static final String JSON_SERVICE_APIS 		= "jsonserviceapis";
		public static final String JSON_SERVICE_DROPDOWN	= "jsonservicedropdown";
	}

	public class Key{
		
		public static final String VALUE 				= "Value";
		
		public static final String SERVICE_NAME 		= "serviceName";
		public static final String EXTERNAL_APIS_TYPE 	= "externalApisType";

		public static final String WSDL_LOCATION 		= "wsdlLocation";
		public static final String SERVICE_END_POINT 	= "serviceEndpoint";
		public static final String PORT_TYPE_NAME 		= "portTypeName";
		public static final String PORT_TYPE_NAME_LIST 	= "portTypeNameList";
		public static final String OPERATION_NAME 		= "operationName";
		public static final String OPERATION_NAME_LIST 	= "operationNameList";
		
		public static final String INPUT_FIELD_LIST 	= "inputFieldList";
		public static final String OUTPUT_FIELD_LIST 	= "outputFieldList";
		
		public static final String INDEX 				= "index";
		public static final String LABEL_NAME 			= "labelName";
		public static final String FIELD_NAME 			= "fieldName";
		public static final String FRONT_FIELD_NAME 	= "frontFieldName";
		public static final String XPATH 				= "xpath";		
		public static final String FIELD_TYPE 			= "fieldType";
		public static final String PROMOTE 				= "promote";
		public static final String DEFAULT_VALUE		= "defaultValue";	
		public static final String REQUIRED 			= "required";
		public static final String MAX_LENGTH 			= "maxlength";
		public static final String SERVICE_ID 			= "serviceId";
		
		public static final String CODE_SUCCESS_TYPE 	= "codeSuccessType";
		public static final String SUCCESS_VALUE 		= "successValue";
		public static final String WORD_SUCCESS 		= "wordSuccess";
		
		public static final String NEED_INPUT 			= "needInput";
		public static final String VALUE_FROM 			= "valueFrom";
		public static final String SUBMIT_FROM 			= "submitFrom";
		public static final String TYPE 				= "type";
		public static final String ON_CHANGE_LOAD 		= "onChangeLoad";
		
		public static final String JSON_LIST 			= "jsonList";
		public static final String JSON_SAMPLE_LIST 	= "jsonSampleList";
		public static final String DATASET_LIST 		= "dataSetList";
		public static final String DATASET_TYPE 		= "dataSetType";
	}
	
	public class Value{
		public static final String YES 					= "yes";
		public static final String NO 					= "no";
	}
	
	public class Type{
		
		public static final String UNUSED 				= "unused";
		public static final String STRING 				= "string";
		public static final String BOOLEAN 				= "boolean";
		public static final String TEXTFIELD 			= "textfield";
		public static final String JSON 				= "json";
		public static final String CODE 				= "code";
		public static final String SUCCESS 				= "success";
		public static final String CODE_SUCCESS 		= "codesuccess";
		public static final String MESSAGE 				= "message";
		public static final String DATA_LIST 			= "datalist";
		public static final String DATA_MAP 			= "datamap";
		public static final String DATASET 				= "dataset";	
	}

	public class DropDownType{
		
		public static final String VALUE 				= "value";
		public static final String LABEL 				= "labelname";
	}

}
