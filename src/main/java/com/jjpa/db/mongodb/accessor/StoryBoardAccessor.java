package com.jjpa.db.mongodb.accessor;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;

public interface StoryBoardAccessor {

	/* 04/01/2562 add by Akanit : data is very large size. */
	public void loadRelateData(StoryBoardDocument document, String storyName) throws Exception;
	public void moveRelateData(StoryBoardDocument document, String user) throws Exception;
	public void deleteRelateData(String storyName, String user) throws Exception;
	public List<StoryBoardDocument> findAllNotGetRelateData() throws Exception;
	
	
	public StoryBoardDocument findStoryByName(String storyName) throws Exception;
	public StoryBoardDocument findStoryByUiName(AppUiDocument appUiDoc) throws Exception;
	public Map<String,List<Map<String, Object>>> listStoryBoardWithApi(ServiceData input) throws Exception;
	public Map<String,Object> listStory(ServiceData input) throws Exception;
	public Map<String,Object> listStoryByCriteria(Query query) throws Exception;
	public void deleteStory(ServiceData input) throws Exception;
	public Object saveStoryBoard(StoryBoardDocument document, String user) throws Exception;
	
	public Object updateStoryBoardOneLevel(StoryBoardDocument storyBoardDoc, String user) throws Exception;
	public void updateGroups(AppUiDocument appUiDoc, String user) throws Exception;
	public void updateGroups(StoryBoardDocument document, AppUiDocument appUiDoc, String user) throws Exception;
	public void updateVersionScreenToStoryboard(AppUiDocument appUiDoc) throws Exception;
	public void updateVersionScreenToStoryboard(AppUiDocument appUiDoc, StoryBoardDocument storyDoc) throws Exception;
	
	public void upgradeVersionScreenToStoryboard(AppUiDocument appUiDoc) throws Exception;
	public void upgradeVersionScreenToStoryboard(AppUiDocument appUiDoc, StoryBoardDocument storyDoc) throws Exception;
	
	public boolean isNeedUpgradeScreen(String storyBoardDoc) throws Exception;
	public boolean isNeedUpgradeScreen(StoryBoardDocument storyBoardDoc) throws Exception;
	public boolean isNeedUpgradeStoryBoard(Map<String, Object> storyBoardMap, String storyboardName) throws Exception;
	
	public Map<String, Object> bindingNeedUpgradeScreen(StoryBoardDocument storyBoardDoc)throws Exception;
	public void bindingNeedUpgradeStoryBoard(StoryBoardDocument storyBoardDoc) throws Exception;
	public void bindingUpdateVersionScreenToStoryboard(Map<String, Object> storyBoardRow, AppUiDocument appUiDoc) throws Exception;
	public void bindingUpgradeVersionScreenToStoryboard(Map<String, Object> storyBoardRow, AppUiDocument appUiDoc) throws Exception;

	public int count(Query query) throws Exception;
	
}
