package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardTargetTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardTargetTypeDocument;

public class StoryBoardTargetTypeAccessorImpl implements StoryBoardTargetTypeAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected StoryBoardTargetTypeAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Map<String,List<Map<String, Object>>> listStoryBoardTargetType(ServiceData input) throws Exception{
		Map<String,List<Map<String, Object>>> targetType = new HashMap<String,List<Map<String, Object>>>();
		List<Map<String, Object>> targetTypeList = new ArrayList<Map<String, Object>>();
		List<StoryBoardTargetTypeDocument> storyboardTargetTypeList = mongoDbAccessor.findAll(StoryBoardTargetTypeDocument.class);
		for(StoryBoardTargetTypeDocument document : storyboardTargetTypeList){
			Map<String, Object> targetMap = new HashMap<String, Object>();
			targetMap.put("name", document.getName());
			targetMap.put("api", document.getApi());
			targetTypeList.add(targetMap);
		}
		targetType.put("targetType", targetTypeList);
		return targetType;
	}

}
