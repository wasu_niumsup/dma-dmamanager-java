package com.jjpa.dma.manager.processor.dmapreview;


import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.PreviewConfig;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

/**
 * ResetScreenShotProcessor
 */
public class ResetScreenShotProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

    
	private static final Logger logger = LoggerFactory.getLogger(ResetScreenShotProcessor.class);

	@Override
	public Object process(HttpServletRequest request, ServiceData serviceData) throws Exception {
		logger.info("Process ===> ResetScreenShotProcessor");

		Map<String, Object> input = serviceData.getData();
		logger.info("inputBody=" + input);

		String previewDomain = PreviewConfig.getPreviewURLEndpoint();

		if (!previewDomain.equals("")) {
			String callUrl = previewDomain + PropConstants.PreviewProperties.RESET_SCREENSHOT;
			logger.info("Call Preview at " + callUrl);
			Map<String, Object> responseData = getJsonAccessor().send(callUrl, JsonPath.parse(input)).json();
			logger.info("Response from dmaPreview = " + responseData.toString());
			if ((Boolean) responseData.get("success") == true) {
				return responseData.get("responseData");
			} else {
				String errorMessage = "Service error. Please contact admin";
				if (responseData.containsKey("responseDesc")) {
					errorMessage = (String) responseData.get("responseDesc");
				}
				throw new Exception(errorMessage);
			}
		} else {
			logger.info("Not Call Preview becuase domain not found.");
			throw new Exception("Cannot get dma preview enpoint url");
		}
	}
}