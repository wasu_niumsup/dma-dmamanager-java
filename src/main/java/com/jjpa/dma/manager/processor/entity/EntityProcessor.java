package com.jjpa.dma.manager.processor.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.jjpa.common.DmaConstants;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class EntityProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(EntityProcessor.class);
	
	protected EntityAccessor entityAccessor;
	
	public EntityProcessor() {
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.debug("Do EntityProcessor");
		
		String functionName = (String)input.getValue("functionName");
		logger.debug("Do function : {}", functionName);
		if("/entity/listEntity".equals(functionName)){
			return entityAccessor.listEntity(input);
		}else if("/entity/findEntityByName".equals(functionName)){
			String entityName = (String)input.getValue("entityName");
			return entityAccessor.findEntityByName(entityName);
		}else if("/entity/saveEntity".equals(functionName)){
			return saveEntity(input);
		}else if("/entity/listTheme".equals(functionName)){
			return listTheme();
		}else if("/entity/deleteEntity".equals(functionName)){
			entityAccessor.deleteEntity(input);
			return entityAccessor.listEntity(input);
		}else if("/entity/listEntityStatus".equals(functionName)){
			return listEntityStatus();
		}
		return input.getData();
	}
	
	private Object saveEntity(ServiceData input) throws Exception {
		EntityDocument oldDoc = entityAccessor.findEntityByName((String)input.getValue("entityName"));
		EntityDocument document = new EntityDocument();
		if((input.getValue("id")!=null && !"".equals(((String)input.getValue("id")).trim()))){
			String id = (String)input.getValue("id");
			if(oldDoc!=null && !id.equals(oldDoc.getId())){
				throw new Exception("Name duplicated");
			}
			document.setId(id);
		}else if(oldDoc != null){
			throw new Exception("Name duplicated");
		}
		document.setEntityName((String)input.getValue("entityName"));
		document.setRemark((String)input.getValue("remark"));
		document.setStoryName((String)input.getValue("storyName"));
		document.setRole((String)input.getValue("role"));
		document.setTheme((String)input.getValue("theme"));
		document.setStatus((String)input.getValue("status"));
		document.setLastUpdate(new Date());
		document.setAppInfo(input.getValue(DmaConstants.Entity.Key.APP_INFO));
		String user = (String)input.getValue("user");
		return entityAccessor.saveEntity(document, user);
	}
	
	private Map<String, Object> listTheme() throws Exception {
		Map<String, Object> themeResponse = new HashMap<String, Object>();
		List<Map<String, Object>> themeList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.THEME){
			Map<String, Object> theme = new HashMap<String, Object>();
			theme.put("themeName", s);
			themeList.add(theme);
		}
		themeResponse.put("themeList", themeList);
		return themeResponse;
	}
	
	private Map<String, Object> listEntityStatus() throws Exception {
		Map<String, Object> statusResponse = new HashMap<String, Object>();
		List<Map<String, Object>> statusList = new ArrayList<Map<String, Object>>();
		for(String s : DmaManagerConstant.ACTIVE_STATUS){
			Map<String, Object> status = new HashMap<String, Object>();
			status.put("statusName", s);
			statusList.add(status);
		}
		statusResponse.put("statusList", statusList);
		return statusResponse;
	}
	
}
