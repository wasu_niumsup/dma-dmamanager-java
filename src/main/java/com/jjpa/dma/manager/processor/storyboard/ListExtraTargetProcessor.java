package com.jjpa.dma.manager.processor.storyboard;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardExtraTargetTypeAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListExtraTargetProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListExtraTargetProcessor.class);
	
	protected StoryBoardExtraTargetTypeAccessor storyBoardExtraTargetTypeAccessor;

	public ListExtraTargetProcessor() {
		this.storyBoardExtraTargetTypeAccessor = SpringApplicationContext.getBean("StoryBoardExtraTargetTypeAccessor");
    }
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListExtraTargetProcessor");
		logger.debug("Process ===> ListExtraTargetProcessor : {}", input.toJsonString());

		return storyBoardExtraTargetTypeAccessor.listStoryBoardExtraTargetType(input);
		
	}

}