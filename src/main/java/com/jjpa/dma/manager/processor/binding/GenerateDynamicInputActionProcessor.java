package com.jjpa.dma.manager.processor.binding;

import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.APIsConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.common.util.ServiceGatewayUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.dma.manager.binding.action.NetworkRequest;
import com.jjpa.dma.manager.binding.action.SetGlobalToApi;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.api.ServiceGatewayApi;
import servicegateway.config.ServiceGatewayConfig;
import servicegateway.utils.ManagerMap;

//@SuppressWarnings("unchecked")
public class GenerateDynamicInputActionProcessor {

	private static final Logger logger = LoggerFactory.getLogger(GenerateDynamicInputActionProcessor.class);

	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}

	public List<Map<String, Object>> process(AppUiDocument appUiDoc, List<Map<String, Object>> actionChainList,
			Map<String, Object> storyBoard) throws Exception {
		logger.debug("###Process ===> GenerateDynamicInputActionProcessor");

		String templateType = appUiDoc.getTemplateType();

		if (DmaConstants.AppUI.Value.DYNAMIC_INPUT.equals(templateType)) {
			logger.debug("case : templateType=" + templateType);

			generateDynamicInputAction(appUiDoc, actionChainList, storyBoard);

		}
		return actionChainList;
	}

	private void generateDynamicInputAction(AppUiDocument appUiDoc, List<Map<String, Object>> actionList,
			Map<String, Object> storyBoard) throws Exception {

		// set service name
		logger.debug("=== Binding initial form ===");
		String apiName = appUiDoc.getDynamicApi();

		// add extra event to template
		logger.debug("Start : Start generate action for dynamic form : request configuration of servcie : " + apiName);
		Map<String, Object> sgResponse = new ServiceGatewayApi().findServiceByName(apiName);
	

		/* new 19/01/2561 add by Akanit : */
		ServiceGatewayConfig sgConfig = new ServiceGatewayConfig(sgResponse);
		List<Map<String, Object>> serviceFieldsList = sgConfig.getInputFieldList_raw();
		logger.debug("inputFieldList="+ serviceFieldsList);
		
		/* 21/02/2561 add by Akanit :  */
		String externalApisType = sgConfig.getExternalApisType();

		Map<String, Object> indexFieldActionMap = new HashMap<String, Object>();

		if (serviceFieldsList != null) {
			
			boolean promote, onChangeLoad;
			String key, fieldType, fieldName, needInput, valueFrom, serviceName_child;
			Map<String, Object> fieldActionMap;
			
			// first loop for create fieldlist with index
			for (Map<String, Object> serviceData : serviceFieldsList) {
				promote = ManagerMap.getBoolean(serviceData, APIsConstants.Key.PROMOTE);
				if (promote) {
					fieldActionMap = new HashMap<String, Object>();

					/* 22/02/2561 add by Akanit : */
					key = ServiceGatewayUtils.getKeyField(externalApisType, serviceData);

					fieldName = ManagerMap.getString(serviceData, APIsConstants.Key.FIELD_NAME);
					fieldActionMap.put(APIsConstants.Key.FIELD_NAME, fieldName);
					indexFieldActionMap.put(key, fieldActionMap);
				}
			}

			logger.debug(" first loop for create fieldlist with index = " + indexFieldActionMap);

			// second loop for create add relate field
			for (Map<String, Object> serviceData : serviceFieldsList) {
				promote = ManagerMap.getBoolean(serviceData, APIsConstants.Key.PROMOTE);
				if (promote) {
					fieldType = (String) serviceData.get(APIsConstants.Key.FIELD_TYPE);
					fieldName = (String) serviceData.get(APIsConstants.Key.FIELD_NAME);
					
					if (APIsConstants.ExternalApiType.DYNAMIC_DROPDOWN.equals(fieldType)) {
						
						needInput = ManagerMap.getString(serviceData, APIsConstants.Key.NEED_INPUT);
						if (APIsConstants.Value.YES.equalsIgnoreCase(needInput)) {
							
							List<Map<String, Object>> inputFieldList = ManagerMap.get(serviceData, APIsConstants.Key.INPUT_FIELD_LIST, List.class);
							
							for (Map<String, Object> inputField : inputFieldList) {
								
								valueFrom = ManagerMap.getString(inputField, APIsConstants.Key.VALUE_FROM);
								onChangeLoad = ManagerMap.getBoolean(inputField, APIsConstants.Key.ON_CHANGE_LOAD);
								
								if (onChangeLoad && !StringUtil.isBlank(valueFrom)) {
									logger.debug("found valueFrom =" + inputField);

									fieldActionMap = ManagerMap.get(indexFieldActionMap, valueFrom, Map.class);
									
									logger.debug("find fieldActionMap by valueFrom =" + fieldActionMap);
									fieldActionMap.put("trigger", "load_" + fieldName);
								}
							}
						}
					}
				}
			}
			// final loop for generate action
			for (Map<String, Object> serviceData : serviceFieldsList) {
				promote = ManagerMap.getBoolean(serviceData, APIsConstants.Key.PROMOTE);
				if (promote) {
					String inputType = DmaManagerConstant.DYNAMIC_FORM_INPUT_TYPE.get(serviceData.get(APIsConstants.Key.FIELD_TYPE));
					logger.debug("Step loop read Promoted field : {}", serviceData);
					if (inputType == null) {
						inputType = "textfield";
					}

					if ("dropdown".equals(inputType)) {
						
						needInput = ManagerMap.getString(serviceData, APIsConstants.Key.NEED_INPUT);
						serviceName_child = ManagerMap.getString(serviceData, APIsConstants.Key.SERVICE_ID);

						if (APIsConstants.Value.YES.equalsIgnoreCase(needInput) && !StringUtil.isBlank(serviceName_child)) {

							String trigger = "";
							String sourceIndex = "";
							logger.debug("Tracking : =====> field action Map : {}", indexFieldActionMap);

							List<Map<String, Object>> inputFieldList = ManagerMap.get(serviceData, APIsConstants.Key.INPUT_FIELD_LIST, List.class);
							
							for (Map<String, Object> inputField : inputFieldList) {
								
								valueFrom = ManagerMap.getString(inputField, APIsConstants.Key.VALUE_FROM);
								onChangeLoad = ManagerMap.getBoolean(inputField, APIsConstants.Key.ON_CHANGE_LOAD);
								
								if (onChangeLoad && !StringUtil.isBlank(valueFrom)) {
									sourceIndex = valueFrom;
								}
							}
							if (!StringUtil.isBlank(sourceIndex)) {
								
								fieldActionMap = ManagerMap.get(indexFieldActionMap, sourceIndex, Map.class);
								
								logger.debug("Tracking : =====> data from field action index {} : {}", sourceIndex, fieldActionMap);

								/**
								 * Generate Dynamic Dropdown  Action for Trigger
								 * 
								 */
								if (fieldActionMap.containsKey("trigger")) {
									trigger = (String) fieldActionMap.get("trigger");
								}
								if (!StringUtil.isBlank(trigger)) {

									/**
									 * Tanawat W.
									 *
									 * [2018-02-09] Add feature show error when SG error
									 * [2018-02-20] Add feature use global in sub service
									 */
									logger.debug("Add action for trigger (NEW !): {}", trigger);
									String postUrl = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/send";
									fieldName = ManagerMap.getString(serviceData, APIsConstants.Key.FIELD_NAME);

									Map<String, Object> requestData = new LinkedHashMap<String, Object>();
									requestData.put(DmaManagerConstant.DMA_TYPE, "dropdown");
									requestData.put(DmaManagerConstant.DMA_UUID, serviceName_child);
									requestData.put(DmaManagerConstant.DMA_SERVICE, apiName);
									requestData.put(DmaManagerConstant.DMA_RELATE_FROM,
											fieldActionMap.get(APIsConstants.Key.FIELD_NAME));
									requestData.put(DmaManagerConstant.DMA_FIELD, fieldName);
									
									/**
									 * Tanawat W.
									 * [2018-07-10] improve performance send data
									 */
									//requestData.put(DmaManagerConstant.DMA_DATA, "{{JSON.stringify($get)}}");
									new NetworkRequest().setData(storyBoard, requestData);


									requestData.put("contentType", "200");
									requestData.put("message", "Request dropdown field " + fieldName);
									requestData.put("source", "2");

									
									Map<String, Object> trigger_chain = generateTriggerDropdropAction(apiName,
											serviceName_child, postUrl, requestData, fieldName, storyBoard);

									logger.debug("Final step (NEW !!): trigger for action from field : " + fieldName
											+ " {}", trigger_chain);

									Map<String, Object> map = new LinkedHashMap<>();
									map.put(trigger, trigger_chain);

									actionList.add(map);

								}
							}
						}

					}
				}
			}
		}
	}

	private Map<String, Object> generateTriggerDropdropAction(String serviceName, String currentServiceName, String url,
			Map<String, Object> requestData, String targetFieldName, Map<String, Object> storyBoard) throws Exception {
		/**
		 * Tanawat W.
		 * 2018-02-09
		 * Add feature show error when SG error
		 * 
		 */
		logger.debug("serviceName :  " + serviceName + "\t, currentServiceName : " + currentServiceName);

		/**
		 * Tanawat W.
		 * [2018-05-08] Change transaction message.
		 */
		String message = "";
		message = "Call : " + currentServiceName;
		requestData.put( DmaManagerConstant.MESSAGE, message);
		requestData.put( DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.CALL_SUB_API);


		String prefix = "$.nativeJson";

		InputStream is = GenerateDynamicInputActionProcessor.class
				.getResourceAsStream("/action/triggerDynamicDropdown.json");
		DocumentContext ctx = JsonPath.parse(is);
		Map<String, Object> networkOptions = ctx.read(prefix + ".success.options");
		Map<String, Object> networkOptionsData = ManagerMap.get(networkOptions, "data", Map.class);

		networkOptions.put("url", url);

		networkOptionsData.putAll(requestData);

		/**
		 * Set targetName
		 */
		Map<String, Object> utilPropertyOptions = ctx.read(prefix + ".success.success.success.success.success.options");
		utilPropertyOptions.put("name", targetFieldName);

		/**
		 * 
		 * Set globalInput to sub service
		 */
		logger.debug("Set globalInput to sub service");
		List<Map<String, Object>> actionList = ManagerMap.get(storyBoard, DmaConstants.StoryBoard.Key.ACTION_LIST, List.class);
		
		String apiName;
		Map<String, Object> eventMap = new LinkedHashMap<>();
		for (Map<String, Object> itMap : actionList) {
			apiName = ManagerMap.getString(itMap, DmaConstants.StoryBoard.Key.API_NAME);
			if (serviceName.equalsIgnoreCase(apiName)) {
				logger.debug("found serviceName == apiName : " + apiName);
				eventMap = new LinkedHashMap<>(itMap);
				break;
			}
		}
		logger.debug("eventMap = " + eventMap);

		/**
		 * Parintorn 17/6/61
		 * Set error data
		 */
		Map<String, Object> firstError = ctx.read(prefix + ".success.success.success.success.error.success");
		Map<String, Object> optionsError = new LinkedHashMap<>();
		optionsError.put("name", targetFieldName);
		optionsError.put("data_source", "{{$get.temp_result.resultData}}");
		firstError.put("type", "$util.property");
		firstError.put("options", optionsError);
		firstError.put("success", new LinkedHashMap<>());
		firstError.put("error", new LinkedHashMap<>());
		
	

		Map<String, Object> optionsData = ctx.read(prefix + ".success.options.data");
		optionsData.put("_dma_split_data", new LinkedHashMap<>());
		String bindingPath = prefix + ".success.options.data._dma_split_data";

		SetGlobalToApi g = new SetGlobalToApi(new SetGlobalToApi.ISetData() {

			@Override
			public Boolean skipCondition(String parrent_serviceName, String child_serviceName) {

				Boolean result = true;

				Boolean condtion = !StringUtil.isBlank(currentServiceName) && !StringUtil.isBlank(child_serviceName)
						&& currentServiceName.equalsIgnoreCase(child_serviceName);
				if (condtion) {
					result = false;
				}

				logger.debug("isSkip : " + result + " , parrent_serviceName : " + currentServiceName
						+ " ,child_serviceName : " + child_serviceName);

				return result;
			}

			@Override
			public DocumentContext setCtx(DMAActionChainDocument document) {
				return ctx;
			}

			@Override
			public String getBindingPath(DMAActionChainDocument document) {
				return bindingPath;
			}
		});
		@SuppressWarnings("unused")
		DocumentContext dd = g.bind(null, null, storyBoard, eventMap);

		
		return ctx.read(prefix);

	}

}
