package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardStoryBoardListAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardStoryBoardListDocument;

public class StoryBoardStoryBoardListAccessorImpl implements StoryBoardStoryBoardListAccessor{

	protected MongoDbAccessor mongoDbAccessor;
	
	protected StoryBoardStoryBoardListAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	private List<StoryBoardStoryBoardListDocument> findMongoByName(String name) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where(MongoConstants.Simple.NAME).is(name));
		return mongoDbAccessor.findByCriteria(findByName, StoryBoardStoryBoardListDocument.class);
	}
	
	private StoryBoardStoryBoardListDocument findOneMongoByScreenName(String screenName) throws Exception {
		Query findByName = new Query().addCriteria(Criteria.where("data.source").is(screenName));
		return mongoDbAccessor.findOneByCriteria(findByName, StoryBoardStoryBoardListDocument.class);
	}
	
	
	
	@Override
	public List<Map<String, Object>> findByName(String name) throws Exception {
		
		/* find data from mongo */
		List<StoryBoardStoryBoardListDocument> resultList = findMongoByName(name);

		
		/* 
		 * convert mongoData to listData 
		 * */
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardStoryBoardListDocument resultMap : resultList){
				list.add(resultMap.getData());
			}
		}
		
		
		return list;
	}


	@Override
	public void deleteByName(String name, String user) throws Exception {

		/* find data from mongo */
		List<StoryBoardStoryBoardListDocument> resultList = findMongoByName(name);
		
		
		/* 
		 * loop delete
		 * */
		if(resultList != null && resultList.size() != 0){
			for(StoryBoardStoryBoardListDocument resultMap : resultList){
				mongoDbAccessor.delete(resultMap, user);
			}
		}
		
	}


	@Override
	public void saveByName(String name, List<Map<String, Object>> dataList, String user) throws Exception {
		
		/* delete */
		deleteByName(name, user);
		
		
		/* 
		 * loop save
		 * */
		StoryBoardStoryBoardListDocument document;
		
		if(dataList != null && dataList.size() != 0){
			
			for(Map<String, Object> dataMap : dataList){
				
				document = new StoryBoardStoryBoardListDocument(); 
				document.setName(name);
				document.setData(dataMap);
				
				mongoDbAccessor.save(document, user);
			}
		}
		
	}

	public void updateOneByScreenName(String screenName, Map<String, Object> dataMap, String user) throws Exception {
		
		StoryBoardStoryBoardListDocument document = findOneMongoByScreenName(screenName);
		document.setData(dataMap);
		
		mongoDbAccessor.save(document, user);
	}
	
}
