package com.jjpa.dma.manager.processor.appui;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListWithActionAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(ListWithActionAppUiProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public ListWithActionAppUiProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListWithActionAppUiProcessor");
		logger.debug("Process ===> ListWithActionAppUiProcessor : {}", input.toJsonString());
		
		return appUiAccessor.listAppUiWithAction(input);
		
	}
	
}
