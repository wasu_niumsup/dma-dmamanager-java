package com.jjpa.dma.manager.processor.reference;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToScreenProcessor;
import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToTemplateProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToScreenProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToStoryBoardProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;
import servicegateway.utils.ManagerLogger;
import servicegateway.utils.ManagerMap;

public class FixReferenceProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(FixReferenceProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	protected TemplateAccessor templateAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	
	private String user = "FixReference";
	
	public FixReferenceProcessor() {
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Start Process ===> FixReference");
		fixRefAll();
		logger.info("End Process ===> FixReference");
		return new LinkedHashMap<>();
	}
	
	
	
	public void fixRefAll() throws Exception {
		fixRefTemplate();
		fixRefStoryboard();
		fixRefScreen();
	}
	
	
	
	/* 
	 * StoryBoard
	 * */
	public void fixRefStoryboard() throws Exception {
		logger.info("process : fixRefStoryboard.");
		ManagerLogger managerLog = new ManagerLogger();
		managerLog.logStartTime();
		
		List<StoryBoardDocument> storysDoc = storyBoardAccessor.findAllNotGetRelateData();
		if( storysDoc==null || storysDoc.size()==0 ){
			return;
		}
		
		for(StoryBoardDocument storyDoc : storysDoc){
			fixRefStoryboard(storyDoc);
		}
		
		managerLog.logTotalTime("fixRefStoryboard", Thread.currentThread());
	}
	public void fixRefStoryboard(StoryBoardDocument storyDoc) throws Exception {
		
		fixScreenRequiredReferenceToStoryboard(storyDoc);
//		fixTemplateRequiredReferenceToStoryboard(storyDoc);

	}
	private void fixScreenRequiredReferenceToStoryboard(StoryBoardDocument storyDoc) throws Exception {
		
		List<Map<String, Object>> screenRequiredList = new GetReferenceProcessor().getDmaScreenRequireds(storyDoc.getReference());
		if( screenRequiredList==null || screenRequiredList.size()==0){
			return;
		}else {
			screenRequiredList = new ArrayList<>(screenRequiredList);
		}
		
		
		/* 
		 * 24/05/2562 Tag by Akanit
		 * 
		 * This method 'appUiAccessor.findAppUiByName' is function call database. 
		 *    it in for loop, if for loop = 100 then call database 100 loop
		 *    it very slow.
		 *   
		 * In the future : should query IN (query many row by nameList)  
		 *    it very fast.
		 *    but memory of Java will be used a lot (use mem very large size)
		 * 	  server maybe down.
		 * */
		String screenName;
		AppUiDocument appUiDoc;
		for(Map<String, Object> screen : screenRequiredList){
			
			screenName = ManagerMap.getString(screen, "name");
			
			appUiDoc = appUiAccessor.findAppUiByName(screenName);
			if(appUiDoc==null){
				new DeleteReferenceProcessor().clearRequiredDmaScreen(storyDoc.getReference(), screenName);
				storyBoardAccessor.updateStoryBoardOneLevel(storyDoc, user);
				logger.info("fix ref storyboard '"+ storyDoc.getStoryName() +"', remove required screen '"+ screenName +"'.");
				continue;
			}
			
			if(!isScreenInUsedByStorboard(appUiDoc, storyDoc.getStoryName())){
				new BindingInUsedReferenceToScreenProcessor().process(appUiDoc, storyDoc);
				appUiAccessor.saveUi(appUiDoc, user);
				logger.info("fix ref screen '"+ appUiDoc.getUiName() +"', add inUsedBy storyboard '"+ storyDoc.getStoryName() +"'.");
			}
			
			String templateName = new GetReferenceProcessor().getDmaTemplateRequired(appUiDoc.getReference());

			if(!isStroyBoardRequiredTemplate(storyDoc, templateName)){
				new BindingRequiredReferenceToStoryBoardProcessor().bindingDmaTemplateRequiredReference(storyDoc, appUiDoc);
				storyBoardAccessor.updateStoryBoardOneLevel(storyDoc, user);
				logger.info("fix ref storyboard '"+ storyDoc.getStoryName() +"', add required template '"+ templateName +"'.");
			}
		}
		
	}
//	private void fixTemplateRequiredReferenceToStoryboard(StoryBoardDocument storyDoc) throws Exception {
//		
//		List<Map<String, Object>> templateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(storyDoc.getReference());
//		if( templateRequiredList==null || templateRequiredList.size()==0 ){
//			return;
//		}else {
//			templateRequiredList = new ArrayList<>(templateRequiredList);
//		}
//		
//		String templateName;
//		DMATemplate templateDoc;
//		for(Map<String, Object> template : templateRequiredList){
//			
//			templateName = ManagerMap.getString(template, "name");
//			
//			templateDoc = templateAccessor.findTemplateByName(templateName);
//			if( templateDoc==null || !isTemplateInUsedByStorboard(templateDoc, storyDoc.getStoryName()) ){
//				new DeleteReferenceProcessor().clearRequiredDmaTemplate(storyDoc.getReference(), templateName);
//				storyBoardAccessor.loadRelateData(storyDoc, storyDoc.getStoryName());
//				storyBoardAccessor.saveStoryBoard(storyDoc, user);
//				logger.info("fix ref storyboard '"+ storyDoc.getStoryName() +"', remove required template '"+ templateName +"'.");
//				continue;
//			}
//			
//		}
//		
//	}
	private boolean isScreenInUsedByStorboard(AppUiDocument appUiDoc, String storyName) throws Exception {
		
		List<Map<String, Object>> inUsedByStoryboardList = new GetReferenceProcessor().getInUsedByDmaStroyBoards(appUiDoc.getReference());
		if( inUsedByStoryboardList==null || inUsedByStoryboardList.size() == 0){
			return false;
		}
		
		return ManagerArrayList.findByKey(inUsedByStoryboardList, "name", storyName);
	}
//	private boolean isTemplateInUsedByStorboard(DMATemplate templateDoc, String storyName) throws Exception {
//		
//		List<Map<String, Object>> inUsedByStoryboardList = new ArrayList<>();
//		try{
//			inUsedByStoryboardList = templateDoc.getReference().getInUsed().getInUsedByDmaStroyBoard();
//			if(inUsedByStoryboardList==null){
//				return false;
//			}
//		}catch (Exception e){
//			return false;
//		}
//		
//		return ManagerArrayList.findByKey(inUsedByStoryboardList, "name", storyName);
//	}
	
	
	
	/* 
	 * Screen
	 * */
	public void fixRefScreen() throws Exception {
		logger.info("process : fixRefScreen.");
		ManagerLogger managerLog = new ManagerLogger();
		managerLog.logStartTime();
		
		List<AppUiDocument> appUisDoc = appUiAccessor.findAll();
		if( appUisDoc==null || appUisDoc.size()==0 ){
			return;
		}
		
		for(AppUiDocument appUiDoc : appUisDoc){
			fixRefScreen(appUiDoc);
		}
		
		managerLog.logTotalTime("fixRefScreen", Thread.currentThread());
	}
	public void fixRefScreen(AppUiDocument appUiDoc) throws Exception {
		fixInUsedByStoryboardReferenceToScreen(appUiDoc);
		fixTemplateRequiredReferenceToScreen(appUiDoc);
	}
	private void fixInUsedByStoryboardReferenceToScreen(AppUiDocument appUiDoc) throws Exception {
		
		List<Map<String, Object>> inUsedByStroyBoardList = new GetReferenceProcessor().getInUsedByDmaStroyBoards(appUiDoc.getReference());
		if( inUsedByStroyBoardList==null || inUsedByStroyBoardList.size()==0){
			return;
		}else {
			inUsedByStroyBoardList = new ArrayList<>(inUsedByStroyBoardList);
		}
		
		String storyboardName;
		StoryBoardDocument storyDoc;
		for(Map<String, Object> inUsedByStoryBoard : inUsedByStroyBoardList){
			
			storyboardName = ManagerMap.getString(inUsedByStoryBoard, "name");
			
			storyDoc = storyBoardAccessor.findStoryByName(storyboardName);
			if(storyDoc==null){
				new DeleteReferenceProcessor().clearInUsedByDmaStroyBoard(appUiDoc.getReference(), storyboardName);
				appUiAccessor.saveUi(appUiDoc, user);
				logger.info("fix ref screen '"+ appUiDoc.getUiName() +"', remove inUsedBy storyboard '"+ storyboardName +"'.");
				continue;
			}
			
			if(!isStroyBoardRequiredScreen(storyDoc, appUiDoc.getUiName())){
				
				Map<String, Object> storyBoard = ManagerArrayList.findRowBySearchString(storyDoc.getStoryBoard(), DmaConstants.StoryBoard.Key.SOURCE, appUiDoc.getUiName());
				
				new BindingRequiredReferenceToStoryBoardProcessor().process(storyDoc, storyBoard, appUiDoc);
				storyBoardAccessor.updateStoryBoardOneLevel(storyDoc, user);
				logger.info("fix ref storyboard '"+ storyboardName +"', add required screen '"+ appUiDoc.getUiName() +"'.");
			}
		}
		
	}
	private void fixTemplateRequiredReferenceToScreen(AppUiDocument appUiDoc) throws Exception {
		
		List<Map<String, Object>> templateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(appUiDoc.getReference());
		if( templateRequiredList==null || templateRequiredList.size()==0){
			return;
		}else {
			templateRequiredList = new ArrayList<>(templateRequiredList);
		}
		
		String templateName;
		DMATemplate templateDoc;
		for(Map<String, Object> template : templateRequiredList){
			
			templateName = ManagerMap.getString(template, "name");
			
			templateDoc = templateAccessor.findTemplateByName(templateName);
			if(templateDoc==null){
				throw new Exception("template '"+ templateName +"' not found. Please contact admin.");
			}
			
			if(!isTemplateInUsedByScreen(templateDoc, appUiDoc.getUiName())){
				new BindingInUsedReferenceToTemplateProcessor().process(templateDoc, appUiDoc);
				templateAccessor.saveTemplate(templateDoc, user);
				logger.info("fix ref template '"+ appUiDoc.getUiName() +"', add inUsedBy screen '"+ appUiDoc.getUiName() +"'.");
			}
		}
	}
	private boolean isStroyBoardRequiredScreen(StoryBoardDocument storyDoc, String screenName) throws Exception {
		
		List<Map<String, Object>> screenRequiredList = new GetReferenceProcessor().getDmaScreenRequireds(storyDoc.getReference());
		if( screenRequiredList==null || screenRequiredList.size() == 0){
			return false;
		}
		
		return ManagerArrayList.findByKey(screenRequiredList, "name", screenName);
	}
	public static void main(String[] args) {
		List<Map<String, Object>> a = null;
		if(a==null){
			System.out.println(a);
		}
	}
	private boolean isStroyBoardRequiredTemplate(StoryBoardDocument storyDoc, String templateName) throws Exception {

		List<Map<String, Object>> templateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(storyDoc.getReference());
		if( templateRequiredList==null || templateRequiredList.size() == 0){
			return false;
		}
		
		return ManagerArrayList.findByKey(templateRequiredList, "name", templateName);
	}
	private boolean isTemplateInUsedByScreen(DMATemplate templateDoc, String screenName) throws Exception {
		
		List<Map<String, Object>> inUsedByScreenList = new GetReferenceProcessor().getInUsedByDmaScreens(templateDoc.getReference());
		if( inUsedByScreenList==null || inUsedByScreenList.size() == 0){
			return false;
		}
		
		return ManagerArrayList.findByKey(inUsedByScreenList, "name", screenName);
	}
	
	
	
	/* 
	 * Template
	 * */
	public void fixRefTemplate() throws Exception {
		logger.info("process : fixRefTemplate.");
		ManagerLogger managerLog = new ManagerLogger();
		managerLog.logStartTime();
		
		List<DMATemplate> templatesDoc = templateAccessor.listTemplates();
		if( templatesDoc==null || templatesDoc.size()==0 ){
			return;
		}
		
		for(DMATemplate templateDoc : templatesDoc){
			fixRefTemplate(templateDoc);
		}
		
		managerLog.logTotalTime("fixRefTemplate", Thread.currentThread());
	}
	public void fixRefTemplate(DMATemplate templateDoc) throws Exception {
		
		List<Map<String, Object>> inUsedByScreenList = new GetReferenceProcessor().getInUsedByDmaScreens(templateDoc.getReference());
		if( inUsedByScreenList==null || inUsedByScreenList.size()==0){
			return;
		}else {
			inUsedByScreenList = new ArrayList<>(inUsedByScreenList);
		}
		
		String screenName;
		AppUiDocument appUiDoc;
		for(Map<String, Object> screen : inUsedByScreenList){
			
			screenName = ManagerMap.getString(screen, "name");
			
			appUiDoc = appUiAccessor.findAppUiByName(screenName);
			if(appUiDoc==null){
				new DeleteReferenceProcessor().clearInUsedByDmaScreen(templateDoc.getReference(), screenName);
				templateAccessor.saveTemplate(templateDoc, user);
				logger.info("fix ref template '"+ templateDoc.getTemplateName() +"', remove inUsedBy screen '"+ screenName +"'.");
				continue;
			}
			
			if(!isScreenRequiredTemplate(appUiDoc, templateDoc.getTemplateName())){
				new BindingRequiredReferenceToScreenProcessor().process(appUiDoc, templateDoc);
				appUiAccessor.saveUi(appUiDoc, user);
				logger.info("fix ref screen '"+ appUiDoc.getUiName() +"', add required template '"+ templateDoc.getTemplateName() +"'.");
			}
		}

	}
	private boolean isScreenRequiredTemplate(AppUiDocument appUiDoc, String templateName) throws Exception {
		
		List<Map<String, Object>> templateRequiredList = new GetReferenceProcessor().getDmaTemplateRequireds(appUiDoc.getReference());
		if( templateRequiredList==null || templateRequiredList.size() == 0){
			return false;
		}
		
		return ManagerArrayList.findByKey(templateRequiredList, "name", templateName);
	}
	
}
