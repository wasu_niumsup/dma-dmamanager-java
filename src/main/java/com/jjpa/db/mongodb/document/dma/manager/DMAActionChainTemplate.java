package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jjpa.dma.manager.model.reference.Reference;

import servicegateway.utils.ManagerMap;

@Document(collection = "dma_action_chain_template")
public class DMAActionChainTemplate extends BaseDMADocument{

	private static final long serialVersionUID = 1L;

	@Indexed(unique=true)
	private String actionType;
    private Map<String, Object> chainTemplate;
    private Reference refference;
    private String version;

    public DMAActionChainTemplate() {
	}
    
    public DMAActionChainTemplate(Map<String, Object> dataMap) throws Exception {
    	
    	
    	/* ************************** set extends *********************************** */
		this.setId(ManagerMap.getString(dataMap, "id"));
		this.setCreateBy(ManagerMap.getString(dataMap, "createBy"));
		this.setLastUpdateBy(ManagerMap.getString(dataMap, "lastUpdateBy"));
		this.setDmaManagerVersion(ManagerMap.getString(dataMap, "dmaManagerVersion"));
		this.setNativeJson(ManagerMap.get(dataMap, "nativeJson", Map.class));
		this.setBinding(ManagerMap.get(dataMap, "binding", List.class));
		
		/* set create date */
		Long createDate = ManagerMap.getLong(dataMap, "createDate");
		if(createDate!=null){
			this.setCreateDate(new Date(createDate));
		}
		
		/* set lastUpdate date */
		Long lastUpdateDate = ManagerMap.getLong(dataMap, "lastUpdateDate");
		if(lastUpdateDate!=null){
			this.setLastUpdateDate(new Date(lastUpdateDate));
		}
		
		/* set lastUpdate */
		Long lastUpdate = ManagerMap.getLong(dataMap, "lastUpdate");
		if(lastUpdate!=null){
			this.setLastUpdate(new Date(lastUpdate));
		}else {
			Object lastUpdateObj = dataMap.get("lastUpdate");
			if(lastUpdateObj instanceof Date){
				this.setLastUpdate( (Date)lastUpdateObj );
			}
		}
		/* *************************************************************************** */
		
		
		/* ****************************** set this *********************************** */
		this.setActionType(ManagerMap.getString(dataMap, "actionType"));
		this.setChainTemplate(ManagerMap.get(dataMap, "chainTemplate", Map.class));
		
		Map<String, Object> referenceMap = ManagerMap.get(dataMap, "refference", Map.class);
		Reference reference = new ObjectMapper().convertValue(referenceMap, Reference.class);
		
		this.setReference(reference);
		this.setVersion(ManagerMap.getString(dataMap, "version"));
		/* *************************************************************************** */
		
	}
    
    public void setActionType(String actionType){
        this.actionType = actionType;
    }

    public String getActionType(){
        return actionType;
    }

    public void setChainTemplate(Map<String, Object> chainTemplate){
        this.chainTemplate = chainTemplate;
    }

    public Map<String, Object> getChainTemplate(){
        return chainTemplate;
    }

	public Reference getReference() {
		return refference;
	}

	public void setReference(Reference reference) {
		this.refference = reference;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public Map<String, Object> toMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		
		map.put("id", this.getId());
		map.put("createBy", this.getCreateBy());
		map.put("lastUpdateBy", this.getLastUpdateBy());
		map.put("createDate", this.getCreateDate().getTime());
		map.put("lastUpdateDate", this.getLastUpdateDate().getTime());
		map.put("dmaManagerVersion", this.getDmaManagerVersion());
		
		map.put("nativeJson", this.getNativeJson());
		map.put("binding", this.getBinding());
		try{
			map.put("lastUpdate", this.getLastUpdate().getTime());
		}catch(Exception e){
			map.put("lastUpdate", "");
		}
		
		map.put("actionType", this.getActionType());
		map.put("chainTemplate", this.getChainTemplate());
		map.put("refference", this.getReference());
		map.put("version", this.getVersion());
		
		return map;
	}
    
}