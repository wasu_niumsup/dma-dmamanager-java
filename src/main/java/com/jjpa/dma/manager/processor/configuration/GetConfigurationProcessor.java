package com.jjpa.dma.manager.processor.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ConfigurationAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAConfigurationDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class GetConfigurationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetConfigurationProcessor.class);
		
	protected ConfigurationAccessor configurationAccessor;
	
	public GetConfigurationProcessor() {
		this.configurationAccessor = SpringApplicationContext.getBean("ConfigurationAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> GetConfigurationProcessor");
		logger.debug("Process ===> GetConfigurationProcessor : {}", input.toJsonString());
		
		return getConfiguration(input);
		
	}
	
	private Map<String, Object> getConfiguration(ServiceData input) throws Exception{
		String configName = (String)input.getValue("configName");
		DMAConfigurationDocument configDoc = configurationAccessor.findConfigurationByName(configName);
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("configName", configDoc.getConfigName());
		output.put("nativeJson", JsonPath.parse(configDoc.getNativeJson().get("data")).json());
		return output;
	}

}
