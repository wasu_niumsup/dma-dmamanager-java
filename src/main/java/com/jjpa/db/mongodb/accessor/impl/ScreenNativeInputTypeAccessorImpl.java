package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ScreenNativeInputTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.ScreenNativeInputTypeDocument;

public class ScreenNativeInputTypeAccessorImpl implements ScreenNativeInputTypeAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected ScreenNativeInputTypeAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Map<String,List<Map<String, Object>>> listScreenNativeInputType(ServiceData input) throws Exception{
		Map<String,List<Map<String, Object>>> nativeInputType = new HashMap<String,List<Map<String, Object>>>();
		List<Map<String, Object>> nativeInputList = new ArrayList<Map<String, Object>>();
		List<ScreenNativeInputTypeDocument> screenNativeInputTypeList = mongoDbAccessor.findAll(ScreenNativeInputTypeDocument.class);
		for(ScreenNativeInputTypeDocument document : screenNativeInputTypeList){
			Map<String, Object> inputMap = new HashMap<String, Object>();
			inputMap.put("globalName", document.getGlobalName());
			inputMap.put("fieldType", document.getFieldType());
			inputMap.put("source", document.getSource());
			inputMap.put("api", document.getApi());
			inputMap.put("action", document.getAction());
			inputMap.put("dataStructure", document.getDataStructure());
			inputMap.put("className", document.getClassName());
			inputMap.put("dmaParameter", document.getDmaParameter());
			nativeInputList.add(inputMap);
		}
		nativeInputType.put("globalList", nativeInputList);
		return nativeInputType;
	}

}
