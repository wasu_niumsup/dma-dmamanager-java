package com.jjpa.common;

public class PatchConstants {

	public static final String PATCH_VERSION 		= "patch_version";
	public static final String PATCH_DESCRIPTION 	= "patch_description";
	public static final String PATCH_LAST_UPDATE 	= "patch_lastUpdate";
	
	public class Input{
		
		public static final String STORY_LIST 	= "storyList";
		public static final String START_VERSION 	= "startVersion";
	}
	
}
