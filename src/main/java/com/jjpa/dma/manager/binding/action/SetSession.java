package com.jjpa.dma.manager.binding.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public class SetSession implements ActionBinding{

	private final Logger logger = LoggerFactory.getLogger(SetSession.class);
	
	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) {
		logger.info("Binding set session in Action");
		DocumentContext ctx =  JsonPath.parse(document.getNativeJson().get("data"));
		
		List<Map<String, Object>> bindingList = document.getBinding();
		
		String bindingPath;
		
		for(Map<String, Object> binding : bindingList){
			
			String bindingName = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			if("options".equals(bindingName)){
				
				bindingPath = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));
				
				
				// binding domain
				String publishUrl = "dma.beebuddy.net";
				try {
					logger.info("get publish url.");
					publishUrl =JasonConfigurationUtil.getOnlyDomain();// JasonConfigurationUtil.getPublishUrl();
					
				} catch (Exception e) {
					logger.info("can not get publish url then use default publish url=dma.beebuddy.net");
				}
				
				String domainName = publishUrl.replace("https://", "").replace("http://", "");
				ctx.put(bindingPath, "domain", domainName);
							
				
				// binding header
				Map<String, Object> header = new LinkedHashMap<>();
				header.put("DMA-Device-Key", "{{$get.result.dmaDeviceKey}}");
				
				ctx.put(bindingPath, "header", header);
				
				break;
			}
		}
			
		return ctx;
	}

}
