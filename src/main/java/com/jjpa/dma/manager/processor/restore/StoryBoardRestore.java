package com.jjpa.dma.manager.processor.restore;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerStore;

public class StoryBoardRestore {

	private static final Logger logger = LoggerFactory.getLogger(StoryBoardRestore.class);
	
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardAccessor storyBoardAccessor;
	
	public StoryBoardRestore() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	
	public List<String> process(String filePath, String user) throws Exception {
		logger.info("Process ===> StoryBoardRestore");
		logger.info("filePath="+ filePath);
		
		List<String> storyList = new ArrayList<>();
		
		File file = new File(filePath);
		if(file.exists()){
			
			StoryBoardDocument document;
			Map<String, Object> jsonMap;
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {
					
					storyList.add(fileEntry.getName());
					
					jsonMap = ManagerStore.readJsonMapFromFile(fileEntry, true);
					jsonMap.remove("id");
					
					/* 16/05/2561 new lastUpdate, clear lastPublish */
					jsonMap.put(MongoConstants.Path.LAST_UPDATE, new Date());
					jsonMap.put(MongoConstants.Path.LAST_PUBLISH, "");
	
					document = new StoryBoardDocument(jsonMap);
					
					storyBoardAccessor.saveStoryBoard(document, user);
					logger.info("restore StoryBoardName="+ document.getStoryName());
				}
			}
		}
		
		logger.info("End Process ===> StoryBoardRestore");
		return storyList;
	}
	
}
