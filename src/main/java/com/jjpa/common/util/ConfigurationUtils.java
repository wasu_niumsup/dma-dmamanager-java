package com.jjpa.common.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.dma.manager.processor.info.InfoProcessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.jayway.jsonpath.DocumentContext;
//import com.jayway.jsonpath.JsonPath;
//import com.jjpa.http.JsonAccessor;
//import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;

public class ConfigurationUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationUtils.class);
	
	private static final String configPath = "/opt/stack/dma/conf/";
	//private static final String configPath = "D:\\opt\\stack\\dma\\conf\\";
	private static final String configFile = "dmaconfig.properties";
	private static final String PUBLISH_URL = "publishUrl";
	
	private static String publishUrl;
	private static String engineVersion=null;
	
	public static String getPublishUrl() throws Exception{
		if(StringUtil.isBlank(publishUrl)){
			reloadPublishUrl();
		}
		return publishUrl;
	}

	@SuppressWarnings("unchecked")
	public static String getEngineVersion(){
		if(engineVersion == null){
			try {
				Map<String, Object> info = (Map<String, Object>) new InfoProcessor().process(new ServiceData());
				engineVersion = DataUtils.getStringValue(info, "version","0.0.0");

			} catch (Exception e) {
				logger.error("getEngineVersion error : {}",e);		
			}
		}
		
		return engineVersion;
	}

	public static String getTempPreview(){


		
		return "/tmp/preview/";
	}

//	Unused method for new config reader
//	private static JsonAccessor getJsonAccessor() {
//		return SpringApplicationContext.getBean("JsonAccessor");
//	}
	
//  old method read publish url from wdchat config
//	public static void reloadPublishUrl() throws Exception{
//		DocumentContext requestCtx = JsonPath.parse("{}");
//		DocumentContext responseCtx = getJsonAccessor().send("/rest/ws/console/gatewayconfig", requestCtx);
//		publishUrl = responseCtx.read("publish_url");
//	}
	
// new method read publish url from property file store on server
	public static void reloadPublishUrl() throws Exception{
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(configPath + configFile);

			// load a properties file
			prop.load(input);

			// get the property value
			publishUrl = prop.getProperty(PUBLISH_URL);
			logger.info("Caching ===> Load publish url from config to cache : {}", publishUrl);
		} catch (IOException ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw e;
				}
			}
		}
	}
	
	public static void clearPublishUrlCache(){
		publishUrl = null;
	}

}
