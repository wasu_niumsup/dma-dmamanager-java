package com.jjpa.db.mongodb.model;

public class WhereOption {

	private String path;
	private String value;
	
	private boolean search;
	private boolean sensitive;
	
	private boolean notEqual;
	private boolean acceptNull;
	private boolean acceptBlank;
	
	private boolean between;
	private Object start;
	private Object end;
	
	private WhereOption and;
	private WhereOption or;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public boolean isSensitive() {
		return sensitive;
	}
	public void setSensitive(boolean sensitive) {
		this.sensitive = sensitive;
	}
	
	public boolean isNotEqual() {
		return notEqual;
	}
	public void setNotEqual(boolean notEqual) {
		this.notEqual = notEqual;
	}
	public boolean isAcceptNull() {
		return acceptNull;
	}
	public void setAcceptNull(boolean acceptNull) {
		this.acceptNull = acceptNull;
	}
	public boolean isAcceptBlank() {
		return acceptBlank;
	}
	public void setAcceptBlank(boolean acceptBlank) {
		this.acceptBlank = acceptBlank;
	}
	
	public boolean isBetween() {
		return between;
	}
	public void setBetween(boolean between) {
		this.between = between;
	}
	public Object getStart() {
		return start;
	}
	public void setStart(Object start) {
		this.start = start;
	}
	public Object getEnd() {
		return end;
	}
	public void setEnd(Object end) {
		this.end = end;
	}
	
	public WhereOption getAnd() {
		return and;
	}
	public void setAnd(WhereOption and) {
		this.and = and;
	}
	public WhereOption getOr() {
		return or;
	}
	public void setOr(WhereOption or) {
		this.or = or;
	}
	
}
