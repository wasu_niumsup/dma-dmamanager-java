package com.jjpa.dma.manager.processor.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.ComponentAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAComponent;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

@SuppressWarnings("unchecked")
public class SaveComponentProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
private static final Logger logger = LoggerFactory.getLogger(SaveComponentProcessor.class);
	
	protected ComponentAccessor componentAccessor;
	
	public SaveComponentProcessor() {
		this.componentAccessor =  SpringApplicationContext.getBean("ComponentAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> SaveComponentProcessor");
		logger.debug("Process ===> SaveComponentProcessor : {}", input.toJsonString());
		
		return saveComponent(input);
		
	}
	
	private Object saveComponent(ServiceData input) throws Exception{
		String user = (String)input.getValue("user");
		DMAComponent component = new DMAComponent();
		component.setComponentName((String)input.getValue("componentName"));
		component.setBinding((List<Map<String,Object>>)input.getValue("binding"));
		DocumentContext ctx = JsonPath.parse(input.getValue("nativeJson"));
		String nativeJson = (String)ctx.jsonString();
		Map<String,String> data = new HashMap<String, String>();
		data.put("data", nativeJson);
		component.setNativeJson(data);
		return componentAccessor.saveComponent(component, user);
	}

}
