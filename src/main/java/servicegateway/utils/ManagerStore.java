package servicegateway.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

@SuppressWarnings({"resource"})
public class ManagerStore {

	private static Logger logger = LoggerFactory.getLogger(ManagerStore.class);
	
	public static String getFileNameByPart(String pathFile) throws Exception {
		String[] pathSplit = pathFile.split("\\/");
		if(pathSplit.length==1){
			String[] pathSplit2 = pathFile.split("\\");
			if(pathSplit2.length==1){
				return pathFile;
			}else{
				return pathSplit2[pathSplit2.length-1];
			}
		}else{
			return pathSplit[pathSplit.length-1];
		}
	}
	
	public static List<InputStream> getInputStreamFromZip(String pathZip) throws Exception{

		ZipFile zipFile = new ZipFile(pathZip);
	    ZipEntry zipEntry;	
	    InputStream inputStream;
	    List<InputStream> inputStreamList = new ArrayList<InputStream>();
	    
	    Enumeration<? extends ZipEntry> entries = zipFile.entries();
	    while(entries.hasMoreElements()){		
	    	zipEntry = entries.nextElement();    
	    	inputStream = zipFile.getInputStream(zipEntry);
	    	inputStreamList.add(inputStream);  		
	    }
	    return inputStreamList;
	}
	
	public static List<InputStream> getInputStreamByIndexEntryNameFromZip(String pathZip,String entryName,String fileType) throws Exception{
		
		int index_zipEntry = 1;
		String pathEntry;
	    ZipEntry zipEntry;	
	    InputStream inputStream;
	    List<InputStream> inputStreamList = new ArrayList<InputStream>();
	    
	    ZipFile zipFile = new ZipFile(pathZip);
	    
	    for(int i = 0 ; i < zipFile.size() ; i++){
	    	
	    	pathEntry = entryName+"_"+String.valueOf(index_zipEntry)+fileType;
	    	logger.info("(findEntryInZip)pathEntry : "+pathEntry);
	    	
	    	zipEntry = zipFile.getEntry(pathEntry);
	    	
	    	if(zipEntry!=null){
	    		logger.info("(findEntryInZip)found");
	    		inputStream = zipFile.getInputStream(zipEntry);
		    	inputStreamList.add(inputStream);
		    	index_zipEntry++;
	    	}else{
	    		logger.info("(findEntryInZip)not found");
	    		break;
	    	}	
	    }
	    return inputStreamList;
	}
	
	public static synchronized List<String> extractZipFile(String zipSrc, String extractPath) throws Exception{
		if(zipSrc==null||"".equals(zipSrc)){
			return null;
		}
		
		return extractZipFile(new File(zipSrc), extractPath);
	}
	
	public static synchronized List<String> extractZipFile(File file, String extractPath) throws Exception{
		if(file==null || !file.exists()){
			logger.info("file=null or empty");
			return null;
		}
		if(extractPath==null){
			logger.info("file=null");
			return null;
		}
		logger.info("fileName="+file.getName());
		
		extractPath = ManagerPath.toPath(extractPath);
		logger.info("extractPath="+extractPath);
		
		List<String> entryPathList = new ArrayList<>();
		
		
		/* extract zip */
	    String fileSrc;
		ZipEntry zipEntry;
		ZipFile zipFile = new ZipFile(file);
	    Enumeration<? extends ZipEntry> entries = zipFile.entries();
	    while(entries.hasMoreElements()){	
	    	
	    	zipEntry = entries.nextElement();    
	    	fileSrc = extractPath + zipEntry.getName();
	    	
	    	/* write file : param is File, Bype[] */
	    	FileUtils.writeByteArrayToFile(new File(extractPath, zipEntry.getName()), IOUtils.toByteArray(zipFile.getInputStream(zipEntry)));
	    	
	    	logger.info("create file : fileSrc="+fileSrc);
	    	entryPathList.add(fileSrc);	
	    }
	    
	    return entryPathList;
	}
	
	public static synchronized List<String> extractZipFile(CommonsMultipartFile file, String extractPath) throws Exception{
		if(file==null){
			logger.info("file=null");
			return null;
		}
		if(extractPath==null){
			logger.info("file=null");
			return null;
		}
		logger.info("fileName="+file.getName());
		
		extractPath = ManagerPath.toPath(extractPath);
		logger.info("extractPath="+extractPath);
		
		List<String> entryPathList = new ArrayList<>();
		
		
		/* extract zip */
		String fileSrc;
		ZipEntry zipEntry;
		ZipInputStream zis = new ZipInputStream(file.getInputStream());
		while ((zipEntry = zis.getNextEntry()) != null) {
			
			fileSrc = extractPath + zipEntry.getName();
			
			/* write file */
			FileUtils.writeByteArrayToFile(new File(extractPath, zipEntry.getName()), IOUtils.toByteArray(zis));

			logger.info("create file : fileSrc="+fileSrc);
			entryPathList.add(fileSrc);	
		}
		
		return entryPathList;
	}
	
//	public void deleteTemp(String tableName,String sqlType) throws Exception{
//		JDBCService jdbcService = new JDBCService();
//		jdbcService.deleteTable(tableName,sqlType);
//		jdbcService.closeDB();
//	}
//	
//	public void deleteTemp(List tableName,String sqlType) throws Exception{
//		JDBCService jdbcService = new JDBCService();
//		for(int i = 0 ; i < tableName.size() ; i++){
//			jdbcService.deleteTable(tableName.get(i).toString(),sqlType);
//		}
//		jdbcService.closeDB();
//	}
//	
//	public void deleteTempBySession(String tableName,HttpSession session,String sqlType) throws Exception{
//		JDBCService jdbcService = new JDBCService();
//		Object oldTableNameObj = session.getAttribute(tableName);
//		if(oldTableNameObj!=null){
//			String oldTableName = (String)oldTableNameObj;
//			jdbcService.deleteTable(oldTableName,sqlType);
//			jdbcService.closeDB();
//			session.removeAttribute(tableName);
//		}
//	}
	
	public static void write(InputStream inputStream, String writeSrc) throws Exception{
		
		byte[] buffer = new byte[2048];
		FileOutputStream  output = new FileOutputStream(new File(writeSrc));
		
		int len;
        while ((len = inputStream.read(buffer)) > 0){
            output.write(buffer, 0, len);
        }
        output.close();
	}
	
	
	public static Map<String, Object> readJsonMapFromFile(String filePath, boolean deleteFile) throws Exception {
		return readJsonMapFromFile(new File(filePath), deleteFile);
	}
	public static Map<String, Object> readJsonMapFromFile(File file, boolean deleteFile) throws Exception {
		if(!file.exists()){
			throw new Exception("file not found. filePath="+ file.getAbsolutePath());
		}
		
		/* read restoreFile to jsonString */
		DocumentContext ctx = JsonPath.parse(file);
//		logger.info("readJsonObjectFromFile jsonString = "+ ctx.jsonString());
		logger.info("readJsonObjectFromFile jsonString = data is large then ignore show data.");
		
		if(deleteFile){
			file.delete();
			logger.info("delete file="+ file.getAbsolutePath());
		}
		
		return ctx.json();
		
	}
	
	public static String readJsonStringFromFile(String filePath, boolean deleteFile) throws Exception {
		return readJsonStringFromFile(new File(filePath), deleteFile);
	}
	public static String readJsonStringFromFile(File file, boolean deleteFile) throws Exception {
		if(!file.exists()){
			throw new Exception("file not found. filePath="+ file.getAbsolutePath());
		}
		
		/* read restoreFile to jsonString */
		DocumentContext ctx = JsonPath.parse(file);
//		logger.info("readJsonObjectFromFile jsonString = "+ ctx.jsonString());
		logger.info("readJsonObjectFromFile jsonString = data is large then ignore show data.");
		
		if(deleteFile){
			file.delete();
			logger.info("delete file="+ file.getAbsolutePath());
		}
		
		return ctx.jsonString();
	}
	
	/**
	 * example jsonObject : Map, mongoDocument
	 *  */
	public static String createFileByJsonMap(Map<String, Object> jsonMap, String directoryFile) throws Exception {
		
		String jsonString = JsonPath.parse(jsonMap).jsonString();
		
		// put string to file and write
		try{
			FileUtils.writeStringToFile(new File(directoryFile), jsonString, "UTF-8");
			logger.info("createFile : fileSrc="+directoryFile);
			
			return directoryFile;
			
		}catch (Exception e){
			logger.error("(createFile)exception", e);
			throw e;
		}
	}

	public static void deleteFile(File file) throws Exception{
		
		if(file.exists()){
	    	if(file.isDirectory()){

	    		if(file.list().length==0){
	    			file.delete();
	    			logger.info("Directory is deleted : "  + file.getAbsolutePath());
	    		}else{
	    			
	    			String files[] = file.list();
	    			for (String temp : files) {  
	    				File fileDelete = new File(file, temp);
	    				deleteFile(fileDelete);
	    			}
	    			
	    			if(file.list().length==0){
	    				file.delete();
	    				logger.info("Directory is deleted : " + file.getAbsolutePath());
	    			}
	    		}
	    		
	    	}else{
	    		file.delete();
	    		logger.info("File is deleted : " + file.getAbsolutePath());
	    	}
		}else {
			logger.info("File dose not exists.");
		}
    }
	
	public static void deleteFile(String pathFile) throws Exception {
		if(pathFile==null)return;
		deleteFile(new File(pathFile));
	}
	
	public static void deleteFiles(List<String> pathFileList) throws Exception {
		if(pathFileList==null)return;
		if(pathFileList.size()==0)return;
		
		for(String pathFile : pathFileList){
			deleteFile(pathFile);
		}
	}
	
	public static void deleteFileBySession(String fileName, HttpSession session) throws Exception {
		String oldPathFile = (String)session.getAttribute(fileName);
		deleteFile(oldPathFile);
		session.removeAttribute(fileName);
	}	
	
	public static String renameFile(String pathFile, String newPathFile)  throws Exception{
		File oldFile = new File(pathFile);	
		File newFile = new File(newPathFile);
		
		oldFile.renameTo(newFile);
		return newPathFile;
	}
	
	public static String addVersionFile(String pathFile,String version)  throws Exception{
				
		String[] sp = pathFile.split("_"),sp2;
		String newPathFile = "";
		for(int i = 0; i < sp.length ;i++){
			if(i==sp.length-1){
				
				sp2 = sp[i].split("\\.");
														
				newPathFile+="V"+version+"."+sp2[1];
			}else{
				newPathFile+=sp[i]+"_";
			}
		}
		
		File oldFile = new File(pathFile);				
		File newFile = new File(newPathFile);
		
		oldFile.renameTo(newFile);
		return newPathFile;
	}
	
	public static void addVersionFileBySession(String fileName,String version,HttpSession session)  throws Exception{
		
		String oldPathFile = (String)session.getAttribute(fileName);
		if(oldPathFile!=null){
			
			String[] sp = oldPathFile.split("_"),sp2;			
			
			String newPathFile = "";
			for(int i = 0; i < sp.length ;i++){
				if(i==sp.length-1){
					
					sp2 = sp[i].split(".");
															
					newPathFile+="V"+version+"."+sp2[1];
				}else{
					newPathFile+=sp[i]+"_";
				}
			}
			
			File oldFile = new File(oldPathFile);
					
			File newFile = new File(newPathFile);
			
			oldFile.renameTo(newFile);
			
			session.removeAttribute(fileName);
		}
	}
	
	public static void intervalCheckFileExists(String file, int loopTimeout, int delayTime) throws Exception{
		logger.info("do intervalCheckFileExists()");
		
		for(int i = 0;i < loopTimeout ;i++){
			logger.info("round : "+(i));
			if(new File(file).exists()){
				logger.info("file found : "+file);
				break;
			}else{
				logger.info("file not found : "+file);
				TimeUnit.MILLISECONDS.sleep(delayTime);
			}
		}
		logger.info("end intervalCheckFileExists()");
	}
	
	public static void intervalCheckFileExists(List<String> fileList, int loopTimeout, int delayTime) throws Exception{
		logger.info("do intervalCheckFileExists");
		
		boolean foundFile;
		for(int i = 0;i < loopTimeout ;i++){
			logger.info("round : "+(i));
			foundFile = true;
			for(String file : fileList){
				if(!new File(file).exists()){
					logger.info("sub file not found : "+file);
					foundFile=false;
					break;
				}
				logger.info("sub file found : "+file);
			}
			
			if(foundFile){
				logger.info("all file found");
				break;
			}else{
				logger.info("all file not found");
				TimeUnit.MILLISECONDS.sleep(delayTime);
			}
		}
		logger.info("end intervalCheckFileExists()");
	}
	
	public static void intervalCheckEntryZipFileExists(String pathZip, List<String> fileNameList, int loopTimeout, int delayTime) throws Exception{
		logger.info("do intervalCheckEntryZipFileExists");
		
		ZipFile zipFile = new ZipFile(pathZip);

		boolean foundFile;
		for(int i = 0;i < loopTimeout ;i++){
			logger.info("round : "+(i));
			foundFile = true;
			for(String fileName : fileNameList){
				
				try{
					ZipEntry zipEntry = zipFile.getEntry(fileName);
					zipEntry.getName();
					logger.info("sub file found : "+fileName);
				}catch (Exception e){
					logger.info("sub file not found : "+fileName);
					foundFile=false;
					break;
				}
			}		
			
			if(foundFile){
				logger.info("all file found");
				break;
			}else{
				logger.info("all file not found");
				TimeUnit.MILLISECONDS.sleep(delayTime);
			}
		}
		logger.info("end intervalCheckEntryZipFileExists");
	}
}
