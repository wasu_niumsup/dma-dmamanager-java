package servicegateway.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"rawtypes","unchecked"})
public class ManagerArrayList {
	
	public static boolean find(List list,String searchWord){
		if(list==null)return false;
		if(searchWord==null)return false;
		
		for(Object value : list){
			if(value.toString().trim().equals(searchWord.trim())){
				return true;
			}
		}
		return false;
	}
	
	public static boolean find(List list,List<String> searchList){
		if(list==null)return false;
		if(searchList==null)return false;
		
		boolean find;
		for(String searchWord : searchList){
			find = false;
			for(Object value : list){
				if(value.toString().trim().equals(searchWord.trim())){
					find = true;
					break;
				}
			}
			if(!find){
				return false;
			}
		}
		return true;
	}
	
	public static boolean find(List<Integer> listInt,int searchInt){
		if(listInt==null)return false;
		
		for(int value : listInt){
			if(value==searchInt){
				return true;
			}
		}
		return false;
	}
	
	public static boolean findByKey(List<Map<String, Object>> dataList, String key, String searchWord){
		String value;
		try{
			for(Map<String, Object> dataMap : dataList){
				value = String.valueOf(dataMap.get(key));
				if(searchWord.equals(value)){
					return true;
				}
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	public static List<Map<String, Object>> getRowListBySearch(List<Map<String, Object>> dataList, String searchKey, Object searchValue){
		if(dataList==null)return null;
		if(searchKey==null)return null;
		
		List<Map<String, Object>> result = new ArrayList<>();
		
		Object value;
		for(Map<String, Object> dataMap : dataList){
			value = dataMap.get(searchKey);
			if(value == searchValue){
				result.add(dataMap);
			}
		}
		return result;
	}
	
	public static List<Map<String, Object>> getRowListBySearchString(List<Map<String, Object>> dataList, String searchKey, String searchValue){
		if(dataList==null)return null;
		if(searchKey==null)return null;
		if(searchValue==null)return null;
		
		List<Map<String, Object>> result = new ArrayList<>();
		
		String value;
		for(Map<String, Object> dataMap : dataList){
			value = String.valueOf(dataMap.get(searchKey));
			if(value.equals(searchValue)){
				result.add(dataMap);
			}
		}
		return result;
	}
	
	public static Map<String, Object> findRowBySearch(List<Map<String, Object>> dataList, String searchKey, Object searchValue){
		if(dataList==null)return null;
		if(searchKey==null)return null;
		if(searchValue==null)return null;
		
		Object value;
		for(Map<String, Object> dataMap : dataList){
			value = dataMap.get(searchKey);
			if(value == searchValue){
				return dataMap;
			}
		}
		return null;
	}
	
	public static Map<String, Object> findRowBySearchString(List<Map<String, Object>> dataList, String searchKey, String searchValue){
		if(dataList==null)return null;
		if(searchKey==null)return null;
		if(searchValue==null)return null;
		
		String value;
		for(Map<String, Object> dataMap : dataList){
			value = String.valueOf(dataMap.get(searchKey));
			if(value.equals(searchValue)){
				return dataMap;
			}
		}
		return null;
	}
	
//	public static boolean find(List<Integer> listInt,List<Integer> searchIntList){
//		if(listInt==null)return false;
//		for(int value : listInt){
//			if(value==searchInt){
//				return true;
//			}
//		}
//		return false;
//	}
	
	public static boolean findBetween(List masterData,String searchWord) throws Exception{
		if(masterData==null)return false;
		
		List<BigDecimal> masterDataSort = sortStringNumber(masterData);
		BigDecimal searchWordBigd = new BigDecimal(searchWord);
		BigDecimal min = masterDataSort.get(0);
		BigDecimal max = masterDataSort.get(masterDataSort.size()-1);
		if(searchWordBigd.compareTo(min)>=0 && searchWordBigd.compareTo(max)<=0){
			return true;
		}
		return false;
	}
	
	public static boolean findBetweenMinMax(List<Map<String,String>> masterData,String searchWord,String factorMin,String factorMax) throws Exception{
		if(masterData==null)return false;
		
		BigDecimal min,max,searchWordBigd = new BigDecimal(searchWord);
		
		for(Map<String, String> map : masterData){
			min = new BigDecimal(map.get(factorMin));
			max = new BigDecimal(map.get(factorMax));
			if(searchWordBigd.compareTo(min)>=0 && searchWordBigd.compareTo(max)<=0){
				return true;
			}
		}
		return false;
	}
	
	public static List<Map<String, Object>> filterByKey(List<Map<String, Object>> dataList, String key) throws Exception{
		if(dataList==null)return null;
		if(key==null)return null;
		
		List<Map<String, Object>> resultList = new ArrayList<>(); 	
		Map<String, Object> map = new LinkedHashMap<>();
		Object value;
		for(Map<String, Object> dataMap : dataList){
			
			value = dataMap.get(key);
			if(value!=null){
				
				map = new LinkedHashMap<>();
				map.put(key, value);
				
				resultList.add(map);
			}
		}
		
		return resultList;
	}
	
	public static List<Map<String, Object>> filterByKey(List<Map<String, Object>> dataList, List<String> keyList) throws Exception{
		if(dataList==null)return null;
		if(keyList==null)return null;
		
		List<Map<String, Object>> resultList = new ArrayList<>(); 	
		Map<String, Object> map = new LinkedHashMap<>();
		Object value;
		for(Map<String, Object> dataMap : dataList){
			
			map = new LinkedHashMap<>();
			
			for(String key : keyList){
				value = dataMap.get(key);
				if(value!=null){
					map.put(key, value);			
				}
			}
			
			if(map.size()>0){
				resultList.add(map);
			}
		}
		
		return resultList;
	}
	
	public static void removeByKey(List<Map<String, Object>> list, String key){
		removeByKey(list, new ArrayList<>(Arrays.asList(key)));
	}

	public static void removeByKey(List<Map<String, Object>> dataList, List<String> keyList){
		if(dataList==null)return;
		if(keyList==null)return;
		
		for(Map dataMap : dataList){			
			for(String key : keyList){
				if(key==null)continue;
				dataMap.remove(key);
			}
		}
	}
	
	public static boolean isEmpty(List list){
		if(list==null)return false;
		
		String value;
		for(int i=0;i<list.size();i++){
			value = list.get(i).toString();
			if(value!=null && !"".equals(value.trim())){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSameValueArray(String [] list){
		if(list==null)return false;
		
		for(int i=0;i<list.length;i++){
			for(int j=i+1;j<list.length;j++){
				if(list[i].trim().equals(list[j].trim())){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isSameValueArray(List<String> list){
		if(list==null)return false;
		
		for(int i=0;i<list.size();i++){
			for(int j=i+1;j<list.size();j++){
				if(list.get(i).trim().equals(list.get(j).trim())){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isSameValueBySameKey(Map<String, String> map1,Map<String, String> map2){
		List<String> sameKey = getSameKey(map1,map2);
		if(sameKey==null)return false;
		
		String value1,value2;
		for(int i=0;i<sameKey.size();i++){
			value1 = map1.get(sameKey.get(i));
			value2 = map2.get(sameKey.get(i));
			if(!value1.trim().equals(value2.trim())){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSameValueBySameKey(Map<String, String> map1,Map<String, String> map2,List<String> ignoreList){
		List<String> sameKey = getSameKey(map1,map2,ignoreList);
		if(sameKey==null)return false;
		
		String value1,value2;
		for(int i=0;i<sameKey.size();i++){
			value1 = map1.get(sameKey.get(i));
			value2 = map2.get(sameKey.get(i));
			if(!value1.trim().equals(value2.trim())){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isOverlabValueArray(String value1,String value2,String symbo){

		int value1S,value1E,value2S,value2E;
//		boolean value1IsEmpty = false, value2IsEmpty = false;
		
//		if("".equals(value1)){
//			value1IsEmpty = true;
//		}
//		if("".equals(value2)){
//			value2IsEmpty = true;
//		}
//		
//		if(value1IsEmpty || value2IsEmpty){
//			if(value1IsEmpty && value2IsEmpty){
//				return true;
//			}else{
//				return false;
//			}
//		}
		
		
		if(value1.split(symbo).length==1){
			value1S = Integer.parseInt(value1);
			value1E = Integer.parseInt(value1);
		}else{
			value1S = Integer.parseInt(value1.split(symbo)[0]);
			value1E = Integer.parseInt(value1.split(symbo)[1]);
		}
		
		if(value2.split(symbo).length==1){
			value2S = Integer.parseInt(value2);
			value2E = Integer.parseInt(value2);
		}else{
			value2S = Integer.parseInt(value2.split(symbo)[0]);
			value2E = Integer.parseInt(value2.split(symbo)[1]);
		}
		
		if( value2S >= value1S && value2S <= value1E ){
			
			return true;
			
		}else if(value2S < value1S){
			
			if(value2E >= value1S){
				return true;
			}		
		}
		return false;
	}
	
///////////////////////////////////////////   get Result  ////////////////////////////////////////////////
	
	public static ArrayList<String> getColumn(String name,ArrayList<Map> list){
		if(name==null)return null;
		if(list==null)return null;
		
		ArrayList<String> result = new ArrayList<String>();
		for(Map row : list){
			if(row.get(name)==null)return null;
			result.add(row.get(name).toString());
		}
		return result;
	}
	
	public static Map getRow(String key,String value,ArrayList<Map> list){
		if(key==null)return null;
		if(value==null)return null;
		if(list==null)return null;

		Map row,result = new HashMap();		
		for(int i =0;i<list.size();i++){
			row = list.get(i);
			if(value.trim().equals(row.get(key).toString().trim())){
				result.putAll(row);
				return result;
			}
		}
		return null;
	}
	
	public static ArrayList<Map> getMultiRow(String key,String value,ArrayList<Map> list){
		if(list==null)return null;
		
		Map row;
		ArrayList<Map> result = new ArrayList<Map>();
		
		for(int i =0;i<list.size();i++){
			row = list.get(i);
			if(value.trim().equals(row.get(key).toString().trim())){
				result.add(row);
			}
		}
		if(result.size()>0){
			return result;
		}else{
			return null;
		}
	}
	
//	public static Map<String, String> getRow(String key,String value,ArrayList<Map<String, String>> list){
//		if(list==null){
//			return null;
//		}
//		Map<String, String> row,result = new HashMap<String, String>();
//		
//		for(int i =0;i<list.size();i++){
//			row = list.get(i);
//			if(value.trim().equals(row.get(key).trim())){
//				result.putAll(row);
//				return result;
//			}
//		}
//		return null;
//	}
	
	public static List<String> getSameKey(Map<String, String> map1,Map<String, String> map2){
		if(map1==null)return null;
		if(map2==null)return null;
		
		List<String> list = new ArrayList<String>();
		for(String key1 : map1.keySet()){
			for(String key2 : map2.keySet()){
				if(key1.trim().equals(key2.trim())){
					list.add(key1);
					break;
				}
			}
		}
		return list;
	}
	
	public static List<String> getSameKey(Map<String, String> map1,Map<String, String> map2,List<String> ignoreList){
		if(map1==null)return null;
		if(map2==null)return null;
		if(ignoreList==null)return null;

		List<String> list = new ArrayList<String>();
		boolean next;
		for(String key1 : map1.keySet()){
			next = false;
			for(String ignore : ignoreList){
				if(key1.trim().equals(ignore.trim())){
					next=true;
				}
			}
			if(next)continue;
			for(String key2 : map2.keySet()){
				if(key1.trim().equals(key2.trim())){
					list.add(key1);
					break;
				}
			}
		}
		return list;
	}
	
	public static List getSameValue(List list1,List list2){
		if(list1==null)return null;
		if(list2==null)return null;

		List result = new ArrayList();
		String value1, value2;
		for(int i = 0 ; i < list1.size() ; i++){
			value1 = list1.get(i).toString();
			
			for(int j = 0;j <list2.size() ; j++){
				value2 = list2.get(j).toString();
				
				if(value1.equalsIgnoreCase(value2)){
					result.add(value1);
					break;
				}
			}
		}
		return result;
	}
	
	public static List<Integer> getIndexSameValue(List list1,List list2){
		if(list1==null)return null;
		if(list2==null)return null;
		
		List<Integer> list = new ArrayList<Integer>();
		String value1;
		for(Object value2 : list2){
			for(int i=0;i<list1.size();i++){
				value1=list1.get(i).toString();
				if(value2.toString().trim().equals(value1.trim())){
					list.add(i);
					break;
				}
			}
		}
		return list;
	}
	
	public static List getValueByIndex(List list1,List<Integer> list2){
		if(list1==null)return null;
		if(list2==null)return null;
		
		List list = new ArrayList();
		for(Integer index : list2){
			list.add(list1.get(index).toString());
		}
		return list;
	}
	
	public static ArrayList<String> getBetween(List masterData,String start,String end) throws Exception{
		if(masterData==null)return null;
		
		List<BigDecimal> masterDataSort = sortStringNumber(masterData);
		BigDecimal amountPremium;
		BigDecimal startBigd = new BigDecimal(start);
		BigDecimal endBigd = new BigDecimal(end);
		ArrayList<String> list = new ArrayList<String>();
		for(int i=0;i<masterDataSort.size();i++){
			amountPremium = masterDataSort.get(i);
			if(amountPremium.compareTo(startBigd)>=0 && amountPremium.compareTo(endBigd)<=0){
				list.add(amountPremium.toString());
			}
		}
		return list;
	}
	
	/* 22/08/2560 create by Akanit :  */
	public static List<Object> getListByKey(List<Map<String, Object>> dataList, String key){
		if(dataList==null)return null;
		
		List<Object> result = new ArrayList<>();
			
		Object value;
		for(Map<String, Object> dataMap : dataList){
			value = dataMap.get(key);
			if(value!=null){
				result.add(value);
			}
		}
		return result;
	}
	
	public static List<String> getStringListByKey(List<Map<String, Object>> dataList, String key){
		if(dataList==null)return null;
		
		List<String> result = new ArrayList<>();
			
		Object value;
		for(Map<String, Object> dataMap : dataList){
			value = dataMap.get(key);
			if(value!=null){
				result.add(String.valueOf(value));
			}
		}
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : [{"name":"1"},{"name":"2"},{"name":"3"}]<br>
	 * list_new : [{"name":"3"},{"name":"4"},{"name":"5"}]<br>
	 * <br>
	 * when call getListDuplicateNodeByKey();<br>
	 * <br>
	 * result : [{"name":"3"}]<br>
	 *  */
	public static List<Map<String, Object>> getListDuplicateNodeByKey(String key, List<Map<String, Object>> list_old, List<Map<String, Object>> list_new) throws Exception {
		
		if( list_old==null || list_old.size() == 0 || list_new==null || list_new.size() == 0 ){
			return new ArrayList<>();
		}
		
		List<Map<String, Object>> result = new ArrayList<>();
		
		Map<String, Object> map_old;
		boolean foundInList;
		for(int i = 0 ; i < list_old.size() ; i++){
			
			map_old = list_old.get(i);
			
			String value_old = ManagerMap.getString(map_old, key);
			
			// stream().anyMatch is find string in list
			foundInList = list_new.stream().anyMatch(map -> ( (String)map.get(key) ).equals(value_old));
			
			if(foundInList){
				result.add(map_old);
			}
		}
		
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : [{"name":"1"},{"name":"2"},{"name":"3"}]<br>
	 * list_new : [{"name":"3"},{"name":"4"},{"name":"5"}]<br>
	 * <br>
	 * when call getStringListDuplicateNodeByKey();<br>
	 * <br>
	 * result : ["3"]<br>
	 *  */
	public static List<String> getStringListDuplicateNodeByKey(String key, List<Map<String, Object>> list_old, List<Map<String, Object>> list_new) throws Exception {
		
		if( list_old==null || list_old.size() == 0 || list_new==null || list_new.size() == 0 ){
			return new ArrayList<>();
		}
		
		List<String> result = new ArrayList<>();
		
		boolean foundInList;
		for(int i = 0 ; i < list_old.size() ; i++){
			
			String value_old = ManagerMap.getString(list_old.get(i), key);
			
			// stream().anyMatch is find string in list
			foundInList = list_new.stream().anyMatch(map -> ( (String)map.get(key) ).equals(value_old));
			
			if(foundInList){
				result.add(value_old);
			}
		}
		
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : [{"name":"1"},{"name":"2"},{"name":"3"}]<br>
	 * list_new : [{"name":"3"},{"name":"4"},{"name":"5"}]<br>
	 * <br>
	 * when call getListDisapearNodeByKey();<br>
	 * <br>
	 * result : [{"name":"1"},{"name":"2"}]<br>
	 *  */
	public static List<Map<String, Object>> getListDisapearNodeByKey(String key, List<Map<String, Object>> list_old, List<Map<String, Object>> list_new) throws Exception {
		
		List<Map<String, Object>> result = new ArrayList<>();
		
		if(list_old != null && list_old.size() > 0){
			
			Map<String, Object> map_old;
			boolean foundInList;
			for(int i = 0 ; i < list_old.size() ; i++){
				
				map_old = list_old.get(i);
	
				if( list_new == null || list_new.size() == 0 ){
					result.add(map_old);
					continue;
				}
				
				String value_old = ManagerMap.getString(map_old, key);
				
				// stream().anyMatch is find string in list
				foundInList = list_new.stream().anyMatch(map -> ( (String)map.get(key) ).equals(value_old));
				
				if(!foundInList){
					result.add(map_old);
				}
			}
			
		}
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : [{"name":"1"},{"name":"2"},{"name":"3"}]<br>
	 * list_new : [{"name":"3"},{"name":"4"},{"name":"5"}]<br>
	 * <br>
	 * when call getStringListDisapearNodeByKey();<br>
	 * <br>
	 * result : ["1","2"]<br>
	 *  */
	public static List<String> getStringListDisapearNodeByKey(String key, List<Map<String, Object>> list_old, List<Map<String, Object>> list_new) throws Exception {
		
		List<String> result = new ArrayList<>();
		
		if(list_old != null && list_old.size() > 0){
			
			boolean foundInList;		
			for(int i = 0 ; i < list_old.size() ; i++){
				String value = ManagerMap.getString(list_old.get(i), key);
	
				if( list_new == null || list_new.size() == 0 ){
					result.add(value);
					continue;
				}
				
				// stream().anyMatch is find string in list
				foundInList = list_new.stream().anyMatch(map -> ( (String)map.get(key) ).equals(value));
				
				if(!foundInList){
					result.add(value);
				}
			}
			
		}
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : ["1", "2", "3"]<br>
	 * list_new : ["3", "4", "5"]<br>
	 * <br>
	 * when call getDuplicateListString();<br>
	 * <br>
	 * result : ["3"]<br>
	 *  */
	public static List<String> getDuplicateListString(List<String> list_old, List<String> list_new) throws Exception {
		
		List<String> result = new ArrayList<>();
		
		if( list_old==null || list_old.size() == 0 || list_new==null || list_new.size() == 0 ){
			return result;
		}
		
		String value_old, value_new;
		boolean foundInList;
		for(int i = 0 ; i < list_old.size() ; i++){
			
			value_old = list_old.get(i);
			
			foundInList = false;
			for(int j = 0 ; j < list_new.size() ; j++){
				
				value_new = list_new.get(j);
				if(value_new==value_old){
					foundInList = true;
					break;
				}
			}
			
			if(foundInList){
				result.add(value_old);
			}
		}
		
		return result;
	}
	
	/**
	 * Example
	 * <br>
	 * list_old : ["1", "2", "3"]<br>
	 * list_new : ["3", "4", "5"]<br>
	 * <br>
	 * when call getDisapearListString();<br>
	 * <br>
	 * result : ["1", "2"]<br>
	 *  */
	public static List<String> getDisapearListString(List<String> list_old, List<String> list_new) throws Exception {
		
		List<String> result = new ArrayList<>();
		
		if( list_new == null || list_new.size() == 0 ){
			return list_old;
		}
		
		if(list_old != null && list_old.size() > 0){
			
			String value_old, value_new;
			boolean foundInList;
			for(int i = 0 ; i < list_old.size() ; i++){
				
				value_old = list_old.get(i);
				
				foundInList = false;
				for(int j = 0 ; j < list_new.size() ; j++){
					
					value_new = list_new.get(j);
					if(value_new==value_old){
						foundInList = true;
						break;
					}
				}
				
				if(!foundInList){
					result.add(value_old);
				}
			}
			
		}
		return result;
	}
	
	/* 03/10/2560 create by Akanit :  */
	public static String getFirstSameValueByKey(List<Map<String, Object>> list, String key){
		if(list==null)return null;
		
		Object value, valueNext;
		for(int i=0;i<list.size();i++){
			
			/* 17/11/2560 add by Akanit : fix bug : check value==null */
			value = list.get(i).get(key);
			if(value==null || "".equals(value.toString().trim())){
				continue;
			}
			
			for(int j=i+1;j<list.size();j++){
				
				/* 17/11/2560 add by Akanit : fix bug : check value==null */
				valueNext = list.get(j).get(key);
				if(valueNext==null || "".equals(valueNext.toString().trim())){
					continue;
				}
				
				if(value.toString().trim().equals(valueNext.toString().trim())){
					return list.get(i).get(key).toString().trim();
				}
			}
		}
		return null;
	}
	
	/* 22/08/2560 create by Akanit :  */
	public static String getFirstSameValue(List<String> list){
		if(list==null)return null;
		
		for(int i=0;i<list.size();i++){
			for(int j=i+1;j<list.size();j++){
				if(list.get(i).trim().equals(list.get(j).trim())){
					return list.get(i).trim();
				}
			}
		}
		return null;
	}
	
	public static List<Map<String, Object>> cloneByKey(List<Map<String, Object>> cloneList, String key){
		return cloneByKey(cloneList, new ArrayList<>(Arrays.asList(key)));
	}
	
	public static List<Map<String, Object>> cloneByKey(List<Map<String, Object>> cloneList, List<String> keyList){
		List<Map<String, Object>> result = new ArrayList<>();
		Map<String, Object> map;
		Object value;
		
		for(Map cloneMap : cloneList){
			
			map = new LinkedHashMap<>();
			for(String key : keyList){
				value = cloneMap.get(key);
				if(value!=null){
					map.put(key, value);
				}
			}			
			result.add(map);
		}
		return result;
	}
	
	public static List<BigDecimal> sortStringNumber(List stringNumberList) throws Exception{
		if(stringNumberList==null)return null;
		
		List<BigDecimal> sortNumberList = new ArrayList<BigDecimal>();
		
		for(Object number : stringNumberList) {
			sortNumberList.add(new BigDecimal(number.toString())); 
		}
		
		Collections.sort(sortNumberList);
		return sortNumberList;
	}
	
	// convert value of sortNumberList to default. example - sortNumberList = {1,2,11,12} change to default is {01,02,11,12}
	public static List sortStringNumberSensitive(List stringNumberList){
		if(stringNumberList==null)return null;
		
		List<BigDecimal> sortNumberList = new ArrayList<BigDecimal>();
		
		for(Object number : stringNumberList) {
			sortNumberList.add(new BigDecimal(number.toString())); 
		}
		
		Collections.sort(sortNumberList);
		
		List result = new ArrayList();
		for(BigDecimal number : sortNumberList) {
			result.add(number.toEngineeringString());
		}
		
		String sortNumber;
		boolean firstTime=true;
		List<BigDecimal> tempList = new ArrayList();

		for(int i = 0 ; i < sortNumberList.size() ; i++){
			sortNumber = sortNumberList.get(i).toEngineeringString();
			if(!find(stringNumberList, sortNumber)){
				if(firstTime){
					firstTime = false;
					for(Object number : stringNumberList) {
						tempList.add(new BigDecimal(number.toString())); 
					}			
				}				
				for(int j = 0 ; j < tempList.size() ; j++){
					if(tempList.get(j).toEngineeringString().equals(sortNumber)){
						result.set(i, stringNumberList.get(j));
					}
				}
			}
		}	
		return result;
	}
	
	public static List mergeList(List list1,List list2){
		if(list1==null)return null;
		if(list2==null)return null;
		
		List mergeList = new ArrayList();
		mergeList.addAll(list1);
		String value2;
		boolean equal;
		for(int k =0;k<list2.size();k++){
			value2 = list2.get(k).toString();
			equal = false;
			for(int l=0;l<list1.size();l++){
				if(list1.get(l).toString().trim().equals(value2.trim())){
					equal = true;
					break;
				}
			}
			if(!equal){
				mergeList.add(value2);
			}
		}
		return mergeList;
	}
	
	public static List<Map<String, Object>> mergeByKey(List<Map<String, Object>> list1, List<Map<String, Object>> list2, String key){
		if(list1==null)return null;
		if(list2==null)return null;

		List<Map<String, Object>> mergeList = new ArrayList(list1);
		
		String value1, value2;
		boolean equal;
		for(int i =0;i<list2.size();i++){
			equal = false;
			
			value2 = list2.get(i).get(key).toString();
			for(int j=0;j<list1.size();j++){
				value1 = list1.get(j).get(key).toString();
				if(value1.equals(value2)){
					equal = true;
					break;
				}
			}
			if(!equal){
				mergeList.add(list2.get(i));
			}
		}
		return mergeList;
	}
	
	public static ArrayList<Map<String,String>> changeKeyListMap(ArrayList<Map<String,String>> list,ArrayList<String> oldKeyList,ArrayList<String> newKeyList){
		if(list==null)return null;
		if(oldKeyList==null)return null;
		if(newKeyList==null)return null;
		
		ArrayList<Map<String,String>> result = new ArrayList<Map<String,String>>();
		Map row,map;
		for(int i = 0 ; i < list.size() ; i++){
			row = (Map)list.get(i);
			
			map = new HashMap();
			for(int j = 0 ; j < newKeyList.size() ; j++){
				map.put(newKeyList.get(j), row.get(oldKeyList.get(j)));
			}			
			result.add(map);
		}
		return result;
	}
	
	public static String convertListToHTMLColumn(List<String> list,int numberOfColumn,String tab,String color){
		if(list==null)return null;
		
		String result="";
		int tabIndex = 1;
		for(int i = 0 ; i < list.size() ; i++){
//			result += tab+"<font color='"+color+"'>"+list.get(i)+"</font>";
			result += tab+list.get(i);
			if(tabIndex==numberOfColumn && i!=list.size()-1){
				result += "<br/>";
				tabIndex = 1;
			}else{
				tabIndex++;
			}
		}
		if(color!=null && !"".equals(result)){
			result = "<font color='"+color+"'>"+result+"</font>";
		}
		return result;
	}
	
	public static ArrayList<Map<String,String>> convertListToListMap(ArrayList<ArrayList<String>> valueList,List<String> keyList){
		if(valueList==null)return null;
		if(keyList==null)return null;
		
		ArrayList<Map<String,String>> result = new ArrayList<Map<String,String>>();
		HashMap<String, String> map;
		for(List<String> valueRow : valueList){
			map = new HashMap<String,String>();
			for(int j = 0 ; j < keyList.size() ; j++){
				map.put(keyList.get(j), valueRow.get(j));
			}
			result.add(map);
		}		
		return result;
	}
	
	public static ArrayList convertListMapToList(ArrayList<Map<String, String>> listMap){
		if(listMap==null)return null;
		
		ArrayList<String> list = new ArrayList<String>();		
		for(Map<String, String> map : listMap){
			for(String key : map.keySet()){
				list.add(map.get(key));
			}
		}
		return list;
	}
	
	public static List<String> splitToList(String str,String strSplit){
		if(str==null)return null;
		
		List<String> result = new ArrayList<String>();
		String[] strSpList = str.split(strSplit);
		for(String strSp : strSpList){
			result.add(strSp.trim().replace("\"", ""));
		}
		return result;
	}
	
	public static List<String> normalization(List<String> list){
		List<String> normalize = new ArrayList<String>();
		
		String value,normalizeValue;
		boolean sameRow;
		for(int i = 0 ; i < list.size() ; i++){
			value = list.get(i);
			
			sameRow = false;
			for(int j = 0 ; j < normalize.size() ; j++){
				
				normalizeValue = normalize.get(j);
				if(value.equals(normalizeValue)){
					sameRow = true;
					break;
				}
			}
			
			if(!sameRow){
				normalize.add(value);
			}
		}
		return normalize;
	}
	
	public static ArrayList<ArrayList<String>> normalization(ArrayList<ArrayList<String>> list){
		if(list==null)return null;
		
		ArrayList<ArrayList<String>> normalizeList = new ArrayList<ArrayList<String>>();
		ArrayList<String> row,normalizeRow;
		String value,normalizeValue;
		boolean notSame,sameRow;
		for(int i = 0 ; i < list.size() ; i++){
			row = list.get(i);		
			notSame = true;
			
			for(int j = 0 ; j < normalizeList.size() ; j++){
				normalizeRow = normalizeList.get(j);				
				sameRow = false;
				
				for(int k = 0 ; k < normalizeRow.size() ; k++){
					value = row.get(k);
					normalizeValue = normalizeRow.get(k);
					if(!value.equals(normalizeValue)){
						sameRow = false;
						break;
					}else{
						sameRow = true;
					}
				}
				
				if(sameRow){
					notSame = false;
					break;
				}
			}
			
			if(notSame){
				normalizeList.add(row);
			}			
		}	
		return normalizeList;
	}
	
	
	// Renew
	public List<Map<String, Object>> addKeyByProp(List<Map<String, Object>> newList, List<Map<String, Object>> list, String pathProp) throws Exception {
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(pathProp);		
		List<Map<String, Object>> result = addKeyNames(newList, list, propMap);
		return result;
	}
	
//	public List<Map<String, Object>> addKeyByPropCondition(List<Map<String, Object>> newList,List<Map<String, Object>> list,String pathProp){
//		Map<String, String> propMap = readProperty(pathProp);
//		List<Map<String, Object>> result = addKeyCon(newList,list,propMap);
//		return result;
//	}
	
	public List<Map<String, Object>> convertKeyByProp(List<Map<String, Object>> list, String pathProp) throws Exception {
		Map<String, Object> propMap = new ManagerPropertiesFile().readProperties(pathProp);
		return reKeyNames(list, propMap);
	}
	
	private List<Map<String, Object>> addKeyNames(List<Map<String, Object>> newList, List<Map<String, Object>> list, Map<String, Object> propMap) throws Exception {
		
		String newKeyStr;
		String[] newKeyList;
		Object value;
		Map<String,Object> row, rowNewList;
		
		
		for(int i =0; i < list.size() ;i++){
			row = list.get(i);
			rowNewList = newList.get(i);
			
			for (String key : propMap.keySet()) {	    
			    newKeyStr = ManagerMap.getString(propMap, key);
			    newKeyList = newKeyStr.split(",");
			    value = row.get(key);
			    
			    if(value==null)value="";

			    for(String newKey : newKeyList){
//			    	String[] keyValue = newKey.split("=");
//			    	if(keyValue.length>1){
//			    		String newKey2 = keyValue[0];
//			    		String statement = keyValue[1];
//			    		
//			    		Double valueDou = calculateStatement(statement,row);
//			    		rowNewList.put(newKey2,valueDou.intValue());	
//			    		
//			    	}else{
			    		rowNewList.put(newKey, value);	
//			    	}			    					    
			    }	
			}		
		}
		return newList;
	}
	
//	private List<Map<String, Object>> addKeyCon(List<Map<String, Object>> newList,List<Map<String, Object>> list,Map<String, String> propMap){	
//		if(newList==null)return null;
//		if(list==null)return newList;
//		if(propMap==null)return newList;
//		
//		Object value;
//		String newKey;
//		Map<String,Object> row,rowNewList;
//		for(int i =0; i < list.size() ;i++){
//			row = list.get(i);
//			rowNewList = newList.get(i);
//			
//			for ( String key : propMap.keySet() ) {
//		    
//				    newKey = propMap.get(key);			    	
//				    value = row.get(key);
//
//				    if(value!=null && value !=""){
//				    	rowNewList.put(newKey, value);
//				    	break;
//				    }
//			}			
//		}		
//		return newList;
//	}
	
	private List<Map<String, Object>> reKeyNames(List<Map<String, Object>> list, Map<String, Object> propMap) throws Exception {	
		if(list==null)return null;
		if(propMap==null)return list;
		
		String newKey, value;
		Map<String, Object> map;
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		
		for(Map<String, Object> row : list){
			
			map = new LinkedHashMap<String, Object>();
			
			for(String key : propMap.keySet()){   

				    newKey = ManagerMap.getString(propMap, key);
				    value = ManagerMap.getString(row, key);

				    if(value==null){
				    	map.put(newKey, "");
				    }else{
				    	map.put(newKey, value);
				    }					    	
			}
			result.add(map);			
		}		
		return result;
	}
	
	
	/* 04/01/2562 add by Akanit */
	public static List<String> convertObjectToListString(Object dataListObj){
		List<String> dataList = null;
		try{
			dataList = (List<String>)dataListObj;
		}catch(Exception e){}
		return dataList;
	}
	public static List<Map<String, Object>> convertObjectToListMap(Object dataListObj){
		List<Map<String, Object>> dataList = null;
		try{
			dataList = (List<Map<String, Object>>)dataListObj;
		}catch(Exception e){}
		return dataList;
	}
	
	
//	private Double calculateStatement(String statement,Map<String,Object> row){				
//		return eval(statement,row);
////		String[] key2Value = statement.split("\\+");
////		if(key2Value.length>1){
////			String key = key2Value[0];
////			int valueInt1 = Integer.parseInt(row.get(key).toString());
////			
////			String valueStr = key2Value[1];
////			int valueInt2 = Integer.parseInt(valueStr);
////			Object value = valueInt1+valueInt2;
////			return value;			
////		}
//	}
//	
//	public Double eval(String excelEquation,Map<String,Object> map){
//		ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
//		try {
//			Double result = (Double) engine.eval(excelEquation,new SimpleBindings(map));
//			return Double.parseDouble(Float.toString(Float.parseFloat(Double.toString(result))));
//		} catch (ScriptException e) {
//			e.printStackTrace();
//			return -0.0;
//		}
//	}
	
}
