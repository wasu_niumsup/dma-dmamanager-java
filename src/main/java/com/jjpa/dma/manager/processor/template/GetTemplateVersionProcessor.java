package com.jjpa.dma.manager.processor.template;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class GetTemplateVersionProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetTemplateVersionProcessor.class);
		
	protected TemplateAccessor templateAccessor;
	
	public GetTemplateVersionProcessor() {
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> GetTemplateVersionProcessor");
		logger.debug("Process ===> GetTemplateVersionProcessor : {}", input.toJsonString());
		
		String templateName = (String)input.getValue("templateName");
		
		DMATemplate template = templateAccessor.findTemplateByName(templateName);

		logger.info("template.getVersionTemplate() : " + template.getVersionTemplate());

    Map<String,Object> output = new LinkedHashMap();
    output.put("templateName", templateName);
    output.put("version",template.getVersionTemplate());

    return output;
    
	}

}
