package com.jjpa.dma.manager.binding.action;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

public class OpenLoading implements ActionBinding{

	private final Logger logger = LoggerFactory.getLogger(OpenLoading.class);
	
	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) {
		logger.info("Binding open loading in Action");
		DocumentContext ctx =  JsonPath.parse(document.getNativeJson().get("data"));
		return ctx;
	}

}
