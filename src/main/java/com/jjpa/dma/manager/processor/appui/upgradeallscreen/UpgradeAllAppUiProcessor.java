package com.jjpa.dma.manager.processor.appui.upgradeallscreen;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;

public class UpgradeAllAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

  private static final Logger logger = LoggerFactory.getLogger(UpgradeAllAppUiProcessor.class);
  private static final String TAG = "UpgradeAllScreenProcessor";

  @Override
  public Object process(HttpServletRequest request, ServiceData input) throws Exception {

    logger.info("Process ===> " + TAG);
    logger.debug("Process ===> {}" , input.toJsonString());

    return upgradeAllAppUi(input);
  }

  private Object upgradeAllAppUi(ServiceData input) throws Exception {

    Map<String,Object> body = input.getData();
    Map<String,Object> req = new LinkedHashMap<String,Object>();
    req.put("user", DataUtils.getStringValue(body, "user",null));

    Map<String,Object> resultContext = callHook("UpgradeAllScreen", req).json();
    if( ManagerMap.getBoolean(resultContext, "resultSuccess") == false) {
      throw new Exception(ManagerMap.getString(resultContext, "resultMessage"));
    }

    return resultContext;
  }

}