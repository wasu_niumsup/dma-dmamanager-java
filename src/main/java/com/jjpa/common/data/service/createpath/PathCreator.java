package com.jjpa.common.data.service.createpath;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jayway.jsonpath.DocumentContext;
import com.jjpa.common.ResponseCode;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.data.service.createpath.PathValue.CreateType;
import com.jjpa.common.exception.ServiceException;
import com.jjpa.common.util.NameUtils;

import net.minidev.json.JSONArray;

public class PathCreator implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final DefaultPredicate defaultPredicate = new DefaultPredicate();

	// config of path
	private String path;
	private List<PathValue> pathList;
	private ServiceData data;
	private Object value;

	public PathCreator(ServiceData data, String jpath) throws ServiceException {
		this(data, jpath, null);
	}

	public PathCreator(ServiceData data, String jpath, Object value) throws ServiceException {
		if (data != null && jpath != null) {
			this.data = data;
			this.path = jpath;
			this.value = value;
		} else {
			ServiceException ex = new ServiceException("Parameter is invalid! data=" + data + " path=" + path);
			ex.setErrorCode(ResponseCode.DATA_INVALID);
			throw ex;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void create(boolean forceReplace) throws ServiceException {

		// Create Path
		// ====================
		createPathList();
		// ====================

		// validate path
		if (pathList == null) {
			ServiceException ex = new ServiceException("This path is null! -> " + path);
			ex.setErrorCode(ResponseCode.DATA_INVALID);
			throw ex;
		} else if (pathList.size() <= 1) {
			ServiceException ex = new ServiceException("This path is read only! -> " + path);
			ex.setErrorCode(ResponseCode.DATA_INVALID);
			throw ex;
		}

		// Creating...
		// *********************************************
		else {

			int maxIndex = pathList.size() - 1;
			for (int i = 1; i <= maxIndex; i++) {
				int index = i;
				int parentIndex = i - 1;
				PathValue cPathValue = pathList.get(index);
				PathValue pPathValue = pathList.get(parentIndex);
				CreateType cType = cPathValue.getType();
				CreateType pType = pPathValue.getType();

				String cPath = getPathByIndex(index);
				String pPath = getPathByIndex(parentIndex);
				if (cType == CreateType.Array) {
					cPath = getPathByIndex(index, false);
				}

				if (pType == CreateType.Array) {
					pPath = getPathByIndex(parentIndex, false);
				}

				Object pValue = data.getValue(pPath);
				Object cValue = data.getValue(cPath);

				// validate type of path
				// ------------------------------------
				if (pValue == null) {
					ServiceException ex = new ServiceException("Parent path is invalid! -> " + pPath);
					ex.setErrorCode(ResponseCode.DATA_INVALID);
					throw ex;
				} else if (pPathValue.isNotSameType(pValue) && forceReplace == false) {
					ServiceException ex = new ServiceException("Type is not match! -> " + pPathValue.getTypeByObject(pValue) + " != " + pType);
					ex.setErrorCode(ResponseCode.DATA_INVALID);
					throw ex;
				}
				// ------------------------------------

				//System.out.println(cPath);
				//System.out.println(cType);

				if (cValue == null) {
					if (cType == CreateType.Map) {
						cValue = cPathValue.createMap();
						if (pType == CreateType.Array) {
							((Map) cValue).put(cPathValue.getName(), null);
						}
					} else if (cType == CreateType.Array) {
						cValue = cPathValue.createArray();
					}
				}

				addToParent(pPathValue, cPathValue, pValue, cValue);
				//System.out.println(data.toJsonString());

			}

			// Add the value to path
			// This the last sub path
			// ++++++++++++++++++++++++++++++++++++++++++++++

			PathValue pPathValue = pathList.get(maxIndex);
			CreateType pType = pPathValue.getType();

			String pPath = null;
			if (pType == CreateType.Array) {
				pPath = getPathByIndex(maxIndex, false);
			} else {
				pPath = getPathByIndex(maxIndex);
			}

			Object pValue = data.getValue(pPath);

			// Not null value
			if (pValue != null) {
				DocumentContext doc = data.getJsonDocument();
				if (pType == CreateType.Map) {
					doc.set(pPath, value, defaultPredicate);
				} else if (pType == CreateType.Array) {
					JSONArray array = (JSONArray) pValue;
					int index = pPathValue.getIndex();
					setValueToIndex(array, index, value);
				}
			}

			// Null value
			else {

				// find its parent
				pPathValue = pathList.get(maxIndex - 1);
				pPath = getPathByIndex(maxIndex - 1);
				pValue = data.getValue(pPath);
				pType = pPathValue.getTypeByObject(pValue);
				PathValue cPathValue = pathList.get(maxIndex);
				CreateType cType = cPathValue.getType();
				if (pType == CreateType.Map) {
					Map pMap = (Map) pValue;
					pMap.put(cPathValue.getName(), value);
				} else if (pType == CreateType.Array) {
					JSONArray array = (JSONArray) pValue;
					if (cType == CreateType.Map) {
						int index = pPathValue.getIndex();
						setValueToIndex(array, index, value);
					} else {
						int index = cPathValue.getIndex();
						setValueToIndex(array, index, value);
					}
				}

			}
			// ++++++++++++++++++++++++++++++++++++++++++++++
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addToParent(PathValue pPathValue, PathValue cPathValue, Object pValue, Object cValue) throws ServiceException {

		CreateType cType = cPathValue.getType();
		CreateType pType = pPathValue.getType();

		if (pType == CreateType.Map) {
			Map pMap = (Map) pValue;
			pMap.put(cPathValue.getName(), cValue);
		} else if (pType == CreateType.Array) {
			JSONArray array = (JSONArray) pValue;
			if (cType == CreateType.Map) {
				int index = pPathValue.getIndex();
				setValueToIndex(array, index, cValue);
			} else {
				int index = cPathValue.getIndex();
				setValueToIndex(array, index, cValue);
			}
		}

	}

	/**
	 * Create path to create field
	 * 
	 * @throws ServiceException
	 */
	private void createPathList() throws ServiceException {
		List<PathValue> list = new ArrayList<>();
		if (path != null && !path.isEmpty()) {
			String[] paths = path.split("\\.");
			if (paths != null && paths.length > 1) {
				for (int i = 0; i < paths.length; i++) {
					String field = paths[i];

					// Array Type
					if (field.contains("[") || field.contains("]")) {
						List<PathValue> subPath = createPathValueArray(field);
						if (subPath != null && !subPath.isEmpty()) {
							list.addAll(subPath);
						} else {
							ServiceException ex = new ServiceException("Field name is invalid! -> " + field);
							ex.setErrorCode(ResponseCode.DATA_INVALID);
							throw ex;
						}
					}

					// Map Type
					else {

						if (ServiceData.ROOT_PATH.equals(field) || NameUtils.isValidName(field)) {
							PathValue pv = new PathValue(CreateType.Map, field);
							list.add(pv);
						} else {
							ServiceException ex = new ServiceException("Field name is invalid! -> " + field);
							ex.setErrorCode(ResponseCode.DATA_INVALID);
							throw ex;
						}
					}
				}

			}
		}
		this.pathList = list;
	}

	public List<PathValue> getPathValues() throws ServiceException {
		createPathList();
		return pathList;
	}

	/**
	 * Create path value type array
	 * 
	 * @param field
	 * @return
	 * @throws ServiceException
	 */
	private List<PathValue> createPathValueArray(String field) throws ServiceException {
		List<PathValue> subPath = new ArrayList<>();
		int[] columnIndexs = getColumnIndexArrayFromString(field);
		int ss = field.indexOf("[");
		field = field.substring(0, ss);

		// It map over array
		PathValue pv = new PathValue(CreateType.Map, field);
		// subPath.add(pv);

		// It is Array
		if (columnIndexs.length > 0) {
			for (int i = 0; i < columnIndexs.length; i++) {
				int index = columnIndexs[i];
				pv = new PathValue(CreateType.Array, "");
				pv.setIndex(index);
				if (i == 0) {
					pv.setName(field);
				}
				subPath.add(pv);
			}
		}

		return subPath;
	}

	/**
	 * Get column index from string path
	 * 
	 * @param str
	 * @return columnIndexs
	 * @throws ServiceException
	 */
	private int[] getColumnIndexArrayFromString(String str) throws ServiceException {

		int start = 1;
		List<Integer> strIndexs = new ArrayList<>();
		int countL = StringUtils.countMatches(str, "[");
		int countR = StringUtils.countMatches(str, "[");

		ServiceException exception = new ServiceException("Array syntax is not corrected! -> " + str);
		exception.setErrorCode(ResponseCode.DATA_INVALID);

		while (true) {

			int ss = str.indexOf("[", start);
			int ee = str.indexOf("]", start);
			start = ee + 1;

			// Array syntax is not corrected
			if (countL != countR || ss > ee || (ss >= 0 && ee < 0) || (ee >= 0 && ss < 0)) {
				throw exception;
			}

			if (ss >= 0 && ee >= 0) {
				String sindex = str.substring(ss + 1, ee);
				try {
					int index = Integer.parseInt(sindex);
					strIndexs.add(index);
				} catch (Exception e) {
					throw exception;
				}
			} else {
				break;
			}

		}
		if (strIndexs.size() > 0) {
			int[] arrIndex = new int[strIndexs.size()];
			for (int i = 0; i < arrIndex.length; i++) {
				int index = strIndexs.get(i);
				arrIndex[i] = index;
			}
			return arrIndex;
		}
		return null;
	}

	public String getPathByIndex(int index) throws ServiceException {
		return getPathByIndex(index, true);
	}

	public String getPathByIndex(int index, boolean showArrayIndex) throws ServiceException {
		return getPathByIndex(pathList, index, showArrayIndex);
	}

	public String getPathByIndex(List<PathValue> pathList, int index, boolean showArrayIndex) throws ServiceException {
		StringBuilder sb = new StringBuilder();
		if (pathList != null && index < pathList.size()) {
			for (int i = 0; i <= index; i++) {
				PathValue pv = pathList.get(i);
				String subPath = pv.getSubPath();

				if (i > 0 && pv.canAddDotToSubPath()) {
					sb.append(".");
				}

				if (i == index) {
					if (pv.getType() == CreateType.Array) {
						if (!showArrayIndex) {
							subPath = pv.getName();
						}
					}
				}

				sb.append(subPath);

			}
		} else {
			ServiceException ex = new ServiceException("Path of index=" + index + " is not valid");
			ex.setErrorCode(ResponseCode.DATA_INVALID);
			throw ex;
		}

		return sb.toString();
	}

	public String getPath() throws ServiceException {
		return getPathByIndex(pathList.size() - 1);
	}

	private void setValueToIndex(JSONArray array, int index, Object value) throws ServiceException {
		if (index >= 0) {

			boolean forceReplace = true;

			// Auto create empty value array
			while (index >= array.size()) {
				array.add(null);
			}

			Object oldValue = array.get(index);
			if (oldValue == null || value == null) {
				array.set(index, value);
			} else if (forceReplace || isSameType(oldValue, value)) {
				array.set(index, value);
			} else {
				ServiceException ex = new ServiceException("Type is not match! -> " + value.getClass().getSimpleName() + " != " + oldValue.getClass().getSimpleName());
				ex.setErrorCode(ResponseCode.DATA_INVALID);
				throw ex;
			}

		} else {
			ServiceException ex = new ServiceException("Array index is invalid! -> index=" + index);
			ex.setErrorCode(ResponseCode.DATA_INVALID);
			throw ex;
		}
	}

	private boolean isSameType(Object o1, Object o2) {
		Class<? extends Object> c1 = o1.getClass();
		Class<? extends Object> c2 = o2.getClass();
		if (c1 == c2) {
			return true;
		} else {
			return false;
		}
	}

	public void remove() throws ServiceException {
		createPathList();

		int maxIndex = pathList.size() - 1;
		String path = getPathByIndex(maxIndex);
		Object value = data.getValue(path);
		if (value != null) {
			PathValue pv = pathList.get(maxIndex);
			CreateType type = pv.getType();
			if (type == CreateType.Array) {
				path = getPathByIndex(maxIndex, false);
				value = data.getValue(path);
				JSONArray array = (JSONArray) value;
				int index = pv.getIndex();
				if (index < array.size()) {
					array.remove(index);
				}
			} else {
				DocumentContext jsonDocument = data.getJsonDocument();
				jsonDocument.delete(path, defaultPredicate);
			}
		}
	}

}
