package com.jjpa.db.mongodb.accessor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.accessor.ScreenInputTypeAccessor;
import com.jjpa.db.mongodb.document.dma.manager.ScreenInputTypeDocument;

public class ScreenInputTypeAccessorImpl implements ScreenInputTypeAccessor{
	
	protected MongoDbAccessor mongoDbAccessor;

	protected ScreenInputTypeAccessorImpl(MongoDbAccessor mongoDbAccessor) {
		this.mongoDbAccessor = mongoDbAccessor;
	}
	
	public Map<String,List<Map<String, Object>>> listScreenInputType(ServiceData input) throws Exception{
		Map<String,List<Map<String, Object>>> inputType = new HashMap<String,List<Map<String, Object>>>();
		List<Map<String, Object>> inputTypeList = new ArrayList<Map<String, Object>>();
		List<ScreenInputTypeDocument> screenInputTypeList = mongoDbAccessor.findAll(ScreenInputTypeDocument.class);
		for(ScreenInputTypeDocument document : screenInputTypeList){
			Map<String, Object> inputMap = new HashMap<String, Object>();
			inputMap.put("valueFrom", document.getName());
			inputMap.put("api", document.getApi());
			inputTypeList.add(inputMap);
		}
		inputType.put("screenInputType", inputTypeList);
		return inputType;
	}

}
