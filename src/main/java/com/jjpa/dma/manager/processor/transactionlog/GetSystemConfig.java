package com.jjpa.dma.manager.processor.transactionlog;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;

import ichat.api.iChatApi;

public class GetSystemConfig extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(GetSystemConfig.class);

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("Start Process ===> GetSystemConfig");
		Map<String, Object> output = new iChatApi().getSystemConfig();
		logger.info("End Process ===> GetSystemConfig");
		return output;
	}

}