package ichat.api;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.http.JsonAccessor;
import com.jjpa.spring.SpringApplicationContext;

public class iChatApi {

	private static final Logger logger = LoggerFactory.getLogger(iChatApi.class);
	
	public JsonAccessor getJsonAccessor() {
		return SpringApplicationContext.getBean("JsonAccessor");
	}
	
	public Map<String, Object> getSystemConfig() throws Exception {
		
		String url = "/rest/ws/dma/getSystemConfig";
		Map<String, Object> request = new LinkedHashMap<>();
		
		logger.info("call iChatApi requestURL="+ url);
		logger.info("request="+ request);
		
		DocumentContext responseCtx = getJsonAccessor().send(url, JsonPath.parse(request));
		Map<String, Object> response = responseCtx.json();	
		
		logger.info("response="+response);
		return response;	
	}
	
}
