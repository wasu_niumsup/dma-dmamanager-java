package com.jjpa.common;

public class SGConstants {
	
	public static final String CODE_SUCCESS 			= "0000I";
	public static final String PROJECT_CODE 			= "200";
	public static final String PROJECT_NAME 			= "SGW";
	
	public class ApisType{
		
		public class Key{
			public static final String APIS_TYPE 			= "apisType";
		}
		
		public class Value{
			public static final String STATIC 				= "Static";
			public static final String DYNAMIC 				= "Dynamic";
		}
	}
	
	public class ExternalApisType{
		
		public class Key{
			public static final String EXTERNAL_APIS_TYPE 	= "externalApisType";
		}
		
		public class Value{
			public static final String STATIC_DROPDOWN 		= "staticdropdown";
			public static final String DYNAMIC_DROPDOWN 	= "dynamicdropdown";
			public static final String WEB_SERVICE_APIS 	= "webserviceapis";
			public static final String WEB_SERVICE_DROPDOWN = "webservicedropdown";
			public static final String JSON_SERVICE_APIS 	= "jsonserviceapis";
			public static final String JSON_SERVICE_DROPDOWN= "jsonservicedropdown";
		}
	}
	
	public class Key{
		
		public static final String UUID 				= "uuid";
		public static final String JSON_DATA 			= "jsonData";
		
		public static final String SERVICE_ID 			= "serviceId";
		public static final String SERVICE_NAME 		= "serviceName";
		public static final String AUTHEN_ID 			= "authenId";
		public static final String AUTHEN_NAME 			= "authenName";
		public static final String WSDL_NAME 			= "wsdlName";
		public static final String WSDL_FILE_NAME 		= "wsdlFileName";
		
		public static final String INPUT_LIST 			= "inputList";
		public static final String OUTPUT_LIST 			= "outputList";
		public static final String INPUT_FIELD_LIST 	= "inputFieldList";
		public class InputFieldList{
			public static final String FIELD_NAME 			= "fieldName";
			public static final String NAME 				= "name";
			public static final String FIELD_TYPE 			= "fieldType";
			public static final String PROMOTE 				= "promote";
			public static final String LABEL_NAME 			= "labelName";
			public static final String DEFAULT_VALUE 		= "defaultValue";
		}
		
		public static final String OUTPUT_FIELD_LIST 	= "outputFieldList";
		public class OutputFieldList{
			public static final String FIELD_NAME 			= "fieldName";
			public static final String NAME 				= "name";
			public static final String FIELD_TYPE 			= "fieldType";
			public static final String PROMOTE 				= "promote";
			public static final String LABEL_NAME 			= "labelName";
			public static final String DEFAULT_VALUE 		= "defaultValue";
		}
		
		public static final String NAME 				= "name";
		public static final String FIELD_TYPE 			= "fieldType";
		public static final String PROMOTE 				= "promote";
		public static final String LABEL_NAME 			= "labelName";
		public static final String DEFAULT_VALUE 		= "defaultValue";
	}
	
	public class Value{
		
		public static final String NONE_AUTHEN 			= "none-authen";
	}
}
