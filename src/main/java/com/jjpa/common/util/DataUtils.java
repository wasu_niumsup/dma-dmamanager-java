package com.jjpa.common.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.data.service.ServicesConfig;

import blueprint.util.StringUtil;

@SuppressWarnings("unchecked")
public class DataUtils {
	
	private static String HOOK_URL = "";
	private static String SG_URL = "";

	public static String getHookURL() throws Exception {

		

		if("".equals(HOOK_URL)){
			ServicesConfig serviceConfig = new ServicesConfig();
			Map<String,Object> hooking_url = (Map<String, Object>) serviceConfig.getConfig().get("hooking_url");
			HOOK_URL = (String) hooking_url.get("connect_backend_url");
			if( "".equals(HOOK_URL) ) {
				HOOK_URL = ConfigurationUtils.getPublishUrl().replace("https", "http") + ":3003";
			}
		}
		

		return HOOK_URL;
	}


	public static String getSGURL() throws Exception {

		

		if("".equals(SG_URL)){
			ServicesConfig serviceConfig = new ServicesConfig();
			Map<String,Object> hooking_url = (Map<String, Object>) serviceConfig.getConfig().get("hooking_url");
			SG_URL = (String) hooking_url.get("connect_sg_url");
		}
		

		return SG_URL;
	}



	public static class Validate {
		public static final String NORMAL_PATTERN = "(^[a-zA-Z0-9_]+)$";

		public static boolean validateRegex(Object value, String regexString) throws Exception {
			boolean result = true;
			Pattern regex =  Pattern.compile(regexString, Pattern.CASE_INSENSITIVE);
			Matcher matcher = regex.matcher(value.toString());
			if (!matcher.find()) {
				result = false;
			}
			return result;
		}
	}

	static final String trueString = "true";
	static final String falseString = "false";

	public static class Version {
		public int major;
		public int minor;
		public int patch;

		public Version(String version) {
			version = version.substring(0, version.lastIndexOf("."));
			String[] splitVersions = version.split("\\.");
			major = Integer.parseInt(splitVersions[0]);
			minor = Integer.parseInt(splitVersions[1]);
			patch = Integer.parseInt(splitVersions[2]);
		}

		public int toInt() {
			return Integer.parseInt(major + "" + minor + "" + patch);
		}

		@Override
		public String toString() {
			return major + "." + minor + "." + patch;
		}

	}

	public static boolean hasKey(DocumentContext ctx, String path) {
		boolean result = false;
		try {

			ctx.read(path);
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	public static String getStringValue(Object data) {
		return getStringValue(data, "");
	}

	public static String getStringValue(Object data, String defaultValue) {
		try {
			if (data == null) {
				return defaultValue;
			} else if (data instanceof Integer) {
				return ((Integer) data).toString();
			} else if (data instanceof Double) {
				return ((Double) data).toString();
			} else if (data instanceof Long) {
				return ((Long) data).toString();
			} else {
				return String.valueOf(data);
			}
		} catch (Exception ex) {
			// do nothing
		}
		return defaultValue;
	}

	public static String getStringValue(Map<String, Object> data, String key) {
		if (data == null) {
			return "";
		}

		return getStringValue(data.get(key));
	}

	public static String getStringValue(Map<String, Object> data, String key, String defaultValue) {
		if (data == null) {
			return defaultValue;
		}

		return getStringValue(data.get(key), defaultValue);
	}

	public static boolean getBooleanValue(Map<String, Object> data, String key, boolean defaultValue) {
		if (data == null) {
			return defaultValue;
		}

		return getBooleanValue(data.get(key), defaultValue);
	}

	public static Boolean getBooleanValue(Object data) {
		return getBooleanValue(data, false);
	}

	public static Boolean getBooleanValue(Object data, boolean defaultValue) {
		if (data instanceof String) {
			if (trueString.equalsIgnoreCase((String) data)) {
				return true;
			} else {
				return false;
			}
		} else if (data instanceof Boolean) {
			return (Boolean) data;
		} else if (data instanceof Integer) {
			if ((Integer) data == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			return defaultValue;
		}
	}

	public static String getMongoMatchRegex(String str) {
		return "^" + convertMongoRegEx(str) + "$";
	}

	private static final String escapeChar = ".\\+*?[^]$(){}=!<>|:-";

	public static String convertMongoRegEx(String str) {
		if(str==null){
			return "";
		}
		
		char[] index = str.toCharArray();

		for (int i = index.length - 1; i >= 0; i--) {
			if (escapeChar.indexOf(index[i]) >= 0) {
				str = str.substring(0, i) + "\\" + str.substring(i);
			}
		}

		return str;
	}

	public static Map<String, Object> getMapFromObject(Map<String, Object> data, String key) throws Exception {

		if (data.get(key) == null) {
			return null;
		} else if (data.get(key) instanceof Map) {
			return (Map<String, Object>) data.get(key);
		} else if (data.get(key) instanceof String) {
			if (StringUtil.isBlank((String) data.get(key))) {
				return new HashMap<String, Object>();
			} else {
				return (Map<String, Object>) JsonPath.parse(DataUtils.getStringValue(data.get(key))).json();
			}
		} else {
			throw new Exception("Unexpected data type.");
		}
	}



	public static byte[] compress(String str) throws Exception {
		/*  if (str == null || str.length() == 0) {
			 return str;
		 } */
		 System.out.println("String length : " + str.length());
		 ByteArrayOutputStream obj=new ByteArrayOutputStream();
		 GZIPOutputStream gzip = new GZIPOutputStream(obj);
		 gzip.write(str.getBytes("UTF-8"));
		 gzip.close();
		 String outStr = obj.toString("UTF-8");
		 System.out.println("Output String length : " + outStr.length());
		 return obj.toByteArray();
	  }
 
	   public static String decompress(byte[] bytes) throws Exception {
		/*  if (str == null || str.length() == 0) {
			 return str;
		 } */
		/*  System.out.println("Input String length : " + str.length()); */
		 /* GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str.getBytes("UTF-8"))); */
		 GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(bytes));
		 BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
		 String outStr = "";
		 String line;
		 while ((line=bf.readLine())!=null) {
		   outStr += line;
		 }
		 System.out.println("Output String lenght : " + outStr.length());
		 return outStr;
	  }

}
