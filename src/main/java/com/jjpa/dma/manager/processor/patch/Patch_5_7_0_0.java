package com.jjpa.dma.manager.processor.patch;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jjpa.common.PatchConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardGroupsAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardGroupsDocument;
import com.jjpa.dma.manager.processor.binding.BindingDomainProcessor;
import com.jjpa.dma.manager.processor.domain.DMADomainProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.PatchProcessor;
import com.jjpa.spring.SpringApplicationContext;

/* 
 * 29/05/2562 add by Akanit : DMA 5.4 : change domain to {{$get._dma_param_internal_domain}}. :  
 * */
public class Patch_5_7_0_0 extends CommonProcessor<ServiceData> implements PatchProcessor {

	private static Logger logger = Logger.getLogger(Patch_5_7_0_0.class);
	
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardGroupsAccessor storyBoardGroupsAccessor;
	
	private String user = "PatchSystem";
	
	public Patch_5_7_0_0() {
        this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
        this.storyBoardGroupsAccessor = SpringApplicationContext.getBean("StoryBoardGroupsAccessor");
    }
	
	@Override
	public Object process(Map<String, Object> input) throws Exception {
		
		/* can edit (patch setting) */
		final String patchVers = "5.7.0.0";
		final String patchDesc = "DMA 5.4 : change domain to {{$get._dma_param_internal_domain}}.";
		
		Map<String, Object> output = new LinkedHashMap<>();
		
		logger.info("start Patch "+ patchVers);
		
		upgradeDataScreen();
		upgradeDataStoryBoardGroups();
		
		
		output.put(PatchConstants.PATCH_VERSION, patchVers);
		output.put(PatchConstants.PATCH_DESCRIPTION, patchDesc);
		output.put(PatchConstants.PATCH_LAST_UPDATE, new Date().getTime());
		
		logger.info("output="+ output);
		logger.info("end Patch "+ patchVers);
		return output;
	}
	
	private void upgradeDataScreen() throws Exception {
		
		List<AppUiDocument> documentList = appUiAccessor.findAll();
		
		if(documentList != null && documentList.size() != 0){
			
			DMADomainProcessor dmadp = new DMADomainProcessor();
			
			String urlScreenShot;
			for(AppUiDocument document : documentList){
				
				urlScreenShot = document.getUrlScreenShot();
				
				if( dmadp.isUrlWithDomain(urlScreenShot) && !dmadp.isUrlAutoGenImage(urlScreenShot) ){
//					logger.info("upgradeDataScreen urlScreenShot screenName="+ document.getUiName());
					
					document.setUrlScreenShot(dmadp.bindingDMADomain(urlScreenShot));
					appUiAccessor.updateUi(document, user);
				}
				
			}
		}
		
	}

	private void upgradeDataStoryBoardGroups() throws Exception {
		
		List<StoryBoardGroupsDocument> documentList = storyBoardGroupsAccessor.findAll();
		
		if(documentList != null && documentList.size() != 0){
			
			Map<String, Object> data;
			for(StoryBoardGroupsDocument document : documentList){
				
				data = document.getData();
				
				new BindingDomainProcessor().bindDMADomainToImageUrlInGroup(data);
				
				document.setData(data);
				
				storyBoardGroupsAccessor.save(document, user);
			}
			
		}
	}
	
	

}
