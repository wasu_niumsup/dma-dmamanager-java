package com.jjpa.dma.manager.binding.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.dma.manager.processor.binding.GenerateInitialScreenActionProcessor;
import com.jjpa.dma.manager.processor.storyboard.ListScreenNativeInputTypeProcessor;

import blueprint.util.StringUtil;

public class NetworkRequest implements ActionBinding {

	private final Logger logger = LoggerFactory.getLogger(NetworkRequest.class);

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> storyBoard, Map<String, Object> eventMap) throws Exception{
		logger.info("Binding network request in Action");
		DocumentContext ctx =  JsonPath.parse(document.getNativeJson().get("data"));
		
		
		String apiUrl = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/send";
		String loginUrl = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/login";
		
		String actionType = DataUtils.getStringValue(eventMap.get(DmaConstants.StoryBoard.Key.TYPE));

		String actionName = (String)eventMap.get(DmaConstants.StoryBoard.Key.ACTION_NAME);
		String source = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
		String apiName = DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.API_NAME, null);
		
		List<Map<String, Object>> bindingList = document.getBinding();
		for(Map<String, Object> binding : bindingList){
			
			String name = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.NAME));
			String path = DataUtils.getStringValue(binding.get(DmaConstants.ActionChain.Key.PATH));
			
			String url = "";
			
			if("url".equals(name)){
				logger.info("name=url");
				
				if(actionType.startsWith("login")){
					url = loginUrl;
				}else{
					url = apiUrl;
				}
				ctx.set(path, url);
				
			}else if("data".equals(name)){
				logger.info("name=data");
				
				



				ctx.put(path, DmaManagerConstant.DMA_ENTITY, JasonConfigurationUtil.getEntity());
				ctx.put(path, DmaManagerConstant.DMA_STORYBOARD, JasonConfigurationUtil.getStoryBoardName());
				ctx.put(path, DmaManagerConstant.DMA_ACTION, actionName);
				ctx.put(path, DmaManagerConstant.DMA_SOURCE, source);
//				ctx.put(path, DmaManagerConstant.DMA_TYPE, "api");

				/**
				 * Tanawat W.
				 * [2018-05-07] Change transaction message.
				 */
				String message = "";
				message = "Call : " + apiName;
				ctx.put(path, DmaManagerConstant.MESSAGE, message);

				/**
				* Tanawat W.
				* [2018-03-20] Add feature initial screen
				* 
				*/
				if( GenerateInitialScreenActionProcessor.Key.actionName.equalsIgnoreCase(actionName) ){
					logger.info("add param");
					
					ctx.put(path, DmaManagerConstant.DMA_INITIAL_SCREEN, apiName  );
					ctx.put(path, DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.INITIAL_SCREEN);
				}else{
					ctx.put(path, DmaManagerConstant.MESSAGE_TYPE, DmaManagerConstant.MESSAGE_TYPE_VALUE.CALL_API);
				}


				/**
				 * Tanawat W.
				 * [2018-05-09] in case action type login then add username
				 * 
				 */
				if( DataUtils.getStringValue(eventMap, DmaConstants.StoryBoard.Key.TYPE, "normal").toLowerCase().contains("login")){
					ctx.put(path, DmaManagerConstant.USERNAME,  JasonNetteUtils.generateNativeValue(DmaManagerConstant.USERNAME));
				}



				/**
				 * Tanawat W.
				 * [2018-03-02] Clean code in requestData of $network.request ;
				 * 
				 */

				//TODO wait for delete  _dma_data
				Map<String,Object> networkOptions = ctx.read(path);
				//networkOptions.put("_dma_data", "");
				networkOptions.put("_dma_split_data",  new LinkedHashMap<>())  ;

				String bindingPath = path + "._dma_split_data";
				SetGlobalToApi g = new SetGlobalToApi(new SetGlobalToApi.ISetData(){
				
					@Override
					public Boolean skipCondition(String parrent_serviceName, String child_serviceName) {
						Boolean result = false;

						Boolean condtion = !StringUtil.isBlank(child_serviceName)
								&& parrent_serviceName.trim().equalsIgnoreCase(child_serviceName.trim());
						if (!condtion) {
							result = true;
						}
						logger.info(" [Private] isSkip : " + result + " , parrent_serviceName : " + parrent_serviceName
								+ " ,child_serviceName : " + child_serviceName);
						return result;
					}
				
					@Override
					public DocumentContext setCtx(DMAActionChainDocument document) {
						return ctx;
					}
				
					@Override
					public String getBindingPath(DMAActionChainDocument document) {
						return bindingPath;
					}
				});
				@SuppressWarnings("unused")
				DocumentContext dd = g.bind(null, null, storyBoard, eventMap);
				

				/**
				 * Tanawat W.
				 * [2018-07-10] improve performance send data
				 */
				boolean activateImprove = true;
				 logger.info("Activate improve performance send data = " + activateImprove);
				if(activateImprove){
					setData(storyBoard, networkOptions);



				}
				
				

				
			}else {
				logger.info("name=other, then do nothing.");
				// do nothing.
			}
		}
		
		return ctx;
	}

	public void setData(Map<String, Object> storyBoard, Map<String, Object> networkOptions) throws Exception {
		List<Map<String, Object>> nativeList = new ListScreenNativeInputTypeProcessor().extractNativeVariable(storyBoard);
		Map<String,Object> natiaveData = new LinkedHashMap<>();
		for (Map<String,Object> item : nativeList) {
			String globalName = DataUtils.getStringValue(item, DmaConstants.StoryBoard.Key.GLOBAL_NAME,null );
			if(globalName == null){
				logger.warn("imposible case");
			}else{
				natiaveData.put(globalName, JasonNetteUtils.generateNativeValue(globalName));
			}
		}
		
		// add base value
		natiaveData.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DOMAIN), JasonNetteUtils.generateNativeValue (JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DOMAIN)) );
		natiaveData.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.ONLY_DOMAIN), JasonNetteUtils.generateNativeValue (JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.ONLY_DOMAIN)) );
		natiaveData.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.APPLICATION), JasonNetteUtils.generateNativeValue (JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.APPLICATION)) );
		natiaveData.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DMAMANAGER_VERSION), JasonNetteUtils.generateNativeValue (JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DMAMANAGER_VERSION)) );
		natiaveData.put(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.PUBLISH_VERSION), JasonNetteUtils.generateNativeValue (JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.PUBLISH_VERSION)) );

		
		networkOptions.put(DmaManagerConstant.DMA_DATA, natiaveData  );
		logger.info("using improve performance send data");
	}

}
