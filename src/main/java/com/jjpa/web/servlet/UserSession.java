package com.jjpa.web.servlet;

import java.io.Serializable;

import com.jjpa.common.data.service.UserModel;

public class UserSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserModel user;

	@SuppressWarnings("unchecked")
	public <T> T getUser() {
		return (T) user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

}
