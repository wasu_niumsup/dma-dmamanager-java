package com.jjpa.dma.manager.processor.appui.preview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants.AppUI;
import com.jjpa.common.DmaManagerConstant;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.ConfigurationUtils;
import com.jjpa.common.util.DataUtils;
import com.jjpa.common.util.JasonNetteUtils;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.processor.binding.BindingBaseConfiguration;
import com.jjpa.dma.manager.processor.binding.BindingComponentProcessor;
import com.jjpa.dma.manager.processor.binding.BindingDynamicListActionProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;

/**
 * DirectPreviewProcessor
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class DirectPreviewProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private static final Logger logger = LoggerFactory.getLogger(DirectPreviewProcessor.class);
    protected TemplateAccessor templateAccessor;

    public DirectPreviewProcessor() {
        this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
    }

    @Override
    public Object process(HttpServletRequest request, ServiceData input) throws Exception {
        try {
            logger.info("0. DirectPreviewProcessor");
            logger.info("0. body = " + input.getData());

            Map<String, Object> body = input.getData();

            prepareData(body);

            AppUiDocument appUiDoc = new AppUiDocument();
            appUiDoc.setStatus(DmaManagerConstant.STATUS_DRAFT);
            appUiDoc.setUiName((String) input.getValue("uiName"));
            appUiDoc.setRemark((String) input.getValue("remark"));
            appUiDoc.setUiConfigData((Map<String, Object>) input.getValue("uiConfigData"));
            appUiDoc.setDynamicApi((String) input.getValue("dynamicApi"));
            appUiDoc.setTemplateType((String) input.getValue("templateType"));
            appUiDoc.setDynamicInputList((List<Map<String, Object>>) input.getValue("dynamicInputList"));
            appUiDoc.setDynamicOutputList((List<Map<String, Object>>) input.getValue("dynamicOutputList"));

            /*
            logger.info("#step 1 : get default jasonTemplate");
            Map<String, Object> dataBinding = getJsonTemplate(appUiDoc);

            logger.info("#step 2 : binding component to jasonTemplate");

            dataBinding = new BindingComponentProcessor().process(dataBinding, appUiDoc);


            appUiDoc.setUiJson(JsonPath.parse(dataBinding).jsonString());
            new BindingDynamicListActionProcessor().process(appUiDoc);
            */
            appUiDoc = reBindingJson(appUiDoc);

            DocumentContext resultJson = JsonPath.parse(appUiDoc.getUiJson());

            /**
             * 
             * Add sampling data
             */
            addSamplingData(body, resultJson);

            /**
             * Clean all action
             */
            Map<String, Object> actions = resultJson.read("$.$jason.head.actions");
            for (Map.Entry<String, Object> entry : actions.entrySet()) {
                String key = entry.getKey();

                if (key.contains("$load")) {
                    logger.info("Prepare clean all action after $render :" + key);
                    DocumentContext load = JsonPath.parse((Map) actions.get(key));
                    List<Map<String, Object>> allType = load.read("$..[?(@.type)]");

                    if (allType != null && allType.size() > 0) {
                        logger.info("Do nothing : allType.size() " + allType.size());

                        for (int i = 0; i < allType.size(); i++) {
                            Map<String, Object> item = allType.get(i);
                            String type = DataUtils.getStringValue(item, "type", "");
                            if ("$render".equalsIgnoreCase(type)) {
                                logger.info("Found $render : " + item);
                                if (item.containsKey("success")) {
                                    item.put("success", new LinkedHashMap<>());
                                    logger.info("Clear : action " + key);
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    logger.info("Clear : action " + key);
                    actions.put(key, new LinkedHashMap<>());
                }
            }



            
            Map<String,Object> tempJson = resultJson.read("$");
            tempJson = new BindingBaseConfiguration().process(tempJson,"","","");
            String uiJson =  JsonPath.parse(tempJson).jsonString();  //resultJson.jsonString();

            logger.info("uiJson (after) = " + uiJson);

            logger.info("#step 3 : write file");
            Random random = new Random();
            String tempFolderPath = ConfigurationUtils.getTempPreview();

            File tempPreviewFloder = new File(tempFolderPath);
            if (!tempPreviewFloder.exists()) {
                logger.info("create new dir ={}", tempFolderPath);
                tempPreviewFloder.mkdirs();
            }

            String id = String.format("%04d", random.nextInt(100));

            if (body.containsKey("previewNumber")) {
                id = DataUtils.getStringValue(body, "previewNumber", "0000");
            }

            String fullPath = tempFolderPath + id + ".dma";

            logger.info("#step 4 fullPath = " + fullPath);
            writeFile(fullPath, uiJson);
            logger.info("Exist file : " + new File(fullPath).exists());

            Map<String, Object> output = new LinkedHashMap<>();
            output.put("previewNumber", id);

            logger.info("Output : " + output);



            /**
             * Tanawat W.
             * [2019-01-17] DMA 5.0
             * 
             */
            Map<String,Object> hookingOutput = new LinkedHashMap<>(input.getData());
            hookingOutput.put("previewNumber", output.get("previewNumber"));
            callHook("/PreviewScreen", hookingOutput);

            return output;

        } catch (Exception e) {
            logger.error("#Error {}", e);
            throw new Exception("This template is not available to preview.");
        }
    }


    public AppUiDocument reBindingJson(AppUiDocument appUiDoc) throws Exception{


        logger.info("#step 1 : get default jasonTemplate");
        Map<String, Object> dataBinding = getJsonTemplate(appUiDoc);

        logger.info("#step 2 : binding component to jasonTemplate");

        dataBinding = new BindingComponentProcessor().process(dataBinding, appUiDoc);


        appUiDoc.setUiJson(JsonPath.parse(dataBinding).jsonString());
        new BindingDynamicListActionProcessor().process(appUiDoc);

        return appUiDoc;
    }

    public static void writeFile(String path, String content) {
        FileOutputStream fop = null;
        File file;
        //content = "This is the text content";

        try {

            file = new File(path);
            fop = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes("UTF-8");

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareData(Map<String,Object> body){
        // List<Map<String, Object>> output =  (List<Map<String, Object>>) body.get("dynamicOutputList");
        // if(output != null){
        //     for (int i = 0; i < output.size(); i++) {
        //         Map<String,Object> item =  output.get(i);
        //         item.put("path", DataUtils.getStringValue(item, "path","").replace("value[0]", "")  );
        //     }
        // }
    }



    private void addSamplingData(Map<String, Object> body, DocumentContext resultJson) {
        String templateType = DataUtils.getStringValue(body, "templateType", "");
        logger.info("templateType : " + templateType);
        if (AppUI.Value.DYNAMIC_LIST.equalsIgnoreCase(templateType)) {
            List<Map<String, Object>> items = resultJson.read("$.$jason.head.templates.body.sections..items");

            String rawKey = "";
            for (Object obj : items) {
                
                Map<String, Object> i = obj instanceof Map ?  (Map<String, Object> )obj : null;

                if(i == null){
                    logger.warn("skip : " + obj);
                    continue;
                }

                for (Map.Entry<String, Object> entry : i.entrySet()) {
                    String key = entry.getKey();
                    if(key.contains("#each")){
                        rawKey = key;
                        break;
                    }

                }
            }
            String listKey = JasonNetteUtils.cleanScope(rawKey).replace("#each", "").trim().replace("$get.", "");

            logger.info("listkey : " + listKey);
            Map<String,Object>  firstSet = resultJson.read("$.$jason.head.actions.$load");
            if( !StringUtil.isBlank(listKey) &&
                "$set".equalsIgnoreCase( DataUtils.getStringValue(firstSet, "type") ) ){
                Map<String,Object>  options = (Map<String, Object>) firstSet.get("options");
                
                logger.info("gen mockup");
                List<Map<String,Object>> mockupData = createMockupData( (List<Map<String, Object>>) body.get("dynamicOutputList"));
                
                @SuppressWarnings("unused")
				Map<String,Object> valueResult = new LinkedHashMap<>();
                
                options.put(listKey,mockupData );


                Map<String,Object> render = new LinkedHashMap<>();
                render.put("type", "$render");
                firstSet.put("success",render );

            }

            
        }
    }

    private List<Map<String,Object>> createMockupData(List<Map<String,Object>>  dynamicOutputList){
        List<Map<String,Object>> result = new ArrayList<>();

        
        if(dynamicOutputList != null){
            Map<String,Object> masterRow = new LinkedHashMap<>();
            
            for (int i = 0; i < dynamicOutputList.size(); i++) {
                Map<String,Object> item =  dynamicOutputList.get(i);
                String name = DataUtils.getStringValue(item,"name");
                int random = new Random().nextInt(100);
                masterRow.put(name, name+   i +""+random  );
            }

            logger.info("prepare masterRow : " + masterRow);

            for (int i = 0; i < 20; i++) {
                result.add(new LinkedHashMap<>(masterRow));
            }

        }

        return result;
    }

    /* 04/01/2561 add by Akanit */
    private Map<String, Object> getJsonTemplate(AppUiDocument appUiDoc) throws Exception {

        /* get template config */
        String templateName = (String) appUiDoc.getUiConfigData().get("template");
        DMATemplate template = templateAccessor.findTemplateByName(templateName);

        /* set dynamicDataBinding from template */
        appUiDoc.setDynamicDataBinding(template.getDynamicDataBinding());

        /* get templateJson */
        DocumentContext ctx = JsonPath.parse(template.getNativeJson().get("data"));

        /* create dataBinding from templateJson (old system : dataBinding is templateJson) */
        Map<String, Object> dataBinding = new LinkedHashMap<>((Map<String, Object>) ctx.json());

        return dataBinding;
    }

}