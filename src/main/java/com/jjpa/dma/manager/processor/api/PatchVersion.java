package com.jjpa.dma.manager.processor.api;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.jjpa.common.PatchConstants;
import com.jjpa.common.PropConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.MongoDbAccessor;
import com.jjpa.db.mongodb.document.dma.manager.SystemConfigDocument;
import com.jjpa.dma.manager.processor.info.InfoProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.PatchProcessor;
import com.jjpa.processor.ServiceProcessor;

import servicegateway.utils.ManagerMap;

@SuppressWarnings("unchecked")
/* 04/01/2561 add by Akanit */
public class PatchVersion extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
	
	private static Logger logger = Logger.getLogger(PatchVersion.class);
	
	private MongoDbAccessor mongo = getMongoDbAccessor();
	
	private String package_patch = "com.jjpa.dma.manager.processor.patch";
	private String prefix_patchName = "Patch_";
	private String suffix_patchName = ".java";
	private String separator_patchName = "_";
	private String separator_version = ".";
	
	private String defaultVersion = "0.0.0.0";

	private ServletContext servletContext;
	
	
	
	/* 
	 * Public Method
	 * */
	public Object process(HttpServletRequest request, Map<String, Object> input) throws Exception {
		logger.info("Start process : patch version.");

		String startVersion = ManagerMap.getString(input, PatchConstants.Input.START_VERSION);
		
		this.servletContext = request.getServletContext();
		patchVersion(startVersion, input);
		
		logger.info("End process : patch version.");
		return new LinkedHashMap<>();
	}
	
	public Object process(ServletContext servletContext) throws Exception {
		return process(servletContext, null);
	}

	public Object process(ServletContext servletContext, Map<String, Object> input) throws Exception {
		logger.info("Start process : patch version.");

		String startVersion = ManagerMap.getString(input, PatchConstants.Input.START_VERSION);
		
		this.servletContext = servletContext;
		patchVersion(startVersion, input);
		
		logger.info("End process : patch version.");
		return new LinkedHashMap<>();
	}

	
	/**
	 * Compares two version strings. (Code from internet)
	 * 
	 * Use this instead of String.compareTo() for a non-lexicographical 
	 * comparison that works for version strings. e.g. "1.10".compareTo("1.6").
	 * 
	 * @note It does not work if "1.10" is supposed to be equal to "1.10.0".
	 * 
	 * @param str1 a string of ordinal numbers separated by decimal points. 
	 * @param str2 a string of ordinal numbers separated by decimal points.
	 * @return The result is a negative integer if str1 is _numerically_ less than str2. 
	 *         The result is a positive integer if str1 is _numerically_ greater than str2. 
	 *         The result is zero if the strings are _numerically_ equal.
	 */
	public static int compareVersion(String str1, String str2) {
		String[] vals1 = str1.split("\\.");
	    String[] vals2 = str2.split("\\.");
	    int i = 0;
	    // set index to first non-equal ordinal or length of shortest version string
	    while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
	      i++;
	    }
	    // compare first non-equal ordinal number
	    if (i < vals1.length && i < vals2.length) {
	        int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
	        return Integer.signum(diff);
	    }
	    // the strings are equal or one string is a substring of the other
	    // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
	    return Integer.signum(vals1.length - vals2.length);
	}
	
	public static void sortVersion(List<String> strList) {
		
		Collections.sort(strList, new Comparator<String>() {
	        public int compare(final String o1, final String o2){        
	        	try{
	        		return compareVersion(o1, o2);
		        	
	        	}catch(Exception e){
	        		return 0;
	        	}
	        }
	    });
	}
	
	
	
	
	
	/* 
	 * Private Method
	 * */
	private void patchVersion(String startVersion, Map<String, Object> input) throws Exception {
		
		
		/* get system config from db mongo. */
		SystemConfigDocument sysConfigDoc = null;
		try{
			sysConfigDoc = mongo.findAll(SystemConfigDocument.class).get(0);
		}catch(Exception e){}
		
		
		
		/* 
		 * get old version 
		 * */
		String oldVersion = defaultVersion;
		if(startVersion!=null){
			logger.info("startVersion = "+ startVersion);
			
			oldVersion = startVersion;
		}else{
			oldVersion = getDatabaseVersion(sysConfigDoc, defaultVersion);
		}
		
		
		
		/* 
		 * get now version 
		 * 
		 * 	: from this Project(In war) 
		 * */
		String nowVersion = getProjectVersion();
		
		
		
		/* 
		 * patch version from oldVersion to nowVersion 
		 * */
		try{
			patchVersionFromTo(oldVersion, nowVersion, sysConfigDoc, input);
		} catch (Exception e){
			logger.error("!!!patch version failure.", e);
			throw e;
		}
		
	}
	
	private String getDatabaseVersion(SystemConfigDocument sysConfigDoc, String defaultVersion) {
		logger.info("get version from mongoDB.");
		
		String version = defaultVersion;
		if(sysConfigDoc!=null){
			
			String patchVersion = sysConfigDoc.getPatch_version();
			
			if(patchVersion!=null){
				version = patchVersion;
			}else {
				logger.info("'patch_version' in collection 'system_config' not found, then version = default value("+version+").");
			}
			
		}else {
			logger.info("collection 'system_config' not found, then version = default value("+version+").");
		}
		
		logger.info("version = "+ version);
		return version;
	}
	
	private String getProjectVersion() throws Exception {
		logger.info("get version from Project.");
		
		Map<String, Object> info = (Map<String, Object>) new InfoProcessor().process(new ServiceData());
		String version = ManagerMap.getString(info, PropConstants.Service.Info.Engine.Key.VERSION);
			
		logger.info("version = "+ version);
		return version;
	}
	
	
	private void patchVersionFromTo(String oldVersion, String nowVersion, SystemConfigDocument sysConf, Map<String, Object> input) throws Exception {
		logger.info("patch version from "+ oldVersion +" to "+ nowVersion);
		
		List<String> patchVersionList = getPatchVersionList();
		logger.info("patch version List="+ patchVersionList);
		
		String patchName;
		PatchProcessor patchProcessor;
		
		for(String patchVersion : patchVersionList){
		
			if(compareVersion(patchVersion, nowVersion) == 1 ){ // patchVersion > nowVersion
				break;
			}
			
			if(compareVersion(oldVersion, patchVersion) == -1){ // oldVersion < patchVersion
				logger.info("patch version="+ patchVersion);
				
				
				/* generate className */
				patchName = prefix_patchName + patchVersion.replace(separator_version, separator_patchName);
				
				/* get class from className */
				patchProcessor = (PatchProcessor) Class.forName(package_patch + "." + patchName).newInstance();
				
				
				/* patching */
				Map<String, Object> result = (Map<String, Object>) patchProcessor.process(input);
				
				
				/* set patching version, desc, lastUpdate */
				if(sysConf==null){
					sysConf = new SystemConfigDocument(result);
				}else {
					sysConf.fromMap(result);
				}
				
				
				/* update patching version */
				mongo.save(sysConf, "System");
				
			}else {
				logger.info("skip patch version="+ patchVersion);
			}
			
		}
	}
	
	private List<String> getPatchVersionList() {

		/** 
		 * Example
		 * fileNames=[Patch_3_6_0_0.java, Patch_3_7_0_0.java, Patch_3_8_0_0.java]
		 * */
		List<String> fileNames = getResourceFileNames(package_patch);
//		logger.info("fileNames="+ fileNames);
		
		/** 
		 * Example
		 * patchVersionList=[3.6.0.0, 3.7.0.0, 3.8.0.0]
		 * */
		List<String> patchVersionList = getPatchVersionListOfFileNames(fileNames);
//		System.out.println("patchVersionList="+ patchVersionList);
		
		
		/* sort ASC */
		sortVersion(patchVersionList);
		
		return patchVersionList;
	}
	
	private List<String> getResourceFileNames(String path){
	    List<String> filenames = new ArrayList<>();
	    try{
	    		
	    	/* get fileName of folder in src/main/rersources */
//	    	String classPath = Paths.get(".").toAbsolutePath().normalize().toString();
//	 	    String folderPath = classPath + path;
//	    	
//	 	    File folder = new File(folderPath);
//	    	
//	    	for(File file : folder.listFiles()){
//	    		filenames.add(file.getName());
//	    	}
	    	
	    	/* get fileName of package in src/main/java */
//	 	    filenames = getAllClassNameInPackage(path);
	    	
	    	/* get fileName of package in src/main/java on server */
	    	filenames = getAllClassNameInPackageOnServer(servletContext, path);
	    	
	    }catch (Exception e) {
	    	e.printStackTrace();
		}
	    
	    return filenames;
	}
	
	private List<String> getAllClassNameInPackageOnServer(ServletContext servletContext, String packageName) throws Exception {
		List<String> classNameList = new ArrayList<>();

    	String path = "/WEB-INF/classes/" + packageName.replace('.', '/');
    	URL resource = servletContext.getResource(path);
		
    	File directory = new File(resource.getPath());
    	
    	if (!directory.exists()) {
	    	logger.info("directory="+ directory + " dose not exists.");
	    }else {
		    File[] files = directory.listFiles();
		    for (File file : files) {
		    	if (file.getName().endsWith(".class")) {
		            classNameList.add(file.getName().substring(0, file.getName().length() - 6));// -6 because i want to cut .class
		        }
		    }
	    }
    	
	    return classNameList;
	}
	
	private List<String> getPatchVersionListOfFileNames(List<String> fileNames){
		List<String> patchVersionList = new ArrayList<>();
		
		String patchVersion;
		for(String fileName : fileNames){
			
			patchVersion = fileName.replace(prefix_patchName, "").replace(suffix_patchName, "").replace(separator_patchName, separator_version);
			
			patchVersionList.add(patchVersion);
		}
		
		return patchVersionList;
	}
	
}
