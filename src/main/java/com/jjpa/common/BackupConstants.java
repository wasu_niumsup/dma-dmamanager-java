package com.jjpa.common;

public class BackupConstants {

	public static final String FILE_UPLOAD_NAME 			= "filePath";
	
	public class FolderName{
		
		public class DMA{
			public static final String STORY_BOARD 				= "storyBoard";	
			public static final String SCREEN 					= "screen";
			public static final String TEMPLATE 				= "template";
			public static final String ACTION_CHAIN_TEMPLATE 	= "actionChainTemplate";
			public static final String ACTION_CHAIN 			= "actionChain";
			public static final String IMS 						= "ims";
		}
		
		public class SG{
			public static final String EXTERNAL_APIS 			= "ExternalApis";
			public static final String OAUTH 					= "Oauth";
			public static final String WSDL 					= "WSDL";
		}
	}
	
	public class Validator{
		
		public class Key{
			public static final String DETAIL 				= "detail";
			public static final String STATUS 				= "status";
		}
		
		public class Detail{
			public static final String NOT_FOUND 			= "not found";
			public static final String DUPLICATE 			= "duplicate";
			public static final String VERSION_CONFLICT 	= "version conflict";
		}
		
		public class Status{
			public static final boolean TRUE 				= true;
			public static final boolean FALSE 				= false;
		}

	}
	
//	public class FolderName{
//		
//		public class Key{
//			
//			public static final String ID 					= "id";
//			public static final String ENTITY_NAME 			= "entityName";
//			public static final String REMARK 				= "remark";
//			public static final String STORY_NAME 			= "storyName";
//			public static final String ROLE 				= "role";
//			public static final String THEME 				= "theme";
//			public static final String STATUS 				= "status";
//			public static final String VERSION 				= "version";
//			
//			public static final String HOME_PAGE 			= "homePage";
//			public static final String FIRST_PAGE 			= "firstPage";
//			public static final String SCREEN_LIST 			= "screenList";
//		}
//		
//		public class Value{
//			
//		}
//	}
}
