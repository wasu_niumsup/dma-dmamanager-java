package com.jjpa.dma.manager.processor.storyboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.dma.manager.processor.appui.DeleteAppUiProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unchecked")
public class DeleteStoryBoardAndScreenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteStoryBoardAndScreenProcessor.class);
		
	protected StoryBoardAccessor storyBoardAccessor;
	
	public DeleteStoryBoardAndScreenProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> DeleteStoryBoardAndScreenProcessor");
		logger.debug("Process ===> DeleteStoryBoardAndScreenProcessor : {}", input.toJsonString());

		
		Map<String,Object> validate =  (Map<String, Object>) new ValidateDeleteStoryBoard().process(request, input);
		List<Map<String,Object>> screen = (List<Map<String,Object>>) validate.get(ValidateDeleteStoryBoard.Key.resultData);

		/**
		 * Tanawat W.
		 * [2019-01-07] DMA 5.0
		 * 
		 */
		logger.info("0. call DeleteStoryBoardAndScreen to dma new engine");
		callHookSyncDeleteStoryBoardAndScreen(input.getData());
		
		logger.info("1. delete storyboard");
		new DeleteStoryBoardProcessor().process(request, input);
		logger.info("1.1. deleted storyboard");

		
		logger.info("2. delete all screen");
		String user = (String)input.getValue("user");
		List<Map<String, Object>> uiList = new ArrayList<>();

		Map<String,Object> dataScreen = new LinkedHashMap<>();
		dataScreen.put("user", user);
		dataScreen.put("uiList", uiList);

		for (int i = 0; i < screen.size(); i++) {
			Map<String,Object> s = screen.get(i);
			Map<String,Object> row = new LinkedHashMap<>();
			row.put("uiName", DataUtils.getStringValue(s, "uiName", null));
			uiList.add(row);
			
		}
		logger.info("2.1. prepare screen to delete");
		ServiceData deleteScreen = new ServiceData(dataScreen);
		new DeleteAppUiProcessor().process(request, deleteScreen);	
		logger.info("2.2. deleted all screen in storyBoard");

		

		return storyBoardAccessor.listStory(input);
		
	}

}
