package com.jjpa.dma.manager.binding;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;

public class UiJsonBinding {

	private static final Logger logger = LoggerFactory.getLogger(UiJsonBinding.class);
	
	public static void bindingAppUi(AppUiDocument appUiDoc, String path, Object value) throws Exception {
		
		DocumentContext ctx = JsonPath.parse(appUiDoc.getUiJson());
		
		logger.info("binding 'uiJson' in db(app_ui_config)");
		logger.info("path={}", path);
		logger.info("value={}", value);
		ctx.set(path, value);
		
		appUiDoc.setUiJson(ctx.jsonString());
	}
	
	public static void bindingAppUi(AppUiDocument appUiDoc, String path, String key, Object value) throws Exception {
		
		DocumentContext ctx = JsonPath.parse(appUiDoc.getUiJson());
		
		logger.info("binding 'uiJson' in db(app_ui_config)");
		logger.info("path={}", path);
		logger.info("key={}", key);
		logger.info("value={}", value);
		ctx.put(path, key, value);
		
		appUiDoc.setUiJson(ctx.jsonString());
	}
	
	public static void bindingStoryBoard(Map<String, Object> storyBoard, String path, Object value) throws Exception {
		
		String uiJson = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.UI_JSON, null);
		
		DocumentContext ctx = JsonPath.parse(uiJson);
		
		logger.info("binding 'uiJson' in db(story_board_config)");
		logger.info("path={}", path);
		logger.info("value={}", value);
		ctx.set(path, value);
		
		storyBoard.put(DmaConstants.StoryBoard.Key.UI_JSON, ctx.jsonString());
	}
	
	public static void bindingStoryBoard(Map<String, Object> storyBoard, String path, String key, Object value) throws Exception {
		
		String uiJson = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.UI_JSON, null);
		
		DocumentContext ctx = JsonPath.parse(uiJson);
		
		logger.info("binding 'uiJson' in db(story_board_config)");
		logger.info("path={}", path);
		logger.info("key={}", key);
		logger.info("value={}", value);
		ctx.put(path, key, value);
		
		storyBoard.put(DmaConstants.StoryBoard.Key.UI_JSON, ctx.jsonString());
	}

}
