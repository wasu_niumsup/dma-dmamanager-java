package com.jjpa.dma.manager.processor.entity;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.MongoConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import servicegateway.utils.ManagerMap;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.model.GridPagging;
import com.jjpa.db.mongodb.model.WhereOption;
import com.jjpa.db.mongodb.query.CriteriaUtil;
import org.springframework.data.mongodb.core.query.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * ListApplicationProcessor
 */
public class ListApplicationProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {
    private static final Logger logger = LoggerFactory.getLogger(ListApplicationProcessor.class);

    protected EntityAccessor entityAccessor;

    public ListApplicationProcessor() {
        this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");

    }

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListApplicationProcessor");
		logger.debug("Process ===> ListApplicationProcessor : {}", input.toJsonString());
		String username = ManagerMap.getString(input.getData(), "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}

		
        Query query = new Query();
    
        List<WhereOption> whereOptList = createWhereOption(input.getData());
        CriteriaUtil.addCriteriaWhere(query, whereOptList);
        List<EntityDocument> data  = entityAccessor.listFullEntityByCriteria(query);


        /*

{
    "keyname": "app-free-1",
    "deploymentKey": {
        "android": "e4hygYo-V9t3qSGiAISyIWN9EidveANJ6BVGE",
        "iOS": "iOS---xxxxxx"
    }
}
        */

        List<Map<String,Object>> listKey = new ArrayList<>();
        for (EntityDocument item : data) {
            Map<String,Object> key = new LinkedHashMap<>();
            String keyname = item.getEntityName();
            

            Map<String,Object> deploymentKey = new LinkedHashMap<>();
            String androidDeploymentKey = JsonPath.read(item.getAppKey(), "$.androidConfig.deploymentKey");
            String iOSDeploymentKey = JsonPath.read(item.getAppKey(), "$.iosConfig.deploymentKey");
            deploymentKey.put("android", androidDeploymentKey);
            deploymentKey.put("iOS",iOSDeploymentKey);
            
            key.put("keyname", keyname);
            key.put("deploymentKey", deploymentKey);
            listKey.add(key);
        }

        Map<String,Object> output = new LinkedHashMap<>();
        output.put("allKey", listKey);
        return output;
	}




	private List<WhereOption> createWhereOption(Map<String, Object> input) throws Exception {
		

		String username = ManagerMap.getString(input, "user",null);
		if(username == null){
			throw new Exception("ERROR : Not found your [" + username + "] in system");
		}
				

		
		
		// create whereOptList.
		List<WhereOption> whereOptList = new ArrayList<>();
		WhereOption whereOpt;



		if(!"admin".equals(username)){
			whereOpt = new WhereOption();
			whereOpt.setPath(MongoConstants.Entity.Path.CREATE_BY);
			whereOpt.setValue(username);
			whereOpt.setSensitive(true);
			whereOptList.add(whereOpt);
		}
		
		return whereOptList;
	}
    
}