package com.jjpa.db.mongodb.document.dma.manager;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.jjpa.db.mongodb.document.BaseDocument;

@Document(collection = "storyboard_extra_target_type")
public class StoryBoardExtraTargetTypeDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;

	@Indexed
	private String uiName;
	private List<Map<String, Object>> configParameter;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date lastUpdate;
	
	/**
	 * @return the uiName
	 */
	public String getUiName() {
		return uiName;
	}

	/**
	 * @param uiName the uiName to set
	 */
	public void setUiName(String uiName) {
		this.uiName = uiName;
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public List<Map<String, Object>> getConfigParameter() {
		return configParameter;
	}

	public void setConfigParameter(List<Map<String, Object>> configParameter) {
		this.configParameter = configParameter;
	}
	
}
