package com.jjpa.dma.manager.processor.publish;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.AppConstants;
import com.jjpa.common.BackupConstants;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DateTimeUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.EntityAccessor;
import com.jjpa.db.mongodb.accessor.PublishEntityConfigAccessor;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.db.mongodb.accessor.TempEntityConfigAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.EntityDocument;
import com.jjpa.db.mongodb.document.dma.manager.PublishEntityConfigDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.db.mongodb.document.dma.manager.TempEntityConfigDocument;
import com.jjpa.dma.manager.processor.entity.FindByNameEntityProcessor;
import com.jjpa.dma.manager.processor.entity.SaveEntityProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.dma.manager.processor.storyboard.BackupStoryBoardProcessor;
import com.jjpa.dma.manager.processor.storyboard.DownloadBackupFileProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerMap;

public class PublishProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData> {

	private static final Logger logger = LoggerFactory.getLogger(PublishProcessor.class);

	protected TempEntityConfigAccessor tempEntityConfigAccessor;
	protected PublishEntityConfigAccessor publihsEntityConfigAccessor;
	protected EntityAccessor entityAccessor;
	protected AppUiAccessor appUiAccessor;
	protected StoryBoardAccessor storyBoardAccessor;

	public PublishProcessor() {
		this.tempEntityConfigAccessor = SpringApplicationContext.getBean("TempEntityConfigAccessor");
		this.publihsEntityConfigAccessor = SpringApplicationContext.getBean("PublishEntityConfigAccessor");
		this.entityAccessor = SpringApplicationContext.getBean("EntityAccessor");
		this.appUiAccessor = SpringApplicationContext.getBean("AppUiAccessor");
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}

	

	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		logger.info("do savePublishEntityConfig");

		String user = (String) input.getValue(AppConstants.USER);

		// TODO publish ui to engine

		// String entity = "default";
		// if (!StringUtil.isBlank((String) input.getValue("entityName"))) {
		// entity = (String) input.getValue("entityName");
		// }
		// String user = (String) input.getValue("user");
		// EntityDocument entityDoc = entityAccessor.findEntityByName(entity);
		// if (DmaManagerConstant.STATUS_ACTIVE.equals(entityDoc.getStatus())) {
		// return pushPublish(input, entity, user);
		// } else if(DmaManagerConstant.STATUS_INACTIVE.equals(entityDoc.getStatus())){
		// return clearPublish(input, entity, user);
		// }else{
		// throw new Exception("Invalid status");
		// }

		/**
		 * Tanawat W. [2018-03-13] Add auto save entityApplication before publish
		 * 
		 */
		Map<String, Object> entityData = autoSaveEntityApplication(request, input);

		/* new code */
		Object entity = savePublishEntityConfig(request, input,user);

		
		/**
		 * Tanawat W. [2020-01-28] DMA 6.0
		 */
		BackupStoryBoardProcessor bkProcessor = new BackupStoryBoardProcessor();
		Map<String, Object> bodyOfBkProcessor = new LinkedHashMap<>();
		String storyName = (String) entityData.get(DmaConstants.Entity.Key.STORY_NAME);
		bodyOfBkProcessor.put(DmaConstants.StoryBoard.Key.STORY_NAME, storyName);
		bodyOfBkProcessor.put("publishInfo",entityData);
		bodyOfBkProcessor.put("fromPrivate", true);
		Map<String,Object> resultFromBkProcessor = (Map<String, Object>) bkProcessor
				.process(new ServiceData(bodyOfBkProcessor));
		String backupZipFileName = (String)resultFromBkProcessor.get(BackupConstants.FILE_UPLOAD_NAME);
		DownloadBackupFileProcessor dwp = new DownloadBackupFileProcessor();
		String absPathFileBackupZip = dwp.getAbsolutePathFile(backupZipFileName);
		entityData.put("dmaDirectoryPath", absPathFileBackupZip);
		entityData.put("reference", resultFromBkProcessor.get("reference") );
		
		/**
		 * Tanawat W. [2018-12-18] DMA 5.0
		 * 
		 */
		Map<String, Object> resultContext = callHook("PublishApp", entityData).json();

		if ((boolean) resultContext.get("resultSuccess") == false) {
			throw new Exception((String) resultContext.get("resultMessage"));
		}

		// logger.info("entity={}", JsonPath.parse(entity).jsonString());
		return entity;
	}

	private Map<String, Object> autoSaveEntityApplication(HttpServletRequest request, ServiceData input)
			throws Exception {

		EntityDocument entityDocument = (EntityDocument) new FindByNameEntityProcessor().process(input);

		if (entityDocument.getStoryName() == null) {
			throw new Exception("Please select publish storyboard.");
		}

		// Validate

		logger.info("Start ===> Validate AppInfo");
		Object appInfo = entityDocument.getAppInfo();
		if (appInfo != null) {
		Map<String, Object> appInfoMap =  (Map<String, Object>) appInfo;
		logger.info("Start ===>  appInfoMap"+ appInfoMap.toString());
				validateAppInfo(appInfoMap);
		} else {
			throw new Exception("Please, update configuration in application before publishing.");
		}

		Map<String, Object> body = new LinkedHashMap<>();
		body.put(DmaConstants.Entity.Key.ID, entityDocument.getId());
		body.put(DmaConstants.Entity.Key.STORY_NAME, entityDocument.getStoryName());
		body.put(DmaConstants.Entity.Key.STATUS, entityDocument.getStatus());
		body.put(DmaConstants.Entity.Key.REMARK, entityDocument.getRemark());
		body.put(DmaConstants.Entity.Key.ROLE, entityDocument.getRole());
		body.put(DmaConstants.Entity.Key.THEME, entityDocument.getTheme());

		body.put(DmaConstants.Entity.Key.ENTITY_NAME, input.getValue(DmaConstants.Entity.Key.ENTITY_NAME));
		body.put(DmaConstants.Entity.Key.VERSION, input.getValue(DmaConstants.Entity.Key.VERSION));

		body.put(DmaConstants.Entity.Key.APP_INFO, entityDocument.getAppInfo());
		body.put(DmaConstants.Entity.Key.APP_KEY, entityDocument.getAppKey());
		/**
		 * 21-02-2563 add by Parintorn : for DMA 6.0.0 : publish queue tracking
		 */
		body.put(DmaConstants.Entity.Key.CREATE_BY, entityDocument.getCreateBy());

		/*
		 * 09/05/2562 add by Akanit : by Ter : for DMA.5.3 : add flag 'forceLogout' when
		 * publish
		 */
		body.put(DmaConstants.Entity.Key.FORCE_LOGOUT,
				ManagerMap.getBoolean(input.getData(), DmaConstants.Entity.Key.FORCE_LOGOUT));

		ServiceData tempInput = new ServiceData(body);

		new SaveEntityProcessor().process(request, tempInput);

		return body;

	}

	private Object savePublishEntityConfig(HttpServletRequest request, ServiceData input,String user) throws Exception {

		/* get parameter from UI. */
		String entityName = (String) input.getValue(DmaConstants.Entity.Key.ENTITY_NAME);
		String version = (String) input.getValue(DmaConstants.Entity.Key.VERSION);
		if (StringUtil.isBlank(version)) {
			throw new Exception("No Version");
		}

		/* get temp_entity from DB. */
		TempEntityConfigDocument document_temp = tempEntityConfigAccessor.findTempConfigByName(entityName);
		if (document_temp == null) {
			logger.info("entity name '" + entityName + "' not found.");
			return null;
		}

		/* get publish_entity from DB. */
		PublishEntityConfigDocument document_publish = publihsEntityConfigAccessor.findPublishConfigByName(entityName);
		if (document_publish == null) {
			document_publish = new PublishEntityConfigDocument();
		}

		/* copy temp_entity to publish_entiry. */
		document_publish.setEntityName(document_temp.getEntityName());
		document_publish.setHome(document_temp.getHome());
		document_publish.setFirstPage(document_temp.getFirstPage());
		document_publish.setNotificationPage(document_temp.getNotificationPage());
		document_publish.setScreenList(document_temp.getScreenList());
		document_publish.setStoryBoardName(document_temp.getStoryBoardName());

		/* save publish_entiry to DB. */
		publihsEntityConfigAccessor.savePublishConfig(document_publish, user);

		/* get entity config for update status */
		EntityDocument entityDocument = entityAccessor.findEntityByName(entityName);
		if (entityDocument != null) {

			logger.info("set publish status = " + DmaConstants.PublishedStatus.WARNING);
			entityDocument.setPublishStatus(DmaConstants.PublishedStatus.WARNING);

			logger.info("set last publish = currentDate");
			Date lastPublish = new Date();
			entityDocument.setLastPublish(lastPublish);

			logger.info("set version = {}", version);
			entityDocument.setVersion(version);

			entityDocument = (EntityDocument) entityAccessor.updateEntity(entityDocument, lastPublish, user);

			// update lastPublish to storyBoard
			updateLastPublishToStoryBoard(lastPublish, entityDocument.getStoryName(),user);

			return getEntity(entityDocument);
		} else {
			throw new Exception("Entity name \"" + entityName + "\" does not exits.");
		}

	}

	private void updateLastPublishToStoryBoard(Date lastPublish, String storyBoardName,String user) throws Exception {

		/* find storyboard */
		StoryBoardDocument storyBoardDoc = storyBoardAccessor.findStoryByName(storyBoardName);

		if (storyBoardDoc == null) {
			throw new Exception("storyboard '" + storyBoardName + "' not found.");
		}

		logger.info("set last publish = currentDate to storyboard =" + storyBoardName);
		storyBoardDoc.setLastPublish(lastPublish);

		/* publish storyboard */
		storyBoardAccessor.updateStoryBoardOneLevel(storyBoardDoc, user);

		updateLastPublishToScreen(lastPublish, storyBoardDoc,user);
	}

	private void updateLastPublishToScreen(Date lastPublish, StoryBoardDocument storyBoardDoc,String user) throws Exception {

		List<Map<String, Object>> screenRequiredList = new GetReferenceProcessor()
				.getDmaScreenRequireds(storyBoardDoc.getReference());
		if (screenRequiredList == null) {
			return;
		}

		String screenName;
		for (Map<String, Object> screenRequired : screenRequiredList) {

			screenName = ManagerMap.getString(screenRequired, ReferenceConstants.DMA.NAME);

			/* find screen */
			AppUiDocument screenDoc = appUiAccessor.findAppUiByName(screenName);

			logger.info("set last publish = currentDate to screen =" + screenName);
			screenDoc.setLastPublish(lastPublish);

			/* save screen */
			appUiAccessor.updateUi(screenDoc, user);
		}

	}

	private Map<String, Object> getEntity(EntityDocument document) {

		Map<String, Object> entity = new HashMap<String, Object>();
		entity.put(DmaConstants.Entity.Key.ENTITY_NAME, document.getEntityName());
		entity.put(DmaConstants.Entity.Key.REMARK, document.getRemark());
		entity.put(DmaConstants.Entity.Key.ROLE, document.getRole());
		entity.put(DmaConstants.Entity.Key.STORY_NAME, document.getStoryName());
		entity.put(DmaConstants.Entity.Key.THEME, document.getTheme());
		entity.put(DmaConstants.Entity.Key.VERSION, document.getVersion());
		entity.put(DmaConstants.Entity.Key.STATUS, document.getStatus());
		entity.put(DmaConstants.Entity.Key.PUBLISH_STATUS, document.getPublishStatus());
		entity.put(DmaConstants.Entity.Key.LAST_UPDATE,
				document.getLastUpdate() == null ? "" : DateTimeUtils.getFormattedDate(document.getLastUpdate()));
		entity.put(DmaConstants.Entity.Key.LAST_PUBLISH,
				document.getLastPublish() == null ? "" : DateTimeUtils.getFormattedDate(document.getLastPublish()));

		return entity;
	}

	private void validateAppInfo(Map<String,Object> appInfo) throws Exception {
		
			String appMode = (String) appInfo.get(DmaConstants.Entity.Key.APP_MODE);
			if(appMode == null || appMode.equals("")){
				throw new Exception(DmaConstants.Entity.Key.APP_MODE + ": null");
			}
			Map<String,Object> apkInfo = (Map<String,Object>) appInfo.get(DmaConstants.Entity.Key.APK_INFO);
			Map<String,Object> ipaInfo = (Map<String,Object>) appInfo.get(DmaConstants.Entity.Key.IPA_INFO);

			for (Map.Entry<String, Object> entry : apkInfo.entrySet()) {
				String str = (String)entry.getValue();
				if(str == null || str.equals("")){
					throw new Exception(entry.getKey() + ": null");
				}
			}
			for (Map.Entry<String, Object> entry : ipaInfo.entrySet()) {
				String str = (String)entry.getValue();
				if(str == null || str.equals("")){
					throw new Exception(entry.getKey() + ": null");
				}
			}
			logger.info("ValidateAppInfo ===> Success");
		
	}
}
