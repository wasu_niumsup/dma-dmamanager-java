package com.jjpa.dma.manager.binding.action.wrapper;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.dma.manager.processor.actionchain.GenerateCommonActionChainProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;

public class LocationCheck implements ActionWrapper{

	private final Logger logger = LoggerFactory.getLogger(LocationCheck.class);
	
	protected ActionChainAccessor actionChainAccessor;
	
	private String open_loading = "open_loading";
	private String get_location = "get_location";
	private String set_location = "set_location";
	
	public LocationCheck() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	@Override
	public DocumentContext wrapperActionChain(List<String> dmaParamList, DocumentContext masterAction) throws Exception {	
		
		DocumentContext wrapSetLocation = generateSetLocationActionChain(masterAction, dmaParamList);
		
		DocumentContext wrapGetLocation = new GenerateCommonActionChainProcessor().process(wrapSetLocation, get_location);
		
		DocumentContext resultContext = new GenerateCommonActionChainProcessor().process(wrapGetLocation, open_loading);
				
		return resultContext;
	}
	
	private DocumentContext generateSetLocationActionChain(DocumentContext innerContext, List<String> dmaParamList) throws Exception {
		
		/* find action chain type from db */
		DMAActionChainDocument actionChainDocument = actionChainAccessor.findActionChainByType(set_location);
		if(actionChainDocument==null){
			
			logger.info("Need patching. {}", set_location);
			throw new Exception("DMA need patching, Code \"" + set_location + "\"");
		}
		
		DocumentContext resultContext = JsonPath.parse(actionChainDocument.getNativeJson().get(DmaConstants.ActionChain.Key.DATA));
		
		
		List<Map<String, Object>> bindingList = actionChainDocument.getBinding();
		
		
		/* 
		 * binding successPath
		 *  */
		
		/* select row where name=success */
		Map<String, Object> bindingSuccessMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.SUCCESS);
		
		String successPath = DataUtils.getStringValue(bindingSuccessMap.get(DmaConstants.ActionChain.Key.PATH));
		
		logger.info("#binding setLocationActionChain success. (set)");
		
		logger.info("path={}", successPath);
		logger.info("innerContext={}", innerContext.jsonString());
		
		resultContext.set(successPath, innerContext.json());
		

		/* 
		 * binding optionsPath
		 *  */
		
		/* select row where name=options */
		Map<String, Object> bindingOptionsMap = ManagerArrayList.findRowBySearchString(bindingList, DmaConstants.ActionChain.Key.NAME, DmaConstants.ActionChain.Value.OPTIONS);
		
		String optionsPath = DataUtils.getStringValue(bindingOptionsMap.get(DmaConstants.ActionChain.Key.PATH));
		String value = "{{"+ JasonPathConstants.UiJson.Key.COORD_JASON +"}}";
		
		logger.info("#binding setLocationActionChain options. (for(put))");

		for(String dmaParam : dmaParamList){
			logger.info("path={}", optionsPath);
			logger.info("dmaParam={}", dmaParam);
			logger.info("value={}", value);
			
			resultContext.put(optionsPath, dmaParam, value);
		}
		
		return resultContext;
	}

}
