package com.jjpa.dma.manager.processor.storyboard;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.db.mongodb.accessor.StoryBoardAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class ListWithApiStoryBoardProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(ListWithApiStoryBoardProcessor.class);
		
	protected StoryBoardAccessor storyBoardAccessor;
	
	public ListWithApiStoryBoardProcessor() {
		this.storyBoardAccessor = SpringApplicationContext.getBean("StoryBoardAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> ListStoryBoardProcessor");
		logger.debug("Process ===> ListStoryBoardProcessor : {}", input.toJsonString());
		
		return storyBoardAccessor.listStoryBoardWithApi(input);
		
	}

}
