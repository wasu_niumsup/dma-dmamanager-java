package com.jjpa.dma.manager.processor.actionchaintemplate;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;

import com.jjpa.db.mongodb.accessor.ActionChainTemplateAccessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class DeleteActionChainTemplateProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteActionChainTemplateProcessor.class);
		
	protected ActionChainTemplateAccessor actionChainTemplateAccessor;
	
	public DeleteActionChainTemplateProcessor() {
		this.actionChainTemplateAccessor = SpringApplicationContext.getBean("ActionChainTemplateAccessor");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> DeleteActionChainTemplateProcessor");
		logger.debug("Process ===> DeleteActionChainTemplateProcessor : {}", input.toJsonString());
		
		actionChainTemplateAccessor.deleteActionChainTemplate(input);
		return actionChainTemplateAccessor.listActionChainTemplateType();
	}

}
