package com.jjpa.dma.manager.processor.reference;

import java.util.List;
import java.util.Map;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.dma.manager.model.reference.InUsedReference;
import com.jjpa.dma.manager.model.reference.Reference;
import com.jjpa.dma.manager.model.reference.RequiredReference;

import servicegateway.utils.ManagerArrayList;

public class DeleteReferenceProcessor {

//	private static final Logger logger = LoggerFactory.getLogger(DeleteReferenceProcessor.class);
	
	
	private Reference validate(Reference reference){ 
		if(reference==null){
//			logger.info("create Reference.");
			reference = new Reference();
		}
		return reference;
	}
	
	
	public void clearAll(Reference reference) throws Exception {
		clearVersion(reference);
		clearInUsed(reference);
		clearRequired(reference);
	}
	
	
	
	/*
	 * version
	 *  */
	public void clearVersion(Reference reference) throws Exception {
		reference = validate(reference);
		
		reference.setVersion("");
	}
	
	
	
	/*
	 * InUsed
	 *  */
	public void clearInUsed(Reference reference) throws Exception {
		reference = validate(reference);
		
		reference.setInUsed(new InUsedReference());
	}
	
	public void clearInUsedByDmaScreen(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> inUsedList;
		try{
			inUsedList = reference.getInUsed().getInUsedByDmaScreen();
		}catch (Exception e){
			reference.setInUsed(new InUsedReference());
			return;
		}
		
		clearInUsedByName(inUsedList, name);
	}
	
	public void clearInUsedByDmaStroyBoard(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> inUsedList;
		try{
			inUsedList = reference.getInUsed().getInUsedByDmaStroyBoard();
		}catch (Exception e){
			reference.setInUsed(new InUsedReference());
			return;
		}
		
		clearInUsedByName(inUsedList, name);
	}
	
	public void clearInUsedByName(List<Map<String, Object>> inUsedList, String name) throws Exception {

		/* find name for remove */
		Map<String, Object> rowSearchName = ManagerArrayList.findRowBySearchString(inUsedList, ReferenceConstants.DMA.NAME, name);
		if(rowSearchName!=null){
//			logger.info("#clear InUsed Reference at name = "+ name);
			inUsedList.remove(rowSearchName);
		}
	}
	
	
	
	/*
	 * Required
	 *  */
	public void clearRequired(Reference reference) throws Exception {
		reference = validate(reference);
		
		reference.setRequired(new RequiredReference());
	}
	
	public void clearRequiredDmaScreen(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getDmaScreenRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByName(requiredList, name);
	}
	
	public void clearRequiredDmaTemplate(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getDmaTemplateRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByName(requiredList, name);
	}
	
	public void clearRequiredDmaActionChain(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getDmaActionChainRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByName(requiredList, name);
	}
	
	public void clearRequiredDmaActionChainTemplate(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getDmaActionChainTemplateRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByName(requiredList, name);
	}
	
	public void clearRequiredDmaImage(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getDmaImageRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByName(requiredList, name);
	}
	
	public void clearRequiredSgService(Reference reference, String name)  throws Exception {
		reference = validate(reference);
		
		List<Map<String, Object>> requiredList;
		try{
			requiredList = reference.getRequired().getSgServiceRequired();
		}catch (Exception e){
			reference.setRequired(new RequiredReference());
			return;
		}
		
		clearRequiredByServiceName(requiredList, name);
	}
	
	public void clearRequiredByName(List<Map<String, Object>> requiredList, String name) throws Exception {

		/* find name for remove */
		Map<String, Object> rowSearchName = ManagerArrayList.findRowBySearchString(requiredList, ReferenceConstants.DMA.NAME, name);
		if(rowSearchName!=null){
//			logger.info("#clear Required Reference at name = "+ name);
			requiredList.remove(rowSearchName);
		}
	}
	
	public void clearRequiredByServiceName(List<Map<String, Object>> requiredList, String name) throws Exception {

		/* find name for remove */
		Map<String, Object> rowSearchName = ManagerArrayList.findRowBySearchString(requiredList, ReferenceConstants.SG.SERVICE_NAME, name);
		if(rowSearchName!=null){
			requiredList.remove(rowSearchName);
		}
	}
	
}
