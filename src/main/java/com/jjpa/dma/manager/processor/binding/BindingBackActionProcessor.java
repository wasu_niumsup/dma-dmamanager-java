package com.jjpa.dma.manager.processor.binding;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.common.DmaConstants;
import com.jjpa.common.JasonPathConstants;
import com.jjpa.common.util.JasonConfigurationUtil;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;

import blueprint.util.StringUtil;
import servicegateway.utils.ManagerJsonPath;
import servicegateway.utils.ManagerMap;

public class BindingBackActionProcessor{

	private final Logger logger = LoggerFactory.getLogger(BindingBackActionProcessor.class);
	
	public Map<String, Object> process(Map<String, Object> dataBinding, String entityName, Map<String, Object> storyBoard, StoryBoardDocument storyBoardDoc) throws Exception {

		String backType = ManagerMap.getString(storyBoard, DmaConstants.StoryBoard.Key.BACK_TYPE);
		if(!StringUtil.isBlank(backType)){
			logger.info("found back action. then binding back action. backType="+ backType);
			
			/* cast data to DocumentContext */
			DocumentContext masterTemplate = JsonPath.parse(dataBinding);
			
			
			/* generate back action */
			Map<String, Object> backAction = generateBackAction(entityName, storyBoard, storyBoardDoc);		
			logger.info("backAction="+backAction);
		
			
			/* binding back action to DocumentContext */
			masterTemplate.put(JasonPathConstants.UiJson.Root.ACTIONS, JasonPathConstants.UiJson.Key.ON_BACK, backAction);
			
			
			
			/* 21/11/2561 add by Akanit : fix back action */
			String show_type = ManagerJsonPath.read(masterTemplate, JasonPathConstants.UiJson.Root.SHOW_TYPE, String.class);
			if(!StringUtil.isBlank(show_type) && JasonPathConstants.UiJson.Value.UTIL_BACK.equals(show_type)){
				logger.info("found "+ JasonPathConstants.UiJson.Root.SHOW_TYPE +" = "+ JasonPathConstants.UiJson.Value.UTIL_BACK);
				
				Map<String, Object> options = ManagerMap.get(backAction, JasonPathConstants.UiJson.Key.OPTIONS, Map.class);
				
				logger.info("put backAction.options to "+ JasonPathConstants.UiJson.Root.SHOW);
				masterTemplate.put(JasonPathConstants.UiJson.Root.SHOW, JasonPathConstants.UiJson.Key.OPTIONS, options);
			}
			
			
			dataBinding = masterTemplate.json();
			
		}else {
			logger.info("not found back action. then do nothing.");	
		}
		
		return dataBinding;
	}
	
	private Map<String, Object> generateBackAction(String entityName, Map<String, Object> storyBoard, StoryBoardDocument storyBoardDoc) throws Exception{
		
		Map<String, Object> backAction = new HashMap<String, Object>();
		Map<String, Object> options = new HashMap<String, Object>();
		
		
		String backType = ManagerMap.getString(storyBoard, DmaConstants.StoryBoard.Key.BACK_TYPE);
		
		if(DmaConstants.StoryBoard.Value.BackType.FIRST_PAGE.equalsIgnoreCase(backType)){
			
			options.put("allowback", "true");
			options.put("url", JasonConfigurationUtil.generateHomeUrl(entityName, storyBoardDoc.getFirstPage()));
			
		}else if(DmaConstants.StoryBoard.Value.BackType.HOME.equalsIgnoreCase(backType)){
			
			options.put("allowback", "true");
			options.put("url", JasonConfigurationUtil.generateHomeUrl(entityName, storyBoardDoc.getHomePage()));
			options.put("showBackButton","true");
			
		}else if(DmaConstants.StoryBoard.Value.BackType.PIN.equalsIgnoreCase(backType)){
			
			String pinPage = ManagerMap.getString(storyBoard, DmaConstants.StoryBoard.Key.PIN_PAGE);
			if(StringUtil.isBlank(pinPage)){
				throw new Exception("Please select \"Back To Pinned Screen\".");
			}
			
			options.put("allowback", "true");
			options.put("url", JasonConfigurationUtil.generateHomeUrl(entityName, pinPage));
			options.put("showBackButton","true");
			
		}else if(DmaConstants.StoryBoard.Value.BackType.STACK.equalsIgnoreCase(backType)){
			options.put("allowback", "true");
			
		}else if(DmaConstants.StoryBoard.Value.BackType.NONE.equalsIgnoreCase(backType)){
			options.put("allowback", "false");
		}
		
		backAction.put("options", options);
		
		
		return backAction;
	}
	
	
}
