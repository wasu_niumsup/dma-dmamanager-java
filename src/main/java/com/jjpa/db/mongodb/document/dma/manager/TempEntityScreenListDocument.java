package com.jjpa.db.mongodb.document.dma.manager;

import org.springframework.data.mongodb.core.mapping.Document;

import com.jjpa.db.mongodb.document.SimpleDocument;

@Document(collection = "temp_entity_screenlist")
public class TempEntityScreenListDocument extends SimpleDocument{
	private static final long serialVersionUID = 1L;
}
