package servicegateway.utils;

import blueprint.util.StringUtil;

public class ManagerUrl {

	public static String replaceHostName(String url, String hostname) throws Exception {
		if(url==null||"".equals(url)||hostname==null){
			return null;
		}

		/* check if last char of hostname is / then remove /  */
		String lastChar = hostname.substring(hostname.length()-1);
		if(lastChar.equals("/")){
			hostname = hostname.substring(0, hostname.length()-1);
		}
		
		url = url.replace("http://", "");
		url = url.replace("https://", "");

		String[] urlSP = url.split("/");
		
		String result = hostname;
		for(int i = 1 ; i < urlSP.length ; i++){
			result += "/"+urlSP[i];
		}
		
		return result;
	}
	
	public static String generateDNS(String protocol, String hostname, String port) {
		
		String dns ="";
		if(!StringUtil.isBlank(protocol)) {
			dns=protocol+"//";
		}
		dns+=hostname;
		if(!StringUtil.isBlank(port)) {
			dns+=":"+port;
		}
		
		return dns;
	}

}
