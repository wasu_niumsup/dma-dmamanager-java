package com.jjpa.dma.manager.processor.appui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.ReferenceConstants;
import com.jjpa.common.data.service.ServiceData;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.accessor.TemplateAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.DMATemplate;
import com.jjpa.dma.manager.model.reference.InUsedReference;
import com.jjpa.dma.manager.processor.reference.DeleteReferenceProcessor;
import com.jjpa.dma.manager.processor.reference.GetReferenceProcessor;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;

public class DeleteAppUiProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{
	private static final Logger logger = LoggerFactory.getLogger(DeleteAppUiProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	protected TemplateAccessor templateAccessor;
	private String user;
	
	public DeleteAppUiProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
		this.templateAccessor = SpringApplicationContext.getBean("TemplateAccessor");
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> DeleteAppUiProcessor");
		logger.info("Process ===> DeleteAppUiProcessor : {}", input.toJsonString());
		
		user = (String)input.getValue("user");
		List<Map<String, Object>> uiList = (List<Map<String, Object>>)input.getValue("uiList");
	
		logger.info("1. validation");
		List<Map<String,Object>> errorList = new ArrayList<>();
		for(Map<String, Object> uiObj : uiList){
			String uiName = (String)uiObj.get("uiName");
			AppUiDocument  screen =  appUiAccessor.findAppUiByName(uiName);
			if(screen.getReference() != null && screen.getReference().getInUsed() !=null ){
				InUsedReference inUsed = screen.getReference().getInUsed();
				
				if (inUsed.getInUsedByDmaStroyBoard() !=null && inUsed.getInUsedByDmaStroyBoard().size() > 0 ) {
					/**
					 * Tanawat W.
					 * 
					 * 1 Screen can use in only 1 storyBoard.
					 * 
					 */
					 Map<String,Object>  storyBoard = inUsed.getInUsedByDmaStroyBoard().get(0);
					 logger.info("inUsed : " + storyBoard);
					 Map<String,Object> row = new LinkedHashMap<>();
					 row.put("uiName", uiName);
					 row.put("storyBoardName", DataUtils.getStringValue(storyBoard, ReferenceConstants.DMA.NAME,null));
					 errorList.add(row);
				}
			}
			
		}
		Map<String,Object> output = new LinkedHashMap<>();
		output.put("resultSuccess", false);
		output.put("appUiList", new LinkedHashMap<>());
		if(errorList.size() > 0){
			output.put("resultData", errorList);
			logger.info("2.1 prepare : error data");
		}else{
			// TODO Wait for akanit. Please clear inUsed in Template
			
			String screenName;
			AppUiDocument screen;
			
			for(Map<String, Object> uiObj : uiList){
				
				screenName = (String)uiObj.get("uiName");
				screen = appUiAccessor.findAppUiByName(screenName);
				
				String templateName = new GetReferenceProcessor().getDmaTemplateRequired(screen.getReference());
				clearInUsedReferenceToTemplate(templateName, screenName);
			}
			
			
			appUiAccessor.deleteAppUi(input);
			output.put("appUiList", appUiAccessor.listAppUi(input).get("appUiList"));
			output.put("resultSuccess", true);
			logger.info("2.2 delete data from db");
			/**
			 * Tanawat W.
			 * 
			 * DMA 5 Hooking Delete Screen
			 */
			callHook("DeleteScreen", input.getData());
			
		}
		logger.info("3. output= {}" , output);
		return output;
		
		
	}
	
	private void clearInUsedReferenceToTemplate(String templateName, String screenName)  throws Exception {
		
		if(templateName != null){
			DMATemplate template_old = templateAccessor.findTemplateByName(templateName);
			
			if(template_old!=null){
				logger.info("#clear InUsed Reference to template = "+ templateName);
				new DeleteReferenceProcessor().clearInUsedByDmaScreen(template_old.getReference(), screenName);
				
				templateAccessor.saveTemplate(template_old, user);
			}else {
				logger.info("#templateName="+ templateName+" not found. then skip clear InUsed Reference.");
			}
			
		}
	}
}