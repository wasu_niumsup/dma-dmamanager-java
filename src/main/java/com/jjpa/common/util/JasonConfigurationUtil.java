package com.jjpa.common.util;

import com.jayway.jsonpath.DocumentContext;

/**
 * JasonConfigurationUtil
 */
public class JasonConfigurationUtil {

    public static final String SET_LOCALVARIABLE_PATH = "$.$jason.head.actions.$load.options";
    
    private static final String _Prefix = "_dma_param_internal_";
    public static String getKeyWithPrefix(String key){
        return _Prefix + key;
    }
    public static class Key{
        public static final String ONLY_DOMAIN = "onlyDomain";
        public static final String DOMAIN = "domain";
        public static final String APPLICATION = "application";
        public static final String STORYBOARD = "storyboard";
        public static final String PUBLISH_VERSION = "publish_version";
        public static final String FIRST_PAGE = "first_page";
        public static final String DMAMANAGER_VERSION = "dmamanager_version";
    }


    public static String generateHomeUrl(String entity, String pageName) throws Exception{
        String url = JasonConfigurationUtil.getPublishUrl() + "/rest/ws/dma/loadConfig?contentType=200&source=2&message=back&_dma_entity=" + JasonConfigurationUtil.getEntity() 
        + "&_dma_storyboard=" + JasonConfigurationUtil.getStoryBoardName()
        + "&_dma_pagename=" + pageName;
		return url;
    }

    public static String getPublishUrl(){
        // {{$get._internal_domain_url}}
        return JasonNetteUtils.generateNativeValue(getKeyWithPrefix(Key.DOMAIN));
    }

    public static String getOnlyDomain(){
        // {{$get._internal_onlyDomain}}
        return JasonNetteUtils.generateNativeValue(getKeyWithPrefix(Key.ONLY_DOMAIN));
    }

    public static String getEntity(){
        return JasonNetteUtils.generateNativeValue(getKeyWithPrefix(Key.APPLICATION));
    }

    public static String getStoryBoardName(){
        return JasonNetteUtils.generateNativeValue(getKeyWithPrefix(Key.STORYBOARD));
    }

    public static void generateGetLocalValue(DocumentContext ctx,String path){
        ctx.put(path,JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.PUBLISH_VERSION),  JasonNetteUtils.generateSystemGlobalValue(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.PUBLISH_VERSION)) );
        ctx.put(path,JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DOMAIN),  JasonNetteUtils.generateNativeValue(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.DOMAIN)) );
        ctx.put(path,JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.ONLY_DOMAIN),  JasonNetteUtils.generateNativeValue(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.ONLY_DOMAIN)) );
        ctx.put(path,JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.APPLICATION),  JasonNetteUtils.generateNativeValue(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.APPLICATION)) );
        ctx.put(path,JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.STORYBOARD),  JasonNetteUtils.generateNativeValue(JasonConfigurationUtil.getKeyWithPrefix(JasonConfigurationUtil.Key.STORYBOARD)) );
    
    }
    
}