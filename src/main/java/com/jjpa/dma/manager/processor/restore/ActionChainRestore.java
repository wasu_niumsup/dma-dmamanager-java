package com.jjpa.dma.manager.processor.restore;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.MongoConstants;
import com.jjpa.db.mongodb.accessor.ActionChainAccessor;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerStore;

public class ActionChainRestore {

	private static final Logger logger = LoggerFactory.getLogger(ActionChainRestore.class);
	
	protected ActionChainAccessor actionChainAccessor;
	
	public ActionChainRestore() {
		this.actionChainAccessor = SpringApplicationContext.getBean("ActionChainAccessor");
	}
	
	
	public List<String> process(String filePath, String user) throws Exception {
		logger.info("Process ===> ActionChainRestore");
		logger.info("filePath="+ filePath);
		
		List<String> actionChainList = new ArrayList<>();
		
		File file = new File(filePath);
		if(file.exists()){
			
			DMAActionChainDocument document;
			Map<String, Object> jsonMap;
			for(File fileEntry : file.listFiles()){
				if (!fileEntry.isDirectory()) {

					actionChainList.add(fileEntry.getName());
					
					jsonMap = ManagerStore.readJsonMapFromFile(fileEntry, true);
					
					/* 16/05/2561 new lastUpdate */
					jsonMap.put(MongoConstants.Path.LAST_UPDATE, new Date());
					
					document = new DMAActionChainDocument(jsonMap);
					
					actionChainAccessor.saveActionChain(document, user);
					logger.info("restore ActionChainType="+ document.getActionChainType());
				}
			}
		}
		
		logger.info("End Process ===> ActionChainRestore");
		return actionChainList;
	}
}
