package com.jjpa.dma.manager.model.reference;

public class Reference {
	
	private String version;
	private RequiredReference required;
	private InUsedReference inUsed;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public RequiredReference getRequired() {
		return required;
	}
	public void setRequired(RequiredReference required) {
		this.required = required;
	}
	public InUsedReference getInUsed() {
		return inUsed;
	}
	public void setInUsed(InUsedReference inUsed) {
		this.inUsed = inUsed;
	}
	
}
