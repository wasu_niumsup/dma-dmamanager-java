package com.jjpa.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.http.ssl.EasySSLProtocolSocketFactory;

import blueprint.util.StringUtil;

public class JsonAccessor {

	private static final Logger logger = LoggerFactory.getLogger(JsonAccessor.class);
	@SuppressWarnings("rawtypes")
	private Map dns;
	private String protocol= "http";
	private String address = null;
	private int port = 80;

	private MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
//	private int maxTotalConnections, maxConnectionsPerHost, timeout;
	
	protected HttpClient httpClient = null;

	public void setMaxTotalConnections(int maxTotalConnections){
		connectionManager.getParams().setMaxTotalConnections(maxTotalConnections);
//		this.maxTotalConnections = maxTotalConnections;
	}
	
	public void setDefaultMaxConnectionsPerHost(int maxConnectionsPerHost){
		connectionManager.getParams().setDefaultMaxConnectionsPerHost(maxConnectionsPerHost);
//		this.maxConnectionsPerHost = maxConnectionsPerHost;
	}
	
	public void setConnectionTimeout(int timeout){
		connectionManager.getParams().setConnectionTimeout(timeout);
		connectionManager.getParams().setSoTimeout(timeout);
//		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
//		httpClient.getHttpConnectionManager().getParams().setSoTimeout(timeout);
//		this.timeout = timeout;
	}

	public JsonAccessor(String protocol, String address, int port){
		setDefaultMaxConnectionsPerHost(50);
		setMaxTotalConnections(400);
		createHttpClient(protocol, address, port);
	}
	
	@SuppressWarnings("rawtypes")
	public void setDns(Map dns){
		this.dns=dns;
	}
	
//	public void createHttpClient(){
//		httpClient = new HttpClient(connectionManager);
//		
//		
//		RequestConfig a = RequestConfig.custom().setConnectTimeout(5000).setSocketTimeout(5000).build();
		
//		RequestConfig requestConfig = RequestConfig.custom()
//		        .setConnectTimeout(5000)
//		        .setSocketTimeout(5000)
//		        .build();
//		SocketConfig socketConfig = SocketConfig.custom()
//		        .setSoTimeout(5000)
//		        .build();
//		CloseableHttpClient client = HttpClients.custom()
//		        .setDefaultRequestConfig(requestConfig)
//		        .setDefaultSocketConfig(socketConfig)
//		        .build();
//		
//		
//		
//		httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, value);
//		address = new HttpClient(httpConnectionManager)
//		org.apache.http.params.HttpConnectionParams.setConnectionTimeout(params, timeout);
//		
//		httpClient.getHostConfiguration().setHost(address, port, protocol);
//		httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
//	}
	
	private void createHttpClient(String protocol, String address, int port){
		this.protocol = protocol;
		this.address = address;
		this.port = port;
		httpClient = new HttpClient(connectionManager);
		httpClient.getHostConfiguration().setHost(address, port, protocol);
		httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
//		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(1000*30);
//		httpClient.getHttpConnectionManager().getParams().setSoTimeout(1000*20);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T send(String endpoint, T input) throws Exception {
		String className = null;
		if(input != null){
			if(input instanceof String){
				return (T)send(endpoint, (String)input);
			}else if(input instanceof DocumentContext){
				return (T)send(endpoint, (DocumentContext)input);
			}else{
				className = input.getClass().getName();
			}
		}
		throw new Exception("Json Proxy not support Data " + className);
	}
	
	public DocumentContext send(String endpoint, DocumentContext reqContext) throws Exception {
		return send(endpoint, reqContext, (Map<String,String>)null);
	}
		
	
	
	
	
	public DocumentContext send(String endpoint, DocumentContext reqContext, Map<String,String> header) throws Exception {
		String resString = send(endpoint, reqContext.jsonString(), header);
		DocumentContext resContext = JsonPath.parse(resString);
		return resContext;
	}
	
	public String send(String endpoint, String connent) throws Exception {
		return send(endpoint, connent, (Map<String,String>)null);
	}
	
	public String send(String endpoint, String connent, Map<String,String> header) throws Exception {
		endpoint=createEndpoint(endpoint);
		logger.info("endpoint="+endpoint);
		PostMethod httpMethod = new PostMethod(endpoint);
		try {
			// Apache HTTP Client setup
			ProtocolSocketFactory p = new EasySSLProtocolSocketFactory();
	        Protocol.unregisterProtocol("https");
	        Protocol.registerProtocol( "https", new Protocol("https", p, 443));
			RequestEntity requestEntity = new StringRequestEntity(connent, "application/json;charset=UTF-8", "UTF-8");
			httpMethod.setRequestEntity(requestEntity);
			String headerName = null;
			String headerValue = null;
			// set http header
			if(header != null){
				Iterator<String> iter = header.keySet().iterator();
				while (iter.hasNext()) {
					headerName = (String) iter.next();
					headerValue = header.get(headerName);
					httpMethod.setRequestHeader(headerName, headerValue);
				}
			}
			URI uri = URI.create(endpoint);
			headerValue = getCookie(uri);
			if(headerValue != null){
				headerName = "Cookie";
				httpMethod.setRequestHeader(headerName, headerValue);
			}
			
	        int statusCode = httpClient.executeMethod(httpMethod);
	        // set cookie from response
	        Header headerSetCookie = null;
	        if((headerSetCookie = httpMethod.getResponseHeader("Set-Cookie2")) != null
	        || (headerSetCookie = httpMethod.getResponseHeader("Set-Cookie")) != null){
	        	setCookie(uri, headerSetCookie.getValue());
	        }
	        
			if (HttpStatus.SC_OK <= statusCode && statusCode <= HttpStatus.SC_ACCEPTED) {
				Header contentEncoding = httpMethod.getResponseHeader("Content-Encoding");
				if (contentEncoding != null && contentEncoding.getValue().indexOf("gzip") != -1) {
					logger.debug("This is gzipped Content-encoding header " + contentEncoding);
					InputStream zippedInputStream = new GZIPInputStream(httpMethod.getResponseBodyAsStream());
					return IOUtils.toString(zippedInputStream, httpMethod.getResponseCharSet());
				}
				return httpMethod.getResponseBodyAsString();
			}else{
				throw new Exception(httpMethod.getStatusLine().toString());
			}
		//} catch (Exception e) {
		//	throw e;
		} finally {
			httpMethod.releaseConnection();
		}
	}

	public void close(){
		httpClient.getHttpConnectionManager().closeIdleConnections(0);
	}
	
	public void setCookie(URI uri, String cookieValue){
		try {
			CookieHandler cookieHandler = CookieHandler.getDefault();
			List<String> cookies = new ArrayList<String>();
			cookies.add(cookieValue);
		    if(!cookies.isEmpty() && cookieHandler != null){
			    Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();
			    headers.put("Set-Cookie", cookies);
			    cookieHandler.put(uri, headers);
		    }
		} catch (IOException e) {
			logger.error("Set-Cookie", e);
		}
	}

	public String getCookie(URI uri){
		String cookieValue = null;
		Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();
		try {
			CookieHandler cookieHandler = CookieHandler.getDefault();
			if(cookieHandler != null){
				headers = cookieHandler.get(uri, headers);
				List<String> cookies = headers.get("Cookie");
				StringBuilder escapedCookie = new StringBuilder();
				for(String value:cookies){
					if (escapedCookie.length() > 0) {
						escapedCookie.append("; ");
					}
					escapedCookie.append(value);
				}
			}
		} catch (IOException e) {
			logger.error("Cookie", e);
		}
		return cookieValue;
	}
	
	public String createEndpoint(String endpoint){
		int start=-1;
		int end=-1;
		if((start=endpoint.indexOf("://") + 3) > 3 && (end=endpoint.indexOf("/", start+1))>0){
			String protocol = endpoint.substring(0, start - 3);
			String txt=endpoint.substring(start, end);
			String ip="";
			String port="";
			String[] tmp=txt.split(":");
			if(tmp.length>0)ip=tmp[0];
			if(tmp.length>1)port=tmp[1];
			if(dns !=null && dns.containsKey(ip)) ip=(String)dns.get(ip);
			if(StringUtil.isBlank(port) || ("http".equals(protocol) && "80".equals(port)) || ("https".equals(protocol) && "443".equals(port))){
				endpoint = endpoint.replaceFirst(txt, ip);
			}else{
				endpoint = endpoint.replaceFirst(txt, ip+":"+port);
			}
		}else if(endpoint.indexOf("://") == -1){
			String ip = address;
			String portStr = port==0 || ("http".equals(protocol) && port == 80) || ("https".equals(protocol) && port == 443)? "":(":"+port);
			if(dns !=null && dns.containsKey(ip)) ip=(String)dns.get(ip);
			if(!endpoint.startsWith("/")){
				endpoint = "/"+endpoint;
			}
			endpoint = protocol +"://"+ ip + portStr + endpoint;
		}
		return endpoint;
	}
	
}
