package com.jjpa.common.data.service;

import java.io.Serializable;

public class ServiceRequest<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T requestData;
	
	public T getRequestData() {
		return requestData;
	}
	public void setRequestData(T requestData) {
		this.requestData = requestData;
	}

}
