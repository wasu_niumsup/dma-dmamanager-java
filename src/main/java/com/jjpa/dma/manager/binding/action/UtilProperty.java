package com.jjpa.dma.manager.binding.action;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jjpa.db.mongodb.document.dma.manager.DMAActionChainDocument;

/**
 * UtilProperty
 */
public class UtilProperty implements ActionBinding {
	private final Logger logger = LoggerFactory.getLogger(UtilProperty.class);

	@Override
	public DocumentContext bind(String entity, DMAActionChainDocument document, Map<String, Object> screenConfig,
			Map<String, Object> eventMap) throws Exception {
		logger.info("Binding UtilProperty in Action");
		DocumentContext ctx = JsonPath.parse(document.getNativeJson().get("data"));

		logger.warn("Do nothing wait for implement");

		//document.getNativeJson()
		//screenConfig
		// JasonNetteUtils.getGlobalName(name)
		logger.info("entity : " +  entity);
		logger.info("screenConfig : " + screenConfig);
		logger.info("eventMap : " + eventMap);
		logger.info("document.getActionChainType : "+document.getActionChainType());
		logger.info("document.getNativeJson : "+document.getNativeJson());

		
		Map<String,Object> uiJson = JsonPath.parse(screenConfig.get("uiJson").toString()).json();
		logger.info("uiJson : " + uiJson ) ;
		DocumentContext findInputCtx = JsonPath.parse(uiJson);

//		String actionType = (String)eventMap.get("type");


		if( "$set".equalsIgnoreCase(  findInputCtx.read("$.$jason.head.actions.$load.type").toString() ) ){
			Map<String,Object> optionsInput = findInputCtx.read("$.$jason.head.actions.$load.options");
			logger.info("optionsInput : " + optionsInput);
			
			Map<String,Object> options = ctx.read("$.options");

			logger.info("Before copy options : " + options);
//			long i = 0;
			for (Map.Entry<String,Object> pair : optionsInput.entrySet()) {
				//i += pair.getKey() + pair.getValue();
				String key = pair.getKey();
				if( "name".equalsIgnoreCase(key) ){
					logger.info("Skip name");
					continue;
				}

				options.put(key, pair.getValue());

			}

			logger.info("After copy options : " + options);
		}


		logger.info("ctx=" + ctx.jsonString());
		return ctx;
	}

}