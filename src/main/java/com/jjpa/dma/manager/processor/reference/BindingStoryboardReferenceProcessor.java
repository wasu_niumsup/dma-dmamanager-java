package com.jjpa.dma.manager.processor.reference;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.DmaConstants;
import com.jjpa.common.util.DataUtils;
import com.jjpa.db.mongodb.accessor.AppUiAccessor;
import com.jjpa.db.mongodb.document.dma.manager.AppUiDocument;
import com.jjpa.db.mongodb.document.dma.manager.StoryBoardDocument;
import com.jjpa.dma.manager.processor.reference.inused.binding.BindingInUsedReferenceToScreenProcessor;
import com.jjpa.dma.manager.processor.reference.required.binding.BindingRequiredReferenceToStoryBoardProcessor;
import com.jjpa.dma.manager.processor.reference.version.binding.BindingVersionReferenceProcessor;
import com.jjpa.spring.SpringApplicationContext;

import servicegateway.utils.ManagerArrayList;

/* 
 * 12/06/2561 create by Akanit : for DMA5 
 * */
public class BindingStoryboardReferenceProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BindingStoryboardReferenceProcessor.class);
	
	protected AppUiAccessor appUiAccessor;
	
	public BindingStoryboardReferenceProcessor() {
		this.appUiAccessor =  SpringApplicationContext.getBean("AppUiAccessor");
	}
	
	public void process(StoryBoardDocument storyBoardDoc, List<String> screenList, String user) throws Exception {
		logger.info("process : BindingReferenceProcessor.");
		
		
		/* update version reference */
		new BindingVersionReferenceProcessor().process(storyBoardDoc.getReference());
		
		
		String screenName;
		AppUiDocument appUiDoc;
		
		for(Map<String, Object> storyBoard : storyBoardDoc.getStoryBoard()){
			
			screenName = DataUtils.getStringValue(storyBoard, DmaConstants.StoryBoard.Key.SOURCE, null);
			
			if(ManagerArrayList.find(screenList, screenName)){
				
				appUiDoc = appUiAccessor.findAppUiByName(screenName);
				
				if(appUiDoc==null){
					throw new Exception("screen '"+ screenName +"' not found.");
				}
				
				/* Reference */
				
				/* update inUsed reference to appUI(screen) */
				new BindingInUsedReferenceToScreenProcessor().process(appUiDoc, storyBoardDoc);
				appUiAccessor.saveUi(appUiDoc, user);
				
				/* update required reference to storyBoard */
				new BindingRequiredReferenceToStoryBoardProcessor().process(storyBoardDoc, storyBoard, appUiDoc);
			}
			
		}
		
	}
	
}
