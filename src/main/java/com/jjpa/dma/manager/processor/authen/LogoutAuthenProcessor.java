package com.jjpa.dma.manager.processor.authen;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jjpa.common.data.service.ServiceData;
import com.jjpa.processor.CommonProcessor;
import com.jjpa.processor.ServiceProcessor;
import com.jjpa.spring.SpringApplicationContext;
import com.jjpa.web.servlet.UserSession;

public class LogoutAuthenProcessor extends CommonProcessor<ServiceData> implements ServiceProcessor<ServiceData>{

	private static final Logger logger = LoggerFactory.getLogger(LogoutAuthenProcessor.class);
	
	private UserSession getUserSession(){
		return SpringApplicationContext.getBean("UserSession");
	}
	
	@Override
	public Object process(HttpServletRequest request, ServiceData input) throws Exception {
		
		logger.info("Process ===> LogoutAuthenProcessor");
		logger.debug("Process ===> LogoutAuthenProcessor : {}", input.toJsonString());
		
		return logout(input);
		
	}
	
	public Map<String, Object> logout(ServiceData input) throws Exception {
		UserSession session = getUserSession();
		session.setUser(null);
		return null;
	}
	
}
